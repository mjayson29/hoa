<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Main extends CI_Model {

		function login($username, $password) {

			$this -> db -> select();
			$this -> db -> from('ZVL_users');
			$this -> db -> where('vl_u_uname = ' . "'" . htmlspecialchars($username) . "'"); 
			$this -> db -> where('vl_u_pword = ' . "'" . MD5($password) . "'"); 
			$this -> db -> where('vl_is_active = 1'); 
			$this -> db -> limit(1);

			$query = $this -> db -> get();
			if($query -> num_rows() == 1)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		}

		function select_data($select, $table){

			$this -> db -> SELECT($select)
						-> FROM($table);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_odby($select, $table, $order){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($order);
			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

			function select_data_l($select, $table, $start, $limit){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> LIMIT($start, $limit);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

			function select_data_odby_l($select, $table, $odby, $start, $limit){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($odby)
						-> LIMIT($start, $limit);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_where($select, $table, $where){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_where_in($select, $table, $where, $array){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE_IN($where, $array);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_where_limit($select, $table, $where, $limit){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> LIMIT($limit);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_odby_gpby($select, $table, $odby, $gpby){

			$this -> db -> SELECT($select)
						-> FROM($table)
						// -> WHERE($where)
						-> GROUP_BY($gpby)
						-> ORDER_BY($odby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_distinct_where($select, $table, $where, $odby){

			$this -> db -> DISTINCT()
						-> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> ORDER_BY($odby);

			$query = $this -> db -> get();

			return $query -> result_array();

		}

		function get_data_distinct($select, $table, $odby){

			$this -> db -> DISTINCT()
						-> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($odby);

			$query = $this -> db -> get();

			return $query -> result_array();

		}

		function get_data_where($select, $table, $where, $orderby){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> ORDER_BY($orderby, 'ASC');

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_where_limit($select, $table, $where, $orderby, $limit, $start){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> ORDER_BY($orderby)
						-> LIMIT($limit, $start);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_join($select, $table, $orderby, $join1, $join2){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> ORDER_BY($orderby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_join_wherein($select, $table, $join1, $join2, $where, $array){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> WHERE_IN($where, $array);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_where_join($select, $table, $orderby, $join1, $join2, $where){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> WHERE($where)
						-> ORDER_BY($orderby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_where_join_limit($select, $table, $orderby, $join1, $join2, $where, $limit, $offset){ 

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> WHERE($where)
						-> ORDER_BY($orderby)
						-> LIMIT($limit, $offset);
 			  
			$query = $this -> db -> get();

			return $query -> result_array();

		}

		function count_select($select, $table){

			$this -> db -> SELECT($select)
						-> FROM($table);

			$query = $this -> db -> get();

			return $query -> num_rows();

		}	

		function count_select_where($select, $table, $where){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where);

			$query = $this -> db -> get();

			return $query -> num_rows();

		}	

		function count_select_where_orderby($select, $table, $where, $orderby){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($orderby)
						-> WHERE($where);

			$query = $this -> db -> get();

			return $query -> num_rows();

		}

		function insert_data($table, $data){

	  		$this -> db -> insert($table, $data);
	  		return $this -> db -> insert_id();

	  	}	

	  	function update_data($table, $data, $where) {

	  		$this -> db -> WHERE($where);
	        $this -> db -> UPDATE($table, $data);

	  	}

	  	function delete_data_where($where, $id, $table) {
	  		$this->db->where($where, $id);
	  		$this->db->delete($table);
	  	}

	}

/* End of file main.php */
/* Location: ./application/models/main.php */