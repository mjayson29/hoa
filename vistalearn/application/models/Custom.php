<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Custom extends CI_Model {
		
		function custom1($query) {
			$query = $this->db->query($query);
			return $query->result_array();	
		}

		function get_course($limit, $offset) {
			$query ="SELECT * FROM ZVL_courses
					ORDER BY c_desc ASC, c_active ASC
			 		OFFSET ".$offset." ROWS 
 			  		FETCH NEXT ".$limit." ROWS ONLY";
			$query = $this->db->query($query);
			return $query->result_array();
		}

		function get_course_rows() {
			$query ="SELECT * FROM ZVL_courses";
			$query = $this->db->query($query);
			return $query->num_rows();
		}
		function get_quotes($limit, $offset) {
			$query ="SELECT * FROM ZVL_announcements
					WHERE a_display <= '".date('m/d/Y')."'
					ORDER BY a_display DESC
			 		OFFSET ".$offset." ROWS 
 			  		FETCH NEXT ".$limit." ROWS ONLY";
			$query = $this->db->query($query);
			return $query->result_array();
		}
		
		function get_quote_rows() {
			$query ="SELECT * FROM ZVL_announcements";
			$query = $this->db->query($query);
			return $query->num_rows();
		}

		function get_announcements($limit, $offset) {
			$query ="SELECT * FROM ZVL_announcements
					ORDER BY a_display DESC
			 		OFFSET ".$offset." ROWS 
 			  		FETCH NEXT ".$limit." ROWS ONLY";
			$query = $this->db->query($query);
			return $query->result_array();
		}
		
		function get_announcement_rows() {
			$query ="SELECT * FROM ZVL_announcements";
			$query = $this->db->query($query);
			return $query->num_rows();
		}

		function get_cur_month_training($date){

			$date = explode("-",$date);
			$yr = $date[0];
        	$month = $date[1];

			$query = "	SELECT * FROM ZVL_trainings
						WHERE DATEPART(year,t_start)=$yr
						AND
						DATEPART(month,t_start)=$month
						AND t_is_deleted = 0
					";
			$query = $this->db->query($query);
			return $query->result_array();	
		}

		function get_selected_day_training($date){

			$date = explode("-",$date);
			$yr = $date[0];
        	$month = $date[1];
        	$day = $date[2];

			$query = "	SELECT * FROM ZVL_trainings
						WHERE DATEPART(year,t_start)=$yr
						AND
						DATEPART(month,t_start)=$month
						AND
						DATEPART(day,t_start)=$day
						AND t_is_deleted = 0
					";
			$query = $this->db->query($query);
			return $query->result_array();	
		}
	}
?>