<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class courses extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Manila");
        $this -> load -> model('Main','',TRUE);
        $this -> load -> model('Custom','',TRUE);
        $this -> load -> helper('text');

    }

    function index(){

    	$data = $this->check_access();

    	$data['active'] = "courses";
        $data['bg'] = "bg home";
        $data['tc'] = $this->Main->select_data($select='', $table='ZVL_training_courses');

    	$this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/courses');
        $this -> load -> view('templates/footer');

    }

    function fetch(){
        if ($_POST) {
            $id = $this->input->post('tc');
            $query = "";
            $select = "";
            $table = "ZVL_training_det";
            $where = "td_tc_id = ".$id." and td_active = 1";
            $content = $this->Main->select_data_where($select,$table,$where);
            if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                echo "<h3>Positions</h3>";
            }else{
                echo "<h3>Courses</h3>";
            }
            echo "<div class='cont'>";
            echo "<table id='enlistedCourse' class='table'>";
            foreach ($content as $a) {
                if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                    $table = "ZVL_positions";
                    $where = "p_id = ".$a['td_course'];
                }else{
                    $table = "ZVL_courses";
                    $where = "c_id = ".$a['td_course'];
                }
                $course = $this->Main->select_data_where($select,$table,$where);
                foreach ($course as $b) {
                    if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                        $key = $b['p_id'];
                        $desc = $b['p_desc'];
                    }else{
                        $key = $b['c_id'];
                        $desc = $b['c_desc'];
                    }
                    echo "<tr id='cr".$key."' class='train'><td><div>".$desc."<span onclick='rem(".$key.")'>&#215;</span></div></td></tr>";
                }
            }
            echo "</table>";
            echo "</div>";
            echo "<div class='opt'>";
            if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                    $query = "SELECT * FROM ZVL_positions as a WHERE a.p_id NOT IN (
                        SELECT td_course FROM ZVL_Training_det as b WHERE b.td_tc_id = ".$id." and td_active = 1)";
                }else{
                    $query = "SELECT * FROM ZVL_courses as a WHERE a.c_id NOT IN (
                        SELECT td_course FROM ZVL_Training_det as b WHERE b.td_tc_id = ".$id." and td_active = 1)";
                }
            
            $not = $this->Custom->custom1($query);
            echo "<select id='sel_course' class='form-control input-sm' onchange='enlist()'>";
            echo "<option value=''>Select</option>";
            foreach ($not as $c) {
                if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                    $key = $c['p_id'];
                    $desc = $c['p_desc'];
                }else{
                    $key = $c['c_id'];
                    $desc = $c['c_desc'];
                }
                echo "<option value='".$key."'>".$desc."</option>";
            }
            echo "</select>";
            echo "</div>";
        }
    }

    function addCourse() {
        if($_POST){
            $data = $this->check_access();
            $now = date("Y-m-d h:i:s");
            $id = $this->input->post('col');
            $cid = $this->input->post('val');

            $table = "ZVL_training_det";
            $data = array('td_tc_id'=>$id,
                    'td_course'=>$cid,
                    'td_by'=>$data['u_id'],
                    'td_date'=>$now,
                    'td_active'=>1);
            $this->Main->insert_data($table,$data);
        }
    }

    function remCourse() {
        if($_POST){
            $id = $this->input->post('col');
            $cid = $this->input->post('cid');

            $table = "ZVL_training_det";
            $data = array('td_active'=>0);
            $where = "td_tc_id = ".$id." and td_course = ".$cid;
            $this->Main->update_data($table, $data, $where);

            if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                $query = "SELECT * FROM ZVL_positions as a WHERE a.p_id NOT IN (
                      SELECT td_course FROM ZVL_Training_det as b WHERE b.td_tc_id = ".$id." and td_active = 1)";
             }else{
                $query = "SELECT * FROM ZVL_courses as a WHERE a.c_id NOT IN (
                      SELECT td_course FROM ZVL_Training_det as b WHERE b.td_tc_id = ".$id." and td_active = 1)";
            }
            $not = $this->Custom->custom1($query);
            echo "<select id='sel_course' class='form-control input-sm' onchange='enlist()'>";
            echo "<option value=''>Select</option>";
            foreach ($not as $c) {
                if($id == 1 || $id == 8 || $id == 15 || $id == 22 || $id == 29){
                    $key = $c['p_id'];
                    $desc = $c['p_desc'];
                }else{
                    $key = $c['c_id'];
                    $desc = $c['c_desc'];
                }
                echo "<option value='".$key."'>".$desc."</option>";
            }
            echo "</select>";
        }
    }

    function check_access(){
        if(!$this->session->userdata('l@ginUs3r')){
            redirect(base_url('/'));
            exit;
        } else {
            $session_data = $this -> session -> userdata('l@ginUs3r');
            $data['u_fname'] = $session_data['u_fname'];
            $data['u_lname'] = $session_data['u_lname'];
            $data['u_id'] = $session_data['u_id'];
            $data['u_uname'] = $session_data['u_uname'];
            $data['u_pword'] = $session_data['u_pword'];
            $data['u_role'] = $session_data['u_role'];
            $data['is_active'] = $session_data['is_active']; 
            return $data;
        }
    }

}
?>

