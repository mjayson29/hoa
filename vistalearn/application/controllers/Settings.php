<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class settings extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Manila");
        $this -> load -> model('Main','',TRUE);
        $this -> load -> model('custom','',TRUE);
        $this -> load -> helper('text');
        $this -> load-> library('Pagination');

    }

    function index(){

    	$data = $this->check_access();

    	$data['active'] = "";
        $data['bg'] = "bg home";
        $data['current'] = "";

    	$this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/settings_nav');
        $this -> load -> view('templates/footer');

    }

    function courses() {
        $data = $this->check_access();

        $data['active'] = "";
        $data['bg'] = "bg home";
        $data['current'] = "mtc";

        $this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/settings_nav');
        $this -> load -> view('pages/settings_courses');
        $this -> load -> view('templates/footer');
    }

    function course_table($offset = 0){
        $config['base_url'] = base_url('settings/course_table/0');
        $config['per_page'] = 10;
        $config['total_rows'] = $this -> custom -> get_course_rows();
        $this->pagination->initialize($config);
        $data['new']= $this -> custom -> get_course($config["per_page"],$offset);
        $data["new_links"] = $this->pagination->create_links();
        $data['total_pages'] = $config['total_rows'];
        $data['display_offset'] = $offset + 1;
        $data['display_limit'] = $offset + $config['per_page'];
        if($data['display_limit'] > $data['total_pages']){
            $data['display_limit'] = $data['total_pages'];
        }
        if($data['total_pages'] == 0){
            $data['display_offset'] = $offset;
            $data['display_limit'] = 0;
        }
        $this -> load -> view('pages/course_table', $data);
    }

    function add_course(){
        if($_POST){
            $data = $this->check_access();
            $now = date('Y-m-d H:i:s');
            $name = $this->input->post('name');
            $desc = $this->input->post('desc');

            $data1 = array('c_name'=>$name, 
                        'c_desc'=>$desc,
                        'c_active'=>1,
                        'c_createdby'=>$data['u_id'],
                        'c_createdate'=>$now);
            $table = "ZVL_courses";
            $this->Main->insert_data($table,$data1);
        }
    }

    function announcements() {
        $data = $this->check_access();

        $data['active'] = "";
        $data['bg'] = "bg home";
        $data['current'] = "mtc";

        $this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/settings_nav');
        $this -> load -> view('pages/settings_announcements');
        $this -> load -> view('templates/footer');
    }

    function announcement_table($offset = 0) {
        $config['base_url'] = base_url('settings/announcement_table/0');
        $config['per_page'] = 10;
        $config['total_rows'] = $this -> custom -> get_announcement_rows();
        $this->pagination->initialize($config);
        $data['new']= $this -> custom -> get_announcements($config["per_page"],$offset);
        $data["new_links"] = $this->pagination->create_links();
        $data['total_pages'] = $config['total_rows'];
        $data['display_offset'] = $offset + 1;
        $data['display_limit'] = $offset + $config['per_page'];
        if($data['display_limit'] > $data['total_pages']){
            $data['display_limit'] = $data['total_pages'];
        }
        if($data['total_pages'] == 0){
            $data['display_offset'] = $offset;
            $data['display_limit'] = 0;
        }
        $this -> load -> view('pages/announcement_table', $data);
    }

    function add_announcements(){
        if($_POST){
            $data = $this->check_access();
            $now = date('Y-m-d H:i:s');
            $ddate = $this->input->post('display');
            $cont = $this->input->post('cont');
            $cont1 = $this->input->post('cont1');

            $data1 = array('a_content'=>$cont, 
                        'a_content1'=>$cont1,
                        'a_display'=>$ddate,
                        'a_date'=>$now,
                        'a_by'=>$data['u_id'],
                        'a_active'=>1);
            $table = "ZVL_announcements";
            $this->Main->insert_data($table,$data1);
        }
    }

    function soft(){
        $data = $this->check_access();

        $data['active'] = "";
        $data['bg'] = "bg home";
        $data['current'] = "mat";

        $select = "";
        $table = "ZVL_trainings";
        $data['trainings'] = $this->Main->select_data($select, $table);

        $this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/settings_nav');
        $this -> load -> view('pages/settings_soft');
        $this -> load -> view('templates/footer');
    }

    function technical(){
        $data = $this->check_access();

        $data['active'] = "";
        $data['bg'] = "bg home";
        $data['current'] = "mat";

        $select = "";
        $table = "ZVL_trainings";
        $data['trainings'] = $this->Main->select_data($select, $table);

        $this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/settings_nav');
        $this -> load -> view('pages/settings_technical');
        $this -> load -> view('templates/footer');
    }

        function training_info(){
            $id = $this->input->post('id');
            $select = "";
            $table = "ZVL_trainings";
            $where = "t_id = ".$id;
            $data['info'] = $this->Main->select_data_where($select,$table,$where);
            $this -> load -> view('pages/tr_info', $data);
        }

        function upload_materials(){
            if($_POST){
                $filename = '';
                $config['upload_path'] = APPPATH.'../assets/uploads/learning';
                $config['allowed_types'] = 'pdf|docx|doc|xls|xlsx';
                $config['max_size'] = '2097152';    //2097152 B or 2MB

                $files = $_FILES;
                $count = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $count; $i++) {
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];  

                    $this->load->library('upload', $config);
                    if ( ! $this->upload->do_upload()) {
                        $error = array('error' => $this->upload->display_errors());
                        echo "<script> alert('".$error['error']."');</script>";
                        $filename = "Attachment is not valid";
                    } else {
                        $upload_data = $this->upload->data();
                        $filename = $upload_data['file_name'];
                    }
                }

                $tr_id = $this -> input -> post('tr_id');
                $cat = $this ->input -> post('cat');
                $today = date('Y-m-d H:i:s');

            }
        }

    function check_access(){
        if(!$this->session->userdata('l@ginUs3r')){
            redirect(base_url('/'));
            exit;
        } else {
            $session_data = $this -> session -> userdata('l@ginUs3r');
            $data['u_fname'] = $session_data['u_fname'];
            $data['u_lname'] = $session_data['u_lname'];
            $data['u_id'] = $session_data['u_id'];
            $data['u_uname'] = $session_data['u_uname'];
            $data['u_pword'] = $session_data['u_pword'];
            $data['u_role'] = $session_data['u_role'];
            $data['is_active'] = $session_data['is_active']; 
            return $data;
        }
    }

}
?>
