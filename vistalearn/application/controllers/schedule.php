<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class schedule extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Manila");
        $this -> load -> model('Main','',TRUE);
        $this -> load -> model('Custom','',TRUE);
        $this -> load -> helper('text');
        $this->load->library('calendar');

    }

    function index(){

        $data = $this->check_access();

        date_default_timezone_set("Asia/Manila"); 

        $get_date = date('Y-m-d H:i:s'); //Returns IST

        $date = date('Y/m', strtotime($get_date));

        $data['active'] = "schedule";
        $data['bg'] = "bg home";

        $this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/schedule');
        $this -> get_month($date);
        $this -> load -> view('templates/footer');
        
    }

    function check_access(){
        if(!$this->session->userdata('l@ginUs3r')){
            redirect(base_url('/'));
            exit;
        } else {
            $session_data = $this -> session -> userdata('l@ginUs3r');
            $data['u_fname'] = $session_data['u_fname'];
            $data['u_lname'] = $session_data['u_lname'];
            $data['u_id'] = $session_data['u_id'];
            $data['u_uname'] = $session_data['u_uname'];
            $data['u_pword'] = $session_data['u_pword'];
            $data['u_role'] = $session_data['u_role'];
            $data['is_active'] = $session_data['is_active']; 
            return $data;
        }
    }

    function get_month($val=NULL){

        if($val == "" || $val == " ")
        $date = explode("/",$_POST['id']);
        else
        $date = explode("/",$val);

        $yr = $date[0];
        $month = $date[1];

        $date = $yr."-".$month;
        $trainings = $this -> Custom -> get_cur_month_training($date);
        $data = array();

        foreach($trainings as $t){
            $start = $t['t_start'];
            $datestart = str_replace('/', '-', $start);
            $datestart = date('d', strtotime($datestart));

            if($datestart[0] == 0)
            {    

            $datestartcheck = substr($datestart, -1);

            }

            else{

                $datestartcheck = $datestart;
            }

            if($datestartcheck == 0){

                $datestartcheck = $datestart;
            }

            $id = $t['t_id'];

            $data[$datestartcheck] = "<a title='Training' style='color:black;' onclick = 'myFunction2($id)' href ='#'>".$t['t_desc']."</a>";
        }

        $vars['calendar'] = $this->calendar->generate($yr, $month, $data);
        $this->load->view('pages/calendar', $vars);

    }

    function view_trainings(){

            $date = explode("/",$_POST['tc']);

            $yr = $date[0];
            $month = $date[1];
            $day = $date[2];
        
            $date = $yr."-".$month."-".$day;
            $trainings = $this -> Custom -> get_selected_day_training($date);

            echo "<div class='modal-container'>";
            echo "<div class='col-md-12'>
                        <a href='#' id ='add_training' onclick = 'myFunction()'><span title ='View Training' class ='icon-file pull-right'></span></a>
                        <br><br>
                  </div>";
            foreach($trainings as $row)
            {
                $id = $row['t_id'];
                $desc = $row['t_desc'];
                $place = $row['t_place'];
                $created_by = $row['t_created_by'];
                $select = "*";
                $table = "ZVL_users";
                $where = "vl_u_id =".$created_by;
                $create = $this -> Main -> select_data_where($select, $table, $where);
                foreach($create as $c){
                    $createdby = $c['vl_u_lname'] .', '. $c['vl_u_fname'] .' '. $c['vl_u_mname']; 
                }
            echo "<div class='col-md-12'>";
            echo "<label>$desc</label>";
            echo "<a href='#'><span title ='Delete Training' onclick='delTraining($id)' class ='icon-trash pull-right'></span></a>";
            echo "<a href='#' onclick = 'myFunction2($id)'><span title ='Edit Training' class ='icon-pencil pull-right'></span></a>";
            echo "<br>";
            echo "<p>Time; $place; $createdby</p>";
            echo "<hr>";
            echo "</div>";

            }
            echo "</div>";
        

    }

    function add_event(){
        
            echo "<div class='modal-container'>";
            echo "<form method ='POST' "; ?> action ='<?=base_url()?>schedule/add_training/'> <?php
            echo "<label class='control-label col-md-2' for='comment'>Training</label>";
            echo "<div class='col-md-10'>
                        <textarea rows='8' class='form-control' id='desc' name='comments' placeholder='Description'></textarea>
                  </div>";
            echo "<p><span></span><span ><label style ='visibility:hidden;'>Label</label></span></p>";
            echo "<label class='control-label col-md-2' for='comment'>Period</label>";
            echo "<div class='col-md-4'>
                 <input type ='date' class='form-control' id ='start' placeholder ='mm/dd/yyyy'>
                </div>";
            echo "<label class='control-label col-md-1' for='comment'>-</label>";
            echo "<div class='col-md-4'>
            <input type ='date' class='form-control' id ='end' placeholder ='mm/dd/yyyy'>
                </div>";
            echo "<div class='col-md-1'>
                    &nbsp;
                </div>";
            echo "<p><span></span><span ><label style ='visibility:hidden;'>Label</label></span></p>";
            echo "<label class='control-label col-md-2' for='comment'>Place</label>";
            echo "<div class='col-md-6'>
                 <input type ='text' id = 'place' name ='place' class='form-control'>
                </div>";
            echo "<p><span></span><span ><label style =''><hr></label></span></p>";
            echo "<div class='col-md-4'>
                 <input type='button' id ='btnTraining' class='btn btn-primary' name='btnTraining' value='Submit' onclick='addTraining()'>
                </div>";
            echo "</form>";
            echo "</div>";
        
    }

    function add_training(){
        
            $data = $this->check_access();
            $now = date("Y-m-d h:i:s");
            $desc = $this->input->post('desc');
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $place = $this->input->post('place');

            $table = "ZVL_trainings";
            $data = array(  't_desc'        => $desc,
                            't_start'       => $start,
                            't_end'         => $end,
                            't_place'       => $place,
                            't_created_by'  => $data['u_id'],
                            't_updated_by'  => $data['u_id'],
                            't_is_deleted'  => 0
                            );
            $this->Main->insert_data($table,$data);



            $yr = date('Y', strtotime($start));
            $month = date('m', strtotime($start));

            $date = $yr."-".$month;
            $trainings = $this -> Custom -> get_cur_month_training($date);

            $data = array();

            foreach($trainings as $t){
            $start = $t['t_start'];
            $datestart = str_replace('/', '-', $start);
            $datestart = date('d', strtotime($datestart));

            if($datestart[0] == 0)
            {    

            $datestartcheck = substr($datestart, -1);

            }

            else{

                $datestartcheck = $datestart;
            }

            if($datestartcheck == 0){

                $datestartcheck = $datestart;
            }

            $id = $t['t_id'];

            $data[$datestartcheck] = "<a title='Training' style='color:black;' onclick = 'myFunction2($id)' href ='#'>".$t['t_desc']."</a>";
        }

            $vars['calendar'] = $this->calendar->generate($yr, $month, $data);
            $this->load->view('pages/calendar', $vars);


        
    }

    function edit_event(){
        if ($_POST) {

            $id = $_POST['tc'];

            $select = "*";
            $table = "ZVL_trainings";
            $where = "t_id = ".$id;
            $orderby = "t_start";
            $training = $this -> Main -> get_data_where($select, $table, $where, $orderby);

            foreach($training as $t){
                $id = $t['t_id'];
                $desc = $t['t_desc'];
                $start = substr($t['t_start'], 0, -15);
                $end = substr($t['t_end'], 0, -15);
                $place = $t['t_place'];

            }

            echo "<div class='modal-container'>";
            echo "<form method ='POST' "; ?> action ='<?=base_url()?>schedule/edit_training/'> <?php
            echo "<label class='control-label col-md-2' for='comment'>Training</label>";
            echo "<input type ='text' id ='train_id' name ='train_id' style ='display: none;' value = '$id'>";
            echo "<div class='col-md-10'>
                        <textarea rows='8' class='form-control' id='edesc' name='comments' placeholder='Description'>$desc</textarea>
                  </div>";
            echo "<p><span></span><span ><label style ='visibility:hidden;'>Label</label></span></p>";
            echo "<label class='control-label col-md-2' for='comment'>Period</label>";
            echo "<div class='col-md-4'>
                 <input type ='date' class='form-control' id ='estart' placeholder ='mm/dd/yyyy' value ='$start'>
                </div>";
            echo "<label class='control-label col-md-1' for='comment'>-</label>";
            echo "<div class='col-md-4'>
            <input type ='date' class='form-control' id ='eend' placeholder ='mm/dd/yyyy' value ='$end'>
                </div>";
            echo "<div class='col-md-1'>
                    &nbsp;
                </div>";
            echo "<p><span></span><span ><label style ='visibility:hidden;'>Label</label></span></p>";
            echo "<label class='control-label col-md-2' for='comment'>Place</label>";
            echo "<div class='col-md-6'>
                 <input type ='text' id = 'eplace' name ='eplace' class='form-control' value ='$place'>
                </div>";
            echo "<p><span></span><span ><label style =''><hr></label></span></p>";
            echo "<div class='col-md-4'>
                 <input type='button' id ='btnTraining' class='btn btn-primary' name='btnTraining' value='Update' onclick='editTraining()'>
                </div>";
            echo "</form>";
            echo "</div>";
        }
    }

    function edit_training(){
        if($_POST){
            $data = $this->check_access();
            $now = date("Y-m-d h:i:s");
            $id = $this->input->post('id');
            $desc = $this->input->post('desc');
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $place = $this->input->post('place');

            $where = "t_id = ".$id;
            $table = "ZVL_trainings";
            $data = array(  't_desc'        => $desc,
                            't_start'       => $start,
                            't_end'         => $end,
                            't_place'       => $place,
                            't_updated_by'  => $data['u_id']
                            );
            $this->Main->update_data($table, $data, $where);

            $yr = date('Y', strtotime($start));
            $month = date('m', strtotime($start));

            $date = $yr."-".$month;
            $trainings = $this -> Custom -> get_cur_month_training($date);

            $data = array();

            foreach($trainings as $t){
            $start = $t['t_start'];
            $datestart = str_replace('/', '-', $start);
            $datestart = date('d', strtotime($datestart));

            if($datestart[0] == 0)
            {    

            $datestartcheck = substr($datestart, -1);

            }

            else{

                $datestartcheck = $datestart;
            }

            if($datestartcheck == 0){

                $datestartcheck = $datestart;
            }

            $id = $t['t_id'];

            $data[$datestartcheck] = "<a title='Training' style='color:black;' onclick = 'myFunction2($id)' href ='#'>".$t['t_desc']."</a>";
        }

            $vars['calendar'] = $this->calendar->generate($yr, $month, $data);
            $this->load->view('pages/calendar', $vars);


        }
    }


    function delete_training(){
        if($_POST){
            $data = $this->check_access();
            $now = date("Y-m-d h:i:s");
            $id = $this->input->post('id');

            $date = date('Y/m', strtotime($now));

            $date = explode("/", $date);
            $yr = $date[0];
            $month = $date[1];

            $where = "t_id = ".$id;
            $table = "ZVL_trainings";
            $data = array(  't_is_deleted' => 1,
                            't_updated_by'  => $data['u_id']
                            );
            $this->Main->update_data($table, $data, $where);

            $date = $yr."-".$month;
            $trainings = $this -> Custom -> get_cur_month_training($date);

            $data = array();

            foreach($trainings as $t){
            $start = $t['t_start'];
            $datestart = str_replace('/', '-', $start);
            $datestart = date('d', strtotime($datestart));

            if($datestart[0] == 0)
            {    

            $datestartcheck = substr($datestart, -1);

            }

            else{

                $datestartcheck = $datestart;
            }

            if($datestartcheck == 0){

                $datestartcheck = $datestart;
            }

            $id = $t['t_id'];

            $data[$datestartcheck] = "<a title='Training' style='color:black;' onclick = 'myFunction2($id)' href ='#'>".$t['t_desc']."</a>";
        }

            $vars['calendar'] = $this->calendar->generate($yr, $month, $data);
            $this->load->view('pages/calendar', $vars);


        }
    }

}
?>

