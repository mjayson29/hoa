<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {

  	function __construct(){
	    parent::__construct();
		date_default_timezone_set("Asia/Manila");
	    $this -> load -> model('Main','',TRUE);
	    $this -> load -> model('custom','',TRUE);
	    $this -> load -> helper('text');
	    $this->load->library('session');
	    $this->load->library('pagination');
	}

	function index(){
		$data = $this->check_access();
		
		if($data != FALSE){
			$data['active'] = "home";
			$data['bg'] = "bg home";

			$this -> load -> view('templates/header', $data);
			$this -> load -> view('pages/home');
		}
	}

	 function announcements($offset = 0) {
        $config['base_url'] = base_url('home/announcements/0');
        $config['per_page'] = 1;
        $config['last_link'] = False;
        $config['first_link'] = False;
        $config['next_link'] = "<span class='icon-circle-left'> </span> PREV";
        $config['prev_link'] = "NEXT <span class='icon-circle-right'> </span>";
        $config['display_pages'] = FALSE;
        $config['total_rows'] = $this -> custom -> get_quote_rows();
        $this->pagination->initialize($config);
        $data['new']= $this -> custom -> get_quotes($config["per_page"],$offset);
        $data["new_links"] = $this->pagination->create_links();
        $data['total_pages'] = $config['total_rows'];
        $data['display_offset'] = $offset + 1;
        $data['display_limit'] = $offset + $config['per_page'];
        if($data['display_limit'] > $data['total_pages']){
            $data['display_limit'] = $data['total_pages'];
        }
        if($data['total_pages'] == 0){
            $data['display_offset'] = $offset;
            $data['display_limit'] = 0;
        }
        $this -> load -> view('pages/quotes', $data);
    }

	function verify_login(){

	    //This method will have the credentials validation
	    $this->load->library('form_validation');

	    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
	    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
	  
	    if($this->form_validation->run() == FALSE){
	        //Field validation failed.  User redirected to login page
	        $this -> load -> view('templates/loginheader');
	        $this -> load -> view('pages/login');
	        $this -> load -> view('templates/footer');
		}else{
	        //Go to private area
	        redirect(base_url('home/'), 'refresh');
	    }
	}

	function check_database($password){
	    //Field validation succeeded.  Validate against database
	    $username = $this->input->post('username');
	    //$filtered = mysql_real_escape_string(stripslashes($username));
	    
	    //query the database
	    $result = $this->Main->login($username, $password);
	    
	    if($result){
	      	$sess_array = array();
	      	foreach($result as $row){
	        	$sess_array = array('u_fname' => $row->vl_u_fname,
						          'u_lname' => $row->vl_u_lname,
						          'u_id' => $row->vl_u_id,
						          'u_uname' => $row->vl_u_uname,
						          'u_pword' => $row->vl_u_pword,
						          'u_role' => $row->vl_u_role,
						          'is_active' => $row->vl_is_active
	        	);
	        	$this->session->set_userdata('l@ginUs3r', $sess_array);
	    	}
			date_default_timezone_set("Asia/Manila"); 
			return TRUE;
	    }else{
	      	$this->form_validation->set_message('check_database', "<label class='col-md-12 col-sm-12 col-lg-12 col-xs-12 label label-danger flash-notif' style='font-size: 15px;'> Invalid Username or Password. </label><br><br>");
	        return false;
	    }
	}

	function check_access(){
	    if(!$this->session->userdata('l@ginUs3r')){
	        $this -> load -> view('templates/loginheader');
			$this -> load -> view('pages/login');
        	$this -> load -> view('templates/footer');
        	return false;
	    } else {
	        $session_data = $this -> session -> userdata('l@ginUs3r');
	        $data['u_fname'] = $session_data['u_fname'];
	        $data['u_lname'] = $session_data['u_lname'];
	        $data['u_id'] = $session_data['u_id'];
	        $data['u_uname'] = $session_data['u_uname'];
	        $data['u_pword'] = $session_data['u_pword'];
	        $data['u_role'] = $session_data['u_role'];
	        $data['is_active'] = $session_data['is_active']; 
	        return $data;
	    }
	}

	function logout(){
		$this->session->unset_userdata('l@ginUs3r');
		session_destroy();
		redirect(base_url('/'), 'refresh');
	}		

}

?>

