<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class users extends CI_Controller {

    function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Manila");
        $this -> load -> model('Main','',TRUE);
        $this -> load -> helper('text');

    }

    function index(){

    	$data = $this->check_access();

    	$data['active'] = "users";
        $data['bg'] = "bg home";

    	$this -> load -> view('templates/header', $data);
        $this -> load -> view('pages/users');
        $this -> load -> view('templates/footer');
    }

    function check_access(){
        if(!$this->session->userdata('l@ginUs3r')){
            redirect(base_url('/'));
            exit;
        } else {
            $session_data = $this -> session -> userdata('l@ginUs3r');
            $data['u_fname'] = $session_data['u_fname'];
            $data['u_lname'] = $session_data['u_lname'];
            $data['u_id'] = $session_data['u_id'];
            $data['u_uname'] = $session_data['u_uname'];
            $data['u_pword'] = $session_data['u_pword'];
            $data['u_role'] = $session_data['u_role'];
            $data['is_active'] = $session_data['is_active']; 
            return $data;
        }
    }

}
?>

