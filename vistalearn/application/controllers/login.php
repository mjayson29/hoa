<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this -> load -> model('Main','',TRUE);
    $this -> load -> helper('text');
    $this->load->library('session');
  }

function index(){

			if($this->session->userdata('l@ginUs3r')) {

			    $session_data = $this -> session -> userdata('l@ginUs3r');
			    $data['u_id'] = $session_data['u_id'];
			    $data['u_uname'] = $session_data['u_uname'];
			    $data['u_pword'] = $session_data['u_pword'];
			    $data['u_role'] = $session_data['u_role'];
			    $data['is_active'] = $session_data['is_active']; 

			    redirect(base_url('hr/'),'refresh');

			} else {

				$this -> load -> view('templates/loginheader');
				$this -> load -> view('pages/login');
        $this -> load -> view('templates/footer');

			}

		}
  
function verify_login(){

          //This method will have the credentials validation
          $this->load->library('form_validation');

          $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
          $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
  
          if($this->form_validation->run() == FALSE)
          {
          //Field validation failed.  User redirected to login page
            $this -> load -> view('templates/loginheader');
            $this -> load -> view('pages/login');
            $this -> load -> view('templates/footer');

          }
          else
          {
            //Go to private area
            redirect(base_url('hr/'), 'refresh');
          }

          }

  function check_database($password)
  {
    //Field validation succeeded.  Validate against database
    $username = $this->input->post('username');
    //$filtered = mysql_real_escape_string(stripslashes($username));
    
    //query the database
    $result = $this->Main->login($username, $password);
    
    if($result)
    {
      $sess_array = array();
      foreach($result as $row)
      {
        $sess_array = array(
          'u_fname' => $row->vl_u_fname,
          'u_lname' => $row->vl_u_lname,
          'u_id' => $row->vl_u_id,
          'u_uname' => $row->vl_u_uname,
          'u_pword' => $row->vl_u_pword,
          'u_role' => $row->vl_u_role,
          'is_active' => $row->vl_is_active
        );
        $this->session->set_userdata('l@ginUs3r', $sess_array);
      }

      date_default_timezone_set("Asia/Manila"); 

      return TRUE;
    }
    else
    {
      $this->form_validation->set_message('check_database', "<label class='col-md-12 col-sm-12 col-lg-12 col-xs-12 label label-danger flash-notif' style='font-size: 15px;'> Invalid Username or Password. </label><br><br>");
          return false;
    }
  }

  function changepword(){
        if(isset($_REQUEST['email'])){
          $email = $_REQUEST['email'];
          $regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 
          if ( preg_match( $regex, $email ) ) {
            $email = $_REQUEST['email'];
            $select = "vl_u_email, vl_u_id";
            $table = "ZVL_users";
            $validation = $this -> Main -> select_data($select, $table);
            $counter=0;
            foreach($validation as $row){
              if($email == $row['vl_u_email']){
                $counter++;
                $owner = $row['vl_u_id'];
              }
            }
            if($counter != 1){
              echo "<script> alert('Invalid Email. Please try again.') </script>";
            } else {
              // if($sec1 == $rsec1 AND $ans1 == $rans1 AND $sec2 == $rsec2 AND $ans2 == $rans2){
              $hash = hash('adler32', date('Y-m-d H:i:s').$owner.'uNiqU3'); //Returns IST   
              $where = "vl_u_id";
              $table = "ZVL_users";
              $id = $owner;
              $data = array(
                'vl_u_pword' => md5($hash));
              $this -> Main -> update_data($where, $table, $id, $data);
              // $select = "b_id, b_uname";
              $select = "vl_u_uname";
              $where = "vl_u_id = ".$owner;
              $table = "ZVL_users";
              $username = $this -> Main -> select_data_where($select, $table, $where);
              foreach($username as $row){
                $uname = $row['vl_u_uname'];
              }
              $email  = $_POST['email'];  
              $subject = "Vista Learn Change of Password";
              $message =  "<style>
                    .email_format{
                    font-family: 'arial'; 
                    font-size: 16px;}
                    </style>
                    <div class='email_format'>".
                    "Your Password has been successfully updated.".
                    "<br/>".
                    "User ID <span style='font-weight: bold;'>$uname</span>".
                    "<br/>".
                    "Your new password is <span style='font-weight: bold;'>$hash</span>".
                    "<br/>".
                    "<br/>".
                    "<a href='".base_url('login/')."'>Click here to login </a>".
                    "</div>";
              $this -> send_email($email, $message, $subject);
              echo "<script> alert('Password has been successfully reset. Please check your email for your new password.'); </script>";
              echo "<script> window.location.href='".base_url('login')."' </script>";
              // } else {
                // echo "<script> alert('Security information is not valid.'); </script>";
              // }  
            }
          } else { 
              echo "<script> alert('".$email . " is an invalid email. Please try again.'); </script>";
          } 
        }
      }

 function send_email($email, $message, $subject){
      $this->load->library('email');
      $this->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => '192.168.7.83',
          'smtp_user' => '',
          'smtp_pass' => '',
          'smtp_port' => 25,
          'crlf' => "\r\n",
          'newline' => "\r\n",
          'mailtype' => 'html'
      ));
      $this->email->from('noreply@vistahome.com.ph', "Vista Learn");
      $this->email->to($email);
      $this->email->subject($subject);
      $this->email->message($message);
      if($this->email->send()){
        return true;
      }else{
        return false;
      }
    }     

}
?>

