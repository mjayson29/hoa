<script type="text/javascript">
	$(document).ready(function() {
		$('#q_cont').hide();       
        $.ajax({
            type:"get",
            url: "<?=base_url()?>home/announcements/",
            success: function(data){
                $('#q_cont').html(data);
                $('#q_cont').fadeIn(500);
            }
        });
	});
</script>		

		<div class="table">
			<div class="quote">
				<div class="q_header">Quote Of The Day:</div><br/>
				<div id="q_cont"></div>
				<!-- <div class="q_body">“Only by binding together by a single force will we remain strong and unconquerable.”</div><br/>
				<div class="q_footer">- Chris Bradford</div><br/> -->
			</div>
			<div id="q_keys"></div>
		</div>
	</div>	
</body>