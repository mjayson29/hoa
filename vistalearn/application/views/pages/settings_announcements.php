<script type="text/javascript">
	$(document).ready(function() {
		$('#course_table').hide();       
        $.ajax({
            type:"get",
            url: "<?=base_url()?>settings/announcement_table/",
            success: function(data){
                $('#announcement_table').html(data);
                $('#announcement_table').fadeIn(500);
            }
        });
	});

	$(document).ready(function() {
		$('#succ').css('display','none');
		$('#fail').css('display','none');
		$('form').submit(function(event) {

	        $.ajax({
	            type        : 'POST',
	            url         : '<?=base_url()?>settings/add_announcements/',
	            data		:	{
	            			display : $('#a_ddate').val(),
	            			cont : $('#a_content').val(),
	            			cont1 : $('#a_cont2').val()
	            }, 
	            success: function(data){
					$('#succ').css('display','block');
	            }
	        });
        	event.preventDefault();
    	});
	});
</script>

<div class="col-md-10">
	<div class="row">
		<div class="col-md-10">
			<label class="title">Announcements</label>
			<div class="panel panel-primary">
				<div class="panel-heading">Add New</div>
				<div class="panel-body">
					<form method="POST" action="">
						<div class="ifield"><span>* </span><input type="date" id="a_ddate" class="input-sm" placeholder='Display Date' required></div>
						<div class="ifield"><span>* </span><textarea maxlength="150" id="a_content" class="input-sm" placeholder='Annoucement Content' required></textarea></div>
						<div class="ifield">&nbsp;&nbsp;<input type="date" id="a_cont2" class="input-sm" placeholder='Others'></div>
						&nbsp;&nbsp;<input type="submit" class="btn btn-sm" value="Submit"><br/>
						<span id='succ'>You have successfully added one course.</span>
						<span id='fail'></span>
					</form>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Search
				</div>
			</div>
			<div id="announcement_table"></div>
		</div>
	</div>
</div>