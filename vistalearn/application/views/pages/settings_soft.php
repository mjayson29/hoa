<script type="text/javascript">
	function disp(val){
		$.ajax({
			type: 'POST',
			url: '<?=base_url()?>settings/training_info/',
			data: {
				id: val
			},
			success: function(data){
				$('#training_info').html(data);
				$('#training_info').fadeIn(500);
			}
		});
	}

	$(document).ready(function() {
		$('#succ').css('display','none');
		$('#fail').css('display','none');

		$('#formup').submit(function(event) {

			console.log('1');

			var tr_id = $('#tr selected:option').val();
		    var fileInput = $('#userfile');
	        var files = fileInput.prop('files');
	        var cat = "soft";

	        if(files.length != 0){

	        	console.log('2');
	            var fd = new FormData();
	            fd.append('tr_id', tr_id);
	            fd.append('cat', cat);
	            var ins = files.length;
				for (var x = 0; x < ins; x++) {
				    fd.append("userfile[]", files[x]);
				}

	            $.ajax({
	                url: '<?=base_url()?>settings/upload_materials/',
	                data: fd,
	                contentType: false,
	                processData:false,
	                type:'POST',
	                dataType:'json',
	                success: function(result){
	                    $('#succ').css('display','block');
	                }
	            });
	        	event.preventDefault();
	        }
    	});
	});

</script>

<div class="col-md-10">
	<div class="row">
		<div class="col-md-10">
			<label class="title">Soft Skills</label>
			<div class="panel panel-primary">
				<div class="panel-heading">Add New</div>
				<div class="panel-body">
					<form enctype="multipart/form-data" accept-charset="utf-8" name="formname" id="formup"  method="post" action="">
						<div class="ifield"><span>* Select Training</span>
							<select id="tr" onchange="disp(this.value)">
								<option value="">Select Training</option>
							<?php
								foreach($trainings as $a){
									echo "<option value='".$a['t_id']."'>".$a['t_desc']."</option>";
								}
							?>
							</select>
						</div>
						<div id="training_info"></div>
						<div class="ifield">&nbsp;&nbsp;<input type="file" multiple id="userfile" size="3" /></div>
						&nbsp;&nbsp;<input type="submit" class="btn btn-sm" value="Submit"><br/>
						<span id='succ'>You have successfully uploaded a file.</span>
						<span id='fail'></span>
					</form>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Search
				</div>
			</div>
			<div id="announcement_table"></div>
		</div>
	</div>
</div>