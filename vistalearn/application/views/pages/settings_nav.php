<?php 
    $mtc=""; $mat="";
    switch ($current) {
        case 'mtc':
            $mtc = "in";
            break;
        case 'mat':
            $mat = "in";
            break;
        default:
            # code...
            break;
    }
?>
<!-- <div class="row"> -->

    <div id="settings_menus" class="col-md-2">
        <span><img src="<?=base_url('assets/img/gif/gear.gif')?>" style='margin:-10px'></img>SETTINGS</span>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" role="tab" id="headingOne">
                    <h4 class="panel-title"><a href="">Maintenance </a> </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse <?=$mtc?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <ul>
                            <li>
                                <a href="<?=base_url()?>settings/courses">Course</a>
                            </li>
                            <li>
                                <a href="<?=base_url()?>settings/announcements">Announcements</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" role="tab" id="headingTwo">
                    <h4 class="panel-title"><a href="">Learning Materials </a> </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse <?=$mat?>" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <ul>
                            <li>
                                <a href="<?=base_url()?>settings/soft">Soft Skills</a>
                            </li>
                            <li>
                                <a href="<?=base_url()?>settings/technical">Technical Skills</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" role="tab" id="headingThree">
                    <h4 class="panel-title"> Menu 3 </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <ul>
                            <li>
                                <a href="">Menu 3 Option 1</a>
                            </li>
                            <li>
                                <a href="">Menu 3 Option 2</a>
                            </li>
                            <li>
                                <a href="">Menu 3 Option 3</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- </div> -->