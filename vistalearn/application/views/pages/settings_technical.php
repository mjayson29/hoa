
<div class="col-md-10">
	<div class="row">
		<div class="col-md-10">
			<label class="title">Technical Skills</label>
			<div class="panel panel-primary">
				<div class="panel-heading">Add New</div>
				<div class="panel-body">
					<form method="POST" action="">
						<div class="ifield"><span>* Select Training</span>
							<select>
								<option value="">Select Training</option>
							<?php
								foreach($trainings as $a){
									echo "<option value='".$a['t_id']."'>".$a['t_desc']."</option>";
								}
							?>
							</select>
						</div>
						<div class="ifield"><span>* </span><textarea maxlength="150" id="a_content" class="input-sm" placeholder='Annoucement Content' required></textarea></div>
						<div class="ifield">&nbsp;&nbsp;<input type="date" id="a_cont2" class="input-sm" placeholder='Others'></div>
						&nbsp;&nbsp;<input type="submit" class="btn btn-sm" value="Submit"><br/>
						<span id='succ'>You have successfully added one course.</span>
						<span id='fail'></span>
					</form>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Search
				</div>
			</div>
			<div id="announcement_table"></div>
		</div>
	</div>
</div>