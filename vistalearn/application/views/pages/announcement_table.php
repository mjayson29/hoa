<script type="text/javascript">
	function paginate(val){
  	$('#course_table').hide();
  	$('#loading').show();
    $.ajax({
    	type:"get",
    	url: "<?=base_url()?>settings/announcement_table/"+val,
      success: function(data){
        $('#announcement_table').html(data);
        $('#announcement_table').fadeIn(500);
        $('#loading').hide();
      }
    });
  }
</script>

<div class="panel panel-primary">
	<div class="table-responsive trans" style="border-radius:inherit">
		<table class="table" style="border-radius:inherit">
			<tr style="background-color:#337AB7;color:#fff">
				<th style="width: 20%;text-align:;border-color:#337AB7">Display Date</th>
				<th style="width: 20%;text-align:;border-color:#337AB7">Content</th>
				<th style="width: 15%;text-align:;border-color:#337AB7">Date Added</th>
				<th style="width: 15%;text-align:;border-color:#337AB7">Added By</th>
				<th style="width: 10%;text-align:;border-color:#337AB7">Status</th>
			</tr>
	<?php 
		foreach ($new as $a) {
			switch ($a['a_active']) {
				case 0:
					$stat = "Inactive";
					break;
				case 1:
					$stat = "Active";
					break;
				default:
					break;
			}
			$s = "";
			$t = "ZVL_users";
			$w = "vl_u_id = ".$a['a_by'];
			$q = $this->Main->select_data_where($s,$t,$w);
			foreach ($q as $b) {
				$u = $b['vl_u_uname'];
			}

			$d = strtotime($a['a_date']);
			$dd = strtotime($a['a_display']);
			echo "<tr class='data_click'>";
			echo "	<td onclick='' style='width: 20%;'>".date('F d, Y', $dd)."</td>";
			echo "	<td onclick='' style='width: 20%;'>".$a['a_content']."</td>";
			echo "	<td onclick='' style='width: 15%;'>".date('F d, Y', $d)."</td>";
			echo "	<td onclick='' style='width: 15%;'>".$u."</td>";
			echo "	<td onclick='' style='width: 10%;'>".$stat."</td>";
			echo "</tr>";
		}
	?>
		</table>
	</div>
</div>
<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>