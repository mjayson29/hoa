<script type="text/javascript">
	$(document).ready(function() {
		$('#course_table').hide();       
        $.ajax({
            type:"get",
            url: "<?=base_url()?>settings/course_table/",
            success: function(data){
                $('#course_table').html(data);
                $('#course_table').fadeIn(500);
            }
        });
	});

	$(document).ready(function() {
		$('#succ').css('display','none');
		$('#fail').css('display','none');
		$('form').submit(function(event) {

	        $.ajax({
	            type        : 'POST',
	            url         : '<?=base_url()?>settings/add_course/',
	            data		:	{
	            			name : $('#t_name').val(),
	            			desc : $('#t_desc').val()
	            }, 
	            success: function(data){
					$('#succ').css('display','block');
	            }
	        });
        	event.preventDefault();
    	});
	});
</script>

<div class="col-md-10">
	<div class="row">
		<div class="col-md-10">
			<label class="title">Courses</label>
			<div class="panel panel-primary">
				<div class="panel-heading">Add New</div>
				<div class="panel-body">
					<form method="POST" action="">
						<span>* </span><input type="text" id="t_name" class="input-sm" placeholder='Course Name' required>
						<span>* </span><input type="text" id="t_desc" class="input-sm" placeholder='Course Description' required>
						<input type="submit" class="btn btn-sm" value="Submit"><br/>
						<span id='succ'>You have successfully added one course.</span>
						<span id='fail'></span>
					</form>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Search
				</div>
			</div>
			<div id="course_table"></div>
		</div>
	</div>
</div>