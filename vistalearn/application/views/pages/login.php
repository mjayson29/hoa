
<section>

	<article class="container-fluid" id="container-login">

        <div class="col-sm-offset-4 col-sm-4 col-md-4 col-md-offset-4 login-form tab-field login-bg" id="login">

            <hr>

            <form method="POST" action="<?=base_url('home/verify_login')?>">

                <div id="flash-notif"><?php echo validation_errors(); ?></div>

                <input type="text" class="form-control login-fields" placeholder="Username" name="username" autocomplete="off" autofocus>
                <br>
                <input type="password" class="form-control login-fields" placeholder="Password" name="password" autocomplete="off">
                <br>
                <input type="submit" class="form-control btn-login" value="Login" name="hoa_login">
                <br>
                <br>
                <a href="javascript:;" onclick="changepass()" style="float: right;"><i>Forgot Password?</i></a>
                <br>
                <br>

            </form>

        </div>

    <div class="col-sm-offset-4 col-sm-4 col-md-4 col-md-offset-4 login-form tab-field login-bg" id="changepass">

            <h4 style="float: left;">&nbsp;Change Password</h4>
            <h4><a href="javascript:;" onclick="cancel_cp()" style="float: right; color: red;"><span class="glyphicon glyphicon-remove"></span></a></h4>
            <br>
            <hr>

            <form method="POST" action="<?=base_url()?>login/changepword">

                <div id="notification"></div>
                <input type="email" class="form-control" placeholder="email@email.com" name="email" id="email" autocomplete="off" required>
                <br>
                <input type="submit" class="form-control btn btn-primary" value="Submit" name="hoa_changepass" id="hoa_changepass" onclick="changepword()">
                <br>
                <br>

            </form>

        </div>

    </article>

</section>

<script type="text/javascript">

        $(function() {

            $('#flash-notif').hide().slideDown('fast').delay('1500').slideUp('fast');
            $('#login').show();
            $('#changepass').hide();
                             
        });

        function cancel_cp(){

            $('#login').slideDown('fast');
            $('#changepass').slideUp('fast');

        }

        function changepass(){

            $('#login').slideUp('fast');
            $('#changepass').slideDown('fast');

        }

    </script>
 