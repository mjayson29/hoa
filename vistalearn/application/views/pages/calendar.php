<!DOCTYPE html>
<html>
<head>
<title>Calendar Example</title>
<style type="text/css">
.calendar {
	font-family: Arial, Verdana, Sans-serif;
	width: 100%;
	min-width: 960px;
	border-collapse: collapse;
}

.calendar tbody tr:first-child th {
	color: #505050;
	margin: 0 0 10px 0;
}

.day_header {
	font-weight: normal;
	text-align: center;
	color: #7697B2;
	background: rgba(255, 255, 255, 0.54);
	font-size: 15px;
}

.main_header {
	font-weight: normal;
	text-align: center;
	color: #7697B2;
	background: rgb(55, 174, 255);
	font-size: 15px;
}

.calendar td {
	width: 14%; /* Force all cells to be about the same width regardless of content */
	border:1px solid #CCC;
	background: rgba(86, 162, 213, 0.52);
	height: 100px;
	vertical-align: top;
	font-size: 10px;
	padding: 0;
}

.calendar td:hover {
	background: rgba(142, 137, 137, 0.46);
}

.day_listing {
	display: block;
	text-align: right;
	font-size: 12px;
	color: #2C2C2C;
	padding: 5px 5px 0 0;
}

div.today {
	background: rgba(233, 239, 247, 0.46);
	height: 100%;
}

.modal-title{
	height: 30px;
    line-height: 30px;
    border-bottom: 1px solid #CECECE;
}

.modal-inside{
	margin-left: 3px;
	width: 570px;
	border-top-width: 0;
	border: 1px solid transparent;
	background-color: #fff;
}

.modal-container{
  visibility: visible;
    height: relative;
    display: block;
    top: 159px;
    left: 313px;
}

.modal-text{
	padding: 2px 0 2px 10px;
  width: 200px;
  height:auto;
}

.modal-break{
  padding: 15px;
  border-bottom: 1px solid #e5e5e5;
  margin: 0;
}
</style>

<script type="text/javascript">
        $(function getMonth(){
            $('.link').click(function(){
                var elem = $(this);
                $.ajax({
                    type: "post",
                    url: "<?=base_url()?>schedule/get_month/",
                    data: "id="+elem.attr('data-artid'),
                    success: function(data) {	
                               $('#calendar_page').html(data);
                    }

                });
                return false;
            });
        });

function addTraining() {
    var desc = $("textarea#desc").val();
    var start = $("input#start").val();
    var end = $("input#end").val();
    var place = $("input#place").val();

    $.ajax({
                    type: "post",
                    url: "<?=base_url()?>schedule/add_training/",
                    data: {
                      desc : desc,
                      start : start,
                      end : end,
                      place : place
                    },
                    success: function(data) { 
                               $('#tcourse').modal('hide');
                               $('#calendar_page').html(data);
                    }

                });
  }

function editTraining() {
    var id = $("#train_id").val();
    var desc = $("textarea#edesc").val();
    var start = $("input#estart").val();
    var end = $("input#eend").val();
    var place = $("input#eplace").val();

    $.ajax({
                    type: "post",
                    url: "<?=base_url()?>schedule/edit_training/",
                    data: {
                      id : id,
                      desc : desc,
                      start : start,
                      end : end,
                      place : place
                    },
                    success: function(data) { 
                               $('#ecourse').modal('hide');
                               $('#calendar_page').html(data);
                    }

                });
  }

function delTraining(x) {

    $.ajax({
                    type: "post",
                    url: "<?=base_url()?>schedule/delete_training/",
                    data: {
                      id : x
                    },
                    success: function(data) { 
                               $('#view_trainings').modal('hide');
                               $('#calendar_page').html(data);
                    }

                });
  }

function myFunction () {
		//$('#col').val(x);
		$('#tcourse').modal("show");
		$.ajax({
            url: '<?=base_url()?>schedule/add_event/',
            type:'POST',
            //data: {tc : x},
            success: function(data){
              $('#view_trainings').modal('hide');
            	$('#tcourse .modal-body').html(data);
            }
        });
	}

function myFunction2 (x) {
    $('#col').val(x);
    $('#ecourse').modal("show");
    $.ajax({
            url: '<?=base_url()?>schedule/edit_event/',
            type:'POST',
            data: {tc : x},
            success: function(data){
              $('#view_trainings').modal('hide');
              $('#ecourse .modal-body').html(data);
            }
        });
  }

function myFunction3 (x) {
    $('#view_trainings').modal("show");
    $.ajax({
            url: '<?=base_url()?>schedule/view_trainings/',
            type:'POST',
            data: {tc : x},
            success: function(data){
              $('#view_trainings .modal-body').html(data);
            }
        });
  }

        </script>




</head>
<body>

    <div id="view_trainings" class="modal fade" role="dialog">
            <div class="modal-dialog" style ="height: 100%;" role="document">
                <div class="modal-content" style ="height: 100%;">
                  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">List of Trainings</h3>
              </div>
                    <div class="modal-body">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


		<div id="tcourse" class="modal fade" role="dialog">
            <div class="modal-dialog" style ="height: 100%;" role="document">
                <div class="modal-content" style ="height: 100%;">
                	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h3 id="myModalLabel">Add Training</h3>
			      	</div>
                    <div class="modal-body">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    <div id="ecourse" class="modal fade" role="dialog">
            <div class="modal-dialog" style ="height: 100%;" role="document">
                <div class="modal-content" style ="height: 100%;">
                  <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3>Edit Training</h3>
              </div>
                    <div class="modal-body">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<div id ="calendar_page">

	<?php echo $calendar;?>

</div>

</body>
</html>