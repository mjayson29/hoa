<script type="text/javascript">
	function myFunction (x) {
		$('#col').val(x);
		$('#tcourse').modal("show");
		$.ajax({
            url: '<?=base_url()?>courses/fetch/',
            type:'POST',
            data: {tc : x},
            success: function(data){
            	$('#tcourse .modal-body').html(data);
            }
        });
	}

	function enlist() {
		var el = $('#sel_course option:selected');
		var col = $('#col').val();
		var str = el.text();
		var val = el.val();

		$.ajax({
            url: '<?=base_url()?>courses/addCourse/',
            type:'POST',
            data: {
            	col : col,
            	val : val
            },
            success: function(data){
				var arg = $("<tr class='train'><td><div>"+str+"<span>&#215;</span></div></td></tr>");
				if($('#enlistedCourse tbody').lenght == 1){
					$('#enlistedCourse > tbody:last').append("<tr id='cr"+col+"' class='train'><td><div>"+str+"<span>&#215;</span></div></td></tr>");
				}else{
					$('#enlistedCourse').append("<tr id='cr"+val+"' class='train'><td><div>"+str+"<span onclick='rem("+val+")'>&#215;</span></div></td></tr>");
				}
				var q = $("#c"+col+" > p").size();
				if(q <= 2){
					if(q == 3){
						$('#c'+col).append("<p title='"+str+"' style='display:inline'>"+str+"</p>");
					}else{
						$('#c'+col).append("<p title='"+str+"'>"+str+"</p>");
					}
				}else {
					$('#c'+col).append("<b class='caret'></b>");
				}
            }
        });
	}

	function rem(val) {
		var col = $('#col').val();
		var cid = val;
		var txt = $('#cr'+cid).text().slice(0,-1);

		$.ajax({
            url: '<?=base_url()?>courses/remCourse/',
            type:'POST',
            data: {
            	col : col,
            	cid : cid
            },
            success: function(data){
            	$('.opt').html(data);
            	$('#cr'+cid).remove();
            	$('#c'+col+' p:contains('+txt+')').remove();
            }
        });


	}
</script>

<div class="row">	
	<div class="col-md-12">
	<!-- <div class="panel panel-default"> -->
	
		<div id="tcourse" class="modal fade" role="dialog">
			<input type="hidden" id="col" value="">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
		<div class="table-responsive">
			<table class="table">
				<tr class="t_header">
					<th>Position Level</th>
					<th>General Education</th>
					<th>Self Development</th>
					<th>Communication</th>
					<th>Culture Build-Up</th>
					<th>Service Leadership</th>
					<th>Management</th>
				</tr>			
			<?php
				if($tc){
					foreach ($tc as $tc) {
						echo "<tr class='t_data'>";
						$select = "";
						$table = "ZVL_training_det";
						for($i=1; $i <= 7; $i++){
							$table = "ZVL_training_det";
							$where = "td_tc_id =".$tc['tc_col'.$i]." and td_active = 1";
							$td = $this->Main->select_data_where($select,$table,$where);
							echo "<td id='c".$tc['tc_col'.$i]."' ondblclick='myFunction(".$tc['tc_col'.$i].")'>";
							$a = 1;
							foreach ($td as $td) {
								$table = $i == 1? "ZVL_positions" : "ZVL_courses";
								$where = $i == 1? "p_id = ".$td['td_course'] : "c_id = ".$td['td_course'];
								$data = $this->Main->select_data_where($select,$table,$where);
								foreach ($data as $info) {
									if ($a <= 3) {
										if($a == 3) {$ab = "style='display:inline'";}else{$ab ="";}
										echo $i == 1? "<p title='".$info['p_desc']."' ".$ab.">".$info['p_desc']." </p>" : "<p title='".$info['c_name']."' ".$ab.">".$info['c_desc']." </p>";
									}
									$a++;
								}
							}
							if($a>4){ echo "<b class='caret'></b>";}
							echo "</td>";
						}
						echo "</tr>";
					}
				}
			?>
			</table>
		</div>
	</div>
</div>
