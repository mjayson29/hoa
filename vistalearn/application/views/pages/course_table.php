<script type="text/javascript">
	function paginate(val){
  	$('#course_table').hide();
  	$('#loading').show();
    $.ajax({
    	type:"get",
    	url: "<?=base_url()?>settings/course_table/"+val,
      success: function(data){
        $('#course_table').html(data);
        $('#course_table').fadeIn(500);
        $('#loading').hide();
      }
    });
  }
</script>

<div class="panel panel-primary">
	<div class="table-responsive trans" style="border-radius:inherit">
		<table class="table" style="border-radius:inherit">
			<tr style="background-color:#337AB7;color:#fff">
				<th style="width: 20%;text-align:;border-color:#337AB7">Course Name</th>
				<th style="width: 20%;text-align:;border-color:#337AB7">Description</th>
				<th style="width: 15%;text-align:;border-color:#337AB7">Date Added</th>
				<th style="width: 15%;text-align:;border-color:#337AB7">Added By</th>
				<th style="width: 10%;text-align:;border-color:#337AB7">Status</th>
			</tr>
	<?php 
		foreach ($new as $a) {
			switch ($a['c_active']) {
				case 0:
					$stat = "Inactive";
					break;
				case 1:
					$stat = "Active";
					break;
				default:
					break;
			}
			$s = "";
			$t = "ZVL_users";
			$w = "vl_u_id = ".$a['c_createdby'];
			$q = $this->Main->select_data_where($s,$t,$w);
			foreach ($q as $b) {
				$u = $b['vl_u_uname'];
			}

			$d = strtotime($a['c_createdate']);
			echo "<tr class='data_click'>";
			echo "	<td onclick='' style='width: 20%;'>".$a['c_name']."</td>";
			echo "	<td onclick='' style='width: 20%;'>".$a['c_desc']."</td>";
			echo "	<td onclick='' style='width: 15%;'>".date('F d, Y', $d)."</td>";
			echo "	<td onclick='' style='width: 15%;'>".$u."</td>";
			echo "	<td onclick='' style='width: 10%;'>".$stat."</td>";
			echo "</tr>";
		}
	?>
		</table>
	</div>
</div>
<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>