<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Vista Learn</title>

        <link href="<?=base_url('assets/styles/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url('assets/styles/css/bootstrap-theme.css')?>" rel="stylesheet" type="text/css">
        <!-- <link href="<?=base_url('assets/styles/grid.css')?>" rel="stylesheet" type="text/css"> -->
        <link href="<?=base_url('assets/styles/style.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url('assets/styles/icomoon/style.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url('assets/styles/custom.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url('assets/plugins/icomoon/style.css')?>" rel="stylesheet" type="text/css">
        <link href="<?=base_url('assets/plugins/custom-f6/css/foundation.min.css')?>" rel="stylesheet" type="text/css">
        
        <script src="<?=base_url('assets/plugins/jquery/jquery.min.js')?>"></script>
        <script src="<?=base_url('assets/styles/js/bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/plugins/custom-f6/js/foundation.min.js')?>"></script>
        <?php header('Content-Type: text/html; charset=UTF-8'); ?>

        <script>

            function logout(){
                var prompt;
                var logout = confirm('Logout?');
                if(logout == true){
                    window.location.href="<?=base_url('hr/logout')?>";
                }
            }

        </script> 

    </head>

    <body>
        <div class="wrapper">
            <div class="<?=$bg?>"></div>
            <nav class="navbar navbar-default navbar-static-top" style ="background: rgb(255, 255, 255);box-shadow: 1px 0.1px 10px black">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <header style="padding:0"><a href ="<?=base_url('hr')?>" class="logo"><img src="<?=base_url('assets/img/logo/vland.jpg')?>"></a></header>
                    </div>
                <?php if($u_role == 1){ ?>
                    <ul class="nav navbar-nav">
                        <li class="<?php if($active == 'home'){ echo 'active'; }?>"><a href="<?=base_url()?>home">HOME</a></li>
                        <li class="<?php if($active == 'courses'){ echo 'active'; }?>"><a href="<?=base_url()?>courses">TRAINING COURSES</a></li>
                        <li class="<?php if($active == 'schedule'){ echo 'active'; }?>"><a href="<?=base_url()?>schedule">TRAINING SCHEDULE</a></li>
                        <li class="dropdown <?php if($active == 'materials'){ echo 'active'; }?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">LEARNING MATERIALS</a>
                            <ul class="dropdown-menu pull-right">
                                  <li style="text-align:center"><a href="<?=base_url()?>materials/technical">TECHNICAL SKILLS</a></li>
                                  <li style="text-align:center"><a href="<?=base_url()?>materials/soft">SOFT SKILLS</a></li>
                            </ul>
                        </li>
                        <li class="<?php if($active == 'enrollees'){ echo 'active'; }?>"><a href="<?=base_url()?>enrollees">ENROLLEES</a></li>
                        <li class="<?php if($active == 'users'){ echo 'active'; }?>"><a href="<?=base_url()?>users">USERS</a></li>
                    </ul>
                <?php } 
                    else{
                ?>
                    <ul class="nav navbar-nav">
                          <li class="<?php if($active == 'home'){ echo 'active'; }?>"><a href="#">HOME</a></li>
                          <li class="<?php if($active == 'training_sched'){ echo 'active'; }?>"><a href="#">TRAINING SCHEDULE</a></li>
                          <li class="<?php if($active == 'learning_materials'){ echo 'active'; }?>"><a href="#">LEARNING MATERIALS</a></li>
                          <li class="<?php if($active == 'learning_materials'){ echo 'active'; }?>"><a href="#">ENROLLED COURSES</a></li>
                    </ul>
                <?php } ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $u_fname;?><b class="caret"></b></a>
                            <ul class="dropdown-menu pull-right">
                                <li style="text-align:center"><a href="<?=base_url()?>settings">SETTINGS</a></li>
                                <li style="text-align:center"><a href="<?=base_url('home/logout')?>">LOGOUT</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="container-fluid">
                
            





