USE [homeownersdb]
GO

/****** Object:  Table [dbo].[ZHOA_logs]    Script Date: 5/21/2015 2:10:15 PM ******/
DROP TABLE [dbo].[ZHOA_logs]
GO

/****** Object:  Table [dbo].[ZHOA_logs]    Script Date: 5/21/2015 2:10:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZHOA_logs](
	[tl_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tl_date] [datetime] NULL,
	[tl_username] [char](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[tl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

