USE [homeownersdb]
GO

/****** Object:  Table [dbo].[ZHOA_concerns]    Script Date: 5/21/2015 2:09:55 PM ******/
DROP TABLE [dbo].[ZHOA_concerns]
GO

/****** Object:  Table [dbo].[ZHOA_concerns]    Script Date: 5/21/2015 2:09:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZHOA_concerns](
	[c_id] [bigint] IDENTITY(1,1) NOT NULL,
	[c_rtype] [int] NULL,
	[c_stype] [int] NULL,
	[c_message] [nvarchar](max) NULL,
	[c_is_active] [tinyint] NULL,
	[c_date_created] [datetime] NULL,
	[c_is_deleted] [tinyint] NULL,
	[c_reply_by] [int] NULL,
	[c_ticket_id] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


