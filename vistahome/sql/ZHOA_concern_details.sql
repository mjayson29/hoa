USE [homeownersdb]
GO

/****** Object:  Table [dbo].[ZHOA_concern_details]    Script Date: 5/21/2015 2:09:43 PM ******/
DROP TABLE [dbo].[ZHOA_concern_details]
GO

/****** Object:  Table [dbo].[ZHOA_concern_details]    Script Date: 5/21/2015 2:09:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZHOA_concern_details](
	[cd_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cd_desc] [nvarchar](360) NULL,
	[cd_concern_id] [bigint] NULL,
	[cd_date_created] [datetime] NULL,
	[cd_is_deleted] [tinyint] NULL,
 CONSTRAINT [PK_ZHOA_concern_details] PRIMARY KEY CLUSTERED 
(
	[cd_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

