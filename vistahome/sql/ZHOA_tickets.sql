USE [homeownersdb]
GO

/****** Object:  Table [dbo].[ZHOA_tickets]    Script Date: 5/21/2015 2:10:24 PM ******/
DROP TABLE [dbo].[ZHOA_tickets]
GO

/****** Object:  Table [dbo].[ZHOA_tickets]    Script Date: 5/21/2015 2:10:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZHOA_tickets](
	[t_id] [bigint] IDENTITY(1,1) NOT NULL,
	[t_blklot] [nchar](10) NULL,
	[t_date_created] [datetime] NULL,
	[t_status] [tinyint] NULL,
	[t_owner_id] [bigint] NULL,
	[t_is_deleted] [tinyint] NULL
) ON [PRIMARY]

GO


