<div class="carousel-inner signup-background">

    <div class="item active">

        <div class="fill" style="background-image:url('<?=base_url()?>assets/img/banner/crownasia.jpg');"></div>
    
    </div>
           
</div>

<article class="">

	<div class="container">

		<div class="row signup-form-header" id="signup">

		    <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
		    	<br>
				<div style="text-align: center;">
					<img src="<?=base_url()?>assets/img/logos/Logo02-02.png" style="width: 80px; margin-top: 20px;">
					&nbsp;
					<img src="<?=base_url()?>assets/img/logos/vistahome.png" style="width: 150px;">
		    	</div>
		    	<br>
			</div>

		</div>

		<div class="row signup-form1" style="min-height: 400px; margin-bottom: 0px; border-radius: 0px;">
		
			<div class="col-md-12 confirmation"> <br> <br>
				<h3 class="mod_title">Registration Confirmation</h3> <br>
				<h4>Thank you for registration! <br><br>
				Your registration is on process of validation, we will send a confirmation message to <span style='color:#ffff66'><?php if(!isset($email)){ echo "NO EMAIL FETCHED"; } else { echo $email; } ?></span>. 
				Please access your email to confirm your account activation. </h4>
				<button class="btn btn-primary btn-sm" onclick="window.location.href='<?=base_url()?>'">Close</button>
				<br> <br> <br> <br>
			</div>

		</div>

		<div class="row signup-form-header" id="signup" style="margin-top: 0px; margin-bottom: 50px; border-radius: 0px; min-height: 50px;">
		</div>

		<div class="col-md-2"> </div>

	</div>

</article>