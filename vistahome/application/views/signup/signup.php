
<div class="carousel-inner signup-background">

    <div class="item active">

        <div class="fill" style="background-image:url('<?=base_url()?>assets/img/banner/crownasia.jpg');"></div>
    
    </div>
           
</div>

<script type="text/javascript">

    $(document).ready(function(){
		$("#confirmation").css('display','none');
    	$('#loading').css('display','none');
    	$('#password_validation').css('display','none');
    	$('#load_err').css('display','none');
    	$('#email_err').css('display','none');
    	$('#bdate_validation').css('display','none');
    	$('#zip_validation').css('display','none');
    });

</script>

<script type="text/javascript">
  function get_sec1(val){
    
    var sec1 = $('#sec1').val();
  	var sec2 = $('#sec2').val();
  	if(sec1 == sec2 || sec2 == ""){
	  	$.ajax({
	    type:'POST',
	    url: '<?=base_url()?>signup/check_sec1/'+val,
	    success: function(data){
	      document.getElementById("sec2").innerHTML = data;  
	    }
	  });
	}
  }

  function get_sec2(val){

  	var sec1 = $('#sec1').val();
  	var sec2 = $('#sec2').val();
  	if(sec1 == sec2 || sec1 == ""){
	  $.ajax({
	    type:'POST',
	    url: '<?=base_url()?>signup/check_sec2/'+val,
	    success: function(data){
	      document.getElementById("sec1").innerHTML = data;  
	    }
	  });
	}
  }

</script>

<div id="mod-help" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title">My Customer Number</h4>
                <h6 class="modal-title">Where is it?</h6>
            </div>
            <div class="modal-body">
            	<div class="row">
		        	<div class="col-md-12">
		        		<img style="width:100%" src="<?=base_url()?>assets/img/tools/sampleOR.jpg">
		        	</div>
		        </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="page_loader" class="page_loader">
    <img src="<?=base_url()?>assets/img/tools/loader.gif"> 
</div> 

<article class="">

	<div class="container">

		<div class="row signup-form-header" id="signup">

		    <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
		    	<br>
				<div style="text-align: center;">
					<img src="<?=base_url()?>assets/img/logos/Logo02-02.png" style="width: 80px; margin-top: 20px;">
					&nbsp;
					<img src="<?=base_url()?>assets/img/logos/vistahome.png" style="width: 150px;">
		    	</div>
		    	<br>
			</div>


		</div>

		<div class="row signup-form">

		    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<form role="form" name="validation" method="post" action="<?=base_url()?>signup/confirmation/" id="validation">
					<h2 class="mod_title">Sign Up</h2>
					<small class="mod_title">Yellow fields are required</small> <br />

					<!-- <hr class="colorgraph"> -->
					<small class="mod_title">Indicate the details of your property in Vistaland</small>
					<div class="row">

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="cust_no" id="cust_no" class="form-control input-sm" placeholder="Customer No." required>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-4 col-md-4" style="padding: 0">
							<span id='cnum_help' style="cursor:pointer;color:white;font-size:20px" data-target="#mod-help" data-toggle="modal" title="Whats this?" class="glyphicon glyphicon-question-sign"></span>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<select class="form-control input-sm" name="company" id="company" onchange="get_company(this.value)" required>
									<option value="">-- SELECT PROJECT AREA --</option>
									<?php foreach($company as $row){ ?>
									<!-- <input type="text" name="" id="" class="form-control input-sm" placeholder="Company"> -->
									<option value="<?=$row['PROJ_ID']?>"><?=$row['PROJ_AREA']?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<select class="form-control input-sm" name="unit" id="unit" required onchange="get_proj(this.value)">
									<option value="">-- SELECT UNIT TYPE--</option>
									<?php foreach($unit as $row){ ?>
									<!-- <input type="text" name="" id="" class="form-control input-sm" placeholder="Company"> -->
									<option value="<?=str_pad($row['u_id'],4,0,STR_PAD_LEFT)?>"><?=$row['u_desc']?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div style="display: none" id="dvloader"><img src="../assets/img/tools/loader2.gif" width="100px"/></div>
							<div class="form-group" id="load_project" >
								<select name="project" id="project" class="form-control input-sm" required onchange="get_text(this.value)">
									<option value=''>-- SELECT PROJECT --</option>
								</select>
								<input type="hidden" name="projtext" id="projtext" value="">
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<input type="text" name="blklot" id="blklot" class="form-control input-sm" placeholder="Blk/Flr No. (000-0000)" required onblur="validate_blklot()">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-6" id="blklot_validation" style="display:none">
							<div class="form-group">
		                        <div class="alert-danger btn-sm" style="padding:5px;"> 
		                        	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		                        	Invalid Blk/Flr No.
		                        	<input type="hidden" id="val_blklot" value="">
		                        </div> 
							</div>
						</div>

					</div>
					<!-- <div class="row">
						<div class="col-xs-4 col-sm-3 col-md-3">
							<span class="button-checkbox">
								<button type="button" class="btn" data-color="info" tabindex="7">I Agree</button>
		                        <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
							</span>
						</div>
						<div class="col-xs-8 col-sm-9 col-md-9">
							 By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="#" data-toggle="modal" data-target="#t_and_c_m">Terms and Conditions</a> set out by this site, including our Cookie Use.
						</div>
					</div> -->
					
					<!-- <hr class="colorgraph"> -->
					<small class="mod_title">Personal Information</small>

					<div class="row">

						<div class="col-xs-12 col-sm-4 col-md-4">
							<div class="form-group">
		                        <input type="text" name="lname" id="lname" class="form-control input-sm" placeholder="Last Name" required> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-4 col-md-4">
							<div class="form-group">
		                        <input type="text" name="fname" id="fname" class="form-control input-sm" placeholder="First Name" required> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-4 col-md-4">
							<div class="form-group">
		                        <input type="text" name="mname" id="mname" class="form-control input-sm" placeholder="Middle Name" style="background: #fff;"> 
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-4 col-md-4">
							<small class="mod_title">Birthdate</small>
							<div class="form-group">
		                        <input type="date" name="bdate" id="bdate" class="form-control input-sm" placeholder="MM/DD/YYYY" required onblur="validate_bdate()"> 
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4" id="bdate_validation">
						<br/>
							<div class="form-group">
		                        <div class="alert-danger btn-sm" style="padding:5px;"> 
		                        	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		                        	Incorrect date format
		                        	<input type="hidden" id="val_bdate" value="">
		                        </div> 
							</div>
						</div>

						<!-- <div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
		                        <input type="text" name="add1" id="add1" class="form-control input-sm" placeholder="Address 1" required> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
		                        <input type="text" name="add2" id="add2" class="form-control input-sm" placeholder="Address 2" required> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="province" id="province" class="form-control input-sm" placeholder="Province" required> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="zip" id="zip" class="form-control input-sm" placeholder="Zip Code" required maxlength="4" onblur="validate_zip()"> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-6" id="zip_validation">
							<div class="form-group">
		                        <div class="alert-danger btn-sm" style="padding:5px;"> 
		                        	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		                        	Incorrect Zip Code Format
		                        	<input type="hidden" id="val_zip" value="">
		                        </div> 
							</div>
						</div> -->

					</div>

					<!-- <hr class="colorgraph"> -->
					<!-- <small class="mod_title">Account Information</small>
					
					<div class="row">

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="uname" id="uname_validation" class="form-control input-sm" placeholder="Username" onblur ="check_username(this.value)" required pattern=".{8,}"   required title="8 characters minimum" max-lenght="25"> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6" id="loader">
							<div id="loading">
								<input type='hidden' id='validate_uname' value=''>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="password" name="pword" id="pword" class="form-control input-sm" placeholder="Password" onblur="validate_password()" required max-lenght="25"> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="password" name="cpword" id="cpword" class="form-control input-sm" placeholder="Confirm Password" onblur="validate_password()" required max-lenght="25"> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6" id="password_validation">
							<div class="form-group">
		                        <div class="alert alert-danger btn-sm" style="padding:5px;"> 
		                        	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		                        	Passwords do not match
		                        	<input type="hidden" id="validate_pword" value="">
		                        </div> 
							</div>
						</div>

					</div> -->

					<!-- <hr class="colorgraph"> -->
					<small class="mod_title">Contact Information</small>

					<div class="row">

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="contact" id="contact" class="form-control input-sm" placeholder="Contact Number" required> 
							</div>
						</div>

						<!-- <div class="col-xs-12 col-sm-12 col-md-12"> </div> -->

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="email" name="email" id="email" class="form-control input-sm" placeholder="email@email.com" required> <!-- onblur="email_err(this.value)" > -->
							</div>
						</div>	

					</div>

					<div class="row">

						<div class="col-xs-12 col-sm-6 col-md-6 col-md-offset-6">
							<div class="form-group" id="email_err">
		                        <div class="alert alert-danger btn-sm" style="padding:5px;"> 
		                        	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		                        	Email is not valid. Please try again.
		                        	<input type="hidden" id="validate_email" value="">
		                        </div> 
							</div>
						</div>

					</div>

					<!-- <hr class="colorgraph"> -->
					<!-- <small class="mod_title">Security Question</small>

					<div class="row">
					
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<select class="form-control input-sm" name="sec1" id="sec1" required onchange="get_sec1(this.value)">
		                        	
		                        <option value="">-- Select Security Question 1--</option>
								<?php foreach($secquestion as $row) { ?>
		                        	<option value="<?=$row['sq_id']?>"><?=$row['sq_desc']?></option>
		                        <?php } ?>

		                        </select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="ans1" id="ans1" class="form-control input-sm" placeholder="Security Answer 1" required> 
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12"> </div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <select class="form-control input-sm" name="sec2" id="sec2" required onchange="get_sec2(this.value)">
		                        	
		                        	<option value="">-- Select Security Question 2--</option>
									<?php foreach($secquestion as $row) { ?>
		                        	<option value="<?=$row['sq_id']?>"><?=$row['sq_desc']?></option>
		                       		<?php } ?>

		                        </select>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
		                        <input type="text" name="ans2" id="ans2" class="form-control input-sm" placeholder="Security Answer 2" required> 
							</div>
						</div>

					</div>
 -->
					<!-- <hr class="colorgraph"> -->
					<small class="mod_title">Concern(Optional)</small>

					<div class="row">
						
						<script language="javascript" type="text/javascript">
							function limitText(limitField, limitCount, limitNum) {
								if (limitField.value.length > limitNum) {
									limitField.value = limitField.value.substring(0, limitNum);
								} else {
									limitCount.value = limitNum - limitField.value.length;
								}
							}
						</script>

						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="form-group">
								<textarea name="concern" id="concern" onkeydown="limitText(this.form.concern,this.form.countdown,360);" onkeyup="limitText(this.form.concern,this.form.countdown,360);" style="height: 100px; resize: vertical;" class="form-control input-sm" maxlength="360" placeholder="Write your concern here!"></textarea>
								<br>
								<font size="1" style="color: #fff;">(Maximum characters: 360)<br>
								You have <input readonly="" type="text" name="countdown" size="3" value="360" style="width:40px; color: #000;"> characters left.</font>
							</div>
						</div>
					</div>

					<div class="row">
				        <?php

				            // function generateRandomString($length = 4, $letters = '1234567890QWERTYUIOPASDFGHJKLZXCVBNMmnbvcxzlkasdjfhgpqowieurytthequickbrownfoxjumpsoverthelazydog'){
				                    
				            //     $s = '';

				            //     $lettersLength = strlen($letters)-1;

				            //     for($i = 0 ; $i < $length ; $i++){
				                    
				            //         $s .= $letters[rand(0,$lettersLength)];
				            //     }

				            //         return $s;
				            // } 

				            // $_SESSION['captcha'] = generateRandomString()."-".generateRandomString();

				            // Set the content-type
							// header('Content-Type: image/png');

							// Create the image
							// $im = imagecreatetruecolor(400, 30);

							// Create some colors
							// $white = imagecolorallocate($im, 255, 255, 255);
							// $grey = imagecolorallocate($im, 128, 128, 128);
							// $black = imagecolorallocate($im, 0, 0, 0);
							// imagefilledrectangle($im, 0, 0, 399, 29, $white);

							// The text to draw
							// $text = $_SESSION['captcha'];
							// Replace path by your own font path
							// $font = base_url().'assets/styles/fonts/viga-regular-webfont.ttf';

							// Add some shadow to the text
							// imagettftext($im, 20, 0, 11, 21, $grey, $font, $text);

							// Add the text
							// imagettftext($im, 20, 0, 10, 20, $black, $font, $text);

							// Using imagepng() results in clearer text compared with imagejpeg()
							// imagepng($im);
							// imagedestroy($im);

							// Create a 100*30 image


				        ?>

						<!-- <div class="col-md-12">

							<div class="mod_captcha"><?=$_SESSION['captcha'];?></div>
		    				<br>

						</div> -->

					</div>
					
					<div class="row">

						   	<div class="col-md-12">
						   		<div id="rcapt" class="g-recaptcha" data-sitekey="6LfLyw8TAAAAANsTHTLCRxKNMDb_vm0gruw54qiQ"></div><br />
						   	</div>


						<!-- <div class="col-md-12">
							<div class="form-group">
		                		<input type="text" name="captcha" id="captcha" class="form-control input-sm" placeholder="Input Captcha" required onblur="validate_captcha()"> 
							</div>
						</div> -->
 
						<div class="col-md-12" id="load_err">
							<div class="form-group">
		                        <div class="alert alert-danger btn-sm" style="padding:5px;"> 
		                        	<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		                        	Please check the Captcha. Please try again.
		                        </div> 
							</div>
						</div>

					</div>

					<!-- <hr class="colorgraph"> -->

					<div class="row"> 
						<div class="col-xs-12 col-md-6">
							<input type="submit" value="Register" class="btn btn-primary btn-block btn-sm" name="submit" onclick="return signup(this)" id="submit">
						</div>
						<div class="col-xs-12 col-md-6"style="text-align:center">
							<a href="<?=base_url()?>" class="btn btn-success btn-block btn-sm" >I already have an account</a>
						</div>
					</div>
				</form>
				<br>
			</div>
		</div>
		<!-- <div class="row signup-form" style="min-height: 400px;" id="confirmation">

			<br>
			<br>
		
			<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 confirmation">
			
				<h3 class="mod_title">Registration Confirmation</h3>
				<br>
				<h4>Thank you for registration!
				<br>
				You're registration is on process of validation, we will send a confirmation message to <span id="email_loader"></span>. 
				Please access your email to confirm your account activation. </h4>
				<button class="btn btn-primary btn-sm" onclick="window.location.href='<?=base_url()?>'">Close</button>
				<br>
				<br>
				<br>
				<br>
			</div>

		</div>
	</div> -->
		
</article>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	<script type="text/javascript">

		function validate_blklot(){
			var blk = $('#blklot').val();
			var pat = /^([a-zA-Z0-9]){3}\-([a-zA-Z0-9]){4}$/;
			if(blk != ""){
				if(blk.match(pat)){
					// console.log("match");
					$('#blklot_validation').slideUp('fast');
					$('#val_blklot').val('valid');
				}else{
					// console.log("not match");
					$('#blklot_validation').slideDown('fast');
					$('#val_blklot').val('invalid');
				}
			}
		}

		function validate_zip(){
			var zip = $('#zip').val();
			if(zip != ""){
				if(isNaN(zip)){
					$('#zip_validation').slideDown('fast');
					$('#zip').effect('shake');
					$('#val_zip').val("invalid");	
				} else {
					$('#zip_validation').slideUp('fast');	
					$('#val_zip').val("valid");
				}
			}
		}
	</script>

	<script type="text/javascript">
	
		function validate_bdate(){
			var strdate = $('#bdate').val();
			if(strdate != ""){
				var strvalidate = /^([1-9]|0[1-9]|1[0-2])([\/])([1-9]|0[1-9]|1\d|2\d|3[01])([\/])(19|20)\d{2}$/;
				var datepick = /^(19|20)\d{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])$/;

				if(strdate.match(strvalidate)){
					$('#bdate_validation').slideUp('fast');	
					$('#val_bdate').val("valid");
					// var dt = convertDate(strdate);
					// $('#bdate').val(dt);
					// console.log("match " + strdate + " " + dt);
				} else{
					if(strdate.match(datepick)){
						$('#bdate_validation').slideUp('fast');	
						$('#val_bdate').val("valid");
						// console.log("date pick match " + strdate);
					} else{
						$('#bdate_validation').slideDown('fast');	
						$('#val_bdate').val("invalid");
						// console.log("not match " + strdate);
					}
				}
			}			
		}

		function convertDate(usDate) {
	  		var dateParts = usDate.split(/[\/|\-]/);
	  		return dateParts[2] + "-" + dateParts[0] + "-" + dateParts[1];
		}
	</script>

    <script type="text/javascript">

        function check_username(val){ 

			var str = val;
			if(str != ""){
				var result = str.toString();
	           
	             $.ajax({
	                type:"get",
	                url: "<?=base_url()?>signup/check_username/"+result,
	                success: function(data){
	                    $('#loader').html(data);
	                }
	            });
        	}
        }

    </script>

    <script type="text/javascript">
    
		function validate_password() {

			var pword=document.forms["validation"]["pword"].value;
			var cpword=document.forms["validation"]["cpword"].value;

			if(pword != "" && cpword != ""){
				if(pword == cpword){
				 	//alert('Password match.');
					$('#password_validation').slideUp('fast');	
					$('#validate_pword').val('valid');
				}else {
				   	
				 	// alert('!= Password match.');
					$('#password_validation').slideDown('fast');	
					$('#validate_pword').val('invalid');
				}
			}
		}

		function validate_captcha(){

			var session_captcha=document.forms["validation"]["captcha_valid"].value;
			var captcha=document.forms["validation"]["captcha"].value;

			if(session_captcha == captcha){
			 	//alert('Password match.');
				$('#load_err').slideUp('slow');	
			} else {
			   	
				// alert('!= Password match.');
				$('#load_err').slideDown('slow');	

			}

			var v = grecaptcha.getResponse();
		    if(v.length == 0){
		        $('#captcha_valid').val("invalid");
		        return false;
		    }
		    if(v.length != 0){
		        document.getElementById('rcapt').innerHTML="Captcha completed";
		         $('#captcha_valid').val("valid");
		        return true; 
		    }
		}
			 
	</script>

<script type="text/javascript">

	function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

	function email_err(email){

		var get_email = $('#email').val();

		if(email == ""){
			$('#email_err').slideUp();
			return false;
		}

		if(!validateEmail(email)) {
			$('#email_err').slideUp();
			return false;
		}

		$('#email_err').slideDown();
	    $.ajax({
	        type:"POST",
	        url: "<?=base_url()?>signup/check_email/",
	        data: {
	            email: email, 
	        },
	        success: function(data){
	            $('#email_err').html(data);
	        }
	    });
	}

	function get_text(val) {
		var projtext = $('#project option[value="' + val + '"]').text();
		$('#projtext').val(projtext);
	}

	function signup(form){

		var v = grecaptcha.getResponse();
	    if(v.length == 0) {
	        $('#load_err').slideDown('slow');	
	        return false;
	    } else{
	    	$('#load_err').slideUp('slow');	
	        return true;
	    }

		$('#page_loader').show;


		var cust_no = $('#cust_no').val();
		var company = $('#company').val();
		var unit = $('#unit').val();
		var projcode;
		var project;
		$('#project option:selected').each(function(){
            projcode = $(this).val();
            project = $(this).text();
        });
		var blklot = $('#blklot').val();
		var lname = $('#lname').val();
		var mname = $('#mname').val();
		var fname = $('#fname').val();
		var bdate = $('#bdate').val();
		var add1 = $('#add1').val();
		var add2 = $('#add2').val();
		var province = $('#province').val();
		var zip = $('#zip').val();
		var uname = $('#uname_validation').val();
		var cpword = $('#cpword').val();
		var pword = $('#pword').val();
		var contact = $('#contact').val();
		var email = $('#email').val();
		var sec1 = $('#sec1').val();
		var ans1 = $('#ans1').val();
		var sec2 = $('#sec2').val();
		var ans2 = $('#ans2').val();
		var concern = $('#concern').val();
		var captcha = $('#captcha').val();
		var submit = $('#submit').val();
		var validate_uname = $('#validate_uname').val();
		var validate_email = $('#validate_email').val();
		var validate_zip = $('#val_zip').val();
		var validate_bday =  $('#val_bdate').val();
		// var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66}	)\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

		if(cust_no == "" || company == "" || unit == "" || project == "" || blklot == "" || lname == "" || fname == "" || bdate == "" || uname == "" || contact == "" || email == ""){
			alert("All yellow fields are required.");
			return false;
		} else {
		
			if(uname.length <8){
				alert("Username must be 8 characters and up.");
			    return false;
			}

			if(validate_uname == "invalid"){
				alert("Username is invalid. Please try again.");
			    return false;
			}

			// if(pword != cpword){
			// 	alert("Password validation didn't match. Please try again.");
			// 	return false;
			// }

			// if(pword.length <8 || cpword.length <8){
			// 	alert("Password must be 8 characters and up.");
			// 	return false;
			// }

			/*if(!validateEmail(email)) {
			    alert("Invalid Email!");
			    return false;
			} */

			if(validate_email== "invalid"){
				alert("Email is not valid. Please try again.");
			    return false;
			}

			if(validate_bday == "invalid"){
				alert("Invalid date format in birthday field. Please try again.");
			    return false;
			}	

			// if(validate_zip == "invalid"){
			// 	alert("Invalid zip code format. Please try again.");
			//     return false;
			// }			
		}
		if(!confirm("Are you sure want to submit this form?")){
			return false;
		}
	}

</script>
