<script type="text/javascript">
	$(function(){
		$('#req_code').css('display','none');
	});
	function toggle_form(){
		$('#req_code').slideToggle();
		$('#ver_email').slideToggle();
	}
	function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}
	function generate(){

		var email = $('#email').val();
		var validate_email = $('#validate_email').val();
		if(email == ""){
			alert('Email field is required!');
			return false;
		} else {
			if(validateEmail(email)){
				if(validate_email == 'invalid'){ 
					alert('Email is not valid.');
				} else {
					$.ajax({
		                type:"POST",
		                url: "<?=base_url()?>signup/req_code/",
		                data: {
		                	email: email

		                },
		                success: function(data){
		                    alert('Verification code has been sent your email. Please verify your email within 3 days.');
		                    window.location.href="<?=base_url()?>";
		                }
		            });
		        }
			} else {
				alert('Invalid email format!');
			}
			return false;
		}
	}  
</script>	
<div class="carousel-inner signup-background">

    <div class="item active">

        <div class="fill" style="background-image:url('<?=base_url()?>assets/img/banner/crownasia.jpg');"></div>
    
    </div>
           
</div>

<article class="">

	<div class="container">

		<div class="row signup-form-header" id="signup">

		    <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
		    	<br>
				<div style="text-align: center;">
					<img src="<?=base_url()?>assets/img/logos/Logo02-02.png" style="width: 80px; margin-top: 20px;">
					&nbsp;
					<img src="<?=base_url()?>assets/img/logos/vistahome.png" style="width: 150px;">
		    	</div>
		    	<br>
			</div>

		</div>

		<div class="row signup-form">
				
			<div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4"> <br> <br> </div>  

			<div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4" id="ver_email">

		    	<form method="POST" action="<?=base_url()?>signup/verify_email">

		    		<strong class="mod_title">Email verification.</strong>
		    		<br>
		    		<small class="mod_title">Enter Verfication Code</small>
		    		<br>
		    		<input type="text" name="code" placeholder="Verification Code" class="form-control input-sm" required>		    		
		    		
		    		<small class="mod_title">Enter Email</small>
		    		<br>
		    		<input type="email" name="email" placeholder="Email" class="form-control input-sm" required>		    		
		    		
		    		<br>
		    		<input type="submit" name="verify" value="Verify" class="btn btn-primary btn-sm form-control input-sm">
		    		<input type="button" value="Cancel" class="btn btn-danger btn-sm form-control input-sm" onclick="window.location.href='<?=base_url()?>'">
		    		<input type="button" value="Request another code." class="btn btn-warning btn-sm form-control input-sm" onclick="toggle_form()">
		    		<br>

		    	</form>

		    	<br> <br> <br> <br>

			</div> 

			<div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4" id="req_code">

		    	<form method="POST" action="<?=base_url()?>signup/req_code">

		    		<strong class="mod_title">Request for new verification code.</strong>
		    		<br>
		    		<small class="mod_title">Enter Email</small>
		    		<br>
		    		<input type="email" name="email" placeholder="Email" id="email" class="form-control input-sm" required onkeyup="email_err(this.value)" oninput="email_err(this.value)">		    		
					
					<div class="form-group" id="email_err">
		            	<input type="hidden" id="validate_email" value="">
		    		</div>

		    		<br>
		    		<input type="submit" value="Generate Code" class="btn btn-primary btn-sm form-control input-sm" onclick="return generate()" id="generate()">
		    		<input type="button" value="Cancel" class="btn btn-danger btn-sm form-control input-sm" onclick="toggle_form()">
		    		<br>

		    	</form>

		    	<br> <br> <br> <br>

			</div> 

		</div>

	</div>

</article>

<script type="text/javascript">

	function email_err(email){

		var get_email = $('#email').val();

		if(email == ""){
			$('#email_err').slideUp();
		} else {
			if(validateEmail(email)) {
			    
			    $('#email_err').slideDown();
	            $.ajax({
	                type:"get",
	                url: "<?=base_url()?>signup/check_existing_email/",
	                data: {
	                	email: email, 
	                },
	                success: function(data){
	                    $('#email_err').html(data);
	                }
	            });
			
			} else {
				$('#email_err').slideDown();
	            $.ajax({
	                type:"get",
	                url: "<?=base_url()?>signup/check_existing_email/",
	                data: {
	                	email: email, 
	                },
	                success: function(data){
	                    $('#email_err').html(data);
	                }
	            });
			}
		}
	}

</script>
