Dear Mr. / Ms. <span style="font-weight: bold;"><?=$fullname?></span>
<br/><br/>
	Thank you for registering an account into our Vista HOMe - the Vista Land Homeowner's Portal. Please validate this email address by clicking on the link below.
<br/>
	Your code is <span style="font-weight: bold;"><?=$code?></span>
<br/><br/>
	Thank you,
<br/><br/>
	Vista HOMe Team
<br/><br/>
<a href="<?=base_url('signup/verify_email/')?>">Click here to verfiy your email. </a>