<script type="text/javascript">
  function paginate(val){
    
        $.ajax({
        type:"get",
        url: "<?=base_url()?>portal/announcements/"+val,
            success: function(data){
                $('#li_announcements').html(data);
            }
        });
    }

    $(document).ready(function(){
        $.ajax({
            type:"post",
            url: "<?=base_url()?>portal/announcements/",
            success: function(data){
                $('#li_announcements').html(data);
            }
        });
    })
</script>

<script type="text/javascript">
  $(document).ready(function() {
    var showChar = 500;
    var ellipsestext = "...";
    var moretext = "show more";
    var lesstext = "";
    $('.more p').each(function() {
      var content = $(this).html();

      if(content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar, content.length - showChar);

        var html = c+'<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>'+h+'</span><a href="" class="morelink">'+moretext+'</a></span>';

        $(this).html(html);
      }

    });

    $(".morelink").click(function(){
      if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });
</script>

  <div class="mod_announcements">
    <div class="mod_announcements_1">
      <div class="container">
        <h3><strong>Announcements</strong></h3>
      </div>
    </div>
  </div>

  <section class="container mod_announce">
    <div id="li_announcements" class="row">

    </div>
  </section> 
