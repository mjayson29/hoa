<script type="text/javascript">
	$(document).ready(function(){
		$('#my_tickets').hide();
		$('#loading').show();
        $.ajax({
        type:"get",
        url: "<?=base_url()?>portal/my_request_load/",
            success: function(data){
                $('#my_tickets').html(data);
                $('#my_tickets').slideDown('fast');
                $('#loading').hide();
            }
        });

    });
</script>

<script type="text/javascript">
	function paginate(val){

        $('#my_tickets').hide();
        $('#loading').show();

        $.ajax({
        type:"get",
        url: "<?=base_url()?>portal/my_request_load/"+val,
            success: function(data){
                $('#my_tickets').html(data);
                $('#my_tickets').slideDown('fast');
                $('#loading').hide();
            }
        });

    }
</script>

<script type="text/javascript">
	function enab() {
		console.log('hello');
		// var nodes = $("#yui").each(function(){
		// 	for(var i = 0; i < nodes.length; i++){
		// 		nodes[i].disabled = true;
		// });
		$("#yui :input").attr("disabled", true);
	}
</script>

<?php
	if(count($blklot) == 1){
		foreach ($blklot as $key) {
			$proj = $key['XWETEXT'];
			$model = $key['ARKTX'];
			$blk = $key['REFNO'];
			$flr = $key['FLR_AREA'];
			$lot = $key['LOT_SIZE'];
			$move1 = $key['MOVEIN_DATE'];
			$so = $key['VBELN'];
			if($move1 != 0){
				$move = date('F d, Y', strtotime($move1));
			} else {
				$move = "--";
			}
			$i = 1;
		}
	} else {
		echo "<script> enab(); </script>";
		$proj = "--";
		$model = "--";
		$blk = "--";
		$flr = "--";
		$lot = "--";
		$move = "--";
		$so = "--";
		$i = 0;
	}
?>



<form action="<?=base_url()?>portal/new_request" method="POST" enctype="multipart/form-data">
	<div class="mod_nfilter signup-form" style="text-align: left;text-shadow:none">

	  <!-- <div class="container"> -->
	  	<div class="row">
			<div class="container">
				<div class="col-md-12"><?=$notification?></div>
				<div class="col-md-4">
					<div class="col-md-12">
						<div class="col-xs-3 col-sm-2 col-md-5">
							<label>Blk/Lot</label>
						</div>

						<div class="col-xs-9 col-sm-4 col-md-7">
						<?php if($i == 1) { ?>
								<?=$blk?>
						<?php }else { ?>	
							<select id="sel_blk" class="form-control input-sm" onchange="get_blklot(this.value)" name="blklot" >
								<option value="">--Select--</option>
								<?php if($blklot){
									foreach($blklot as $bl){ ?>
										<option value="<?=$bl['REFNO']?>"><?=$bl['REFNO']?></option>
									<?php }
									} ?>
							</select>
						<?php } ?>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-4">
					<div class="col-md-12">
						<div class="col-md-8">
							<label><?=date('F d, Y');?></label>
						</div>
					</div>
				</div>

				<!-- <div class="col-xs-12 col-sm-2 col-md-1"><strong><input class="btn btn-success input-sm btn-sm form-control" type="submit" value="New"></strong></div> -->
				<!-- <div class="col-xs-12 col-sm-2 col-md-1"><strong><input class="btn btn-danger input-sm btn-sm form-control" type="button" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_request'"></strong></div> -->

				<!-- <div class="col-sm-3 col-md-1">
				    <button type="button" class="btn btn-success form-control input-sm btn-sm" onclick="add_concern()">New</button>
				</div>

				<div class="col-sm-3 col-md-1">
				    <input type="button" class="btn btn-danger form-control input-sm btn-sm" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_request'";>
				</div> -->

			</div>

		</div>

	  <!-- </div> -->
		<div class="row">
		<br/>
			<div class="container" id="bl_loader">
				<div class="col-md-4">
					<div class="col-md-12">
						<div class="col-xs-6 col-sm-2 col-md-5">
							<label> Project </label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-7"><?=$proj?></div>
					</div>
					<div class="col-md-12">
						<div class="col-xs-6 col-sm-2 col-md-5">
							<label> Model </label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-7"><?=$model?></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="col-md-12">
						<div class="col-xs-6 col-sm-2 col-md-5">
							<label> Blk/Lot </label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-7"><?=$blk?></div>
					</div>
					<div class="col-md-12">
						<div class="col-xs-6 col-sm-2 col-md-5">
							<label> Flr Area </label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-7"><?=$flr?></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="col-md-12">
						<div class="col-xs-6 col-sm-2 col-md-5">
							<label> Lot Area </label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-7"><?=$lot?></div>
					</div>
					<div class="col-md-12">
						<div class="col-xs-6 col-sm-2 col-md-5">
							<label> Move In Date </label>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-7"><?=$move?></div>
					</div>
				</div>
				<input type="hidden" name="so" value="<?=$so?>">
				<input type="hidden" name="blk" value="<?=$blk?>">
			</div>

		</div>

	</div>

	<section class="mod_nrequest signup-form" style="text-align: left;text-shadow:none;padding: 10px 0">


		<div id="yui" class="row">

			<div class="container">

				<div class="col-sm-6 col-md-3">
					<div class="col-md-12">
						<small>Request Type</small>
						<br>
						<select class="form-control input-sm" name="req_type" onchange="get_request(this.value)" id="req_type">

							<option value="">--Select--</option>

							<?php if($request_type) { ?>

								<?php foreach($request_type as $rtype) { ?>

								<option value="<?=$rtype['rt_id']?>"><?=$rtype['rt_desc']?></option>

								<?php } ?>

							<?php } ?>
					    </select>
				    </div>
				    <!-- <input type="text" class="form-control input-sm" name="req_type"> -->
				    <div id="sub_type_con" class="col-md-12" style="display:none">
						<small>Sub Type</small>
						<br>

						<div>
							<select class="form-control input-sm" name="sub_req_type" id="sub_req_type">
								<option value="">--Select--</option>
						    </select>
					    </div>
				    </div>
				</div>

				<div class="col-sm-6 col-md-6">
					<small>Message</small>
					<br>
					<textarea class="form-control input-sm" style="min-height: 80px; resize: vertical;" id="message" maxlength = "360"></textarea>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-1">
					<br>
				    <button type="button" class="btn btn-warning form-control input-sm btn-sm" onclick="attach_file()" title="Add attachment"><span class="glyphicon glyphicon-paperclip"></span></button>
				    <input type="file" style="display: none;" id="userfile" multiple>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-1">
					<br>
				    <button type="button" class="btn btn-success form-control input-sm btn-sm" onclick="add_concern()" title="Add concern"><span class="glyphicon glyphicon-plus"></span></button>
				</div>

				<div class="col-xs-4 col-sm-4 col-md-2">
					<br>
				    <input type="button" class="btn btn-danger form-control input-sm btn-sm" value="Cancel" onclick="reset_concern()">
				</div>

			</div>

		</div>

		<br>

		<div class="row">
			<div class="container">
				<div class="col-md-12">
					<input type="checkbox" name="getnotif" checked="checked" id="getnotif"> Email me notifications for this ticket.
				</div>
			</div>
		</div>

		<br>

		<div class="row" id="load_concern">
			<div class="container">
				<div class="col-md-12">
					Concerns...
				</div>
			</div>
		</div>

	</section>

</form>
<div class="row" style="margin-bottom:20px">
	<div class="col-md-8 col-md-offset-2">
		<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
	<div class="panel panel-dark" id="my_tickets"></div>
	</div>
</div>