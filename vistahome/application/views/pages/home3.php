<?php 

  if(!$logs){

    $last_login ="";

  } else {

    foreach($logs as $g){

      $get_log = $g['tl_date'];

    }

    $last_login = strtotime($get_log);

    $select=""; $table="ZHOA_attempts"; $where="at_username ='".$uname."' AND at_date > '".date('Y-m-d H:i:s', $last_login)."'";
    $attempts = $this -> Main -> count_select_where($select, $table, $where);

    if(!$attempts){

      $attempt_count = 0;

    } else {

      $attempt_count = $attempts;

    }

  }

?>
<section class="container">

  <div class="row">

    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"> 
      <div class="panel panel-success">
        <div class="panel-heading">Welcome <strong><?=$uname?></strong></div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9"> 

      <div class="panel panel-primary">
        <?php if($last_login !=""){ ?>
          <div class="panel-heading">Your last login was <strong><?=date('F d, Y h:i:s A', $last_login)?></strong>. You have <strong><?=$attempt_count?></strong> invalid login attempts since last login.</div>
        <?php } else { ?>
          <div class="panel-heading">Welcome to Homeowner's Portal, this is your first login.</div>
        <?php } ?>
      </div>

    </div>

  </div>
  
  <div class="row">

    <?php if(!$get_property){ } else { ?>

      <?php $counter=0;?>
      <?php foreach($get_property as $row){ ?>

        <aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 

          <?php if($counter == 0){ ?>
          <!-- <br> -->
          <table class="table">
            <tr class="alert alert-danger">
              <th>Announcements</th>
            </tr>
          </table>

          <div class="announcement">

          <table class="table">
            <?php if(!$get_announcements){ } else { ?>
              <?php foreach($get_announcements as $a){
              $date_posted = strtotime($a['a_dposted']); ?>

                <tr>
                  <td style="width: 250px;"><?php if(strlen($a['a_subject']) > 20){ echo substr($a['a_subject'], 0,20)."..."; } else { echo $a['a_subject']; }?></td>
                  <td><small><?=date('m/d/Y', $date_posted)?></small></td>
                </tr>

             <?php } ?>
            <?php } ?>
          </table>

          </div>

          <br>

          <?php } else { ?>

          <?php } ?>

        </aside>

        <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">

          <!-- <br> -->
          <table class="table">
            <tr class="panel panel-primary">
              <th class="panel-heading">House Model</th>
            </tr>
          </table>

          <img src="<?=base_url()?>assets/img/properties/default.jpg" width="100%"; class="img-thumbnail">
          <div style="height: 10px;">&nbsp;</div>
          <img src="<?=base_url()?>assets/img/properties/default.jpg" width="100%"; class="img-thumbnail">

        </div>

        <?php $select="XWETEXT"; $table="ZHOA_PROJECTS"; $where="SWENR = ".$row['SWENR'];?>
        <?php $get_projname = $this -> Main -> select_data_where($select, $table, $where); ?>

        <?php foreach($get_projname as $gp){ $projname = $gp['XWETEXT']; } ?>

        <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
          <!-- <br> -->
          <table class="table">
            <tr class="panel panel-primary">
              <th class="panel-heading" style="width: 50%;">House Details</th>
              <td class="panel-heading"><button style="float: right;" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-pawn" title="Open"></span></button></td>
            </tr>
            <tr>
            <th style="width: 50%;">SO Number</th>
              <td><?=$row['VBELN']?></td>
            </tr>
            <tr>
              <th style="width: 50%;">Project Name</th>
              <td><?=$projname?></td>
            </tr>
            <tr>
              <th>Blk/Lot Unit #</th>
              <td><?=$row['REFNO']?></td>
            </tr>
            <tr>
              <th>House Model</th>
              <td><?=$row['ARKTX']?></td>
            </tr>
            <tr>
              <th>Floor Area</th>
              <td><?=$row['FLR_AREA']?></td>
            </tr>
            <tr>
              <th>Lot Area</th>
              <td><?=$row['LOT_SIZE']?></td>
            </tr>
          </table>
        </div>

      <div class="col-lg-12">&nbsp;</div> 

    <?php $counter++; } ?>

    </div>


    <?php } ?>

  <!-- <div class="row">

    <aside class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
      <table class="table">
      </table>
    </aside>

    <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">

      <table class="table">
        <tr class="panel panel-primary">
          <th class="panel-heading">House Model</th>
        </tr>
      </table>

      <img src="<?=base_url()?>assets/img/properties/sample1.jpg" width="100%"; class="img-thumbnail">
      <div style="height: 10px;">&nbsp;</div>
      <img src="<?=base_url()?>assets/img/properties/sample2.jpg" width="100%"; class="img-thumbnail">

    </div>

    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7">
      <table class="table">
        <tr class="panel panel-primary">
          <th class="panel-heading" style="width: 50%;">House Details</th>
          <td class="panel-heading"></td>
        </tr>
        <tr>
        <th style="width: 50%;">SO Number</th>
          <td>21000000006</td>
        </tr>
        <tr>
          <th style="width: 50%;">Project Name</th>
          <td>asd</td>
        </tr>
        <tr>
          <th>Blk/Lot Unit #</th>
          <td>asd</td>
        </tr>
        <tr>
          <th>House Model</th>
          <td>asd</td>
        </tr>
        <tr>
          <th>Floor Area</th>
          <td>asd</td>
        </tr>
        <tr>
          <th>Lot Area</th>
          <td>asd</td>
        </tr>
      </table>
    </div>

  </div>
 -->

</section>