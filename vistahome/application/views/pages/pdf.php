<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="<?=base_url()?>assets/img/logos/icon1.png">
        <title>Vista HOMe Portal Admin</title>
	<style type="text/css">

		table > tbody > tr > td {
			padding: 0 1mm;
		}

		body {
			font-family: Trebuchet MS;
			font-size: 3.3mm;
			color: black;
		}
		table {
			border-collapse: collapse;
		}

	</style>

</head>
<body>

<?php 
	foreach ($SOA as $bill) { 
		$soa_date = strtotime($bill['SOA_DATE']);
		$date_due = strtotime($bill['DATE_DUE']);
		$due_imm  = $bill['PREV_BAL'] + $bill['PENALTY'];
		switch ($bill['BUKRS']) {
			case '2000':
				$logo = "brittany";
				$payments = "Brittany";
				$company = "BRITTANY CORPORATION INCORPORATED";
				break;
			case '4001':
				$logo = "camella";
				$payments = "HDC South";
				$company = "HOUSEHOLD DEVELOPMENT CORPORATION";
				break;
			case '6910':
				$logo = "vistares";
				$payments = "Vista Residences - Branch";
				$company = "VISTA RESIDENCES, INC.";
				break;
			case '3000':
				$logo = "crown";
				$payments = "Crown Asia North";
				$company = "CROWN ASIA PROPERTIES, INC.";
				break;			
			default:
				# code...
				break;
		}
?>


	<div style="border: 0.2mm solid; padding: 0mm; background-color: white; width: 159mm; height: 251mm;">
		<div style="height:197mm; border-bottom: 0.2mm solid black">
			<img width="232px" height="70px" src="../vistahome/assets/img/pdf/<?=$logo?>.jpg" />
			<div class="col-md-12 nop4d text-center" style="font-size:6mm;">
				BILLING STATEMENT
			</div>
				<table style="padding: 4mm 0;width:100%;font-size:3.1mm">
					<tr>
						<td style="padding: 4mm 5mm; vertical-align:top">
								<!-- Arsela Bustos Barroso<br/> -->
								<?=$bill['NAME1']?><br/>
								<?=$bill['ADDRESS1']?><br/>
								<?=$bill['ADDRESS2']?>
							
						</td>
						<td style="padding: 4mm 0;">
								<table style="border: 0.2mm solid;width: 66mm; height: 23mm;margin: 0 8mm;">
									<tr>
										<td style=" padding: 0 1.5mm;">STATEMENT NO.</td>
										<!-- <td style="padding: 0 1.5mm;text-align:right">0000030943</td> -->
										<td style="padding: 0 1.5mm;text-align:right"><?=str_pad($bill['SOA_NUM'],10,'0',STR_PAD_LEFT);?></td>
									</tr>
									<tr>
										<td style=" padding: 0 1.5mm;">STATEMENT DATE</td>
										<td style="padding: 0 1.5mm;text-align:right"><?=date('m/d/Y', $soa_date)?></td>
										<!-- <td style="padding: 0 1.5mm;text-align:right">02/03/2014</td> -->
									</tr>
									<tr>
										<td style=" padding: 0 1.5mm;">CUSTOMER NUMBER</td>
										<!-- <td style="padding: 0 1.5mm;text-align:right">100000362</td> -->
										<td style="padding: 0 1.5mm;text-align:right"><?=$bill['KUNNR']?></td>
									</tr>
									<tr>
										<td style=" padding: 0 1.5mm;">AMOUNT DUE</td>
										<!-- <td style="padding: 0 1.5mm;text-align:right">0.00</td> -->
										<td style="padding: 0 1.5mm;text-align:right"><?=number_format($bill['AMT_DUE'], 2, '.', ',');?></td>
									</tr>
									<tr>
										<td style=" padding: 0 1.5mm;">PAYMENT DUE DATE</td>
										<!-- <td style="padding: 0 1.5mm;text-align:right">12/31/2013</td> -->
										<td style="padding: 0 1.5mm;text-align:right"><?=date('m/d/Y', $date_due)?></td>
									</tr>
								</table>
							
						</td>
					</tr>
				</table>
			<div style="38mm">
				<img src="../vistahome/assets/img/pdf/h1.png" style="width:86%" />
				<div style="width:84%; padding: 3mm 2mm 6mm;">
				<table style="width:100%">
					<tr>
						<td>Property</td>
						<td>:</td>
						<!-- <td style="text-align:right">Amore of PortoPh 3/055-0053</td> -->
						<td style="text-align:right"><?=$bill['XWETEXT']?>/ <?=$bill['REFNO']?></td>
					</tr>
					<tr>
						<td>Purchase Amount</td>
						<td>:</td>
						<!-- <td style="text-align:right">5,792,978.00</td> -->
						<td style="text-align:right"><?=number_format($bill['TCP'], 2, '.', ',');?></td>
					</tr>
					<tr>
						<td>Financing</td>
						<td>:</td>
						<!-- <td style="text-align:right">Bank</td> -->
						<td style="text-align:right"><?=$bill['BEZEI']?></td>
					</tr>
				</table>
				</div>

				<!-- <div style="width:86%; padding: 3mm 2mm 6mm;font-family:Trebuchet MS;">
					<div style="width:40%;display:inline-block"><span>Property</span><span style="float:right">:</span></div><div style="display:inline-block;width:60%"><span style="float:right">Amore of PortoPh 3/055-0053</span></div>
					<div style="width:40%;display:inline-block"><span>Purchase Amount</span><span style="float:right">:</span></div><div style="display:inline-block;width:60%"><span style="float:right">5,792,978.00</span></div>
					<div style="width:40%;display:inline-block"><span>Financing</span><span style="float:right">:</span></div><div style="display:inline-block;width:60%"><span style="float:right">Bank</span></div>
				</div> -->
			</div>
			<div style="height:45mm">
				<img src="../vistahome/assets/img/pdf/h2.png" style="width:86%" />
				<div style="width:84%; padding: 3mm 2mm 6mm;">
				<table style="width:100%">
					<tr>
						<td>Previous Balance</td>
						<td>:</td>
						<!-- <td style="text-align:right">5,792,978.00</td> -->
						<td style="text-align:right"><?=number_format($bill['PREV_BAL'], 2, '.', ',');?></td>
					</tr>
					<tr>
						<td>plus: Penalty</td>
						<td>:</td>
						<!-- <td style="text-align:right">5,792,978.00</td> -->
						<td style="text-align:right"><?=number_format($bill['PENALTY'], 2, '.', ',');?></td>
					</tr>
					<tr>
						<td>plus: Accrued Interest based on previous balance</td>
						<td>:</td>
						<!-- <td style="text-align:right">5,792,978.00</td> -->
						<td style="text-align:right">0.00</td>
					</tr>
					<tr>
						<td>plus: Current Amount Due</td>
						<td>:</td>
						<!-- <td style="text-align:right">5,792,978.00</td> -->
						<td style="text-align:right"><?=number_format($bill['CUR_AMT'], 2, '.', ',');?></td>
					</tr>
					<tr>
						<td style="font-weight:bold"><br/>TOTAL AMOUNT DUE</td>
						<td><br/>:</td>
						<!-- <td style="text-align:right"><br/>5,792,978.00</td> -->
						<td style="text-align:right"><br/><?=number_format($bill['AMT_DUE'], 2, '.', ',');?></td>
					</tr>
				</table>
				</div>
				<!-- <div style="width:86%; padding: 3mm 2mm 6mm;font-family:Trebuchet MS;">
					<div style="width:60%;display:inline-block"><span>Previous Balance</span><span style="float:right">:</span></div><div style="display:inline-block;width:40%"><span style="float:right">0.00</span></div>
					<div style="width:60%;display:inline-block"><span>plus: Penalty</span><span style="float:right">:</span></div><div style="display:inline-block;width:40%"><span style="float:right">0.00</span></div>
					<div style="width:60%;display:inline-block"><span>plus: Accrued Interest based on previous balance</span><span style="float:right">:</span></div><div style="display:inline-block;width:40%"><span style="float:right">0.00</span></div>
					<div style="width:60%;display:inline-block"><span>plus: Current Amount Due</span><span style="float:right">:</span></div><div style="display:inline-block;width:40%"><span style="float:right">0.00</span></div><br />
					<div style="width:60%;display:inline-block"><span style="font-weight:bold; padding-top: 5px">TOTAL AMOUNT DUE</span><span style="float:right">:</span></div><div style="display:inline-block;width:40%"><span style="float:right">0.00</span></div>
					<br />
				</div> -->
			</div>	
			<div style="height:67mm">
				<img src="../vistahome/assets/img/pdf/h3.png" style="width:86%" />
				<!-- <div style="padding: 5px">
					<div>
						Payments can be made at the following:
					</div>
					<div style="padding: 0 12mm">
						<div style="display:inline-block; width:48mm">
							For Cash and Check Payments
						</div>
						<div style="display:inline-block; width:70mm; paddin 0 5mm">
							Brittany
						</div>
					</div>
					<div style="padding: 0 12mm">
						<div style="display:inline-block; width:48mm">
							For Cash Payments only
						</div>
						<div style="display:inline-block; width:75mm; paddin 0 5mm">
							All Branches of Unionbank of the Phils. (UBP) *c/o Bills Payment
						</div>
					</div>
				</div> -->
				<table>
					<tr>
						<td style="padding: 0 2mm">Payments can be made at the following:</td>
					</tr>
					<tr>
						<td style="padding: 0 6mm 0 12mm">For Cash and Check Payments</td>
						<td style="width:70mm; paddin 0 5mm"><?=$payments?></td>
					</tr>
					<tr>
						<td style="padding: 0 6mm 0 12mm; vertical-align:top">For Cash Payments only</td>
						<td style="width:70mm; paddin 0 5mm">All Branches of Unionbank of the Phils. (UBP) *c/o Bills Payment</td>
					</tr>
				</table>
				<div style="padding: 2mm">
					<span style="font-weight:bold">Question About Your Bill</span><br/>
					<div style="font-size:3.1mm">
						Please examine the charges posted in your Statement of Account. In case you have any question, please call  for AMG and  for Sales Admin Dept. or visit the Brittany. It is agreed and understood that if no complaint is received within 20 days from statement date, said statement and all transactions therein shall be considered correct and any claim against the developer shall be waived.
						<br/><br/>
						Note:<br/>
						1. Please disregard if payment has been made earlier.<br/>
						2. Computation is valid as of current month.
					</div>
				</div>
			</div>
		</div>
		<div>
			<!-- <div style="height:23mm; border-bottom: 0.2mm solid black">
				<div style="text-align:center;font-weight:bold">PAYMENT CENTER/ BANK COPY</div>
				<div style="display:inline-block;font-size: 3.17mm;"><br/>
					Arsela Bustos Barroso<br/>
					B1 L2 Ponticelli Garden<br/>
					Bacoor Cavite 4102
				</div>
				<div style="display:inline-block;width:75mm;float:right">
					Please make all checks payable to BRITTANY CORPORATION INCORPORATED, INDICATE YOUR CUSTOMER NUMBER AND NAME at the back of check.
				</div>
			</div> -->
			<table style="height:23mm;width:100%; border-bottom: 0.2mm solid black">
				<tr>
					<th colspan="2" style="text-align:center"><div>PAYMENT CENTER/ BANK COPY</div></th>
				</tr>
				<tr>
					<td style="width:85mm; padding: 0 2mm 2mm">
						<!-- Arsela Bustos Barroso<br/> -->
						<?=$bill['NAME1']?><br/>
						<?=$bill['ADDRESS1']?><br/>
						<?=$bill['ADDRESS2']?>
					</td>
					<td style="padding: 0 2mm 2mm">Please make all checks payable to <span style="font-weight:bold"><?=$company?></span>, INDICATE YOUR CUSTOMER NUMBER AND NAME at the back of check.</td>
				</tr>
			</table>
			<table style="text-align:center; border-bottom: 0.2mm solid black">
				<tr>
					<th style="height:6mm;text-align:center;width:24.5%; border-right:0.2mm solid black">CUSTOMER NUMBER</th>
					<th style="height:6mm;text-align:center;width:30.20%; border-right:0.2mm solid black">STATEMENT No.</th>
					<th style="height:6mm;text-align:center;width:38mm;">PAYMENT DUE DATE</th>
					<th style="height:6mm;text-align:center;width:34mm;">AMOUNT</th>
				</tr>
				<tr>
					<!-- <td rowspan="2" style="width:24.5%; border-right:0.2mm solid black;border-top:0.2mm solid black">1234567890</td> -->
					<td rowspan="2" style="width:24.5%; border-right:0.2mm solid black;border-top:0.2mm solid black"><?=$bill['KUNNR']?></td>
					<!-- <td rowspan="2" style="width:30.20%; border-right:0.2mm solid black;border-top:0.2mm solid black">9876543210</td> -->
					<td rowspan="2" style="width:30.20%; border-right:0.2mm solid black;border-top:0.2mm solid black"><?=str_pad($bill['SOA_NUM'],10,'0',STR_PAD_LEFT);?></td>
					<!-- <td style="height:11mm;width:38mm;text-align:left;padding:0 1mm;border-top:0.2mm solid black">Due Immediately<br />12/31/2013</td> -->
					<td style="height:11mm;width:38mm;text-align:left;padding:0 1mm;border-top:0.2mm solid black">Due Immediately<br /><?=date('m/d/Y', $date_due)?></td>
					<td style="height:11mm;width:34mm;text-align:right;padding:0 1mm;border-top:0.2mm solid black"><?=number_format($due_imm, 2, '.', ',');?><br/><?=number_format($bill['CUR_AMT'], 2, '.', ',');?></td>
					<tr style="height:7mm">
						<td style="height:7mm;width:38mm;text-align:left;padding:0 1mm;border-top:0.2mm solid black;font-weight:bold">TOTAL AMOUNT DUE</td>
						<td style="height:7mm;width:34mm;text-align:right;padding:0 1mm;border-top:0.2mm solid black"><?=number_format($bill['AMT_DUE'], 2, '.', ',');?></td>
					</tr>
				</tr>	
				<tr></tr>
			</table>



			<!-- <div style="height: 6mm; border-bottom: 0.2mm solid black; font-size: 12px;">
				<div style="border-right: 0.2mm solid black; text-align: center; display: inline-block; width: 38mm; height: 100%; padding: 0.2mm;">
					CUSTOMER NUMBER
				</div>
				<div style="width: 47mm; border-right: 0.2mm solid black; text-align: center; display: inline-block; height: 100%; padding: 0.2mm;">
					STATEMENT No.
				</div>
				<div style="width: 72mm;">
					<div style="width: 39mm; text-align: center; display: inline-block; padding: 0.2mm; height: 100%;">
						PAYMENT DUE DATE
					</div>
					<div style="width: 32mm; text-align: center; display: inline-block; padding: 0.2mm; height: 100%;">
						AMOUNT
					</div>
				</div>
			</div>
			<div style="height:18mm; border-bottom: 0.2mm solid black">
				<div style="border-right: 0.2mm solid black; text-align: center; display: inline-block; width: 38mm; height: 100%; padding: 0.2mm;">
					9876543210
				</div>
				<div style="width: 47mm; border-right: 0.2mm solid black; text-align: center; display: inline-block; height: 100%; padding: 0.2mm;">
					1234567890
				</div>
				<div style="width: 39mm; text-align: center; display: inline-block; padding: 0.2mm; height: 100%;">
					<div style="height:11mm; border-bottom:0.2mm solid black"></div>
					<div style="height:7mm"></div>
				</div>
				<div style="width: 32mm; text-align: center; display: inline-block; padding: 0.2mm; height: 100%;">
					<div style="height:11mm; border-bottom:0.2mm solid black"></div>
					<div style="height:7mm"></div>
				</div>
			</div>
			<div style="height:7mm"></div> -->
			<!-- <htmlpagefooter name="footer">
				<hr />
				<div id="footer">	
					<table>
						<tr><td>Software Solutions</td><td>Mobile Solutions</td><td>Web Solutions</td></tr>
					</table>
				</div>
			</htmlpagefooter>
			<sethtmlpagefooter name="footer" value="on" />	 -->
		</div>	

	</div>
<?php } ?>

</body>
</html>