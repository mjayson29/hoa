				<div class="panel-heading">List of Request</div>

					<?php if(!$tickets){ ?>

						<div class="tab-field"><h3><span class="glyphicon glyphicon-exclamation-sign"></span> No Data</h3></div>

					<?php } else { ?>

					<div style="overflow-x: auto; ">
					
						<table class="table">

							<tr>
								<th style="width: 15%;">Ticket #</th>
								<th style="width: 15%;">Blklot</th>
								<th style="width: 30%;">Date Posted</th>
								<th style="width: 30%;">Concern(s)</th>
								<th style="width: 10%;">Status</th>

							</tr>

							<!-- <tr>
								<td style="width: 100px;">Ticket #</td>
								<td style="width: 100px;">2000000009</td>
								<td> Mar 23, 2015; 12:30pm</td>
								<td style="width: 100px;"><?php $msg = "the quick brown fox jumps over the lazy dog the quick brown fox jumps over the lazy dog"; 
								if(strlen($msg) > 25){ echo substr($msg, 0,24)."..."; }?></td>
								<td>Support</td>
								<td>Mar 23, 2015; 12:30pm</td>
								<td style="width: 100px;">Closed</td>
							</tr> -->
							<?php foreach($tickets as $row){ $date_created = strtotime($row['t_date_created']);?>
								<?php $select = "c_rtype"; $table = "ZHOA_concerns"; $where = "c_ticket_id = ".$row['t_id']." AND c_is_deleted = 0"; ?>
								<?php $concerns = $this -> Main -> select_data_where($select, $table, $where); ?>
								<?php $select = "rt_desc, rt_id"; $table = "ZHOA_request_type"; $where = "rt_is_deleted = 0"; ?>
								<?php $request = $this -> Main -> select_data_where($select, $table, $where); ?>
									<?php $concern_list=""; ?>
									<?php if(!$concerns){ } else { ?>
										<?php foreach($concerns as $c){ ?>
											<?php foreach($request as $rtype){ ?>

												<?php if($rtype['rt_id'] == $c['c_rtype']){ ?>

													<?php $concern_list.=$rtype['rt_desc'].", ";?>

												<?php }?>

											<?php } ?>

										<?php } ?>
									<?php } ?>
									<tr onclick="window.location.href='<?=base_url()?>portal/view_request/<?=str_pad($row['t_id'], 10, 0, STR_PAD_LEFT)?>'" class="mod_tr">
										<td style="width: 15%;"><?=str_pad($row['t_id'], 10, '0', STR_PAD_LEFT)?></td>
										<td style="width: 15%;"><?=$row['t_blklot']?></td>
										<td style="width: 30%;"><?=date('F d, Y h:i:s A', $date_created)?></td>
										<td style="width: 30%;"> <?=substr($concern_list, 0, -2)?> </td>
										<td style="width: 10%;"><?php if($row['t_status'] == 1){ echo "Open"; }else{echo "Closed";} ?></td>
									</tr>
							<?php } ?>

						</table>

					</div>

					<?php }?>

					<div class="col-md-12 pagination-dark" style="text-align: center;"><?=$ticket_links?></div>	
