<script type="text/javascript">
	$("#attachfile").click(function(){
	    $("#userfile").trigger("click");
	});
</script>
<?php
	if(!$concern_details){
		// NO DATA";
	} else { ?>
	<div class="row" style="margin:10px; max-height: 300px; overflow: auto; overflow-x: hidden; min-width: 320px;">
		
		<?php 
		foreach($concern_details as $cd){
			if($cd['cd_concern_id'] == $get_id){
				$date_created = strtotime($cd['cd_date_created']);
				$today = date('Y-m-d H:i:s');
				$start_date = new DateTime(date('Y-m-d H:i:s', $date_created));
				$since_start = $start_date->diff(new DateTime($today));
				if($since_start->h <= 0){
					if($since_start->i <= 0){
						if($since_start->s <= 1){
							$seconds = $since_start->s;
							$get_time = $seconds." sec ago";
						} else {
							$seconds = $since_start->s;
							$get_time = $seconds." secs ago";
						}
					} else {
						if($since_start->i == 1){
							$minutes = $since_start->i;
							$get_time = $minutes." minute ago";
						} else {
							$minutes = $since_start->i;
							$get_time = $minutes." minutes ago";
						}
					}
				}

				if($since_start->h >= 1 AND $since_start->h <= 24){
					if($since_start->h == 1){
						$hours = $since_start->h;
						$get_time = $hours." hour ago";
					} else {			
						$hours = $since_start->h;
						$get_time = $hours." hours ago";
					}
				}

				if($since_start->d >= 1 AND $since_start->d <= 6){
					if($since_start->d == 1){
						$days = $since_start->d;
						$get_time = $days." day ago";
					} else {
						$days = $since_start->d;
						$get_time = $days." days ago";
					}
				}

				if($since_start->d > 6){
					$get_time = date('F d, Y H:i:s a', $date_created);
				}

				$select=""; 
				$table="ZHOA_byrlogin as a"; 
				$where="a.b_uname = '".$cd['cd_reply_by']."'";
				$join1 = "ZHOA_byrinfo as b";
				$join2 = "a.b_id = b.bi_owner_id";
	    		$orderby = "a.b_id DESC";
				$get_prof_img = $this -> Main -> get_data_where_join($select, $table, $orderby, $join1, $join2, $where);

				if($get_prof_img){ 
					foreach($get_prof_img as $row){
						if(isset($row['bi_profimg'])){
							$prof_img = $row['bi_profimg'];
							$user_img = $row['bi_profimg'];
						}else {
							$prof_img = "user.png";
							$user_img = "user.png";
						}
					}
					// if(!file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/users/".$prof_img)){  // for local use only
					if(!file_exists(APPPATH."../assets/img/users/".$prof_img)){  //for devsite
						$prof_img = "user.png";
						$user_img = "user.png";
					}
				} else {
					$select="a_profimg"; $table="ZHOA_admlogin"; $where="a_uname = '".$cd['cd_reply_by']."'";
					$get_prof_img2 = $this -> Main -> select_data_where($select, $table, $where);
					if($get_prof_img2){ 
						foreach($get_prof_img2 as $row){
							if(isset($row['a_profimg'])){
								$prof_img = $row['a_profimg'];
								$user_img = $row['a_profimg'];
							}else {
								$prof_img = "official.png";
								$user_img = "official.png";
							}	
						}
						// if(!file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/admin/".$prof_img)){  // for local use only
						if(!file_exists(APPPATH."../assets/img/admin/".$prof_img)){  //for devsite
							$prof_img = "official.png";
							$user_img = "official.png";
						}
					}
				}
				?>
		<div class="col-md-10 col-md-offset-1">
		<br/>
			<?php
			if($cd['cd_persona'] == 0) { ?>
				<div class='col-xs-2 col-sm-1 col-md-1' style="margin-left:-15px">
					<img class="img-circle prof_img media-object mod_user1" src="<?=base_url()?>assets/img/admin/<?=$prof_img?>" alt="<?=$user_img?>">
				</div>
			<?php }
			?>
			<div class="col-md-10 <?php if($cd['cd_persona'] == 1){ echo"col-md-offset-1";} ?>" style="background: white; color:black; text-shadow: none; border-radius: 5px; margin-left:<?php if($cd['cd_persona'] == 0){ echo"15px;";} ?>">
				<div class='col-xs-9 col-sm-11 col-md-12' style='word-wrap: break-word; padding: 3px 0'>
					<a href='#'><?=$cd['cd_reply_by']?></a>
				</div>
				<div class='col-xs-9 col-sm-11 col-md-12' style='word-wrap: break-word;'><?=$cd['cd_desc']?></div>

				<?php 
				if($cd['cd_filename']){
					$fileID = $cd['cd_id'];
					$full_ftype = trim($cd['cd_filename']);
					$file_type = substr($cd['cd_filename'], -3);
					$file_type_1 = substr($cd['cd_filename'], -3);

					if($full_ftype != "Attachment is not valid" && file_exists(APPPATH."../assets/concerns/".$full_ftype)){  // for devsite
					// if($full_ftype != "Attachment is not valid" && file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/concerns/".$full_ftype)){   // for local use only

						if($file_type == "gif" || $file_type == "jpg" || $file_type == "png" || $file_type_1 == "jpeg" || $file_type == "GIF" || $file_type == "JPG" || $file_type == "PNG" || $file_type_1 == "JPEG"){ ?>
							<div class="col-xs-9 col-sm-11 col-md-12">
								<div class="col-md-5">
									<img src="<?=base_url()?>assets/concerns/<?=$cd['cd_filename']?>" style="height:100px; width:100px;" class="img-responsive">
								</div>
							</div>
						<?php } ?>
						<div class="col-xs-9">
							<a href="<?=base_url()?>support/download_file/<?=$fileID?>"><span class="glyphicon glyphicon-paperclip"></span> <?=$cd['cd_filename']?> </a>
						</div>
					<?php } else { ?>
					<div class="col-xs-9" title="File Corrupted" style="color: #d43f3a;text-decoration: line-through;">
						<span class="glyphicon glyphicon-paperclip"></span> <?=$full_ftype?>
					</div>	
					<?php } ?>
				<?php } ?>

				<div class='col-xs-9 col-sm-11 col-md-6' style='word-wrap: break-word;'>
					<small style='color: #aaa;'><?=$get_time?></small>
				</div>
			</div>
			<?php
			if($cd['cd_persona']==1){ ?>
				<div class='col-xs-2 col-sm-1 col-md-1'>
					<img class="img-circle prof_img media-object mod_user1" src="<?=base_url()?>assets/img/users/<?=$prof_img?>" alt="<?=$user_img?>">
				</div>
			<?php } ?>
		</div>
			<?php
			} ?>
		<?php 
		} ?>
	</div>
	<?php 
	} ?>

	<div class="row" style='margin: 10px;'>

		<?php if($get_status == 2 || $get_status == 3){ ?>
			<div class='col-xs-2 col-sm-2 col-md-2'> </div>
			<div class='col-xs-10 col-sm-10 col-md-10'>
				Concern closed. You cannot reply to this thread.
			</div>
		<?php } else { ?>

			<!-- <div class='col-xs-2 col-sm-2 col-md-2'>
				<div class="media pull-right">
			  	    <div class="media-left">
				    	<a href="#">
			      			<img class="img-circle prof_img" src="<?=base_url('assets/img/users/').'/'.$user_img?>" alt="..." style="width: 60px;">
					    </a>
				    </div>
				</div>
			</div> -->
			<br/>
			<div class='col-md-5 col-md-offset-2' style="padding-left:0px">
				<small>Send a reply</small>
				<textarea class='form-control' style='height: 35px; resize: vertical;' name='concern_msg' id='concern_msg<?=$get_id?>'></textarea>
			</div>
			<div class="col-md-3">
				<br />
				<div class='col-md-3' style="padding-right:0px">
					<button type='button' class='btn btn-primary btn-sm form-control' onclick='send_reply(<?=$get_id?>)' title='Send'><span class='glyphicon glyphicon-send'></span></button>
				</div>
				<div class='col-md-3' style="padding-right:0px">
					<button type='button' class='btn btn-warning btn-sm form-control' title='Attach Files' id='attachfile'><span class='glyphicon glyphicon-paperclip'></span></button>
					<input type="file" name="userfile" id="userfile" style="display: none;">
				</div>
				<div class='col-md-6' style="padding-right:0px">
					<button type='button' class='btn btn-danger btn-sm form-control' onclick='close_request(<?=$get_id?>)' title='Close Request'>Close Request</button>
				</div>
			</div>

		<?php } ?>

	</div>