  <?php if(!$get_announcements){ echo "No Available Announcements"; } else { ?>
    <?php foreach($get_announcements as $row){ 
      $date_posted = strtotime($row['a_dposted']);
      $date_arr = date_parse($date_posted);
    ?>

      <div class="media">
        
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-dark">
            <h4 class="media-heading panel-heading"><?=ucwords($row['a_subject'])?><br/>
            <?php if($row['a_is_updated']==0){ ?>
              <small style="font-size:60%">posted by <a href="#"> <?=$row['postedby']?></a> - <?=date('F d, Y', $date_posted)?> at <?=date('h:i A',$date_posted)?></small>
            <?php }else{ ?>
              <small style="font-size:60%">posted by <a href="#"> <?=$row['postedby']?></a> - <?=date('F d, Y', $date_posted)?></small>
              <small style="font-size:60%;float:right">edited by <a href="#"> <?=$row['byupd'];?></a> - <?=date('F d, Y', strtotime($row['a_dupd']))?></small>
            <?php } ?>
            </h4>
            <div class="well-sm"> 
              <div class="more"><?=$row['a_msg']?></div>
                <?php if($row['a_attachment']) {
                    // if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/announcements/".$row['a_attachment'])){  // for local use only
                    if(file_exists(APPPATH."../assets/img/announcements/".$row['a_attachment'])){  //for devsite
                ?>
                <div class="uprev" style="position:relative;box-shadow: 2px 5px 5px black;">
                  <img src="<?=base_url()?>assets/img/announcements/<?=$row['a_attachment']?>" class="img-responsive">
                </div>
                  <?php } ?>
                <?php } ?>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <div class="col-md-12 pagination-dark" style="text-align: center;"><?=$announce_links?></div> 
  <?php } ?>