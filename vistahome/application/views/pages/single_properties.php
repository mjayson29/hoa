<script type="text/javascript">

    $(document).ready(function(){

      var fileInput = $('#userfile_l');
      var propid_l = $('#get_id').val();
      // button = $('#upload');
      fileInput.on('change', function(){
        var files = fileInput.prop('files');
        var curr_img = $('#prop_img').val();
        // No file was chosen!
        /*if(files.length == 0) {
          alert('Please choose a file to upload!');
          return false;
        }*/
        // Create a new FormData object, append yung file para makuha mo yung $_POST
        var fd = new FormData();
		fd.append('currfile', curr_img);
        var attachment = fd.append('userfile', files[0]);
        // var upload = confirm('Are you sure to change profile image?');
        
        // if(upload == true){   
        // Upload the file m,

          $.ajax({
            url: '<?=base_url()?>portal/upload_prop_l/'+propid_l,
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){
              document.getElementById("prop_loader"+propid_l).innerHTML = data;  
            }

          });
      });
    });

	$(document).ready(function(){
		$('#load_img_left').hover(function(){
			$('#load_img_left .glyphicon-remove').css('visibility','visible');
			$('#load_img_left .glyphicon-camera').css('visibility','visible');
		},function(){
			$('#load_img_left .glyphicon-remove').css('visibility','hidden');
			$('#load_img_left .glyphicon-camera').css('visibility','hidden');
		});
		$('#load_img_right').hover(function(){
			$('#load_img_right .glyphicon-remove').css('visibility','visible');
			$('#load_img_right .glyphicon-camera').css('visibility','visible');
		},function(){
			$('#load_img_right .glyphicon-remove').css('visibility','hidden');
			$('#load_img_right .glyphicon-camera').css('visibility','hidden');
		});
	})

	$(document).ready(function(){
		var selector = $('#removefile_l');
		var propid_l = $('#get_id').val();
		var curr_img = $('#prop_img').val();
		if (curr_img != "no-image.jpg") {
			selector.on('click', function(){
				$.ajax({
		            url: '<?=base_url()?>portal/prop_img_def_l/'+propid_l,
		            type:'POST',
		            data: {
		            	curr_img: curr_img
		            },
		            success: function(data){
		            	document.getElementById("prop_loader"+propid_l).innerHTML = data;  
		            }
	          	});
			});
		}
	});

	$(document).ready(function(){
		var selector = $('#removefile_r');
		var propid_r = $('#get_id_r').val();
		var curr_img = $('#prop_img_r').val();
		if (curr_img != "no-image.jpg") {
			selector.on('click', function(){
				$.ajax({
		            url: '<?=base_url()?>portal/prop_img_def_r/'+propid_r,
		            type:'POST',
		            data: {
		            	curr_img: curr_img
		            },
		            success: function(data){
		            	document.getElementById("prop_loader_r"+propid_r).innerHTML = data;  
		            }
	          	});
			});
		}
	});

	$(document).ready(function(){

	    var fileInput = $('#userfile_r');
	    var propid_r = $('#get_id_r').val();
	    // button = $('#upload');
	    fileInput.on('change', function(){
		    var files = fileInput.prop('files');
		    var curr_img = $('#prop_img').val();
		    // No file was chosen!
		    /*if(files.length == 0) {
		    alert('Please choose a file to upload!');
		    return false;
		    }*/
		    // Create a new FormData object, append yung file para makuha mo yung $_POST

		    var fd = new FormData();
		    var attachment = fd.append('userfile', files[0]);

		    // var upload = confirm('Are you sure to change profile image?');
		        
		    // if(upload == true){   
		    // Upload the file m,

		    $.ajax({
		        url: '<?=base_url()?>portal/upload_prop_r/'+propid_r,
		        data: fd,
		        contentType:false,
		        processData:false,
		        type:'POST',
		        success: function(data){
		            document.getElementById("prop_loader_r"+propid_r).innerHTML = data;  
		        }
		    });
	    });
	});

	function upload_attachment(){

		var fileInput = $('#attachments');
	    // button = $('#upload');
		var files = fileInput.prop('files');

		    // No file was chosen!
		if(files.length == 0) {
	  		alert('Please choose a file to upload!');
			return false;
	    } else {
			$('#attach_loader').css('display','block');
		    var fd = new FormData();
		    var attachment = fd.append('userfile', files[0]);
		    $.ajax({
		        url: '<?=base_url()?>portal/upload_attach/',
		        data: fd,
		        contentType:false,
		        processData:false,
		        type:'POST',
		        success: function(data){
		            document.getElementById("load_attachments").innerHTML = data;  
					$('#attach_loader').css('display','none');
		        }
		    });
		}
	}

	function download(val){

		var download_attach = confirm('Download attachment?');

		if(download_attach == true){
			window.location.href='<?=base_url()?>portal/download_attach/'+val;
		}

	}

	function del_attach(val){

		var del_attach = confirm("Delete attachment?");

		if(del_attach == true){
			$('#attach_loader').css('display','block');
			$.ajax({
			    url: '<?=base_url()?>portal/del_attach/'+val,
			    type:'POST',
			    success: function(data){
			        document.getElementById("load_attachments").innerHTML = data;  
					$('#attach_loader').css('display','none');
			    }
		    });
		} else {

		}
	}

</script>
	
	<section class="container mod_prop" style="padding: 10px;">

		<?php if($get_property){ ?>

	      	<?php foreach($get_property as $row){ ?>
	      	<?php 
	   			$prop_img1 = $row['PROP_IMG1'];
	   			$prop_img2 = $row['PROP_IMG2'];
	   			// if($prop_img1 == "" || !file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$prop_img1)){
	   			if($prop_img1 == "" || !file_exists(APPPATH."../assets/img/properties/".$prop_img1)){
				 	$prop_img1 = "no-image.jpg";
	   			}
	   			// if($prop_img2 == "" || !file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$prop_img2)){
	   			if($prop_img2 == "" || !file_exists(APPPATH."../assets/img/properties/".$prop_img2)){
					$prop_img2 = "no-image.jpg";
	   			}
	   		 ?>
	      		<div id="alert"></div>
				<div class="row">
					<div class="col-md-6" id="load_img_left">
                		<?php $array=array('id'=>'uploadform'); echo form_open_multipart(); ?>
							<div id="prop_loader<?=$row['ID']?>">
								<img src="<?=base_url()?>assets/img/properties/<?=$prop_img1?>" alt="<?=$prop_img1?>" width="100%" class="img-thumbnail">
								<input type="hidden" id="prop_img" value="<?=$prop_img1?>">
							</div>
									<span id="removefile_l" class="glyphicon glyphicon-remove property_image_remove" style="visibility:hidden" title="Remove Picture"></span>
							<label for="userfile_l">
								<span class="glyphicon glyphicon-camera property_image" style="visibility:hidden" title="Upload Picture"></span>
							</label>
							<?php echo form_upload(array('name'=>'userfile', 'accept'=>'image/jpg,image/jpeg,image/png','id'=>'userfile_l','style'=>'display:none;')); ?>
	                		<input type="hidden" id="get_id" value="<?=$row['ID']?>">
	                		
                		<?php echo form_close(); ?>
					</div>
					<div class="col-md-6" id="load_img_right">
						<?php $array=array('id'=>'uploadform'); echo form_open_multipart(); ?>
							<div id="prop_loader_r<?=$row['ID']?>">
								<img src="<?=base_url()?>assets/img/properties/<?=$prop_img2?>" alt="<?=$prop_img2?>" width="100%" class="img-thumbnail">
								<input type="hidden" id="prop_img_r" value="<?=$prop_img2?>">
							</div>
							<span id="removefile_r" class="glyphicon glyphicon-remove property_image_remove" style="visibility:hidden" title="Remove Picture"></span>
							<label for="userfile_r">
								<span class="glyphicon glyphicon-camera property_image" title="Upload Picture" style="visibility:hidden"></span>
							</label>
							<?php echo form_upload(array('name'=>'userfile', 'accept'=>'image/jpg,image/jpeg,image/png','id'=>'userfile_r','style'=>'display:none;')); ?>
	                		<input type="hidden" id="get_id_r" value="<?=$row['ID']?>">
	                		
                		<?php echo form_close(); ?>
					</div>
				</div>

			<?php } ?>

		<?php } ?>

	</section>

	<div class="container">
		
		<div class="row">
			<div class="col-md-12 mod_bg1" style="padding-bottom: 20px;border-radius:5px">
			<?php if(!$get_property){ } else { ?>

			    <?php foreach($get_property as $row){ ?>

			    	<?php $select="XWETEXT"; $table="ZHOA_PROJECTS"; $where="SWENR = ".$row['SWENR'];?>
				    <?php $get_projname = $this -> Main -> select_data_where($select, $table, $where); ?>

				    <?php foreach($get_projname as $gp){ $projname = $gp['XWETEXT']; } ?>

				    <div class="row mod_home_icons">
					    <div class="col-sm-6 col-md-6">
					      <div class="mod_thumbnail" style="height:auto">
					        <div class="caption" style="padding: 10px; padding-top: 20px;">
					          <h4><strong>House Details</strong></h4>
					          <hr>
					          	<div class="col-md-12">
					          		<div class="col-md-4">
					          			<strong>SO Number</strong>
					          		</div>
					          		<div class="col-md-7">
					          			<?=$row['VBELN']?>
					          		</div>
					          	</div>
					          	<div class="col-md-12">
					          		<div class="col-md-4">
					          			<strong>Project Name</strong>
					          		</div>
					          		<div class="col-md-7">
					          			<?=$projname?>
					          		</div>
					          	</div>
					          	<div class="col-md-12">
					          		<div class="col-md-4">
					          			<strong>Blk/Lot Unit #</strong>
					          		</div>
					          		<div class="col-md-7">
					          			<?=$row['REFNO']?>
					          		</div>
					          	</div>
					          	<div class="col-md-12">
					          		<div class="col-md-4">
					          			<strong>House Model</strong>
					          		</div>
					          		<div class="col-md-7">
					          			<?=$row['ARKTX']?>
					          		</div>
					          	</div>
					          	<div class="col-md-12">
					          		<div class="col-md-4">
					          			<strong>Floor Area</strong>
					          		</div>
					          		<div class="col-md-7">
					          			<?=$row['FLR_AREA']?>
					          		</div>
					          	</div>
					          	<div class="col-md-12">
					          		<div class="col-md-4">
					          			<strong>Lot Area</strong>
					          		</div>
					          		<div class="col-md-7">
					          			<?=$row['LOT_SIZE']?>
					          		</div>
					          	</div>
					            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
					        </div>
					      </div>
					    </div>
					    <div class="col-sm-6 col-md-6">
					      <div class="mod_thumbnail" style="height:auto">
					        <div class="caption " style="padding: 0px; padding-top: 20px;">
					          <h4><strong>Attachments</strong></h4>
								<img src="<?=base_url()?>assets/img/tools/loader.gif" style="display:none;" id="attach_loader">
					          	<div class="row panel-body" id="load_attachments">
									<?php if(!$attachments){ echo "NO DATA"; } else { ?>
							          	<div class="col-md-12">
							          		<table class="table" style="margin-bottom: 0px;">

							          			<tr>
													<th style="width: 90%;">Filename</th>
													<th style="width: 5%;">Action</th>
													<th style="width: 5%;"></th>
												</tr>

							          		</table>
								        </div>
							          	<div style=" max-height: 150px; margin: 0px; text-align: left; overflow: auto;" class="col-md-12">
											<table class="table" style="margin: 0px;">
													<?php foreach($attachments as $a){ ?>
													<tr>
														<td style="width: 90%;"><?=$a['pa_filename']?></td>
														<td style="width: 5%;"><span style="font-size: 20px; color: rgb(92, 184, 92);" class="glyphicon glyphicon-download" onclick="download(<?=$a['pa_id']?>)"></span></td>
														<td style="width: 5%;"><span style="font-size: 20px; color: rgb(217, 83, 79);" class="glyphicon glyphicon-remove-sign" onclick="del_attach(<?=$a['pa_id']?>)"></span></td>
													</tr>
													<?php } ?>
											</table>
								        </div>
									<?php } ?>
						        </div>
						        <div class="row panel-body">
						        	<?php $array=array('id'=>'uploadform'); echo form_open_multipart(); ?>
							        	<div class="col-xs-3 col-sm-3">
							        		Upload File
							        	</div>
							        	<div class="col-xs-6 col-sm-6 col-md-6">
							        		<div class="col-md-12">
												<?php echo form_upload(array('name'=>'userfile', 'accept'=>'image/jpg,image/jpeg,image/png', 'id'=>'attachments','style'=>'background: #333;padding: 5px 15px;height:100%','class'=>'form-control input-sm')); ?>
							        		</div>
							        		<div class="clearfix"></div>
							        	</div>
							        	<div class="col-xs-3 col-sm-3">
							        		<button type="button" class="form-control input-sm btn btn-primary btn-sm" onclick="upload_attachment()"><span class="glyphicon glyphicon-upload" style="float: none; font-size: 20px;"></span></button>
							        	</div>
			                		<?php echo form_close(); ?>
					            <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
					        	</div>
					        </div>
					    </div>
					    <div class="col-md-12">&nbsp;</div>
					</div>
			    <?php } ?>
			<?php } ?>
			</div>
		</div>
	</div>
	<br>
	<br>