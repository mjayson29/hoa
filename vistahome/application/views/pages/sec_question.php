      <?php if(!$get_profile){ } else { ?>
      
      <?php foreach($get_profile as $row){ ?>

        <?php $select = "sq_desc, sq_id"; $table = "ZHOA_secquestion"; ?>
        <?php $squestion = $this -> Main -> select_data($select, $table); ?>

        <div class="container" style="border: 1px solid #000">

          <div class="row mod_profile_bg">

            <div class="col-md-4"></div>

            <div class="col-md-4">

              <?php if(!$sec_update){ } else {
                    foreach($sec_update as $row2){ 
                      $sec_result = strtotime($row2['bi_csec_date']);
                    }
                  }

                  $from = new DateTime(date('Y-m-d', $sec_result));
                  $to   = new DateTime('today');
                  $csec_date = $from->diff($to)->d;

                  if($csec_date == 0){

                    $result = "Today";

                  } else if($csec_date == 1){

                    $result = "Yesterday";

                  } else if($csec_date >= 2 and $csec_date <= 6) {

                    $result = $csec_date. " days ago.";

                  } else {

                    $result = date('F d, Y h:i:s a', $sec_result);

                  }

              ?>
              <strong>Sec. question last updated: </strong> <?=$result?>

              <br>

              <small>Security Question 1</small>
              <br>
              <select class="form-control input-sm" id="sec1">
                
                <?php foreach($squestion as $row1){ ?>
                  <option value="<?=$row1['sq_id']?>" <?php if($row1['sq_id'] == $row['bi_sec1']){ echo "selected"; } ?>><?=$row1['sq_desc']?></option>
                <?php } ?> 
              </select>

              <small>Answer 1</small>
              <br>
              <input type="text" class="form-control input-sm" value="<?=$row['bi_ans1']?>" id="ans1">
           
              <small>Security Question 2</small>
              <br>
              <select class="form-control input-sm" id="sec2">
                
                <?php foreach($squestion as $row1){ ?>
                  <option value="<?=$row1['sq_id']?>" <?php if($row1['sq_id'] == $row['bi_sec2']){ echo "selected"; } ?>><?=$row1['sq_desc']?></option>
                <?php } ?> 
              </select>

              <small>Answer 2</small>
              <br>
              <input type="text" class="form-control input-sm" value="<?=$row['bi_ans2']?>" id="ans2">
           
            </div>

            <div class="col-md-12">&nbsp;</div>

            <div class="col-md-4"></div>
            <div class="col-md-2">
              <input type="button" name="" class="btn btn-primary btn-sm form-control input-sm" value="Update" onclick="updated_secquestion(<?=$row['bi_owner_id']?>)">
            </div>
            <div class="col-md-2">
              <input type="button" name="" class="btn btn-danger btn-sm form-control input-sm" value="Cancel" onclick="prof_cancel()">
            </div>
            <div class="col-md-4"></div>


          </div>

        </div>

      <?php } ?>

    <?php } ?>