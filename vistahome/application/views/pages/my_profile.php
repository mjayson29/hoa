<script type="text/javascript">

    $(document).ready(function(){

      var fileInput = $('#userfile');
      var bi_id = $('#get_id').val();
      // button = $('#upload');
      fileInput.on('change', function(){
        var files = fileInput.prop('files');
        // Create a new FormData object, append yung file para makuha mo yung $_POST
        var fd = new FormData();
        var attachment = fd.append('userfile', files[0]);

          $.ajax({
            url: '<?=base_url()?>portal/upload/'+bi_id,
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){
              document.getElementById("image_loader").innerHTML = data;  
            }

          });
      });
    });

</script>

<script type="text/javascript">
    
      $(document).ready(function(){
          $('#image_loader').hover(function(){
          $('#dP').addClass('hov');
          $('.profile_image').addClass('sho');
          $('.profile_image_rm').addClass('sho');
        }, function(){
          $('#dP').removeClass('hov');
          $('.profile_image').removeClass('sho');
          $('.profile_image_rm').removeClass('sho');
        });
      });

</script>

<script type="text/javascript">
    $(document).ready(function(){
      $(".glyphicon").click(function(){
          $(this).toggleClass("glyphicon-minus");
      });
    });
</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('.profile_image_rm').on('click', function(){
      var propid_l = $('#get_id').val();
      var curr_img = $('#prop_img').val();
      if (curr_img != "users.png"){
        $.ajax({
          url: '<?=base_url()?>portal/dP_def/'+propid_l,
          type:'POST',
          data: {
            curr_img: curr_img
          },
          success: function(data){
            document.getElementById("image_loader").innerHTML = data;  
          }
        });
      }
    }); 
  });
</script>

<script type="text/javascript">
    function shw(val){

    if(val == 1){

      $('#changepword').slideToggle(200);

    }

    if(val == 2){

      $('#edprofile').slideToggle(200);

    }

    if(val == 3){

      $('#secquestion').slideToggle(200);

    }

  }

</script>
</script>
  
<!-- <div class="home_banner_design"></div> -->
<!-- <div class="mod_cover"></div> -->
<section class="mod_profile">

  <?php if(!$get_profile){} else { ?>

    <?php foreach($get_profile as $row){ ?>
      <?php if($row['bi_profimg'] != ""){ $profimg = $row['bi_profimg']; } else { $profimg = "user.png"; } ?>
      <?php $fullname = $row['bi_lname'].", ".$row['bi_fname']." ".$row['bi_mname']; ?>
      <?php if(!$owned_prop){ $count_prop = 0; } else { $count_prop = $owned_prop; }  ?>
  
        <div class="container">
        
          <div class="row mod_profile_bg panel panel-green">
            <div class="panel-heading">Profile</div>

            <!--  <div class="col-md-4" style="vertical-align: center;"> 

              <div class="media-left img-circle mod_user_container">
              <img src="<?=base_url()?>assets/img/brands/brittany.jpg" class="media-object mod_user"> 
              </div>

            </div> -->

            <?php 
              // if(!file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/users/".$profimg)){  // for local use only
              if(!file_exists(APPPATH."../assets/img/users/".$profimg)){  //for devsite
                $profimg = "user.png";
              }
              ?>

            <div class="col-md-3 col-md-offset-2">
              <div class="media-left img-circle mod_user_container">
                <?php 
                  $array=array('id'=>'uploadform');
                  echo form_open_multipart(); 
                ?>
                <div id="image_loader">
                  <img id="dP" src="<?=base_url()?>assets/img/users/<?=$profimg?>" class="media-object mod_user"> 
                  <input type="hidden" id="prop_img" value="<?=$profimg?>">
                  <label for="userfile">
                    <span class="glyphicon glyphicon-camera profile_image"></span>
                  </label>
                  <span class="glyphicon glyphicon-remove profile_image_rm"></span>
                </div>
                <?php echo form_upload(array('name'=>'userfile','id'=>'userfile','style'=>'display:none;')); ?>
                <input type="hidden" id="get_id" value="<?=$row['bi_id']?>">
                <?php echo form_close(); ?> 
                <br>
                <span style="font-size: 15px;"><?=ucwords($fullname)?></span>     
                <hr>
              </div>
            </div>


            <div class="col-md-5 mod_prof_details" style="overflow: auto; padding:15px; min-width: 200px; word-wrap: break-word;">

              <label><small>Customer Code</small></label> <strong><?=$row['bi_cust_no']?> </strong>
              <br>
              <label><small>Username</small></label> <strong><?=$uname?></strong>
              <br>  
              <label><small>Address 1</small></label> <strong><?=$row['bi_add1']?></strong>
              <br>
              <label><small>Address 2</small></label> <strong><?=$row['bi_add2']?></strong>
              <br>
              <label><small>Province</small></label> <strong><?=$row['bi_province']?></strong>
              <br>
              <label><small>Zip</small></label> <strong><?=$row['bi_zip']?></strong>
              <br>
              <label><small>Contact</small></label> <strong><?=$row['bi_contact']?></strong>
              <br>
              <label><small>Email</small></label> <strong><?=$row['bi_email']?></strong>
              <br>
              <label><small>Units Owned</small></label> <strong><?=$count_prop?></strong>

              <br>

            </div>


           <!--  <div class="col-sm-3 col-md-2">
              <button type="button" class="btn btn-primary btn-sm form-control input-sm" onclick="change_pword(<?=$row['bi_owner_id']?>)">Change Password</button>
            </div>

            <div class="col-sm-6 col-md-4">
              <button type="button" class="btn btn-primary btn-sm form-control input-sm" onclick="sec_question(<?=$row['bi_owner_id']?>)">Change Security Question</button>
            </div>

            <div class="col-sm-3 col-md-2">
              <button type="button" class="btn btn-primary btn-sm form-control input-sm" onclick="edprofile(<?=$row['bi_owner_id']?>)">Edit Profile</button>
            </div> -->
        
        </div>
        <div class="row mod_profile_bg panel panel-green">
          <div class="panel-heading">Change Password <span class="pull-right"><span class="glyphicon glyphicon-plus click_tool" style="font-size: 10px;" onclick="shw(1)" aria-expanded="true"></span></span></div>
          <div class=" collapse" aria-expanded="true" id="changepword">
            <div class="container">       
              <div class="row">
                <div class="panel-body">
                  <div class="col-md-4"> </div>
                  <div class="col-md-4">
                    <?php if(!$pword_update){ } else {
                              foreach($pword_update as $row){ 
                                $pword_result = strtotime($row['b_cpass_date']);
                              }
                            }

                            $from = new DateTime(date('Y-m-d', $pword_result));
                            $to   = new DateTime('today');
                            $cpass_date = $from->diff($to)->d;

                            if($cpass_date == 0){

                              $result = "Today";

                            } else if($cpass_date == 1){

                              $result = "Yesterday";

                            } else if($cpass_date >= 2 and $cpass_date <= 6) {

                              $result = $cpass_date. " days ago.";

                            } else {

                              $result = date('F d, Y h:i:s a', $pword_result);

                            }

                            //$cpass_date = date('F d, Y h:i A', $pword_result);

                          ?>
                          
                          <strong>Password last updated: </strong> <?=$result?>

                          <br>

                    <small>Current Password</small>
                    <br>
                    <input type="password" id="curpword" class="form-control input-sm" required>

                    <!-- <br> -->

                    <small>New Password</small>
                    <br>
                    <input type="password" id="newpword" class="form-control input-sm" required>

                    <!-- <br> -->

                    <small>Retype Password</small>
                    <br>
                    <input type="password" id="repword" class="form-control input-sm" required>
                  </div>
                  <div class="col-md-12"> &nbsp; </div>
                  <div class="col-md-4"> </div>
                  <div class="col-md-2">
                    <input type="button" name="change_password" class="form-control input-sm btn btn-primary btn-sm" value="Update" onclick="updated_pword(<?=$id?>)">
                  </div>
                  <div class="col-md-2">
                    <input type="button" name="" class="form-control input-sm btn btn-danger btn-sm" value="Cancel" data-toggle="collapse" data-target="#changepword" aria-expanded="true">
                  </div>
                  <div class="col-md-12"> </div>
                </div>
              </div>
            </div>
          </div>  
        </div>
        <div class="row mod_profile_bg panel panel-green">
          <div class="panel-heading">Edit Profile <span class="pull-right"><span class="glyphicon glyphicon-plus click_tool" style="font-size: 10px;" onclick="shw(2)" aria-expanded="true"></span></span></div>
          <div class="collapse" aria-expanded="true" id="edprofile">
            <div class="container">
              <div class="panel-body">
              <?php if(!$get_profile){} else { ?>

                <?php foreach($get_profile as $row){ 
                  $cust_no = $row['bi_cust_no'];
                  $add1 = $row['bi_add1'];
                  $add2 = $row['bi_add2'];
                  $province = $row['bi_province'];
                  $zip = $row['bi_zip'];
                  $contact = $row['bi_contact'];
                  $email = $row['bi_email'];

                } ?>

                    <div class="row">

                      <div class="col-md-2"> </div>

                      <div class="col-md-4">

                        <small>Customer Code</small>
                        <br>
                        <input type="text" class="form-control input-sm" readonly value="<?=$cust_no?>" disabled>

                        <small>Username</small>
                        <br>
                        <input type="text" class="form-control input-sm" readonly value="<?=$uname?>" disabled>

                        <small>Address 1</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$add1?>" id="add1">

                        <small>Address 2</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$add2?>" id="add2">
                      
                      </div>

                      <div class="col-md-4">

                        <small>Province</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$province?>" id="province">

                        <small>Zip</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$zip?>" id="zip">

                        <small>Contact</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$contact?>" id="contact">

                        <small>Email</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$email?>" id="email">

                      </div>
                      
                      <div class="col-md-12">&nbsp;</div>

                      <div class="col-md-2"> </div>
                      <div class="col-md-4"><input type="button" name="" class="btn btn-primary btn-sm form-control input-sm" value="Update" onclick="updated_profile(<?=$row['bi_owner_id']?>)"></div>
                      <div class="col-md-4"><input type="button" name="" class="btn btn-danger btn-sm form-control input-sm" value="Cancel" data-toggle="collapse" data-target="#edprofile" aria-expanded="true"></div>
                      <div class="col-md-2"> </div>

                      <div class="clearfix">&nbsp;</div>

                    </div>

              <?php } ?>

              </div>
            </div>
          </div>
        </div>
        <div class="row mod_profile_bg panel panel-green">
          <div class="panel-heading">Change Security Question <span class="pull-right"><span class="glyphicon glyphicon-plus click_tool" style="font-size: 10px;" onclick="shw(3)" aria-expanded="true"></span></span></div>
          <div class=" collapse" aria-expanded="true" id="secquestion">
            <div class="container">
              <div class="panel-body">
              <?php if(!$get_profile){ } else { ?>
        
                <?php foreach($get_profile as $row){ ?>

                  <?php $sec1 = $row['bi_sec1'];
                        $sec2 = $row['bi_sec2'];  
                        $ans1 = $row['bi_ans1'];  
                        $ans2 = $row['bi_ans2'];  
                        $owner_id = $row['bi_owner_id'];  
                  ?>
                  <?php $select = "sq_desc, sq_id"; $table = "ZHOA_secquestion"; $where = "sq_id != ".$sec2;?>
                  <?php $squestion1 = $this -> Main -> select_data_where($select, $table, $where); ?>

                  <?php $select = "sq_desc, sq_id"; $table = "ZHOA_secquestion"; $where = "sq_id != ".$sec1;?>
                  <?php $squestion2 = $this -> Main -> select_data_where($select, $table, $where); ?>
                    
                <?php } ?>
                    <div class="row">

                      <div class="col-md-2"></div>

                      <div class="col-md-4">

                        <?php if(!$sec_update){ } else {
                              foreach($sec_update as $row2){ 
                                $sec_result = strtotime($row2['bi_csec_date']);
                              }
                            }

                            $from = new DateTime(date('Y-m-d', $sec_result));
                            $to   = new DateTime('today');
                            $csec_date = $from->diff($to)->d;

                            if($csec_date == 0){

                              $result = "Today";

                            } else if($csec_date == 1){

                              $result = "Yesterday";

                            } else if($csec_date >= 2 and $csec_date <= 6) {

                              $result = $csec_date. " days ago.";

                            } else {

                              $result = date('F d, Y h:i:s a', $sec_result);

                            }

                        ?>
                        <strong>Sec. question last updated: </strong> <?=$result?>

                        <br>

                        <small>Security Question 1</small>
                        <br>
                        <select class="form-control input-sm" id="sec1" onchange="sec1(this.value)">
                          
                          <?php foreach($squestion1 as $row1){ ?>
                            <option value="<?=$row1['sq_id']?>" <?php if($row1['sq_id'] == $sec1){ echo "selected"; } ?>><?=$row1['sq_desc']?></option>
                          <?php } ?> 

                        </select>

                        <small>Answer 1</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$ans1?>" id="ans1">

                      </div>

                      <div class="col-md-4">
                     
                        <br>
                        <small>Security Question 2</small>
                        <br>

                        <select class="form-control input-sm" id="sec2" onchange="sec2(this.value)">
                          
                          <?php foreach($squestion2 as $row2){ ?>
                            <option value="<?=$row2['sq_id']?>" <?php if($row2['sq_id'] == $sec2){ echo "selected"; } ?>><?=$row2['sq_desc']?></option>
                          <?php } ?> 
                        </select>

                        <small>Answer 2</small>
                        <br>
                        <input type="text" class="form-control input-sm" value="<?=$ans2?>" id="ans2">
                     
                      </div>

                      <div class="col-md-12">&nbsp;</div>

                      <div class="col-md-2"></div>
                      <div class="col-md-4">
                        <input type="button" name="" class="btn btn-primary btn-sm form-control input-sm" value="Update" onclick="updated_secquestion(<?=$owner_id?>)">
                      </div>
                      <div class="col-md-4">
                        <input type="button" name="" class="btn btn-danger btn-sm form-control input-sm" value="Cancel" data-toggle="collapse" data-target="#secquestion" aria-expanded="true">
                      </div>
                      <div class="col-md-2"></div>
                  </div>
              <?php } ?>
              </div>
            </div>
          </div>  
        </div>
      </div>
      <div id="load_transaction">

        

      </div>

    <?php } ?>

  <?php } ?>

</section> 