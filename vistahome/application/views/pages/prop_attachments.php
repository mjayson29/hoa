<?php if(!$attachments){ echo "NO DATA"; } else { ?>
	<div class="col-md-12">
   		<table class="table" style="margin-bottom: 0px;">
   			<tr>
				<th style="width: 90%;">Filename</th>
				<th style="width: 5%;">Action</th>
				<th style="width: 5%;"></th>
			</tr>
   		</table>
    </div>
   	<div style=" max-height: 150px; margin: 0px; text-align: left; overflow: auto;" class="col-md-12">
		<table class="table" style="margin: 0px;">
			<?php foreach($attachments as $a){ ?>
			<tr>
				<td style="width: 90%;"><?=$a['pa_filename']?></td>
				<td style="width: 5%;"><span style="font-size: 20px; color: rgb(92, 184, 92);" class="glyphicon glyphicon-download" onclick="download(<?=$a['pa_id']?>)"></span></td>
				<td style="width: 5%;"><span style="font-size: 20px; color: rgb(217, 83, 79);" class="glyphicon glyphicon-remove-sign" onclick="del_attach(<?=$a['pa_id']?>)"></span></td>
			</tr>
			<?php } ?>
		</table>
	</div>
<?php } ?>