<script type="text/javascript">
	function close_request(val){
		var close_ticket = confirm("Are you sure to close this ticket?");
		if(close_ticket == true){
			window.location.href='<?=base_url()?>portal/close_ticket/'+val;
		}
	}
</script>

<script type="text/javascript">
	function tick_cbox(val){

		var email_cbox = $('#email_cbox').val(); 

		if(email_cbox == 1){

			$.ajax({
	            type:"POST",
	            url: "<?=base_url()?>portal/remove_email_notif/"+val,
	            success: function(data){
	          	    $('#load_cbox').html(data);
	          	    alert('You turn off email notitication for this ticket.');
	          	}
	        });

		} else {
			$.ajax({
	            type:"POST",
	            url: "<?=base_url()?>portal/set_email_notif/"+val,
	            success: function(data){
	          	    $('#load_cbox').html(data);
	          	    alert('You turn on email notitication for this ticket.');
	          	}
	        });
		}
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var showChar = 60;
		var ellipsestext = "...";
		$('#messagecont').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span>';
				$(this).html(html);
			}
		});
	});
</script>

<?php if(!$tickets){

	$blk_lot = "";
	$status = "";

	$project = "";
	$blklot = "";
	$larea = "";
	$model = "";
	$farea = "";
	$movein = "";

 } else {

	foreach($tickets as $rows){

		$getnotif = $rows['t_notif']; 
		$ticket_id = $rows['t_id'];
		$so = $rows['t_so'];
		$blk_lot = $rows['t_blklot'];
		switch($rows['t_status']){
			case 1: $status="Open"; break;
			default: $status="Not identified";						
		}

		$select=""; $table="ZHOA_BUYER_PROF"; $where="REFNO = '".$blk_lot."' AND VBELN = '".$so."' AND ACTIVE = 1";
		$get_blklot = $this -> Main -> select_data_where($select, $table, $where);		
		if(!$get_blklot){

			$project = "";
			$blklot = "";
			$larea = "";
			$model = "";
			$farea = "";
			$movein = "";

		 } else {

			foreach($get_blklot as $rb){

				$rb['XWETEXT'] == " " || $rb['XWETEXT'] == "" ? $project = "&nbsp;": $project = $rb['XWETEXT'] ;
				$rb['REFNO'] == " " || $rb['REFNO'] == "" ? $blklot = "&nbsp;": $blklot = $rb['REFNO'] ;
				$rb['LOT_SIZE'] == " " || $rb['LOT_SIZE'] == "" ? $larea = "&nbsp;": $larea = $rb['LOT_SIZE'] ;
				$rb['ARKTX'] == " " || $rb['ARKTX'] == "" ? $model = "&nbsp;": $model = $rb['ARKTX'] ;
				$rb['FLR_AREA'] == " " || $rb['FLR_AREA'] == "" ? $farea = "&nbsp;": $farea = $rb['FLR_AREA'] ;
				if($rb['MOVEIN_DATE'] == 0){
					$movein = "";
				} else {
					$movein = strtotime($rb['MOVEIN_DATE']);
					$movein = date('m/d/Y', $movein);
				}
			}
		}
	}
} ?>
<!-- HIDDEN ITEMS -->
<input type="hidden" id="ticket_id" value="<?=$ticket_id?>">
<!-- END OF HIDDEN ITEMS -->

<form action="<?=base_url()?>portal/new_request" method="POST" enctype="multipart/form-data">
	<div class="mod_nfilter signup-form" style="text-align: left;text-shadow:none">

	  <!-- <div class="container"> -->
		<div class="row">
	  		<div class="container" style="text-align: left;">
				<div class="col-md-12"><?=$notification?></div>
				<div class="col-md-4">
					<div class="col-md-12">
						<div class="col-md-5">
							<h5><strong>Ticket Number</strong></h5>
						</div>

						<div class="col-xs-6 col-sm-2 col-md-7">
							<h5><?=str_pad($ticket_id,10,'0', STR_PAD_LEFT)?></h5>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-md-offset-4">
					<div class="col-md-12">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<h5><strong>Status: </strong></h5>
						</div>
						<div class="col-md-7">
							<h5><?=$status?></h5>
						</div>
					</div>
				</div>
				<!-- <div class="col-xs-12 col-sm-2 col-md-1"><strong><input class="btn btn-success input-sm btn-sm form-control" type="submit" value="New"></strong></div> -->
				<!-- <div class="col-xs-12 col-sm-2 col-md-1"><strong><input class="btn btn-danger input-sm btn-sm form-control" type="button" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_request'"></strong></div> -->

				<!-- <div class="col-sm-3 col-md-1">
				    <button type="button" class="btn btn-success form-control input-sm btn-sm" onclick="add_concern()">New</button>
				</div>

				<div class="col-sm-3 col-md-1">
				    <input type="button" class="btn btn-danger form-control input-sm btn-sm" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_request'";>
				</div> -->

			</div>

		</div>

	  <!-- </div> -->

		<div class="container">

			<div class="row">
				<div class="container" id="bl_loader">
					<div class="col-md-4">
						<div class="col-md-12">
							<div class="col-xs-6 col-sm-2 col-md-5">
								<label> Project </label>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-7"><?=$project?></div>
						</div>
						<div class="col-md-12">
							<div class="col-xs-6 col-sm-2 col-md-5">
								<label> Model </label>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-7"><?=$model?></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="col-md-12">
							<div class="col-xs-6 col-sm-2 col-md-5">
								<label> Blk/Lot </label>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-7"><?=$blklot?></div>
						</div>
						<div class="col-md-12">
							<div class="col-xs-6 col-sm-2 col-md-5">
								<label> Flr Area </label>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-7"><?=$farea?></div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="col-md-12">
							<div class="col-xs-6 col-sm-2 col-md-5">
								<label> Lot Area </label>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-7"><?=$larea?></div>
						</div>
						<div class="col-md-12">
							<div class="col-xs-6 col-sm-2 col-md-5">
								<label> Move In Date </label>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-7"><?=$movein?></div>
						</div>
					</div>
				</div>

			</div>

		</div>
</div>

		<div class="container table-responsive" style="min-width: 300px; overflow-x: auto; overflow-y: hidden; border: none;">

			<?php if(!$concerns){ ?>

				<div class="row">
					
					<div class="col-md-2 mod_cols"> </div>
					<div class="col-md-8 mod_cols">
						NO DATA
					</div>
					<div class="col-md-2 mod_cols"> </div>
				
				</div>

			<?php } else { ?>

				<div class="panel panel-dark table-responsive">
					<table class="table panel-heading">
						<tr>
							<th style="width: 15%;">Request Type</th>
							<th style="width: 20%;">Sub Type</th>
							<th style="width: 35%;">Message</th>
							<th style="width: 10%;">Status</th>
							<th style="width: 10%;">Date Reply</th>
							<th style="width: 10%;">Reply By</th>
						</tr>
					</table>

					<div id="accordion" class="panel-group" style="margin:5px 0 0 0">

					<?php $val = 0; ?>
					<?php foreach($concerns as $row){ $val++; $c_ticket_id = $row['c_ticket_id']; ?>

					<?php switch($row['c_is_active']){ 
						case 1: 
							$status = "Open";
							break;
						case 2:
							$status = "Closed";
							break;
						case 3:
							$status = "Closed";
							break;
						default: 
							$status = "Unrecognized";
							break;
					} ?>
					<?php $rep_date = strtotime($row['c_reply_date']); ?>
					<script type="text/javascript">
						$(document).ready(function(){
							reply(<?=$row['c_id']?>);
						})
					</script>
						<div class="panel panel-dark" style="margin-bottom:5px">
							<div class="panel panel-heading" style="cursor:pointer" data-parent="#accordion" data-toggle="collapse" data-target="#reply<?=$val?>" onclick="reply(<?=$row['c_id']?>)">
								<table class="table">
									<tr class="collapsed" style="margin-bottom: 0px;">
										<td style="width: 10%; padding:0 5px"><?=$row['rt_desc']?></td>
										<td style="width: 25%; padding:0 5px"><?=$row['st_desc']?></td>
										<td id="messagecont" style="width: 35%; padding:0 5px"><?=$row['c_message']?></td>
										<td style="width: 10%; padding:0 5px"><?=$status?></td>
										<td style="width: 10%; padding:0 5px"><?=date('M d, Y', $rep_date)?></td>
										<td style="width: 10%; padding:0 5px"><?=$row['c_reply_by']?></td>
									</tr>
								</table>
							</div>	
							<div id="reply<?=$val?>" class="collapse">
								<div class="panel-body" style="border-top: 1px solid #eee; border-bottom: 1px solid #eee;">

									<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>

								    <div class="row load_reply" id="load_reply<?=$row['c_id']?>" style="">
								    		
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
				
				<?php $getnotif == 1 ? $check = "checked" : $check = ""; ?>
				<div class="row">
					<div class="col-md-12" id="load_cbox">
						<input type="checkbox" id="email_cbox" name="getinfo" value="<?=$getnotif?>" <?=$check?> onclick="tick_cbox(<?=$c_ticket_id?>)"> Email me notifications for this ticket.
					</div>
				</div>
			<?php } ?>
			
		</div>

</form>