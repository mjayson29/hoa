<div class="mod_cover"></div>

<section class="mod_profile">

  <?php $this -> load -> view('templates/myprofile'); ?>
  
  <hr class="colorgraph">
  
  <div class="container">

    <?php $this->load->view('templates/nav-profile');?>

    <br>

  </div>


  <?php if(!$get_profile){} else { ?>

      <?php foreach($get_profile as $row){ ?>

      <?=$notification?>

      <form method="POST" action="<?=base_url()?>portal/edit_profile">

        <div class="default-bg">
        
          <div class="container">

            <div class="row">

              <div class="col-md-2"></div>
              <div class="col-sm-3 col-md-2">
                <small>Customer Code</small>
                <br>
                <input type="text" class="form-control" readonly value="<?=$row['bi_cust_no']?>" required>
              </div>

              <div class="col-sm-3 col-md-2">
                <small>Username</small>
                <br>
                <input type="text" class="form-control" readonly value="<?=$uname?>" required>
              </div>

            </div>

            <div class="row">

              <div class="col-md-2"></div>
              <div class="col-sm-6 col-md-4">
                <small>Address 1</small>
                <br>
                <input type="text" class="form-control" value="<?=$row['bi_add1']?>" name="add1" required>
              </div>

              <div class="col-sm-6 col-md-4">
                <small>Address 2</small>
                <br>
                <input type="text" class="form-control" value="<?=$row['bi_add2']?>"  name="add2" required>
              </div>
              <div class="col-md-2"></div>

            </div>

            <div class="row">

              <div class="col-md-2"></div>
              <div class="col-sm-6 col-md-4">
                <small>City/Province</small>
                <br>
                <input type="text" class="form-control" value="<?=$row['bi_province']?>" name="province" required>
              </div>
              <div class="col-sm-6 col-md-4">
                <small>Zip Code</small>
                <br>
                <input type="text" class="form-control" value="<?=$row['bi_zip']?>" name="zip" required>
              </div>

            </div>

            <div class="row">

              <div class="col-md-2"></div>
              <div class="col-sm-6 col-md-4">
                <small>Contact</small>
                <br>
                <input type="text" class="form-control" value="<?=$row['bi_contact']?>" name="contact" required>
              </div>

              <div class="col-sm-6 col-md-4">
                <small>Email</small>
                <br>
                <input type="email" class="form-control" value="<?=$row['bi_email']?>" name="email" required>
              </div>

            </div>

            <div class="row">

              <div class="col-md-2"></div>
              <div class="col-sm-2 col-md-2">
                <small>Units Owned</small>
                <br>
                <input type="text" class="form-control" readonly value="">
              </div>

            </div>

            <br>

            <div class="row">

              <div class="col-md-2"></div>
              <div class="col-sm-6 col-md-2">
                <input type="submit" class="btn btn-primary btn-sm form-control" value="Update" name="update">
              </div>

              <div class="col-sm-6 col-md-2">
                <input type="button" class="btn btn-danger btn-sm form-control" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_profile'">
              </div>
              <div class="col-md-3"></div>

            </div>
            
          </div>

        </div>

      </form>

      <?php } ?>

  <?php } ?>
  
</section> 