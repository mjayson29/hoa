<script type="text/javascript">

    $(document).ready(function(){

      var fileInput = $('#userfile_l');
      var propid_l = $('#get_id').val();
      // button = $('#upload');
      fileInput.on('change', function(){
        var files = fileInput.prop('files');

        // No file was chosen!
        /*if(files.length == 0) {
          alert('Please choose a file to upload!');
          return false;
        }*/
        // Create a new FormData object, append yung file para makuha mo yung $_POST
        var fd = new FormData();
        var attachment = fd.append('userfile', files[0]);

        // var upload = confirm('Are you sure to change profile image?');
        
        // if(upload == true){   
        // Upload the file m,

          $.ajax({
            url: '<?=base_url()?>portal/upload_prop/'+propid_l,
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){
              document.getElementById("prop_loader"+propid_l).innerHTML = data;  
            }

          });
      });
    });

</script>
	<?php if(!$get_property){ ?>
		<section class="container mod_prop" style="color: #fff;">
			<div class="row mod_bg">
				<div class="col-md-12">
					<h3 style="text-align: center;">NO PROPERTY HAS BEEN RETRIEVED.</h3>
				</div>
			</div>
		</section>
	<?php } else { ?>
		<section class="container mod_prop">
	   		<?php foreach($get_property as $row){ ?>
	   		<?php 
	   			$prop_img1 = $row['PROP_IMG1'];
	   			$prop_img2 = $row['PROP_IMG2'];
	   			// if($prop_img1 == "" || !file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$prop_img1)){
	   			if($prop_img1 == "" || !file_exists(APPPATH."../assets/img/properties/".$prop_img1)){
				 	$prop_img1 = "no-image.jpg";
	   			}
	   			// if($prop_img2 == "" || !file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$prop_img2)){
	   			if($prop_img2 == "" || !file_exists(APPPATH."../assets/img/properties/".$prop_img2)){
					$prop_img2 = "no-image.jpg";
	   			}
	   		 ?>
	      		<div id="alert"></div>
				<div class="row mod_bg" style="border-radius:5px">
					<div class="col-md-4 load_img_left" id="prop_left">
						<img src="<?=base_url()?>assets/img/properties/<?=$prop_img1?>" width="100%" alt="<?=$prop_img1?>" class="img-thumbnail">
					</div>
					<div class="col-md-4 load_img_right">
						<img src="<?=base_url()?>assets/img/properties/<?=$prop_img2?>" width="100%" alt="<?=$prop_img2?>" class="img-thumbnail">
					</div>
			    	<?php $select="XWETEXT"; $table="ZHOA_PROJECTS"; $where="SWENR = ".$row['SWENR'];?>
				    <?php $get_projname = $this -> Main -> select_data_where($select, $table, $where); ?>
				    <?php foreach($get_projname as $gp){ $projname = $gp['XWETEXT']; } ?>
				    <div class="mod_home_icons">
					    <div class="col-md-4">
					      <div class="mod_thumbnail" style="margin:0;height:auto">
					        <div class="caption" style="padding: 0px;">
					          <a href="<?=base_url()?>portal/single_properties/<?=$row['VBELN']?>"><h4><strong>House Details</strong></h4></a>
					          <br>
					          	<div class="img_details">
									<div class="col-md-12 nop4d">
										<div class="col-md-4 nop4d">
											<label class="custom_label">SO Number</label>
										</div>
										<div class="col-md-8 nop4d">
											<span><?=$row['VBELN']?></span>
										</div>
									</div>
									<div class="col-md-12 nop4d">
										<div class="col-md-4 nop4d">
											<label class="custom_label">Project Name</label>
										</div>
										<div class="col-md-8 nop4d">
											<span><?=$projname?></span>
										</div>
									</div>
									<div class="col-md-12 nop4d">
										<div class="col-md-4 nop4d">
											<label class="custom_label">Blk/Lot Unit #</label>
										</div>
										<div class="col-md-8 nop4d">
											<span><?=$row['REFNO']?></span>
										</div>
									</div>
									<div class="col-md-12 nop4d">
										<div class="col-md-4 nop4d">
											<label class="custom_label">House Model</label>
										</div>
										<div class="col-md-8 nop4d">
											<span><?=$row['ARKTX']?></span>
										</div>
									</div>
									<div class="col-md-12 nop4d">
										<div class="col-md-4 nop4d">
											<label class="custom_label">Floor Area</label>
										</div>
										<div class="col-md-8 nop4d">
											<span><?=$row['FLR_AREA']?></span>
										</div>
									</div>
									<div class="col-md-12 nop4d">
										<div class="col-md-4 nop4d">
											<label class="custom_label">Lot Area</label>
										</div>
										<div class="col-md-8 nop4d">
											<span><?=$row['LOT_SIZE']?></span>
										</div>
									</div>
						        </div>
					          <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
					        </div>
					      </div>
					    </div>
					</div>
				</div>
				<br>
			<?php } ?>
		</section>
	<?php } ?>