<div class="mod_cover"></div>

<section class="mod_profile">

  <?php $this -> load -> view('templates/myprofile'); ?>

  <hr class="colorgraph">
  
  <div class="container">

    <?php $this->load->view('templates/nav-profile');?>

    <br>

  </div>

  <?=$notification?>

  <div class="default-bg">

    <div class="container">

      <div class="row">

        <div class="col-sm-3 col-md-4"></div>

        <div class="col-sm-6 col-md-4">
          <?php if(!$sec_update){ } else {
                foreach($sec_update as $row){ 
                  $sec_result = strtotime($row['bi_csec_date']);
                }
              }

              $from = new DateTime(date('Y-m-d', $sec_result));
              $to   = new DateTime('today');
              $csec_date = $from->diff($to)->d;

              if($csec_date == 0){

                $result = "Today";

              } else if($csec_date == 1){

                $result = "Yesterday";

              } else if($csec_date >= 2 and $csec_date <= 6) {

                $result = $csec_date. " days ago.";

              } else {

                $result = date('F d, Y h:i:s a', $sec_result);

              }

          ?>
          <strong>Sec. question last update: </strong> <?=$result?><??>
        
        </div>

      </div>
    
    </div>

    <?php if(!$get_profile){ } else { ?>
      
      <?php foreach($get_profile as $row){ ?>

        <?php $select = "sq_desc, sq_id"; $table = "ZHOA_secquestion"; ?>
        <?php $squestion = $this -> Main -> select_data($select, $table); ?>
        
        <div class="container">

          <form method="POST" action="<?=base_url()?>portal/update_security">

          <div class="row">

            <div class="col-sm-3 col-md-4"></div>

            <div class="col-sm-6 col-md-4">
              <small>Security Question 1</small>
              <br>
              <select class="form-control" name="sec1" id="sec1">
                
                <?php foreach($squestion as $row1){ ?>
                  <option value="<?=$row1['sq_id']?>" <?php if($row1['sq_id'] == $row['bi_sec1']){ echo "selected"; } ?>><?=$row1['sq_desc']?></option>
                <?php } ?> 
              </select>
            </div>

          </div>


          <div class="row">

            <div class="col-sm-3 col-md-4"></div>

            <div class="col-sm-6 col-md-4">
              <small>Answer 1</small>
              <br>
              <input type="text" class="form-control" value="<?=$row['bi_ans1']?>" name="ans1" id="ans1">
            </div>

          </div>

          <div class="row">

            <div class="col-sm-3 col-md-4"></div>

            <div class="col-sm-6 col-md-4">
              <small>Security Question 2</small>
              <br>
              <select class="form-control" name="sec2" id="sec2">
                
                <?php foreach($squestion as $row1){ ?>
                  <option value="<?=$row1['sq_id']?>" <?php if($row1['sq_id'] == $row['bi_sec2']){ echo "selected"; } ?>><?=$row1['sq_desc']?></option>
                <?php } ?> 
              </select>
            </div>

          </div>

          <div class="row">

            <div class="col-sm-3 col-md-4"></div>

            <div class="col-sm-6 col-md-4">
              <small>Answer 2</small>
              <br>
              <input type="text" class="form-control" value="<?=$row['bi_ans2']?>" name="ans2" id="ans2">
            </div>

          </div>

        <br>

        <div class="row">

          <div class="col-sm-3 col-md-4"></div>

          <div class="col-sm-3 col-md-2">
            <input type="submit" class="btn btn-primary btn-sm form-control" value="Save" name="save">
          </div>

          <div class="col-sm-3 col-md-2">
            <input type="button" class="btn btn-danger btn-sm form-control" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_profile'">
          </div>

        </div>

        </form>

      </div>
      <?php } ?>

    <?php } ?>

  </div>
  
</section> 