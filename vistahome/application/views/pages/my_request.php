<script type="text/javascript">
	function paginate(val){

        $('#loader').hide();
        $('#loading').show();

        $.ajax({
        type:"get",
        url: "<?=base_url()?>portal/my_request_load/"+val,
            success: function(data){
                $('#loader').html(data);
                $('#loader').slideDown('fast');
                $('#loading').hide();
            }
        });

    }
</script>
<div class="mod_nfilter signup-form" style="text-align: left;text-shadow:none">

  <div class="container">

  	<div class="row">

			<div class="col-md-1"></div>
			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="form-group">
					<small>From </small>
					<br>
					<input type="date" class="form-control input-sm" placeholder="mm/dd/yyyy" id="date_created_start">
				</div>
			</div>

			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="form-group">
					<small>To</small>
					<br>
					<input type="date" class="form-control input-sm" placeholder="mm/dd/yyyy" id="date_created_end">
				</div>
			</div>

			<!-- <div class="col-md-12"> </div> -->

			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="form-group">
					<small>Status</small>
					<br>
					<select class="form-control input-sm" id="status">
						<option value="">--Select Status--</option>
						<option value="1">Open</option>
						<option value="2">Closed</option>
					</select>
					<!-- <input type="text" class="form-control input-sm" placeholder="Status"> -->
				</div>
			</div>

			<!-- <div class="col-xs-4 col-sm-4 col-md-2">
				<div class="form-group">
					<small>Type</small>
					<br>
					<select class="form-control input-sm" >
						<option value="">--Select Request--</option>
						<?php if(!$request_type){} else {?>
							<?php foreach($request_type as $rt){?>
								<option value="<?=$rt['rt_id']?>"><?=$rt['rt_desc']?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div> -->

			<div class="col-xs-6 col-sm-4 col-md-2">
				<div class="form-group">
					<small>Keyword</small>
					<br>
					<input type="text" class="form-control input-sm" placeholder="Search" id="keyword">
				</div>
			</div>

			<div class="col-xs-6 col-sm-2 col-md-1">
				<div class="form-group">
					<br>
					<input type="button" onclick="search_request()" class="form-control btn btn-primary btn-sm input-sm" value="Search">
				</div>
			</div>

			<div class="col-xs-6 col-sm-2 col-md-1">
				<div class="form-group">
					<br>
					<button type="button" class="form-control btn btn-primary btn-sm input-sm" onclick="window.location.href='<?=base_url()?>portal/new_request'">New</button>
				</div>
			</div>

			<div class="col-md-1"></div>

		</div>

    </div>

</div>

<div class="request">
	
	<div class="container">

		<div class="row">

			<div class="col-md-1"></div>
			<div class="col-md-10">

				<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
				
				<div class="panel panel-dark" id="loader">

					<div class="panel-heading">List of Request</div>

					<?php if(!$tickets){ ?>

						<div class="tab-field"><h3><span class="glyphicon glyphicon-exclamation-sign"></span> No Data</h3></div>

					<?php } else { ?>

					<div style="overflow-x: auto; ">
					
						<table class="table" style="white-space: nowrap;">

							<tr>
								<th style="width: 100px;">Ticket #</th>
								<th style="width: 100px;">Blklot</th>
								<th style="width: 100px;">Date Posted</th>
								<th style="width: 100px;">Concern(s)</th>
								<th style="width: 100px;">Status</th>

							</tr>

							<!-- <tr>
								<td style="width: 100px;">Ticket #</td>
								<td style="width: 100px;">2000000009</td>
								<td> Mar 23, 2015; 12:30pm</td>
								<td style="width: 100px;"><?php $msg = "the quick brown fox jumps over the lazy dog the quick brown fox jumps over the lazy dog"; 
								if(strlen($msg) > 25){ echo substr($msg, 0,24)."..."; }?></td>
								<td>Support</td>
								<td>Mar 23, 2015; 12:30pm</td>
								<td style="width: 100px;">Closed</td>
							</tr> -->
							<?php foreach($tickets as $row){ $date_created = strtotime($row['t_date_created']);?>
								<?php $select = "c_rtype"; $table = "ZHOA_concerns"; $where = "c_ticket_id = ".$row['t_id']." AND c_is_deleted = 0"; ?>
								<?php $concerns = $this -> Main -> select_data_where($select, $table, $where); ?>
								<?php $select = "rt_desc, rt_id"; $table = "ZHOA_request_type"; $where = "rt_is_deleted = 0"; ?>
								<?php $request = $this -> Main -> select_data_where($select, $table, $where); ?>
									<?php $concern_list=""; ?>
									<?php if(!$concerns){ } else { ?>
										<?php foreach($concerns as $c){ ?>
											<?php foreach($request as $rtype){ ?>

												<?php if($rtype['rt_id'] == $c['c_rtype']){ ?>

													<?php $concern_list.=$rtype['rt_desc'].", ";?>

												<?php }?>

											<?php } ?>

										<?php } ?>
									<?php } ?>
									<tr onclick="window.location.href='<?=base_url()?>portal/view_request/<?=str_pad($row['t_id'], 10, 0, STR_PAD_LEFT)?>'" class="mod_tr">
										<td><?=str_pad($row['t_id'], 10, '0', STR_PAD_LEFT)?></td>
										<td><?=$row['t_blklot']?></td>
										<td><?=date('F d, Y h:i:s A', $date_created)?></td>
										<td>
											<?=substr($concern_list, 0, -2)?>
										</td>
										<td><?php if($row['t_status'] == 1){ echo "Open"; } else { echo "Closed"; } ?></td>
									</tr>
							<?php } ?>

						</table>

					</div>

					<?php }?>

					<div class="col-md-12 pagination-dark" style="text-align: center;"><?=$ticket_links?></div>	

				</div>

			</div>

			<div class="col-md-1"></div>


		</div>

	</div>


</div>

