<div class="mod_cover"></div>

<section class="mod_profile">

  <?php $this -> load -> view('templates/myprofile'); ?>

  <hr class="colorgraph">
  
  <div class="container">

    <?php $this->load->view('templates/nav-profile');?>

    <br>

  </div>

  <form action="<?=base_url()?>portal/change_password" method="POST">
    
    <?=$notification?>

  <div class="default-bg">
  
    <div class="container">

      <div class="row">

        <div class="col-md-4"></div>

        <div class="col-sm-3 col-md-4">
          <?php if(!$pword_update){ } else {
                foreach($pword_update as $row){ 
                  $pword_result = strtotime($row['b_cpass_date']);
                }
              }

              $from = new DateTime(date('Y-m-d', $pword_result));
              $to   = new DateTime('today');
              $cpass_date = $from->diff($to)->d;

              if($cpass_date == 0){

                $result = "Today";

              } else if($cpass_date == 1){

                $result = "Yesterday";

              } else if($cpass_date >= 2 and $cpass_date <= 6) {

                $result = $cpass_date. " days ago.";

              } else {

                $result = date('F d, Y h:i:s a', $pword_result);

              }

              //$cpass_date = date('F d, Y h:i A', $pword_result);

          ?>
          <strong>Password last updated: </strong> <?=$result?><??>
        
        </div>

      </div> 

      <div class="row">

        <div class="col-md-4"></div>

        <div class="col-sm-3 col-md-4">
          <small>Current Password</small>
          <br>
          <input type="password" class="form-control" name="curpword" required title="Please enter your current password first.">
        </div>

      </div>


      <div class="row">

        <div class="col-md-4"></div>

        <div class="col-sm-3 col-md-4">
          <small>New Password</small>
          <br>
          <input type="password" class="form-control" name="newpword" pattern=".{8,24}" required title="Password must 8 to 24 characters.">
        </div>

      </div>

      <div class="row">

        <div class="col-md-4"></div>

        <div class="col-sm-3 col-md-4">
          <small>Retype Password</small>
          <br>
          <input type="password" class="form-control" name="repword" pattern=".{8,24}" required title="Password must 8 to 24 characters.">
        </div>

      </div>

      <br>

      <div class="row">

        <div class="col-md-4"></div>

        <div class="col-sm-3 col-md-2">
          <input type="submit" class="btn btn-primary btn-sm form-control" value="Save" name="save">
        </div>

        <div class="col-sm-3 col-md-2">
          <input type="button" class="btn btn-danger btn-sm form-control" value="Cancel" onclick="window.location.href='<?=base_url()?>portal/my_profile'">
        </div>

      </div>

    </div>

  </div>

  </form>
  
</section> 