<script type="text/javascript">

  $(function(){
   $('.home_login_attempts').hide();
   $('.home_announcements').hide();
   $('.home_calendar').hide();
  });

  $(document).ready(function(){
    $(".glyphicon").click(function(){
        $(this).toggleClass("glyphicon-minus");
    });
  });

  function show(val){

    if(val == 1){
      $('.home_login_attempts').slideToggle(200);
    }

    if(val == 2){
      $('.home_announcements').slideToggle(200);
    }

    if(val == 3){
      $('.home_calendar').slideToggle(200);
    }
  }

  function qa(val){
    if(val!=""){
      $('#qva').hide();
      $.ajax({
        url: "<?=base_url()?>portal/quick_view",
        type: "POST",
        data: {val:val},
        success: function(data){
          $('#qva').html(data);
          $('#qva').slideDown('fast');
        }
      });
    }
  }

</script>

<?php 

  if(!$logs){
    $last_login ="";
  } else {
    foreach($logs as $g){
      $get_log = $g['tl_date'];
    }

    $last_login = strtotime($get_log);

    $select=""; $table="ZHOA_attempts"; $where="at_username ='".$uname."' AND at_date > '".date('Y-m-d H:i:s', $last_login)."'";
    $attempts = $this -> Main -> count_select_where($select, $table, $where);

    if(!$attempts){
      $attempt_count = 0;
    } else {
      $attempt_count = $attempts;
    }
  }

?>

<!-- <div class="home_banner_design"></div> -->

<section class="container">

  <div class="row mod_home_intro">
    <div class="col-xs-3 col-sm-2 col-md-1 mod_intro2">
      <br><br>
      <img src="<?=base_url()?>assets/img/logos/Logo02-02.png">
    </div>

    <div class="col-xs-7 col-sm-5 col-md-3 mod_intro2">
      <img src="<?=base_url()?>assets/img/logos/vistahome.png">
    </div>
  </div>

</section>

<section class="container mod_home">
  <div class="row">
    <div class="col-md-3">
      <div class="row mod_home_icons">
        <div class="col-sm-12 col-md-12">
          <div class="thumbnail">
            <!-- <span class="glyphicon glyphicon-warning-sign" style="color: #eea236;"></span> -->
            <div class="caption">
              <h5><span>Login Attempts</span> <span class="glyphicon glyphicon-plus" onclick="show(1)"></span></h5>
              <!-- <hr> -->
              <div class="home_login_attempts">

                <?php if($last_login !=""){ ?>
                  <small>Your last login was <strong><?=date('F d, Y h:i:s A', $last_login)?></strong>. You have <strong><?=$attempt_count?></strong> invalid login attempts since last login.</small>
                <?php } else { ?>
                  <small>Welcome to Homeowner's Portal, this is your first login.</small>
                <?php } ?>

              </div>
              <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
            </div>
          </div>
        </div>
      </div>
      <div class="row mod_home_icons">
        <div class="col-sm-12 col-md-12">
          <div class="thumbnail">
            <!-- <span class="glyphicon glyphicon-info-sign" style="color: #337ab7;"></span> -->
            <div class="caption">
              <h5><span>Announcements</span> <span class="glyphicon glyphicon-plus" onclick="show(2)"></span></h5>
              <!-- <hr> -->
                <div class="home_announcements">

                  <br>
                
                  <?php if(!$get_announcements){ ?>

                   <p>No Posted Announcements</p>

                  <?php } else { ?>
                    
                    <?php foreach($get_announcements as $a){ $date_posted = strtotime($a['a_dposted']); ?>
                      <div class="col-md-12" style="border: solid 1px; margin-bottom:5px">
                        <div style="padding-top:5px">
                          <span><a href="javascript:;" onclick="qa(<?=$a['a_id']?>)"><?php if(strlen($a['a_subject']) > 25){ echo ucwords(substr($a['a_subject'], 0,25)."..."); } else { echo ucfirst($a['a_subject']); }?></a></span>
                        </div>
                        <hr / style="margin: 5px 0px">
                        <div>
                          <small>Date Posted: <?=date('m/d/Y', $date_posted)?></small><br />
                          <small>Posted By: <?=$a['postedby']?></small>
                        </div>
                      </div>

                      <!-- <hr> -->

                    <?php } ?>
                  <a href="<?=base_url()?>portal/announcement_page"> View All </a>

                <?php } ?>
                </div>
              <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
            </div>
          </div>
        </div>
      </div>
      <div class="row mod_home_icons">
        <div class="col-sm-12 col-md-12">
          <div class="thumbnail">
            <!-- <span class="glyphicon glyphicon-home"></span> -->
            <div class="caption">
              <h5><span>Appointments</span> <span class="glyphicon glyphicon-plus" onclick="show(3)"></span></h5>
              <!-- <hr> -->
              
              <div class="home_calendar">

               <?=$calendar?>

              </div>
              <!-- </div> -->
              <!-- <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="qva" class="col-md-8 col-md-offset-1">

    </div>
  </div>
</section>

<script src="<?=base_url()?>assets/styles/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
<script src="<?=base_url()?>assets/styles/js/bootstrap.min.js"></script>