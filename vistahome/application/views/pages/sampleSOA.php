<?php
	$i = 0;
	if(count($blklot) == 1){
		foreach ($blklot as $key) {
			$proj = $key['XWETEXT'];
			$blk = $key['REFNO'];
			$so = $key['VBELN'];
		}
	}
?>

<script type="text/javascript">
	function get_soa(val){
		$.ajax({
	        type:"post",
	        url: "<?=base_url()?>portal/soa_date/",
	        data : { so : val },
	        success: function(data){
	            $('#soa_period').html(data);
            }
        });
	}

	// $(function() {
	// 	$('button[type=submit]').click(function(){
	// 		if($('#prop').val() != '' && $('#soa_period').val() != '') {
				
	// 			var prop = $('#prop').val();
	// 			var soa = $('#soa_period').val();
	// 			$.ajax({
	// 		        type:"get",
	// 		        url: "<?=base_url()?>portal/viewSOA/",
	// 		        data : { 
	// 		        	soa_period : soa,
	// 		        	prop : prop
	// 		        },
	// 		        success: function(data){
	// 		            $('#pdf_view').html(data);
	// 	            }
	//         	});
	// 		} 
	// 	});
	// });

	// function qwe() {

		$(function(){
			 $("#submit").click(function(e) {
				var prop = $('#prop').val();
				var soa = $('#soa_period').val();
				// $("#plugin").load('http://127.0.0.1:8080/vistahome/portal/viewSOA/?prop="+prop+"&soa_period="+soa+"&submit=Submit');
				// var parent = $('#pdf_view').parent();
				// var newElement = "<embed id='plugin' width='650' height='815' type='application/pdf'  scr='http://127.0.0.1:8080/vistahome/portal/viewSOA/?prop="+prop+"&soa_period="+soa+"&submit=Submit'>";
				$('#plugin').attr('src', "http://127.0.0.1:8080/vistahome/portal/viewSOA/?prop="+prop+"&soa_period="+soa+"&submit=Submit");
				// $('#plugin').remove();
				// parent.append(newElement);
				e.preventDefault();
			});
		});

	// }
</script>

<section>
	<div class="container" style="background:rgba(0, 0, 0, 0.8) none repeat scroll 0 0; color: white; padding-bottom: 10px">
		<h1>Statement of Accounts</h1>
		<span>Select your Property/ Blklot and Statement Date below. </span><br />
		<form  action="" method="get" onsubmit="return false;" >
			<div class="col-md-12" style="padding: 20px 0">
				<div class="col-md-5 col-md-offset-1">
					<div class="col-md-5">
						<label>Project / Blk/Lot</label>
					</div>

					<div class="col-md-7">	
						<select id="prop" class="form-control input-sm" onchange="get_soa(this.value)" name="prop" >
							<option value="">Select Property</option>
					<?php if($blklot){
						foreach($blklot as $bl){ ?>
							<option value="<?=$bl['VBELN']?>">[<?=$bl['REFNO']?>] <?=$bl['XWETEXT']?></option>
						<?php }
						} ?>
						</select>
					</div>
				</div>
				<div class="col-md-5">
					<div class="col-md-5">
						<label>Statement Date</label>
					</div>
					<div class="col-md-7">
						<select id="soa_period" class="form-control input-sm" name="soa_period">
							<option value="">Select Statement Date</option>
						</select>
					</div>
				</div>
				<div class="col-md-1" style="text-align:center">
					<!-- <button type="button" class="btn btn-success form-control input-sm btn-sm" onclick="gen_soa()">Submit</button> -->
					<button type="" value="Submit" class="btn btn-primary btn-sm" name="submit" id="submit">Submit</button>
				</div>
			</div>

			<!-- <div id="pdf_view" class="col-md-12">
				<embed id="soa" width="650" height="815" type="application/pdf" scr="http://ims.skycable.com/index.php/billsonline/soa?f=%2Fl4Baty2cSiVwqtGa18hdST7DjB9YDPqVZMptQPC78Qffh1DvMJzd1zZScYXsR9IhTkxU3o1kNygnuBADzhLxjQFfTDjumnExeMoyuAA2CJEY4U2RHAWHIgYDa%2FJnxnb">
			</div> -->

			<div class="col-md-12" style="text-align:center">
				<embed id="plugin" class="soa" width="1000" height="815" type="application/pdf" src="" name="plugin"> 
				
			</div>
		</form>
	</div>
</section>