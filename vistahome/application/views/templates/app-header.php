<!DOCTYPE html>
<html class="full" lang="en">

<head>  
    <title>Vista Home Portal</title>
    <!-- <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-8"> -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?=base_url()?>assets/img/logos/icon1.png">

    <!-- Bootstrap Core CSS -->
    

    <!-- Latest compiled and minified CSS -->
    <link href="<?=base_url()?>assets/styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?=base_url()?>assets/styles/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=base_url()?>assets/styles/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <link href="<?=base_url()?>assets/styles/css/full-slider.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/styles/css/column5.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/styles/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/styles/js/onepagescroll.min.js"></script>

    <script type="text/javascript">

        function check_username(val){ 

            // val = 1;
            // val = document.getElementById(roles);

            $.ajax({
                type:"get",
                url: "<?=base_url()?>signup/check_username/"+val,
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#loader').html(data);
                }
            });

        }
        
    </script>


    <script type="text/javascript">

        function get_request(val){

            if(val != "" && val == 1){
                $.ajax({
                    type:"get",
                    url: "<?=base_url()?>portal/get_sub_req/"+val,
                    success: function(data){
                        $('#sub_req_type').html(data);
                        $('#sub_type_con').css('display','block');
                    }
                });
            } else {
                $('#sub_type_con').css('display','none');
            }
        }

        function get_blklot(val){ 

            if(val == ""){
                var nodes = document.getElementById("yui").getElementsByTagName('*');
                for(var i = 0; i < nodes.length; i++)
                {
                     nodes[i].disabled = true;
                }
            }else {
                var nodes = document.getElementById("yui").getElementsByTagName('*');
                for(var i = 0; i < nodes.length; i++)
                {
                     nodes[i].disabled = false;
                }
                $.ajax({
                    type:"get",
                    url: "<?=base_url()?>portal/get_blklot/"+val,
                    // url: <?=base_url()?>+"personnel/get_items/"+val,
                    success: function(data){
                        $('#bl_loader').html(data);
                    }
                });
            }
        }

        function add_concern(){ 

            var request = $("#req_type").val();
            var sub_request = $("#sub_req_type").val();
            var message = $("#message").val();
            var attachment = $("#userfile").val();
            var fileInput = $('#userfile');

            
            if(request == 1 || message == ""){
                if(sub_request == "" || message == ""){
                    alert("Yellow fields are required.");
                    return false;
                }
            }else{
                if(message == ""){
                    alert("Yellow fields are required.");
                    return false;
                }
            }

            if($('#sel_blk').length > 0){
                if($('#sel_blk').val() == ""){
                    alert("Yellow fields are required.");
                    return false;
                }
            }
            
            var files = fileInput.prop('files');
            var fd = new FormData();
            fd.append('userfile', files[0]);

            $.ajax({
                url: '<?=base_url()?>portal/request_upload/',
                data: fd,
                contentType:false,
                processData:false,
                type:'POST',
                success: function(data){ 
                }
            });

            var concern = {"request" : request, "sub_request" : sub_request, "message" : message, "attachment" : attachment }

            $("#load_concern").load('<?=base_url()?>portal/add_concern2/', concern );

            $("#req_type").val('');
            $("#sub_req_type").val('');
            $("#message").val('');
            $("#userfile").val('');
        }

        function reset_concern(){

            $.ajax({
                type:"json",
                url: "<?=base_url()?>portal/reset_concern/",
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#load_concern').html(data);
                }
            });

        }

        function attach_file(){ 
            $("#userfile").trigger('click');
            // alert('Attach file');
        }

        function reply(val){

            var get_value = val;
            // alert(get_value);

            $.ajax({
                type:"GET",
                url: "<?=base_url()?>portal/reply/"+get_value,
                
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#load_reply'+get_value).html(data);
                }
            });

            // var loader = document.getElementById('load_reply'+get_value);
            $('#load_reply'+get_value).show();
                // $('#load_reply'+get_value).slideDown('slow');

        }

            function send_reply(val){

                var get_value = val;
                var get_concern = $("#concern_msg"+val).val();
                var ticket_id = $("#ticket_id").val();
                // $('#loading').css('display', 'block');

                if(get_concern == ""){
                    alert('Please populate the reply box.');
                } else {

                    var fileInput = $('#userfile');
                    var files = fileInput.prop('files');
                    var concerns = {

                        "concern" : get_concern,
                        "ticket_id" : ticket_id,
                        "concern_id" : get_value

                    }
                    
                    $('#loading').css('display', 'block');

                    if(files.length != 0){
                        var fd = new FormData();
                        fd.append('userfile', files[0]);
                        fd.append('concern', get_concern);
                        fd.append('ticket_id', ticket_id);
                        fd.append('concern_id', get_value);
                        //data to be sent to server         

                        // $("#load_reply"+get_value).load('<?=base_url()?>support/reply_upload/', concerns );
                        $.ajax({
                            url: '<?=base_url()?>portal/reply_upload/'+get_value,
                            data: fd,
                            contentType: false,
                            processData:false,
                            type:'POST',
                            dataType:'json',
                            success: function(result){
                                $('#load_reply'+get_value).html(result);
                                $('#loading').css('display', 'none');
                                // location.reload();
                            },
                            error:function(x,e) {
                                reply(get_value);
                                $('#loading').css('display', 'none');
                                // location.reload();
                            }
                        });

                    } else {
                        $('#loading').css('display', 'block');
                        // $("#load_reply"+get_value).load('<?=base_url()?>portal/reply/'+get_value, concerns );
                        $.ajax({
                            url: "<?=base_url()?>portal/reply/"+get_value,
                            type:"POST",
                            data: {
                                get_concern: get_concern,
                                ticket_id: ticket_id
                            },

                            // url: <?=base_url()?>+"personnel/get_items/"+val,
                            success: function(data){
                                // reply(get_value);
                                $('#load_reply'+get_value).html(data);
                                $('#load_reply'+get_value).show();
                                $('#loading').css('display', 'none');
                            },
                        });
                    }

                }

                // return false;
            }

        function upload_img_prop(val){ 

            // val = 1;
            // val = document.getElementById(roles);

            $.ajax({
                type:"get",
                url: "<?=base_url()?>portal/upload_img_prop/"+val,
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#load_img').html(data);
                }
            });

        }

        //$(function(){$("#userfile").click();}); 

    </script>

    <script type="text/javascript">

        $(document).ready(function(){

            var fileInput = $('#userfile_l'),
            button = $('#upload_left');
            button.on('click', function(){
        
            var files = fileInput.prop('files');

        // No file was chosen!
            if(files.length == 0) {
                alert('Please choose a file to upload!');
                return false;
            }
        // Create a new FormData object, append yung file para makuha mo yung $_POST

            var fd = new FormData();
            var attachment = fd.append('userfile', files[0]);
            // var SO = $('#SO').value;
            var SO = document.getElementById('SO').value;
        
        // Upload the file 
                $.ajax({
                    url: '<?php echo base_url()?>portal/upload_img_prop_l/'+SO,
                    data: fd,
                    contentType:false,
                    processData:false,
                    type:'POST',
                    success: function(data){
                        document.getElementById("load_img_left").innerHTML = data;  
                    }
                });

                // $("#load_img").load('<?=base_url()?>portal/get_file/', SO );

            });
        });

    </script>

    <script type="text/javascript">

        $(document).ready(function(){

            var fileInput = $('#userfile_r'),
            button = $('#upload_right');
            button.on('click', function(){
        
            var files = fileInput.prop('files');

        // No file was chosen!
            if(files.length == 0) {
                alert('Please choose a file to upload!');
                return false;
            }
        // Create a new FormData object, append yung file para makuha mo yung $_POST

            var fd = new FormData();
            var attachment = fd.append('userfile', files[0]);
            // var SO = $('#SO').value;
            var SO = document.getElementById('SO').value;
        
        // Upload the file 
                $.ajax({
                    url: '<?php echo base_url()?>portal/upload_img_prop_r/'+SO,
                    data: fd,
                    contentType:false,
                    processData:false,
                    type:'POST',
                    success: function(data){
                        document.getElementById("load_img_right").innerHTML = data;  
                    }
                });

                // $("#load_img").load('<?=base_url()?>portal/get_file/', SO );

            });
        });

    </script>

    <script type="text/javascript">

        $(function(){ $('#solid_head').hide(); });

        /*if(<?=$active?> == "contacts"){

            $('#solid_head').show(); 

        }*/

        // $(window).width() <= 568


        /*$(window).scroll(function() {

            if ($(window).scrollTop() > 60 || $(window).scrollTop() != 0) {
                $('#solid_head').show();//slideDown('fast');
                $('#trans_head').hide();//slideUp('fast');
            } else {
                $('#trans_head').show();//slideDown('fast');
                $('#solid_head').hide();//slideUp('fast');
            }
        });*/

    </script>

    <script type="text/javascript">

        function change_pword(val){

            $.ajax({
                url: '<?php echo base_url()?>portal/change_pword/'+val,
                type:'POST',
                success: function(data){
                    document.getElementById("load_transaction").innerHTML = data;  
                }
            });

        }

            function updated_pword(val){

                var curpword = $('#curpword').val();
                var newpword = $('#newpword').val();
                var repword = $('#repword').val();

                if(curpword == "" || newpword == "" || repword == ""){ alert('All fields are required!'); } 

                if(newpword != repword){ alert('New Password and Retype Password didn\'t match. Please Try Again.'); }else {

                    var update_pword = {
                        "curpword" : curpword,
                        "newpword" : newpword,
                        "repword" : repword
                    }

                    $("#load_transaction").load('<?=base_url()?>portal/change_pword/'+val, update_pword );

                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>portal/change_pword/"+val,
                        // url: <?=base_url()?>+"personnel/get_items/"+val,
                        success: function(data){
                            $('#load_transaction'+val).html(data);
                        }
                    });

                }

            }

        function sec_question(val){

            $.ajax({
                url: '<?php echo base_url()?>portal/sec_question/'+val,
                type:'POST',
                success: function(data){
                    document.getElementById("load_transaction").innerHTML = data;  
                }
            });

        }

            function updated_secquestion(val){

                var sec1 = $('#sec1').val();
                var ans1 = $('#ans1').val();
                var sec2 = $('#sec2').val();
                var ans2 = $('#ans2').val();

                if(sec1 == "" || ans1 == "" || sec2 == "" || ans2 == ""){ alert('All fields are required!'); } else {

                    var sec_question = {
                        "sec1" : sec1,
                        "ans1" : ans1,
                        "sec2" : sec2,
                        "ans2" : ans2
                    }

                    $("#load_transaction").load('<?=base_url()?>portal/sec_question/'+val, sec_question );

                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>portal/sec_question/"+val,
                        // url: <?=base_url()?>+"personnel/get_items/"+val,
                        success: function(data){
                            $('#load_transaction'+val).html(data);
                        }
                    });

                }

            }

        function edprofile(val){

            $.ajax({
                url: '<?php echo base_url()?>portal/edprofile/'+val,
                type:'POST',
                success: function(data){
                    document.getElementById("load_transaction").innerHTML = data;  
                }
            });

        }

            function updated_profile(val){

                var add1 = $('#add1').val();
                var add2 = $('#add2').val();
                var province = $('#province').val();
                var zip = $('#zip').val();
                var contact = $('#contact').val();
                var email = $('#email').val();

                if(add1 == "" || add2 == "" || province == "" || zip == "" || contact == "" || email == ""){ alert('All fields are required!'); } else {

                    var edprofile = {
                        "add1" : add1,
                        "add2" : add2,
                        "province" : province,
                        "zip" : zip,
                        "contact" : contact,
                        "email" : email
                    }

                    $("#load_transaction").load('<?=base_url()?>portal/edprofile/'+val, edprofile );

                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>portal/edprofile/"+val,
                        // url: <?=base_url()?>+"personnel/get_items/"+val,
                        success: function(data){
                            $('#load_transaction'+val).html(data);
                        }
                    });

                }

            }

        function prof_cancel(){

            var cancel = confirm("Are you sure to cancel?");

            if(cancel == true){
                window.location.href="<?=base_url()?>portal/my_profile";   
            } else { }

        }

    </script>

    <script type="text/javascript">
        function search_request(){

            var date_created_start = $('#date_created_start').val();
            var date_created_end = $('#date_created_end').val();
            var status = $('#status').val();
            var keyword = $('#keyword').val(); 

            if(date_created_start == "" && date_created_end == "" && status == "" && keyword == ""){
                alert("Please populate filter first");
            } else {

                $('#loader').hide();
                $('#loading').show();
                /*$.ajax({
                type:"POST",
                url: "<?=base_url()?>support/search/"+val,
                    success: function(data){
                        $('#loader').html(data);
                        $('#loader').slideDown('fast');
                        $('#loading').hide();
                    }
                });*/
                $.ajax({
                    type:"POST",
                    url: "<?=base_url()?>portal/my_search_request/",
                    data: {
                        date_created_start: date_created_start, 
                        date_created_end: date_created_end,
                        status: status,
                        keyword: keyword
                    },
                    // url: <?=base_url()?>+"personnel/get_items/"+val,
                    success: function(data){
                        $('#loader').html(data);
                        $('#loader').slideDown('slow');
                        $('#loading').css('display', 'none');
                    }
                });
            }
        }
    </script>    

    <script type="text/javascript">

        function Logout(){ 

            var logout = confirm('Are you sure you want to Logout?');
            if(logout == true){

                window.location.href="<?=base_url()?>portal/logout";

            } else { }

        }

    </script>
    
</head>
<body>
<div class="wrapper1">
<div class="home_banner"></div>
    <nav class="navbar navbar-inverse navbar-fixed-top solid-header" role="navigation" id="trans_head">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle mod-navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <?php 
            if($profimg){  
                $prof_img = $profimg;
            }else {
                $prof_img = "user.png";
            }
            // if(!file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/users/".$profimg)){  // for local use only
            if(!file_exists(APPPATH."../assets/img/users/".$prof_img)){  //for devsite
                $prof_img = "user.png";
            }
            ?>
                    <a class="navbar-brand mod-navbar-brand" href="<?=base_url()?>portal/">
                        <span class="nav-pheader">
                            <img class="prof_thumb" src="<?=base_url()?>assets/img/users/<?=$prof_img?>">
                        </span>
                        <span class="nav-pheader"> Hi <?=ucwords($uname)?> </span>
                    </a>
               
                <!-- <img src="<?=base_url()?>assets/img/logos/Logo02-02.png" class="company-logo"> -->


            </div>
            <div class="col-sm-2 col-md-3 col-lg-4"></div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav mod-navbar-nav" id="trans_nav">
                    <li class="<?php if($active == "myHome"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal">Homepage</a>
                    </li>
                    <li class="<?php if($active == "myProfile"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/my_profile">My Profile</a>
                     </li>
                    <!-- <li>
                        <a href="#">My Property</a>
                    </li> -->
                    <!-- <li class="<?php if($active == "announcements"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/announcements">Announcement</a>
                    </li> -->
                    <li class="<?php if($active == "myProperty"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/properties">My Property</a>
                    </li>
                    <li class="<?php if($active == "myRequest"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/new_request">Make Request</a>
                    </li>
                    <li class="<?php if($active == "docs"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/myDocs">My Docs</a>
                    </li>
                    <li class="<?php if($active == "soa"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/SOA">SOA</a>
                    </li>
                    <li class="<?php if($active == "contacts"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/contacts">Contacts</a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="Logout()">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!--  <nav class="navbar navbar-inverse navbar-fixed-top solid-header" role="navigation" id="solid_head">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle mod-navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="<?=base_url()?>assets/img/logos/Logo02-02.png" class="company-logo">

            </div>
            <div class="col-sm-2 col-md-3 col-lg-4"></div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav mod-navbar-nav" id="trans_nav">
                    <li class="<?php if($active == "myHome"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal">My Home</a>
                    </li>
                    <li class="<?php if($active == "myProfile"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/my_profile">My Profile</a>
                     </li>
                    <li class="<?php if($active == "myProperty"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/properties">My Property</a>
                    </li>
                    <li class="<?php if($active == "myRequest"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/my_request">My Request</a>
                    </li>
                    <li>
                        <a href="#">My Docs</a>
                    </li>
                    <li>
                        <a href="#">SOA</a>
                    </li>
                    <li class="<?php if($active == "contacts"){ echo "active";} ?>">
                        <a href="<?=base_url()?>portal/contacts">Contacts</a>
                    </li>
                    <li>
                        <a href="javascript:;" onclick="Logout()">Logout</a>
                    </li>
                </ul>
            </div>
       </div>
     </nav> -->