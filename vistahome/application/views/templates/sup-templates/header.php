<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="<?=base_url()?>assets/img/logos/icon1.png">
        <title>Vista HOMe Portal Admin</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/styles/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/admin/chosen/chosen.jquery.js"></script>
        <script src="<?=base_url()?>assets/admin/jquery-ui/jquery-ui.js"></script>

        <link href="<?=base_url()?>assets/styles/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/admin/icomoon/style.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/styles/css/column5.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/support/css/styles.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/admin/chosen/bootstrap-chosen.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/admin/jquery-ui/jquery-ui.css" rel="stylesheet">
        
        <script src="<?=base_url()?>assets/support/js/bootbox.min.js"></script>
        <!--[if lt IE 9]>
            <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <style type="text/css">
            body.modal-open {
                overflow: hidden;
            }
            .chosen-container .chosen-results {
               max-height:200px !important;
            }
        </style>
        
        <script type="text/javascript">

            function logout(){ 
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to Logout?',
                    callback: function(result){
                        if(result){
                            window.location.href="<?=base_url()?>support/logout";
                        }
                    }
                });
            }

        </script>

        <script type="text/javascript">

            function handler(e){
                if(e.keyCode === 13){
                    search_homeowner();
                }
            }

            function uhandler(e){
                if(e.keyCode === 13){
                    search_users();
                }
            }

            function ahandler(e){
                if(e.keyCode === 13){
                    search_announcements();
                }
            }

            function userSearchTxtBoxEvent() {
                var keyword = $('#userSearch').val();
                if(keyword == "") {
                    $("#searchUser").animate({width:'toggle'},350);
                }
            }

            function announceSearchTxtBoxEvent() {
                var keyword = $('#announceSearch').val();
                if(keyword == "") {
                    $("#searchAnnounce").animate({width:'toggle'},350);
                }
            }

            function slideSearch() {
                var search = $('#userSearch').val();
                if(search == "") {
                    $("#searchUser").animate({width:'toggle'},350);
                } else {
                    search_users();
                }
            }

            function slideSearch_2() {
                var search = $('#announceSearch').val();
                if(search == "") {
                    $("#searchAnnounce").animate({width:'toggle'},350);
                } else {
                    search_announcements();
                }
            }

            function keyword(val) {
                var process = $('#search');
                if(val == "") {
                    process.slideUp();
                } else {
                    process.slideDown();
                }
            }

            $(document).ready(function(){
                $("#toggle_filter").click(function(){
                    // $(this).toggleClass('.glyphicon-chevron-up');
                    if(this.className == "pull-right toggle_filter glyphicon glyphicon-chevron-down") {
                       this.className="pull-right toggle_filter glyphicon glyphicon-chevron-up";
                    } else {
                       this.className="pull-right toggle_filter glyphicon glyphicon-chevron-down";
                    }
                    $('#load_filter').slideToggle('fast');
                });
            });

        </script>

        <script type="text/javascript">

            function registration(val){ 
                $('#loader').hide();
                $('#loading').css('display', 'block');

                if(val == 1){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_new/",
                        success: function(data){
                            $('#loader').html(data);
                            $('#loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }
                if(val == 2){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_outstanding/",
                        success: function(data){
                            $('#loader').html(data);
                            $('#loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }
                if(val == 3){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_confirmed/",
                        success: function(data){
                            $('#loader').html(data);
                            $('#loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }
                if(val == 4){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_rejected/",
                        success: function(data){
                            $('#loader').html(data);
                            $('#loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }
                if(val == 5){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_waiting/",
                        success: function(data){
                            $('#loader').html(data);
                            $('#loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }
            }  

            function enablediv() {
                var node_list = document.getElementsByTagName('input');
                for (var i = 0; i < node_list.length; i++) {
                    var node = node_list[i];
                    if (node.getAttribute('type') == 'text') {
                        node.value="";
                    }
                    if (node.getAttribute('type') == 'checkbox') {
                        node.checked = false;
                    }
                } 
               var el = document.getElementById('checklist-body'),
                all = el.getElementsByTagName('*'),
                i;
                for (i = 0; i < all.length; i++) {
                    all[i].disabled = false;
                }
                $('#buttons').css('display', 'block');
                $('#btnaddroles').css('visibility', 'hidden');
                $('#checklist-body').css('display', 'block');
            }

            function newProject() {
                $('#updProjSaveBtn').css('display', 'none');
                $('#newProjSaveBtn').css('display', 'block');
                $('#bgrpname').val('');
                $('#selectedcompany option[value=0]').attr('selected','selected');
                $('#resultcompany optgroup').remove();
                $('#newProjBtn').css('visibility', 'hidden');
                $('#businessGroupDiv').css('display', 'block');
            }

            function newProjCancelBtn() {
                $('#newProjSaveBtn').css('display', 'none');
                $('#newProjBtn').css('visibility', 'visible');
                $('#businessGroupDiv').css('display', 'none');
            }

            function cancelroleadd() {
            var node_list = document.getElementsByTagName('input');
                for (var i = 0; i < node_list.length; i++) {
                    var node = node_list[i];
                    if (node.getAttribute('type') == 'text') {
                        node.value="";
                    }
                    if (node.getAttribute('type') == 'checkbox') {
                        node.checked = false;
                    }
                } 
                $('#buttons').css('display', 'none');
                $('#btnaddroles').css('visibility', 'visible');
                $('#checklist-body').css('display', 'none');
                $('#editchecklist').css('display', 'none');
                var el = document.getElementById('checklist-body'),
                all = el.getElementsByTagName('input'),
                i;
                for (i = 0; i < all.length; i++) {
                    all[i].disabled = true;
                }
            }

            function cancelroleedit() {
            var node_list = document.getElementsByTagName('input');
                for (var i = 0; i < node_list.length; i++) {
                    var node = node_list[i];
                    if (node.getAttribute('type') == 'text') {
                        node.value="";
                    }
                    if (node.getAttribute('type') == 'checkbox') {
                        node.checked = false;
                    }
                }
                $('#def_checklist').css('display','block');
                $('#checklist').css('display','none');
                $('#editbuttons').css('display', 'none');
                var el = document.getElementById('editchecklist'),
                all = el.getElementsByTagName('input'),
                i;
                for (i = 0; i < all.length; i++) {
                    all[i].disabled = true;
                }
            }

            function addroles() {
                var node_list = document.getElementsByTagName('input');
 
                for (var i = 0; i < node_list.length; i++) {
                    var node = node_list[i];
                 
                    if (node.getAttribute('type') == 'text') {
                        node.value="";
                    }
                    if (node.getAttribute('type') == 'checkbox') {
                        node.checked = false;
                    }
                } 
                
                $('#def_checklist').css('display','block');
                $('#checklist').css('display','none');
            }

            function status(val){
                var process = $('#process');
                if(val == 0 || val == 1 || val == 4){
                    process.slideUp();
                } 
                if(val == 2 || val == 3){
                    process.slideDown();
                }
            } 

            function checkusrname(val) {
                $('#usrnamestatus').css('display','none');
                var usrname = val;
                if(usrname!=""){ 
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>support/checkusrname/",
                        data: {
                            usrname: usrname
                        },
                        success: function(data) {
                            
                            $('#usrnamestatus').html(data);
                            $('#nu_uname').css('width','100%');
                            // $('#usrnamestatus').animate({width: '100%'},550); 
                            $('#usrnamestatus').css('display','block');
                        }
                    });
                } else {
                    $('#nu_uname').animate({width:'200%'},350);    
                }
            }

            function checkusrname1(val) {
                var current = val;
                $('#usrnamestatus').css('display','none');
                var usrname = $('#nu_uname').val();
                if(current != usrname){
                    if(usrname!=""){ 
                        $.ajax({
                            type: "POST",
                            url: "<?=base_url()?>support/checkusrname/",
                            data: {
                                usrname: usrname
                            },
                            success: function(data) {
                                
                                $('#usrnamestatus').html(data);
                                $('#nu_uname').css('width','100%');
                                $('#usrnamestatus').animate({width: '100%'},550); 
                                $('#usrnamestatus').css('display','block');
                            }
                        });
                    } else {
                        $('#nu_uname').animate({width:'200%'},350);   
                        $('#nu_uname').val(current); 
                    }               
                } else {
                    $('#nu_uname').animate({width:'200%'},350);   
                    $('#nu_uname').val(current); 
                }
            }

            function search_homeowner(){
                $('#owners').hide();
                $('#loading').css('display', 'block');
                var keyword = $('#keyword').val(); 
                var project = $('#project').val();
                var company = $('#company').val();
                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/search_homeowner/",
                        data: {
                            keyword: keyword,
                            company: company,
                            project: project
                        },
                        success: function(data){
                            $('#owners').html(data);
                            $('#owners').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
            }

            function search_users(){
                $('#users').hide();
                $('#loading').css('display', 'block');
                var keyword = $('#userSearch').val(); 
                $.ajax({
                    type:"POST",
                    url: "<?=base_url()?>support/search_users/",
                    data: {
                        keyword: keyword 
                    },
                    success: function(data){
                        $('#users').html(data);
                        $('#users').slideDown('slow');
                        $('#loading').css('display', 'none');
                    }
                });
            }
            
            function search_announcements(){
                $('#ann_wrap').hide();
                $('#loading').css('display', 'block');
                var keyword = $('#announceSearch').val(); 
                $.ajax({
                    type:"POST",
                    url: "<?=base_url()?>support/search_announcements/",
                    data: {
                        keyword: keyword 
                    },
                    success: function(data){
                        $('#ann_wrap').html(data);
                        $('#loading').css('display', 'none');
                        $('#ann_wrap').css('display', 'block');

                    }
                });
            }

            function edit_roles(val) {
                $('#page_loader').css('display','block');
                var id = val;
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>support/edit_roles",
                    data: {
                        id: id
                    },
                    success :function(data) {
                        $('#page_loader').css('display','none');
                        $('#checklist').html(data);
                        $('#checklist').css('display','block');
                        $('#editchecklist').css('display','block');
                        $('#def_checklist').css('display','none');
                    }
                })
            }

            function editBusinessGroup(val) {
                var id= val;
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>support/list_BG",
                    data: {
                        id: id,
                    },
                    success :function(data) {
                        $('#resultcompany').html(data);
                        $('#businessGroupDiv').css('display','block');
                        $('#newProjBtn').css('visibility', 'hidden');
                        $('#newProjSaveBtn').css('display','none');
                        $('#updProjSaveBtn').css('display','block');
                        $('#selectedcompany').prop('disabled', false);
                        $('#assignBG').prop('disabled', false);
                    }
                });
            }

            function list_roles(val) {
                var id = val;
                if(id!=0){
                    $('#page_loader').css('display','block');
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>support/list_roles",
                        data: {
                            id: id
                        },
                        success :function(data) {
                            $('#page_loader').css('display','none');
                           /* $('#display2').css('display','block');
                            $('#display2').html(data);*/
                            $('#display').html(data);
                        }
                    });
                }
            }

            function list_BG(val) {
                var usrid = $('#nu_usrid').val();
                var id= val;
                if(id != 0) {
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>support/list_BG",
                        data: {
                            id: id,
                            usrid: usrid
                        },
                        success :function(data) {
                            $('#resultbussEnt').html(data);
                            $('#selectedcompany').prop('disabled', false);
                            $('#assignBG').prop('disabled', false);
                        }
                    });
                } else {
                    $('#selectedcompany').prop('disabled', true);
                    $('#assignBG').prop('disabled', true);
                }
            }

            function deleteroles(val) {
                var id = val;
                bootbox.confirm({
                    size: 'small',
                    message: "Are you sure you want to delete this?",
                    callback: function(result){
                        if(result){
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url()?>support/deleteroles",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if(data == 1){
                                        bootbox.alert({
                                            size: 'small',
                                            message: "Role Successfully Deleted!"
                                        });
                                    } else {
                                        bootbox.alert({
                                            size: 'small',
                                            message: 'Unable to delete this role. Please remove this role to a user to procceed.'
                                        });
                                    }
                                    load_roles();
                                }
                            });
                        }
                    }
                });
            }

            function load_roles() {
                $('#loadroles').hide();
                $('#loading').css('display', 'block');         
                $.ajax({
                    type:"get",
                    url: "<?=base_url()?>support/load_roles/",
                    success: function(data){
                        $('#loadroles').html(data);
                        $('#loadroles').css('display', 'block'); 
                        $('#loading').css('display', 'none');
                    }
                });
            }

            function runScript(e) {
                if (e.keyCode == 13) {
                    search();
                    return false;
                }
            }

            function onEnt(e){
                if (e.keyCode == 13) {
                    search_concern();
                    return false;
                }
            }

            function listproj(){
                var co = $('#selectedcompany option:selected').val();
                var options = {};
                $('#resultcompany option').each(function(){
                    options[$(this).val()] = $(this).text();
                });
                if(co==""){
                } else {         
                    $('#loading').css('display','block');
                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/listproj/",
                        data: {
                            co: co,
                            options: options
                        },
                        success: function(data){
                            $('#sourcecompany').html(data);
                            $('#loading').css('display','none');
                            return false;
                        }
                    });
                }
            }

            function listproj2(val){
                var co = $('#selectedcompany option:selected').val();
                var options = {};
                $('#resultbussEnt option').each(function(){
                    options[$(this).val()] = $(this).text();
                });
                if(co!=0){         
                    $('#load_sourcebussEnt').css('display','block');
                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/listproj/",
                        data: {
                            co: co,
                            options: options
                        },
                        success: function(data){
                            $('#sourcebussEnt').html(data);
                            $('#load_sourcebussEnt').css('display','none');
                            return false;
                        }
                    });
                }
            }

            function bussEnt(){
                var bgID = $('#selectedBusinessGroup option:selected').val();
                var co = $('#assignBG option:selected').val();
                if(co==""){
                } else {         
                    $('#load_resultbussEnt').css('display','block');
                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/bussEnt/",
                        data: {
                            co: co,
                            bgID: bgID
                        },
                        success: function(data){
                            $('#resultbussEnt').html(data);
                            $('#load_resultbussEnt').css('display','none');
                            return false;
                        }
                    });
                }
            }

            function addGroup() {
                var usrid = $('#nu_usrid').val();
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>support/addGroup/",
                    data: {
                        usrid: usrid
                    },
                    success: function(data) {
                        $('#addGroup').html(data);
                        $('#addGroup').slideDown('slow');
                        $('#addGrpBtn').css('display','none');
                    }
                });
            }

            function cancelAddGroup() {
                $('#addGroup').css('display','none');
                $('#editGroup').css('display','none');
                $('#addGrpBtn').css('display','block');
            }

            function saveAddGrp() {
                var usrid = $('#nu_usrid').val();
                var bgrp = $('#selectedBusinessGroup option:selected').val();
                var bussEnt = {};
                $('#resultbussEnt option').each(function(){
                    bussEnt[$(this).val()] = $(this).parent().attr('value');
                });
                if(($('#resultbussEnt option').length)!=0){
                    $.ajax ({
                        type: "POST",
                        url: "<?=base_url()?>support/saveAddGrp",
                        data: {
                            bussEnt: bussEnt,
                            bgrp: bgrp,
                            usrid: usrid
                    },
                        success: function(data) {
                            $('#bGrpTable').html(data);
                            $('#bGrpTable').css('display', 'block');
                            $("#selectedBusinessGroup option[value=" + bgrp + "]").remove();
                            $("#selectedBusinessGroup option[value='0']").attr('selected','selected');
                            $("#selectedcompany option[value='0']").attr('selected','selected');
                            $("#sourcebussEnt option").remove();
                            var thisGroup = $('#resultbussEnt option').parent();
                            thisGroup.remove();
                            $('#selectedcompany').prop('disabled', true);
                        }
                    });
                }else {
                    bootbox.alert({
                        size: 'small',
                        message: 'Select valid business group.'
                    });
                }  
            }

            function saveuser() {
                var bussEnt = {};
                $('#resultbussEnt option').each(function(){
                    bussEnt[$(this).val()] = $(this).parent().attr('value');
                });
                if($('#regview').prop('checked') == true) {var regview = 1;} else { var regview = 0;}
                if($('#regconf').prop('checked') == true) {var regconf = 1;} else { var regconf = 0;}
                if($('#suppview').prop('checked') == true) {var suppview = 1;} else { var suppview = 0;}
                if($('#suppreply').prop('checked') == true) {var suppreply = 1;} else { var suppreply = 0;}
                if($('#rptview').prop('checked') == true) {var rptview = 1;} else { var rptview = 0;}
                if($('#rptexpo').prop('checked') == true) {var rptexpo = 1;} else { var rptexpo = 0;}
                if($('#mtc').prop('checked') == true) {var mtc = 1;} else { var mtc = 0;}
                if($('#carview').prop('checked') == true) {var carview = 1;} else { var carview = 0;}
                if($('#annview').prop('checked') == true) {var annview = 1;} else { var annview = 0;}
                if($('#soaview').prop('checked') == true) {var soaview = 1;} else { var soaview = 0;}
                if($('#hoview').prop('checked') == true) {var hoview = 1;} else { var hoview = 0;}
                if($('#accnt').prop('checked') == true) {var accnt = 1;} else { var accnt = 0;}
                if($('#usrview').prop('checked') == true) {var usrview = 1;} else { var usrview = 0;}
                if($('#usredit').prop('checked') == true) {var usredit = 1;} else { var usredit = 0;}
                if($('#usrdel').prop('checked') == true) {var usrdel = 1;} else { var usrdel = 0;}
                if($('#usradd').prop('checked') == true) {var usradd = 1;} else { var usradd = 0;}
                if($('#roleadd').prop('checked') == true) {var roleadd = 1;} else { var roleadd = 0;}
                if($('#roleedit').prop('checked') == true) {var roleedit = 1;} else { var roleedit = 0;}
                if($('#roledel').prop('checked') == true) {var roledel = 1;} else { var roledel = 0;}
                if($('#concern').prop('checked') == true) {var concern = 1;} else { var concern = 0;}
                if($('#inq').prop('checked') == true) {var inq = 1;} else { var inq = 0;}
                if($('#req').prop('checked') == true) {var req = 1;} else { var req = 0;}
                if($('#suppcomm').prop('checked') == true) {var suppcomm = 1;} else { var suppcomm = 0;}
                var uname = $('#nu_uname').val();
                var lname = $('#nu_lname').val();
                var fname = $('#nu_fname').val();
                var mail = $('#nu_email').val();
                var spark = $('#nu_spark').val();
                var role = $('#selectedrole option:selected').val();
                var bgrp = $('#selectedBusinessGroup option:selected').val();
                var valid = $('#chckusrname').val();
                if(uname=="" || lname=="" || fname=="" || mail=="" || spark=="" || role==0 || bgrp==0) {
                    bootbox.alert("All fields are required!"); 
                    return false;
                }
                if(valid != 'valid') {
                    bootbox.alert('Invalid Username!');
                    return false;
                }
                bootbox.confirm({
                    size: 'small',
                    message: "Are you sure you want to save this?", 
                    callback: function(result){
                        if (result) {
                            $('#page_loader').css('display','block');
                            $.ajax ({
                                type: "POST",
                                url: "<?=base_url()?>support/saveuser",
                                data: {
                                    bussEnt: bussEnt,
                                    uname: uname,
                                    lname: lname,
                                    fname: fname,
                                    mail: mail,
                                    spark: spark,
                                    role: role,
                                    bgrp: bgrp,
                                    regview: regview,
                                    regconf: regconf,
                                    suppview: suppview,
                                    suppreply: suppreply,
                                    rptview: rptview,
                                    rptexpo: rptexpo,
                                    mtc: mtc,
                                    carview: carview,
                                    annview: annview,
                                    soaview: soaview,
                                    hoview: hoview,
                                    accnt: accnt,
                                    usrview: usrview,
                                    usredit: usredit,
                                    usrdel: usrdel,
                                    usradd: usradd,
                                    roleadd: roleadd,
                                    roleedit: roleedit,
                                    roledel: roledel,
                                    concern: concern,
                                    inq: inq,
                                    req: req,
                                    suppcomm: suppcomm
                                },
                                success: function(data) {
                                    $('#page_loader').css('display','none');
                                    bootbox.alert({
                                        size: 'small',
                                        message: "New user Successfully added!"
                                    });
                                    window.location.replace("<?=base_url()?>support/users");
                                }
                            });
                        }  
                    }
                });
            }

            function del_usr(val, val2) {
                var usrId = val;
                var uname = val2;
                bootbox.confirm({
                    size: 'small',
                    message: "Do you want to delete this user?", 
                    callback: function(result){
                        if(result) {
                            $.ajax ({
                                type: "POST",
                                url: "<?=base_url()?>support/del_usr",
                                data: {
                                    usrId: usrId
                                },
                                success: function(data) {
                                    $('#users').html(data);
                                    $('#users').css('display','block');
                                    bootbox.alert({
                                        size: 'small',
                                        message: uname+" Successfully Deleted!"
                                    });
                                }
                            });
                        }
                    }
                });  
            }

            function edit_usrgrp(val, val2) {
                var usrId = val;
                var grpId = val2;
                $.ajax ({
                    type: "POST",
                    url: "<?=base_url()?>support/edit_usrgrp",
                    data: {
                        usrId: usrId,
                        grpId: grpId
                    },
                    success: function(data) {
                        $('#editGroup').html(data);
                        $('#editGroup').css('display', 'block');
                        $('#addGrpBtn').css('display','none');
                        $('#addGroup').css('display','none');

                    }
                });
            }

            function del_usrgrp(val,val2) {
                var usrId = val;
                var grpId = val2;
                bootbox.confirm({
                    size: 'small',
                    message: "Do you want to delete this Business Group?",
                    callback: function(result){
                        if(result) {
                            $.ajax ({
                                type: "POST",
                                url: "<?=base_url()?>support/del_usrgrp",
                                data: {
                                    usrId: usrId,
                                    grpId: grpId
                                },
                                success: function(data) {
                                    $('#bGrpTable').html(data);
                                    $('#bGrpTable').css('display', 'block');
                                    $('#addGrpBtn').css('display', 'block');
                                    bootbox.alert({
                                        size: 'small',
                                        message: " Successfully Deleted!"
                                    });
                                }
                            });
                        }
                    }
                });
            }

            function add1() {
                if($('#sourcecompany option:selected').val() != null) {
                    var grpLabel;
                    $('#selectedcompany option:selected').each(function(){
                        grpLabel = $(this).text();
                    });
                    var targetGroup = $('#resultcompany optgroup[label="' + grpLabel + '"]');
                    if (targetGroup.length == 0) {
                        targetGroup = $('<optgroup label="' + grpLabel + '"/>');
                        $('#resultcompany').append(targetGroup);
                    }
                    targetGroup.append($('#sourcecompany option:selected').clone());
                    $('#sourcecompany option:selected').remove();
                }
            }

            function addall1() {
                var grpLabel;
                $('#selectedcompany option:selected').each(function(){
                    grpLabel = $(this).text();
                });
                var targetGroup = $('#resultcompany optgroup[label="' + grpLabel + '"]');
                if (targetGroup.length == 0) {
                    targetGroup = $('<optgroup label="' + grpLabel + '"/>');
                    $('#resultcompany').append(targetGroup);
                }
                targetGroup.append($('#sourcecompany option').clone());
                $('#sourcecompany option').remove(); 
            }

            function removed1() {
                var targetGroup = $('#sourcecompany');
                targetGroup.append($('#resultcompany option:selected').clone());
                var thisGroup = $('#resultcompany option:selected').parent();
                $('#resultcompany option:selected').remove();
                if (thisGroup.find('option').length == 0) {
                    thisGroup.remove();
                }
            }

            function removeall1() {
                var targetGroup = $('#sourcecompany');
                targetGroup.append($('#resultcompany option').clone());
                var thisGroup = $('#resultcompany option').parent();
                $('#resultcompany option').remove();
                if (thisGroup.find('option').length == 0) {
                    thisGroup.remove();
                }
            }


            function add() {
                if($('#sourcebussEnt option:selected').val() != null) {
                    var grpLabel;
                    var grpVal;
                    $('#selectedcompany option:selected').each(function(){
                        grpLabel = $(this).text();
                        grpVal = $(this).val();
                    });
                    var targetGroup = $('#resultbussEnt optgroup[label="' + grpLabel + '"]');
                    if (targetGroup.length == 0) {
                        targetGroup = $('<optgroup value="' + grpVal + '" label="' + grpLabel + '"/>');
                        $('#resultbussEnt').append(targetGroup);
                    }
                    targetGroup.append($('#sourcebussEnt option:selected').clone());
                    $('#sourcebussEnt option:selected').remove();
                }   
            }

            function addall() {
                var grpLabel;
                $('#selectedcompany option:selected').each(function(){
                    grpLabel = $(this).text();
                    grpVal = $(this).val();
                });
                var targetGroup = $('#resultbussEnt optgroup[label="' + grpLabel + '"]');
                if (targetGroup.length == 0) {
                   targetGroup = $('<optgroup value="' + grpVal + '" label="' + grpLabel + '"/>');
                    $('#resultbussEnt').append(targetGroup);
                }
                targetGroup.append($('#sourcebussEnt option').clone());
                $('#sourcebussEnt option').remove(); 
            }

            function removed() {
                var targetGroup = $('#sourcebussEnt');
                targetGroup.append($('#resultbussEnt option:selected').clone());
                var thisGroup = $('#resultbussEnt option:selected').parent();
                $('#resultbussEnt option:selected').remove();
                if (thisGroup.find('option').length == 0) {
                    thisGroup.remove();
                }
            }

            function removeall() {
                var targetGroup = $('#sourcebussEnt');
                targetGroup.append($('#resultbussEnt option').clone());
                var thisGroup = $('#resultbussEnt option').parent();
                $('#resultbussEnt option').remove();
                if (thisGroup.find('option').length == 0) {
                    thisGroup.remove();
                }
            }

            function updateroles(val) {
                var id = val;
                var rname = $('#rolename').val();
                if($('#regview').prop('checked') == true) {var regview = 1;} else { var regview = 0;}
                if($('#regconf').prop('checked') == true) {var regconf = 1;} else { var regconf = 0;}
                if($('#suppview').prop('checked') == true) {var suppview = 1;} else { var suppview = 0;}
                if($('#suppreply').prop('checked') == true) {var suppreply = 1;} else { var suppreply = 0;}
                if($('#rptview').prop('checked') == true) {var rptview = 1;} else { var rptview = 0;}
                if($('#rptexpo').prop('checked') == true) {var rptexpo = 1;} else { var rptexpo = 0;}
                if($('#mtc').prop('checked') == true) {var mtc = 1;} else { var mtc = 0;}
                if($('#carview').prop('checked') == true) {var carview = 1;} else { var carview = 0;}
                if($('#annview').prop('checked') == true) {var annview = 1;} else { var annview = 0;}
                if($('#soaview').prop('checked') == true) {var soaview = 1;} else { var soaview = 0;}
                if($('#hoview').prop('checked') == true) {var hoview = 1;} else { var hoview = 0;}
                if($('#accnt').prop('checked') == true) {var accnt = 1;} else { var accnt = 0;}
                if($('#usrview').prop('checked') == true) {var usrview = 1;} else { var usrview = 0;}
                if($('#usredit').prop('checked') == true) {var usredit = 1;} else { var usredit = 0;}
                if($('#usrdel').prop('checked') == true) {var usrdel = 1;} else { var usrdel = 0;}
                if($('#usradd').prop('checked') == true) {var usradd = 1;} else { var usradd = 0;}
                if($('#roleadd').prop('checked') == true) {var roleadd = 1;} else { var roleadd = 0;}
                if($('#roleedit').prop('checked') == true) {var roleedit = 1;} else { var roleedit = 0;}
                if($('#roledel').prop('checked') == true) {var roledel = 1;} else { var roledel = 0;}
                if($('#concern').prop('checked') == true) {var concern = 1;} else { var concern = 0;}
                if($('#inq').prop('checked') == true) {var inq = 1;} else { var inq = 0;}
                if($('#req').prop('checked') == true) {var req = 1;} else { var req = 0;}
                if($('#suppcomm').prop('checked') == true) {var suppcomm = 1;} else { var suppcomm = 0;}
                $('#page_loader').css('display','block');
                $.ajax({
                    type: "POST",
                    url: "<?=base_url()?>support/updateroles",
                    data: {
                        id: id,
                        rname: rname,
                        regview: regview,
                        regconf: regconf,
                        suppview: suppview,
                        suppreply: suppreply,
                        rptview: rptview,
                        rptexpo: rptexpo,
                        mtc: mtc,
                        carview: carview,
                        annview: annview,
                        soaview: soaview,
                        hoview: hoview,
                        accnt: accnt,
                        usrview: usrview,
                        usredit: usredit,
                        usrdel: usrdel,
                        usradd: usradd,
                        roleadd: roleadd,
                        roleedit: roleedit,
                        roledel: roledel,
                        concern: concern,
                        inq: inq,
                        req: req,
                        suppcomm: suppcomm
                    },
                    success: function(data) {
                        $('#page_loader').css('display','none');
                        $('#def_checklist').css('display','block');
                        $('#checklist').css('display','none');
                        bootbox.alert({
                            size: 'small',
                            message: 'Successfully Updated!'
                        });
                        var el = document.getElementById('editbody'),
                        all = el.getElementsByTagName('input'),
                        i;
                        for (i = 0; i < all.length; i++) {
                            all[i].disabled = true;
                        }
                    }
                });
            }

            function updateUsr(val) {
                var usrid = val;
                var roleid = $('#selectedrole').val();
                var uname = $('#nu_uname').val();
                var lname = $('#nu_lname').val();
                var fname = $('#nu_fname').val();
                var email = $('#nu_email').val();
                var spark = $('#nu_spark').val();
                var valid = $('#chckusrname').val();
                if($('#regview').prop('checked') == true) {var regview = 1;} else { var regview = 0;}
                if($('#regconf').prop('checked') == true) {var regconf = 1;} else { var regconf = 0;}
                if($('#suppview').prop('checked') == true) {var suppview = 1;} else { var suppview = 0;}
                if($('#suppreply').prop('checked') == true) {var suppreply = 1;} else { var suppreply = 0;}
                if($('#rptview').prop('checked') == true) {var rptview = 1;} else { var rptview = 0;}
                if($('#rptexpo').prop('checked') == true) {var rptexpo = 1;} else { var rptexpo = 0;}
                if($('#mtc').prop('checked') == true) {var mtc = 1;} else { var mtc = 0;}
                if($('#carview').prop('checked') == true) {var carview = 1;} else { var carview = 0;}
                if($('#annview').prop('checked') == true) {var annview = 1;} else { var annview = 0;}
                if($('#soaview').prop('checked') == true) {var soaview = 1;} else { var soaview = 0;}
                if($('#hoview').prop('checked') == true) {var hoview = 1;} else { var hoview = 0;}
                if($('#accnt').prop('checked') == true) {var accnt = 1;} else { var accnt = 0;}
                if($('#usrview').prop('checked') == true) {var usrview = 1;} else { var usrview = 0;}
                if($('#usredit').prop('checked') == true) {var usredit = 1;} else { var usredit = 0;}
                if($('#usrdel').prop('checked') == true) {var usrdel = 1;} else { var usrdel = 0;}
                if($('#usradd').prop('checked') == true) {var usradd = 1;} else { var usradd = 0;}
                if($('#roleadd').prop('checked') == true) {var roleadd = 1;} else { var roleadd = 0;}
                if($('#roleedit').prop('checked') == true) {var roleedit = 1;} else { var roleedit = 0;}
                if($('#roledel').prop('checked') == true) {var roledel = 1;} else { var roledel = 0;}
                if($('#concern').prop('checked') == true) {var concern = 1;} else { var concern = 0;}
                if($('#inq').prop('checked') == true) {var inq = 1;} else { var inq = 0;}
                if($('#req').prop('checked') == true) {var req = 1;} else { var req = 0;}
                if($('#suppcomm').prop('checked') == true) {var suppcomm = 1;} else { var suppcomm = 0;}
                if(uname=="" || roleid==0 || lname=="" || fname=="" || email=="" || spark=="") {
                    bootbox.alert({
                        size: 'small',
                        message: "All fields are required!"
                    }); 
                    return false;
                }

                if(valid == 'invalid') {
                    bootbox.alert({
                        size: 'small',
                        message: 'Invalid Username!'
                    });
                    return false;
                }

                bootbox.confirm({ 
                    size: 'small',
                    message: "Are you sure you want to save this?", 
                    callback: function(result){ 
                        if (result) {
                            $('#page_loader').css('display','block');
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url()?>support/updateUsr",
                                data: {
                                    usrid: usrid,
                                    uname: uname,
                                    lname: lname,
                                    fname: fname,
                                    email: email,
                                    spark: spark,
                                    roleid: roleid,
                                    regview: regview,
                                    regconf: regconf,
                                    suppview: suppview,
                                    suppreply: suppreply,
                                    rptview: rptview,
                                    rptexpo: rptexpo,
                                    mtc: mtc,
                                    carview: carview,
                                    annview: annview,
                                    soaview: soaview,
                                    hoview: hoview,
                                    accnt: accnt,
                                    usrview: usrview,
                                    usredit: usredit,
                                    usrdel: usrdel,
                                    usradd: usradd,
                                    roleadd: roleadd,
                                    roleedit: roleedit,
                                    roledel: roledel,
                                    concern: concern,
                                    inq: inq,
                                    req: req,
                                    suppcomm: suppcomm
                                },
                                success: function(data) {
                                    $('#page_loader').css('display','none');
                                    bootbox.alert({
                                        size: 'small',
                                        message: 'Successfully Updated!'
                                    });
                                }
                            });
                        }
                    }
                })
            }

            function deleteBusinessGroup(val) {
                var grpid = val;
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to delete this group?',
                    callback: function(result){
                        if(result) {
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url()?>support/deleteBusinessGroup",
                                data: {
                                    grpid: grpid
                                },
                                success: function(data) {
                                    $('#page_loader').css('display','none');
                                    if(data == 1){
                                        bootbox.alert({
                                            size: 'small',
                                            message: "Role Successfully Deleted!"
                                        });
                                    } else {
                                        bootbox.alert({
                                            size: 'small',
                                            message: 'Unable to delete this role. Please remove this role to a user to procceed.'
                                        });
                                    }
                                    loadBusinessGroup();
                                }
                            });
                        }
                    }
                });
            }

            function loadBusinessGroup() {    
                $('#businessGroup').hide();
                $('#loadingBG').css('display', 'block');         
                $.ajax({
                    type:"get",
                    url: "<?=base_url()?>support/businessGroup/",
                    success: function(data){
                        $('#businessGroup').html(data);
                        $('#businessGroup').slideDown('fast');
                        $('#loadingBG').css('display', 'none');
                    }
                });
            }

            function updateBusinessGroup() {
                var grpid = $('#grpid').val();
                var gname = $('#bgrpname').val();
                var bussEnt = {};
                $('#resultcompany option').each(function(){
                    bussEnt[$(this).val()] = $(this).text();
                });
                if (($('#resultcompany option').length) === 0) {
                    bootbox.alert('Please put atleast one project!');
                    return false;
                }
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to update this group?',
                    callback: function(result){
                        if(result) {
                            $('#page_loader').css('display','block');
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url()?>support/updateBusinessGroup",
                                data: {
                                    gname: gname,
                                    grpid: grpid,
                                    bussEnt: bussEnt
                                },
                                success: function(data) {
                                    $('#page_loader').css('display','none');
                                    $('#businessGroup').html(data);
                                    bootbox.alert({
                                        size: 'small',
                                        message: "Business Group Successfully Updated!"
                                    });
                                    $('#newProjSaveBtn').css('display', 'none');
                                    $('#newProjBtn').css('visibility', 'visible');
                                    $('#businessGroupDiv').css('display', 'none');  
                                }
                            });
                        }
                    }
                });
            }

            function updateUsrGrp() {
                var usrid = $('#id').val();
                var grpid = $('#grpid').val();
                var bussEnt = {};
                $('#resultbussEnt option').each(function(){
                    bussEnt[$(this).val()] = $(this).parent().attr('value');
                });
                if (($('#resultbussEnt option').length) === 0) {
                    bootbox.alert({
                        size: 'small',
                        message: 'Please put atleast one project!'
                    });
                    return false;
                }
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to update this group in this specific user?',
                    callback: function(result){
                        if(result) {
                            $('#page_loader').css('display','block');
                            $.ajax({
                                type: "POST",
                                url: "<?=base_url()?>support/updateUsrGrp",
                                data: {
                                    usrid: usrid,
                                    grpid: grpid,
                                    bussEnt: bussEnt
                                },
                                success: function(data) { 
                                    $('#page_loader').css('display','none');
                                    bootbox.alert({
                                        size: 'small',
                                        message: 'Business Group Successfully updated!'
                                    });
                                    $('#edit_usrgrp').css('display','none');
                                }
                            });
                        }
                    }
                });  
            }

            function savebusinessgrp() {
                var rname = $('#bgrpname').val();
                var bussEnt = {};
                $('#resultcompany option').each(function(){
                    bussEnt[$(this).val()] = $(this).text();
                });
                if(($('#resultcompany option').length)!=0){
                $('#page_loader').css('display','block'); 
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>support/savebusinessgrp",
                        data: {
                            rname: rname,
                            bussEnt: bussEnt
                        },
                        success: function(data) {
                            $('#businessGroup').html(data);
                            $('#page_loader').css('display','none');
                            $('#newProjBtn').css('visibility', 'visible');
                            $('#businessGroupDiv').css('display', 'none');
                            bootbox.alert({
                                size: 'small',
                                message: "Business Group Successfully Added!"
                            });
                        }
                    });
                } 
            }

            function saverole() {
                var rname = $('#rolename1').val();
                var bussEnt = {};
                $('#resultcompany option').each(function(){
                    bussEnt[$(this).val()] = $(this).text();
                });
                if (rname != "") {
                    $('#page_loader').css('display','block');
                    if($('#regview1').prop('checked') == true) {var regview = 1;} else { var regview = 0;}
                    if($('#regconf1').prop('checked') == true) {var regconf = 1;} else { var regconf = 0;}
                    if($('#suppview1').prop('checked') == true) {var suppview = 1;} else { var suppview = 0;}
                    if($('#suppreply1').prop('checked') == true) {var suppreply = 1;} else { var suppreply = 0;}
                    if($('#rptview1').prop('checked') == true) {var rptview = 1;} else { var rptview = 0;}
                    if($('#rptexpo1').prop('checked') == true) {var rptexpo = 1;} else { var rptexpo = 0;}
                    if($('#mtc1').prop('checked') == true) {var mtc = 1;} else { var mtc = 0;}
                    if($('#carview1').prop('checked') == true) {var carview = 1;} else { var carview = 0;}
                    if($('#annview1').prop('checked') == true) {var annview = 1;} else { var annview = 0;}
                    if($('#soaview1').prop('checked') == true) {var soaview = 1;} else { var soaview = 0;}
                    if($('#hoview1').prop('checked') == true) {var hoview = 1;} else { var hoview = 0;}
                    if($('#accnt1').prop('checked') == true) {var accnt = 1;} else { var accnt = 0;}
                    if($('#usrview1').prop('checked') == true) {var usrview = 1;} else { var usrview = 0;}
                    if($('#usredit1').prop('checked') == true) {var usredit = 1;} else { var usredit = 0;}
                    if($('#usrdel1').prop('checked') == true) {var usrdel = 1;} else { var usrdel = 0;}
                    if($('#usradd1').prop('checked') == true) {var usradd = 1;} else { var usradd = 0;}
                    if($('#roleadd1').prop('checked') == true) {var roleadd = 1;} else { var roleadd = 0;}
                    if($('#roleedit1').prop('checked') == true) {var roleedit = 1;} else { var roleedit = 0;}
                    if($('#roledel1').prop('checked') == true) {var roledel = 1;} else { var roledel = 0;}
                    if($('#concern1').prop('checked') == true) {var concern = 1;} else { var concern = 0;}
                    if($('#inq1').prop('checked') == true) {var inq = 1;} else { var inq = 0;}
                    if($('#req1').prop('checked') == true) {var req = 1;} else { var req = 0;}
                    if($('#suppcomm1').prop('checked') == true) {var suppcomm = 1;} else { var suppcomm = 0;}
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>support/saverole",
                        data: {
                            rname: rname,
                            regview: regview,
                            regconf: regconf,
                            suppview: suppview,
                            suppreply: suppreply,
                            rptview: rptview,
                            rptexpo: rptexpo,
                            mtc: mtc,
                            carview: carview,
                            annview: annview,
                            soaview: soaview,
                            hoview: hoview,
                            accnt: accnt,
                            usrview: usrview,
                            usredit: usredit,
                            usrdel: usrdel,
                            usradd: usradd,
                            roleadd: roleadd,
                            roleedit: roleedit,
                            roledel: roledel,
                            concern: concern,
                            inq: inq,
                            req: req,
                            bussEnt: bussEnt,
                            suppcomm: suppcomm
                        },
                        success: function(data) {
                            $('#loadroles').html(data);
                            $('#page_loader').css('display','none');
                            bootbox.alert({
                                size: 'small',
                                message: "Role Successfully Added!"
                            });
                            $('#buttons').css('display', 'none');
                            $('#btnaddroles').css('visibility', 'visible');
                            var el = document.getElementById('checklist-body'),
                            all = el.getElementsByTagName('*'),
                            i;
                            for (i = 0; i < all.length; i++) {
                                all[i].disabled = true;
                            }
                        }
                    });
                } else {
                    bootbox.alert({
                        size: 'small',
                        message: "Please put a role name!"
                    });
                } 
            }

            function search(){

                $('#loader').hide();
                $('#loading').css('display', 'block');
                var signup_from = $('#signup_from').val();
                var signup_to = $('#signup_to').val();
                var status = $('#status').val();
                var company = $('#company').val();
                var project = $('#project').val(); 
                var keyword = $('#keyword').val(); 
                var process_from = $('#process_from').val();
                var process_to = $('#process_to').val();
                var process_by = $('#process_by').val();

                // bootbox.alert(signup_from);
                // bootbox.alert(signup_to);

                // if(signup_from == "" && signup_to == "" && status == "" && company == "" && project == "" && keyword == "" && process_from == "" && process_to == "" && process_by == ""){

                // } else {

                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/search/",
                        data: {
                            signup_from: signup_from, 
                            signup_to: signup_to,
                            status: status,
                            process_from: process_from,
                            process_to: process_to,
                            process_by: process_by,
                            company: company,
                            project: project,
                            keyword: keyword
                        },
                        // url: <?=base_url()?>+"personnel/get_items/"+val,
                        success: function(data){
                            $('#loader').html(data);
                            $('#loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                // }
            }

            function search_concern(){

                $('#ticket_loader').hide();
                $('#loading').css('display', 'block');
                var posted_from = $('#posted_from').val();
                var posted_to = $('#posted_to').val();
                var status = $('#status').val();
                var company = $('#company').val();
                var project = $('#project').val(); 
                var keyword = $('#keyword').val(); 

                // bootbox.alert(signup_from);
                // bootbox.alert(signup_to);

                // if(signup_from == "" && signup_to == "" && status == "" && company == "" && project == "" && keyword == "" && process_from == "" && process_to == "" && process_by == ""){

                // } else {

                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/search_concern/",
                        data: {
                            posted_from: posted_from, 
                            posted_to: posted_to,
                            status: status,
                            company: company,
                            project: project,
                            keyword: keyword
                        },
                        // url: <?=base_url()?>+"personnel/get_items/"+val,
                        success: function(data){
                            $('#ticket_loader').html(data);
                            $('#ticket_loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });

                // }

            }            

            function get_proj(val){

                var get_val = val;
                
                if(get_val==""){
                    
                } else {                

                    $('#page_loader').css('display','block');

                    $.ajax({
                        type:"GET",
                        url: "<?=base_url()?>support/get_proj/"+val,
                        success: function(data){
                            $('#load_project').html(data);
                            $('#page_loader').css('display','none');
                            return false;
                        }
                    });
                }
            }

        </script>

        <script type="text/javascript">

            function tickets(val){ 
                $('#ticket_loader').hide();
                $('#loading').css('display', 'block');

                if(val == 1){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_new_tickets/",
                        success: function(data){
                            $('#ticket_loader').html(data);
                            $('#ticket_loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }

                if(val == 2){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_open_tickets/",
                        success: function(data){
                            $('#ticket_loader').html(data);
                            $('#ticket_loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }

                if(val == 3){
                    $.ajax({
                        type:"post",
                        url: "<?=base_url()?>support/get_closed_tickets/",
                        success: function(data){
                            $('#ticket_loader').html(data);
                            $('#ticket_loader').slideDown('slow');
                            $('#loading').css('display', 'none');
                        }
                    });
                }
            }  

            function reply(val){

                var get_value = val;
                $.ajax({
                    type:"POST",
                    url: "<?=base_url()?>support/reply/"+get_value,
                    // url: <?=base_url()?>+"personnel/get_items/"+val,
                    success: function(data){
                        $('#load_reply'+get_value).html(data);
                    }
                });
                
                // $('#load_reply'+get_value).slideToggle();

                var loader = document.getElementById('load_reply'+get_value);

                // $("#load_reply").load("<?=base_url()?>support/reply/"+get_value);
                // return false;

            }

            function send_reply(val){
                var get_value = val;
                var get_concern = $("#concern_msg"+val).val();
                var ticket_id = $("#ticket_id").val();

                var fileInput = $('#userfile');
                var files = fileInput.prop('files');

                if(files.length != 0){
                    var fd = new FormData();
                    fd.append('userfile', files[0]);
                    fd.append('concern', get_concern);
                    fd.append('ticket_id', ticket_id);
                    fd.append('concern_id', get_value);

                    $('#loading').css('display', 'block');

                    $.ajax({
                        url: '<?=base_url()?>support/reply_upload/'+get_value,
                        data: fd,
                        contentType: false,
                        processData:false,
                        type:'POST',
                        dataType:'json',
                        success: function(result){
                            $('#load_reply'+get_value).html(result);
                            $('#loading').css('display', 'none');
                        },
                        error:function(x,e) {
                            reply(get_value);
                            $('#loading').css('display', 'none');
                        }
                    });
                } else {
                    if(get_concern == ""){
                        bootbox.alert({
                        size: 'small',
                        message: 'Please populate the reply box.'
                        });
                    } else {
                        $('#loading').css('display', 'block');
                        $.ajax({
                            url: "<?=base_url()?>support/reply/"+get_value,
                            type:"POST",
                            data: { concern : get_concern,
                                    ticket_id : ticket_id, 
                                    concern_id : get_value
                            },
                            success: function(data){
                                $('#load_reply'+get_value).html(data);
                                $('#loading').css('display', 'none');
                            },
                        });
                    }
                }
            }

            function send_comment(i){
                var get_value = i;
                var get_concern = $("#concern_msg"+i).val();
                var ticket_id = $("#ticket_id").val();

                var fileInput = $('#userfile');
                var files = fileInput.prop('files');
                  
                if(files.length != 0){
                    var fd = new FormData();
                    fd.append('userfile', files[0]);
                    fd.append('concern', get_concern);
                    fd.append('ticket_id', ticket_id);
                    fd.append('concern_id', get_value);
                    $('#loading').css('display', 'block');
                    $.ajax({
                        url: '<?=base_url()?>support/comment_upload/'+get_value,
                        data: fd,
                        contentType: false,
                        processData:false,
                        type:'POST',
                        dataType:'json',
                        success: function(result){
                            $('#load_reply'+get_value).html(result);
                            $('#loading').css('display', 'none');
                        },
                        error:function(x,e) {
                            reply(get_value);
                            $('#loading').css('display', 'none');
                        }
                    });
                } else {
                    if(get_concern == ""){
                        bootbox.alert({
                        size: 'small',
                        message: 'Please populate the reply box.'
                    });
                    } else {
                        $('#loading').css('display', 'block');
                        $.ajax({
                            url: "<?=base_url()?>support/comment/"+get_value,
                            type:"POST",
                            data: { concern : get_concern,
                                    ticket_id : ticket_id, 
                                    concern_id : get_value
                            },
                            success: function(data){
                                $('#load_reply'+get_value).html(data);
                                $('#loading').css('display', 'none');
                            },
                        });
                    }
                }
            }

            function changeusername(){

                var email = $('#email_1').val();
                var captcha = $('#captcha').val();
                var username = $('#username').val();
                var captcha_session = $('#captcha_valid').val();

                if(email == "" || captcha == "" || username == ""){

                    bootbox.alert({
                        size: 'small',
                        message: 'All fields are required.'
                    });

                } else {

                    var changeuname = {

                        "email" : email,
                        "captcha" : captcha,
                        "username" : username,
                        "captcha_session" : captcha_session

                    }

                    $("#notification").load('<?=base_url()?>login/changeuname/', changeuname );

                }

            }

            function get_request(val){ 

                if(val!=""){
                    $.ajax({
                        type:"get",
                        url: "<?=base_url()?>support/get_sub_req/"+val,
                        // url: <?=base_url()?>+"personnel/get_items/"+val,
                        success: function(data){
                            $('#sub_req_type').html(data);
                        }
                    });
                }
            }

            function sub_req_type(){
                var request = $('#request').val();
                if(request == ""){
                    bootbox.alert({
                        size: 'small',
                        message: 'Select request type first!'
                    });
                }
            }

            function getStats(id) {
                var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerText || body.textContent);

                return {
                    chars: text.length,
                    words: text.split(/[\w\u2019\'-]+/).length
                };
            }

            function post_announce(){
                var fileInput = $('#userfile');
                var files = fileInput.prop('files');
                var subject = $('#ann_id').val();
                var recipient = {};
                var i = 0;
                if($('#sendtoall').is(":checked")){
                    recipient[1] = "";
                    i++;
                }else{
                $('#sendto option').each(function(){
                    recipient[$(this).val()] = $(this).text();
                    i++;
                });
                }
                var message = window.parent.tinymce.get('thisarea2').getContent();
                if(i==0){
                    bootbox.alert({
                        size: 'small',
                        message: 'Please select a recipient.'
                    });
                    return false;
                }
                if (getStats('thisarea2').chars > 1000) {
                    bootbox.alert({
                        size: 'small',
                        message: 'Characters too long. 1000 characters only.'
                    });
                    return false;
                }
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to post this Announcement?',
                    callback: function(result){
                        if(result){
                            if(files.length != 0){
                                var json_arr = JSON.stringify(recipient);
                                var fd = new FormData();
                                fd.append('userfile', files[0]);
                                fd.append('subject', subject);
                                fd.append('message', message);
                                fd.append('recipient', json_arr);

                                $.ajax({
                                    url: "<?=base_url()?>support/post_announcement2/",
                                    data: fd,
                                    contentType: false,
                                    processData:false,
                                    type:'POST',
                                    success: function(response){
                                        $('#ann_wrap').html(response);
                                        $('#new_post_announce').css('display', 'none');
                                        $('#ann_btn_post').css('display', 'none');
                                        $('#post_notif').css('display', 'block');
                                        $('#update_notif').css('display', 'none');
                                        $('#cancel_notif').css('display', 'none');
                                        reset($('#userfile'));
                                    },
                                    error: function(x,e){

                                    }
                                });
                            }else{
                                $.ajax({
                                    type:"post",
                                    url: "<?=base_url()?>support/post_announcement/",
                                    data: {
                                        subject: subject,
                                        message: message,
                                        recipient: recipient
                                    },
                                    success: function(data){
                                        $('#ann_wrap').html(data);
                                        $('#new_post_announce').css('display', 'none');
                                        $('#ann_btn_post').css('display', 'none');
                                        $('#post_notif').css('display', 'block');
                                        $('#update_notif').css('display', 'none');
                                        $('#cancel_notif').css('display', 'none');
                                        reset($('#userfile'));
                                    }
                                });
                            }
                        }
                    }
                });
            }

            function upd_announce(){
                var fileInput = $('#userfile');
                var files = fileInput.prop('files');
                var flabel = $('#uplabel').text();
                var id = $('#upd_id').val();
                var subject = $('#ann_id').val();
                var message = window.parent.tinymce.get('thisarea2').getContent();
                if (getStats('thisarea2').chars > 1000) {
                    bootbox.alert({
                        size: 'small',
                        message: 'Characters too long. 1000 characters only.'
                    });
                    return false;
                }
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to update this Announcement?',
                    callback: function(result){
                        if(result){
                            if(files.length != 0){
                                var fd = new FormData();
                                fd.append('userfile', files[0]);
                                fd.append('id', id);
                                fd.append('subject', subject);
                                fd.append('message', message);

                                $.ajax({
                                    url: "<?=base_url()?>support/update_announcement2/",
                                    data: fd,
                                    contentType: false,
                                    processData:false,
                                    type:'POST',
                                    success: function(response){
                                        $('#ann_wrap').html(response);
                                        $('#new_post_announce').css('display', 'none');
                                        $('#ann_btn_post').css('display', 'none');
                                        $('#update_notif').css('display', 'block');
                                        $('#post_notif').css('display', 'none');
                                        $('#cancel_notif').css('display', 'none');
                                        reset($('#userfile'));
                                    },
                                    error: function(x,e){

                                    }
                                });
                            } else{
                                $.ajax({
                                    type:"post",
                                    url: "<?=base_url()?>support/update_announcement/",
                                    data: {
                                        id: id,
                                        subject: subject,
                                        message: message,
                                        flabel: flabel
                                    },
                                    success: function(data){
                                        $('#ann_wrap').html(data);
                                        $('#new_post_announce').css('display', 'none');
                                        $('#ann_btn_post').css('display', 'none');
                                        $('#update_notif').css('display', 'block');
                                        $('#post_notif').css('display', 'none');
                                        $('#cancel_notif').css('display', 'none');
                                        reset($('#userfile'));
                                    }
                                });
                            }
                        }
                    }
                });
            }

            function cancel_announce(val){
                var id = val;
                bootbox.confirm({
                    size: 'small',
                    message: 'Are you sure you want to cancel this Announcement?',
                    callback: function(result){
                        if(result){
                            $.ajax({
                                type:"post",
                                url: "<?=base_url()?>support/cancel_announcement/",
                                data: {
                                    id: id
                                },
                                success: function(data){
                                    $('#ann_wrap').html(data);
                                    $('#new_post_announce').css('display', 'none');
                                    $('#ann_btn_post').css('display', 'none');
                                    $('#post_notif').css('display', 'none');
                                    $('#update_notif').css('display', 'none');
                                    $('#cancel_notif').css('display', 'block');
                                }
                            });
                        }
                    }
                });
            }

            function new_announce(){
                if( $('#new_post_announce').css('display')=='none'){
                    $('#new_post_announce').css('display', 'block');
                    $('#post_notif').css('display', 'none');
                    $('#update_notif').css('display', 'none');
                    $('#ann_btn_post').css('display', 'block');
                    $('#ann_btn_upd').css('display', 'none');
                    $('#post_notif').css('display', 'none');
                    $('#update_notif').css('display', 'none');
                    $('#cancel_notif').css('display', 'none');
                    $('#sendingpanel').css('display','block');
                }
            }

            function cancel_new_announce(){
                $('#new_post_announce').css('display', 'none');
                $('#ann_btn_post').css('display', 'none');
                $('#ann_btn_upd').css('display', 'none');
                $('#post_notif').css('display', 'none');
                tinymce.get('thisarea2').setContent('');
                $('#ann_id').val('');
            }

            function get_req(val){ 
                $('#housecon').slideUp('fast');
                $.ajax({
                    type:"get",
                    url: "<?=base_url()?>support/get_sub_req/"+val,
                    success: function(data){
                        $('#subtyp_loader').html(data);
                    }
                });
            }

            function house(val){
                var house = $('#subtyp_loader option:selected').val();
                var where;

                switch (val) {
                    case '2':
                        where = "HSEC";
                        break;
                    case '3':
                        where = "DEVT";
                        break;
                    case '4':
                        where = "LOTC";
                        break;
                    case '5':
                        where = "SERV";
                        break;
                    default:
                        where ="";
                }
                if(where != ""){
                    $.ajax({
                        type:"POST",
                        url: "<?=base_url()?>support/housecon_type/",
                        data: { where: where},
                        success: function(data){
                            $('#housetyp_loader').html(data);
                            $('#housecon').slideDown('fast');
                            $('#haus').html(data);
                            $('#haus_loader').slideDown('fast');
                        }
                    });
                } else{
                    $('#housecon').slideUp('fast');
                    $('#haus_loader').slideUp('fast');
                }
            }

            function hauscon(val){
                var house = val;
                if(house == 3){
                    $.ajax({
                        type:"get",
                        url: "<?=base_url()?>support/housecon_type/",
                        success: function(data){
                            $('#haus').html(data);
                            $('#haus_loader').slideDown('fast');
                        }
                    });
                } else{
                    $('#haus_loader').slideUp('fast');
                }
            }

            function date_validator(strdate){
                var strvalidate = /^([1-9]|0[1-9]|1[0-2])([\/])([1-9]|0[1-9]|1\d|2\d|3[01])([\/])(19|20)\d{2}$/;
                var datepick = /^(19|20)\d{2}\-([1-9]|0[1-9]|1[0-2])\-([1-9]|0[1-9]|1\d|2\d|3[01])$/;

                if(!strdate.match(strvalidate)){
                    if(!strdate.match(datepick)){
                        bootbox.alert({
                            size: "small",
                            message: "Invalid date format"
                        });
                        return 1;
                    }
                }
            }

            function update_reqtype(){
                var con_id = $('#con_id').val();
                var con_type = $('#reqtyp option:selected').val();
                var con_sub = $('#subtyp_loader option:selected').val();
                var con_sub2 = $('#housetyp_loader option:selected').val();
                var pstart = $('#pstart').val();
                var pfinish = $('#pfinish').val();
                var compby = $('#compby').val();
                var compdate = $('#compdate').val();
                var validity = 1;
                var valid = $('#c_valid');
                var invalid = $('#c_invalid');
                var text = $('#tasktxt').val();
                var employee = $('#er option:selected').val();
                var department = $('#dpt option:selected').text();

                if(pstart == "" && pfinish == ""){
                    bootbox.alert({
                        size: "small",
                        message: "Please fill up the date field"
                    });
                    return false;
                } else {
                    if(date_validator(pstart) == 1){
                        return false;
                    }if(date_validator(pfinish) == 1){
                        return false;
                    }
                    var start = new Date(pstart);
                    var finish = new Date(pfinish);

                    if(start>finish) {
                        bootbox.alert({
                            size: "small",
                            message: "Please enter a date not less than start date."
                        })
                        return false;
                    }
                }

                if(employee == ""){
                    bootbox.alert({
                        size: "small",
                        message: "Please indicate the Employee Responsible for this concern"
                    });
                     return false;
                }

                if(department == ""){
                    bootbox.alert({
                        size: "small",
                        message: "Please indicate the Department Responsible for this concern"
                    });
                     return false;
                }

                if(!invalid.is(':checked') && !valid.is(':checked')){
                    bootbox.alert({
                        size: "small",
                        message: "Please set the validity of this concern"
                    });
                     return false;
                }

                if(invalid.is(':checked')){
                    validity = 3;
                }

                if(validity == 3){
                    if(compby == "" || compdate == ""){
                        bootbox.alert({
                            size: "small",
                            message: "Please fill up the field required"
                        });
                         return false;
                    }
                }

                if(con_type == "" || con_sub == ""){
                    bootbox.alert({
                        size: "small",
                        message: "Please fill up the field required"
                    });
                     return false;
                }
                if((con_sub == 2 || con_sub == 3 || con_sub == 4 || con_sub == 5) && con_sub2 == "") {
                    bootbox.alert({
                        size: "small",
                        message: "Please select valid options"
                    });
                    return false;
                }
                if( $('#housecon').css('display')=='none'){
                    con_sub2 = "";
                }

                bootbox.confirm({
                    size: "small",
                    message: "Are you sure you want to save changes?",
                    callback: function(result){
                        if(result){
                            $('#cssload-jumping').css('display','block');
                            $.ajax({
                                type:"post",
                                url: "<?=base_url()?>support/update_reqtype/",
                                data: {
                                    con_id: con_id,
                                    con_type: con_type,
                                    con_sub: con_sub,
                                    con_sub2: con_sub2,
                                    validity: validity,
                                    pstart: pstart,
                                    pfinish: pfinish,
                                    employee: employee,
                                    department: department,
                                    text: text,
                                    compby: compby,
                                    compdate: compdate
                                },
                                success: function(data){
                                    $('#cssload-jumping').css('display','none');
                                    $('#load_reply'+con_id+ ' .panel-body').before(data);
                                    $('#load_reply'+con_id+ ' .panel-primary').delay(5000).fadeOut();
                                    $('#mod').modal('toggle');
                                    var button = "<div id='closeCont' class='col-xs-4 col-sm-4 col-md-2' style='padding-right:0;'>" +
                                                    "<br>" +
                                                    "<button type='button' class='btn btn-danger btn-sm form-control input-sm' data-toggle='modal' data-target='#concern_close' title='Close Request'  onclick='cler(" + con_id + ")'> Close Request</button>" +  
                                                "</div>";
                                    if($('#closeCont').length == 0){
                                        $('#msgButtons').append(button);
                                    }
                                }
                            });
                        }
                    }
                });    
            }

        </script>

        <script type="text/javascript" src="<?=base_url()?>assets/tinymce/tinymce.min.js"></script>

        <script type="text/javascript">
            tinymce.init({
                selector: "textarea#thisarea",
            });
            tinymce.init({
                selector: "textarea#thisarea2"
            });
        </script>

    </head>
    <body>
        <!-- header -->

        <div class="wrapper">
            <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <img src="<?=base_url()?>assets/img/tools/vistahome.png" width="100px">
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> <?=$sess_uname?> <span class="caret"></span></a>
                                <ul id="g-account-menu" class="dropdown-menu" role="menu">
                                    <li><a href="<?=base_url()?>support/myProfile">My Profile</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;" onclick="logout()"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /container -->
            </div>
        <!-- /Header -->

        <!-- Main -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3">
                        <!-- Left column -->
                        <strong><i class="glyphicon glyphicon-tasks"></i> Navigation</strong>

                        <hr>
<?php
    $new_reg = $home_new_counter;
    $new_ticket = $home_new_ticket;

    if($new_reg == 0) {
        $new_reg = "";
    }else {
        if ($new_reg>10) {
            $new_reg = "9+";
        }
    }
    if($new_ticket == 0) {
        $new_ticket = "";
    }else {
        if ($new_ticket>10) {
            $new_ticket = "9+";
        }
    } 
     
?>
<?php
$project=array();

$acc = explode("-", $access);
$reg=$acc[0];
$supp=$acc[2];
$rpt=$acc[4];
$mtc=$acc[6];
$ho=$acc[7];
$accnt=$acc[8];
$usr=$acc[9];

?>
                        <ul class="nav nav-stacked">
                            <li class="nav-header">
                                <ul class="nav nav-stacked collapse in" id="userMenu">
                                <?php if($reg!=0){?>
                                    <li class="<?php if($active == 'home') { ?>active <?php } ?>"> <a href="<?=base_url()?>support/reg_list"><i class="glyphicon glyphicon-home"> <div class="badge1" data-badge="<?=$new_reg ?>"></div></i> &nbsp; &nbsp; Registration List </a></li>
                                <?php } ?>  
                                <?php if($supp!=0){?>  
                                    <li class="<?php if($active == 'support') { ?>active <?php } ?>"><a href="<?=base_url()?>support/support_page"><i class="glyphicon glyphicon-comment"> <div class="badge1" data-badge="<?=$new_ticket ?>"></div></i> &nbsp; &nbsp; Support </a></li>
                                <?php } ?>  
                                <?php if($rpt!=0){?>     
                                    <li class="<?php if($active == 'reports') { ?>active <?php } ?>"><a href="<?=base_url()?>support/reports"><i class="glyphicon glyphicon-stats"></i> &nbsp; &nbsp; Reports</a></li>
                                <?php } ?>  
                                <?php if($mtc!=0){?>     
                                    <li class="<?php if($active == 'mtc') { ?>active <?php } ?>"><a href="<?=base_url()?>support/mtc"><i class="glyphicon glyphicon-wrench"></i> &nbsp; &nbsp; Maintenance</a></li>
                                <?php } ?>  
                                <?php if($mtc!=0){?>     
                                    <!-- <li class="nav-header">
                                        <a href="javascript:;" data-toggle="collapse" data-target="#menu3" aria-expanded="false"><i class="glyphicon glyphicon-wrench"></i> &nbsp; &nbsp; Maintenance <i class="glyphicon glyphicon-chevron-right pull-right"></i></a>
                                        <?php if($active == "carousel" || $active == "announcements"){ $collapse = "in"; } else { $collapse = ""; } ?>
                                        <ul class="nav nav-stacked collapse <?=$collapse?>" id="menu3">
                                            <li class="<?php if($active == 'carousel') { ?>active <?php } ?>"><a href="<?=base_url()?>support/carousel"> &emsp;<i class="glyphicon glyphicon-film"></i> &nbsp; &nbsp; Carousel</a></li>
                                            <li class="<?php if($active == 'announcements') { ?>active <?php } ?>"><a href="<?=base_url()?>support/announcements"> &emsp;<i class="glyphicon glyphicon-blackboard"></i> &nbsp; &nbsp; Announcements</a></li>
                                        </ul>
                                    </li> -->
                                <?php } ?>  
                                <?php if($ho!=0){?> 
                                    <li class="<?php if($active == 'homeownerList') { ?>active <?php } ?>"><a href="<?=base_url()?>support/homeownerList"><i class="glyphicon glyphicon-briefcase"></i> &nbsp; &nbsp; Homeowner List</a></li>    
                                <?php } ?>  
                                <?php if($accnt!=0){?>     
                                    <li class="<?php if($active == 'accountSettings') { ?>active <?php } ?>"><a href="<?=base_url()?>support/account_settings"><i class="glyphicon glyphicon-cog"></i> &nbsp; &nbsp; Account Settings</a></li>
                                <?php } ?>  
                                <?php if($usr!=0){?>     
                                    <li class="<?php if($active == 'users') { ?>active <?php } ?>"><a href="<?=base_url()?>support/users"><i class="glyphicon glyphicon-user"></i> &nbsp; &nbsp; Users</a></li>
                                <?php } ?>
                                </ul>
                            </li>
                        </ul>
                        <br>
                    </div>
                <!-- /col-3 -->

                    <div id="page_loader">
                        <img src="<?=base_url()?>assets/img/tools/loader.gif"> 
                        <figcaption style="color:white">Loading...</figcaption>
                    </div>

                    <div class="col-sm-9">

            <!-- column 2 -->
            <!-- ul class="list-inline pull-right">
                <li><a href="#"><i class="glyphicon glyphicon-cog"></i></a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-comment"></i><span class="count">3</span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">1. Is there a way..</a></li>
                        <li><a href="#">2. Hello, admin. I would..</a></li>
                        <li><a href="#"><strong>All messages</strong></a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="glyphicon glyphicon-user"></i></a></li>
                <li><a title="Add Widget" data-toggle="modal" href="#addWidgetModal"><span class="glyphicon glyphicon-plus-sign"></span> Add Widget</a></li>
            </ul> -->
            <?php
                if($active == "home"){

                    $glyphicon = "glyphicon-home";
                    $title = "Registration List";

                }

                if($active == "homeownerList"){

                    $glyphicon = "glyphicon-briefcase";
                    $title = "Homeowner List";

                }

                if($active == "users"){

                    $glyphicon = "glyphicon-user";
                    $title = "Users";

                }

                if($active == "accountSettings"){

                    $glyphicon = "glyphicon-cog";
                    $title = "Account Settings";

                }

                if($active == "support"){

                    $glyphicon = "glyphicon-comment";
                    $title = "Support Page";

                }

                if($active == "mtc"){

                    $glyphicon = "glyphicon-wrench";
                    $title = "Maintenance";

                }

                if($active == "soa"){

                    $glyphicon = "icon-profile";
                    $title = "SOA";

                }

                if($active == "carousel"){

                    $glyphicon = "glyphicon-film";
                    $title = "Carousel";

                }

                if($active == "announcements"){

                    $glyphicon = "glyphicon-blackboard";
                    $title = "Announcements";

                }

                if($active == "reports"){

                    $glyphicon = "glyphicon-stats";
                    $title = "Reports";

                }

                if($active == "myProfile"){

                    $glyphicon = "glyphicon-user";
                    $title = "My Profile";

                }

            ?>
                        <h3 class="panel-title"><i class="glyphicon <?=$glyphicon?>"></i> <?=$title?></h3>
                        <hr>

            