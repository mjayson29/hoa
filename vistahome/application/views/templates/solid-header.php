<!DOCTYPE html>
<html class="full" lang="en">

<head>  

    <title>Vista Home Portal</title>
    <!-- <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-8"> -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="vistaland,vistahome,camella,brittany,lumina,communities,crownasia">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<?=base_url()?>assets/img/logos/icon1.png">
    <!-- Bootstrap Core CSS -->
    
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

    <link href="<?=base_url()?>assets/styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?=base_url()?>assets/styles/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=base_url()?>assets/styles/js/bootstrap.min.js"></script>

    <link href="<?=base_url()?>assets/styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=base_url()?>assets/styles/css/full-slider.css" rel="stylesheet">
    
    <link href="<?=base_url()?>assets/styles/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/styles/js/onepagescroll.min.js"></script>


    <script type="text/javascript">

        $(function(){ $('#solid_head').hide(); });

        $(window).scroll(function() {

            if ($(window).scrollTop() > 60 || $(window).scrollTop() != 0) {
                $('#solid_head').show();//slideDown('fast');
                $('#trans_head').hide();//slideUp('fast');
            } else {
                $('#trans_head').show();//slideDown('fast');
                $('#solid_head').hide();//slideUp('fast');
            }
        });

    </script>

    <script type="text/javascript">
    
        function get_company(val){
            
            if(val != ""){

                $('#page_loader').css('display', 'block');

                $.ajax({
                    type:"GET",
                    url: "<?=base_url()?>signup/get_company/"+val,
                    success: function(data){
                        $('#project').html(data);
                        $('#page_loader').css('display', 'none');
                        return false;
                    }
                });
            }
        }

        function get_proj(val){

            if(val != ""){

                $('#page_loader').css('display', 'block');
                
                $.ajax({
                    type:"GET",
                    url: "<?=base_url()?>signup/get_proj/"+val,
                    success: function(data){
                        $('#project').html(data);
                        $('#page_loader').css('display', 'none');
                        return false;
                    }
                });
            }
        }
        
    </script>
    
</head>
<body>