    <div class="row">

      <div class="col-md-1"> </div>
      <div class="col-md-10">
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary <?php if($btn_active == 'myProfile'){ echo 'active'; }?>" onclick="window.location.href='<?=base_url()?>portal/edit_profile'">Edit Profile</button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary <?php if($btn_active == 'cPassword'){ echo 'active'; }?>" onclick="window.location.href='<?=base_url()?>portal/change_password'">Change Password</button>
          </div>
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary <?php if($btn_active == 'uSec'){ echo 'active'; }?>" onclick="window.location.href='<?=base_url()?>portal/update_security'">Update Security Questions</button>
          </div>
        </div>
      </div>

    </div>