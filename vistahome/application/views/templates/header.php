<!DOCTYPE html>
<html class="full" lang="en">

<head>  

    <title>Vista Home Portal</title>
    <!-- <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-8"> -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="vistaland,vistahome,camella,brittany,lumina,communities,crownasia">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<?=base_url()?>assets/img/logos/icon1.png">
    <!-- Bootstrap Core CSS -->
    

    <!-- Latest compiled and minified CSS -->
    <link href="<?=base_url()?>assets/styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?=base_url()?>assets/styles/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=base_url()?>assets/styles/js/bootstrap.min.js"></script>

    <link href="<?=base_url()?>assets/styles/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=base_url()?>assets/styles/css/full-slider.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/styles/css/column5.css" rel="stylesheet">
    
    <?php if($active == "contacts"){ ?> <link href="<?=base_url()?>assets/styles/css/onepage-scroll.css" rel="stylesheet"> <?php } ?>
    <link href="<?=base_url()?>assets/styles/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/styles/js/onepagescroll.min.js"></script>

    <script type="text/javascript">

        function check_username(val){ 

            // val = 1;
            // val = document.getElementById(roles);

            $.ajax({
                type:"get",
                url: "<?=base_url()?>signup/check_username/"+val,
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#loader').html(data);
                }
            });

        }
        
    </script>


    <script type="text/javascript">

        function reply(val){

            var get_value = val;
            // alert(get_value);

            $.ajax({
                type:"POST",
                url: "<?=base_url()?>portal/reply/"+get_value,
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#load_reply'+get_value).html(data);
                }
            });

            var loader = document.getElementById('load_reply'+get_value);
            if(loader.style.display == 'block')
                $('#load_reply'+get_value).slideUp('fast');
            else
                $('#load_reply'+get_value).slideDown('fast');
                loader.style.display = 'block';
                // $('#load_reply'+get_value).slideDown('slow');

        }

        function send_reply(val){

            var get_value = val;
            var get_concern = $("#concern_msg"+val).val();

            // alert(get_concern);

            var concerns = {

                "concern" : get_concern,
                "concern_id" : get_value

            } 

            $("#load_reply"+get_value).load('<?=base_url()?>portal/reply/', concerns );

            $.ajax({
                type:"POST",
                url: "<?=base_url()?>portal/reply/"+get_value,
                // url: <?=base_url()?>+"personnel/get_items/"+val,
                success: function(data){
                    $('#load_reply'+get_value).html(data);
                }
            });

        }

    </script>

    <script type="text/javascript">

        $(function(){ $('#solid_head').hide(); });

        <?php if($active == "contacts"){ ?>

            $(function(){ $('#solid_head').show(); });

        <?php } ?>

        /*if(<?=$active?> == "contacts"){

            $('#solid_head').show(); 

        }*/

        // $(window).width() <= 568

        $(window).scroll(function() {

            if ($(window).scrollTop() > 60 || $(window).scrollTop() != 0) {
                $('#solid_head').show();//slideDown('fast');
                $('#trans_head').hide();//slideUp('fast');
            } else {
                $('#trans_head').show();//slideDown('fast');
                $('#solid_head').hide();//slideUp('fast');
            }
        });

    </script>

    <script type="text/javascript">

        function changepword(){

            var email = $('#email').val();
            var sec1 = $('#sec1').val();
            var sec2 = $('#sec2').val();
            var ans1 = $('#ans1').val();
            var ans2 = $('#ans2').val();

            if(email == "" || sec1 == "" || sec2 == "" || ans1 == "" || ans2 == ""){

                alert('All fields are required.');

            } else {

                $('#page_loader').css('display', 'block');
                
                var changepword = {

                    "email" : email,
                    "sec1" : sec1,
                    "sec2" : sec2, 
                    "ans1" : ans1,
                    "ans2" : ans2

                }

                $("#notification").load('<?=base_url()?>login/changepword/', changepword );

            }

        }

            function changepword_admin(){

                var email = $('#email').val();
                // var sec1 = $('#sec1').val();
                // var sec2 = $('#sec2').val();
                // var ans1 = $('#ans1').val();
                // var ans2 = $('#ans2').val();

                if(email == "" ){ // || sec1 == "" || sec2 == "" || ans1 == "" || ans2 == ""){

                    alert('All fields are required.');

                } else {

                    $('#page_loader').css('display', 'block');
                    
                    var changepword = {

                        "email" : email
                        // "sec1" : sec1,
                        // "sec2" : sec2, 
                        // "ans1" : ans1,
                        // "ans2" : ans2

                    }

                    $("#notification").load('<?=base_url()?>support/changepword/', changepword );

                }

            }

        function changeusername(){

            var email = $('#email_1').val();
            var captcha = $('#captcha').val();
            var username = $('#username').val();
            var captcha_session = $('#captcha_valid').val();

            if(email == "" || captcha == "" || username == ""){

                alert('All fields are required.');

            } else {

                $('#page_loader').css('display', 'block');

                var changeuname = {

                    "email" : email,
                    "captcha" : captcha,
                    "username" : username,
                    "captcha_session" : captcha_session

                }

                // $('#page_loader').css('display', 'none');
                $("#notification").load('<?=base_url()?>login/changeuname/', changeuname );
                // $('#page_loader').css('display', 'none');

            }

        }

    </script>

    <script type="text/javascript">

        function Logout(){ 

            var logout = confirm('Are you sure you want to Logout?');
            if(logout == true){

                window.location.href="<?=base_url()?>portal/logout";

            } else { }

        }

    </script>
    
</head>
<body>
<div class="wrapper1">
    <!-- <nav class="navbar navbar-inverse navbar-fixed-top solid-header" role="navigation" id="trans_head">
        <div class="container">
            <div class="navbar-header">
                <img src="<?=base_url()?>assets/img/logos/Logo02-02.png" class="company-logo">

            </div>
            <div class="col-sm-2 col-md-3 col-lg-4"></div>
        </div>
    </nav>

    <nav class="navbar navbar-inverse navbar-fixed-top solid-header" role="navigation" id="solid_head">
        <div class="container">
            <div class="navbar-header">
                <img src="<?=base_url()?>assets/img/logos/Logo02-02.png" class="company-logo">

            </div>
            <div class="col-sm-2 col-md-3 col-lg-4"></div>
       </div>
     </nav> -->