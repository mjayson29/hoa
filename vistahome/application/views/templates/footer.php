
	<!-- jQuery -->
    <script src="<?=base_url()?>assets/styles/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>assets/styles/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 10000 //changes the speed
    })
    </script>
    </div> <!-- /* Wrapper */ -->
    <div class="push"></div>
    <nav class="navbar navbar-inverse navbar-fixed-bottom solid-header" role="navigation" style="min-height: 10px;">
        
        <footer class="container" style="padding: 5px;">

            <small class="col-sm-10 col-md-10">&copy; All Rights Reserved. Vista Home Portal.</small>
            <!-- <small class="col-sm-2 col-md-2"><a href="#">Contact Us</a></small> -->

        </footer>

    </nav>

</body>

</html>

