    <div id="myCarousel" class="carousel  carousel-fade slide">

        <!-- Indicators -->
        <ol class="carousel-indicators">

                <?php for($x=0; $x<$count; $x++){ ?>            
               
                    <li data-target="#myCarousel" data-slide-to="<?=$x?>" class="<?php if($x == 0){ echo 'active'; }?>"></li>
                    <!-- <li data-target="#myCarousel" data-slide-to="1"></li> -->

                <?php } ?>            

                <!-- <li data-target="#myCarousel" data-slide-to="2"></li> -->

        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">

            <?php $x=0;?>
            <?php foreach($carousel as $row){ ?> 
            <?php $x++;?>
            <div class="item <?php if($x == 1){ echo 'active'; }?>">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('<?=base_url()?>assets/img/banner/<?=$row['c_filename']?>');"></div>
                <div class="carousel-caption">
                    <h2><?=$row['c_brandname']?></h2>
                </div>
            </div>

            <?php } ?>
           
        </div>
    </div>
     <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    <!-- Page Content -->
    <div id="page_loader" class="page_loader">
        <img src="<?=base_url()?>assets/img/tools/loader.gif"> 
    </div> 
    <article class="container-fluid" id="container-login">

        <div class="col-sm-offset-4 col-sm-4 col-md-4 col-md-offset-4 login-form tab-field" id="login">

            <h5><img src="<?=base_url()?>assets/img/logos/Logo02-02.png" style="width: 40px;"/>&nbsp;&nbsp;<img src="<?=base_url()?>assets/img/logos/vistahome.png" style="width: 120px;"/> <a href="<?=base_url()?>signup/" style="float: right; line-height: 70px;">Sign Up</a></h5>
            <hr>

            <form method="POST" action="<?=base_url('login/verify_login')?>">
                
                <div id="flash-notif"><?php echo validation_errors(); ?></div>

                <input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off" autofocus>
                <br>
                <input type="password" class="form-control" placeholder="Password" name="password" autocomplete="off">
                <br>
                <input type="submit" class="form-control btn btn-primary" value="Login" name="hoa_login">
                <br>
                <br>
                <a href="javascript:;" onclick="changepass()" style="float: left;">Forgot Password</a>
                <a href="javascript:;" onclick="changeuname()" style="float: right;">Forgot Username</a>
                <br>
                <br>

            </form>

        </div>

        <div class="col-sm-offset-4 col-sm-4 col-md-4 col-md-offset-4 login-form tab-field" id="changepass">

            <h4 style="float: left;"><img src="<?=base_url()?>assets/img/logos/Logo02-02.png" style="width: 40px;"/>&nbsp;Change Password</h4>
            <h4><a href="javascript:;" onclick="cancel_cp()" style="float: right; color: red;"><span class="glyphicon glyphicon-remove"></span></a></h4>
            <br>
            <hr>

            <form method="POST" action="<?=base_url()?>login/changepass">
                
                <?php $select = "sq_id, sq_desc"; $table = "ZHOA_secquestion"; $where = "sq_is_deleted = 0"; $orderby = "sq_desc ASC"; ?>
                <?php $secquestion = $this -> Main -> get_data_where($select, $table, $where, $orderby); ?>

                <div id="notification"></div>
                <input type="email" class="form-control" placeholder="email@email.com" name="email" id="email" autocomplete="off" required>
                <br>
                <select class="form-control" name="sec1" id="sec1">
                    <option value="">-- Security Question 1 --</option>
                    <?php foreach($secquestion as $row){ ?>

                    <option value="<?=$row['sq_id']?>"><?=$row['sq_desc']?></option>

                    <?php } ?>
                </select>
                <br>
                <input type="password" class="form-control" placeholder="Security Answer 1" name="ans1" id="ans1" autocomplete="off" required>
                <br>
                <select class="form-control" name="sec2" id="sec2">
                    <option value="">-- Security Question 2 --</option>
                    <?php foreach($secquestion as $row){ ?>

                    <option value="<?=$row['sq_id']?>"><?=$row['sq_desc']?></option>

                    <?php } ?>
                </select>
                <br>
                <input type="password" class="form-control" placeholder="Security Answer 2" name="ans2" id="ans2" autocomplete="off" required>
                <br>
                <input type="button" class="form-control btn btn-primary" value="Submit" name="hoa_changepass" id="hoa_changepass" onclick="changepword()">
                <br>
                <br>

            </form>

        </div>

        <div class="col-sm-offset-4 col-sm-4 col-md-4 col-md-offset-4 login-form tab-field" id="changeuname">

            <?php

                function generateRandomString($length = 4, $letters = '1234567890QWERTYUIOPASDFGHJKLZXCVBNM'){
                    
                    $s = ''; 

                    $lettersLength = strlen($letters)-1;

                    for($i = 0 ; $i < $length ; $i++)
                    {
                        $s .= $letters[rand(0,$lettersLength)];
                    }

                    return $s;
                } 

                $_SESSION['captcha'] = generateRandomString()."-".generateRandomString();

            ?>
            <input type="hidden" name="captcha_valid" id="captcha_valid" value="<?=$_SESSION['captcha']?>">
            <h4 style="float: left;"><img src="<?=base_url()?>assets/img/logos/Logo02-02.png" style="width: 40px;"/>&nbsp;Change Username</h4>
            <h4><a href="javascript:;" onclick="cancel_cu()" style="float: right; color: red;"><span class="glyphicon glyphicon-remove"></span></a></h4>
            <br>
            <hr>

            <form method="POST" action="<?=base_url()?>login/changeUsername">
                
                <div id="notification"></div>
                <input type="email" class="form-control" placeholder="email@email.com.ph" name="email" id="email_1" autocomplete="off" required>
                <br>
                <div class="mod_captcha"><?=$_SESSION['captcha'];?></div>
                <br>
                <input type="text" class="form-control" placeholder="Captcha" name="captcha" id="captcha" autocomplete="off" required>
                <br>
                <input type="text" class="form-control" placeholder="New Username" name="username" id="username" autocomplete="off" required>
                <br>
                <input type="button" class="form-control btn btn-primary" value="Submit" name="hoa_changeuname" onclick="changeusername()">
                <br>
                <br>

            </form>

        </div>

    </article>

    <script type="text/javascript">

        $(function() {

            $('#flash-notif').hide().slideDown('fast').delay('1500').slideUp('fast');
            $('#login').show();
            $('#changepass').hide();
            $('#changeuname').hide();
                             
        });

        function cancel_cp(){

            $('#login').slideDown('fast');
            $('#changepass').slideUp('fast');

        }

        function cancel_cu(){

            $('#login').slideDown('fast');
            $('#changeuname').slideUp('fast');

        }

        function changepass(){

            $('#login').slideUp('fast');
            $('#changepass').slideDown('fast');

        }

        function changeuname(){

            $('#login').slideUp('fast');
            $('#changeuname').slideDown('fast');

        }

    </script>
