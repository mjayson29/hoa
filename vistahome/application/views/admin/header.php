<!DOCTYPE html>
<html lang="en" charset="utf-8">
<head>
	<title></title>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/bootstrap/css/bootstrap-theme.min.css">
	<script src="<?=base_url()?>assets/admin/styles/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/normalize.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/grid.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/grid2.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/inner_grid.css">
	<!--link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/custom_grid.css"-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/icomoon/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/lytebox/lytebox.css">
	<script type="text/javascript" src="<?=base_url()?>assets/admin/lytebox/lytebox.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/admin/bx-slider/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/bx-slider/jquery.bxslider.css">

	<link rel="stylesheet" href="<?=base_url()?>assets/admin/datepicker/jquery-ui.css" /> 
    <!--script src="<?=base_url()?>assets/datepicker/jquery-1.9.1.js"></script-->
    <script src="<?=base_url()?>assets/admin/datepicker/jquery-ui.js"></script>
    <script src="<?=base_url()?>assets/admin/js/datepicker.js"></script>
    <!--script src="https://code.jquery.com/jquery-2.1.1.min.js"></script--> 
	<script type="text/javascript" src="<?=base_url()?>assets/admin/beaverSlider/js/beaverslider.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/admin/beaverSlider/js/beaverslider-effects.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/beaverSlider/css/style.css">
	

	<script language="javascript" type="text/javascript">
	function limitText(limitField, limitCount, limitNum) {
		if (limitField.value.length > limitNum) {
			limitField.value = limitField.value.substring(0, limitNum);
		} else {
			limitCount.value = limitNum - limitField.value.length;
		}
	}
	</script>
	
	<script>

		$(function(){
	      var slider = new BeaverSlider({
		    structure: {
		        container: {
		            id: "login-slider",
		            width: 740,
		            height: 100,
		            margin-left: 200
		        },
		       /* controls: {
		            previewMode: false,
		            align: "center",
		            containerClass: "control-container-img",
		            elementClass: "control-element-img",
		            elementActiveClass: "control-element-active-img"
		        }*/
		    },
		    content: {
		        images: [

		            "<?=base_url()?>assets/admin/img/featured/aa.jpg",
		            "<?=base_url()?>assets/admin/img/featured/cc.jpg",
		            "<?=base_url()?>assets/admin/img/featured/dd.jpg",
		            "<?=base_url()?>assets/admin/img/featured/bb.jpg"

		          ]   
		    },
		    animation: {
		        waitAllImages: true,
		        effects: effectSets["slider: big set 2"],
		        initialInterval: 1000,
		        interval: 2000
		    },
		    events: {
		        afterSlideStart: function(slider) {
		            var scrollTo = (slider.currentImage - 2) * $(".control-element-active-img").width();
		            
		            $(".control-container-img").animate({ scrollLeft: scrollTo }, 400);
		        }
		    }
		});   
	});

	</script>

	<script type="text/javascript">

		function PostData() {
   			   // 1. Create XHR instance - Start
			   var xhr;
			   if (window.XMLHttpRequest) {
			       xhr = new XMLHttpRequest();
			    }
			   else if (window.ActiveXObject) {
			       xhr = new ActiveXObject("Msxml2.XMLHTTP");
			   }
			   else {
			       throw new Error("Ajax is not supported by this browser");
			   }
			   // 1. Create XHR instance - End
			   
			   // 2. Define what to do when XHR feed you the response from the server - Start
			   xhr.onreadystatechange = function () {
			       if (xhr.readyState === 4) {
			           if (xhr.status == 200 && xhr.status < 300) {
			               document.getElementById('cat').innerHTML = xhr.responseText;
			           }
			       }
			   }
		    // 2. Define what to do when XHR feed you the response from the server - Start
		    var get_class = document.getElementById("get_class").value;
		    // 3. Specify your action, location and Send to the server - Start 
		    xhr.open('POST', <?=base_url('portal/new_ticket')?>);
		    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		    xhr.send("get_class=" + get_class);
			    // 3. Specify your action, location and Send to the server - End
			}

	</script>

</head>
<body>

	<div id="bg-header"><span></span></div>	
	<!-- <div id="bg-header1"><span></span></div> -->
	<!-- <div id="bg-header3"><span></span></div> -->
	<!-- <div id="bg-header2"></div> -->

		<div id="main-wrapper" class="container_12" >

			<header class="grid_12">
					
				<div id="header-left">

					<div class="header-logo" style="margin-left: 170px;">

						<img src="<?=base_url()?>assets/img/tools/vistahome.png">
						<!-- <h3>Homeowner's Portal</h3> -->

					</div>

					<!--div class="header-line"></div-->

				</div>

				

				<!--script type="text/javascript">

					$(document).ready(function(){
					  $('.bxslider').bxSlider();
					});

				</script-->

			</header>

