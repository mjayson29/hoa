<script type="text/javascript">

$(document).ready(function () {
	//called when key is pressed in textbox
	$("#submitBtn").click(function(){
		if(confirm('Do you really want to confirm this?')) {
			$('#status').val("confirm");
			$("#form").submit();
		}
	});

		$("#rejectBtn").click(function(){
			$("#rejectHeader").toggle(function(){
			});
			$("#rejectField").toggle(function(){
			});
			$("#rejectSpace").toggle(function(){
			});
			$("#rejectSubmit").toggle(function(){
			});
			$("#submitArea").toggle(function(){
			});
			//if(confirm('Do you really want to reject this?')) {
			// do things if OK
			//}
		});

	  $("#cancelRejectBtn").click(function(){
		  $("#rejectHeader").toggle(function(){
		  });
		  $("#rejectField").toggle(function(){
		  });
		  $("#rejectSpace").toggle(function(){
		  });
		  $("#rejectSubmit").toggle(function(){
		  });
		  $("#submitArea").toggle(function(){
		  });
		  //if(confirm('Do you really want to reject this?')) {
		  // do things if OK
			 //}
	  });

	  	$("#submitRejectBtn").click(function(){
	  		if(confirm('Do you really want to reject this?')) {
	  			$('#status').val("reject");
	  			$("#form1").submit();
	  		}
		  });

	  		 
	  		 
});

	</script>
	<section class="grid_11" >
	<article class="content-fields">
	<div class="panel panel-default	">
	<div class="panel-heading user" style="padding:5px;">
	<label class="label label-primary stat-title"><span class="icon-notebook"></span> Registration :</label>
	<label class="label label-success stat"><a href="<?php echo base_url()?>admin/registration/" class="<?php if($isActive == "open"){echo"active";} ?>"><span class="icon-user-add"> </span> New</a></label>
	<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/out_reg/" class="<?php if($isActive == "close"){echo"active";} ?>"><span class="icon-file"> </span> Outstanding</a></label>
	<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/con_reg/" class="<?php if($isActive == "valid"){echo"active";} ?>"><span class="icon-check"> </span> Confirmed</a></label>
	<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/rej_reg/" class="<?php if($isActive == "invalid"){echo"active";} ?>"><span class="icon-trash"> </span> Rejected</a></label>
	</div>
	</div>
	</article>

	<article class="grid_11">
		<?=$notification?>
	</article>
	
	<article class="grid_11">
		<h2 class="form-title"><span class="icon-info"></span> BUYER REGISTRATION FORM</h2>
		<h3 class="form-sub-title">SIGN UP DATE  <?php echo substr($buyerInfo->row()->bi_regdate,0,10); ?></h3>
	</article>
	
	<article class="grid_3 form-fields">
		<label class="form-control">Company</label>
		<label class="form-control">Unit type</label>
		<label class="form-control">Subdivision / Project Name</label>
		<label class="form-control">Blk / Floor No.</label>
	</article>
	<article class="grid_3 form-fields">
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->tc_brand; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->u_desc; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_project; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_blklot; ?>"></label>
	</article>
	<article class="grid_1 form-fields"> &nbsp;</article>
	<article class="grid_11">
		<h3 class="form-sub-title"></h3>
		<h3 class="form-field-title"><span class="icon-profile"></span> Personal Information</h3>
	</article>
	<article class="grid_3 form-fields">
		<label class="form-control">Last Name</label>
		<label class="form-control">First Name</label>
		<label class="form-control">Middle Name</label>
		<label class="form-control">Birthdate</label>
		<label class="form-control">Street</label>
		<label class="form-control">City</label>
		<label class="form-control">Zipcode</label>
	</article>
	<article class="grid_3 form-fields">
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_lname; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_fname; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_mname; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_bdate; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_add1; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_add2; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_zip; ?>"></label>
	</article>
	<article class="grid_11">
		<h3 class="form-sub-title"></h3>
		<h3 class="form-field-title"><span class="icon-address-book"></span> Contact Information</h3>
	</article>
	<article class="grid_3 form-fields">
		<label class="form-control">Contact Number</label>
		<label class="form-control">Email</label>
	</article>
	<form action="<?php echo base_url()?>admin/reg_info/<?php echo $isActive?>/<?php echo $buyerInfo->row()->bi_id; ?>/1" method="POST" id="form">
	</form>
	<article class="grid_3 form-fields">
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_contact; ?>"></label>
		<label class="form-control"><input type="text" class="form-control" name="unit" autocomplete="off" readonly value="<?php echo $buyerInfo->row()->bi_email; ?>"></label>
	</article>
	<article class="grid_1 form-fields"> &nbsp;</article>
	
	<?php if(count($buyerInfo->row()->ct_id) != ""){
		?>
	<article class="grid_11">
		<h3 class="form-sub-title"></h3>
		<h3 class="form-field-title"><span class="icon-message"></span> Concern(Optional)</h3>
	</article>	
	<article class="grid_2 form-fields">&nbsp;</article>
	<article class="grid_8 form-fields">
		<textarea name="limitedtextarea" style="height: 100px;" class="form-control" readonly><?php echo $buyerInfo->row()->ct_desc; ?></textarea><br>
	</article>
		<?php 
	}?>
	
	<?php if($isActive == "invalid"){
		?>
	<article class="grid_11" id="rejectHeader">
		<h3 class="form-sub-title"></h3>
		<h3 class="form-field-title"><span class="icon-message"></span> Reason</h3>
	</article>
	<article class="grid_2 form-fields" id="rejectSpace">&nbsp;</article>
	<form action="<?php echo base_url()?>admin/reg_info/<?php echo $isActive?>/<?php echo $buyerInfo->row()->bi_id; ?>/2" method="POST" id="form1">
	<article class="grid_8 form-fields" id="rejectField">
		<textarea name="reason" style="height: 100px;" class="form-control" ><?php echo $buyerInfo->row()->bi_reason;?></textarea><br>
	</article>
	</form>
	<?php 
		}else{
	?>
			<article class="grid_11" id="rejectHeader" style="display:none;">
				<h3 class="form-sub-title"></h3>
				<h3 class="form-field-title"><span class="icon-message"></span> Reason</h3>
			</article>
			<article class="grid_2 form-fields" id="rejectSpace" style="display: none;">&nbsp;</article>
			<form action="<?php echo base_url()?>admin/reg_info/<?php echo $isActive?>/<?php echo $buyerInfo->row()->bi_id; ?>/2" method="POST" id="form1">
			<article class="grid_8 form-fields" id="rejectField" style="display:none;">
			<textarea name="reason" style="height: 100px;" class="form-control" placeholder="Write your reason here."></textarea><br>
			</article>
			</form>
	<article class="grid_3" style="float: right; margin-right: -70px; display:none;" id="rejectSubmit">
		<input type="submit" value="Submit" class="btn btn-primary" id="submitRejectBtn">
		<input type="button" value="Cancel" class="btn btn-danger" id="cancelRejectBtn">
	</article>
	<?php 
		}
	?>
	
	<article class="grid_11">
		<h3 class="form-sub-title"></h3>
	</article>
	<br>
	<article class="grid_3" style="float:right; margin-right: -70px; "  id="submitArea">
		<input type="submit" value="Confirm" class="btn btn-primary" id="submitBtn">
		<?php 
		if($isActive != "invalid"){
		?>
			<input type="button" value="Reject" class="btn btn-danger" id="rejectBtn">
		<?php 
		}
		?>
	</article>
	<article class="grid_11">
		<h3 class="form-field-title" style="background: none repeat scroll 0 0 #5cb85c;"> SAP Information related to property</h3>
	</article>
	<article class="grid_3 form-fields">
		<label class="form-control" style="font-weight: normal; color: black;">Company</label>
		<label class="form-control" style="font-weight: normal; color: black;">Project</label>
		<label class="form-control" style="font-weight: normal; color: black;">Blk / lot</label>
		<label class="form-control" style="font-weight: normal; color: black;">Customer Number</label>
		<label class="form-control" style="font-weight: normal; color: black;">Customer Name</label>
		<label class="form-control" style="font-weight: normal; color: black;">Note</label>
	</article>
	<article class="grid_3 form-fields">
		<label class="form-control" style="font-weight: bold; color: black;"><?php echo $buyerInfo->row()->BUTXT; ?></label>
		<label class="form-control" style="font-weight: bold; color: black;"><?php echo $buyerInfo->row()->XWETEXT; ?></label>
		<label class="form-control" style="font-weight: bold; color: black;"><?php echo $buyerInfo->row()->REFNO; ?></label>
		<label class="form-control" style="font-weight: bold; color: black;"><?php echo $buyerInfo->row()->KUNNR; ?></label>
		<label class="form-control" style="font-weight: bold; color: black;"><?php echo $buyerInfo->row()->NAME1; ?></label>
		<label class="form-control" style="font-weight: bold; color: black;"><?php //echo $buyerInfo->row()->XWETEXT; ?></label>
	</article>
</section>


	
