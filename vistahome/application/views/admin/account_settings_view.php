<section class="grid_11">

		<div class="panel panel-default" id="b_list">

			<div class="panel-heading"><span class="icon-cog"> </span><?=ucwords($title)?></div>

		</div>

		<div class="panel panel-default" id="b_list">

			<div class="panel-heading"><span class="icon-pencil"> </span>Change Password</div>
			<?php 
			if(validation_errors()){
				?>
				<div class='notification-false'>
				<?php echo validation_errors();?>
				</div>
				<?php 
				
			}
			 ?>
			 <?php echo $notification;?>
			<div class="tab-field">

				<form method="post" action="<?=base_url()?>admin/account_settings">

					<label> Old Password</label> <input type="password" class="form-control" name="pword" value="<?=set_value('pword')?>"><br>
					<label> New Password</label> <input type="password" class="form-control" name="npword" value="<?=set_value('npword')?>"><br>
					<label> Retype-Password</label> <input type="password" class="form-control" name="cpword" value="<?=set_value('cpword')?>"><br>
					<br>
					<input type="submit" name="update" value="Submit" class="btn btn-primary">

				</form>

			</div>

		</div>


</section>

