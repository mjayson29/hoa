<script type="text/javascript">

$(document).ready(function () {
	//called when key is pressed in textbox
	$("#confirmBtn").click(function(){
		if(confirm('Do you really want to confirm this?')) {
			$("#form").submit();
		}
	});

	$("#confirmRejectBtn").click(function(){
		if(confirm('Do you really want to reject this?')) {
			$("#form").submit();
		}else{
			return false;
			}
	});

	$("#rejectBtn").click(function(){
		$("#rejectArea").toggle(function(){
		});
	});

	$("#cancelBtn").click(function(){
		$("#rejectArea").toggle(function(){
		});
	});
	
	
	
	

	

	 

	  		 
	  		 
});

	</script><section class="grid_11" >
	<article class="content-fields">
	<div class="panel panel-default	">
	<div class="panel-heading user" style="padding:5px;">
	<label class="label label-primary stat-title"><span class="icon-notebook"></span> Registration :</label>
	<label class="label label-success stat"><a href="<?php echo base_url()?>admin/registration/" class="<?php if($isActive == "open"){echo"active";} ?>"><span class="icon-user-add"> </span> New</a></label>
	<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/out_reg/" class="<?php if($isActive == "close"){echo"active";} ?>"><span class="icon-file"> </span> Outstanding</a></label>
	<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/con_reg/" class="<?php if($isActive == "valid"){echo"active";} ?>"><span class="icon-check"> </span> Confirmed</a></label>
	<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/rej_reg/" class="<?php if($isActive == "invalid"){echo"active";} ?>"><span class="icon-trash"> </span> Rejected</a></label>
	</div>
	</div>
	</article>
	<?php echo $notification;?>
	<div class="panel panel-default" id="b_list">

				<div class="panel-heading"><span class="icon-pie"> </span>BUYER REGISTRATION FORM</div>

			</div>
	<form action="<?php echo base_url()?>admin/reg_info/<?php echo $isActive?>/<?php echo $buyerInfo->row()->bi_id; ?>/1" method="POST" id="form">
	<input type="hidden" name="email" value="<?php echo $buyerInfo->row()->bi_email; ?>">
	</form>
	<div class="panel panel-default" id="b_list">

				<div class="panel-heading"><span class="icon-profile"> </span>SIGN UP DATE  <?php $regDate = strtotime($buyerInfo->row()->bi_regdate); echo date('m/d/Y', $regDate) ;?></div>
				
				<div class="tab-field">
					
					<label>Company</label>
					<?php echo $buyerInfo->row()->tc_brand; ?>
					<br/>
					
					<label>Unit type</label>
					<?php echo $buyerInfo->row()->u_desc; ?>
					<br/>
					
					<label>Project Name</label>
					<?php 
					$this->load->model('Buyer_Model');
					$projects = $this->Buyer_Model->get_projects_by_cust_no($buyerInfo->row()->bi_cust_no);
					$projCount = count($projects->result());
					$counter = 0;
					if(!empty($projects)){
						foreach($projects->result() as $key2){
							$counter++;
							echo $key2->XWETEXT;
							if($counter != $projCount){
								echo ", ";
							}else{
								echo " ";
							}
							
						}
					} ?>
					<br/>
					
					<label>Blk/Lot</label>
					<?php echo $buyerInfo->row()->bi_blklot; ?>
					<br/>
				</div>
				</div>
				
	<div class="panel panel-default" id="b_list">
				<div class="panel-heading"><span class="icon-profile"> </span>PERSONAL INFORMATION</div>
				
				<div class="tab-field">
					
					<label>Last Name</label>
					<?php echo $buyerInfo->row()->bi_lname; ?>
					<br/>
					
					<label>First Name</label>
					<?php echo $buyerInfo->row()->bi_fname; ?>
					<br/>
					
					<label>Middle Name</label>
					<?php echo $buyerInfo->row()->bi_mname; ?>
					<br/>
					
					<label>Birthdate</label>
					<?php $birthDate = strtotime($buyerInfo->row()->bi_bdate); echo date('m/d/Y', $birthDate) ;?>
					<br/>
					
					<label>Street</label>
					<?php echo $buyerInfo->row()->bi_add1; ?>
					<br/>
					
					<label>City</label>
					<?php echo $buyerInfo->row()->bi_add2; ?>
					<br/>
					
					<label>Zipcode</label>
					<?php echo $buyerInfo->row()->bi_zip; ?>
					<br/>
				</div>
	</div>
	
	<div class="panel panel-default" id="b_list">
				<div class="panel-heading"><span class="icon-profile"> </span>CONTACT INFORMATION</div>
				
				<div class="tab-field">
					
					<label>Contact Number</label>
					<?php echo $buyerInfo->row()->bi_contact; ?>
					<br/>
					
					<label>Email</label>
					<?php echo $buyerInfo->row()->bi_email; ?>
					<br/>
					
				</div>
	</div>
	<div class="panel panel-default" id="b_list">
				<div class="panel-heading"><span class="icon-profile"> </span>CONCERN (Optional)</div>
				
				<div class="tab-field">
					
					
					<?php echo $buyerInfo->row()->ct_desc; ?>
					<br/>
					
					
					
				</div>
	</div>
	<?php if($isActive == "invalid"){?>
			<div class="panel panel-default">
				<div class="panel-heading"><span class="icon-notebook"></span> Rejecting Reason <a href="javascript:;" onclick="edit_reason()"><span class="icon-pencil" style="float: right;"></span></a></div>
				<div class="edit-reason" style="display: none;">
					<?php //foreach($this->Support_admin->fetch_rejreason($owner_id) as $reason) { ?>
						<textarea id="thisarea" class="form-control" name="edit_reason"> <?=$reason['r_desc']?> </textarea>
					<?php //} ?>
					<div class="tab-field">
						<input type="submit" name="save" value="Save" class="btn btn-primary">
						<input type="button" name="" value="Cancel" class="btn btn-danger" onclick="cancel1()">
					</div>
				</div>
				<div class="fetch_reason tab-field">
					<?php echo $buyerInfo->row()->bi_reason; ?>
					</br>
				</div>
			</div>
	<?php } ?>
	<?php if($isActive != "valid"){
		?>
		<div class="" id="b_list" align="center">
			<input type="submit" value="Confirm" class="btn btn-primary" id="confirmBtn" style="width: 100px;">
			<input type="button" value="Reject" class="btn btn-danger" id="rejectBtn" style="width: 100px;">
		</div>
		<?php 
	}?>
	
	<br>

		<div class="panel panel-default" id="rejectArea" style="display: none;">
		<form action="<?php echo base_url()?>admin/reg_info/<?php echo $isActive?>/<?php echo $buyerInfo->row()->bi_id; ?>/2" method="POST" id="form1">
				<input type="hidden" name="email" value="<?php echo $buyerInfo->row()->bi_email; ?>">
				<div class="panel-heading"><span class="icon-notebook"></span> Rejecting Reason</div>

				<div class="edit-reason">
					<textarea id="thisarea" class="form-control" name="reason"></textarea>
				<div class="tab-field">

					<input type="submit" name="invalid" value="Confirm Reject" class="btn btn-success" id="confirmRejectBtn">
					<input type="button" name="" value="Cancel" class="btn btn-primary" id="cancelBtn">

				</div>
				</div>
		</form>
		</div>
	
	
	<div class="panel panel-default" id="b_list">
				<div class="panel-heading"><span class="icon-profile"> </span>SAP Information related to property</div>
				
				<div class="tab-field">
					
					<label>Company</label>
					<?php echo $buyerInfo->row()->BUTXT; ?>
					<br/>
					
					<label>Project</label>
					<?php
					$this->load->model('Buyer_Model');
					$projects = $this->Buyer_Model->get_projects_by_cust_no($buyerInfo->row()->bi_cust_no);
					$projCount = count($projects->result());
					$counter = 0;
					if(!empty($projects)){
						foreach($projects->result() as $key2){
							$counter++;
							echo $key2->XWETEXT;
							if($counter != $projCount){
								echo ", ";
							}else{
								echo " ";
							}
							
						}
					}
					?>
					<br/>
					<label>Blk / lot</label>
					<?php echo $buyerInfo->row()->REFNO; ?>
					<br/>
					
					<label>Customer Number</label>
					<?php echo $buyerInfo->row()->KUNNR; ?>
					<br/>
					
					<label>Customer Name</label>
					<?php echo $buyerInfo->row()->NAME1; ?>
					<br/>
					
					<label>Note</label>
					<?php //echo $buyerInfo->row()->KUNNR; ?>
					<br/>
					
				</div>
	</div>
	
	
</section>

			
	