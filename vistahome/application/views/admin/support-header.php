<!DOCTYPE html>
<html lang="english">
<head>
	<title>HOA Portal</title>

	<meta charset="UTF-8">

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/bootstrap/css/bootstrap-theme.min.css">
	<script src="<?=base_url()?>assets/admin/styles/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/normalize.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/grid2.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/inner_grid.css">
	<!--link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/styles/custom_grid.css"-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/styles/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/icomoon/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/lytebox/lytebox.css">
	<script type="text/javascript" src="<?=base_url()?>assets/admin/lytebox/lytebox.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/admin/bx-slider/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/bx-slider/jquery.bxslider.css">

	<script type="text/javascript" src="<?=base_url()?>assets/admin/beaverSlider/js/beaverslider.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/admin/beaverSlider/js/beaverslider-effects.js"></script>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/beaverSlider/css/style.css">

	<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jQuery.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/admin/js/ajax.js"></script>
	
	<link rel="stylesheet" href="<?=base_url()?>assets/admin/datepicker/jquery-ui.css" /> 
    <script src="<?=base_url()?>assets/admin/datepicker/jquery-1.9.1.js"></script>
    <script src="<?=base_url()?>assets/admin/datepicker/jquery-ui.js"></script>
    <script src="<?=base_url()?>assets/admin/js/datepicker.js"></script>
	
	
	<script>

		$(function(){
	      var slider = new BeaverSlider({
		    structure: {
		        container: {
		            id: "login-slider",
		            width: 890,
		            height: 100
		        },
		        /*controls: {
		            previewMode: false,
		            align: "center",
		            containerClass: "control-container-img",
		            elementClass: "control-element-img",
		            elementActiveClass: "control-element-active-img"
		        }*/
		    },
		    content: {
		        images: [

		            "<?=base_url()?>assets/admin/img/featured/aa.jpg",
		            "<?=base_url()?>assets/admin/img/featured/cc.jpg",
		            "<?=base_url()?>assets/admin/img/featured/dd.jpg",
		            "<?=base_url()?>assets/admin/img/featured/bb.jpg"

		          ]   
		    },
		    animation: {
		        waitAllImages: true,
		        effects: effectSets["slider: big set 2"],
		        initialInterval: 1000,
		        interval: 2000
		    },
		    events: {
		        afterSlideStart: function(slider) {
		            var scrollTo = (slider.currentImage - 2) * $(".control-element-active-img").width();
		            
		            $(".control-container-img").animate({ scrollLeft: scrollTo }, 400);
		        }
		    }
		});   
	});

	</script>

	<script type="text/javascript">
	
	$(document).ready(function(){
		$("#ug").change(function(){
			var acctype = this.value;
			if(acctype==3){
				$("#is_cc").val(1);
				$("#for_cc").removeClass("hidden");
				$("#allowed_tickets").removeClass("hidden");
				$("#for_cc").addClass("box-block");
				$("#allowed_tickets").addClass("box-block");
				document.getElementById("hid_ug").value = 3;
			} else {
				$("#is_cc").val(0);
				$("#for_cc").removeClass("box-block");
				$("#allowed_tickets").removeClass("box-block");
				$("#for_cc").addClass("hidden");
				$("#allowed_tickets").addClass("hidden");
				document.getElementById("hid_ug").value = "";

			}
		});
	});
	
	</script>

	<script type="text/javascript" src="<?=base_url()?>assets/admin/tinymce/tinymce.min.js"></script>

	<script type="text/javascript">
		tinymce.init({
		    selector: "textarea#thisarea"
		});
	</script>
	
	<script type="text/javascript">
		// tinymce.init({
		// 	selector: "textarea#msg",
		// 	theme: "modern",
		// 	plugins: [
		// 		 "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
		// 		 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		// 		 "save table contextmenu directionality emoticons template paste textcolor"
		//    ],
		//    content_css: "css/content.css",
		//    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | fontsizeselect", 
		//    style_formats: [
		// 		{title: 'Bold text', inline: 'b'},
		// 		{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
		// 		{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
		// 		{title: 'Example 1', inline: 'span', classes: 'example1'},
		// 		{title: 'Example 2', inline: 'span', classes: 'example2'},
		// 		{title: 'Table styles'},
		// 		{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		// 	]
		//  }); 

	</script>

</head>
<body>

	<div id="bg-header"><span></span></div>
	<!-- <div id="bg-header1"><span></span></div> -->
	<!-- <div id="bg-header3"></div> -->

		<div id="main-wrapper" class="container_13">

			<header class="grid_12">
					
				<div id="header-left">

					<div class="header-logo">

						<img src="<?=base_url()?>assets/img/tools/hoa2.png">
						<!-- <h3>Homeowner's Portal</h3> -->

					</div>

					<!--div class="header-line"></div-->

				</div>

				<div id="header-right">

					<!--ul class="bxslider">

						<li><img src="<?=base_url()?>assets/img/sample1.jpg" /></li>
						<li><img src="<?=base_url()?>assets/img/sample2.jpg" /></li>
						<li><img src="<?=base_url()?>assets/img/sample3.jpg" /></li>

					</ul-->

					<div id="login-slider"></div>
					<!--h1>Feature Image</h1-->

				</div>

				<!--script type="text/javascript">

					$(document).ready(function(){
					  $('.bxslider').bxSlider();
					});

				</script-->

			</header>
		
		<!-- <div class="grid_12">&nbsp;</div> -->



