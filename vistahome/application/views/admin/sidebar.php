<aside class="grid_3 user-sidebar">
	<div class="panel panel-default">

	    <div class="panel-heading user"><span class="icon-user"> </span>Hi <?=ucwords($sess_uname)?></span></div>
	   
	    <!--div class="user-container">

	    	<?php foreach($userdata as $row){ ?>

	    		<h5><span class	="icon-email">&nbsp;</span><?=$row['email']?></h5>
	    		<h5><span class	="icon-users">&nbsp;</span><?=ucwords($row['dept'])?></h5>
	    		<h5><span class	="icon-users">&nbsp;</span><?=ucwords($row['office_loc'])?></h5>

	    	<?php } ?>

	    </div-->


	</div>

	<div class="panel panel-default">
		  
	    <div class="panel-heading"><span class="icon-numbered-list"> </span>Navigation</div>
		  
		<ul class="list-group">
		  	<li class="list-group-item <?php if($active == 'registration'){ echo 'active'; }?>" onclick="window.location.href='<?=base_url()?>admin/registration'"><span class="icon-notebook">&nbsp;</span>Registration</li>
			<li class="list-group-item <?php //if($active == 'support'){ echo 'active'; }else{}?>" onclick="window.location.href='<?=base_url()?>support/support_center'"><span class="icon-phone">&nbsp;</span>Support</li>
			<li class="list-group-item <?php //if($active == 'reports'){ echo 'active'; }else{}?>" onclick="window.location.href='<?=base_url()?>support/reports'"><span class="icon-globe">&nbsp;</span>Reports</li>
			<li class="list-group-item <?php if($active == 'settings'){ echo 'active'; }?>" onclick="window.location.href='<?=base_url()?>admin/account_settings'"><span class="icon-cog2">&nbsp;</span>Account Settings</li>
			<li class="list-group-item  " onclick="window.location.href='<?=base_url()?>support/account_settings'"><span class="icon-info">&nbsp;</span>Maintenance</li>
			<li class="list-group-item" onclick="logout()"><span class="icon-switch">&nbsp;</span>Logout</li>
			
		</ul>

	</div>

	<script type="text/javascript">
		
		function logout(){

			var prompt;
			var logout = confirm('Logout?');
			if(logout == true){

				window.location.href="<?=base_url('admin/logout')?>";

			} else {

				

			}

		}

	</script>

</aside>