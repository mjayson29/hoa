<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HOA Support</title>
<style type="text/css">
<!--
@import url(http://fonts.googleapis.com/css?family=Josefin+Sans+Std+Light);
html,body{height:100%;}
*{outline:none;}
body{margin:0px; padding:0px; background:#000;}
#toolbar{position:fixed; z-index:3; right:10px; top:-50px; padding:1px; background:url(fs_img_g_bg.png);}
#toolbar img{border:none;}
#img_title{position:fixed; z-index:3; left:10px; top:10px; padding:10px; background:url(fs_img_g_bg.png); color:#FFF; font-family:'Josefin Sans Std Light', arial, serif; font-size:24px; text-transform:uppercase;}
#bg{position:fixed; z-index:1; overflow:hidden; width:100%; height:100%;}
#bgimg{display:none; -ms-interpolation-mode: bicubic;}
#preloader{position:relative; z-index:3; width:32px; padding:20px; top:80px; margin:auto; background:#000;}
#thumbnails_wrapper{z-index:2; position:fixed; bottom:0; width:100%; background:url(empty.gif); /* stupid ie needs a background value to understand hover area */}
#outer_container{position:relative; padding:0; width:100%; margin:40px auto;}
#outer_container .thumbScroller{position:relative; overflow:hidden; background:url(fs_img_g_bg.png);}
#outer_container .thumbScroller, #outer_container .thumbScroller .container, #outer_container .thumbScroller .content{height:170px;}
#outer_container .thumbScroller .container{position:relative; left:0;}
#outer_container .thumbScroller .content{float:left;}
#outer_container .thumbScroller .content div{margin:5px; height:100%;}
#outer_container .thumbScroller img{border:5px solid #fff;}
#outer_container .thumbScroller .content div a{display:block; padding:5px;}

.nextImageBtn, .prevImageBtn{display:block; position:absolute; width:50px; height:50px; top:50%; margin:-25px 10px 0 10px; z-index:3; filter:alpha(opacity=40); -moz-opacity:0.4; -khtml-opacity:0.4; opacity:0.4;}
.nextImageBtn:hover,.prevImageBtn:hover{filter:alpha(opacity=80); -moz-opacity:0.8; -khtml-opacity:0.8; opacity:0.8;}
.nextImageBtn{right:0; background:#000 url(nextImgBtn.png) center center no-repeat;}
.prevImageBtn{background:#000 url(prevImgBtn.png) center center no-repeat;}
#mydiv {
    position:fixed;
    top: 35%;
    left: 50%;
    width:30em;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    border: 1px solid #ccc;
    background-color: rgba(51, 51, 51, 0.5) !important;
    z-index:3;
    padding: 5px;
}

 html body #mydiv h4, html body #mydiv a {
      color: #fff;
      font-family: sans-serif;
      font-weight: 500;
      font-size: 18px; 
      margin-top: 10px;
      margin-bottom: 0px;}
      
hr {
  height: 0;
  -webkit-box-sizing: content-box;
     -moz-box-sizing: content-box;
          box-sizing: content-box;
}
hr {
  
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #eee;
}

.alert-box {
    color:#d9534f;
    border-radius:10px;
    font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px;
    padding:10px 10px 10px 36px;
    margin-bottom:0px;
}

.alert-box span {
    font-weight:bold;
    text-transform:uppercase;
}
.error {
    background:#ffecec url('images/error.png') no-repeat 10px 50%;
    border:1px solid #f5aca6;
    float: center;
}
.success {
    background:#e9ffd9 url('images/success.png') no-repeat 10px 50%;
    border:1px solid #a6ca8a;
}
.warning {
    background:#fff8c4 url('images/warning.png') no-repeat 10px 50%;
    border:1px solid #f2c779;
}
.notice {
    background:#e3f7fc url('images/notice.png') no-repeat 10px 50%;
    border:1px solid #8ed9f6;
}

/* #### bootstrap Form #### */
.bootstrap-frm {
    margin-left:auto;
    margin-right:auto;

    max-width: 500px;
/*     background: #FFF; */
    padding: 20px 30px 20px 30px;
    font: 12px "Helvetica Neue", Helvetica, Arial, sans-serif;
    color: #888;
    text-shadow: 1px 1px 1px #FFF;
/*     border:1px solid #DDD; */
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
}
.bootstrap-frm h1 {
    font: 25px "Helvetica Neue", Helvetica, Arial, sans-serif;
    padding: 0px 0px 10px 40px;
    display: block;
    border-bottom: 1px solid #DADADA;
    margin: -10px -30px 30px -30px;
    color: #888;
}
.bootstrap-frm h1>span {
    display: block;
    font-size: 11px;
}
.bootstrap-frm label {
    display: block;
    margin: 0px 0px 5px;
}
.bootstrap-frm label>span {
    float: left;
    width: 20%;
    text-align: right;
    padding-right: 10px;
    margin-top: 10px;
    color: #333;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-weight: bold;
}
.bootstrap-frm input[type="text"], .bootstrap-frm input[type="password"], .bootstrap-frm input[type="email"], .bootstrap-frm textarea, .bootstrap-frm select{
/*     border: 1px solid #CCC; */
    color: #888;
    height: 25px;
    line-height:15px;
    margin-bottom: 16px;
    font-size: 14px;
    margin-right: 6px;
    margin-top: 2px;
    outline: 0 none;
    padding: 5px 0px 5px 5px;
    width: 100%;
    border-radius: 4px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;    
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
.bootstrap-frm select {
    background: #FFF url('down-arrow.png') no-repeat right;
    background: #FFF url('down-arrow.png') no-repeat right;
    appearance:none;
    -webkit-appearance:none;
    -moz-appearance: none;
    text-indent: 0.01px;
    text-overflow: '';
    width: 70%;
    height: 35px;
    line-height:15px;
}
.bootstrap-frm textarea{
    height:100px;
    padding: 5px 0px 0px 5px;
    width: 70%;
}
.bootstrap-frm .button {
    background: #FFF;
    border: 1px solid #CCC;
    padding: 10px 25px 10px 25px;
    color: #333;
    border-radius: 4px;
}
.bootstrap-frm .button:hover {
    color: #333;
    background-color: #EBEBEB;
    border-color: #ADADAD;
}

.btn-primary {
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
   -moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
    width: 100%;
}




</style>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/admin/icomoon/style.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/admin/js/jquery.easing.1.3.js"></script>
</head>
<body>
<div id="bg"><a href="#" class="nextImageBtn" title="next"></a><a href="#" class="prevImageBtn" title="previous"></a><img src="<?=base_url()?>assets/admin/login_img/azienda_clubhouse.jpg" width="1680" height="1050" alt="Azienda Clubhouse" title="Azienda Clubhouse" id="bgimg" /></div>
<div id="preloader"><img src="<?=base_url()?>assets/admin/img/ajax-loader_dark.gif" width="32" height="32" /></div>

<div id="toolbar"><a href="<?=base_url()?>"><img src="<?=base_url()?>assets/admin/login_img/Logo02-06.png" width="250" height="250"  /></a></div>
<div id="mydiv">
       <h4 style="float: left;">Support Login</h4>
       <br>
       <br>
       <hr>
       <?php if(validation_errors()){
       	?>
       	<div class="alert-box error"><span><span class="icon-cancel-circle">
       	</span> <?php echo validation_errors();?></span>
       	</div>
       	<?php 
       }?>
      
		<form class="bootstrap-frm" method="post" action="<?=base_url()?>admin/">
		
		<input type="text" name="username" placeholder="Username" required>
		
		<input type="password" name="password" placeholder="Password" required>
		
        <input type="submit" class="btn-primary" value="Login" name="hoa_login">
        
        <a style="float: left; font-size: 15px;" onclick="changepass()" href="javascript:;">Forgot Password</a>
		<br>
		<br>
		<div class="clear" style="margin-bottom: 10px;"></div>
		</form>
		
</div>
<div id="thumbnails_wrapper">
<div id="outer_container">
<div class="thumbScroller">
	<div class="container">
    	<div class="content">
        	<div><a href="<?=base_url()?>assets/admin/login_img/azienda_clubhouse.jpg"><img src="<?=base_url()?>assets/admin/login_img/azienda_clubhouse_thumb.jpg" title="Azienda Clubhouse" alt="Azienda Clubhouse" class="thumb" width="220" height="138"/></a></div>
        </div>
        <div class="content">
        	<div><a href="<?=base_url()?>assets/admin/login_img/camella_community_2.jpg"><img src="<?=base_url()?>assets/admin/login_img/camella_community_2_thumb.jpg" title="Camella Community 2" alt="Camella Community 2" class="thumb" width="220" height="138"/></a></div>
        </div>
    	<div class="content">
        	<div><a href="<?=base_url()?>assets/admin/login_img/batangas_clubhouse.jpg"><img src="<?=base_url()?>assets/admin/login_img/batangas_clubhouse_thumb.jpg" title="Batangas Clubhouse" alt="Batangas Clubhouse" class="thumb" width="220" height="138" /></a></div>
        </div>
        <div class="content">
        	<div><a href="<?=base_url()?>assets/admin/login_img/lifestyle_jogging.jpg"><img src="<?=base_url()?>assets/admin/login_img/lifestyle_jogging_thumb.jpg" title="Lifestyle Jogging" alt="Lifestyle Jogging" class="thumb" width="220" height="138"/></a></div>
        </div>
        <div class="content">
        	<div><a href="<?=base_url()?>assets/admin/login_img/lifestyle.jpg"><img src="<?=base_url()?>assets/admin/login_img/lifestyle_thumb.jpg" title="Lifestyle" alt="Lifestyle" class="thumb" width="220" height="138" /></a></div>
        </div>
         <div class="content">
        	<div><a href="<?=base_url()?>assets/admin/login_img/lifestyle_pool_party.jpg"><img src="<?=base_url()?>assets/admin/login_img/lifestyle_pool_party_thumb.jpg" title="Lifestyle" alt="Lifestyle" class="thumb" width="220" height="138" /></a></div>
        </div>
     </div>
</div>
</div>
</div>
<script>
//config
//set default images view mode
$defaultViewMode="full"; //full, normal, original
$tsMargin=30; //first and last thumbnail margin (for better cursor interaction) 
$scrollEasing=600; //scroll easing amount (0 for no easing) 
$scrollEasingType="easeOutCirc"; //scroll easing type 
$thumbnailsContainerOpacity=0.8; //thumbnails area default opacity
$thumbnailsContainerMouseOutOpacity=0; //thumbnails area opacity on mouse out
$thumbnailsOpacity=0.6; //thumbnails default opacity
$nextPrevBtnsInitState="show"; //next/previous image buttons initial state ("hide" or "show")
$keyboardNavigation="on"; //enable/disable keyboard navigation ("on" or "off")

//cache vars
$thumbnails_wrapper=$("#thumbnails_wrapper");
$outer_container=$("#outer_container");
$thumbScroller=$(".thumbScroller");
$thumbScroller_container=$(".thumbScroller .container");
$thumbScroller_content=$(".thumbScroller .content");
$thumbScroller_thumb=$(".thumbScroller .thumb");
$preloader=$("#preloader");
$toolbar=$("#toolbar");
$toolbar_a=$("#toolbar a");
$bgimg=$("#bgimg");
$img_title=$("#img_title");
$nextImageBtn=$(".nextImageBtn");
$prevImageBtn=$(".prevImageBtn");

$(window).load(function() {
	$toolbar.data("imageViewMode",$defaultViewMode); //default view mode
	if($defaultViewMode=="full"){
		$toolbar_a.html("<img src='<?=base_url()?>assets/admin/login_img/Logo02-06.png' width='250' height='250'  />");
	} else {
		$toolbar_a.html("<img src='<?=base_url()?>assets/admin/login_img/Logo02-06.png' width='250' height='250'  />");
	}
	ShowHideNextPrev($nextPrevBtnsInitState);
	//thumbnail scroller
	$thumbScroller_container.css("marginLeft",$tsMargin+"px"); //add margin
	sliderLeft=$thumbScroller_container.position().left;
	sliderWidth=$outer_container.width();
	$thumbScroller.css("width",sliderWidth);
	var totalContent=0;
	fadeSpeed=200;
	
	var $the_outer_container=document.getElementById("outer_container");
	var $placement=findPos($the_outer_container);
	
	$thumbScroller_content.each(function () {
		var $this=$(this);
		totalContent+=$this.innerWidth();
		$thumbScroller_container.css("width",totalContent);
		$this.children().children().children(".thumb").fadeTo(fadeSpeed, $thumbnailsOpacity);
	});

	$thumbScroller.mousemove(function(e){
		if($thumbScroller_container.width()>sliderWidth){
	  		var mouseCoords=(e.pageX - $placement[1]);
	  		var mousePercentX=mouseCoords/sliderWidth;
	  		var destX=-((((totalContent+($tsMargin*2))-(sliderWidth))-sliderWidth)*(mousePercentX));
	  		var thePosA=mouseCoords-destX;
	  		var thePosB=destX-mouseCoords;
	  		if(mouseCoords>destX){
		  		$thumbScroller_container.stop().animate({left: -thePosA}, $scrollEasing,$scrollEasingType); //with easing
	  		} else if(mouseCoords<destX){
		  		$thumbScroller_container.stop().animate({left: thePosB}, $scrollEasing,$scrollEasingType); //with easing
	  		} else {
				$thumbScroller_container.stop();  
	  		}
		}
	});

	$thumbnails_wrapper.fadeTo(fadeSpeed, $thumbnailsContainerOpacity);
	$thumbnails_wrapper.hover(
		function(){ //mouse over
			var $this=$(this);
			$this.stop().fadeTo("slow", 1);
		},
		function(){ //mouse out
			var $this=$(this);
			$this.stop().fadeTo("slow", $thumbnailsContainerMouseOutOpacity);
		}
	);

	$thumbScroller_thumb.hover(
		function(){ //mouse over
			var $this=$(this);
			$this.stop().fadeTo(fadeSpeed, 1);
		},
		function(){ //mouse out
			var $this=$(this);
			$this.stop().fadeTo(fadeSpeed, $thumbnailsOpacity);
		}
	);

	//on window resize scale image and reset thumbnail scroller
	$(window).resize(function() {
		FullScreenBackground("#bgimg",$bgimg.data("newImageW"),$bgimg.data("newImageH"));
		$thumbScroller_container.stop().animate({left: sliderLeft}, 400,"easeOutCirc"); 
		var newWidth=$outer_container.width();
		$thumbScroller.css("width",newWidth);
		sliderWidth=newWidth;
		$placement=findPos($the_outer_container);
	});

	//load 1st image
	var the1stImg = new Image();
	the1stImg.onload = CreateDelegate(the1stImg, theNewImg_onload);
	the1stImg.src = $bgimg.attr("src");
	$outer_container.data("nextImage",$(".content").first().next().find("a").attr("href"));
	$outer_container.data("prevImage",$(".content").last().find("a").attr("href"));
});

function BackgroundLoad($this,imageWidth,imageHeight,imgSrc){
	$this.fadeOut("fast",function(){
		$this.attr("src", "").attr("src", imgSrc); //change image source
		FullScreenBackground($this,imageWidth,imageHeight); //scale background image
		$preloader.fadeOut("fast",function(){$this.fadeIn("slow");});
		var imageTitle=$img_title.data("imageTitle");
		if(imageTitle){
			$this.attr("alt", imageTitle).attr("title", imageTitle);
			$img_title.fadeOut("fast",function(){
				$img_title.html(imageTitle).fadeIn();
			});
		} else {
			$img_title.fadeOut("fast",function(){
				$img_title.html($this.attr("title")).fadeIn();
			});
		}
	});
}

// mouseover toolbar
// if($toolbar.css("display")!="none"){
// 	$toolbar.fadeTo("fast", 0.4);
// }
$toolbar.hover(
	function(){ //mouse over
		var $this=$(this);
// 		$this.stop().fadeTo("fast", 1);
	},
	function(){ //mouse out
		var $this=$(this);
// 		$this.stop().fadeTo("fast", 0.4);
	}
);

//Clicking on thumbnail changes the background image
$("#outer_container a").click(function(event){
	event.preventDefault();
	var $this=$(this);
	GetNextPrevImages($this);
	GetImageTitle($this);
	SwitchImage(this);
	ShowHideNextPrev("show");
}); 

//next/prev images buttons
$nextImageBtn.click(function(event){
	event.preventDefault();
	SwitchImage($outer_container.data("nextImage"));
	var $this=$("#outer_container a[href='"+$outer_container.data("nextImage")+"']");
	GetNextPrevImages($this);
	GetImageTitle($this);
});

$prevImageBtn.click(function(event){
	event.preventDefault();
	SwitchImage($outer_container.data("prevImage"));
	var $this=$("#outer_container a[href='"+$outer_container.data("prevImage")+"']");
	GetNextPrevImages($this);
	GetImageTitle($this);
});

//next/prev images keyboard arrows
if($keyboardNavigation=="on"){
$(document).keydown(function(ev) {
    if(ev.keyCode == 39) { //right arrow
        SwitchImage($outer_container.data("nextImage"));
		var $this=$("#outer_container a[href='"+$outer_container.data("nextImage")+"']");
		GetNextPrevImages($this);
		GetImageTitle($this);
        return false; // don't execute the default action (scrolling or whatever)
    } else if(ev.keyCode == 37) { //left arrow
        SwitchImage($outer_container.data("prevImage"));
		var $this=$("#outer_container a[href='"+$outer_container.data("prevImage")+"']");
		GetNextPrevImages($this);
		GetImageTitle($this);
        return false; // don't execute the default action (scrolling or whatever)
    }
});
}

function ShowHideNextPrev(state){
	if(state=="hide"){
		$nextImageBtn.fadeOut();
		$prevImageBtn.fadeOut();
	} else {
		$nextImageBtn.fadeIn();
		$prevImageBtn.fadeIn();
	}
}

//get image title
function GetImageTitle(elem){
	var title_attr=elem.children("img").attr("title"); //get image title attribute
	$img_title.data("imageTitle", title_attr); //store image title
}

//get next/prev images
function GetNextPrevImages(curr){
	var nextImage=curr.parents(".content").next().find("a").attr("href");
	if(nextImage==null){ //if last image, next is first
		var nextImage=$(".content").first().find("a").attr("href");
	}
	$outer_container.data("nextImage",nextImage);
	var prevImage=curr.parents(".content").prev().find("a").attr("href");
	if(prevImage==null){ //if first image, previous is last
		var prevImage=$(".content").last().find("a").attr("href");
	}
	$outer_container.data("prevImage",prevImage);
}

//switch image
function SwitchImage(img){
	$preloader.fadeIn("fast"); //show preloader
	var theNewImg = new Image();
	theNewImg.onload = CreateDelegate(theNewImg, theNewImg_onload);
	theNewImg.src = img;
}

//get new image dimensions
function CreateDelegate(contextObject, delegateMethod){
	return function(){
		return delegateMethod.apply(contextObject, arguments);
	}
}

//new image on load
function theNewImg_onload(){
	$bgimg.data("newImageW",this.width).data("newImageH",this.height);
	BackgroundLoad($bgimg,this.width,this.height,this.src);
}

//Image scale function
function FullScreenBackground(theItem,imageWidth,imageHeight){
	var winWidth=$(window).width();
	var winHeight=$(window).height();
	if($toolbar.data("imageViewMode")!="original"){ //scale
		var picHeight = imageHeight / imageWidth;
		var picWidth = imageWidth / imageHeight;
		if($toolbar.data("imageViewMode")=="full"){ //fullscreen size image mode
			if ((winHeight / winWidth) < picHeight) {
				$(theItem).attr("width",winWidth);
				$(theItem).attr("height",picHeight*winWidth);
			} else {
				$(theItem).attr("height",winHeight);
				$(theItem).attr("width",picWidth*winHeight);
			};
		} else { //normal size image mode
			if ((winHeight / winWidth) > picHeight) {
				$(theItem).attr("width",winWidth);
				$(theItem).attr("height",picHeight*winWidth);
			} else {
				$(theItem).attr("height",winHeight);
				$(theItem).attr("width",picWidth*winHeight);
			};
		}
		$(theItem).css("margin-left",(winWidth-$(theItem).width())/2);
		$(theItem).css("margin-top",(winHeight-$(theItem).height())/2);
	} else { //no scale
		$(theItem).attr("width",imageWidth);
		$(theItem).attr("height",imageHeight);
		$(theItem).css("margin-left",(winWidth-imageWidth)/2);
		$(theItem).css("margin-top",(winHeight-imageHeight)/2);
	}
}

//Image view mode function - fullscreen or normal size
function ImageViewMode(theMode){
	$toolbar.data("imageViewMode", theMode);
	FullScreenBackground($bgimg,$bgimg.data("newImageW"),$bgimg.data("newImageH"));
	if(theMode=="full"){
		$toolbar_a.html("<img src='toolbar_n_icon.png' width='50' height='50'  />").attr("onClick", "ImageViewMode('normal');return false").attr("title", "Restore");
	} else {
		$toolbar_a.html("<img src='toolbar_fs_icon.png' width='50' height='50'  />").attr("onClick", "ImageViewMode('full');return false").attr("title", "Maximize");
	}
}

//function to find element Position
	function findPos(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			curleft = obj.offsetLeft
			curtop = obj.offsetTop
			while (obj = obj.offsetParent) {
				curleft += obj.offsetLeft
				curtop += obj.offsetTop
			}
		}
		return [curtop, curleft];
	}

	 $(function() {


         $('#login').show();
         $('#changepass').hide();
         $('#changeuname').hide();
                          
     });
	
</script>
</body>
</html>