<script type="text/javascript">
	$( document ).ready(function() {    
        $('#users').hide();
        $('#loading').css('display', 'block');         
        $.ajax({
            type:"get",
            url: "<?=base_url()?>support/users_list/",
            success: function(data){
                $('#users').html(data);
                $('#users').slideDown('fast');
                $('#loading').css('display', 'none');
            }
        });
   	});
</script>
<?php
$acc = explode("-", $access);
?>
		<div class="row">	
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading" style="padding: 5px 15px;">
						<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-user"></span> Users</div> 
							<div class="btn-group col-md-8 pull-right">
								<div class="col-md-1" style="float:right;padding-left:5px;padding-right:0px" onclick="slideSearch()">
									<button class="btn btn-xs input-sm btn-success form-control"><i class="glyphicon glyphicon-search"></i></button>
								</div>
								<div class="col-md-5" id="searchUser" style="float:right;display:none;padding-left:5px;padding-right:0px">
									<input type="texbox" id="userSearch" autocomplete="off" onblur="userSearchTxtBoxEvent()" onkeypress="uhandler(event)" class="input-sm form-control" placeholder="Search">
								</div>
							<?php if($acc[12]!=0){ ?>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/add_users'"><i class="icon-user-add"></i>&nbsp Add User</button>
								</div>
							<?php } ?>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/roles'"><i class="icon-key"></i>&nbsp Roles</button>
								</div>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/projects'"><i class="icon-clippy"></i>&nbsp Projects</button>
								</div>
							</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div id="users">
					<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
				</div>
			</div>
		</div>
	</div>
	