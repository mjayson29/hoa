 <script type="text/javascript">

    function paginate(val){
        $('#businessGroup').hide();
        $('#loadingBG').show();
        $.ajax({
        type:"get",
        url: "<?=base_url()?>support/businessGroup/"+val,
            success: function(data){
                $('#businessGroup').html(data);
                $('#businessGroup').slideDown('fast');
                $('#loadingBG').hide();
            }
        });
    }
</script>
<?php
$acc = explode("-", $access);
?>
<?php if(!$new){ ?>
<div class="panel panel-primary" style="border-color:#d43f3a">
	<div class="panel-heading" style="background-color:#d9534f;border-color:transparent">
		<span class="panel-title pull-left">No Results Found</span> 
		<div class="clearfix"></div>
	</div>
</div>
<?php } else { ?>
<div class="panel panel-primary">
	<div class="table-responsive" style="border-radius:inherit">
		<table class="table" style="border-radius:inherit">
			<tr style="background-color:#337AB7;color:#fff">
				<th style="text-align:center;border-color:#337AB7">Business Group</th>
				<th style="text-align:center;border-color:#337AB7">Updated By</th>
				<th style="text-align:center;border-color:#337AB7">Date Updated</th>
				<th style="text-align:center;border-color:#337AB7">Action</th>
			</tr>
	<?php foreach($new as $row){ ?>
	<?php $update = strtotime($row['rb_update']); ?>
			<tr class="data_click" onclick="">
				<td style="width: 15%;"><?=ucwords(strtolower($row['rb_name']))?></td>
				<td style="width: 15%;text-align:center"><?=strtolower($row['a_uname'])?></td>
				<td style="width: 15%;text-align:center"><?=date('F d, Y', $update)?></td> 
				<td style="width: 25%;text-align:center">
				<?php if($acc[14]!=0){ ?>
					<span>
						<a onclick="editBusinessGroup(<?=$row['rb_grp_id']?>)" class="icon-pencil" title="Edit"></a> 
					</span>
				<?php } ?>
					&nbsp;&nbsp;&nbsp;
				<?php if($acc[14]!=0){ ?>
					<span>
						<a onclick="deleteBusinessGroup(<?=$row['rb_grp_id']?>)" class="icon-bin" style="color:reds" title="Delete"> </a>
					</span>
				<?php } ?>
				</td> 
			</tr>
	<?php } ?>
		</table>
	</div>
	<?php } ?>
</div>
<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>
