 <script type="text/javascript">

    function paginate(val){
        $('#ticket_loader').hide();
        $('#loading').show();

       	$.ajax({
            type:"post",
            url: "<?=base_url()?>support/get_new_tickets/"+val,
            success: function(data){
                $('#ticket_loader').html(data);
                $('#ticket_loader').slideDown('slow');
                $('#loading').css('display', 'none');
            }
        });

    }

</script>

<script type="text/javascript">
	$(document).ready(function() {
		var showChar = 20;
		var ellipsestext = "...";
		$('.messagecont').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span>';
				$(this).html(html);
			}
		});
	});
</script>

<div class="panel panel-primary">
	<div class="panel-heading">New</div>
	<?php if(!$new_tickets){ ?>

		<div class="panel-body">

			<span>NO DATA</span>

		</div>

	<?php } else { ?>

		<div class="table-responsive">

			<table class="table">

				<tr>
					<th>Ticket #</th>
					<th>Date Posted</th>
					<th>Project Area</th>
					<th>Project</th>
					<th>Blk/Lot</th>
					<th>Unit</th>
					<th>Request</th>
					<th>Status</th>
					<th>Last Reply</th>
					<th>Last Reply Date</th>
				</tr>
	
				<?php foreach($new_tickets as $row){ 
					$date_posted = strtotime($row['t_date_created']);		
				    $select="";
					$table = "ZHOA_concerns";
					$where = "c_ticket_id = ".$row['t_id'];
					$stat = $this->Main->select_data_where($select, $table, $where);
					$posted = 0; 
					$con = 0;
					foreach ($stat as $cstat) {
						if($cstat['qmnum'] != ""){
							$posted++;
						} $con++;
					}
					if($posted == $con){
						$status = "POSTED";
					}
					if($posted < $con){
						$status = "PARTIAL";
					}
					if($posted == 0){
						$status = "UNPOSTED";
					}
				    switch ($row['t_status']) {
						case 1:
							if($row['t_date_created'] >= date('Y-m-d').' 00:00:00.000' && $row['t_date_created'] <= date('Y-m-d').' 23:59:59.997'){
								$status1 = "NEW";
							} else {
								$status1 = "OPEN";
							}
							break;
					
						default:
							$status1 = "CLOSED";
							break;
					}

					$date_created = strtotime($row['t_date_created']);
					$today = date('Y-m-d H:i:s');
					$start_date = new DateTime(date('Y-m-d H:i:s', $date_created));
					$since_start = $start_date->diff(new DateTime($today));
					
					$age = "";
					if($row['t_status'] == 1 AND $since_start->d >= 3){
						$age = "age_red";
					} 
					if($row['t_status'] == 2){
						$age = "age_green";
					}

					$repdate = strtotime($row['t_reply_date']);
					strlen($row['t_concern']) <= 25 ? $concern=substr($row['t_concern'],0,25) : $concern=$row['t_concern'];

					$blk = "";
					$parea = "";
					$proj = "";
					$unit = "";
					
					if(isset($row['PROJ_AREA'])){
						$parea = $row['PROJ_AREA'];
					}
					if(isset($row['XWETEXT'])){
						$proj = $row['XWETEXT'];
					}
					if(isset($row['XMBEZ'])){
						$unit = $row['XMBEZ'];
					}
					if($row['t_blklot'] != 0){
						$blk = $row['t_blklot'];
					}


				?>
					<tr class="data_click <?=$age?>" onclick="window.location.href='<?=base_url()?>support/single_ticket/<?=str_pad($row['t_id'], 10, '0', STR_PAD_LEFT)?>'">
						<td style="width: 10%;"><?=str_pad($row['t_id'], 10, "0", STR_PAD_LEFT);?></td>
						<td style="width: 10%;"><?=date('M d, Y', $date_posted)?></td>
						<td style="width: 10%;"><?=$parea?></td>
						<td style="width: 10%;"><?=$proj?></td>
						<td style="width: 10%;"><?=$blk?></td>
						<td style="width: 10%;"><?=$unit?></td>
						<td class="messagecont" style="width: 10%;"><?=$concern?></td>
						<td style="width: 10%;"><?=$status1?><br>[<?=$status?>]</td>
						<td style="width: 10%;"><?=$row['t_reply_by']?></td>
						<td style="width: 10%;"><?=date('M d, Y', $repdate)?></td>
					</tr>			

				<?php } ?>		

			</table>

		</div>

	<?php } ?>
</div>

<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>
