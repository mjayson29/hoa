<?php

if($brands) {
	$co = array();
	foreach ($brands as $value) {
		$co[$value['BUKRS']] = $value['BUTXT'];
	}
}
$acc = explode("-", $access);
?>

<script type="text/javascript">
	
	function checkEmail(email){

		var get_email = $('#email').val();

		if(email == ""){
			$('#mailstatus').slideUp();
			return false;
		}

		$('#mailstatus').slideDown();
	    $.ajax({
	        type:"POST",
	        url: "<?=base_url()?>support/checkEmail/",
	        data: {
	            email: email, 
	        },
	        success: function(data){
	            $('#mailstatus').html(data);
	            $('#nu_email').css('width','100%');
	        }
	    });
	}

</script>

		<div class="panel panel-primary">
					<div class="panel-heading" style="padding: 5px 15px;">
						<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-user"></span> Users</div> 
							<div class="btn-group col-md-8 pull-right">
								<!-- <div class="col-md-1" style="float:right;padding-left:5px;padding-right:0px" onclick="slideSearch()">
									<button class="btn btn-xs input-sm btn-success form-control"><i class="glyphicon glyphicon-search"></i></button>
								</div>
								<div class="col-md-5" id="searchUser" style="float:right;display:none;padding-left:5px;padding-right:0px">
									<input type="texbox" id="userSearch" autocomplete="off" onblur="userSearchTxtBoxEvent()" onkeypress="uhandler(event)" class="input-sm form-control" placeholder="Search">
								</div> -->
							<?php if($acc[12]!=0){ ?>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/add_users'"><i class="icon-user-add"></i>&nbsp Add User</button>
								</div>
							<?php } ?>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/roles'"><i class="icon-key"></i>&nbsp Roles</button>
								</div>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/projects'"><i class="icon-clippy"></i>&nbsp Projects</button>
								</div>
							</div>
						<div class="clearfix"></div>
					</div>
				</div>

		<div class="panel panel-primary" >
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="icon-user-add"> </span>Add User</div>
				<div class="clearfix"></div>
			</div>
			<div  id="addusr"></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3" style="margin-bottom:15px">
							<input style="width:200%" id="nu_uname" type="text" maxlength="12" autocomplete="off" required onblur="checkusrname(this.value)" class="form-control" placeholder="Username">
						</div>
						<div class="col-md-3" style="margin-bottom:15px">
							<div id="usrnamestatus"></div>
						</div>
						<div class="col-md-6" style="margin-bottom:15px">
							<div class="col-md-6" style="padding-left:0px; padding-right:30px">
								<button class="btn btn-success btn-sm form-control" onclick="saveuser()">Save</button>
							</div>
							<div class="col-md-6" style="padding-left:0px; padding-right:30px">
								<button class="btn btn-danger btn-sm form-control" onclick="window.location.href='http://127.0.0.1:8080/vistahome/support/users'">Cancel</button>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3" style="margin-bottom:15px">
							<input id="nu_lname" style="width:200%" type="text" class="form-control" autocomplete="off" required placeholder="Last Name">
						</div>
						<div class="col-md-3 col-md-offset-3" style="margin-bottom:15px">
							<input id="nu_fname" style="width:200%" type="text" class="form-control" autocomplete="off" required placeholder="First Name">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3" style="margin-bottom:15px">
							<input id="nu_email" style="width:200%" type="email" class="form-control" autocomplete="off" required placeholder="Email Address"> <!-- onblur="checkEmail(this.value)"> -->
						</div>
						<div class="col-md-3" style="margin-bottom:15px">
							<div id="mailstatus"></div>
						</div>
						<div class="col-md-3" style="margin-bottom:15px">
							<input id="nu_spark" style="width:200%" type="text" class="form-control" autocomplete="off" required placeholder="Spark ID">
						</div>
						<div class="col-md-3" style="margin-bottom:15px">
							<div id="sparkstatus"></div>
						</div>
					</div>
					
					<div class="col-md-12" style="margin-top:25px">
						<div class="col-md-3">
							<select class="form-control input-sm" required autocomplete="off" onchange="list_roles(this.value)" id="selectedrole">
								<option value="0" autocomplete="off">-- Select Roles --</option>
								<?php foreach($role as $row){ ?>
								<?php $key = strtoupper($row['ua_rname']); ?>
								<option value="<?=$row['ua_id']?>"> <?=$key?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div id="display2" class="row">
					</div>
					<div id="display" class="row">
						<div class="col-md-12" style="padding-top:20px">
							<div class="col-md-12">
								<div class="col-md-3">
									<label class="custom_label">Navigation</label><br/>
								</div>
							</div>
							<div class="col-md-12"> <hr> </div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Registration List</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="regview" value="1" disabled> 
									<label for="regview">View</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="regconf" value="1" disabled > 
									<label for="regconf">Confirm</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Support</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="suppview" value="1" disabled > 
									<label for="suppview">View</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="suppreply" value="1" disabled> 
									<label for="suppreply">Reply</label>
								</div>

								<div class="col-md-2">
									<input type="checkbox" id="suppcomm" value="" disabled name="check" /> 
									<label for="suppcomm">Comment</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Reports</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="rptview" value="1" disabled> 
									<label for="rptview">View</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="rptexpo" value="1" disabled > 
									<label for="rptexpo">Export</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Maintenance</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="mtcview" value="1" disabled> 
									<label for="mtcview">View</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label"> &nbsp;&nbsp;&nbsp; Carousel</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="carview" value="1" disabled> 
									<label for="carview">View</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label"> &nbsp;&nbsp;&nbsp; Announcements</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="annview" value="1" disabled> 
									<label for="annview">View</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label"> &nbsp;&nbsp;&nbsp; SOA</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="soaview" value="1" disabled> 
									<label for="soaview">View</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Homeowners List</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="hoview" value="1" disabled > 
									<label for="hoview">View</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Account Settings</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="settings" value="1" disabled> 
									<label for="settings">View</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label">Users</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="ursview" onch value="1" disabled>
									<label for="ursview">View</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="ursedit" value="1" disabled> 
									<label for="ursedit">Edit</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="ursdel" value="1" disabled> 
									<label for="ursdel">Delete</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label"> &nbsp;&nbsp;&nbsp; Add Users</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="ursadd" value="1" disabled> 
									<label for="ursadd">Add</label>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-1">
									<label class="custom_label"> &nbsp;&nbsp;&nbsp; Roles</label>
								</div>
								<div class="col-md-2 col-md-offset-1">
									<input type="checkbox" id="roleadd" value="1" disabled> 
									<label for="roleadd">Add</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="roleedit" value="1" disabled > 
									<label for="roleedit">Edit</label>
								</div>
								<div class="col-md-2">
									<input type="checkbox" id="roledel" value="1" disabled > 
									<label for="roledel">Delete</label>
								</div>
							</div>
							<div class="col-md-12"> <hr> </div>
						</div>
					<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Receive E-mail Alert</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-12">
							<div class="col-md-2 col-md-offset-2">
								<input type="checkbox" id="concern" value="1" disabled > 
								<label for="concern">Concern</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="req" value="1" disabled > 
								<label for="req">Request</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="inq" value="1" disabled> 
								<label for="inq">Inquiry</label>
							</div>
							<div class="col-md-12"> <hr> </div>
						</div>
					</div>
				</div>
				<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Business Entity Assignment</label><br/>
							</div>
						</div>
						<div class="col-md-12" style="margin-top:25px">
						<div class="col-md-3">
							<select class="form-control input-sm" required autocomplete="off" onchange="list_BG(this.value)" id="selectedBusinessGroup">
							<option value="0" autocomplete="off">-- Select Business Group --</option>
							<?php foreach($company as $row){ ?>
							<?php $key = strtoupper($row['rb_name']); ?>
							<option value="<?=$row['rb_grp_id']?>"> <?=$key?></option>
							<?php } ?>
							</select>
						</div>
					</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-3 col-md-offset-2 container-fluid">
							<span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Assigned Business Entity</label>
                            </span>
							<div class="selector-box">
								<div class="row-fluid" style="height:100%" id="loadselected">
									<select multiple id='resultbussEnt' autocomplete="off" class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
										<p style="text-align: center; display: none;" id="load_sourcebussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="selection-box">
								<div class="col-md-6 col-md-offset-3" style="top:25%">
									<button class="btn btn-sm btn-default form-control"  onclick="removed()" style="padding:5px; margin-top:3px">></button>
									<button class="btn btn-sm btn-default form-control"  onclick="add()" style="padding:5px; margin-top:3px"><</button>
									<button class="btn btn-sm btn-default form-control"  onclick="removeall()" style="padding:5px; margin-top:3px">>></button>
									<button class="btn btn-sm btn-default form-control"  onclick="addall()" style="padding:5px; margin-top:3px"><<</button>
								</div>
							</div>
						</div>
						<div class="col-md-3 container-fluid">
							<span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Unassigned Business Entity</label>
                            </span>
							<div class="selector-box">
								<select class="form-control input-sm" autocomplete="off" disabled onchange="listproj2()" id="selectedcompany">
									<option value="0" autocomplete="off">-- Select Company --</option>
									<?php if($co) { ?>
										<?php foreach($co as $ro => $we){ ?>
										<option value="<?=$ro?>"><?=$we?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<div class="row-fluid" style="height:90%; padding-top:10px" id="loadedproj">
									<select multiple id='sourcebussEnt' autocomplete="off" class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
										<p style="text-align: center; display: none;" id="load_sourcebussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
					</div>
				</div>
			</div>
		</div>
	</div>

