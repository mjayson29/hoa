<script type="text/javascript">

    function paginate(val){

        $('#ticket_loader').hide();
        $('#loading').css('display', 'block');
        var posted_from = $('#posted_from').val();
        var posted_to = $('#posted_to').val();
        var status = $('#status').val();
        var company = $('#company').val();
        var project = $('#project').val(); 
        var keyword = $('#keyword').val(); 

        $.ajax({
	        type:"POST",
	        url: "<?=base_url()?>support/search_concern/"+val,
	        data: {
	                posted_from: posted_from, 
	                posted_to: posted_to,
	                status: status,
	                company: company,
	                project: project,
	                keyword: keyword
	        },
	        success: function(data){
	            $('#ticket_loader').html(data);
	            $('#ticket_loader').slideDown('fast');
	            $('#loading').hide();
	        }
        });

    }

</script>

<script type="text/javascript">
	$(document).ready(function() {
		var showChar = 20;
		var ellipsestext = "...";
		$('.messagecont').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span>';
				$(this).html(html);
			}
		});
	});
</script>

<div class="panel panel-primary">
	<div class="panel-heading">Search Result</div>
	<?php if(!$search){ ?>

		<div class="panel-body">

			<span>NO DATA</span>

		</div>

	<?php } else { ?>

		<div class="table-responsive">

			<table class="table">

				<tr>
					<th>Ticket #</th>
					<th>Date Posted</th>
					<th>Project Area</th>
					<th>Project</th>	
					<th>Blk/Lot</th>
					<th>Unit</th>
					<th>Request</th>
					<th>Status</th>
					<th>Last Reply</th>
					<th>Last Reply Date</th>
				</tr>
	
				<?php 

				foreach($search as $row){ $date_posted = strtotime($row['t_date_created']);
					$date_posted = strtotime($row['t_date_created']);		
				    $select="";
					$table = "ZHOA_concerns";
					$where = "c_ticket_id = ".$row['t_id'];
					$stat = $this->Main->select_data_where($select, $table, $where);
					$posted = 0; 
					$con = 0;
					foreach ($stat as $cstat) {
						if($cstat['qmnum'] != ""){
							$posted++;
						} $con++;
					}
					if($posted == $con){
						$status = "POSTED";
					}
					if($posted < $con){
						$status = "PARTIAL";
					}
					if($posted == 0){
						$status = "UNPOSTED";
					}
				switch ($row['t_status']) {
					case 1:
						if($row['t_date_created'] >= date('Y-m-d').' 00:00:00.000' && $row['t_date_created'] <= date('Y-m-d').' 23:59:59.997'){
							$status1 = "NEW";
						} else {
							$status1 = "OPEN";
						}
						break;
					
					default:
						$status1 = "CLOSED";
						break;
				}

				$date_created = strtotime($row['t_date_created']);
				$today = date('Y-m-d H:i:s');
				$start_date = new DateTime(date('Y-m-d H:i:s', $date_created));
				$since_start = $start_date->diff(new DateTime($today));
				
				$blk = "";
				$age = "";
				if($row['t_status'] == 1 AND $since_start->d >= 3){
					$age = "age_red";
				} 
				if($row['t_status'] == 2){
					$age = "age_green";
				}
				if(isset($row['t_blklot'])){
					$blk = $row['t_blklot'];
				}
				
				$repdate = strtotime($row['t_reply_date']);
				strlen($row['t_concern']) <= 25 ? $concern=substr($row['t_concern'],0,25)."..." : $concern=$row['t_concern'];
				?>
					<tr class="data_click <?=$age?>" onclick="window.location.href='<?=base_url()?>support/single_ticket/<?=str_pad($row['t_id'], 10, "0", STR_PAD_LEFT);?>'">
						<td style="width: 10%;"><?=str_pad($row['t_id'], 10, "0", STR_PAD_LEFT);?></td>
						<td style="width: 10%;"><?=date('M d, Y', $date_posted)?></td>
						<td style="width: 10%;"><?=$row['PROJ_AREA']?></td>
						<td style="width: 10%;"><?=$row['XWETEXT']?></td>
						<td style="width: 10%;"><?=$blk?></td>
						<td style="width: 10%;"><?=$row['XMBEZ']?></td>
						<td class="messagecont" style="width: 10%;"><?=$row['t_concern']?></td>
						<td style="width: 10%;"><?=$status1?><br>[<?=$status?>]</td>
						<td style="width: 10%;"><?=$row['t_reply_by']?></td>
						<td style="width: 10%;"><?=date('M d, Y', $repdate)?></td>
					</tr>			

				<?php } ?>		

			</table>

		</div>

	<?php } ?>
</div>

<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$search_links?></div>
