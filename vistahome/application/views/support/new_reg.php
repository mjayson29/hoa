<script type="text/javascript">
	
	$(function(){
		$('#reject').hide();
		$('#haus_loader').hide();
	});

	function reject(){

		$('#reject').slideToggle();

	}

	function conf(val){

		var email = $('#email').val();
		var uname = $('#username').val();
		var property = $('#property').val();
		var fullname = $('#fullname').val();
		var b_id = $('#b_id').val();
		var conc = $('#conc').val();

		var request = $('#request').val();
		var sub_request = $('#sub_req_type').val();
		var sub_req2 = $('#haus').val();

		if(conc==1){
            if(request == "" || sub_request == ""){
                bootbox.alert({
                    size: "small",
                    message: "Please fill up the field required"
                });
                return false;
            }
            if((sub_request == 2 || sub_request == 3 || sub_request == 4 || sub_request == 5) && sub_req2 == "") {
                bootbox.alert({
                    size: "small",
                    message: "Please select valid options"
                });
                return false;
            }
            if( $('#haus_loader').css('display')=='none'){
                sub_req2 = "";
            }
		}

		bootbox.confirm({
            size: "small",
            message: "Do you really want to confirm this?",
            callback: function(result){
                if(result){
                	$('#page_loader').show();
					$.ajax({
			            type:"POST",
			            url: "<?=base_url()?>support/confirmation/"+val,
			            data: {
			                email: email,
			                uname: uname,
			                property: property,
			                fullname: fullname,
			                b_id: b_id,
			                request: request,
			                sub_request: sub_request,
			                sub_req2: sub_req2
			            },
			            success: function(data){
							$('#page_loader').hide();
			                bootbox.alert({
			                    size: 'small',
			                    message: "Confirmed!"
			                });
			                window.location.href='<?=base_url()?>support/validated_reg/'+val;
			            }
			        });
                }
            }
        });
	}

	function rej(val){

		var email = $('#email').val();
		var reason = window.parent.tinymce.get('thisarea').getContent();
		var fullname = $('#fullname').val();

		if(reason == ""){ 
			alert("Please input reason for rejecting.");
		} else {

			var conf = confirm("Do you really want to reject this?");

			if(conf == true){
				
				$('#page_loader').show();

				$.ajax({
	                type:"POST",
	                url: "<?=base_url()?>support/rejected/"+val,
	                data: {
	                    email: email,
	                	get_reason: reason,
	                	fullname: fullname
	                },
	                success: function(data){
						$('#page_loader').hide();
	                    // alert('Rejected!');
	                    window.location.href='<?=base_url()?>support/validated_reg/'+val;
	                }
	            });
			} 
		}
	}

	function cancel(){
		$('#reject').slideToggle();
	}

</script>

<div id="page_loader" class="page_loader">
    <img src="<?=base_url()?>assets/img/tools/loader.gif"> 
</div> 

<?php 
	function clean($string) {
	   $string = str_replace('.', '', $string); // Replaces all dots with ''.
	   return preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
	}
?>

<?php 
	if(!$buyer){ ?>
	<div class="panel panel-primary">
		<div class="panel-heading">Homeowner Registration Form</div>
		<div class="panel-body">
			<span>NO DATA</span>
		</div>
	</div>

<?php 
	} else { 

		foreach($buyer as $row){ 

			$signup = strtotime($row['bi_regdate']); 
			$bdate = strtotime($row['bi_bdate']);
			$unit = $row['u_desc'];
			$concern = "No Concern";

			if($row['bi_temp_concern'] != ""){
				$concern = $row['bi_temp_concern'];
			}

			$get_id = $row['bi_id'];
			$get_owner_id = $row['bi_owner_id'];
			$cust_no = $row['bi_cust_no'];
			$signup_date = date('M d, Y', $signup);
			$val_project_area = $row['PROJ_AREA'];
			$val_project = $row['bi_project'];
			$val_blklot = $row['bi_blklot'];

			$val_custname = ucwords($row['bi_fname']." ".$row['bi_mname']." ".$row['bi_lname']);
			$fname = $row['bi_fname'];
			$mname = $row['bi_mname'];
			$lname = $row['bi_lname'];

			$fullname = ucwords($fname." ".$lname);
			$property = $val_blklot." ".$val_project;
			$b_id = $row['b_id'];
			$uname = $row['b_uname'];

			$email_verif = $row['bi_email_verif'];
			$reg_date = strtotime($row['bi_regdate']);
			$get_reg_date = date('Y-m-d', $reg_date);

			switch ($row['bi_status']){ 
				case 0: 
					if($get_reg_date == date("Y-m-d") && $email_verif == 1){ ?>
					<div class="panel panel-green">
						<div class="panel-heading">New</div>
					</div>
				<?php } 
					if($get_reg_date < date("Y-m-d") && $email_verif == 1){ ?>
					<div class="panel panel-yellow">
						<div class="panel-heading">Outstanding</div>
					</div>
				<?php }
					if($email_verif == 0){ ?>
					<div class="panel panel-brown">
						<div class="panel-heading">Waiting</div>
					</div>
				<?php }
			}
			$acc = explode("-", $access);
?>

<?php 
	$sap_matching = $this -> Buyer_Model -> get_sap_info($cust_no, $val_blklot); 
	if(!$sap_matching){
		$project_area = "--";
		$project = "--";
		$blklot = "--";
		$customer_num = "--";
		$customer_name = "--";
		$note = "No matching SAP Property Information. Not valid for registration.";
		$validation = 0;
		$notif = "remove\" style=\"color:red";
		$notif1 = "remove\" style=\"color:red";
		$notif2 = "remove\" style=\"color:red";
		$notif3 = "remove\" style=\"color:red";
		$notif4 = "remove\" style=\"color:red";
		$notif5 = "remove\" style=\"color:red";
		$notif6 = "remove\" style=\"color:red";
		$notif7 = "remove\" style=\"color:red";
		$so = "";
		$house_model ="";
		$flr_area ="";
		$lot_area ="";
		$project_area ="";
		$project ="";
		$blklot ="";
		$customer_num ="";
		$customer_name ="";
		$customer_name = "--";
	} else { 
		$so = "";
		foreach($sap_matching as $sap){ 
			$so .= ' '.$sap['VBELN'];
			$house_model = $sap['ARKTX'];
			$flr_area = $sap['FLR_AREA'];
			$lot_area = $sap['LOT_SIZE'];
			$project_area = $sap['PROJ_AREA'];
			$project = $sap['XWETEXT'];
			$blklot = $sap['REFNO'];
			$customer_num = $sap['KUNNR'];
			$customer_name = ucwords($sap['NAME1']);	
			$customer_name = trim(clean($customer_name)," ");
			$fullname = trim($val_custname," ");

			if($project_area != $val_project_area || $project != $val_project || $blklot != $val_blklot || $customer_num != $cust_no){// || $customer_name != $fullname){
				// $note = "Property has no prior registration.";
				$note = "Some SAP Property Information are not matched. Not valid for registration.";
				$notif5 = "remove\" style=\"color:red";
				$validation = 0;

			} else {
				$select = "";
				$where = "bi_cust_no = ".$row['bi_cust_no'];
				$table = "ZHOA_byrinfo";
				$validity = $this->Main->select_data_where($select, $table, $where);
				$count = 0;
				foreach ($validity as $val) {
					if($val['bi_status'] == 1){
						$count++;
					}
				}
				if($count != 0){
					$note = "This registration has a valid confirmed registration";
					$notif5 = "remove\" style=\"color:red";
					$validation = 0;
				} else{
					if($row['b_is_validated'] == 1 && $row['b_is_active'] == 1){
						$note = "Buyer is already registered and validated. ";
						$notif5 = "remove\" style=\"color:red";
						$validation = 0;
					} else {
						$note = "Valid for registration.";					
						$notif5 = "ok\" style=\"color:green";
						$validation = 1;
					}
				}
			}

			$customer_num == $cust_no ? $notif = "ok\" style=\"color:green" : $notif = "remove\" style=\"color:red";
			$project_area == $val_project_area ? $notif1 = "ok\" style=\"color:green" : $notif1 = "remove\" style=\"color:red";
			$project == $val_project ? $notif2 = "ok\" style=\"color:green" : $notif2 = "remove\" style=\"color:red";
			$blklot == $val_blklot ? $notif3 = "ok\" style=\"color:green" : $notif3 = "remove\" style=\"color:red";
		} 	
	} 
?>	

	<input type="hidden" id="email" value="<?=$row['bi_email']?>">
	<input type="hidden" id="username" value="<?=$uname?>">
	<input type="hidden" id="property" value="<?=$property?>">
	<input type="hidden" id="fullname" value="<?=$fullname?>"> 
	<input type="hidden" id="b_id" value="<?=$b_id?>">

		<div class="panel panel-primary">
			<div class="panel-heading">SAP Information related to property</div>

			<div class="panel-body">	
				<div class="row">
					<div class="col-md-6">
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Homeowner Name</label>
							</div>
							<div class="col-md-8 nop4d">
								<span><?=$customer_name?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Customer Number</label>
							</div>
							<div class="col-md-8 nop4d">
								<span class="glyphicon glyphicon-<?=$notif?>"></span> <span><?=$customer_num?></span> 
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Project Area</label>
							</div>
							<div class="col-md-8 nop4d">
								<span class="glyphicon glyphicon-<?=$notif1?>"></span> <span><?=$project_area?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Project Name</label>
							</div>
							<div class="col-md-8 nop4d">
								<span class="glyphicon glyphicon-<?=$notif2?>"></span> <span><?=$project?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Blk / lot</label>
							</div>
							<div class="col-md-8 nop4d">
								<span class="glyphicon glyphicon-<?=$notif3?>"></span> <span><?=$blklot?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Note</label>
							</div>
							<div class="col-md-8 nop4d">
								<span class="glyphicon glyphicon-<?=$notif5?>"></span> <span><?=$note?></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">SO Number</label>
							</div>
							<div class="col-md-8 nop4d">
								<span><?=$so?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">House Model</label>
							</div>
							<div class="col-md-8 nop4d">
								<span><?=$house_model?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Floor Area</label>
							</div>
							<div class="col-md-8 nop4d">
								<span><?=$flr_area?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-4 nop4d">
								<label class="custom_label">Lot Area</label>
							</div>
							<div class="col-md-8 nop4d">
								<span><?=$lot_area?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">Homeowner Registration Form</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<label class="custom_label">Customer Number</label>
						<span><?=$cust_no?></span>
						<br>
						<label class="custom_label">Sign Up Date</label>
						<span><?=$signup_date?></span>
						<br>
						<label class="custom_label">Project Location</label>
						<span><?=$val_project_area?></span>
						<br>
						<label class="custom_label">Unit type</label>
						<span><?=$unit?></span>
						<br>
						<label class="custom_label">Project Name</label>
						<span><?=$val_project?></span>
						<br>
						<label class="custom_label">Blk/Lot</label>
						<span><?=$val_blklot?></span>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">Personal Information</div>
			<div class="panel-body">
				<label class="custom_label">Customer Name</label>
				<span><?=ucwords($row['bi_fname']." ".$row['bi_mname']." ".$row['bi_lname'])?></span>
				<br>
				<label class="custom_label">Birthdate</label>
				<span><?=date('M d, Y', $bdate)?></span>
				<br>
				<label class="custom_label">Address 1</label>
				<span><?=$row['bi_add1']?></span>
				<br>
				<label class="custom_label">Address 2</label>
				<span><?=$row['bi_add2']?></span>
				<br>
				<label class="custom_label">Zip Code</label>
				<span><?=$row['bi_zip']?></span>
				<br>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">Contact Information</div>
			<div class="panel-body">
				<label class="custom_label">Contact Number</label>
				<span><?=$row['bi_contact']?></span>
				<br>
				<label class="custom_label">Email</label>
				<span><?=$row['bi_email']?></span>
				<br>
			</div>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">Security Questions</div>
			<div class="panel-body">

<?php 
	$select = ""; 
	$table = "ZHOA_secquestion"; 
	$where = "sq_id = '".$row['bi_sec1']."'"; 
	$sec1 = $this -> Main -> select_data_where($select, $table, $where);
?>
				<label class="custom_label"><strong>Question 1</strong></label>
<?php 
	foreach($sec1 as $row1){ ?>
					<span><?=$row1['sq_desc']?></span>
<?php } ?>
				<br>
				<label class="custom_label"><strong>Answer 1</strong></label>
				<span><?=$row['bi_ans1']?></span>
				<br>
<?php 
	$where = "sq_id = '".$row['bi_sec2']."'"; 
	$sec2 = $this -> Main -> select_data_where($select, $table, $where);
?>
				<label class="custom_label"><strong>Question 2</strong></label>
<?php 
	foreach($sec2 as $row2){ ?>
					<span><?=$row2['sq_desc']?></span>
<?php } ?>
				<br>
				<label class="custom_label"><strong>Answer 2</strong></label>
				<span><?=$row['bi_ans2']?></span>
				<br>
			</div>
		</div>

<?php 
	if($concern == "No Concern"){ 
		$disabled = "disabled"; 
		$conc = 0; 
	} else { 
		$disabled = ""; 
		$conc = 1;
	}
?>
		<input type="hidden" id="conc" value="<?=$conc?>">
		<div class="panel panel-primary">
			<div class="panel-heading">Concern</div>

			<div class="panel-body">
			<?php if($acc[1]!=0 || $concern != "No Concern") { ?>
				<div style="padding-bottom:10px" class="row">
					<div class="col-sm-3">
						<small>Request Type</small>
						<select class="form-control input-sm" onchange="get_request(this.value)" id="request" <?=$disabled?>>
						<?php if(!$request_type){ ?>
							<option value=''>--Select--</option>
							<?php } else { ?>
								<option value=''>--Select--</option>
								<?php foreach($request_type as $rt){ ?>
									<option value='<?=$rt['rt_id']?>'><?=$rt['rt_desc']?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-3">
						<small>Sub Type</small>
						<div id='req_loader'>
							<select class="form-control input-sm" onchange="house(this.value)" id="sub_req_type" <?=$disabled?>>
								<option value="">--Select--</option>
							</select>
						</div>
					</div>
					<div class="col-md-3" id='haus_loader'>
						<small>House Concern Sub Type</small>
						<div>
							<select class="form-control input-sm" id="haus" <?=$disabled?>>
								<option value="">--select house concern type--</option>
							</select>
						</div>
					</div>
				</div>
			<?php } ?>
				<div class="row">
					<div class="col-sm-12">
						<strong>Concern Details:</strong>
					</div>
					<div class="col-sm-12">
						<span><?=$concern?></span>
					</div>
				</div>
			</div>	
		</div>

	<?php } ?>

<?php } ?>


<?php if($acc[1]!=0) { ?>
<div class="row">
	<div class="col-sm-2">
		<input type="button" name="" class="btn btn-sm btn-primary form-control input-sm" value="Confirm" <?php if($validation == 0 || $email_verif == 0){ echo "disabled"; } else { echo " onclick='conf($get_id)'"; } ?> > 
	</div>
	<div class="col-sm-2">	
		<input type="button" name="" class="btn btn-sm btn-danger form-control input-sm" value="Reject" <?php if($email_verif == 0){ echo "disabled"; } else { echo "onclick='reject()'";} ?> > 
	</div>
</div>
<?php } ?>
<br>

<div id="reject">

	<div class="panel panel-primary">

		<div class="panel-heading">Rejection Reason</div>

		<textarea id="thisarea" name="thisarea"></textarea>

	</div>

	<div class="row">
		<div class="col-sm-2">
			<input type="button" class="btn btn-primary btn-sm form-control input-sm" value="Reject" onclick="rej(<?=$get_id?>)">
			<input type="hidden" id="get_id" value="<?=$get_id?>">
		</div>		
		<div class="col-sm-2">
			<input type="button" class="btn btn-danger btn-sm form-control input-sm" value="Cancel" onclick="cancel()">
		</div>			
	</div>

	<br>

</div>



