 <script type="text/javascript">

    function paginate(val){
        $('#owners').hide();
        $('#loading').show();
		var keyword = $('#keyword').val(); 

        $.ajax({
            type:"POST",
            url: "<?=base_url()?>support/search_homeowner/"+val,
            data: {
                keyword: keyword
            },
            success: function(data){
                $('#owners').html(data);
                $('#owners').slideDown('slow');
                $('#loading').css('display', 'none');
            }
        });
    }

</script>
		<?php if(!$search){ ?>
				<?php
				if(!$search && $company!="") {
					$where = "PROJ_ID = ".$company;
					$select = "";
					$table = "ZHOA_PROJ_AREA";
					$odby = "PROJ_ID ASC";
					$asd = $this->Main->get_data_distinct_where($select, $table, $where, $odby);
					$co;
					foreach ($asd as $key) {
						$co = $key['PROJ_AREA'];
					}
					echo "<div class=\"panel panel-primary\" style=\"border-color:#d43f3a\">";
					echo "<div class=\"panel-heading\" style=\"background-color:#d9534f;border-color:transparent\">";
					echo "<span class=\"panel-title pull-left\">No Results Found for "; if($company != ""){echo"[ Company: <i>$co</i> ]";} if($project != ""){echo"[ Project: <i>$project</i> ]";} if($keyword != ""){echo"[ Keyword: <i>$keyword</i> ]";} echo "</span>"; 
				} elseif($keyword!="") {
					echo "<div class=\"panel panel-primary\" style=\"border-color:#d43f3a\">";
					echo "<div class=\"panel-heading\" style=\"background-color:#d9534f;border-color:transparent\">";
					echo "<span class='panel-title pull-left'>No Results Found for "; if($project != ""){echo"[ Project: <i>$project</i> ]";} if($keyword != ""){echo"[ Keyword: <i>$keyword</i> ]";} echo"</span>"; 
				} else {
					echo "<div class=\"panel panel-primary\" style=\"border-color:#d43f3a\">";
					echo "<div class=\"panel-heading\" style=\"background-color:#d9534f;border-color:transparent\">";
					echo "<span class='panel-title pull-left'>No Results Found!</span>"; 
				}
				?>
					<div class="clearfix"></div>
				</div>
			</div>
		<?php } else { ?>
			
				<?php
				if($company != "") {
					$where = "PROJ_ID = ".$company;
					$select = "";
					$table = "ZHOA_PROJ_AREA";
					$odby = "PROJ_AREA ASC";
					$asd = $this->Main->get_data_distinct_where($select, $table, $where, $odby);
					$co;
					foreach ($asd as $key) {
						$co = $key['PROJ_AREA'];
					}
					echo "<div class=\"panel panel-primary\" style=\"border-color:#4cae4c\">";
					echo "<div class=\"panel-heading\" style=\"background-color:#5cb85c;border-color:transparent\">";
					echo "<span class=\"panel-title pull-left\">Search results for"; if($company != ""){echo"[ Company: <i>$co</i> ]";} if($project != ""){echo"[ Project: <i>$project</i> ]";} if($keyword != ""){echo"[ Keyword: <i>$keyword</i> ]";} echo "</span>"; 
				} elseif($keyword != "") {
					echo "<div class=\"panel panel-primary\" style=\"border-color:#4cae4c\">";
					echo "<div class=\"panel-heading\" style=\"background-color:#5cb85c;border-color:transparent\">";
					echo "<span class=\"panel-title pull-left\">Search results for"; if($project != ""){echo"[ Project: <i>$project</i> ]";} if($keyword != ""){echo"[ Keyword: <i>$keyword</i> ]";} echo "</span>"; 
				}
				?>
					<div class="clearfix"></div>
				</div>
			</div>
	<div class="panel panel-primary">
		<div class="table-responsive" style="border-radius:inherit">
			<table class="table" style="border-radius:inherit">
				<tr style="background-color:#337AB7;color:#fff">
					<th style="text-align:center;border-color:#337AB7">Username</th>
					<th style="text-align:center;border-color:#337AB7">Last Name</th>
					<th style="text-align:center;border-color:#337AB7">First Name</th>
					<th style="text-align:center;border-color:#337AB7">Project</th>
					<th style="text-align:center;border-color:#337AB7">Block/Lot</th>
					<th style="text-align:center;border-color:#337AB7">Unit Type</th>
					<th style="text-align:center;border-color:#337AB7">Last Login Date</th>
				</tr>
				<?php foreach($search as $row){ ?>
					<?php
					$dte="";
					$select=""; $table="ZHOA_logs"; $where="tl_username ='".$row['b_uname']."'"; $orderby = "tl_date DESC"; $limit = 1;
					$get_date = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start = 0);
						foreach($get_date as $d){
						$dte = $d['tl_date'];
						}
						$log = strtotime($dte);
						if (!$log) {
							$lastlog ="";
						} else {
							$lastlog = date('F d, Y', $log);
						}
					?>
					<tr class="data_click" onclick="window.location.href='<?=base_url()?>support/ownerinfo/<?=$row['bi_id']?>'">
						<td style="width: 12%;"><?=$row['b_uname']?></td>
						<td style="width: 13%;"><?=$row['bi_lname']?></td>
						<td style="width: 15%;"><?=$row['bi_fname']?></td>
						<td style="width: 20%;"><?=$row['bi_project']?></td>
						<td style="width: 10%;"><?=$row['bi_blklot']?></td>
						<td style="width: 15%;"><?=$row['u_desc']?></td>
						<td style="width: 15%;"><?=$lastlog?></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	<?php } ?>
</div>


<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$search_links?></div>
