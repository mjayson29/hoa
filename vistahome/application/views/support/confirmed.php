 <script type="text/javascript">

    function paginate(val){
        $('#loader').hide();
        $('#loading').show();

        $.ajax({
	        type:"post",
	        url: "<?=base_url()?>support/get_confirmed/"+val,
	        success: function(data){
	            $('#loader').html(data);
	            $('#loader').slideDown('fast');
	            $('#loading').hide();
	        }
    	});
    }
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var showChar = 20;
		var ellipsestext = "...";
		$('#messagecont').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span>';
				$(this).html(html);
			}
		});
	});
</script>

<div class="panel panel-green">

	<div class="panel-heading">Confirmed</div>

	<?php if(!$confirmed){ ?>

	<div class="panel-body">

		<span>NO DATA</span>

	</div>

	<?php } else { ?>

	<div class="table-responsive">

		<table class="table">
 			
 			<tr>
				<th style="width: 12%;">SignUp Date</th>
				<th style="width: 12%;">Project Area</th>
				<th style="width: 10%;">Project</th>
				<th style="width: 10%;">Blk/Lot</th>
				<th style="width: 10%;">Unit</th>
				<th style="width: 10%;">Concern</th>
				<th style="width: 10%;">Status</th>
				<th style="width: 14%;">Processed Date</th>
				<th style="width: 12%;">Processed By</th>
			</tr>

			<?php 
				foreach($confirmed as $row){ 
					$signup = strtotime($row['bi_regdate']);
					$select=""; $table="ZHOA_unit"; $where="u_id = ".$row['bi_unit'];
					$get_unit = $this -> Main -> select_data_where($select, $table, $where);
					foreach($get_unit as $u){
						$unit = $u['u_desc'];
					}

				/*$select=""; $table="ZHOA_concern_temp"; $where="ct_owner_id = ".$row['bi_owner_id'];
				$get_concern = $this -> Main -> select_data_where($select, $table, $where);
					$concern = "No Concern";
					foreach($get_concern as $c){
						$trim_concern = $c['ct_desc'];
						$concern = (strlen($trim_concern)<=25 ? $trim_concern : substr($trim_concern,0,25)."...");
					}*/
				$concern = "No Concern";
				if($row['bi_temp_concern'] != ""){
					$concern = $row['bi_temp_concern'];
				}

				if($row['bi_process_by']!="" || $row['bi_process_date']!=""){
					$process_by = $row['a_uname'];
					$get_date = strtotime($row['bi_process_date']);
					$process_date = date('M d, Y', $get_date);//' . "<br>" . date('h:i:s a', $get_date);									
				} else {
					$process_by = "--";									
					$process_date = "--";									
				}
				?>

				<tr class="data_click" onclick="window.location.href='<?=base_url()?>support/validated_reg/<?=$row['bi_id']?>'">

					<td style="width: 10%;"><?=date('M d, Y' ,$signup);?></td>
					<td style="width: 10%;"><?=$row['PROJ_AREA']?></td>
					<td style="width: 10%;"><?=$row['bi_project']?></td>
					<td style="width: 10%;"><?=$row['bi_blklot']?></td>
					<td style="width: 10%;"><?=$unit?></td>
					<td id="messagecont" style="width: 20%;"><?=$concern?></td>
					<td style="width: 10%;">

						<?php 
							$reg_date = strtotime($row['bi_regdate']);
							$get_reg_date = date('Y-m-d', $reg_date);
						   	switch ($row['bi_status']){
						   		case 0:
						   			if($get_reg_date == date("Y-m-d")){
						  				echo "New";
						   			}else{
						   				echo "Outstanding";
						   			}
						   			break;
						   		case 1:
						   			echo "Confirmed";
						   			break;
						   		case 2:
						   			echo "Rejected";
						   			break;
						   	}  	
						?>
					</td>
					<td style="width: 10%;"><?=$process_date?></td>
					<td style="width: 10%;"><?=$process_by?></td>
				</tr>

			<?php } ?>

		</table>

	</div>

	<?php } ?>

</div>

<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$confirmed_links?></div>

