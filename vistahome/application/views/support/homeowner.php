 <script type="text/javascript">
    function paginate(val){
        $('#owners').hide();
        $('#loading').show();
        $.ajax({
	        type:"post",
	        url: "<?=base_url()?>support/homeowner_new/"+val,
            success: function(data){
                $('#owners').html(data);
                $('#owners').slideDown('slow');
                $('#loading').hide();
            }
        });
    }
</script>

<div class="panel panel-primary">
	<div class="panel-heading">Homeowner List</div>

	<?php if(!$new){ ?>
		<div class="panel-body">
			<span>NO DATA</span>
		</div>
	<?php } else { ?>
		<div class="table-responsive">
			<table class="table">
				<tr style="background-color:#F3F3F3">
					<th>Username</th>
					<th>Last Name</th>
					<th>First Name</th>
					<th>Project</th>
					<th>Block/Lot</th>
					<th>Unit Type</th>
					<th>Last Login Date</th>
				</tr>
				<?php foreach($new as $row){ 
					$dte="";
					$select=""; 
					$table="ZHOA_logs"; 
					$where="tl_username ='".$row['b_uname']."'"; 
					$orderby = "tl_date DESC"; 
					$limit = 1;
					$get_date = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start = 0);
					
					foreach($get_date as $d){
						$dte = $d['tl_date'];
					}
					$log = strtotime($dte);
					
					if (!$log) {
						$lastlog ="";
					} else {
						$lastlog = date('M d, Y', $log);
					}
				?>
					<tr class="data_click" onclick="window.location.href='<?=base_url()?>support/ownerinfo/<?=$row['bi_id']?>'">
						<td style="width: 12%;"><?=strtolower($row['b_uname'])?></td>
						<td style="width: 13%;"><?=ucwords(strtolower($row['bi_lname']))?></td>
						<td style="width: 15%;"><?=ucwords(strtolower($row['bi_fname']))?></td>
						<td style="width: 20%;"><?=$row['bi_project']?></td>
						<td style="width: 10%;"><?=$row['bi_blklot']?></td>
						<td style="width: 15%;"><?=$row['u_desc']?></td>
						<td style="width: 15%;"><?=$lastlog?></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	<?php } ?>
</div>


<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>
