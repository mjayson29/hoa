<script type="text/javascript">
	function paginate(val){
	  	$('#plist').hide();
	  	$('#loading').show();
	    $.ajax({
	    	type:"get",
	    	url: "<?=base_url()?>support/project_list/"+val,
	      	success: function(data){
		        $('#plist').html(data);
		        $('#plist').slideDown('fast');
		        $('#loading').hide();
	      	}
	    });
  	}

</script>
<?php
$acc = explode("-", $access);
?>
		<?php if(!$new){ ?>
		<div class="panel panel-primary" style="border-color:#d43f3a">
			<div class="panel-heading" style="background-color:#d9534f;border-color:transparent">
				<span class="panel-title pull-left">No Users Found!</span> 
				<div class="clearfix"></div>
			</div>
		</div>
		<?php } else { ?>
		<div class="panel panel-primary">
			<div class="table-responsive" style="border-radius:inherit">
				<table class="table" style="border-radius:inherit">
					<tr style="background-color:#337AB7;color:#fff">
						<th style="border-color:#337AB7">Active</th>
						<th style="border-color:#337AB7">Project Name</th>
						<th style="border-color:#337AB7">Company</th>
					</tr>
				<?php foreach($new as $row){ ?>
					<tr class="data_click">
						<td style="width: 15%;"><input id="pbox<?=$row['SWENR']?>" type="checkbox" onchange="soa_active(<?=$row['SWENR']?>)" name="myTextEditBox" <?php echo $row['SOA_ACTIVE']==1? 'checked': '';?>></td>
						<td style="width: 15%;"><?=$row['XWETEXT']?></td>
						<td style="width: 15%;"><?=$row['BUTXT']?></td>
					</tr>
				<?php } ?>
				</table>
			</div>
		<?php } ?>
		</div>

<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>
