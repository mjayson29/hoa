<script type="text/javascript">

	function paginate(val){

        $.ajax({
	        type:"post",
	        url: "<?=base_url()?>support/announce_load/"+val,
	        success: function(data){
	            $('#ann_wrap').html(data);
	        }
	    });
    }
	
	function con(val){
		$('#ann_'+val+' .ann_control').addClass('shoCon');
		$('#ann_'+val+' .icon-pencil').hover(function(){
			$('#ann_'+val+' .icon-pencil').css('color','#000000');
			}, function(){
				$('#ann_'+val+' .icon-pencil').css('color','#A3A3A3');
			});
		$('#ann_'+val+' .glyphicon-ban-circle').hover(function(){
			$('#ann_'+val+' .glyphicon-ban-circle').css('color','#000000');
			}, function(){
				$('#ann_'+val+' .glyphicon-ban-circle').css('color','#A3A3A3');
			});

		$('#ann_'+val).mouseout(function(){
			$('#ann_'+val+' .ann_control').removeClass('shoCon');
		});

		$('#ann_'+val+' .icon-pencil').on('click', function(){
			var limit = 16;
			var ew = $('#ann_'+val+' .more p').text();
			var su = $('#ann_'+val+' h4').text();
			var ui = $('#ann_'+val+' #entry_id').val();
			var el = document.getElementById('ann_'+val),
            all = el.getElementsByTagName('img'),
            i=0;
            if(i < all.length) {
				var imgsrc = $('#ann_'+val+' img').attr('src');
				var fname = imgsrc.slice((imgsrc.lastIndexOf("/") - 1 >>> 0) + 2);
				var iname;
				if(fname.length > limit){
					iname = fname.substr(0, limit) + "...";
				}
				document.getElementById("uplabel").textContent=iname;
				$('#uplabel').attr('title',fname);
				$('#blah').attr('src', imgsrc);
				$('#upcon').css('display','block');
            } else {
				$('#upcon').css('display','none');
            }
			tinyMCE.activeEditor.setContent(ew);
			$('#ann_id').val(su);
			$('#upd_id').val(ui);
			$('#post_notif').css('display', 'none');
			$('#update_notif').css('display', 'none');
			$('#cancel_notif').css('display', 'none');
			$('#ann_btn_post').css('display','none');
			$('#new_post_announce').css('display','block');
			$('#ann_btn_upd').css('display','block');
			$('#sendingpanel').css('display','none');
		});

		
	}

	$(document).ready(function() {
		var showChar = 500;
		var ellipsestext = "...";
		var moretext = "show more";
		var lesstext = "";
		$('.more p').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);
				var h = content.substr(showChar, content.length - showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>'+h+'</span><a href="" class="morelink">'+moretext+'</a></span>';
				$(this).html(html);
			}

		});

		$(".morelink").click(function(){
			if($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
			}
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
			return false;
		});
	});


</script>

<?php
		if(!$announcements){ ?>
		<div class="text-center">NO DATA</div>
<?php 
	} else {
	 	$i=0; 
		foreach ($announcements as $value) {
			$subject = $value['a_subject'];
			$message = $value['a_msg'];
			$posted = $value['a_dposted'];
			$dupd = $value['a_dupd'];
			$byupd = $value['byupd'];
			$postedby = $value['postedby'];
			$id = $value['a_id'];
			$attach = $value['a_attachment'];
?>
	<div id="ann_<?=$i?>" class="col-md-6" onmouseover="con(<?=$i?>)">
		<div class="row ann_item">
			<input type="hidden" id="entry_id" value="<?=$id?>">
			<div class="col-md-12 ann_content">
				<div class="col-md-8 ann_control">
					<div style="float:right; margin:10px auto">
						<span class="icon-pencil" title="Edit"> &nbsp;</span>
						<span class="glyphicon glyphicon-ban-circle" onclick="cancel_announce(<?=$id?>)" title="Cancel"></span>
					</div>
				</div>
				<a href="#"><h4><?=$subject?></h4></a>
				<hr style="margin-top:10px">
				<div class="anndet">
					<div class="more"><?=$message?></div>
					<?php if($attach!=""){ ?>
						<?php 
						// if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/announcements/".$attach)){  // for local use only
						if(file_exists(APPPATH."../assets/img/announcements/".$attach)){  //for devsite
						?>
					<div class="uprev" style="position:relative; margin-bottom:10px">
						<img src="application/../../assets/img/announcements/<?=$attach?>">
					</div>
						<?php 
						} ?>
					<?php } ?>
				</div>
			</div>
		
		</div>
	<div style="padding: 0px 10px">
	<?php if($value['a_is_updated']==0){ ?>
		<small><label>Date Posted: &nbsp</label> <?=date('F d, Y', strtotime($posted))?></small><br />
		<small><label>Posted By: &nbsp</label> <?=$postedby?></small>
	<?php }else{ ?>
		<small><label>Original Date Posted: &nbsp</label> <?=date('F d, Y', strtotime($posted))?></small>
		<small style="float:right"><label>Updated: &nbsp</label> <?=date('F d, Y', strtotime($dupd))?></small>
		<br />
		<small><label>Posted By: &nbsp</label> <?=$postedby?></small>
		<small style="float:right"><label>Updated by: &nbsp</label> <?=$byupd?></small>
	<?php } ?>
	</div>
</div>
<?php
	$i++;
	}
} ?>
<div class="col-md-12">
	<hr/>
	<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

	<div style="text-align: center;"><?=$new_links?></div>
</div>