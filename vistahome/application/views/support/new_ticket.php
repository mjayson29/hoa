<div id="tkt_result" class="col-md-12">

<script type="text/javascript">
	tinymce.init({
        selector: "textarea#thisarea4",
    });

    $('#btn_att').click(function(){
    	$('#userfile').trigger('click');
    })

	$('#btn_conadd').click(function() {
		var accnt = $('#name').text();
		var proj = $('#proj').val();
		var req = $('#sel_type option:selected').val();
		var sub_type = $('#sel_sub option:selected').val();
		var sub_opt = $('#sel_opt option:selected').val();
		var message = window.parent.tinymce.get('thisarea4').getContent();
		var attachment = $('#userfile').val();
		var attach = $('#userfile').prop('files');

		if(req == "" || sub_type == "" || message == ""){
			bootbox.alert({
				size: "small",
				message: "Please fill up the field required"
			});
			return false;
		}

		if((sub_type != 1 && sub_type != 6) && sub_opt == "") {
			bootbox.alert({
				size: "small",
				message: "Please fill up the field required 2"
			});
			return false;
		}

        var fd = new FormData();
        fd.append('userfile', attach[0]);

        $.ajax({
            url: '<?=base_url()?>portal/request_upload/',
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){ 
            }
        });

        var concern = {"req" : req, "sub_type" : sub_type, "sub_opt" : sub_opt, "message" : message, "attachment" : attachment }

        $("#clist").load('<?=base_url()?>support/concern_enlist/', concern );
        $("#clist").css('display','block');
        $("#con_list").css('display','block');
        $("#sel_type").val('');
        $("#sel_sub").val('');
        $("#sel_opt option").remove();
        $('#sub_opt').css('display','none');
        tinymce.get('thisarea4').setContent('');
        $("#userfile").val('');

	});

		function showup(input){
		var limit = 16;
    	var raw = input.files;
    	if (input.files && input.files[0]) {
    		if(raw[0].size > 2097152){ //approx 2MB
    			bootbox.alert({
    				size: 'small',
    				message: 'The File you are trying to attach is too large'
    			});
    			reset($('#userfile'));
    			return false;
    		}
            var reader = new FileReader();
            var f = input.files[0];
            var fname = escape(f.name);
            var x = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);
            var ext = x.toUpperCase();
            if(ext != 'JPG' && ext != 'PNG' && ext != 'JPEG' && ext != 'PDF' && ext != 'DOCX' && ext != 'DOC' && ext != 'XLSX' && ext != 'XLS'){
            	bootbox.alert({
    				size: 'small',
    				message: 'Invalid file type'
    			});
    			reset($('#userfile'));
    			return false;
            }
            $('#att_name').text(fname);
            reader.readAsDataURL(input.files[0]);
            $('#att_label').css('display','block');
        }
    }

    window.reset = function (e) {
	    e.wrap('<form>').closest('form').get(0).reset();
	    e.unwrap();
	}

</script>

<?php if(!$res) { ?> 
<div class="col-md-12">	
	<div class="panel panel-red">
		<div class="panel-heading">
			<span> NO RESULTS FOUND!</span>
			<span class="clearfix"></span>
		</div>
	</div>
</div>
<?php } else { 
		$i = 0;
		foreach ($res as $r) {
			$accnt = $r['KUNNR'];
			$name = $r['NAME1'];
			$cont = $r['LANDLINE'];
			$email = $r['EMAIL'];
			$pcode = $r['SWENR'];
			$pro = $r['XWETEXT'];
			$blk = $r['REFNO'];
			$so = $r['VBELN'];
			$i++;
		}
?>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<span>SOURCE OF REQUEST</span>
			<span class="clearfix"></span>
		</div>
		<div class="panel-body">
			<div class="col-md-4 col-md-offset-8">
				<select id="sel_source1" class="form-control">
					<option value=""> -- SOURCE OF REQUEST --</option>
					<?php foreach ($source as $k) { ?>
						<option value="<?=$k['sr_id']?>"> &nbsp;<?=$k['sr_text']?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<span>HOMEOWNER INFORMATION</span>
			<span class="clearfix"></span>
		</div>
		<div class="panel-body">
			<div class="col-md-6">
				<div class="col-md-12">
					<div class="col-md-5">
						<label class="custom_label">NAME</label>
					</div>
					<div id="name" class="col-md-7"> <?=$name?></div>
				</div>
				<div class="col-md-12">
					<div class="col-md-5">
						<label class="custom_label">CONTACT NUMBER</label>
					</div>
					<div class="col-md-7"> <?=$cont?></div>
				</div>
				<div class="col-md-12">
					<div class="col-md-5">
						<label class="custom_label">E-MAIL ADDRESS</label>
					</div>
					<div class="col-md-7"> <?=$email?></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-md-12">
					<div class="col-md-5">
						<label class="custom_label">ACCOUNT NUMBER</label>
					</div>
					<div class="col-md-7"><?=$accnt?></div>
					<input type="hidden" id="so" value="<?=$so?>">	
					<input type="hidden" id="kunnr" value="<?=$accnt?>">
				</div>
				<div class="col-md-12">
					<div class="col-md-5">
						<label class="custom_label">PROJECT</label>
					</div>
					<div id="proj" value="<?=$pcode?>" class="col-md-7"><?=$pro?></div>
				</div>
				<div class="col-md-12">
					<div class="col-md-5">
						<label class="custom_label">BLK/LOT</label>
					</div>
					<?php if($i>1){ ?>
					<div class="col-md-7">
						<select id="blk" class="form-control">
							<option value=""> --Select Blk/ Lot--</option>
							<?php foreach ($res as $a) { ?>
								<option value="<?=$a['REFNO']?>"><?=$a['REFNO']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="clearfix"></div>
					<?php } else { ?>
					<div id="blk" class="col-md-7"><?=$blk?></div>
					<?php } ?> 
				</div>
			</div>
		</div>
	</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span> TYPE OF REQUEST</span>
					<span class="clearfix"></span>
				</div>
				<div class="panel-body">
					<div class="col-md-4">
						<select id="sel_type" class="home form-control" onchange="get_req(this.value)">
							<option value=""> -- SELECT TYPE --</option>
							<?php foreach ($rtype as $r) { ?>
								<option value="<?=$r['rt_id']?>"> &nbsp;<?=$r['rt_desc']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<select id="sel_sub" class="home form-control" onchange="house()">
							<option value=""> --select sub type--</option>
						</select>
					</div>
					<div id="sub_opt" class="home col-md-4" style="display:none">
						<select id="sel_opt" class="form-control">
						</select>
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span>MESSAGE</span>
					<span class="clearfix"></span>
				</div>
				<div class="panel-body">
					<textarea id="thisarea4" name="thisarea4"></textarea>
					<div class="col-md-12" style="margin-top:15px; padding: 0">
						<div class="col-md-6" style="padding-left:0;">
							<div id="att_label" style="display:none">
								<label class="custom_label">Attachment</label>
								<span id="att_name"></span>
							</div>
						</div>
						<div class="col-md-1 col-md-offset-2" style="padding-right: 0">
							<button id="btn_att" class="btn-warning form-control" title="Attach image or document file"><span class="glyphicon glyphicon-paperclip"></span></button>
							<input type="file" id="userfile" style="display:none" onchange="showup(this)">
						</div>
						<div class="col-md-1" style="padding-right: 0">
							<button id="btn_conadd" class="btn-success form-control" title="Add this concern"><span class="glyphicon glyphicon-plus"></span></button>
						</div>
						<div class="col-md-2" style="padding-right: 0">
							<button class="btn-danger form-control" title="Cancel this concern" onclick="can()">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
			<div id="con_list" style="display:none">
				<div id="clist" style="display:none"> </div>
				<div class="container" style="padding:0">
					<div class='col-md-1' style="padding-left:0"> <input type='button' class='btn btn-primary btn-sm form-control' onclick='save_ticket1(0)' value='Save'> </div>
					<div class='col-md-1' style="padding-left:0"> <input type='button' class='btn btn-danger btn-sm form-control' name='cancel' value='Cancel' onclick="cancel1()"> </div>
				</div>
			</div>
</div>

<?php } ?>

<script type="text/javascript">
	
function save_ticket1(n) {
		var id = n;
		var source = $('#sel_source1 option:selected').val();
		var so = $('#so').val();
		var i = "<?php if($res){ echo $blk; }?>";
		var str = $('#name').text();
		var name = str.split(" ");
		var name1 = name[1]; 
		var blk;
		var kunnr = $('#kunnr').val();

		if (i>1) {
			blk = $('select #blk option:selected').val();
		}else {
			blk = $('#blk').text();
		}

		if(source == ""){
			bootbox.alert({
				size: 'small',
				message: 'Please select source of request'
			});
			return false;
		}

		bootbox.confirm({
			size: 'small',
			message: 'Are you sure you want to post this ticket?',
			callback: function(result){
				if(result){
					$('#page_loader').css('display','block');
					$.ajax({
						type: 'POST',
						url: '<?=base_url()?>support/save_ticket/',
						data: {
							source: source,
							so: so,
							blk: blk,
							name: name1,
							id: id,
							kunnr: kunnr
						},
						success: function(){
							$('#page_loader').css('display','none');
							$('#tkt_result').css('display','none');
							$('#con_list').css('display','none');
							$('#tkt_notif').css('display','block');
						}
					})
				}
			}
		})
	}

	function cancel1() {

		bootbox.confirm({
			size: 'small',
			message: 'Are you sure you want to discard all concerns?',
			callback: function(result){
				if(result){
					$.ajax({
						type: 'POST',
						url: "<?=base_url()?>support/unset_sess_tkt/",
			            success: function(data){
							$('#tkt_result').css('display','none');
							$('#con_list').css('display','none');
							$('#tkt_pro').val('');
							$('#tkt_blk').val('');
							$('#tkt_accnt').val('');
			            }
					})			
				}
			}
		})		
	}

	function can() {
		$('#sel_type').val('');
		$('#sel_sub').val('');
		$('#sel_opt').val('');
		$('#sub_opt').css('display','none');
		tinymce.get('thisarea4').setContent('');
		$('#att_label').css('display','none');
		reset($('#userfile'));
	}

</script>