<script type="text/javascript">

    $(document).ready(function(){

        $("#mod").draggable({
            handle: ".modal-header"
        });

        $('#concern_close').on('hidden.bs.modal', function () {
        	$('#cby').val('');
        	$('#cdate').val('');
        });

	    var fileInput = $('#userfile');
	    var bi_id = $('#get_id').val();
	    // button = $('#upload');
	    fileInput.on('change', function(){
        var files = fileInput.prop('files');

        // No file was chosen!
        /*if(files.length == 0) {
          alert('Please choose a file to upload!');
          return false;
        }*/
        // Create a new FormData object, append yung file para makuha mo yung $_POST
        var fd = new FormData();
        var attachment = fd.append('userfile', files[0]);

        // var upload = confirm('Are you sure to change profile image?');
        
        // if(upload == true){   
        // Upload the file m,

          $.ajax({
            url: '<?=base_url()?>support/upload/'+bi_id,
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){
              document.getElementById("load_reply").innerHTML = data;  
            }

          });
      });
    });

</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#mod').on('shown.bs.modal', function () {
			$('.chosen-select', this).chosen();
		});

		var showChar = 90;
		var ellipsestext = "...";
		$('.messcont').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span>';
				$(this).html(html);
			}
		});
	});
</script>

<script type="text/javascript">

	// function dvals(input){

	// 	if(input != ""){
	// 		var q = new Date();
	// 		var m = q.getMonth();
	// 		var d = q.getDate();
	// 		var y = q.getFullYear();

	// 		var today = new Date(y,m,d);

	// 		mydate=new Date(input);

	// 		var strvalidate = /^([1-9]|0[1-9]|1[0-2])([\/])([1-9]|0[1-9]|1\d|2\d|3[01])([\/])(19|20)\d{2}$/;
	// 		var datepick = /^(19|20)\d{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])$/;

	// 		if(input.match(strvalidate)){
	// 			$('#pstart').css({
	// 				'border-color':'',
	// 				'box-shadow':'',
	// 				'outline':''
	// 			});
	// 		} else{
	// 			if(input.match(datepick)){
	// 				$('#pstart').css({
	// 					'border-color':'',
	// 					'box-shadow':'',
	// 					'outline':''
	// 				});
	// 			} else{
	// 				$('#pstart').css({
	// 					'border-color':'#ff4d4d',
	// 					'box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(255, 128, 128, 0.6)',
	// 					'outline':'0 none'
	// 				});
	// 			}
	// 		}

	// 		if(today>mydate) {
	// 		    bootbox.alert({
	// 		    	size: "small",
	// 		    	message: "Please enter a date not less than today."
	// 		    })
	// 		    $('#pstart').css({
	// 				'border-color':'#ff4d4d',
	// 				'box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(255, 128, 128, 0.6)',
	// 				'outline':'0 none'
	// 			});
	// 		}
	// 	}
	// }

	function dvalc(input){
		if(input != ""){
			var q = new Date();
			var m = q.getMonth();
			var d = q.getDate();
			var y = q.getFullYear();

			var today = new Date(y,m,d);

			mydate=new Date(input);

			var strvalidate = /^([1-9]|0[1-9]|1[0-2])([\/])([1-9]|0[1-9]|1\d|2\d|3[01])([\/])(19|20)\d{2}$/;
			var datepick = /^(19|20)\d{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])$/;

			if(!input.match(strvalidate)){
				if(!input.match(datepick)){
					bootbox.alert({
                        size: "small",
                        message: "Invalid date format"
                    });
				}
			}

			if(today<mydate) {
			    bootbox.alert({
			    	size: "small",
			    	message: "Please enter a date not greater than today."
			    })
			}
		}
	}

	function dvalf(input){
		if(input != ""){
			var st = $("#pstart").val();
			finish=new Date(input);
			start = new Date(st);
			console.log(start);
			console.log(finish);

			var strvalidate = /^([1-9]|0[1-9]|1[0-2])([\/])([1-9]|0[1-9]|1\d|2\d|3[01])([\/])(19|20)\d{2}$/;
			var datepick = /^(19|20)\d{2}\-(0[1-9]|1[0-2])\-(0[1-9]|1\d|2\d|3[01])$/;

			if(input.match(strvalidate)){
				$('#pfinish').css({
					'border-color':'',
					'box-shadow':'',
					'outline':''
				});
			} else{
				if(input.match(datepick)){
					$('#pfinish').css({
						'border-color':'',
						'box-shadow':'',
						'outline':''
					});
				} else{
					$('#pfinish').css({
						'border-color':'#ff4d4d',
						'box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(255, 128, 128, 0.6)',
						'outline':'0 none'
					});
				}
			}

			if(start>finish) {
			    bootbox.alert({
			    	size: "small",
			    	message: "Please enter a date not less than start date."
			    })
			    $('#pfinish').css({
					'border-color':'#ff4d4d',
					'box-shadow':'0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(255, 128, 128, 0.6)',
					'outline':'0 none'
				});
			}
		}
	}
</script>

<script type="text/javascript">

	function cler(val){
		$('#con_id').val(val);
        $("#reqtyp").val('');
        $("#subtyp_loader").val('');
        $("#housetyp_loader").val('');
        $('#housecon').css('display','none');
    }

    function reqedit(val){
		var valid = $("#validity"+val).val();
		if(valid == 1){
			$('#c_valid').prop('checked',true);
		}else{
			$('#c_valid').prop('checked',false);
		}
		var req = $("#req"+val).val();
        $("#reqtyp").val(req);
		get_req(req);
		var sub = $("#sub"+val).val();
		house(sub);
		var hsec = $("#hsec"+val).val();
		if(hsec != ""){
			$('#housecon').css('display','block');
			}else{
			$('#housecon').css('display','none');
		}
		var pstart = $("#pstart"+val).val();
		$('#pstart').val(pstart);
		var pfinish = $("#pfinish"+val).val();
		$('#pfinish').val(pfinish);
		var emp = $("#emp"+val).val();
		var dept = $("#dept"+val).val();
		var task = $("#task"+val).val();
		$('#tasktxt').val(task);
		$('#con_id').val(val);
        $('#mod').on('shown.bs.modal', function () {
        	$("#subtyp_loader option[value='"+sub+"']").attr('selected','selected');
        	$("#housetyp_loader option[value='"+hsec+"']").attr('selected','selected');
        	console.log($("#subtyp_loader option[value='"+sub+"']").text());
        	console.log($("#housetyp_loader option[value='"+hsec+"']").text());
        	if(emp !=""){
	        	var er = $("#er option[value='"+emp+"']").text();
	        	var fil2 = "<span>"+er+"</span> <div><b></b></div>"
	        	$('#er_chosen .chosen-single').html(fil2);
	        	$('#er').val(emp);
        	}
        	if(dept !=""){	
        		var dpt_id = $('#dpt option').filter(function () { return $(this).html() == ""+dept+""; }).val();
				var fil = "<span>"+dept+"</span> <div><b></b></div>"
	        	$('#dpt_chosen .chosen-single').html(fil);
	        	$('#dpt').val(dpt_id);
        	}
		});
		$('#mod').on('hidden.bs.modal', function () {
			req = "";
			sub = "";
			hsec = "";
			pstart = "";
			pfinish = "";
			emp = "";
			dept = "";
			task = "";
			er = "";
			fil2 = "";
			dpt_id = "";
			fil = "";
			$('#er_chosen .chosen-single').html("<span>Employee Responsible</span> <div><b></b></div>");
			$('#dpt_chosen .chosen-single').html("<span>Department Responsible</span> <div><b></b></div>");
		})
    }

    function chckall(i){
    	console.log(i);
    	if($('#'+i+'').is(':checked')){
    		console.log('checked');
    		$('.'+i+'').prop('checked', true);
    	}else{
    		$('.'+i+'').prop('checked', false);
    		console.log('unchecked');
    	}
    }

    function cls() {
    	var val = $('#c_valid');
		var inv = $('#c_invalid');
		if(inv.is(':checked')){
			$('#invalid_pane').slideDown('fast');
		} else {
			$('#invalid_pane').slideUp('fast');
		}
    }
</script>

<style type="text/css">
	.table > tbody > tr > td{
		border: 0 none;
	}
</style>

<?php if(!$concerns){ ?>

	<div class="panel-body">No Data</div>

<?php } else { ?>

	<div class="panel panel-primary">
		<div class="panel-heading">Ticket #<?=$ticketid?> </div>
	</div>

	<?php if(!$buyer){ ?>
	<?php  ?>
		<div class="panel panel-primary">
			<div class="panel-heading">Account and Property Information</div>
			<div class="panel-body">
				<span>NO DATA</span>
			</div>
		</div>

	<?php } else { ?>

		<?php foreach($buyer as $row){ ?>	
	 
			<?php 
			$signup = $row['bi_regdate']; 
			$pcode = $row['SWENR'];
			$move_in_temp = $row['MOVEIN_DATE'];
			$accept_temp = $row['ACCEPT_DATE'];
			$comp_temp = $row['COMP_DATE'];

			$signup_date = "--";
			$get_move_in = "--";
			$get_accept = "--";
			$get_comp = "--";

			if($signup){
				$signup_date = date('M d, Y', strtotime($signup));
			}
			if($move_in_temp != 0){
			$get_move_in = date('M d, Y', strtotime($move_in_temp));
			} 
			if($accept_temp != 0){
				$get_accept = date('M d, Y', strtotime($accept_temp));
			}
			if($comp_temp != 0){
				$get_comp = date('M d, Y', strtotime($comp_temp));
			}
			
		} ?>
		
		<div id="mod" class="modal fade" role="dialog" style="height:100%">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        <h4 class="modal-title">Change Request Type</h4>
                    </div>
                    <div class="modal-body">
	                    <div class="row">
	                        <div class="col-md-12" style="margin:10px 0">
	                        	<div class="col-md-6" style="text-align:center">
	                        		<input id="c_valid" type="radio" name="validity" onchange="cls()">
	                        		<label class="custom_label" for="c_valid">Valid</label>
	                        	</div>
	                        	<div class="col-md-6" style="text-align:center">
	                        		<input id="c_invalid" type="radio" name="validity" onchange="cls()">
	                        		<label class="custom_label" for="c_invalid">Invalid</label>
	                        	</div>
	                        </div>
	                        <div id="invalid_pane" class="col-md-12" style="display:none;margin:10px 0">
			                    <div class="col-md-6" style="padding-left:0">
				                    <input id="compby" class="input-sm form-control" type="text" placeholder="Completed By:">
				                </div>
				                <div class="col-md-6" style="padding-right:0">
				                    <input id="compdate" class="input-sm form-control" type="date" placeholder="Completion Date MM/DD/YYYY" onblur="dvalc(this.value)">
			                    </div>
			                </div>
	                        <div class="col-md-12" style="margin:10px 0">
	                            <select id="reqtyp" class="form-control input-sm" onchange="get_req(this.value)" autocomplete="off" placeholder="Request Type">
	                                <option value="">--select request type--</option>
	                                <?php
		                                if ($reqtyp) {
		                                 	foreach ($reqtyp as $value) {
		                                 		echo "<option value='".$value['rt_id']."'>".$value['rt_desc']."</option>";
		                                 	}
		                                } 
	                                ?>
	                            </select>
	                        </div>
		                    <div class="col-md-12" style="margin:10px 0">
		                        <select id="subtyp_loader" class="form-control input-sm" onchange="house(this.value)" autocomplete="off" data-placeholder="Sub Type">
		                            <option value="">--select sub type--</option>
		                        </select>
		                    </div>
		                    <div id="housecon" class="col-md-12" style="display:none;margin:10px 0">
		                        <select id="housetyp_loader" class="form-control input-sm" autocomplete="off" data-placeholder="House Concern Type">
		                            <option value="">--select house concern type--</option>
		                        </select>
		                    </div>
	                        <div class="col-md-12" style="margin: 10px 0">
	                        	<div class="col-md-6" style="padding-left:0">
	                        		<input id="pstart" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="input-sm form-control" placeholder="Plan Start Date (mm/dd/yyyy)"> <!-- onblur="dvals(this.value)" -->
	                        	</div>
	                        	<div class="col-md-6" style="padding-right:0">
	                        		<input id="pfinish" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" class="input-sm form-control" placeholder="Plan Finish Date (mm/dd/yyyy)" onblur="dvalf(this.value)">
	                        	</div>
	                        </div>
	                        <div class="col-md-12" style="margin: 10px 0">
	                        	<div class="col-md-6" style="padding-left:0">
	                        		<select id="er" class="chosen-select" tabindex="-1" data-placeholder="Employee Responsible">
	                        			<option value=""></option>
	                        		<?php	
						            	if ($emp) {
		                                 	foreach ($emp as $e) {
		                                 		echo "<option value='".$e['KUNNR']."'>".$e['NAME1']."</option>";
		                                 	}
	                                 	} 
	                                ?>
	                        		</select>
	                        	</div>
	                        	<div class="col-md-6" style="padding-right:0">	
						            <select id="dpt" class="chosen-select" tabindex="-1" data-placeholder="Department Responsible">
						            	<option value=""></option>
						            <?php	
						            	if ($dept) {
		                                 	foreach ($dept as $d) {
		                                 		echo "<option value='".$d['d_id']."'>".$d['d_desc']."</option>";
		                                 	}
	                                 	} 
	                                ?>
						            </select>
	                        	</div>
	                        </div>
	                        <div class="col-md-12">
	                        	<textarea id="tasktxt" style="min-height:40px; max-width:100%" class="form-control" maxlength="40" placeholder="Task Text"></textarea>
	                        </div>
	                    </div>
                    </div>
                    <div class="modal-footer">
	                    <div class="col-md-12">
							<div class="col-md-3" style="float:right">
	                        	<button type="button" class="btn btn-sm btn-primary" onclick="update_reqtype()">Save changes</button>
	                    	</div>
	                    	<div id="mod_loader" class="col-md-3" style="float:right">
		                    	<div class="cssload-jumping">
									<span></span><span></span><span></span><span></span><span></span>
								</div>
							</div>
	                    </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

		<div id="concern_close" class="modal fade" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                    	<div class="row">
                    		<div id="succ" style="display:none" class="alert alert-success" role="alert">This concern is successfully closed.</div>
                    		<div id="err" style="display:none" class="alert alert-danger" role="alert">Complete the field before to submit.</div>
		                    <div class="col-md-12">Are you sure you want to close this concern?</div>
		                    <div class="col-md-12">
			                    <div class="col-md-6">
				                    <label class="custom_label" for="cby">Completed by:</label>
				                    <input id="cby" class="input-sm form-control" maxlength="8" type="text">
				                </div>
				                <div class="col-md-6">
				                    <label class="custom_label" for="cdate">Completion date:</label>
				                    <input id="cdate" class="input-sm form-control" type="date" placeholder="MM/DD/YYYY">
			                    </div>
			                </div>
		                </div>
                    </div>
                    <div class="modal-footer">
	                    <div class="col-md-12">
							<div class="col-md-3" style="float:right">
	                        	<button type="button" class="btn btn-sm btn-primary" onclick="close_request()">OK</button>
	                        	<button type="button" class="btn btn-sm btn-danger" class="close" data-dismiss="modal">Cancel</button>
	                    	</div>
	                    </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

		<div id="mod_email_forward" class="modal fade" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                        <h4 class="modal-title">Email Forward to</h4>
                    </div>
                    <div class="modal-body">
	                    <div class="row">
			            <?php 

			            $adm = $this->Buyer_Model->get_admin_by_pcode($pcode);
			            // foreach ($roles as $r) { ?>
			            <!-- // 	<div class="col-md-12">
			            // 		<input id="<?=$r['ua_rname']?>" type="checkbox" onchange="chckall('<?=$r['ua_rname']?>')">
			            // 		<label for="<?=$r['ua_rname']?>"><?=$r['ua_rname']?></label>
			            // 	</div> -->
			            <?php 
			            // 	$adm = $this->Buyer_Model->get_usr_by_roles($r['ua_id']);
			            	foreach ($adm as $usr) {
			            	if(isset($usr['ai_fname']) && isset($usr['ai_lname']) && isset($usr['a_email'])){?> 
	                    	<div class="col-md-12" style="margin-left:20px">
			                    <input id="<?=$usr['a_id']?>" class="chckmail" name="users[]" type="checkbox" value="<?=$usr['a_id']?>"> 
			                    <label for="<?=$usr['a_id']?>" ><?=$usr['ai_fname']?> <?=$usr['ai_lname']?></label>
	                    	</div>
			            	<?php } ?>
			            <?php } ?>
	                    </div>
                    </div>
                    <div class="modal-footer">
	                    <div class="col-md-12">
							<div class="col-md-3" style="float:right">
	                        	<button type="button" class="btn btn-sm btn-primary" onclick="email_forward()">Send</button>
	                    	</div>
	                    </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php if($row['KUNNR'] != ""){ ?>
			<div class="panel panel-primary">

				<div class="panel-heading">Account and Property Information</div>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Customer Name</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?php echo(isset($row['bi_fname'])? $row['bi_fname']." ".$row['bi_lname'] : $row['NAME1']); ?></span>
								</div>
							</div><div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Customer Number</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['KUNNR']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Sign Up Date</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$signup_date?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Project Location</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['ADRZUS']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Unit type</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['XMBEZ']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Project Name</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['XWETEXT']?></span>
								</div>
							</div>
						</div>
						<div class="col-md-4 nop4d">
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Blk/Lot</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['REFNO']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">SO Number</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['VBELN']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">House Model</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['ARKTX']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Floor Area</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['FLR_AREA']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Lot Area</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['LOT_SIZE']?></span>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Move In Date</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$get_move_in?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Acceptance Date</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$get_accept?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">House Completion Date</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$get_comp?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">House Contractor</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['CONTRACTOR']?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } else { ?>
			<div class="panel panel-primary">

				<div class="panel-heading">Customer Information</div>

				<div class="panel-body">
					<div class="row">
						<div class="col-md-4">
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Customer Name</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['gi_name']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Contact Number</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['gi_contact']?></span>
								</div>
							</div>
							<div class="col-md-12 nop4d">
								<div class="col-md-6 nop4d">
									<label class="custom_label">Email</label>
								</div>
								<div class="col-md-6 nop4d">
									<span><?=$row['gi_email']?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	

			<?php	}  ?>
		<?php } ?>

	<input type="hidden" value="<?=$ticketid?>" id="ticket_id">
	<input type="hidden" value="" id="con_id">
	
	<div class="panel panel-primary table-responsive" style="overflow-x: hidden">
		<table class="table panel-heading">
			<tr>
				<th style="width: 15%;">Request Type</th>
				<th style="width: 20%;">Sub Type</th>
				<th style="width: 35%;">Message</th>
				<th style="width: 10%;">Status</th>
				<th style="width: 10%;">Date Reply</th>
				<th style="width: 10%;">Reply By</th>
			</tr>
		</table>

		<div id="accordion" class="panel panel-group" style="margin:5px 0 0 0">
	<?php
		$val = 0; 
		foreach($concerns as $row){ 
			$val++;
			$color = "black";
			if(isset($row['c_plan_start'])){
				$color = "green";
			}

			switch($row['c_is_active']){ 
				case 1: 
					$status = "Open";
					break;
				case 2:
					$status = "Closed";
					$color = "red";
					break;
				case 3:
					$status = "Invalid";
					$color = "red";
					break;
				default: 
					$status = "Unrecognized";
					break;
			} 
			$rep_date = strtotime($row['c_reply_date']); ?>

		<script type="text/javascript">
			$(document).ready(function(){
				reply(<?=$row['c_id']?>);
			})
		</script>

			<div class="panel panel-default" style="margin-bottom:5px">
				<div class="panel-heading" style="cursor:pointer;padding:10px 0" onclick="reply(<?=$row['c_id']?>)" data-parent="#accordion" data-toggle="collapse" data-target="#show_thread<?=$row['c_id']?>">
					<table class="table" style="margin-bottom: 0px;border: 0px none !important;">
						<tr class="collapsed">
							<td style="min-width:15%; padding:0 5px"><?=$row['rt_desc']?></td>
							<td  style="min-width:20%; padding:0 5px">
								<div><?=$row['st_desc']?></div>
								<?php if($row['c_hsec'] != ""){ 
										switch ($row['c_stype']){
											case 2: $code = 'HSEC'; break;
											case 3: $code = 'DEVT'; break;
											case 4: $code = 'LOTC'; break;
											case 5: $code = 'SERV'; break;
										}
										$select = "KURZTEXT";
										$table = "ZHOA_TASKS";
										$where = "CODE = '".$row['c_hsec']."' AND CODEGRUPPE = '".$code. "'";
										$qwe= $this->Main->select_data_where($select, $table, $where);
										foreach ($qwe as $k) {
											$hsec_desc = $k['KURZTEXT'];
										} ?>
										<div>[ <?=$hsec_desc?> ]</div>
								<?php } ?>
							</td>
							<td style="min-width:35%; padding:0 5px"><?=$row['c_message']?></td>
							<td style="min-width:10%; color:<?=$color?>; padding:0 5px"><?=$status?></td>
							<td style="min-width:10%; padding:0 5px"><?=date('M d, Y', $rep_date)?></td>
							<td style="min-width:10%; padding:0 5px"><?=$row['c_reply_by']?></td>
						</tr>
					</table>
				</div>
				<div id="show_thread<?=$row['c_id']?>" class="panel-collapse collapse">
					<p style="text-align: center; display: none;margin:10px 0" id="loading"><img src="<?=base_url()?>assets/img/tools/sending.gif" style="width: 70px;"></p>
				    <div class="load_reply" id="load_reply<?=$row['c_id']?>" style=""></div>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>

	
<?php } ?>