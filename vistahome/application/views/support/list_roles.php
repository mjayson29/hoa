<?php 
	foreach ($new as $row) {
		$rid = $row['ua_id'];				
		$rname = $row['ua_rname'];
		$regview = $row['ua_regview'];		// 0
		$regconf = $row['ua_regconfirm'];	// 1
		$suppview = $row['ua_suppview'];	// 2
		$suppreply = $row['ua_suppreply'];	// 3
		$rptview = $row['ua_rptview'];		// 4
		$rptexpo = $row['ua_rptexport'];	// 5
		$mtc = $row['ua_mtcview'];			// 6
		$carview = $row['ua_carview'];		// 7
		$annview = $row['ua_annview'];		// 8
		$soaview = $row['ua_soaview'];		// 9
		$hoview = $row['ua_hoview'];		// 10
		$accnt = $row['ua_settings'];		// 11
		$usrview = $row['ua_usrview'];		// 12
		$usredit = $row['ua_usredit'];		// 13
		$usrdel = $row['ua_usrdel'];		// 14
		$usradd = $row['ua_usradd'];		// 15
		$roleadd = $row['ua_roleadd'];		// 16
		$roleedit = $row['ua_roleedit'];	// 17
		$roledel = $row['ua_roledel'];		// 18
		$concern = $row['ua_concern'];		// 19
		$inq = $row['ua_inq'];				// 20
		$req = $row['ua_req'];				// 21
	} 
?>					
					<div class="col-md-12" style="padding-top:20px" id="editbody">
						<div class="col-md-12">
							<div class="col-md-3">
								<label class="custom_label">Navigation</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Registration List</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="regview"   <?php if($regview == 1){ echo "checked";} ?>> 
								<label for="regview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="regconf"   <?php if($regconf == 1){ echo "checked";} ?>> 
								<label for="regconf">Confirm</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Support</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="suppview"   <?php if($suppview == 1){ echo "checked";} ?>> 
								<label for="suppview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="suppreply"   <?php if($suppreply == 1){ echo "checked";} ?>> 
								<label for="suppreply">Reply</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="suppcomm"   <?php if($suppreply == 1){ echo "checked";} ?>> 
								<label for="suppcomm">Comment</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Reports</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="rptview"   <?php if($rptview == 1){ echo "checked";} ?>> 
								<label for="rptview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="rptexpo"   <?php if($rptexpo == 1){ echo "checked";} ?>> 
								<label for="rptexpo">Export</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Maintenance</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="mtc"   <?php if($mtc == 1){ echo "checked";} ?>> 
								<label for="mtc">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Carousel</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="carview"   <?php if($carview == 1){ echo "checked";} ?>> 
								<label for="carview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Announcements</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="annview"   <?php if($annview == 1){ echo "checked";} ?>> 
								<label for="annview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; SOA</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="soaview"   <?php if($soaview == 1){ echo "checked";} ?>> 
								<label for="soaview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Homeowners List</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="hoview"   <?php if($hoview == 1){ echo "checked";} ?>> 
								<label for="hoview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Account Settings</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="accnt"   <?php if($accnt == 1){ echo "checked";} ?>> 
								<label for="accnt">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Users</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="usrview" onch   <?php if($usrview == 1){ echo "checked";} ?>>
								<label for="usrview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="usredit"   <?php if($usredit == 1){ echo "checked";} ?>> 
								<label for="usredit">Edit</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="usrdel"   <?php if($usrdel == 1){ echo "checked";} ?>> 
								<label for="usrdel">Delete</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Add Users</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="usradd"   <?php if($usradd == 1){ echo "checked";} ?>> 
								<label for="usradd">Add</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Roles</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="roleadd"   <?php if($roleadd == 1){ echo "checked";} ?>> 
								<label for="roleadd">Add</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="roleedit"   <?php if($roleedit == 1){ echo "checked";} ?>> 
								<label for="roleedit">Edit</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="roledel"   <?php if($roledel == 1){ echo "checked";} ?>> 
								<label for="roledel">Delete</label>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
					</div>
					<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Receive E-mail Alert</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-12">
							<div class="col-md-2 col-md-offset-2">
								<input type="checkbox" id="concern"   <?php if($concern == 1){ echo "checked";} ?>> 
								<label for="concern">Concern</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="req"   <?php if($req == 1){ echo "checked";} ?>> 
								<label for="req">Request</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="inq"   <?php if($inq == 1){ echo "checked";} ?>> 
								<label for="inq">Inquiry</label>
							</div>
							<div class="col-md-12"> <hr> </div>
						</div>
					</div>
