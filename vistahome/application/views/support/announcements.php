<script type="text/javascript">

	$(document).ready(function(){
		reset($('#userfile'));     
        $.ajax({
            type:"post",
            url: "<?=base_url()?>support/announce_load/",
            success: function(data){
                $('#ann_wrap').html(data);
            }
        });
		list();
	});

    function list(){
      	var co = $('#company option:selected').val();
      	var options={};
      	$('#sendto option').each(function(){
            options[$(this).val()] = $(this).text();
        });
        
        if(co!=0){
            $.ajax({
                type:"POST",
                url: "<?=base_url()?>support/listproj2/",
                data: {
                    co: co,
                    options: options
                },
                success: function(data){
                    $('#projects').html(data);
                    return false;
                }
            });
        } else {
        	var el = $('#projects');
        	var we = $('<option value="0">--SELECT PROJECT--</option>');
        	el.append(we.clone());
        }
    }

    function inc(){
    	var selectedtext;
    	var selectedval;
    	var obj = $('#projects option:selected');
    	if(obj.val()!=0){
	        obj.each(function(){
	            selectedtext = $(this).text();
	            selectedval = $(this).val();
	        });
	    	var recipient = $('#sendto');
	    	recipient.append($('#projects option:selected').clone());
	    	var dcontain = $('#sendto-display');
	    	var display = $('<li class="search-choice"><span>' + selectedtext + '</span><a class="search-choice-close" href="javascript:;" onclick="qu(this)" value=' + selectedval + '></a></li>');
	    	dcontain.append(display.clone());
	    	$('#projects option:selected').remove();
    	}
    }

    function qu(val) {
    	var q = val.getAttribute('value');
    	var recipient = $('#projects');
    	$('#sendto option[value="' + q + '"]').each(function() {
    		recipient.append($(this).clone());
		    $(this).remove();
		});
    	$(val).parent().remove();

  		return false;
    }

    function sendAll(){
    	if($('#sendtoall').is(":checked")){
    		$('#company').attr('disabled','disabled');
    		$('#projects').attr('disabled','disabled');
    	}else {
    		$('#company').removeAttr('disabled');
    		$('#projects').removeAttr('disabled');
    	}
    }

	function showup(input){
		var limit = 16;
    	var raw = input.files;
    	if (input.files && input.files[0]) {
    		if(raw[0].size > 2097152){ //approx 2MB
    			bootbox.alert({
    				size: 'small',
    				message: 'The File you are trying to attach is too large'
    			});
    			return false;
    		}
            var reader = new FileReader();
            var f = input.files[0];
            var fname = escape(f.name);
            var ext = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);
            if(ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'JPG' && ext != 'PNG' && ext != 'JPEG'){
            	bootbox.alert({
    				size: 'small',
    				message: 'Invalid file type'
    			});
    			reset($('#userfile'));
    			return false;
            }
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            if(fname.length > limit){
					iname = fname.substr(0, limit) + "...";
			}else{
				iname =fname;
			}
			document.getElementById("uplabel").textContent=iname;
			$('#uplabel').attr('title',fname);
            reader.readAsDataURL(input.files[0]);
            $('#upcon').slideDown('fast');
        }
    }

    window.reset = function (e) {
	    e.wrap('<form>').closest('form').get(0).reset();
	    e.unwrap();
	    $('#uplabel').text('');
	    $('#upcon').slideUp('fast');
	}

	$(document).ready(function(){
		$('.uprev_box').hover(function(){
			$('.prevdet').css('display','block');
			$('.uprem').hover(function(){
				$('.uprem').css('color','black');
			}, function(){
				$('.uprem').css('color','#a3a3a3');
			});
		}, function(){
			$('.prevdet').css('display','none');
		});
		
	});

</script>	

<?php $acc = explode("-", $access); ?>

	<div class="row">
	<input type="hidden" value="" id="upd_id">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading" style="padding: 5px 15px;">
					<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-wrench"></span> Maintenance</div> 
						<div class="btn-group col-md-8 pull-right">
						<?php if($acc[22]!=0){ ?>
							<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
								<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/soa'"><i class="icon-profile"></i>&nbsp SOA</button>
							</div>
						<?php } ?>
						<?php if($acc[21]!=0){ ?>
							<div class="col-md-3" style="float:right;padding-left:5px;padding-right:0px">
								<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/announcements'"><i class="glyphicon glyphicon-blackboard"></i>&nbsp Announcements</button>
							</div>
						<?php } ?>
						<?php if($acc[20]!=0){ ?>
							<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
								<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/carousel'"><i class="glyphicon glyphicon-film"></i>&nbsp Carousel</button>
							</div>
						<?php } ?>
						</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="post_notif" class="panel panel-success" style="display:none">
				<div class="panel-heading" style="padding: 5px 15px;">
					<div class="col-md-12" style="padding:5px 0px;text-align:center">
						New Announcement Successfully Posted!
					</div> 
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="update_notif" class="panel panel-success" style="display:none">
				<div class="panel-heading" style="padding: 5px 15px;">
					<div class="col-md-12" style="padding:5px 0px;text-align:center">
						Announcement Successfully Updated!
					</div> 
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="cancel_notif" class="panel panel-success" style="display:none">
				<div class="panel-heading" style="padding: 5px 15px;">
					<div class="col-md-12" style="padding:5px 0px;text-align:center">
						Announcement Successfully Cancelled!
					</div> 
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="new_post_announce" class="panel panel-primary" style="display:none">
				<div class="panel-heading" style="padding: 5px 15px;">
					<div class="col-md-2" style="padding:5px 0px;">
						New Announcement
					</div> 
					<div class="clearfix"></div>
				</div>

				<div class="panel-body">
					<div class="row">
						<div id="sendingpanel" class="col-md-12">
							<div class="col-md-12">
							<div class="row">
								<label for="sendTo">Send To</label>
								<span style="float:right">
									<input type="checkbox" id="sendtoall" autocomplete="off" onchange="sendAll()"> 
									<label for="sendtoall">Send to All</label>
								</span>
							<div class="col-md-12">
								<select id="sendto" style="display:none"></select>
									<div id="sendto-container" class="chosen-container chosen-container-multi" style="width:100%;min-height:32px;">
										<ul id="sendto-display" class="chosen-choices" style="min-height:30px;">
											
										</ul>
									</div>
									</div>
								</div>
							</div>
								<div class="col-md-6">
									<label >
									Company
									</label><select id="company" class="form-control input-sm" autocomplete="off" onchange="list()">
										<option value="0">--SELECT COMPANY--</option>
										<?php
											foreach ($company as $key) {
												echo "<option value='".$key['BUKRS']."'>".$key['BUTXT']."</option>";
											}
										?>
									</select>
								</div>
								<div class="col-md-6">
									<label >
									Projects
									</label><select id="projects" class="form-control input-sm" autocomplete="off" onchange="inc()">
									</select>
								</div>
						</div>
						<div class="col-md-12"><hr/></div>
						<div class="col-md-12">
							<div class="col-md-4" style="padding-left:0px">
								<label for="ann_id">Subject</label>
								<input id="ann_id" type="text" class="input-sm form-control" placeholder="Subject" autocomplete="off">
							</div>
							<?php $array=array('id'=>'uploadform');echo form_open_multipart(); ?>
							<div class="col-md-1" style="padding-top:4px">
								<br /> 
								<label for="userfile" class="btn btn-sm input-sm btn-primary form-control">
								   	<span class="icon-clip"></span>
						        </label>
					            <?php echo form_upload(array('name'=>'userfile','accept'=>'image/jpg,image/jpeg,image/png','id'=>'userfile','style'=>'display:none;','onchange'=>'showup(this);')); ?>
							</div>
							<?php echo form_close(); ?> 
							<div id="ann_btn_upd" class="col-md-3" style="float:right;display:none">
								<div class="col-md-6" style="padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="upd_announce()"><i class="icon-check"></i>&nbsp UPDATE</button>
								</div>
								<div class="col-md-6" style="padding-right:0px">
									<button class="btn btn-xs input-sm btn-danger form-control" onclick="cancel_new_announce()"><i class="icon-cancel-circle"></i>&nbsp CANCEL</button>
								</div>
							</div>
							<div id="ann_btn_post" class="col-md-3" style="float:right;display:none">
								<div class="col-md-6" style="padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="post_announce()"><i class="icon-check"></i>&nbsp POST</button>
								</div>
								<div class="col-md-6" style="padding-right:0px">
									<button class="btn btn-xs input-sm btn-danger form-control" onclick="cancel_new_announce()"><i class="icon-cancel-circle"></i>&nbsp CANCEL</button>
								</div>
							</div>
							<div id="send_loader" class="col-md-2" style="float:right; padding: 5px 0px; display:none" >
								<span style="float:right">
									<span>
										<img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;">
									</span>
									 <i>Loading...</i>
								 </span>
							</div>
						</div>
						<div id="upcon" class="col-md-12" style="display:none">
							<br/>
							<label>
								Attached
							</label>
							<hr/ style="margin:3px 0 10px">
							<div class="uprev_box">
								<div class="uprev">
									<img id="blah" src="#">
								</div>
								<div class="prevdet">
									<span class="upthumb icon-image"> </span> 
									<span id="uplabel" class="uplabel"></span>  
									<div class="glyphicon glyphicon-remove uprem" title="remove image" onclick="reset($('#userfile'))"> </div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<br/>
							<textarea id="thisarea2" name="thisarea2"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading" style="padding: 5px 15px;">
					<div class="col-md-2" style="padding:5px 0px;">
						Active Announcements
					</div> 
					<div class="btn-group col-md-8 pull-right">
						<div class="col-md-1" style="float:right;padding-left:5px;padding-right:0px" onclick="slideSearch_2()">
							<button class="btn btn-xs input-sm btn-success form-control"><i class="glyphicon glyphicon-search"></i></button>
						</div>
						<div class="col-md-4" id="searchAnnounce" style="float:right;display:none;padding-left:5px;padding-right:0px">
							<input type="texbox" id="announceSearch" autocomplete="off" onblur="announceSearchTxtBoxEvent()" onkeypress="ahandler(event)" class="input-sm form-control" placeholder="Search">
						</div>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="new_announce()"><i class="icon-file"></i>&nbsp New</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="panel-body">
					<div id="ann_wrap" class="row">
					</div>
				</div>
			</div>	
		</div>		
	</div>
</div>

