
<?php
$acc = explode("-", $access);
?>
		<div class="row">	
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading" style="padding: 5px 15px;">
						<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-wrench"></span> Maintenance</div> 
							<div class="btn-group col-md-8 pull-right">
							<?php if($acc[22]!=0){ ?>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/soa'"><i class="icon-profile"></i>&nbsp SOA</button>
								</div>
							<?php } ?>
							<?php if($acc[21]!=0){ ?>
								<div class="col-md-3" style="float:right;padding-left:5px;padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/announcements'"><i class="glyphicon glyphicon-blackboard"></i>&nbsp Announcements</button>
								</div>
							<?php } ?>
							<?php if($acc[20]!=0){ ?>
								<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
									<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/carousel'"><i class="glyphicon glyphicon-film"></i>&nbsp Carousel</button>
								</div>
							<?php } ?>
							</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div id="users">
					<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
				</div>
			</div>
		</div>
	</div>
	