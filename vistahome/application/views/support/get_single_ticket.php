<script type="text/javascript">

	function close_request(val){
		var con_id = $('#con_id').val();
		var cby = $('#cby').val();
		var cdate = $('#cdate').val();
		var q = new Date();
		var m = q.getMonth();
		var d = q.getDate();
		var y = q.getFullYear();

		var today = new Date(y,m,d);
		mydate=new Date(cdate);
		if($('#succ').css('display') == "block"){
			return false;
		}
		if(cby != "" && cdate != ""){
			if(date_validator(cdate) == 1){
	            return false;
	        }
	        if(today<mydate) {
			    bootbox.alert({
			    	size: "small",
			    	message: "Please enter a date not greater than today."
			    })
			    return false;
			}
			$.ajax({
				type: "POST",
				url: '<?=base_url()?>support/close_ticket/'+con_id,
				data: {
					con_id : con_id,
					cby : cby,
					cdate : cdate
				},
				success: function(data){
					$('#err').css('display','none');
					$('#succ').css('display','block');
					var cont = "<div class='col-md-12'> <hr> </div>" +
								"<div class='col-xs-10 col-sm-10 col-md-10'> Concern closed. You cannot reply to this thread. </div>";
					$('#load_reply'+con_id+' #replyControl').html(cont);
					$('#concern_close .modal-footer').remove();
					$('#cby').prop("readonly", true);
					$('#cdate').prop("readonly", true);
				}
			})
		}else{
			$('#err').css('display','block');
			$('#succ').css('display','none');	
		}
	}
	
		function email_forward() {
			var con_id = $('#con_id').val();
			var usrs = [];
			$(".chckmail:checked").each ( function() {
			    usrs.push($(this).val());
			});
			if(usrs.length !== 0){
				$.ajax({
					type:"POST",
		            url: "<?=base_url()?>support/email_forward/",
		            data: { usrs: usrs,
		            		con_id: con_id
		            	  },
		            success: function(data){
		            	reply(con_id);
		            	$('#mod_email_forward').modal('toggle');
		            }
				})
			}
		}


</script>

<div class="panel-body">

<?php if($concern_details){ ?>
	
	<div style="max-height: 300px; overflow: auto; overflow-x: hidden; border: 0px solid #ddd; width: 104%;padding-right:40px">
	
	<script type="text/javascript">
		
		window.reset<?php echo($get_id); ?> = function (e) {
		    e.wrap('<form>').closest('form').get(0).reset();
		    e.unwrap();
		    $('#ftype').removeClass('icon-file-picture');
		    $('#ftype').removeClass('icon-file-text2');
		    $('#atdiv<?=$get_id?>').css('display','none');
		}

		function at<?php echo($get_id); ?>(v){
			console.log(<?php echo($get_id); ?>);
			var limit = 16;
	    	var raw = v.files;
	    	var ft;
	    	var exts = ["JPG", "JPEG", "TXT", "PNG", "PDF", "DOC", "DOCX", "XLS", "XLSX"];
	    	if (v.files && v.files[0]) {
	    		if(raw[0].size > 2097152){ //approx 2MB
	    			bootbox.alert({
	    				size: 'small',
	    				message: 'The File you are trying to attach is too large'
	    			});
	    			return false;
	    		}
	            var reader = new FileReader();
	            var f = v.files[0];
	            var fname = escape(f.name);
	            var ext = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);
	            var ext2 = ext.toUpperCase();
	            if(exts.indexOf(ext2) < 0){
	            	bootbox.alert({
	    				size: 'small',
	    				message: 'Invalid file type'
	    			});
	    			reset($('#userfile'));
	    			return false;
	            }
	            	if(ext == 'txt' || ext == 'pdf' || ext == 'doc' || ext == 'xls' || ext == 'TXT' || ext == 'PDF' || ext == 'DOC' || ext == 'XLS'){
	            		ft = "icon-file-text2";
	            	} else {
	            		ft = "icon-file-picture";
	            	}
	            
	            if(fname.length > limit){
					iname = fname.substr(0, limit) + "...";
				}else{
					iname =fname;
				}
				$('#att').text(iname);
				$('#att').attr('title',fname);
				$('#atdiv<?=$get_id?>').css('display','block');
				$('#ftype').addClass(ft);
				$('#fsize').text(Math.floor(raw[0].size/1024) + " KB");
	            reader.readAsDataURL(v.files[0]);
	        }
	    }

	</script>

		<?php foreach($concern_details as $cd){
				if($cd['cd_concern_id'] == $get_id){
					$date_created = strtotime($cd['cd_date_created']);
					$today = date('Y-m-d H:i:s');
					$start_date = new DateTime(date('Y-m-d H:i:s', $date_created));
					$since_start = $start_date->diff(new DateTime($today));

					if($since_start->h <= 0){
						if($since_start->i <= 0){
							if($since_start->s <= 1){
								$seconds = $since_start->s;
								$get_time = $seconds." sec ago";
							} else {
								$seconds = $since_start->s;
								$get_time = $seconds." secs ago";
							}
						} else {
							if($since_start->i == 1){
								$minutes = $since_start->i;
								$get_time = $minutes." minute ago";
							} else {
								$minutes = $since_start->i;
								$get_time = $minutes." minutes ago";
							}
						}
					}
					if($since_start->h >= 1 AND $since_start->h < 24){
						if($since_start->h == 1){
							$hours = $since_start->h;
							$get_time = $hours." hour ago";
						} else {		
							$hours = $since_start->h;
							$get_time = $hours." hours ago";
						}
					}
					if($since_start->d >= 1 AND $since_start->d <= 6){
						if($since_start->d == 1){
							$days = $since_start->d;
							$get_time = $days." day ago";
						} else {
							$days = $since_start->d;
							$get_time = $days." days ago";
						}
					}
					if($since_start->d > 6){
						$strdate = date('M d, Y', $date_created);
						$strtime = date('h:i A', $date_created);
						$get_time = $strdate. " at " .$strtime;
					}
		?>

		<?php if($cd['cd_persona']==9) {?>
		<div class="col-md-12" style="background: #b3ffd9; color:black; text-shadow: none; border-radius: 5px;margin-bottom:15px;padding:10px 20px">
			
			<div class='col-xs-12' style='word-wrap: break-word; padding: 3px 0'>
				<?=$cd['cd_desc']?>
			</div>
			<div class='col-xs-12' style='word-wrap: break-word;' align="right">
				<small><span class="anc"><?=$cd['cd_reply_by']?></span> &nbsp; <span style="color:grey">&#8226;</span> &nbsp; <small style='color: #aaa;'><?=$get_time?></small></small>
			</div>
		</div>
		<?php } else { ?>

		<div class="col-md-11 <?php echo($cd['cd_persona']==0 ? 'col-md-offset-1' : '');?>" style="background:<?php echo($cd['cd_persona']==0 ? "#6699ff;" : "#f2f2f2;");?>; color:black; text-shadow: none; border-radius: 5px;margin-bottom:15px">
			<div class='col-xs-12' style='word-wrap: break-word; padding: 3px 0'>
				<span style="color:<?php echo($cd['cd_persona']==0 ? "#b3d8ff;" : "#337ab7;");?>"><?=$cd['cd_reply_by']?></span>
			</div>
			<div class='col-xs-12' style='word-wrap: break-word;<?php echo($cd['cd_persona']==0 ? "color:#ffffff;" : "color:#000000;");?>'>
				<?=$cd['cd_desc']?>
			</div>
			<?php $select = ""; $table = "ZHOA_concern_details"; $where = "cd_id = ".$cd['cd_id']."" ?>
			<?php $attach = $this -> Main -> select_data_where($select, $table, $where);?>
			<?php foreach($attach as $a){ ?>

				<?php 
				if($cd['cd_filename']){
					$fileID = $cd['cd_id'];
					$full_ftype = trim($cd['cd_filename']);
					$file_type = substr($cd['cd_filename'], -3);
					$file_type_1 = substr($cd['cd_filename'], -3);

					if($full_ftype != "Attachment is not valid" && file_exists(APPPATH."../assets/concerns/".$full_ftype)){  // for devsite
					// if($full_ftype != "Attachment is not valid" && file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/concerns/".$full_ftype)){   // for local use only

						if($file_type == "gif" || $file_type == "jpg" || $file_type == "png" || $file_type_1 == "jpeg" || $file_type == "GIF" || $file_type == "JPG" || $file_type == "PNG" || $file_type_1 == "JPEG"){ ?>
							<div class="col-xs-9 col-sm-11 col-md-12">
								<div class="col-md-5">
									<img src="<?=base_url()?>assets/concerns/<?=$cd['cd_filename']?>" style="height:100px; width:100px;" class="img-responsive">
								</div>
							</div>
						<?php } ?>
						<div class="col-xs-9">
							<a href="<?=base_url()?>support/download_file/<?=$fileID?>"><span class="glyphicon glyphicon-paperclip"></span> <?=$cd['cd_filename']?> </a>
						</div>
					<?php } else { ?>
					<div class="col-xs-9" title="File Corrupted" style="color: #d43f3a;text-decoration: line-through;">
						<span class="glyphicon glyphicon-paperclip"></span> <?=$full_ftype?>
					</div>	
					<?php } ?>
				<?php } ?>

			<?php }?>

			<div class='col-xs-12' style='word-wrap: break-word;'>
				<small style='color:<?php echo($cd['cd_persona']==0 ? "#e1e1ea;" : "#aaa;");?>'><?=$get_time?></small>
			</div>
		</div>
			<?php } ?>
		<?php } ?>
		<?php } ?>
	</div>

<?php } ?>

	<?php 
	
	$acc = explode("-", $access);

	if($acc[3]!=0) {

	$array=array('id'=>'uploadform');

	echo form_open_multipart();
		
	?>
	<div id="replyControl" class="col-md-12">
		<div class="col-md-12"><hr/></div>
		<div id="atdiv<?=$get_id?>" class="col-md-12" style="display:none">
			<div class="col-md-3" style="background-color:#d1d1e1">
				<div class="col-md-1" style="padding: 5px 0">
					<span id="ftype" style="padding: 5px 0; font-size:35px"> </span>
				</div>
				<div class="col-md-8">
					<div class="col-md-12">
						<span id="att"> </span>
					</div>
					<div class="col-md-12">
						<span id="fsize"></span>
					</div>
				</div>
				<div class="col-md-1" style="float:right">
					<span class="glyphicon glyphicon-remove" style="cursor:pointer" title="remove attached file" onclick="reset<?=$get_id?>($('#userfile'))"></span>
				</div>
			</div> 
		</div>
		<?php foreach ($get_status as $e) {
			$status = $e['c_is_active'];
			$cby = $e['c_closed'];
			$cdate = $e['c_completedby'];
			$pstart = $e['c_plan_finish'];
			$pfinish = $e['c_plan_start']; 
		?>
		<input type="hidden" id="validity<?=$get_id?>" value="<?=$e['c_is_active']?>">
		<input type="hidden" id="req<?=$get_id?>" value="<?=$e['c_rtype']?>">
		<input type="hidden" id="sub<?=$get_id?>" value="<?=$e['c_stype']?>">
		<input type="hidden" id="hsec<?=$get_id?>" value="<?=$e['c_hsec']?>">
		<input type="hidden" id="pstart<?=$get_id?>" value="<?=$e['c_plan_start']?>">
		<input type="hidden" id="pfinish<?=$get_id?>" value="<?=$e['c_plan_finish']?>">
		<input type="hidden" id="emp<?=$get_id?>" value="<?=$e['c_employee']?>">
		<input type="hidden" id="dept<?=$get_id?>" value="<?=$e['c_department']?>">
		<input type="hidden" id="task<?=$get_id?>" value="<?=$e['c_task_text']?>">
		<?php } ?>
		<?php if($status == 2){ ?>
			<div class='col-xs-2 col-sm-2 col-md-2'> </div>
			<div class='col-xs-10 col-sm-10 col-md-10'>
				Concern closed. You cannot reply to this thread.
			</div>
			<?php if(!isset($cby) || !isset($cdate)) { ?>
			<div class="col-md-12 nop4d">
				<?php if($acc[19] != 0) { ?> 	
			 	<div class="col-xs-2 col-sm-2 col-md-1" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-default btn-sm form-control input-sm" onclick="send_comment(<?=$get_id?>)" title="Comment"> <span class="icon-bubbles"></span></button>
			 	</div>
				<?php } ?>
			 	<div class="col-xs-2 col-sm-2 col-md-1" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-default btn-sm form-control input-sm" data-toggle="modal" data-target="#mod" title="Edit Request Type" onclick="reqedit(<?=$get_id?>)"> 
						<span class="icon-pencil"></span>
					</button>
			 	</div>
			 	<?php if(isset($pstart) && isset($pfinish)) { ?>
			 	<div class="col-xs-4 col-sm-4 col-md-2" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-danger btn-sm form-control input-sm" data-toggle="modal" data-target="#concern_close" title="Close Request"  onclick="cler(<?=$get_id?>)"> Close Request</button> <!--  onclick='close_request(<?=$get_id?>)' -->
			 	</div>
				<?php } ?>
			 </div>
			<?php } ?>
		<?php } 
		if($status == 3) { ?>
			<div class='col-xs-2 col-sm-2 col-md-2'> </div>
			<div class='col-xs-10 col-sm-10 col-md-10'>
				INVALID Concern. You cannot reply to this thread.
			</div>
		<?php }
		if($status == 1) { ?>
			<div class="col-xs-10 col-sm-10 col-md-12">
				<small>Send Reply</small>
				<br>
				<textarea class="form-control input-sm" style="resize: vertical; min-height: 50px;" id='concern_msg<?=$get_id?>'></textarea>
				<!-- <input type="text" name="" class="form-control input-sm"> -->
			</div>
			<div id="msgButtons" class="col-md-12 nop4d">

			<?php if(!isset($source)) {?>
			   	<div class="col-xs-2 col-sm-2 col-md-2">
					<br>
					<button type="button" class="btn btn-primary btn-sm form-control input-sm" onclick='send_reply(<?=$get_id?>)' title="Send Reply">SEND</button>
			 	</div>
			<?php } ?>
			<?php if($acc[19] != 0) { ?> 	
			 	<div class="col-xs-2 col-sm-2 col-md-1" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-default btn-sm form-control input-sm" onclick="send_comment(<?=$get_id?>)" title="Comment"> <span class="icon-bubbles"></span></button>
			 	</div>
			<?php } ?>	
			   	<div class="col-xs-2 col-sm-2 col-md-1" style="padding-right:0;">
					<br>
			        <label for="userfile" class="btn btn-default input-sm btn-sm form-control" title="Attach File">
					   	<span class="glyphicon glyphicon-paperclip"></span>
			        </label>
		            <?php echo form_upload(array('name'=>'userfile','id'=>'userfile','style'=>'display:none;','onchange'=>"at$get_id(this)")); ?>
		            <!-- <input type="hidden" id="get_id" value="<?=$row['bi_id']?>"> -->
			 	</div>
			 	<div class="col-xs-2 col-sm-2 col-md-1" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-default btn-sm form-control input-sm" data-toggle="modal" data-target="#mod" title="Edit Request Type" onclick="reqedit(<?=$get_id?>)"> 
						<span class="icon-pencil"></span>
					</button>
			 	</div>
			 	<div class="col-xs-2 col-sm-2 col-md-1" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-default btn-sm form-control input-sm" data-toggle="modal" data-target="#mod_email_forward" title="Email Forward" onclick="cler(<?=$get_id?>)"> 
						<span class="icon-forward"></span>
					</button>
			 	</div>
			 	<?php if(isset($pstart) && isset($pfinish)) { ?>
			 	<div id="closeCont" class="col-xs-4 col-sm-4 col-md-2" style="padding-right:0;">
					<br>
					<button type="button" class="btn btn-danger btn-sm form-control input-sm"  data-toggle="modal" data-target="#concern_close" title="Close Request"  onclick="cler(<?=$get_id?>)"> Close Request</button>  <!-- onclick='close_request(<?=$get_id?>)' -->
			 	</div>
			 	<?php } ?>
		 	</div>
		<?php } ?>		 		
	</div>
<?php 
	echo form_close(); 
	}
?> 
</div>

