				<div class="panel panel-primary" >
					<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;">Add Business Group</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">
						<div class="col-md-12" style="padding-top:20px">
							<div class="col-md-12">
								<div class="col-md-6">
									<label >Business Entity Assignment</label><br/>
								</div>
								<div class="col-md-6">
									<div class="col-md-4 col-md-offset-4">
										<button class="btn btn-sm btn-success form-control" onclick="saveAddGrp()">Add to List</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-sm btn-danger form-control" onclick="cancelAddGroup()">Cancel</button>
									</div>
								</div>
							</div>
							<div class="col-md-12" style="margin-top:25px">
								<div class="col-md-3">
									<select class="form-control input-sm" required autocomplete="off" onchange="list_BG(this.value)" id="selectedBusinessGroup">
									<option value="0" autocomplete="off">-- Select Business Group --</option>
									<?php foreach($company as $row){ ?>
										<?php $key = strtoupper($row['rb_name']); ?>
										<option value="<?=$row['rb_grp_id']?>"> <?=$key?></option>
									<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-12"> <hr> </div>
							<div class="col-md-3 col-md-offset-2 container-fluid">
								<span style="text-align:center">
	                                <label class="custom_label" style="width:100%" >Assigned Business Entity</label>
	                            </span>
								<div class="selector-box">
									<div class="row-fluid" style="height:100%" id="loadselected">
										<select multiple id='resultbussEnt' autocomplete="off" class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
											<p style="text-align: center; display: none;" id="load_sourcebussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="selection-box">
									<div class="col-md-6 col-md-offset-3" style="top:25%">
										<button class="btn btn-sm btn-default form-control"  onclick="removed()" style="padding:5px; margin-top:3px">></button>
										<button class="btn btn-sm btn-default form-control"  onclick="add()" style="padding:5px; margin-top:3px"><</button>
										<button class="btn btn-sm btn-default form-control"  onclick="removeall()" style="padding:5px; margin-top:3px">>></button>
										<button class="btn btn-sm btn-default form-control"  onclick="addall()" style="padding:5px; margin-top:3px"><<</button>
									</div>
								</div>
							</div>
							<div class="col-md-3 container-fluid">
							<span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Unassigned Business Entity</label>
                            </span>
								<div class="selector-box">
									<select class="form-control input-sm" autocomplete="off" disabled onchange="listproj2()" id="selectedcompany">
										<option value="0" autocomplete="off">-- Select Company --</option>
										<?php if($co) { ?>
											<?php foreach($co as $ro){ ?>
												<option value="<?=$ro['BUKRS']?>"><?=$ro['BUTXT']?></option>
											<?php } ?>
										<?php } ?>
									</select>
									<div class="row-fluid" style="height:90%; padding-top:10px" id="loadedproj">
										<select multiple id='sourcebussEnt' autocomplete="off" class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
											<p style="text-align: center; display: none;" id="load_sourcebussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12"> <hr> </div>
						</div>
					</div>
				</div>