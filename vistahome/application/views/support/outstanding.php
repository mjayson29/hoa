 <script type="text/javascript">

    function paginate(val){
        $('#loader').hide();
        $('#loading').show();

        $.ajax({
        type:"post",
        url: "<?=base_url()?>support/get_outstanding/"+val,
        success: function(data){
            $('#loader').html(data);
            $('#loader').slideDown('fast');
            $('#loading').hide();
        }
    });

    }

</script>

<script type="text/javascript">
	$(document).ready(function() {
		var showChar = 20;
		var ellipsestext = "...";
		$('#messagecont').each(function() {
			var content = $(this).html();

			if(content.length > showChar) {

				var c = content.substr(0, showChar);

				var html = c+'<span class="moreelipses">'+ellipsestext+'</span>';
				$(this).html(html);
			}
		});
	});
</script>

<div class="panel panel-yellow">

	<div class="panel-heading">Outstanding</div>

	<?php if(!$outstanding){ ?>

	<div class="panel-body">

		<span>NO DATA</span>

	</div>

	<?php } else { ?>

	<div class="table-responsive">

		<table class="table">

			<tr>
				<th style="width: 12%;">SignUp Date</th>
				<th style="width: 12%;">Project Area</th>
				<th style="width: 10%;">Project</th>
				<th style="width: 10%;">Blk/Lot</th>
				<th style="width: 10%;">Unit</th>
				<th style="width: 10%;">Concern</th>
				<th style="width: 10%;">Status</th>
				<th style="width: 14%;">Processed Date</th>
				<th style="width: 12%;">Processed By</th>

			</tr>

			<?php foreach($outstanding as $row){ ?>
				<?php $signup = strtotime($row['bi_regdate']);
				$select=""; $table="ZHOA_unit"; $where="u_id = ".$row['bi_unit'];
				$get_unit = $this -> Main -> select_data_where($select, $table, $where);
					foreach($get_unit as $u){
						$unit = $u['u_desc'];
					}

				/*$select=""; $table="ZHOA_concern_temp"; $where="ct_owner_id = ".$row['bi_owner_id'];
				$get_concern = $this -> Main -> select_data_where($select, $table, $where);
					$concern = "No Concern";
					foreach($get_concern as $c){
						$concern = $c['ct_desc'];
					}*/

				$concern = "No Concern";
				if($row['bi_temp_concern'] != ""){
					$concern = $row['bi_temp_concern'];
				}
				
				date_default_timezone_set("Asia/Manila");
				$get_today = date("Y-m-d");
				// $today = new datetime($get_today);

				$get_reg_date = strtotime($row['bi_regdate']);
				$reg_date = date('Y-m-d');

			   	$waiting = 0;
				$status = "Undefined";

				switch ($row['bi_status']){
					case 0:
				   		if($reg_date == $get_today && $row['bi_email_verif'] == 1){
			   				$waiting = 0;
			  				$status = "New";
			   			}	
			   			if($reg_date < $get_today && $row['bi_email_verif'] == 1){
				   			$waiting = 0;
				   			$status = "Outstanding";
				   		}
				   		// if(substr($row['bi_regdate'],0,10) < $today && $row['bi_email_verif'] == 0){
				   		if($row['bi_email_verif'] == 0){
				   			$waiting = 1;
				   			$status = "Waiting for email verification";
				   		}
				   		break;
				   	case 1:
				   		$waiting = 0;
				   		$status = "Confirmed";
				   		break;
				   	case 2:
				   		$waiting = 0;
				   		$status = "Rejected";
				   		break;
				}
								    	
			?>

				<tr class="data_click" onclick="window.location.href='<?=base_url()?>support/registration/<?=$row['bi_id']?>'">
					<td style="width: 10%;"><?=date('M d, Y' ,$signup);?></td>
					<td style="width: 10%;"><?=$row['PROJ_AREA']?></td>
					<td style="width: 10%;"><?=$row['bi_project']?></td>
					<td style="width: 10%;"><?=$row['bi_blklot']?></td>
					<td style="width: 10%;"><?=$unit?></td>
					<td id="messagecont" style="width: 20%;"><?=$concern?></td>
					<td style="width: 10%;"><?=$status?></td>
					<td style="width: 10%;">Not Yet Processed.</td>
					<td style="width: 10%;">Not Yet Processed.</td>
				</tr>

			<?php } ?>

		</table>

	</div>

	<?php } ?>

</div>

<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$outstanding_links?></div>