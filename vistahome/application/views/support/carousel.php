<script type="text/javascript">
	function showup(input){
		var limit = 16;
    	var raw = input.files;
    	if (input.files && input.files[0]) {
    		if(raw[0].size > 2097152){ //approx 2MB
    			bootbox.alert({
    				size: 'small',
    				message: 'The File you are trying to attach is too large'
    			});
    			return false;
    		}
            var reader = new FileReader();
            var f = input.files[0];
            var fname = escape(f.name);
            var ext = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);
            if(ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'JPG' && ext != 'PNG' && ext != 'JPEG'){
            	bootbox.alert({
    				size: 'small',
    				message: 'Invalid file type'
    			});
    			reset($('#userfile'));
    			return false;
            }
            reader.onload = function (e) {
                $('#carr1').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?php $acc = explode("-", $access); ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-wrench"></span> Maintenance</div> 
					<div class="btn-group col-md-8 pull-right">
					<?php if($acc[22]!=0){ ?>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/soa'"><i class="icon-profile"></i>&nbsp SOA</button>
						</div>
					<?php } ?>
					<?php if($acc[21]!=0){ ?>
						<div class="col-md-3" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/announcements'"><i class="glyphicon glyphicon-blackboard"></i>&nbsp Announcements</button>
						</div>
					<?php } ?>
					<?php if($acc[20]!=0){ ?>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/carousel'"><i class="glyphicon glyphicon-film"></i>&nbsp Carousel</button>
						</div>
					<?php } ?>
					</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<?php if(!$carousel){ ?>
		<div class="col-md-12">
			<h3>NO DATA</h3>
		</div>
	<?php } else { ?>
		<?php foreach($carousel as $row){ ?>
			<?php 
				// if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/banner/".$row['c_filename'])){  // for local use only
				if(file_exists(APPPATH."../assets/img/banner/".$row['c_filename'])) {  //for devsite
			?>
			<div id="carl<?=$row['c_id']?>" class="col-md-4">
				<span class="thumbnail">
					<img id="carr<?=$row['c_id']?>" src="<?=base_url()?>assets/img/banner/<?=$row['c_filename']?>" style="width: 100%; height: 250px; object-fit: cover; object-position: center;" alt="<?=$row['c_brandname']?>">
			   	</span>
			   	<small>Input Brand Name</small>
			   	<input type="text" class="form-control input-sm" placeholder="" value="<?=$row['c_brandname']?>">
				<input type="checkbox" <?php if($row['c_show'] == 1){ echo "checked value='1'"; } else { echo "value='0'"; } ?> name="show_carousel"> Show Carousel
				<label for="userfile">
			   		<span class="glyphicon glyphicon-camera carousel_cam" title="Change Banner"></span>
			   	</label>
			   	<input type="file" style="display: none;" id="userfile" name="userfile" onchange="showup(this)">
			   	<input type="hidden" id="carid" value="<?=$row['c_id']?>">
			</div>
			<?php } ?>
		<?php } ?>
		<div class="col-md-4">
			<form method="POST" action="<?=base_url()?>support/carousel" enctype="multipart/form-data">
				<span class="thumbnail carousel_thumbnail">
					<label for="files" id="load_upload" >
						<span class="glyphicon glyphicon-plus-sign carousel_add" style="margin-bottom: 100px; margin-top: 100px; font-size: 50px;" id="uploader"></span>
				   	</label>
				</span>
				<small>Input Brand Name</small>
				<input type="text" class="form-control input-sm" placeholder="" name="brandname">
				<input type="checkbox" name="show_carousel"> Show Carousel
			   	<input type="file" style="display: none;" id="files" name="files[]">
			   	<br>
			   	<button type="submit" class="btn btn-primary btn-sm" name="">Add</button>
		   	</form>
		</div>	
	<?php } ?>
</div> 

</div>
