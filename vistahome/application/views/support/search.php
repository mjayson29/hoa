 <script type="text/javascript">

    function paginate(val){

        $('#loader').hide();
        $('#loading').show();

        var signup_from = $('#signup_from').val();
        var signup_to = $('#signup_to').val();
        var status = $('#status').val();
        var company = $('#company').val();
		var project = $('#project').val(); 
		var keyword = $('#keyword').val(); 
		var process_from = $('#process_from').val();
		var process_to = $('#process_to').val();
		var process_by = $('#process_by').val();

        $.ajax({
            type:"POST",
            url: "<?=base_url()?>support/search/"+val,
            data: {
                signup_from: signup_from, 
                signup_to: signup_to,
                status: status,
                process_from: process_from,
                process_to: process_to,
                process_by: process_by,
                company: company,
                project: project,
                keyword: keyword
            },
            success: function(data){
                $('#loader').html(data);
                $('#loader').slideDown('slow');
                $('#loading').css('display', 'none');
            }
        });

    }

</script>

<div class="panel panel-primary">

	<div class="panel-heading">Search Results</div>

	<?php if(!$search){ ?>

		<div class="panel-body">

			<span>NO DATA</span>

		</div>

	<?php } else { ?>

		<div class="table-responsive">

			<table class="table">

				<tr>
					<th>SignUp Date</th>
					<th>Company</th>
					<th>Project</th>
					<th>Blk/Lot</th>
					<th>Unit</th>
					<th>Concern</th>
					<th>Status</th>
					<th>Process Date</th>
					<th>Process By</th>
				</tr>

				<?php foreach($search as $row){ ?>
					<?php $signup = strtotime($row['bi_regdate']);

					$concern = "No Concern";
					if($row['bi_temp_concern'] != ""){
						$concern = $row['bi_temp_concern'];
					}
					if($row['bi_status'] == 0){
						$process_date = "<span style='font-style: italic;'>Not yet processed.</span>";
						$process_by = "<span style='font-style: italic;'>Not yet processed.</span>";
					} else {
						if($row['bi_process_by']!="" || $row['bi_process_date']!=""){
							$process_by = $row['a_uname'];
							$get_date = strtotime($row['bi_process_date']);
							$process_date = date('M d, Y', $get_date);								
						} else {
							$process_by = "--";									
							$process_date = "--";									
						}
					}

					date_default_timezone_set("Asia/Manila");
					$get_today = date("Y-m-d");

					$get_reg_date = strtotime($row['bi_regdate']);
					$reg_date = date('Y-m-d');

				   	$waiting = 0;
					$status = "Undefined";

					switch ($row['bi_status']){
						case 0:
					   		if($reg_date == $get_today && $row['bi_email_verif'] == 1){
				   				$waiting = 0;
				  				$status = "New";
				   			}	
				   			if($reg_date < $get_today && $row['bi_email_verif'] == 1){
					   			$waiting = 0;
					   			$status = "Outstanding";
					   		}
					   		if($row['bi_email_verif'] == 0){
					   			$waiting = 1;
					   			$status = "Waiting for email verification";
					   		}
					   		break;
					   	case 1:
					   		$waiting = 0;
					   		$status = "Confirmed";
					   		break;
					   	case 2:
					   		$waiting = 0;
					   		$status = "Rejected";
					   		break;
					} 
					?>
					<?php if($row['bi_status'] == 0){ ?>
					<tr class="data_click" onclick="window.location.href='<?=base_url()?>support/registration/<?=$row['bi_id']?>'">
					<?php } else { ?>
					<tr class="data_click" onclick="window.location.href='<?=base_url()?>support/validated_reg/<?=$row['bi_id']?>'">
					<?php } ?>
						<td><?=date('F d, Y', $signup)?></td>
						<td><?=	$row['ADRZUS']?></td>
						<td><?=$row['bi_project']?></td>
						<td><?=$row['bi_blklot']?></td>
						<td><?=$row['u_desc']?></td>
						<td><?=$concern?></td>
						<td><?=$status?></td>
						<td> <?=$process_date?>	</td>
						<td> <?=$process_by?> </td>

					</tr>

				<?php } ?>

			</table>

		</div>

	<?php } ?>

</div>


<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$search_links?></div>
