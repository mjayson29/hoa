
<?php 
	$dbComp= array();
	foreach ($company as $value) {
		$dbComp[$value['BUKRS']] = $value['BUTXT'];
	}
	$assign= array();
	foreach ($grpBE as $key) {
		$assign[$key['ar_ccode']] = $key['BUTXT'];
	}
	foreach ($grpname as $values) {
		$grpnme = $values['rb_name'];
	}

?>
		<div class="panel panel-primary" id="viewrole">
		<input type="hidden" value="<?=$usrid?>" id="id">
		<input type="hidden" value="<?=$grpid?>" id="grpid">
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-3" style="padding:5px 0px;">Edit Business Entity Group</div>
                    <div class="col-md-2 col-md-offset-6" id="">
                    </div>
                <div class="clearfix"></div>
            </div>
			<div class="panel-body" id="" >
				<div class="row" id="">
					<div class="col-md-12">
						<div class="col-md-3">
							<label> Business Group </label> <br/>
							<span><input type="text" class="form-control input-sm" value="<?=$grpnme?>" id="" disabled></span>
						</div>
						<div class="col-md-4 col-md-offset-5 form-group" id="">
							<div class="col-md-6">
							<button class="btn btn-sm btn-success form-control" onclick="updateUsrGrp()"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Save</button>
							</div>
							<div class="col-md-6">
							<button class="btn btn-sm btn-danger form-control" onclick="cancelAddGroup()"><span class="icon-cancel-circle"></span> &nbsp;Cancel</button>
							</div>
						</div>
					</div>
					<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Business Entity Assignment</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-3 col-md-offset-2 container-fluid">
                            <span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Assigned Business Entity</label>
                            </span>
							<div class="selector-box">
								<div class="row-fluid" style="height:100%" id="">
									<select multiple id='resultbussEnt' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
										<?php
											foreach ($assign as $key => $value) {
												echo "<optgroup value='".$key."' label='".$value."'>";
												foreach ($bussEnt as $id => $values) {
													if($values['ar_ccode'] == $key) {
													echo "<option value='".$values['SWENR']."'>".$values['XWETEXT']."</option>";
													}
												}
												echo "</optgroup>";
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="selection-box">
								<div class="col-md-6 col-md-offset-3" style="top:25%">
									<button class="btn btn-sm btn-default form-control" onclick="removed()" style="padding:5px; margin-top:3px">></button>
									<button class="btn btn-sm btn-default form-control" onclick="add()" style="padding:5px; margin-top:3px"><</button>
									<button class="btn btn-sm btn-default form-control" onclick="removeall()" style="padding:5px; margin-top:3px">>></button>
									<button class="btn btn-sm btn-default form-control" onclick="addall()" style="padding:5px; margin-top:3px"><<</button>
								</div>
							</div>
						</div>
						<div class="col-md-3 container-fluid">
                            <span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Unassigned Business Entity</label>
                            </span>
							<div class="selector-box">
								<select id="selectedcompany" class="form-control input-sm" onchange="listproj2()">
									<option value="">-- Select Company --</option>
									<?php foreach($dbComp as $row => $tabla){ ?>
									<option value="<?=$row?>"><?=$tabla?></option>
									<?php } ?>
								</select>
								<div class="row-fluid" style="height:90%; padding-top:10px" id="loadedproj">
									<select multiple id='sourcebussEnt' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
										<p style="text-align: center; display: none;" id="load_sourcebussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>