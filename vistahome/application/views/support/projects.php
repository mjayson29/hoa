
<?php 
    $compArr = array();
    $projArr = array();
    $combo = array(); 
    foreach ($company as $key) { 
        $compArr[$key['BUKRS']] = $key['BUTXT']; 
        $projArr[$key['SWENR']] = $key['XWETEXT']; 
        $combo = array($key['BUKRS'] => array($key['SWENR'] => $key['XWETEXT'])); 
    }
 ?>
<script type="text/javascript">
    $( document ).ready(function() {    
        $('#businessGroup').hide();
        $('#loadingBG').css('display', 'block');         
        $.ajax({
            type:"get",
            url: "<?=base_url()?>support/businessGroup/",
            success: function(data){
                $('#businessGroup').html(data);
                $('#businessGroup').slideDown('fast');
                $('#loadingBG').css('display', 'none');
            }
        });
    });
</script>

<?php $acc = explode("-", $access);?>
    <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 5px 15px;">
                        <div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-user"></span> Users</div> 
                            <div class="btn-group col-md-8 pull-right">
                                <!-- <div class="col-md-1" style="float:right;padding-left:5px;padding-right:0px" onclick="slideSearch()">
                                    <button class="btn btn-xs input-sm btn-success form-control"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                                <div class="col-md-5" id="searchUser" style="float:right;display:none;padding-left:5px;padding-right:0px">
                                    <input type="texbox" id="userSearch" autocomplete="off" onblur="userSearchTxtBoxEvent()" onkeypress="uhandler(event)" class="input-sm form-control" placeholder="Search">
                                </div> -->
                            <?php if($acc[12]!=0){ ?>
                                <div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
                                    <button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/add_users'"><i class="icon-user-add"></i>&nbsp Add User</button>
                                </div>
                            <?php } ?>
                                <div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
                                    <button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/roles'"><i class="icon-key"></i>&nbsp Roles</button>
                                </div>
                                <div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
                                    <button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/projects'"><i class="icon-clippy"></i>&nbsp Projects</button>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
    
    <div id="editBG"></div>
<?php if($acc[13]!=0) { ?>
    <div>

        <div class="panel panel-primary" id="businessGroupBody">
            <div class="panel-heading" style="padding: 5px 15px;">
                <div class="col-md-2" style="padding:5px 0px;"><span class="icon-clippy"> </span> Business Entity </div>
                    <div class="col-md-2 col-md-offset-8" id="newProjBtn">
                        <button class="btn btn-xs input-sm btn-success form-control" style="float:right" onclick="newProject()"><i class="icon-key"></i>&nbsp New Group</button>
                    </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body" id="businessGroupDiv" style="display:none">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label> Business Group </label> <br/>
                            <span>
                                <input type="text" autocomplete="off" class="form-control input-sm" maxlength="40" value="" id="bgrpname">
                                <input type='hidden' autocomplete="off" id='grpid' value=''>
                            </span>
                        </div>
                        <div class="col-md-4 " style="display:none; float:right" id="newProjSaveBtn">
                            <div class="col-md-6">
                            <button class="btn btn-sm btn-success form-control" onclick="savebusinessgrp()"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Save</button>
                            </div>
                            <div class="col-md-6">
                            <button class="btn btn-sm btn-danger form-control" onclick="newProjCancelBtn()"><span class="icon-cancel-circle"></span> &nbsp;Cancel</button>
                            </div>
                        </div>
                        <div class="col-md-4" style="display:none; float:right" id="updProjSaveBtn">
                            <div class="col-md-6">
                            <button class="btn btn-sm btn-success form-control" onclick="updateBusinessGroup()"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Save</button>
                            </div>
                            <div class="col-md-6">
                            <button class="btn btn-sm btn-danger form-control" onclick="newProjCancelBtn()"><span class="icon-cancel-circle"></span> &nbsp;Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-top:20px">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label >Business Entity Assignment</label><br/>
                            </div>
                        </div>
                        <div class="col-md-12"> <hr> </div>
                        <div class="col-md-3 col-md-offset-2 container-fluid">
                            <span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Assigned Business Entity</label>
                            </span>
                            <div class="selector-box">
                                <div class="row-fluid" style="height:100%"> 
                                    <select multiple id='resultcompany' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
                                        <p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="selection-box">
                                <div class="col-md-6 col-md-offset-3" style="top:25%">
                                    <button class="btn btn-sm btn-default form-control" onclick="removed1()" style="padding:5px; margin-top:3px">></button>
                                    <button class="btn btn-sm btn-default form-control" onclick="add1()" style="padding:5px; margin-top:3px"><</button>
                                    <button class="btn btn-sm btn-default form-control" onclick="removeall1()" style="padding:5px; margin-top:3px">>></button>
                                    <button class="btn btn-sm btn-default form-control" onclick="addall1()" style="padding:5px; margin-top:3px"><<</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 container-fluid">
                            <span style="text-align:center">
                                <label class="custom_label" style="width:100%" >Unassigned Business Entity</label>
                            </span>
                            <div class="selector-box">
                                <select class="form-control input-sm" autocomplete="off" onchange="listproj()" id="selectedcompany">
                                    <option value="">-- Select Company --</option>
                                    <?php foreach($compArr as $key => $val){ ?>
                                    <option value="<?=$key?>"><?=$val?></option>
                                    <?php } ?>
                                </select>
                                <div class="row-fluid" style="height:90%; padding-top:10px" id="loadedproj">
                                    <select multiple autocomplete="off" id='sourcecompany' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
                                    <p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"><figcaption>Loading...</figcaption></p>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"> <hr> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

        <div class="row">
            <p style="text-align: center; display: none;" id="loadingBG"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
            <div class = "col-md-12" id = "businessGroup">      
            </div>
        </div>
</div>
