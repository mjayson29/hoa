
	<div class="row">
		<div id="tkt_notif" class="col-md-12" style="display:none">
			<div class="panel panel-success">
				<div class="panel-heading" style="padding: 5px 15px">
					<div class="col-md-12" style="padding:5px 0px;text-align:center">
						New Ticket Successfully posted!
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div id="tkt_search" class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span>Search Homeowner</span>
					<span class="clearfix"></span>
				</div>
				<div class="panel-body">

					<div class="col-md-6" style="text-align:center">
						<input id="ho" name="ctype" type="radio" onchange="selecttype()" autocomplete="off">
						<label for="ho" class="custom_label">Homeowner</label>
					</div>
					<div class="col-md-6" style="text-align:center">
						<input id="nh" name="ctype" type="radio" onchange="selecttype()" autocomplete="off">
						<label for="nh" class="custom_label">Non-Homeowner</label>
					</div>

					<div id="noneho" style="display:none; margin-top:50px">
						<hr />
						<div class="col-md-12" style="margin-bottom:15px">
							<div class="col-md-4">
								<select id="sel_source" class="form-control">
									<option value=""> -- SELECT SOURCE OF REQUEST --</option>
									<?php foreach ($source as $k) { ?>
										<option value="<?=$k['CODE']?>"> &nbsp;<?=$k['KURZTEXT']?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<br />
						<div class="col-md-12" style="margin-bottom:15px">
							<div class="col-md-6">
								<input id="fname" class="form-control" placeholder="FirstName">
							</div>
							<div class="col-md-6">
								<input id="lname" class="form-control" placeholder="LastName">
							</div>
						</div>
						<div class="col-md-12" style="margin-bottom:15px">
							<div class="col-md-6">
								<input id="cont" class="form-control" placeholder="Contact Number">
							</div>
							<div class="col-md-6">
								<input id="email" class="form-control" placeholder="E-Mail Address">
							</div>
						</div>
					</div>
					<div id="ho_search" style="display:none; margin-top:50px">
						<hr />
						<div class="col-md-4">
							<select id="tkt_pro" class="form-control" onchange="tkt2(this.value)">
								<option value=""> -- SELECT PROJECT -- </option>
								<?php if($mycompany){ 
										$select = "SWENR, XWETEXT";
										$table = "ZHOA_PROJECTS";
										$where = "SWENR IN (".$mycompany.")";
										$q = $this->Main->select_data_where($select, $table, $where);
										foreach ($q as $k) { ?>
											<option value="<?=$k['SWENR']?>"><?=$k['XWETEXT']?></option>
								<?php		}
										
									} else {
										$select = "SWENR, XWETEXT";
										$table = "ZHOA_PROJECTS";
										$q = $this->Main->select_data($select, $table);
										foreach ($q as $k) { ?>
											<option value="<?=$k['SWENR']?>"><?=$k['XWETEXT']?></option>
								<?php	}
									}	
								?>
							</select>
						</div>
						<div class="col-md-4">
							<input id="tkt_blk" class="form-control" placeholder="Blk/Lot No. (000-0000)" onkeypress="tkt(event)">
						</div>
						<div class="col-md-4">
							<input id="tkt_accnt" class="form-control" placeholder="Customer Number" onkeypress="accnt(event)">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="tkt_new" style="display:none"></div>

		<div id="tkt_body" class="col-md-12" style="display:none">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span> TYPE OF REQUEST</span>
					<span class="clearfix"></span>
				</div>
				<div class="panel-body">
					<div class="col-md-4">
						<select id="sel_type" class="form-control" onchange="get_req(this.value)">
							<option value=""> -- SELECT TYPE --</option>
							<?php foreach ($rtype as $r) { ?>
								<option value="<?=$r['rt_id']?>"> &nbsp;<?=$r['rt_desc']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-4">
						<select id="sel_sub" class="form-control" onchange="house()">
							<option> --select sub type--</option>
						</select>
					</div>
					<div id="sub_opt" class="col-md-4" style="display:none">
						<select id="sel_opt" class="form-control">
						</select>
					</div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span>MESSAGE</span>
					<span class="clearfix"></span>
				</div>
				<div class="panel-body">
					<textarea id="thisarea3" name="thisarea3"></textarea>
					<div class="col-md-12" style="margin-top:15px; padding: 0">
						<div class="col-md-6" style="padding-left:0;">
							<div id="att_label1" style="display:none">
								<label class="custom_label">Attachment</label>
								<span id="att_name1"></span>
							</div>
						</div>
						<div class="col-md-1 col-md-offset-2" style="padding-right:0">
							<button id="btn_att" class="btn-warning form-control" title="Attach image or document file"><span class="glyphicon glyphicon-paperclip"></span></button>
							<input type="file" id="userfile" style="display:none" onchange="showup1(this)">
						</div>
						<div class="col-md-1" style="padding-right: 0">
							<button id="btn_conadd" class="btn-success form-control" title="Add this concern"><span class="glyphicon glyphicon-plus"></span></button>
						</div>
						<div class="col-md-2" style="padding-right: 0">
							<button class="btn-danger form-control" title="Cancel this concern" onclick="can1()">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
			<div id="con_list" style="display:none">
				<div id="clist" style="display:none"> </div>
				<div class="container" style="padding:0">
					<div class='col-md-1' style="padding-left:0"> <input type='button' class='btn btn-primary btn-sm form-control' onclick='save_ticket(1)' value='Save'> </div>
					<div class='col-md-1' style="padding-left:0"> <input type='button' class='btn btn-danger btn-sm form-control' name='cancel' value='Cancel' onclick="cancel()"> </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	tinymce.init({
        selector: "textarea#thisarea3",
    });

	$('#btn_att').click(function(){
    	$('#userfile').trigger('click');
    })

	$('#btn_conadd').click(function() {
		var accnt = $('#name').text();
		var proj = $('#proj').val();
		var req = $('#sel_type option:selected').val();
		var sub_type = $('#sel_sub option:selected').val();
		var sub_opt = $('#sel_opt option:selected').val();
		var message = window.parent.tinymce.get('thisarea3').getContent();
		var attachment = $('#userfile').val();
		var attach = $('#userfile').prop('files');

		if(req == "" || sub_type == "" || message == ""){
			bootbox.alert({
				size: "small",
				message: "Please fill up the field required"
			});
			return false;
		}

		if((sub_type != 1 && sub_type != 6) && sub_opt == "") {
			bootbox.alert({
				size: "small",
				message: "Please fill up the field required 2"
			});
			return false;
		}

        var fd = new FormData();
        fd.append('userfile', attach[0]);

        $.ajax({
            url: '<?=base_url()?>portal/request_upload/',
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){ 
            }
        });

        var concern = {"req" : req, "sub_type" : sub_type, "sub_opt" : sub_opt, "message" : message, "attachment" : attachment }

        $("#clist").load('<?=base_url()?>support/concern_enlist/', concern );
        $("#clist").css('display','block');
        $("#con_list").css('display','block');
        $("#sel_type").val('');
        $("#sel_sub").val('');
        $("#sel_opt option").remove();
        $('#sub_opt').css('display','none');
        tinymce.get('thisarea3').setContent('');
        $("#userfile").val('');

	});

		function showup1(input){
		var limit = 16;
    	var raw = input.files;
    	if (input.files && input.files[0]) {
    		if(raw[0].size > 2097152){ //approx 2MB
    			bootbox.alert({
    				size: 'small',
    				message: 'The File you are trying to attach is too large'
    			});
    			reset($('#userfile'));
    			return false;
    		}
            var reader = new FileReader();
            var f = input.files[0];
            var fname = escape(f.name);
            var x = fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);
            var ext = x.toUpperCase();
            if(ext != 'JPG' && ext != 'PNG' && ext != 'JPEG' && ext != 'PDF' && ext != 'DOCX' && ext != 'DOC' && ext != 'XLSX' && ext != 'XLS'){
            	bootbox.alert({
    				size: 'small',
    				message: 'Invalid file type'
    			});
    			reset($('#userfile'));
    			return false;
            }
            $('#att_name1').text(fname);
            reader.readAsDataURL(input.files[0]);
            $('#att_label1').css('display','block');
        }
    }

    window.reset = function (e) {
	    e.wrap('<form>').closest('form').get(0).reset();
	    e.unwrap();
	}


	function tkt(e){
		if(e.keyCode === 13){
			var blk = $('#tkt_blk').val();
			var pro = $('#tkt_pro option:selected').val();
			var tkt = "blk";

			if(pro == "") {
				bootbox.alert({
					size: 'small',
					message: 'Please select project'
				});
				return false;
			}

			if(blk == ""){
				return false;
			}
			$('#page_loader').css('display','block');
			$.ajax({
				type: 'POST',
				url: "<?=base_url()?>support/addTickets/",
				data: {
					blk: blk,
					pro: pro,
					tkt: tkt
				},
				success: function(data){
					$('#tkt_new').html(data);
					$('#tkt_new').css('display','block');
					$('#page_loader').css('display','none');
				}
			});
        }
	}

	function tkt2(q){
		var blk = $('#tkt_blk').val();
		var pro = q;
		var tkt = "blk";

		if(blk != "") {
			$('#page_loader').css('display','block');
			$.ajax({
				type: 'POST',
				url: "<?=base_url()?>support/addTickets/",
				data: {
					blk: blk,
					pro: pro,
					tkt: tkt
				},
				success: function(data){
					$('#tkt_new').html(data);
					$('#tkt_new').css('display','block');
					$('#page_loader').css('display','none');
				}
			});
		}
	}

	function accnt(e){
		if(e.keyCode === 13){
			var num = $('#tkt_accnt').val();
			var tkt = "accnt";
			$('#page_loader').css('display','block');
			if(num != ""){
				$.ajax({
					type: 'POST',
					url: "<?=base_url()?>support/addTickets/",
					data: { num: num,
							tkt: tkt 
					},
					success: function(data){
						$('#tkt_new').html(data);
						$('#tkt_new').css('display','block');
						$('#page_loader').css('display','none');
					}
				});
			}
		}

	}

	function selecttype(){
		var home = $('#ho');
		var non = $('#nh');
		if(home.is(':checked')){
			$('#ho_search').css('display','block');
			$('#tkt_notif').css('display','none');
		} else {
			$('#ho_search').css('display','none');
			$('#tkt_body').css('display','none');
		}
		if(non.is(':checked')){
			$.ajax({
				type:"POST",
                url: "<?=base_url()?>support/unset_sess_tkt/",
                success: function(data){
                }
			})
			$('#tkt_notif').css('display','none');
			$('#sel_source').val("");
			$('#fname').val("");
			$('#lname').val("");
			$('#cont').val("");
			$('#email').val("");
			$('#con_list').css('display','none');
			$('#noneho').css('display','block');
			$('#tkt_body').css('display','block');
			$('#tkt_result').remove();
			$('#tkt_new').css('display','none');
		} else {
			$('#noneho').css('display','none');
			$('#tkt_body').css('display','none');
		}
	}

	function get_req(val){ 
		$("#sub_opt option[value='']").attr('selected','selected');
        $('#sub_opt').slideUp('fast');
        $('#sel_type').css('cursor' , 'progress');
        $.ajax({
            type:"get",
            url: "<?=base_url()?>support/get_sub_req/"+val,
            success: function(data){
                $('#sel_sub').html(data);
                $('#sel_type').css('cursor' , 'default');
            }
        });
    }

    function house(){
        var house = $('#sel_sub option:selected').val();
        var where;

        switch (house) {
            case '2':
                where = "HSEC";
                break;
            case '3':
                where = "DEVT";
                break;
            case '4':
                where = "LOTC";
                break;
            case '5':
                where = "SERV";
                break;
            default:
                where ="";
        }
        if(where != ""){
        	$('#sel_sub').css('cursor' , 'progress');
            $.ajax({
                type:"POST",
                url: "<?=base_url()?>support/housecon_type/",
                data: { where: where},
                success: function(data){
                    $('#sel_opt').html(data);
                    $('#sub_opt').slideDown('fast');
                    $('#sel_sub').css('cursor' , 'default');
                }
            });
        } else{
        	$("#sub_opt option[value='']").attr('selected','selected');
            $('#sub_opt').slideUp('fast');
        }
    }

    	function save_ticket(n) {
		var id = n;
		var source = $('#sel_source option:selected').val();
		var name = $('#fname').val() + " " + $('#lname').val(); 
		var cont = $('#cont').val();
		var email = $('#email').val();

		if(source == ""){
			bootbox.alert({
				size: 'small',
				message: 'Please select source of request'
			});
			return false;
		}
		bootbox.confirm({
			size: 'small',
			message: 'Are you sure you want to post this ticket?',
			callback: function(result){
				if(result){
					$('#page_loader').css('display','block');
					$.ajax({
						type: 'POST',
						url: '<?=base_url()?>support/save_ticket/',
						data: {
							source: source,
							name: name,
							id: id,
							cont: cont,
							email: email
						},
						success: function(){
							$('#page_loader').css('display','none');
							$('#tkt_body').css('display','none');
							$('#sel_source').val("");
							$('#fname').val("");
							$('#lname').val("");
							$('#cont').val("");
							$('#email').val("");
							$('#tkt_notif').css('display','block');

						}
					})	
				}
			}
		})
	}

	function cancel() {

		bootbox.confirm({
			size: 'small',
			message: 'Are you sure you want to discard all concerns?',
			callback: function(result){
				if(result){
					$.ajax({
						type: 'POST',
						url: "<?=base_url()?>support/unset_sess_tkt/",
			            success: function(data){
							$('#tkt_body').css('display','none');
							$('#sel_source').val("");
							$('#fname').val("");
							$('#lname').val("");
							$('#cont').val("");
							$('#email').val("");
							$('#con_list').css('display','none');
			            }
					})			
				}
			}
		})
	}

	function can1() {
		$('#sel_type').val('');
		$('#sel_sub').val('');
		$('#sel_opt').val('');
		$('#sub_opt').css('display','none');
		tinymce.get('thisarea3').setContent('');
		$('#att_label1').css('display','none');
		reset($('#userfile'));
	}
</script>	