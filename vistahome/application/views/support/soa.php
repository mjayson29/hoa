<script type="text/javascript">
	$(document).ready(function(){
		$('#plist').hide();
        $('#loading').css('display', 'block');         
        $.ajax({
            type:"get",
            url: "<?=base_url()?>support/project_list/",
            success: function(data){
                $('#plist').html(data);
                $('#plist').slideDown('fast');
                $('#loading').css('display', 'none');
            }
        });

		$('form').submit(function(event){
			$('#plist').hide();
        	$('#loading').css('display', 'block');   
			$.ajax({
		        type: 'POST',
		        url: '<?=base_url()?>support/search_proj/',
		        data:	{
		            key : $('#pSearch').val()
		        }, 
		        success: function(data){
					$('#plist').html(data);
	                $('#plist').slideDown('fast');
	                $('#loading').css('display', 'none');
		        }
		    });
	        event.preventDefault();
		});
	});

	function val(form){
		if($('#pSearch').val() == ''){
			$("#searchPro").animate({width:'toggle'},350);
			$('#pSearch').focus();
			return false;
		}
	}

	function soa_active(val){
  		var act;
  		if($('#pbox'+val).is(":checked")){
  			act = 1;
  		}else{
  			act = 0;
  		}
  		$.ajax({
	    	type:"post",
	    	url: "<?=base_url()?>support/soa_act/",
	    	data: {
	    		swenr : val,
	    		act : act
	    	},
	      	success: function(data){
	      	}
	    });
  	}

</script>

<?php $acc = explode("-", $access); ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-wrench"></span> Maintenance</div> 
					<div class="btn-group col-md-8 pull-right">
					<?php if($acc[22]!=0){ ?>
						<form method="POST" action="">
							<div class="col-md-1" style="float:right;padding-left:5px;padding-right:0px">
								<button type="submit" onclick="return val(this)" class="btn btn-xs input-sm btn-success form-control"><i class="glyphicon glyphicon-search"></i></button>
							</div>
							<div class="col-md-4" id="searchPro" style="float:right;display:none;padding-left:5px;padding-right:0px">
								<input type="texbox" id="pSearch" autocomplete="off" class="input-sm form-control" placeholder="Search">
							</div>
						</form>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/soa'"><i class="icon-profile"></i>&nbsp SOA</button>
						</div>
					<?php } ?>
					<?php if($acc[21]!=0){ ?>
						<div class="col-md-3" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/announcements'"><i class="glyphicon glyphicon-blackboard"></i>&nbsp Announcements</button>
						</div>
					<?php } ?>
					<?php if($acc[20]!=0){ ?>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/carousel'"><i class="glyphicon glyphicon-film"></i>&nbsp Carousel</button>
						</div>
					<?php } ?>
					</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>

	<div id="plist">
		<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
	</div>