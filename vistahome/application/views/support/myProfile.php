<script type="text/javascript">
   // for uploading profile pic
   $(document).ready(function(){

      var fileInput = $('#userfile');
      var bi_id = $('#usr-id').val();
      // button = $('#upload');
      fileInput.on('change', function(){
        var files = fileInput.prop('files');
        // Create a new FormData object, append yung file para makuha mo yung $_POST
        var fd = new FormData();
        var attachment = fd.append('userfile', files[0]);

          $.ajax({
            url: '<?=base_url()?>support/upload/'+bi_id,
            data: fd,
            contentType:false,
            processData:false,
            type:'POST',
            success: function(data){
              document.getElementById("img-prof").innerHTML = data;  
            }

          });
      });
    });

  // removing profile pic
  $(document).ready(function(){
    $('.rm').on('click', function(){
      var usrid = $('#usr-id').val();
      var curr_img = $('#prop_img').val();
      if (curr_img != "official.png"){
        $.ajax({
          url: '<?=base_url()?>support/dP_def/'+usrid,
          type:'POST',
          data: {
            curr_img: curr_img
          },
          success: function(data){
            document.getElementById("img-prof").innerHTML = data;  
          }
        });
      }
    }); 
  });

        $(document).ready(function(){
          $('#img-prof').hover(function(){
          $('#img-circle').addClass('hov');
          $('.profile_img').addClass('sho');
          $('.profile_img_rm').addClass('sho');
        }, function(){
          $('#img-circle').removeClass('hov');
          $('.profile_img').removeClass('sho');
          $('.profile_img_rm').removeClass('sho');
        });
        $('.glyphicon-camera').hover(function(){
        	$('.glyphicon-camera').addClass('hove');
        }, function(){
        	$('.glyphicon-camera').removeClass('hove');
        });  
        $('.glyphicon-remove').hover(function(){
        	$('.glyphicon-remove').addClass('hove');
        }, function(){
        	$('.glyphicon-remove').removeClass('hove');
        });  
      });
</script>	
<?php
foreach ($get_profile as $value) {
	$fname = $value['ai_fname'];
	$lname = $value['ai_lname'];
	$uname = $value['a_uname'];
	$email = $value['a_email'];
	$spark = $value['ai_spark'];
	$img = $value['a_profimg'];
}
if(!$img){
	$img = "official.png";
}
?>	

		<div class="row prof">	
			<div class="col-md-10 col-md-offset-1">
			<?php 
                  $array=array('id'=>'uploadform');
                  echo form_open_multipart(); 
                ?>
				<div id="img-prof" class="col-md-3">
					<img id="img-circle" class="prof-img" src="<?=base_url()?>assets/img/admin/<?=$img?>">
					<input type="hidden" id="prop_img" value="<?=$img?>">
					<label for="userfile">
                    	<span class="glyphicon glyphicon-camera profile_img" title="upload"></span>
                  	</label>
                  	<span class="glyphicon glyphicon-remove profile_img rm" title="remove"></span>
				</div>
				<?php echo form_upload(array('name'=>'userfile','id'=>'userfile','style'=>'display:none;')); ?>
				<input type="hidden" id="usr-id" value="<?=$sess_id?>">
				<?php echo form_close(); ?> 
				<div class="col-md-9">
					<div class="col-md-9">
						<div class="prof-fname"><?=$fname?></div>
						<div class="prof-lname"><?=$lname?></div>
					</div>
					<div class="col-md-9"><hr></div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-3">
								<label class="custom_label">Username</label>
							</div>
							<div class="col-md-9"><?=$uname?></div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label class="custom_label">Email</label>
							</div>
							<div class="col-md-9"><?=$email?></div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label class="custom_label">Spark ID</label>
							</div>
							<div class="col-md-9"><?=$spark?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
