<script type="text/javascript">
	$( document ).ready(function() {
        $.ajax({
            type:"post",
            url: "<?=base_url()?>support/get_new_tickets/",
            // url: <?=base_url()?>+"personnel/get_items/"+val,
            success: function(data){
                $('#ticket_loader').html(data);
                $('#ticket_loader').slideDown('fast');
                $('#loading').css('display', 'none');
            }
        });
    });

 //    $(document).ready(function () {
 //    	var where = $('#where').val();
	//     setInterval(function() {
	//         $.post("<?=base_url()?>support/count_new_concerns_rows", {where : where}, function (result) {
	//             $('#new').html(result);
	//         });
	//         $.post("<?=base_url()?>support/count_open_concerns_rows", {where : where}, function (result) {
	//             $('#open').html(result);
	//         });
	//         $.post("<?=base_url()?>support/count_closed_concerns_rows", {where : where}, function (result) {
	//             $('#closed').html(result);
	//         });
	//     }, 1000);
	// });

    $(document).ready(function () {
    	var city = $('#company').val();
    	if(city != ""){
    		$.ajax({
                type:"GET",
                url: "<?=base_url()?>support/get_proj/"+city,
                success: function(data){
                    $('#load_project').html(data);
                    $('#page_loader').css('display','none');
                    return false;
                }
            });
    	}
    })

</script>

	<div class="row home-icons">
		<div class="col-md-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-4">
							<span class="glyphicon glyphicon-flag">&nbsp;</span> 
						</div>
						<div class="col-xs-8 home_labels">
							<span class="home_counter" id="new"><?=$home_new_ticket;?></span>
							<br>
							<span class="">New</span>
						</div>
					</div>
				</div>

				<div class="panel-footer">
					<a href="javascript:;" onclick="tickets(1)">
						<span class="pull-left">Show List</span>
						<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
					</a>
					<span class="clearfix"></span>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-4">
							<span class="glyphicon glyphicon-folder-open">&nbsp;</span> 
						</div>

						<div class="col-xs-8 home_labels">
							<span class="home_counter" id="open"><?=$home_open_ticket;?></span>
							<br>
							<span class="">Open</span>
						</div>
					</div>
				</div>

				<div class="panel-footer">
					<a href="javascript:;" onclick="tickets(2)">
						<span class="pull-left">Show List</span>
						<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
					</a>
					<span class="clearfix"></span>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-4">
							<span class="glyphicon glyphicon-folder-close">&nbsp;</span> 
						</div>

						<div class="col-xs-8 home_labels">
							<span class="home_counter" id="closed"><?=$home_closed_ticket;?></span>
							<br>
							<span class="">Closed</span>
						</div>
					</div>
				</div>

				<div class="panel-footer">
					<a href="javascript:;" onclick="tickets(3)">
						<span class="pull-left">Show List</span>
						<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
					</a>
					<span class="clearfix"></span>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="panel panel-lblue">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-4">
							<span class="glyphicon glyphicon-file">&nbsp;</span> 
						</div>

						<div class="col-xs-8 home_labels" style="text-align: justify;font-size: 43px">
							<span class="home_counter">Create</span>
						</div>
					</div>
				</div>

				<div class="panel-footer text-center">
					<a href="<?=base_url()?>support/newTickets">
						<span>New Tickets</span>
					</a>
					<span class="clearfix"></span>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span class="pull-left">Filter</span>
					<span class="pull-right toggle_filter glyphicon glyphicon-chevron-down" id="toggle_filter"></span>
					<span class="clearfix"></span>
				</div>

				<div class="panel-body" id="load_filter">
					<div class="row">
						<div class="col-md-4">
							<small>Date Posted</small>
							<input type="date" class="form-control input-sm" id="posted_from" value=""  placeholder="MM/dd/YYYY">
						</div>

						<div class="col-md-4">
							<small>To</small>
							<input type="date" class="form-control input-sm" id="posted_to" value=""  placeholder="MM/dd/YYYY">
						</div>

						<div class="col-md-4">
							<small>Ticket Status</small>
							<select class="form-control input-sm" id="status" onchange="status(this.value)" autocomplete="off">
								<option value="">--Select Status--</option>
								<option value="0">New</option>
								<option value="1">Open</option>
								<option value="2">Closed</option>
								<option value="3">Unposted</option>
								<option value="4">Posted</option>
								<option value="5">Partial</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<small>Project Area</small>
							<select class="form-control input-sm" id="company" onchange="get_proj(this.value)">
								<option value="">-- Select Project Area --</option>
									<?php foreach($proj_area as $row){ ?>
									<!-- <input type="text" name="" id="" class="form-control input-sm" placeholder="Company"> -->
									<option value="<?=$row['PROJ_ID']?>"><?=$row['PROJ_AREA']?></option>
									<?php } ?>
							</select>
						</div>

						<div class="col-md-4">
							<small>Project</small>
							<div id="load_project">
								<select id="project" class="form-control input-sm">
									<option value=''>-- Select Project --</option>
								</select>
							</div>
						</div>

						<div class="col-md-3">
							<small>Keyword</small>
							<input type="text" class="form-control input-sm" id="keyword" onkeyup="onEnt(event)">
						</div>

						<div class="col-md-1">
							<small>&nbsp;</small>
							<button class="form-control btn btn-primary btn-sm input-sm" onclick="search_concern()"><span class="glyphicon glyphicon-search"></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<strong>NOTE: Tickets with <span class="age_red">RED font</span> exceeds their aging and ticket with <span class="age_green">GREEN font</span> are already closed.</strong>
		</div>
	</div>

	<div class="row">	
		<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
		
		<div class="col-md-12" id="ticket_loader"></div>
	</div>
</div>
	