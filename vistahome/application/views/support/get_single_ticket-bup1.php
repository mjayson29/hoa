<div class="panel-body">
<?php 
	if(!$concern_details){ } else { ?>

	<div style="max-height: 300px; overflow: auto; overflow-x: hidden; border: 0px solid #ddd; min-width: 320px;">
		<?php foreach($concern_details as $cd){
				if($cd['cd_concern_id'] == $get_id){
					$date_created = strtotime($cd['cd_date_created']);
					$today = date('Y-m-d H:i:s');
					$start_date = new DateTime(date('Y-m-d H:i:s', $date_created));
					$since_start = $start_date->diff(new DateTime($today));

					if($since_start->h <= 0){

						if($since_start->i <= 0){

							if($since_start->s <= 1){

								$seconds = $since_start->s;
								$get_time = $seconds." sec ago";

							} else {

								$seconds = $since_start->s;
								$get_time = $seconds." secs ago";

							}

						} else {

							if($since_start->i == 1){

								$minutes = $since_start->i;
								$get_time = $minutes." minute ago";

							} else {

								$minutes = $since_start->i;
								$get_time = $minutes." minutes ago";

							}

						}

					}

					if($since_start->h >= 1 AND $since_start->h < 24){

						if($since_start->h == 1){

							$hours = $since_start->h;
							$get_time = $hours." hour ago";

						} else {
									
							$hours = $since_start->h;
							$get_time = $hours." hours ago";

						}

					}

					if($since_start->d >= 1 AND $since_start->d <= 6){

						if($since_start->d == 1){

							$days = $since_start->d;
							$get_time = $days." day ago";

						} else {

							$days = $since_start->d;
							$get_time = $days." days ago";

						}

					}

					if($since_start->d > 6){

						$get_time = date('F d, Y h:i:s a', $date_created);

					}
		?>

			<div class='col-xs-12' style='word-wrap: break-word;'>
				<a href='#'><?=$cd['cd_reply_by']?></a>
			</div>
			<br>
			<div class='col-xs-12' style='word-wrap: break-word;'>
				<?=$cd['cd_desc']?>
			</div>
			<br>
			<?php $select = ""; $table = "ZHOA_concern_attachments"; $where = "ca_cd_id = ".$cd['cd_id']."" ?>
			<?php $attach = $this -> Main -> select_data_where($select, $table, $where);?>
			<?php foreach($attach as $a){ ?>

				<?php if(!$a['ca_attachment']){ } else { ?>
					<div class="col-xs-12">&nbsp;</div>
					<div class="col-xs-2">
						<img src="<?=base_url()?>assets/img/logos/<?=$a['ca_attachment']?>" class="img-responsive">
					</div>
				<?php } ?>

			<?php }?>

			<div class='col-xs-12' style='word-wrap: break-word;'>
				<small style='color: #aaa;'><?=$get_time?></small>
			</div>
			<hr class='col-xs-12 col-sm-12 col-md-12' style='margin-top: 5px; margin-bottom: 5px; '>

			<?php } ?>

		<?php } ?>

	</div>

<?php } ?>

	<!-- <div style='margin: 10px;'>
		<div class='col-xs-2 col-sm-2 col-md-1'>
			<div class="media">
				<div class="media-left">
					<a href="#">
						<img class="img-circle" src="<?=base_url('assets/img/users/user.png')?>" alt="..." style="width: 60px;">
					</a>
				</div>
			</div>
		</div>
		<div class='col-xs-6 col-sm-6 col-md-6'>
			<small>Send a reply</small>
			<textarea class='form-control' style='height: 35px; resize: vertical;' name='concern_msg' id='concern_msg<?=$get_id?>'></textarea>
		</div>
		<div class='col-xs-2 col-sm-2 col-md-1'>
			<br>
			<button type='button' class='btn btn-primary btn-sm form-control' onclick='send_reply(<?=$get_id?>)'><span class='glyphicon glyphicon-send'></span></button>
		</div>
		<div class='col-xs-2 col-sm-2 col-md-1'>
			<br>
			<button type='file' class='btn btn-warning btn-sm form-control' onclick=''><span class='glyphicon glyphicon-paperclip'></span></button>
		</div>
		<div class='col-md-2'></div>
	</div> -->

		<?php $array=array('id'=>'uploadform');echo form_open_multipart(); ?>
		<div class="col-xs-10 col-sm-10 col-md-6">
			<small>Send Reply</small>
			<br>
			<textarea class="form-control input-sm" style="resize: vertical; height: 30px;" id='concern_msg<?=$get_id?>'></textarea>
			<!-- <input type="text" name="" class="form-control input-sm"> -->
		</div>
	   	<div class="col-xs-2 col-sm-2 col-md-1">
			<br>
			<button type="button" class="btn btn-primary btn-sm form-control input-sm" onclick='send_reply(<?=$get_id?>)'> <span class="glyphicon glyphicon-send"></span></button>
	 	</div>
	 	<div class="col-xs-4 col-sm-4 col-md-2">
			<br>
			<button type="button" class="btn btn-danger btn-sm form-control input-sm" onclick=''> Close Request</button>
	 	</div>
	 	<div class="col-xs-4 col-sm-4 col-md-2">
			<br>
                <label for="userfile">
		   			<span type="button" class="btn btn-warning form-control input-sm btn-sm"><!-- <span class="glyphicon glyphicon-paperclip"></span> -->Attach file</span>
                </label>
                <?php echo form_upload(array('name'=>'userfile','id'=>'userfile','style'=>'display:none;')); ?>
                <!-- <input type="hidden" id="get_id" value="<?=$row['bi_id']?>"> -->
            <?php echo form_close(); ?> 
	 	</div>
	</div>