<?php
	foreach ($grpComp as $asd) {
		$ui_uname = $asd['a_uname'];
		$ui_lname = $asd['ai_lname'];
		$ui_fname = $asd['ai_fname'];
		$ui_email = $asd['a_email'];
		$ui_spark = $asd['ai_spark'];
		$ui_permission = $asd['ur_permission'];
		$ui_rid = $asd['ai_role'];
	}
	$ui = explode("-", $ui_permission);
	$acc = explode("-", $access);
	$select = "";
	$table = "ZHOA_usraccess";
	$saan = "ua_id =".$ui_rid;
	$role = $this -> Main -> select_data_where($select, $table, $saan);
	foreach ($role as $val) {
		$ui_role = $val['ua_rname'];
	}
	
?>
<script type="text/javascript">
	$( document ).ready(function() {            
		$.ajax({
			type:"get",
			url: "<?=base_url()?>support/usrGrp/"+<?=$where?>,
			success: function(data){
				$('#bgrptbl').html(data);
				$('#bgrptbl').css('display', 'block');
				$('#addGrpBtn').css('display', 'none');
			}
		});
	});
</script>
	<div class="panel panel-primary" >
		<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-user"> </span> User's Info</div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6" style="margin-bottom:15px">
						<label class="custom_label">Username</label>
						<span><?=$ui_uname?></span>
						<br>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6" style="margin-bottom:15px">
						<label class="custom_label">Last Name</label>
						<span><?=$ui_lname?></span>
						<br>						
					</div>
					<div class="col-md-6" style="margin-bottom:15px">
						<label class="custom_label">First Name</label>
						<span><?=$ui_fname?></span>
						<br>				
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6" style="margin-bottom:15px">
						<label class="custom_label">Email</label>
						<span><?=$ui_email?></span>
						<br>						
					</div>
					<div class="col-md-6" style="margin-bottom:15px">
						<label class="custom_label">Spark ID</label>
						<span><?=$ui_spark?></span>
						<br>						
					</div>
				</div>
				<div class="col-md-12"> <hr> </div>
				<div class="col-md-12" style="margin-top:25px">
					<div class="col-md-6">
						<label class="custom_label">Role</label>
						<span><?=$ui_role?></span>
						<br>
					</div>
				</div>
				<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-3">
								<label class="custom_label">Navigation</label><br/>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Registration List</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="regview" onclick="return false" <?php if($ui[0]==1){echo"checked";} ?>> 
										<label for="regview">View</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="regconf" <?php if($ui[1]==1){echo"checked";} ?> onclick="return false"> 
										<label for="regconf">Confirm</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Support</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="suppview" <?php if($ui[2]==1){echo"checked";} ?> onclick="return false"> 
										<label for="suppview">View</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="suppreply" <?php if($ui[3]==1){echo"checked";} ?> onclick="return false"> 
										<label for="suppreply">Reply</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="suppcomm" <?php if($ui[19]==1){echo"checked";} ?> onclick="return false"> 
										<label for="suppcomm">Comment</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Reports</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="rptview" <?php if($ui[4]==1){echo"checked";} ?> onclick="return false"> 
										<label for="rptview">View</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="rptexpo" <?php if($ui[5]==1){echo"checked";} ?> onclick="return false"> 
										<label for="rptexpo">Export</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Maintenance</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="mtc" <?php if($ui[6]==1){echo"checked";} ?> onclick="return false"> 
										<label for="mtc">View</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label"> &nbsp;&nbsp;&nbsp; Carousel</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="carview" <?php if($ui[20]==1){echo"checked";} ?> onclick="return false"> 
										<label for="carview">View</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label"> &nbsp;&nbsp;&nbsp; Announcements</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="annview" <?php if($ui[21]==1){echo"checked";} ?> onclick="return false"> 
										<label for="annview">View</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label"> &nbsp;&nbsp;&nbsp; SOA</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="soaview" <?php if($ui[22]==1){echo"checked";} ?> onclick="return false"> 
										<label for="soaview">View</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Homeowners List</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="hoview" <?php if($ui[7]==1){echo"checked";} ?> onclick="return false"> 
										<label for="hoview">View</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Account Settings</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="accnt" <?php if($ui[8]==1){echo"checked";} ?> onclick="return false"> 
										<label for="accnt">View</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label">Users</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="usrview" <?php if($ui[9]==1){echo"checked";} ?> onch onclick="return false">
										<label for="usrview">View</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="usredit" <?php if($ui[10]==1){echo"checked";} ?> onclick="return false"> 
										<label for="usredit">Edit</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="usrdel" <?php if($ui[11]==1){echo"checked";} ?> onclick="return false"> 
										<label for="usrdel">Delete</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label"> &nbsp;&nbsp;&nbsp; Add Users</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="usradd" <?php if($ui[12]==1){echo"checked";} ?> onclick="return false"> 
										<label for="usradd">Add</label>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 col-md-offset-1">
										<label class="custom_label"> &nbsp;&nbsp;&nbsp; Roles</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="roleadd" <?php if($ui[13]==1){echo"checked";} ?> onclick="return false"> 
										<label for="roleadd">Add</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="roleedit" <?php if($ui[14]==1){echo"checked";} ?> onclick="return false"> 
										<label for="roleedit">Edit</label>
									</div>
									<div class="col-md-2">
										<input type="checkbox" id="roledel" <?php if($ui[15]==1){echo"checked";} ?> onclick="return false"> 
										<label for="roledel">Delete</label>
									</div>
								</div>
								<div class="col-md-12"> <hr> </div>
							</div>
							<div class="col-md-12" style="padding-top:20px">
								<div class="col-md-12">
									<div class="col-md-6">
										<label >Receive E-mail Alert</label><br/>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-2 col-md-offset-2">
										<input type="checkbox" id="concern" <?php if($ui[16]==1){echo"checked";} ?> onclick="return false"> 
										<label for="concern">Concern</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="req" <?php if($ui[17]==1){echo"checked";} ?> onclick="return false"> 
										<label for="req">Request</label>
									</div>
									<div class="col-md-2 col-md-offset-1">
										<input type="checkbox" id="inq" <?php if($ui[18]==1){echo"checked";} ?> onclick="return false"> 
										<label for="inq">Inquiry</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<div id="bgrptbl"></div>
		</div>
	</div>
</div>
