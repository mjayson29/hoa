<script type="text/javascript">
	$( document ).ready(function() {  
        $('#loader').hide();
        $('#loading').css('display', 'block');           
        $.ajax({
            type:"post",
            url: "<?=base_url()?>support/get_new/",
            // url: <?=base_url()?>+"personnel/get_items/"+val,
            success: function(data){
                $('#loader').html(data);
                $('#loader').slideDown('fast');
                $('#loading').css('display', 'none');
            }
        });
    });

 //    $(document).ready(function () {
 //    	var where = $('#where').val();
	//     setInterval(function() {
	//         $.post("<?=base_url()?>support/count_new_reg", {where : where}, function (result) {
	//             $('#new').html(result);
	//         });
	//         $.post("<?=base_url()?>support/count_out_reg", {where : where}, function (result) {
	//             $('#out').html(result);
	//         });
	//         $.post("<?=base_url()?>support/count_conf_reg", {where : where}, function (result) {
	//             $('#conf').html(result);
	//         });
	//         $.post("<?=base_url()?>support/count_rej_reg", {where : where}, function (result) {
	//             $('#rej').html(result);
	//         });
	//         $.post("<?=base_url()?>support/count_wait_reg", {where : where}, function (result) {
	//             $('#wait').html(result);
	//         });
	//     }, 1000);
	// });

    $(document).ready(function () {
    	var city = $('#company').val();
    	if(city != ""){
    		$.ajax({
                type:"GET",
                url: "<?=base_url()?>support/get_proj/"+city,
                success: function(data){
                    $('#load_project').html(data);
                    $('#page_loader').css('display','none');
                    return false;
                }
            });
    	}
    })

</script>

			<div class="row home-icons">
				<div class="col-md-15">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4">
									<span class="glyphicon glyphicon-flag">&nbsp;</span> 
								</div>
								<div class="col-xs-8 home_labels">
									<span class="home_counter" id="new"><?=$home_new_counter?></span>
									<br>
									<span class="">New</span>
								</div>
							</div>
						</div>

						<div class="panel-footer">
							<a href="javascript:;" onclick="registration(1)">
								<span class="pull-left">Show List</span>
								<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
							</a>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>

				<div class="col-md-15">
					<div class="panel panel-yellow">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4">
									<span class="glyphicon glyphicon-time">&nbsp;</span> 
								</div>
								<div class="col-xs-8 home_labels">
									<span class="home_counter" id="out"><?=$home_out_counter?></span>
									<br>
									<span class="">Outstanding</span>
								</div>
							</div>
						</div>

						<div class="panel-footer">
							<a href="javascript:;" onclick="registration(2)">
								<span class="pull-left">Show List</span>
								<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
							</a>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>

				<div class="col-md-15">
					<div class="panel panel-green">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4">
									<span class="glyphicon glyphicon-ok">&nbsp;</span>
								</div>
								<div class="col-xs-8 home_labels">
									<span class="home_counter" id="conf"><?=$home_conf_counter?></span>
									<br>
									<span class="">Confirmed</span>
								</div>
							</div>
						</div>

						<div class="panel-footer">
							<a href="javascript:;" onclick="registration(3)">
								<span class="pull-left">Show List</span>
								<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
							</a>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>

				<div class="col-md-15">
					<div class="panel panel-red">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4">
									<span class="glyphicon glyphicon-ban-circle">&nbsp;</span> 
								</div>
								<div class="col-xs-8 home_labels">
									<span class="home_counter" id="rej"><?=$home_rej_counter?></span>
									<br>
									<span class="">Rejected</span>
								</div>
							</div>
						</div>

						<div class="panel-footer">
							<a href="javascript:;" onclick="registration(4)">
								<span class="pull-left">Show List</span>
								<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
							</a>
							<span class="clearfix"></span>
						</div>
					</div>	
				</div>

				<div class="col-md-15">
					<div class="panel panel-brown">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4">
									<span class="glyphicon glyphicon-option-horizontal">&nbsp;</span> 
								</div>

								<div class="col-xs-8 home_labels">
									<span class="home_counter" id="wait"><?=$home_wait_counter?></span>
									<br>
									<span class="">Waiting</span>
								</div>
							</div>
						</div>

						<div class="panel-footer">
							<a href="javascript:;" onclick="registration(5)">
								<span class="pull-left">Show List</span>
								<span class="pull-right"><span class="glyphicon glyphicon-circle-arrow-right"></span></span>
							</a>
							<span class="clearfix"></span>
						</div>
					</div>	
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<span class="pull-left">Filter</span>
							<span class="pull-right toggle_filter glyphicon glyphicon-chevron-down" id="toggle_filter"></span>
							<span class="clearfix"></span>
						</div>

						<div class="panel-body" id="load_filter">
							<div class="row">
								<div class="col-md-4">
									<small>Sign Up Date</small>
									<input type="date" class="form-control input-sm" id="signup_from" value="" placeholder="MM/dd/YYYY">
								</div>

								<div class="col-md-4">
									<small>To</small>
									<input type="date" class="form-control input-sm" id="signup_to" value="" placeholder="MM/dd/YYYY">
								</div>

								<div class="col-md-4">
									<small>Status</small>
									<select class="form-control input-sm" id="status" onchange="status(this.value)" autocomplete="off">
										<option value="">--Select Status--</option>
										<option value="0">New</option>
										<option value="1">Outstanding</option>
										<option value="2">Confirmed</option>
										<option value="3">Rejected</option>
										<option value="4">Waiting</option>
									</select>
								</div>
							</div>

							<div class="row" id="process">
								<div class="col-md-4">
									<small>Process Date</small>
									<input type="date" class="form-control input-sm" id="process_from" value="" placeholder="MM/dd/YYYY">
								</div>

								<div class="col-md-4">
									<small>To</small>
									<input type="date" class="form-control input-sm" id="process_to" value="" placeholder="MM/dd/YYYY">
								</div>

								<div class="col-md-4">
									<small>Process By</small>
									<input type="text" class="form-control input-sm" id="process_by">
								</div>
							</div>

							<div class="row">
								<div class="col-md-4">
									<small>Project Area</small>
									<select class="form-control input-sm" id="company" onchange="get_proj(this.value)">
										<option value="">-- Select Project Area --</option>
											<?php foreach($proj_area as $row){ ?>
											<!-- <input type="text" name="" id="" class="form-control input-sm" placeholder="Company"> -->
											<option value="<?=$row['PROJ_ID']?>"><?=$row['PROJ_AREA']?></option>
											<?php } ?>
									</select>
								</div>

								<div class="col-md-4">
									<small>Project</small>
									<div id="load_project">
										<select id="project" class="form-control input-sm">
											<option value=''>-- Select Project --</option>
										</select>
									</div>
								</div>

								<div class="col-md-3">
									<small>Keyword</small>
									<input type="text" class="form-control input-sm" id="keyword" onkeyup="runScript(event)">
								</div>

								<div class="col-md-1">
									<small>&nbsp;</small>
									<button class="form-control btn btn-primary btn-sm input-sm" onclick="search()"><span class="glyphicon glyphicon-search"></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">	
				<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
				
				<div class="col-md-12" id="loader"></div>
			</div>
		</div>
		