<!-- Add roles page for retrieving roles info -->
<?php 
	foreach ($new as $row) {
		$rid = $row['ua_id'];
		$rname = $row['ua_rname'];
		$regview = $row['ua_regview'];
		$regconf = $row['ua_regconfirm'];
		$suppview = $row['ua_suppview'];
		$suppreply = $row['ua_suppreply'];
		$rptview = $row['ua_rptview'];
		$rptexpo = $row['ua_rptexport'];
		$mtc = $row['ua_mtcview'];
		$carview = $row['ua_carview'];
		$annview = $row['ua_annview'];
		$soaview = $row['ua_soaview'];
		$hoview = $row['ua_hoview'];
		$accnt = $row['ua_settings'];
		$usrview = $row['ua_usrview'];
		$usredit = $row['ua_usredit'];
		$usrdel = $row['ua_usrdel'];
		$usradd = $row['ua_usradd'];
		$roleadd = $row['ua_roleadd'];
		$roleedit = $row['ua_roleedit'];
		$roledel = $row['ua_roledel'];
		$concern = $row['ua_concern'];
		$inq = $row['ua_inq'];
		$req = $row['ua_req'];
	} 
?>
		<div class="panel panel-primary" id="viewrole">
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="icon-pencil"> </span> Edit Roles</div>
					<div class="col-md-2 col-md-offset-8">
						
					</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-body" id="editchecklist" style="display:none">
				<div class="row" id="editbody">
					<div class="col-md-12">
						<div class="col-md-3">
							<label> Role </label> <br/>
							<span><input type="text" class="form-control input-sm" value="<?=strtoupper($rname)?>" id="rolename" disabled></span>
						</div>
						<div class="col-md-4 col-md-offset-5 form-group" id="editbuttons">
							<div class="col-md-6">
							<button class="btn btn-sm btn-success form-control" onclick="updateroles(<?=$rid?>)"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Save</button>
							</div>
							<div class="col-md-6">
							<button class="btn btn-sm btn-danger form-control" onclick="cancelroleedit()"><span class="icon-cancel-circle"></span> &nbsp;Cancel</button>
							</div>
						</div>
					</div>
					<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-3">
								<label class="custom_label">Navigation</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Registration List</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="regview" value="1" <?php if($regview == 1){ echo "checked";} ?>> 
								<label for="regview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="regconf" value="1" <?php if($regconf == 1){ echo "checked";} ?>> 
								<label for="regconf">Confirm</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Support</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="suppview" value="1" <?php if($suppview == 1){ echo "checked";} ?>> 
								<label for="suppview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="suppreply" value="1" <?php if($suppreply == 1){ echo "checked";} ?>> 
								<label for="suppreply">Reply</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="suppcomm" value="1" <?php if($suppreply == 1){ echo "checked";} ?>> 
								<label for="suppcomm">Comment</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Reports</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="rptview" value="1" <?php if($rptview == 1){ echo "checked";} ?>> 
								<label for="rptview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="rptexpo" value="1" <?php if($rptexpo == 1){ echo "checked";} ?>> 
								<label for="rptexpo">Export</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Maintenance</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="mtc" value="1" <?php if($mtc == 1){ echo "checked";} ?>> 
								<label for="mtc">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Carousel</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="carview" <?php if($carview == 1){ echo "checked";} ?>> 
								<label for="carview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Announcements</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="annview" <?php if($annview == 1){ echo "checked";} ?>> 
								<label for="annview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; SOA</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="soaview" <?php if($soaview == 1){ echo "checked";} ?>> 
								<label for="soaview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Homeowners List</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="hoview" value="1" <?php if($hoview == 1){ echo "checked";} ?>> 
								<label for="hoview">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Account Settings</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="accnt" value="1" <?php if($accnt == 1){ echo "checked";} ?>> 
								<label for="accnt">View</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Users</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="usrview"  value="1" <?php if($usrview == 1){ echo "checked";} ?>>
								<label for="usrview">View</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="usredit" value="1" <?php if($usredit == 1){ echo "checked";} ?>> 
								<label for="usredit">Edit</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="usrdel" value="1" <?php if($usrdel == 1){ echo "checked";} ?>> 
								<label for="usrdel">Delete</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Add Users</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="usradd" value="1" <?php if($usradd == 1){ echo "checked";} ?>> 
								<label for="usradd">Add</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Roles</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="roleadd" value="1" <?php if($roleadd == 1){ echo "checked";} ?>> 
								<label for="roleadd">Add</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="roleedit" value="1" <?php if($roleedit == 1){ echo "checked";} ?>> 
								<label for="roleedit">Edit</label>
							</div>
							<div class="col-md-2">
								<input type="checkbox" id="roledel" value="1" <?php if($roledel == 1){ echo "checked";} ?>> 
								<label for="roledel">Delete</label>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
					</div>
					<!-- <div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Business Entity Assignment</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-3 col-md-offset-2 container-fluid">
							<div class="selector-box">
								<select id="selectedcompEnt" class="form-control input-sm" onchange="listproj2(<?=$roleID?>)">
									<option value="">-- Select Company --</option>
									<?php foreach($dbComp as $row => $tabla){ ?>
									<option value="<?=$row?>"><?=$tabla?></option>
									<?php } ?>
								</select>
								<div class="row-fluid" style="height:90%; padding-top:10px" id="loadedproj">
									<select multiple id='sourcebussEnt' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
										<p style="text-align: center; display: none;" id="load_sourcebussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="selection-box">
								<div class="col-md-6 col-md-offset-3" style="top:25%">
									<button class="btn btn-sm btn-default form-control" onclick="removed()" style="padding:5px; margin-top:3px"><</button>
									<button class="btn btn-sm btn-default form-control" onclick="add()" style="padding:5px; margin-top:3px">></button>
									<button class="btn btn-sm btn-default form-control" onclick="removeall()" style="padding:5px; margin-top:3px"><<</button>
									<button class="btn btn-sm btn-default form-control" onclick="addall()" style="padding:5px; margin-top:3px">>></button>
								</div>
							</div>
						</div>
						<div class="col-md-3 container-fluid">
							<div class="selector-box">
								<select id="selectedbussEnt" class="form-control input-sm" onchange="bussEnt(<?=$roleID?>)">
									<option value="">-- Select Company --</option>
 									<?php foreach($compArr as $row => $table){ ?>
									<option value="<?=$row?>"><?=$table?></option>
									<?php } ?> 
								</select>
								<div class="row-fluid" style="height:90%; padding-top:10px" id="loadselected">
									<select multiple id='resultbussEnt' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>
										<p style="text-align: center; display: none;" id="load_resultbussEnt"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
					</div> -->
					<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Receive E-mail Alert</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
						<div class="col-md-12">
							<div class="col-md-2 col-md-offset-2">
								<input type="checkbox" id="concern" value="1" <?php if($concern == 1){ echo "checked";} ?>> 
								<label for="concern">Concern</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="req" value="1" <?php if($req == 1){ echo "checked";} ?>> 
								<label for="req">Request</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="inq" value="1" <?php if($inq == 1){ echo "checked";} ?>> 
								<label for="inq">Inquiry</label>
							</div>
							<div class="col-md-12"> <hr> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>