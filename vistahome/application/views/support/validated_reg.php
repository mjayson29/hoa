<?php 	
	function clean($string) {
	    $string = str_replace('.', '', $string); // Replaces all spaces with hyphens.
	    return preg_replace('/[^A-Za-z0-9\- ]/', '', $string); // Removes special chars.
	}
 ?>
<?php if(!$buyer){ ?>
	
	<div class="panel panel-primary">

		<div class="panel-heading">Homeowner Registration Form</div>

		<div class="panel-body">
			
			<span>NO DATA</span>

		</div>

	</div>

<?php } else { ?>
	
	<?php foreach($buyer as $row){ ?>	

		<?php $signup = strtotime($row['bi_regdate']); $bdate = strtotime($row['bi_bdate']);
		$select=""; $table="ZHOA_unit"; $where="u_id = ".$row['bi_unit'];
		$get_unit = $this -> Main -> select_data_where($select, $table, $where);
			foreach($get_unit as $u){
				$unit = $u['u_desc'];
			}

		// $select=""; $table="ZHOA_concern_temp"; $where="ct_owner_id = ".$row['bi_owner_id'];
		// $get_concern = $this -> Main -> select_data_where($select, $table, $where);
		// $concern = "No Concern";
		// 	foreach($get_concern as $c){
		// 		$concern = $c['ct_desc'];
		// 	}
		$concern = "No Concern";
		if($row['bi_temp_concern'] != ""){
			$concern = $row['bi_temp_concern'];
		}
		$get_id = $row['bi_id'];
		
		$cust_no = $row['bi_cust_no'];
		$signup_date = date('M d, Y', $signup);
		$val_project_area = $row['PROJ_AREA'];
		$val_project = $row['bi_project'];
		$val_blklot = $row['bi_blklot'];
		$val_custname = ucwords($row['bi_fname']." ".$row['bi_mname']." ".$row['bi_lname']);
		$fname = ucwords($row['bi_fname']);
		$mname = ucwords($row['bi_mname']);
		$lname = ucwords($row['bi_lname']);
		$fullname = ucwords($fname." ".$lname);
	?>
		<?php if($row['bi_status']==1){ ?>
			<div class="panel panel-green">
				<div class="panel-heading">Confirmed</div>
			</div>
		<?php } if($row['bi_status']==2) { ?>
			<div class="panel panel-red">
				<div class="panel-heading">Rejected</div>
			</div>
		<?php } ?>

		<div class="panel panel-primary">

			<div class="panel-heading">Homeowner Registration Form</div>

			<div class="panel-body">

				<label class="custom_label">Customer Number</label>
				<span><?=$cust_no?></span>
				<br>
				<label class="custom_label">Sign Up Date</label>
				<span><?=$signup_date?></span>
				<br>
				<label class="custom_label">Project Location</label>
				<span><?=$val_project_area?></span>
				<br>
				<label class="custom_label">Unit type</label>
				<span><?=$unit?></span>
				<br>
				<label class="custom_label">Project Name</label>
				<span><?=$val_project?></span>
				<br>
				<label class="custom_label">Blk/Lot</label>
				<span><?=$val_blklot?></span>
				<br>

			</div>
			
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Personal Information</div>

			<div class="panel-body">

				<label class="custom_label">Customer Name</label>
				<span><?=$val_custname?></span>
				<br>
				<label class="custom_label">Birthdate</label>
				<span><?=date('M d, Y', $bdate)?></span>
				<br>
				<label class="custom_label">Address 1</label>
				<span><?=$row['bi_add1']?></span>
				<br>
				<label class="custom_label">Address 2</label>
				<span><?=$row['bi_add2']?></span>
				<br>
				<label class="custom_label">Zip Code</label>
				<span><?=$row['bi_zip']?></span>
				<br>

			</div>
			
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Contact Information</div>

			<div class="panel-body">

				<label class="custom_label">Contact Number</label>
				<span><?=$row['bi_contact']?></span>
				<br>
				<label class="custom_label">Email</label>
				<span><?=$row['bi_email']?></span>
				<br>

			</div>
			
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Security Questions</div>

			<div class="panel-body">

				<?php $select = ""; $table = "ZHOA_secquestion"; $where = "sq_id = '".$row['bi_sec1']."'"; ?>
				<?php $sec1 = $this -> Main -> select_data_where($select, $table, $where);?>
				<label class="custom_label"><strong>Question 1</strong></label>
				<?php foreach($sec1 as $row1){ ?>
					<span><?=$row1['sq_desc']?></span>
				<?php } ?>
				<br>
				<label class="custom_label"><strong>Answer 1</strong></label>
				<span><?=$row['bi_ans1']?></span>
				<br>
				<?php $select = ""; $table = "ZHOA_secquestion"; $where = "sq_id = '".$row['bi_sec2']."'"; ?>
				<?php $sec2 = $this -> Main -> select_data_where($select, $table, $where);?>
				<label class="custom_label"><strong>Question 2</strong></label>
				<?php foreach($sec2 as $row2){ ?>
					<span><?=$row2['sq_desc']?></span>
				<?php } ?>
				<br>
				<label class="custom_label"><strong>Answer 2</strong></label>
				<span><?=$row['bi_ans2']?></span>
				<br>

			</div>
			
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Concern</div>

			<div class="panel-body">
				<span><?=$concern?></span>
				<br>

			</div>
			
		</div>

	<?php } ?>

<?php } ?>

<?php if($row['bi_status']==2){ ?>
	<div class="panel panel-primary">
		
		<div class="panel-heading">Rejection Reason</div>
		<div class="panel-body">
			<?php if($row['bi_reason'] == ""){ ?>
				<span>No stated reason.</span>
			<?php } else { ?>
				<span><?=ucwords($row['bi_reason'])?></span>
			<?php } ?>
		</div>

	</div>
<?php } ?>

<div class="panel panel-primary">

	<div class="panel-heading">SAP Information related to property</div>

	<div class="panel-body">

		<?php $sap_matching = $this -> Buyer_Model -> get_sap_info($cust_no, $val_blklot); ?>
		<?php if(!$sap_matching){
			$project_area = "--";
			$project = "--";
			$blklot = "--";
			$customer_num = "--";
			$customer_name = "--";
			$note = "No matching SAP Property Information. Not valid for registration.";
			$notif = "remove\" style=\"color:red";
			$notif1 = "remove\" style=\"color:red";
			$notif2 = "remove\" style=\"color:red";
			$notif3 = "remove\" style=\"color:red";
			$notif4 = "remove\" style=\"color:red";
			$notif5 = "remove\" style=\"color:red";
			$notif6 = "remove\" style=\"color:red";
			$notif7 = "remove\" style=\"color:red";
			$customer_name = "--";
			$so = "";
			$house_model ="";
			$flr_area ="";
			$lot_area ="";
			$project_area ="";
			$project ="";
			$blklot ="";
			$customer_num ="";
			$customer_name ="";
			// $sap_fname = "--";
			// $sap_lname = "--";
			$customer_name = "--";

		} else { 
		
			$so = "";
			foreach($sap_matching as $sap){ 
				if($sap['ACTIVE'] == 1){
					$so .= ' '.$sap['VBELN'];
				}
				$house_model = $sap['ARKTX'];
				$flr_area = $sap['FLR_AREA'];
				$lot_area = $sap['LOT_SIZE'];
				$project_area = $sap['PROJ_AREA'];
				$project = $sap['XWETEXT'];
				$blklot = $sap['REFNO'];
				$customer_num = $sap['KUNNR'];
				$customer_name = $sap['NAME1'];	
				// $custname = explode(" ",$customer_name);

				$customer_name = trim(clean($customer_name)," ");
				$fullname = trim($val_custname," ");

				if($project_area != $val_project_area || $project != $val_project || $blklot != $val_blklot || $customer_num != $cust_no){// || $customer_name != $val_custname){
					$note = "Some SAP Property Information are not matched. Not valid for registration.";
					$notif5 = "remove\" style=\"color:red";
				} else {
					if($row['b_is_validated'] == 1 && $row['b_is_active'] == 1){
						$note = "Already validated. ";
						$notif5 = "ok\" style=\"color:green";
					} else {
						$note = "Valid for registration.";					
						$notif5 = "ok\" style=\"color:green";
					}
				}

				$customer_num == $cust_no ? $notif = "ok\" style=\"color:green" : $notif = "remove\" style=\"color:red";
				$project_area == $val_project_area ? $notif1 = "ok\" style=\"color:green" : $notif1 = "remove\" style=\"color:red";
				$project == $val_project ? $notif2 = "ok\" style=\"color:green" : $notif2 = "remove\" style=\"color:red";
				$blklot == $val_blklot ? $notif3 = "ok\" style=\"color:green" : $notif3 = "remove\" style=\"color:red";

				// $customer_name == $fullname ? $notif4 = 'ok' : $notif4 = 'remove';
				// $custname[1] == $mname ? $notif6 = 'ok' : $notif6 = 'remove';
				// $custname[2] == $lname ? $notif7 = 'ok' : $notif7 = 'remove';

				// $sap_fname = $custname[0];
				// $sap_lname = $custname[2];

			} 
		
		} ?>
		<div class="row">
			<div class="col-md-6">
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Homeowner Name</label>
					</div>
					<div class="col-md-8 nop4d">
						<span><?=$customer_name?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Customer Number</label>
					</div>
					<div class="col-md-8 nop4d">
						<span class="glyphicon glyphicon-<?=$notif?>"></span> <span><?=$customer_num?></span> 
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Project Area</label>
					</div>
					<div class="col-md-8 nop4d">
						<span class="glyphicon glyphicon-<?=$notif1?>"></span> <span><?=$project_area?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Project Name</label>
					</div>
					<div class="col-md-8 nop4d">
						<span class="glyphicon glyphicon-<?=$notif2?>"></span> <span><?=$project?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Blk / lot</label>
					</div>
					<div class="col-md-8 nop4d">
						<span class="glyphicon glyphicon-<?=$notif3?>"></span> <span><?=$blklot?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Note</label>
					</div>
					<div class="col-md-8 nop4d">
						<span class="glyphicon glyphicon-<?=$notif5?>"></span> <span><?=$note?></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">SO Number</label>
					</div>
					<div class="col-md-8 nop4d">
						<span><?=$so?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">House Model</label>
					</div>
					<div class="col-md-8 nop4d">
						<span><?=$house_model?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Floor Area</label>
					</div>
					<div class="col-md-8 nop4d">
						<span><?=$flr_area?></span>
					</div>
				</div>
				<div class="col-md-12 nop4d">
					<div class="col-md-4 nop4d">
						<label class="custom_label">Lot Area</label>
					</div>
					<div class="col-md-8 nop4d">
						<span><?=$lot_area?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>