<!-- default add roles page -->
<?php 
	$compArr = array();
	$projArr = array();
	$combo = array(); 
	foreach ($company as $key) { 
		$compArr[$key['BUKRS']] = $key['BUTXT']; 
		$projArr[$key['SWENR']] = $key['XWETEXT']; 
		$combo = array($key['BUKRS'] => array($key['SWENR'] => $key['XWETEXT'])); 
		$acc = explode("-", $access);
} ?>

<script type="text/javascript">
	$( document ).ready(function() {   
	<?php if($acc[13]==0){echo "$('#def_checklist').remove();";} ?> 
	    $('#loadroles').hide();
        $('#loading').css('display', 'block');         
        $.ajax({
            type:"get",
            url: "<?=base_url()?>support/load_roles/",
            success: function(data){
                $('#loadroles').html(data);
                $('#loadroles').css('display', 'block');    
                $('#loading').css('display', 'none');
            }
        });
    });
</script>

		<div class="panel panel-primary">
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="glyphicon glyphicon-user"></span> Users</div> 
					<div class="btn-group col-md-8 pull-right">
						<!-- <div class="col-md-1" style="float:right;padding-left:5px;padding-right:0px" onclick="slideSearch()">
							<button class="btn btn-xs input-sm btn-success form-control"><i class="glyphicon glyphicon-search"></i></button>
						</div>
						<div class="col-md-5" id="searchUser" style="float:right;display:none;padding-left:5px;padding-right:0px">
							<input type="texbox" id="userSearch" autocomplete="off" onblur="userSearchTxtBoxEvent()" onkeypress="uhandler(event)" class="input-sm form-control" placeholder="Search">
						</div> -->
					<?php if($acc[12]!=0){ ?>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px;">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/add_users'"><i class="icon-user-add"></i>&nbsp Add User</button>
						</div>
					<?php } ?>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/roles'"><i class="icon-key"></i>&nbsp Roles</button>
						</div>
						<div class="col-md-2" style="float:right;padding-left:5px;padding-right:0px">
							<button class="btn btn-xs input-sm btn-success form-control" onclick="window.location.href='<?=base_url()?>support/projects'"><i class="icon-clippy"></i>&nbsp Projects</button>
						</div>
					</div>
				<div class="clearfix"></div>
			</div>
		</div>

		<div id = "checklist"></div>

		<div class="panel panel-primary" id="def_checklist" >
			<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="icon-key"> </span> ROLES </div>
					<div class="col-md-2 col-md-offset-8" id="btnaddroles">
						<button class="btn btn-xs input-sm btn-success form-control" style="float:right" onclick="enablediv()"><i class="icon-key"></i>&nbsp Add Roles</button>
					</div>
				<div class="clearfix"></div>
			</div>

			<div class="panel-body" id="checklist-body" style="display:none">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3">
							<label> Role </label> <br/>
							<span><input type="text" class="form-control input-sm" value="" id="rolename1" disabled></span>
						</div>

						<div class="col-md-4 col-md-offset-5" style="display:none" id="buttons">
							<div class="col-md-6">
							<button class="btn btn-sm btn-success form-control" onclick="saverole()"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Save</button>
							</div>
							<div class="col-md-6">
							<button class="btn btn-sm btn-danger form-control" onclick="cancelroleadd()"><span class="icon-cancel-circle"></span> &nbsp;Cancel</button>
							</div>
						</div>
					</div>

					<div class="col-md-12" style="padding-top:20px" id="restrictions">
						<div class="col-md-12">
							<div class="col-md-3">
								<label class="custom_label">Navigation</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Registration List</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="regview1" value="" disabled name="check" />
								<label for="regview1">View</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="regconf1" value="" disabled name="check" /> 
								<label for="regconf1">Confirm</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Support</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="suppview1" value="" disabled name="check" />
								<label for="suppview1">View</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="suppreply1" value="" disabled name="check" /> 
								<label for="suppreply1">Reply</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="suppcomm1" value="" disabled name="check" /> 
								<label for="suppcomm1">Comment</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Reports</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="rptview1" value="" disabled name="check" />
								<label for="rptview1">View</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="rptexpo1" value="" disabled name="check" /> 
								<label for="rptexpo1">Export</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Maintenance</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="mtc1" value="" disabled name="check" />
								<label for="mtc1">View</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Carousel</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="carview1" value="" disabled name="check" /> 
								<label for="carview1">View</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Announcements</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="annview1" value="" disabled name="check" /> 
								<label for="annview1">View</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; SOA</label>
							</div>
							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="soaview1" value="" disabled name="check" /> 
								<label for="soaview1">View</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Homeowners List</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="hoview1" value="" disabled name="check" />
								<label for="hoview1">View</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Account Settings</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="accnt1" value="" disabled name="check" />
								<label for="accnt1">View</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label">Users</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="usrview1" onch value="" disabled name="check" /> 
								<label for="usrview1">View</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="usredit1" value="" disabled name="check" /> 
								<label for="usredit1">Edit</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="usrdel1" value="" disabled name="check" /> 
								<label for="usrdel1">Delete</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Add Users</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="usradd1" value="" disabled name="check" /> 
								<label for="usradd1">Add</label>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-3 col-md-offset-1">
								<label class="custom_label"> &nbsp;&nbsp;&nbsp; Roles</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="roleadd1" value="" disabled name="check" /> 
								<label for="roleadd1">Add</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="roleedit1" value="" disabled name="check" /> 
								<label for="roleedit1">Edit</label>
							</div>

							<div class="col-md-2">
								<input type="checkbox" id="roledel1" value="" disabled name="check" /> 
								<label for="roledel1">Delete</label>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>
					</div>

					<div class="col-md-12" style="padding-top:20px">
						<div class="col-md-12">
							<div class="col-md-6">
								<label >Receive E-mail Alert</label><br/>
							</div>
						</div>
						<div class="col-md-12"> <hr> </div>

						<div class="col-md-12">
							<div class="col-md-2 col-md-offset-2">
								<input type="checkbox" id="concern1" value="" disabled name="check" /> 
								<label for="concern1">Concern</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="req1" value="" disabled name="check" /> 
								<label for="req1">Request</label>
							</div>

							<div class="col-md-2 col-md-offset-1">
								<input type="checkbox" id="inq1" value="" disabled name="check" /> 
								<label for="inq1">Inquiry</label>
							</div>

							<div class="col-md-12"> <hr> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
			<div class = "col-md-12" id = "loadroles"></div>
		</div>
	</div>