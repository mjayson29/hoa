 <script type="text/javascript">
    function paginate(val){
        $('#loadroles').hide();
        $('#loading').show();

        $.ajax({
        type:"get",
        url: "<?=base_url()?>support/load_roles/"+val,
            success: function(data){
                $('#loadroles').html(data);
                $('#loadroles').slideDown('fast');
                $('#loading').hide();
            }
        });
    }
</script>
<?php
$acc = explode("-", $access);
?>
<div class="panel panel-primary">
	<div class="panel-heading">User Roles</div>

	<?php if(!$new){ ?>
		<div class="panel-body">
			<span>NO DATA</span>
		</div>
	<?php } else { ?>
		<div class="table-responsive">
			<table class="table">
				<tr style="background-color:#F3F3F3">
					<th>Role</th>
					<th>Updated By</th>
					<th>Date Updated</th>
					<th>Action</th>
				</tr>
				<?php foreach($new as $row){ ?>
					<?php
					$data['$role'] = $row['ua_rname'];
					$data['$regview'] = $row['ua_regview'];
					$data['$regconf'] = $row['ua_regconfirm'];
					$data['$suppview'] = $row['ua_suppview'];
					$data['$suppreply'] = $row['ua_suppreply'];
					$data['$rptview'] = $row['ua_rptview'];
					$data['$rptexpo'] = $row['ua_rptexport'];
						$dte = $row['ua_update'];
						$log = strtotime($dte);
						if (!$log) {
							$lastlog ="";
						} else {
							$lastlog = date('F d, Y', $log);
						}
					?>
					<tr class="data_click">
						<td style="width: 25%;"><?=strtolower($row['ua_rname'])?></td>
						<td style="width: 25%;"><?=ucwords(strtolower($row['ai_fname']." ".$row['ai_lname']))?></td>
						<td style="width: 25%;"><?=$lastlog?></td>
						<td style="width: 25%;">
						<?php if($acc[14]!=0) { ?>
							<span>
								<a onclick="edit_roles(<?=$row['ua_id']?>)" class="icon-pencil" title="Edit"></a>
							</span> 
						<?php } ?>
							&nbsp;&nbsp;&nbsp;
						<?php if($acc[14]!=0) { ?>
							<span>
								<a onclick="deleteroles(<?=$row['ua_id']?>)" class="icon-bin" style="color:reds" title="Delete"></a>
							</span>
						<?php } ?>
						</td>
					</tr>
				<?php } ?>
			</table>
		</div>
	<?php } ?>
</div>
<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>
<div style="text-align: center;"><?=$new_links?></div>
