<?php
	foreach ($grpComp as $asd) {
		$eu_uname = $asd['a_uname'];
		$eu_lname = $asd['ai_lname'];
		$eu_fname = $asd['ai_fname'];
		$eu_email = $asd['a_email'];
		$eu_spark = $asd['ai_spark'];
		$eu_permission = $asd['ur_permission'];
		$eu_rid = $asd['ai_role'];
	}
	$ui = explode("-", $eu_permission);
	$acc = explode("-", $access);
?>
<script type="text/javascript">
	$( document ).ready(function() {            
		$.ajax({
			type:"get",
			url: "<?=base_url()?>support/usrGrp/"+<?=$where?>,
			success: function(data){
				$('#bGrpTable').html(data);
				$('#bGrpTable').css('display', 'block');
				$('#addGrpBtn').css('display', 'block');
			}
		});
	});
</script>

				<div class="panel panel-primary" >
					<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;"><span class="icon-user-add"> </span>Edit User Info</div>
						<div class="clearfix"></div>
					</div>
					<div  id=""></div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3" style="margin-bottom:15px">
									<input type='hidden' id='nu_usrid' value='<?=$where?>'>
									<label for="nu_uname">Username</label>
									<input style="width:200%" id="nu_uname" value="<?=$eu_uname?>" type="text" maxlength="10" autocomplete="off" required onblur="checkusrname1('<?=$eu_uname?>')" class="form-control" placeholder="Username">
								</div>
								<div class="col-md-3" style="margin-bottom:15px">
									<div id="usrnamestatus" style="padding-top:25px"></div>
									<p style="text-align: center; display: none;" id="loadusr"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"> &nbsp;Verifying Username.</p>
								</div>
								<div class="col-md-6">
									<div class="col-md-12" style="float:right; margin-bottom:15px">
										<div class="col-md-6">
											<button class="btn btn-success btn-sm form-control" onclick="updateUsr('<?=$where?>')">Save</button>
										</div>
										<div class="col-md-6">
											<button class="btn btn-danger btn-sm form-control" onclick="window.location.href='<?=base_url()?>support/users'">Cancel</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-6" style="margin-bottom:15px">
									<label for="nu_lname">Last Name</label>
									<input id="nu_lname" value="<?=$eu_lname?>" type="text" class="form-control" autocomplete="off" required placeholder="Last Name">
								</div>
								<div class="col-md-6" style="margin-bottom:15px">
									<label for="nu_fname">First Name</label>
									<input id="nu_fname" value="<?=$eu_fname?>" type="text" class="form-control" autocomplete="off" required placeholder="First Name">
								</div>
							</div>
							<div class="col-md-12">
								<div class="col-md-6" style="margin-bottom:15px">
									<label for="nu_email">Email</label>
									<input id="nu_email" type="email" value="<?=$eu_email?>" class="form-control" autocomplete="off" required placeholder="Email Address">
								</div>
								<div class="col-md-6" style="margin-bottom:15px">
									<label for="nu_spark">Spark Username</label>
									<input id="nu_spark" type="text" value="<?=$eu_spark?>" class="form-control" autocomplete="off" required placeholder="Spark ID">
								</div>
							</div>
						</div>
					</div>
				</div>	
				<div class="panel panel-primary">
					<div class="panel-heading" style="padding: 5px 15px;">
				<div class="col-md-2" style="padding:5px 0px;">Edit User Role</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">
						<div class="row">		
							<div class="col-md-12" style="margin-top:25px">
								<div class="col-md-3">
									<select class="form-control input-sm" required autocomplete="off" onchange="list_roles(this.value)" id="selectedrole">
										<option value="0" autocomplete="off">-- Select Roles --</option>
										<?php foreach($role as $row){ ?>
										<?php $key = strtoupper($row['ua_rname']); ?>
										<option value="<?=$row['ua_id']?>" <?php if($eu_rid == $row['ua_id']){echo"selected";} ?>> <?=$key?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div id="display2" class="row">
							</div>
							<div id="display" class="row">
								<div class="col-md-12" style="padding-top:20px">
									<div class="col-md-12">
										<div class="col-md-3">
											<label class="custom_label">Navigation</label><br/>
										</div>
									</div>
									<div class="col-md-12"> <hr> </div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Registration List</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="regview" value="1" <?php if($ui[0]==1){echo"checked";} ?>> 
											<label for="regview">View</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="regconf" <?php if($ui[1]==1){echo"checked";} ?> value="1"> 
											<label for="regconf">Confirm</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Support</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="suppview" <?php if($ui[2]==1){echo"checked";} ?> value="1"> 
											<label for="suppview">View</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="suppreply" <?php if($ui[3]==1){echo"checked";} ?> value="1"> 
											<label for="suppreply">Reply</label>
										</div>

										<div class="col-md-2">
											<input type="checkbox" id="suppcomm" <?php if($ui[19]==1){echo"checked";} ?> value="1"> 
											<label for="suppcomm">Comment</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Reports</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="rptview" <?php if($ui[4]==1){echo"checked";} ?> value="1"> 
											<label for="rptview">View</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="rptexpo" <?php if($ui[5]==1){echo"checked";} ?> value="1"> 
											<label for="rptexpo">Export</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Maintenance</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="mtc" <?php if($ui[6]==1){echo"checked";} ?> value="1"> 
											<label for="mtc">View</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label"> &nbsp;&nbsp;&nbsp; Carousel</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="carview" <?php if($ui[20]==1){echo"checked";} ?> value="1"> 
											<label for="carview">View</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label"> &nbsp;&nbsp;&nbsp; Announcements</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="annview" <?php if($ui[21]==1){echo"checked";} ?> value="1"> 
											<label for="annview">View</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label"> &nbsp;&nbsp;&nbsp; SOA</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="soaview" <?php if($ui[22]==1){echo"checked";} ?> value="1"> 
											<label for="soaview">View</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Homeowners List</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="hoview" <?php if($ui[7]==1){echo"checked";} ?> value="1"> 
											<label for="hoview">View</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Account Settings</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="accnt" <?php if($ui[8]==1){echo"checked";} ?> value="1"> 
											<label for="accnt">View</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label">Users</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="usrview" <?php if($ui[9]==1){echo"checked";} ?> onch value="1">
											<label for="usrview">View</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="usredit" <?php if($ui[10]==1){echo"checked";} ?> value="1"> 
											<label for="usredit">Edit</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="usrdel" <?php if($ui[11]==1){echo"checked";} ?> value="1"> 
											<label for="usrdel">Delete</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label"> &nbsp;&nbsp;&nbsp; Add Users</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="usradd" <?php if($ui[12]==1){echo"checked";} ?> value="1"> 
											<label for="usradd">Add</label>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-3 col-md-offset-1">
											<label class="custom_label"> &nbsp;&nbsp;&nbsp; Roles</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="roleadd" <?php if($ui[13]==1){echo"checked";} ?> value="1"> 
											<label for="roleadd">Add</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="roleedit" <?php if($ui[14]==1){echo"checked";} ?> value="1"> 
											<label for="roleedit">Edit</label>
										</div>
										<div class="col-md-2">
											<input type="checkbox" id="roledel" <?php if($ui[15]==1){echo"checked";} ?> value="1"> 
											<label for="roledel">Delete</label>
										</div>
									</div>
									<div class="col-md-12"> <hr> </div>
								</div>
								<div class="col-md-12" style="padding-top:20px">
									<div class="col-md-12">
										<div class="col-md-6">
											<label >Receive E-mail Alert</label><br/>
										</div>
									</div>
									<div class="col-md-12"> <hr> </div>
									<div class="col-md-12">
										<div class="col-md-2 col-md-offset-2">
											<input type="checkbox" id="concern" <?php if($ui[16]==1){echo"checked";} ?> value="1"> 
											<label for="concern">Concern</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="req" <?php if($ui[17]==1){echo"checked";} ?> value="1"> 
											<label for="req">Request</label>
										</div>
										<div class="col-md-2 col-md-offset-1">
											<input type="checkbox" id="inq" <?php if($ui[18]==1){echo"checked";} ?> value="1"> 
											<label for="inq">Inquiry</label>
										</div>
										<div class="col-md-12"> <hr> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="bGrpTable"></div>
				
				<div id="addGroup"></div>

				<div id="editGroup"></div>
			</div>
		</div>
	</div>
</div>
