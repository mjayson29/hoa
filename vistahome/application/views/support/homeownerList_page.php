<script type="text/javascript">
	$( document ).ready(function() {
        $('#loader').hide();
        $('#loading').css('display', 'block'); 
        $.ajax({
            type:"post",
            url: "<?=base_url()?>support/homeowner_new/",
            success: function(data){
                $('#loading').css('display', 'none');
                $('#owners').html(data);
                $('#owners').slideDown('slow');
            }
        });
    });
</script>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span class="pull-left">Filter</span>
					<span class="pull-right toggle_filter glyphicon glyphicon-chevron-down" id="toggle_filter"></span>
					<span class="clearfix"></span>
				</div>
				<div class="panel-body" id="load_filter">
					<div class="col-md-12">
						<div class="col-md-4">
							<select class="form-control input-sm" autocomplete="off" id="company" onchange="get_proj(this.value); search_homeowner();">
								<option value="">-- Select Project Area --</option>
									<?php foreach($proj_area as $row){ ?>
									<option value="<?=$row['PROJ_ID']?>"><?=$row['PROJ_AREA']?></option>
									<?php } ?>
							</select>
						</div>
						<div class="col-md-4">
							<div id="load_project">
								<select id="project" class="form-control input-sm" autocomplete="off">
									<option value=''>-- Select Project --</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control input-sm" autocomplete="off" id="keyword" onkeyup="keyword(this.value)" onkeypress="handler(event)" placeholder="Search">
						</div>
					</div>
					<div class="col-md-12" id="search">
						<div class="col-md-2" style= "float:right;">
						<small>&nbsp;</small>
						<button class="form-control btn btn-primary btn-sm input-sm" onclick="search_homeowner()"><span class="glyphicon glyphicon-search"></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">	
		<p style="text-align: center; display: none;" id="loading"><img src="<?=base_url()?>assets/img/tools/loader1.gif" style="width: 20px;"></p>
		<div class="col-md-12" id="owners">
		</div>
	</div>
</div>

