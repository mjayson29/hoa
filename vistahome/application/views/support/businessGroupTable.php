<?php
$acc = explode("-", $access);
?>
				<div class="panel panel-primary">
					<div class="panel-heading" style="padding: 5px 15px;">
						<div class="col-md-2" style="padding:5px 0px;">Business Group</div>
							<div id="addGrpBtn" class="col-md-1 col-md-offset-9" style="display:none">
								<button class="btn btn-xs input-sm btn-success form-control" onclick="addGroup()" title="Add Another Group"><i class="glyphicon glyphicon-plus"></i></button>
							</div>
						<div class="clearfix"></div>
					</div>

					<?php if(!$grpTable){ ?>
						<div class="panel-body">
							<span>NO DATA</span>
						</div>
					<?php } else { ?>
						<div class="table-responsive">
							<table class="table">
								<tr style="background-color:#F3F3F3">
									<th>Business Group</th>
									<th>Added By</th>
									<th>Date Added</th>
									<th>Action</th>
								</tr>
								<?php foreach($grpTable as $row){ 
										$date = date('F d, Y', (strtotime($row['ar_added_date'])));
								?>
									<tr class="data_click" onclick="">
										<td style="width: 25%;"><?=ucwords(strtolower($row['rb_name']))?></td>
										<td style="width: 25%;"><?=strtolower($row['a_uname'])?></td>
										<td style="width: 25%;"><?=$date?></td>
										<td style="width: 25%;">
										<?php if($acc[11]!=0) { ?>
											<span>
												<a onclick="edit_usrgrp('<?=$usrid?>', '<?=$row['ar_grpid']?>')" class="icon-pencil" style="color:reds" title="Edit"></a>
											</span>
										<?php } ?>
											&nbsp;&nbsp;&nbsp;	
										<?php if($acc[11]!=0) { ?>	
											<span>
												<a onclick="del_usrgrp('<?=$usrid?>', '<?=$row['ar_grpid']?>')" class="icon-bin" style="color:reds" title="Delete"></a>
											</span>	
										<?php } ?>					
										</td> 
									</tr>
								<?php } ?>
							</table>
						</div>
					<?php } ?>
				</div>