<section class="grid_11" >
	<article class="content-fields">
		<div class="panel panel-default	">
			<div class="panel-heading user" style="padding:5px;">
				<label class="label label-primary stat-title"><span class="icon-notebook"></span> Registration :</label>
				<label class="label label-success stat"><a href="<?php echo base_url()?>admin/registration/" class="<?php if($isActive == "open"){echo"active";} ?>"><span class="icon-user-add"> </span> New</a></label>
				<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/out_reg/" class="<?php if($isActive == "close"){echo"active";} ?>"><span class="icon-file"> </span> Outstanding</a></label>
				<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/con_reg/" class="<?php if($isActive == "valid"){echo"active";} ?>"><span class="icon-check"> </span> Confirmed</a></label>
				<label class="label label-primary stat"><a href="<?php echo base_url()?>admin/rej_reg/" class="<?php if($isActive == "invalid"){echo"active";} ?>"><span class="icon-trash"> </span> Rejected</a></label>
			</div>
		</div>
		<form action="<?php echo base_url()?>admin/search/" method="POST">
		<table style="width: 100%;">
						<tr>
							<td width=>
							<span style="font-weight: bold;"> Filter By :</span>
							</td>
							<td width=>
							
							</td>
							<td>
							
							</td>
							<td>
							
							</td>
						</tr>
						<tr>
							<td style="padding: 2px;">
							Sign up Date 
							</td >
							<td style="padding: 2px;">
							<input type="text" placeholder="yyyy-mm-dd" class="form-control" style="width: 200px; display: inline;" id="signUpStartDate" name="signUpStartDate" value="<?php if(isset($signUpStartDate)){ echo $signUpStartDate;}?>"> To
							<input type="text" placeholder="yyyy-mm-dd" class="form-control" style="width: 200px; display: inline;" id="signUpEndDate" name="signUpEndDate" value="<?php if(isset($signUpStartDate)){ echo $signUpEndDate;}?>"> 
							<br>
							</td>
							<td style="padding: 2px;">
							Status &nbsp;
							</td>
							<td>

							<select name="statusSearch">
							    <option value="" <?php  if(isset($statusSearch) && $statusSearch == ''){ echo "";}?>>Please select status</option>
								<option value="0" <?php if(isset($statusSearch) && $statusSearch == '0'){ ?> selected="selected" <?php } else { echo "" ;} ?>>New</option>
								<option value="1" <?php if(isset($statusSearch) && $statusSearch == '1'){ ?> selected="selected" <?php } else { echo "" ;}?>>Outstanding</option>
								<option value="2" <?php if(isset($statusSearch) && $statusSearch == '2'){ ?> selected="selected" <?php } else { echo "" ;}?>>Confirmed</option>
								<option value="3" <?php if(isset($statusSearch) && $statusSearch == '3'){ ?> selected="" <?php } else { echo "" ;}?>>Rejected</option>
							</select></td>
						
						</tr>
						<tr>
							<td style="padding: 2px;">
							Process Date
							</td>
							<td style="padding: 2px;">
							<input type="text" placeholder="yyyy-mm-dd" class="form-control" style="width: 200px; display: inline;" id="processStartDate" name="processStartDate" value="<?php if(isset($processStartDate)){ echo $processStartDate;}?>"> To
							<input type="text" placeholder="yyyy-mm-dd" class="form-control" style="width: 200px; display: inline;" id="processEndDate" name="processEndDate" value="<?php if(isset($processEndDate)){ echo $processEndDate;}?>"> 
							</td>
							<td style="padding: 2px;">
							Process By
							</td>
							<td style="padding: 2px;">
							<input type="text" placeholder="" class="form-control" style="width: 200px; display: inline; float: left;" name="processBy" value="<?php if(isset($processBy)){ echo $processBy;}?>">
							</td>
						</tr>
						<tr>
							
							<td style="padding: 2px;">
							Company &nbsp;
							</td>
							<td style="padding: 2px;">
							<input type="text" placeholder="" class="form-control" style="width: 200px; display: inline;" name="company" value="<?php if(isset($company)){ echo $company;}?>">
							Project &nbsp; <input type="text" placeholder="" class="form-control" style="width: 200px; display: inline;" name="project" value="<?php if(isset($project)){ echo $project;}?>">
							</td>
							<td style="padding: 2px;">
							 Key &nbsp;
							</td>
							<td style="padding: 2px;">
							<input type="text" placeholder="" class="form-control" style="width: 200px; display: inline;" name="key" value="<?php if(isset($key)){ echo $key;}?>">
							</td>
						</tr>
						<tr>
							
							<td style="padding: 2px;">
							</td>
							<td style="padding: 2px;">
							</td>
							<td style="padding: 2px;">
							</td>
							<td style="padding: 5px;">
							<input type="submit" value="Search" class="btn btn-primary" style="float: left; width: 100%"> </td>
						</tr>
					</table>
					</form>
					<br>
		<div class="panel panel-default	">
			<div class="panel-heading" style="padding: 5px 3px 3px 3px;">
				<form method="post" action="<?=base_url()?>support/_tickets">
					<span style="font-weight: bold;">Registration List </span>
				</form>
			</div>
		</div>

		<?php //if(!$tickets){ echo "$notify<br/><h2  style='text-align: center;' class='notify'><span class='icon-notification'></span> No Result Found</h2>"; } else { ?>
		<?//=$notify?>
		<div class="panel panel-default" id="classification">

			<table class="table">

				<tr class="thead">
			    	<th>Sign Up Date</th>
			    	<th>Company</th>
			    	<th>Project</th>
			    	<th>Blk/Lot</th>
			    	<th>Unit/House Model</th>
			    	<th>Concern</th>
			    	<th>Status</th>
			    	<?php 
			    
			    	switch ($isActive){
			    		case "valid":
			    			?>
			    			<th>Process By</th>
			    			<th>Process Date</th>
			    			<?php 
			    			break;
			    		case "invalid":
			    			?>
			    			<th>Process By</th>
			    			<th>Process Date</th>
			    			<?php 
			    			break;
			    		case "":
			    			?>
			    			<th>Process By</th>
			    			<th>Process Date</th>
			    			<?php 
			    			break;
			    	}
			    	?>
			    	
			    </tr>	

			  
						
				    <?php 
				    $this->load->model('Buyer_Model');
				    foreach($buyerInfo->result() as $key){
				    		switch ($key->bi_status){
				    			case '0':
				    				if(substr($key->bi_regdate,0,10) == date("Y-m-d")){
				    					$isActive = "open";
				    					break;
				    				}else{
				    					$isActive = "close";
				    					break;
				    				}
				    			case '1':
				    				$isActive = "valid";
				    				break;
				    			case '2':
				    				$isActive = "invalid";
				    				break;
				    		}
				    	
				
				    ?>
					
				    <tr onclick="window.location.href='<?php echo base_url()?>admin/reg_info/<?php echo $isActive;?>/<?php echo $key->bi_id?>'">
				    	<td><?php $regdate = strtotime($key->bi_regdate); echo date('m/d/Y', $regdate) ; ?></td>
				    	<td><?php echo $key->tc_brand;?></td>
				    	<td>
				    	<?php 
				    	//echo $key->XWETEXT;
				    	if($this->uri->segment(2) != "search"){
				    		$projects = $this->Buyer_Model->get_projects_by_cust_no($key->bi_cust_no);
				    		if(!empty($projects)){
				    			foreach($projects->result() as $key2){
				    				echo $key2->XWETEXT;
				    				echo "<br>";
				    			}
				    		}
				    	}else{
				    		echo $key->XWETEXT;
				    	}
				    	
				    	?>
				    	</td>
				    	<td><?php echo $key->bi_blklot;?></td>
				    	<td><?php echo $key->bi_unit;?></td>
				    	<td>text<?//=$row['catdesc']?></td>
				    	<td>
				    	<?php 
				    	switch ($key->bi_status){
				    		case 0:
				    			
				    			if(substr($key->bi_regdate,0,10) == date("Y-m-d")){
				    				echo "NEW";
				    			}else{
				    				echo "OUTSTANDING";
				    			}
				    			break;
				    		case 1:
				    			echo "CONFIRMED";
				    			break;
				    		case 2:
				    			echo "REJECTED";
				    			break;
				    	}
				    	
				    	?></td>
				    	<?php 
				    		switch ($isActive){
					    		case "valid":
					    			?>
					    			<td><?php echo $key->a_uname;?></td>
					    		  	<td><?php $processDate = strtotime($buyerInfo->row()->bi_process_date); echo date('m/d/Y', $processDate) ;?></td>
						  			<?php 
					    			break;
					    		case "invalid":
					    			?>
					    			<td><?php echo $key->a_uname;?></td>
					    		  	<td><?php $processDate = strtotime($buyerInfo->row()->bi_process_date); echo date('m/d/Y', $processDate) ;?></td>
						  		<?php 
						  			break;
					    		}
				    			?>
				   </tr>

				    <?php  } ?>

			</table>

	    
		</div>
				
		        	<p style="float: left; margin-top:-4px;">Showing <?php echo $display_offset;?> to <?php echo $display_page;?> of <?php echo $total_pages;?> entries</p>
                	<?php echo $pages;?>
        
		
	
    
		<?php //} ?>
			
		<script type="text/javascript">

			$(function() {

			    $('h2.notify').hide().delay('500').slideDown('fast');
			    $('div#classification').hide().delay('50').slideDown('fast');
				//$('div.notification-false').hide().delay(3300).slideDown('slow').delay(2000);
						
			});

		</script>

	</article>	

	<!--script type="text/javascript">

		$(function() {

		    $('div#invalid').hide();
		    $('div#closed').hide();
		    $('div#classification').hide();
		    $('div#valid').hide();
										//$('div.notification-false').hide().delay(3300).slideDown('fast').delay(2000);							

		});		

		function Closed(){

			$('div#open').slideUp('slow');
			$('div#closed').slideDown('slow');
			$('div#classification').slideUp('slow');
			$('div#valid').slideUp('slow');
			$('div#invalid').slideUp('slow');


		}	

		function Open(){

			$('div#open').slideDown('slow');
			$('div#closed').slideUp('slow');
			$('div#classification').slideUp('slow');
			$('div#valid').slideUp('slow');
			$('div#invalid').slideUp('slow');

		}	

		function Valid(){

			$('div#valid').slideDown('slow');
			$('div#open').slideUp('slow');
			$('div#closed').slideUp('slow');
			$('div#classification').slideUp('slow');
			$('div#invalid').slideUp('slow');

		}	

		function Invalid(){

			$('div#invalid').slideDown('slow');
			$('div#open').slideUp('slow');
			$('div#closed').slideUp('slow');
			$('div#classification').slideUp('slow');
			$('div#valid').slideUp('slow');

		}	

	</script-->

	<article class="content-fields">

		<!-- <div class="grid_2 panel panel-default">

			<div class="panel-heading"><h3>Priority</h3></div>
			
			<div class="inner_grid_2 content-fields">

				<h4><span class="icon-notification" style="color: red;"></span> High </h4>
				<h4><span class="icon-flag" style="color: #428bca;"></span> Medium </h4>
				<h4><span class="icon-pushpin" style="color: #333;"></span> Low</h4>

			
			</div>

		</div> -->

	</article>

	<article class="content-fields">

		<!-- <div class="grid_6 panel panel-default">

			<div class="panel-heading"><h3>Status</h3></div>
			
			<div class="inner_grid_2 content-fields">

				<h4><span class="icon-check" style="color: #3e8f3e;"></span> Completed </h4>
				<h4><span class="icon-new" style="color: #428bca;"></span> New </h4>
				<h4><span class="icon-folder" style="color: #333;"></span> Closed</h4>

		
			</div>

			<div class="grid_3 content-fields">


					<h4><span class="icon-user2"></span> With User Reply </h4>
					<h4><span class="icon-users"></span> With Support Reply </h4>
					<h4><span class="icon-message"></span> Comment </h4>


			</div> -->

		</div>

	</article>

</section>


