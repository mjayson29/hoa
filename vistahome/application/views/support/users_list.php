<script type="text/javascript">
	function paginate(val){
  	$('#users').hide();
  	$('#loading').show();
    $.ajax({
    	type:"get",
    	url: "<?=base_url()?>support/users_list/"+val,
      success: function(data){
        $('#users').html(data);
        $('#users').slideDown('fast');
        $('#loading').hide();
      }
    });
  }
</script>
<?php
$acc = explode("-", $access);
?>
		<?php if(!$new){ ?>
		<div class="panel panel-primary" style="border-color:#d43f3a">
			<div class="panel-heading" style="background-color:#d9534f;border-color:transparent">
				<span class="panel-title pull-left">No Users Found!</span> 
				<div class="clearfix"></div>
			</div>
		</div>
		<?php } else { ?>
		<div class="panel panel-primary">
			<div class="table-responsive" style="border-radius:inherit">
				<table class="table" style="border-radius:inherit">
					<tr style="background-color:#337AB7;color:#fff">
						<th style="text-align:center;border-color:#337AB7">Username</th>
						<th style="text-align:center;border-color:#337AB7">Last Name</th>
						<th style="text-align:center;border-color:#337AB7">First Name</th>
						<th style="text-align:center;border-color:#337AB7">Updated By</th>
						<th style="text-align:center;border-color:#337AB7">Date Updated</th>
						<th style="text-align:center;border-color:#337AB7">Action</th>
					</tr>
				<?php foreach($new as $row){ ?>
				<?php $update = strtotime($row['ai_update']); ?>
					<tr class="data_click">
						<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=strtolower($row['a_uname'])?></td>
						<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=ucwords(strtolower($row['ai_lname']))?></td>
						<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=ucwords(strtolower($row['ai_fname']))?></td>
						<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=$row['uname2']?></td>
						<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 20%;"><?=date('F d, Y', $update)?></td> 
						<td style="width: 20%;text-align:center">
						<?php if($row['ai_id'] != 1 || $role == 1) { ?>
						<?php if($acc[10]!=0) { ?>
							<span>
								<a onclick="window.location.href='<?=base_url()?>support/edit_users/<?=$row['ai_id']?>'" class="icon-pencil" title="Edit"></a> 
							</span>
						<?php } ?>
							&nbsp;&nbsp;&nbsp;
						<?php if($acc[11]!=0) { ?>
							<span>
								<a onclick="del_usr('<?=$row['ai_id']?>', '<?=$row['a_uname']?>')" class="icon-bin" style="color:reds" title="Delete"></a>
							</span>	
						<?php } ?>						
						<?php } ?>
						</td> 
					</tr>
				<?php } ?>
				</table>
			</div>
		<?php } ?>
		</div>
	</div>
</div>
<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$new_links?></div>
