 <script type="text/javascript">

    function paginate(val){
        $('#users').hide();
        $('#loading').show();
  		var keyword = '<?php echo $keyword; ?>';
        var project = '<?php echo $project; ?>';
        var company = '<?php echo $company; ?>';
        console.log(keyword+" "+project+" "+company);
        $.ajax({
	        type:"post",
	        url: "<?=base_url()?>support/search_users/"+val,
	        data:{
	            company: company,
	            project: project,
	            keyword: keyword 
	        },
	        success: function(data){
	            $('#users').html(data);
	            $('#users').slideDown('fast');
	            $('#loading').hide();
	        }
        });
    }
</script>
<?php
$acc = explode("-", $access);
?>
		<?php if(!$search){ ?>
			<div class="panel panel-primary" style="border-color:#d43f3a">
				<div class="panel-heading" style="background-color:#d9534f;border-color:transparent">
					<span class="panel-title pull-left">No Results Found for [ Keyword: <i><?=$keyword?></i> ]</span> 
					<div class="clearfix"></div>
				</div>
			</div>
		<?php } else { ?>
			<div class="panel panel-primary" style="border-color:#4cae4c">
				<div class="panel-heading" style="background-color:#5cb85c;border-color:transparent">
					<span class="panel-title pull-left">Search results for [ Keyword: <i><?=$keyword?></i> ]</span> 
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="table-responsive" style="border-radius:inherit">
					<table class="table" style="border-radius:inherit">
						<tr style="background-color:#337AB7;color:#fff">
							<th style="text-align:center;border-color:#337AB7">Username</th>
							<th style="text-align:center;border-color:#337AB7">Last Name</th>
							<th style="text-align:center;border-color:#337AB7">First Name</th>
							<th style="text-align:center;border-color:#337AB7">Updated By</th>
							<th style="text-align:center;border-color:#337AB7">Date Updated</th>
							<th style="text-align:center;border-color:#337AB7">Action</th>
						</tr>
					<?php foreach($search as $row){ ?>
					<?php $update = strtotime($row['ai_update']); ?>
						<tr class="data_click" onclick="">
							<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=strtolower($row['a_uname'])?></td>
							<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=ucwords(strtolower($row['ai_lname']))?></td>
							<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=ucwords(strtolower($row['ai_fname']))?></td>
							<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=$row['uname2']?></td>
							<td onclick="window.location.href='<?=base_url()?>support/user_info/<?=$row['ai_id']?>'" style="width: 15%;"><?=date('F d, Y', $update)?></td> 
							<td style="width: 25%;text-align:center">
							<?php if($acc[10]!=0) { ?>
								<span>
									<a onclick="window.location.href='<?=base_url()?>support/edit_users/<?=$row['ai_id']?>'" class="icon-pencil" title="Edit"></a> 
								</span>
							<?php } ?> 
								&nbsp;&nbsp;&nbsp;
							<?php if($acc[10]!=0) { ?>
								<span>
									<a onclick="del_usr('<?=$row['ai_id']?>', '<?=$row['a_uname']?>')" class="icon-bin" style="color:reds" title="Delete"></a>
								</span>	
							<?php } ?>  
							</td> 
						</tr>
					<?php } ?>
					</table>
				</div>
		<?php } ?>
</div>
<?php echo "Showing ". $display_offset ." to ".$display_limit." of ". $total_pages . " entries";?>

<div style="text-align: center;"><?=$search_links?></div>
