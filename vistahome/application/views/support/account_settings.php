	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-primary">
				<div class="panel-heading"> Change Password </div>

				<div class="panel-body">
					<form method="POST" action="<?=base_url()?>support/account_settings">
						<div><?=$notification?></div>

						<div class="row">
							<div class="col-sm-6">
								<small>Current Password</small>
								<input type="password" class="form-control input-sm" name="cpword" required>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<small>New Password</small>
								<input type="password" class="form-control input-sm" name="npword" required>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<small>Retype New Password</small>
								<input type="password" class="form-control input-sm" name="rpword" required>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">
								<input type="submit" class="form-control input-sm btn btn-primary btn-sm" value="Submit" name="changepword">
							</div>

							<div class="col-sm-3">
								<input type="button" class="form-control input-sm btn btn-danger btn-sm" value="Cancel" onclick="window.location.href='<?=base_url()?>support/account_settings'">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
