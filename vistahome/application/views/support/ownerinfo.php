<?php if(!$buyer){ ?>
	
	<div class="panel panel-primary">

		<div class="panel-heading">Homeowner Registration Form</div>

		<div class="panel-body">
			
			<span>NO DATA</span>

		</div>

	</div>

<?php } else { ?>

	<?php foreach($buyer as $row){ ?>	
 
		<?php $signup = strtotime($row['bi_regdate']); $bdate = strtotime($row['bi_bdate']);
		$select=""; $table="ZHOA_unit"; $where="u_id = ".$row['bi_unit'];
		$get_unit = $this -> Main -> select_data_where($select, $table, $where);
			foreach($get_unit as $u){
				$unit = $u['u_desc'];
			}

		// $select=""; $table="ZHOA_concern_temp"; $where="ct_owner_id = ".$row['bi_owner_id'];
		// $get_concern = $this -> Main -> select_data_where($select, $table, $where);
		// $concern = "No Concern";
		// 	foreach($get_concern as $c){
		// 		$concern = $c['ct_desc'];
		// 	}
		$concern = "No Concern";
			if($row['bi_temp_concern'] != ""){
			$concern = $row['bi_temp_concern'];
		}

		$get_id = $row['bi_id'];
		$get_owner_id = $row['bi_owner_id'];
		
		$cust_no = $row['bi_cust_no'];
		$signup_date = date('M d, Y', $signup);
		$val_project_area = $row['PROJ_AREA'];
		$val_project = $row['bi_project'];
		$val_blklot = $row['bi_blklot'];

		$fname = $row['bi_fname'];
		$mname = $row['bi_mname'];
		$lname = $row['bi_lname'];

		$fullname = ucwords($lname.", ".$fname." ".$mname);
		$property = $val_blklot." ".$val_project;
		$uname = $row['b_uname'];
		$brand = $row['BUTXT'];

		$email_verif = $row['bi_email_verif'];
	?>

	<?php $sap_matching = $this -> Buyer_Model -> get_sap_info($cust_no, $val_blklot); ?>
		<?php if(!$sap_matching){

		} else { 

			$so = "";
			foreach($sap_matching as $sap){ 
				$so .= ' '.$sap['VBELN'];
				$house_model = $sap['ARKTX'];
				$flr_area = $sap['FLR_AREA'];
				$lot_area = $sap['LOT_SIZE'];
				$project_area = $sap['PROJ_AREA'];
				$project = $sap['XWETEXT'];
				$blklot = $sap['REFNO'];
				$customer_num = $sap['KUNNR'];
				$customer_name = ucwords($sap['NAME1']);	
				$move_in_temp = ($sap['MOVEIN_DATE']);
				$accept_temp = ($sap['ACCEPT_DATE']);
				$comp_temp = ($sap['COMP_DATE']);
				$cont = $sap['CONTRACTOR'];
			} 

			$get_move_in = "--";
			$get_accept = "--";
			$get_comp = "--";

			if($move_in_temp != 0){
				$get_move_in = date('m/d/Y', strtotime($move_in_temp));
			} 
			if($accept_temp != 0){
				$get_accept = date('m/d/Y', strtotime($accept_temp));
			}
			if($comp_temp != 0){
				$get_comp = date('m/d/Y', strtotime($comp_temp));
			}

		
		} ?>

		<div class="panel panel-primary">

			<div class="panel-heading">Account and Property Information</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Sign Up Date</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$signup_date?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Project Location</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$val_project_area?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Unit type</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$unit?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Project Name</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$val_project?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Blk/Lot</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$val_blklot?></span>
							</div>
						</div>
					</div>
					<div class="col-md-4 nop4d">
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">SO Number</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$so?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">House Model</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$house_model?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Floor Area</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$flr_area?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Lot Area</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$lot_area?></span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Move In Date</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$get_move_in?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">Acceptance Date</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$get_accept?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">House Completion Date</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$get_comp?></span>
							</div>
						</div>
						<div class="col-md-12 nop4d">
							<div class="col-md-6 nop4d">
								<label class="custom_label">House Contractor</label>
							</div>
							<div class="col-md-6 nop4d">
								<span><?=$cont?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Personal Information</div>

			<div class="panel-body">
				<label class="custom_label">Username</label>
				<span><?=$row['b_uname']?></span>
				<br>
				<label class="custom_label">Customer Name</label>
				<span><?=ucwords($row['bi_fname']." ".$row['bi_mname']." ".$row['bi_lname'])?></span>
				<br>
				<label class="custom_label">Birthdate</label>
				<span><?=date('M d, Y', $bdate)?></span>
				<br>
				<label class="custom_label">Address 1</label>
				<span><?=$row['bi_add1']?></span>
				<br>
				<label class="custom_label">Address 2</label>
				<span><?=$row['bi_add2']?></span>
				<br>
				<label class="custom_label">Zip Code</label>
				<span><?=$row['bi_zip']?></span>
				<br>

			</div>
			
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Contact Information</div>

			<div class="panel-body">

				<label class="custom_label">Contact Number</label>
				<span><?=$row['bi_contact']?></span>
				<br>
				<label class="custom_label">Email</label>
				<span><?=$row['bi_email']?></span>
				<br>

			</div>
			
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">Security Questions</div>

			<div class="panel-body">

				<?php $select = ""; $table = "ZHOA_secquestion"; $where = "sq_id = '".$row['bi_sec1']."'"; ?>
				<?php $sec1 = $this -> Main -> select_data_where($select, $table, $where);?>
				<label class="custom_label"><strong>Question 1</strong></label>
				<?php foreach($sec1 as $row1){ ?>
					<span><?=$row1['sq_desc']?></span>
				<?php } ?>
				<br>
				<label class="custom_label"><strong>Answer 1</strong></label>
				<span><?=$row['bi_ans1']?></span>
				<br>
				<?php $select = ""; $table = "ZHOA_secquestion"; $where = "sq_id = '".$row['bi_sec2']."'"; ?>
				<?php $sec2 = $this -> Main -> select_data_where($select, $table, $where);?>
				<label class="custom_label"><strong>Question 2</strong></label>
				<?php foreach($sec2 as $row2){ ?>
					<span><?=$row2['sq_desc']?></span>
				<?php } ?>
				<br>
				<label class="custom_label"><strong>Answer 2</strong></label>
				<span><?=$row['bi_ans2']?></span>
				<br>

			</div>
			
		</div>

		<?php } ?>

<?php } ?>

		