<!DOCTYPE html>
<html>
	<head>
		<title>404 Page Not Found</title>
		<link href="<?=base_url()?>assets/styles/css/error.css" rel="stylesheet">
		<link rel="shortcut icon" href="<?=base_url()?>assets/img/logos/icon1.png">
	</head>
	<body>
		<div class="figure">
			<img src="<?=base_url()?>assets/img/logos/hoa2.png" alt="Error: 404"/>
		</div>
		<div id="errorbox">
			<h1>Error 404</h1>Page Not Found. Looking for <a href="<?=base_url()?>">Vista HOMe Portal</a>?
		</div>
	</body>
</html>