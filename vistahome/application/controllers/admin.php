<?php
class Admin extends CI_Controller {
	
	function  __construct(){
		parent::__construct();
		date_default_timezone_set("Asia/Manila");
		$this->load->library('form_validation');
		$this -> load -> model('Admin_User_Model');
		$this -> load -> model('Buyer_Model');
		$this->load->library('session');
		
	}
	

function index(){
	
	 $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean');
	 $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|callback_check_admin');
	 $this->form_validation->set_error_delimiters('', '');
	 if($this->input->post('username') && $this->input->post('password')){
	 	if($this->form_validation->run() == TRUE) {
	 		//Go to private area
	 		redirect(base_url('admin/registration/'), 'refresh');
	 	
	 	}
	 }
	 $data['forgot_notif'] = "";
	
	 if(isset($_GET['a']) == 1){
	 	$data['forgot_notif'] = "<div id=\"err-container\" document.location.href='#'></div><div id=\"error-msg\"><h2 class=\"panel-heading\"><span class=\"icon-info\"> </span>Email Sent  <a href=\"#\" id=\"close1\" class=\"icon-cancel-circle link\">&nbsp;</a></h2><h3>New password has been set. Please check your email.</h3></div>";
	 }elseif(isset($_GET['a']) == 2){
	 	$data['forgot_notif'] = "<div id=\"err-container\" document.location.href='#'></div><div id=\"error-msg\"><h2 class=\"panel-heading\"><span class=\"icon-info\"> </span>Email Sent  <a href=\"#\" id=\"close1\" class=\"icon-cancel-circle link\">&nbsp;</a></h2><h3>Something went wrong with the server. Please try again.</h3></div>";
	 }
	 	 
	$this->load->view('admin/login_view',$data);
}

function check_admin($password) {

	//Field validation succeeded.  Validate against database
	$uname = $this -> input -> post('username');
	// 	$filtered = mysql_real_escape_string(stripslashes($uname)); -- deprecated

	//query the database
	$result = $this -> Admin_User_Model -> get_user($uname, $password,1,1);

	if($result) {

		$sess_array = array();

		foreach($result as $row) {

			$sess_array = array(
					'sess_id' => $row->a_id,
					'sess_uname' => $row->a_uname
			);

			$this->session->set_userdata('hoa_admin_accepted__', $sess_array);

		}

		return TRUE;

	} else  {

		$this->form_validation->set_message('check_admin', "Invalid username or password");
		return false;

	}

}

function registration(){
	
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	
	$data_session = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname']);
	$config['isActive'] = "open";
	
	$data_session['active'] = "registration";
	
	$this->load->library("pagination2");
	$keyword = "";
	if(isset($_POST['keyword'])){
		if($_POST['keyword'] != ""){
			$base_64 = base64_encode($_POST['keyword']);
			$keyword = rtrim($base_64, '=');
		}
	}
	if($this->uri->segment(4)){
		$keyword = $this->uri->segment(4);
	}
	$adjacent = '3';
	$base_url = base_url(). 'admin/registration/';
	$total_pages = $this->Buyer_Model->get_new_reg_rows()->num_rows();
	$page = 0;
	$limit = 10;
	if($this->uri->segment(3)){
		$page = $this->uri->segment(3);
		$offset = ($page - 1) * $limit;
	}else{
		$offset = '0';
	}
	
	$adjacent = '3';
	$config['buyerInfo']= $this->Buyer_Model->get_new_reg($limit,$offset,$keyword);
	$config['total_pages'] = $total_pages;
	
	$config['pages'] =  $this->pagination2->displaypagination($total_pages, $adjacent, $base_url, $page, $offset, $limit, $keyword);
	
	// pass the data to the view
	$config['display_offset'] = $offset + 1;
	if($page == '0'){
		$config['display_page'] = $limit;
		if($total_pages == '0'){
			$config['display_offset'] = 0;
		}
		if($limit > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}else if($total_pages == '0'){
		$config['display_offset'] = 0;
		$config['display_page'] = 0;
	}else if($limit > $total_pages){
		$config['display_page'] = $total_pages;
	}else{
		$config['display_page'] = $page * $limit;
		if($config['display_page'] > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}
	
	$this->load->view('admin/support-header');
	$this->load->view('admin/sidebar',$data_session);
	$this->load->view('admin/registration_view',$config);
	$this->load->view('admin/footer');
	

}

function out_reg(){
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	
	$data_session = array('sess_id' => $session_data['sess_id'],
			'sess_uname' => $session_data['sess_uname']);
	
	$data_session['active'] = "registration";
	
	$config['isActive'] = "close";
	
	$this->load->library("pagination2");
	$keyword = "";
	if(isset($_POST['keyword'])){
		if($_POST['keyword'] != ""){
			$base_64 = base64_encode($_POST['keyword']);
			$keyword = rtrim($base_64, '=');
		}
	}
	if($this->uri->segment(4)){
		$keyword = $this->uri->segment(4);
	}
	$adjacent = '3';
	$base_url = base_url(). 'admin/out_reg/';
	$total_pages = $this->Buyer_Model->get_out_reg_rows()->num_rows();
	$page = 0;
	$limit = 10;
	if($this->uri->segment(3)){
		$page = $this->uri->segment(3);
		$offset = ($page - 1) * $limit;
	}else{
		$offset = '0';
	}
	
	$adjacent = '3';
	$config['buyerInfo']= $this->Buyer_Model->get_out_reg($limit,$offset,$keyword);
	$config['total_pages'] = $total_pages;
	
	$config['pages'] =  $this->pagination2->displaypagination($total_pages, $adjacent, $base_url, $page, $offset, $limit, $keyword);
	
	// pass the data to the view
	$config['display_offset'] = $offset + 1;
	if($page == '0'){
		$config['display_page'] = $limit;
		if($total_pages == '0'){
			$config['display_offset'] = 0;
		}
		if($limit > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}else if($total_pages == '0'){
		$config['display_offset'] = 0;
		$config['display_page'] = 0;
	}else if($limit > $total_pages){
		$config['display_page'] = $total_pages;
	}else{
		$config['display_page'] = $page * $limit;
		if($config['display_page'] > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}
	
	$this->load->view('admin/support-header');
	$this->load->view('admin/sidebar',$data_session);
	$this->load->view('admin/registration_view',$config);
	$this->load->view('admin/footer');
}

function con_reg(){
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	
	$data_session = array('sess_id' => $session_data['sess_id'],
			'sess_uname' => $session_data['sess_uname']);
	
	$data_session['active'] = "registration";
	
	$config['isActive'] = "valid";
	
	$this->load->library("pagination2");
	$keyword = "";
	if(isset($_POST['keyword'])){
		if($_POST['keyword'] != ""){
			$base_64 = base64_encode($_POST['keyword']);
			$keyword = rtrim($base_64, '=');
		}
	}
	if($this->uri->segment(4)){
		$keyword = $this->uri->segment(4);
	}
	$adjacent = '3';
	$base_url = base_url(). 'admin/con_reg/';
	$total_pages = $this->Buyer_Model->get_con_reg_rows()->num_rows();
	$page = 0;
	$limit = 10;
	if($this->uri->segment(3)){
		$page = $this->uri->segment(3);
		$offset = ($page - 1) * $limit;
	}else{
		$offset = '0';
	}
	
	$adjacent = '3';
	$config['buyerInfo']= $this->Buyer_Model->get_con_reg($limit,$offset,$keyword);
	$config['total_pages'] = $total_pages;
	
	$config['pages'] =  $this->pagination2->displaypagination($total_pages, $adjacent, $base_url, $page, $offset, $limit, $keyword);
	
	// pass the data to the view
	$config['display_offset'] = $offset + 1;
	if($page == '0'){
		$config['display_page'] = $limit;
		if($total_pages == '0'){
			$config['display_offset'] = 0;
		}
		if($limit > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}else if($total_pages == '0'){
		$config['display_offset'] = 0;
		$config['display_page'] = 0;
	}else if($limit > $total_pages){
		$config['display_page'] = $total_pages;
	}else{
		$config['display_page'] = $page * $limit;
		if($config['display_page'] > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}
	
	$this->load->view('admin/support-header');
	$this->load->view('admin/sidebar',$data_session);
	$this->load->view('admin/registration_view',$config);
	$this->load->view('admin/footer');
}

function rej_reg(){
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	
	$data_session = array('sess_id' => $session_data['sess_id'],
			'sess_uname' => $session_data['sess_uname']);
	
	$data_session['active'] = "registration";
	
	$config['isActive'] = "invalid";
	
	$this->load->library("pagination2");
	$keyword = "";
	if(isset($_POST['keyword'])){
		if($_POST['keyword'] != ""){
			$base_64 = base64_encode($_POST['keyword']);
			$keyword = rtrim($base_64, '=');
		}
	}
	if($this->uri->segment(4)){
		$keyword = $this->uri->segment(4);
	}
	$adjacent = '3';
	$base_url = base_url(). 'admin/rej_reg/';
	$total_pages = $this->Buyer_Model->get_rej_reg_rows()->num_rows();
	$page = 0;
	$limit = 10;
	if($this->uri->segment(3)){
		$page = $this->uri->segment(3);
		$offset = ($page - 1) * $limit;
	}else{
		$offset = '0';
	}
	
	$adjacent = '3';
	$config['buyerInfo']= $this->Buyer_Model->get_rej_reg($limit,$offset,$keyword);
	$config['total_pages'] = $total_pages;
	
	$config['pages'] =  $this->pagination2->displaypagination($total_pages, $adjacent, $base_url, $page, $offset, $limit, $keyword);
	
	// pass the data to the view
	$config['display_offset'] = $offset + 1;
	if($page == '0'){
		$config['display_page'] = $limit;
		if($total_pages == '0'){
			$config['display_offset'] = 0;
		}
		if($limit > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}else if($total_pages == '0'){
		$config['display_offset'] = 0;
		$config['display_page'] = 0;
	}else if($limit > $total_pages){
		$config['display_page'] = $total_pages;
	}else{
		$config['display_page'] = $page * $limit;
		if($config['display_page'] > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}
	
	$this->load->view('admin/support-header');
	$this->load->view('admin/sidebar',$data_session);
	$this->load->view('admin/registration_view',$config);
	$this->load->view('admin/footer');
}

function reg_info($nav_flag, $id){
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	$data_session = array('sess_id' => $session_data['sess_id'],
			'sess_uname' => $session_data['sess_uname']);
	
	$data_session['active'] = "registration";
	
	$id = $this->uri->segment(4);
	$nav_flag = $this->uri->segment(3);
	$data['isActive'] = $nav_flag;
	$data['buyerInfo'] = $this->Buyer_Model->get_buyer_by_id($id);
	
	$status = $this->uri->segment(5);
	$data['notification'] = "";
	$process_by = $session_data['sess_id'];
	$process_date = date("Y-m-d h:i:s");
	$email = $this->input->post('email');
	if(isset($status) && $status == "1"){
		$message = "Registration has been successfully confirmed. You can now login your account.";
		$reason = "";
			if($this->send_email($email, $message, 'Account Registration')){
				$this->Buyer_Model->updateStatus($id,$status,$reason,$process_by,$process_date);
				$data['notification'] = "<div class='notification-true'> Registration has been successfully confirmed.</div>";
			}else{
				$data['notification'] = "<div class='notification-false'> Something went wrong with the server. Please try again.</div>";
			}
		
		}elseif(isset($status) && $status == "2"){
		$reason = $this->input->post('reason');
		$message = "We're sorry, your registration has been rejected. Reason : ".$reason;
		if($this->send_email($email, $message, 'Account Registration')){
			$data['notification'] = "<div class='notification-true'> Registration has been rejected.</div>";
			$this->Buyer_Model->updateStatus($id,$status,$reason,$process_by,$process_date);
		}else{
			$data['notification'] = "<div class='notification-false'> Something went wrong with the server. Please try again.</div>";
		}
		
	}
	
	$this->load->view('admin/support-header');
	$this->load->view('admin/sidebar',$data_session);
	$this->load->view('admin/reg_info_view',$data);
	$this->load->view('admin/footer');

}

function logout(){
	 $this->session->sess_destroy();  
	 redirect(base_url('admin/'), 'refresh');
}

function check_access(){
	
		if(!$this->session->userdata('hoa_admin_accepted__')){
			redirect(base_url('admin/'));
			exit;
		}
	
}

function search(){
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	
	$data_session = array('sess_id' => $session_data['sess_id'],
			'sess_uname' => $session_data['sess_uname']);
	
	$config['isActive'] = "";
	
	$data_session['active'] = "registration";
	
	$this->load->library("pagination2");
	
	if($_POST){
		$sign_up_date_start = $this->input->post('signUpStartDate');
		$sign_up_date_end = $this->input->post('signUpEndDate');
		$process_date_start = $this->input->post('processStartDate');
		$process_date_end = $this->input->post('processEndDate');
		$company = $this->input->post('company');
		$project = $this->input->post('project');
		$statusSearch = $this->input->post('statusSearch');
		$processBy = $this->input->post('processBy');
		$key = $this->input->post('key');
		
		
		$search_array = array('signUpStartDate' => $sign_up_date_start,
							  'signUpEndDate' => $sign_up_date_end,
							  'processStartDate' => $process_date_start,
							  'processEndDate' => $process_date_end,
							  'company' => $company,
							  'project' => $project,
							  'statusSearch' => $statusSearch,
							  'processBy' => $processBy,
							  'key' => $key
				);
		
		$this->session->set_userdata('hoa_reg_search__', $search_array);
	}else{
		$search_data = $this->session->userdata('hoa_reg_search__');
		$sign_up_date_start = $search_data['signUpStartDate'];
		$sign_up_date_end = $search_data['signUpEndDate'];
		$process_date_start = $search_data['processStartDate'];
		$process_date_end = $search_data['processEndDate'];
		$company = $search_data['company'];
		$project = $search_data['project'];
		$statusSearch = $search_data['statusSearch'];
		$processBy = $search_data['processBy'];
		$key = $search_data['key'];
	}
	
	$config['signUpStartDate'] = $sign_up_date_start;
	$config['signUpEndDate'] = $sign_up_date_end;
	$config['processStartDate'] = $process_date_start;
	$config['processEndDate'] = $process_date_end;
	$config['company'] = $company;
	$config['project'] = $project;
	$config['statusSearch'] = $statusSearch;
	$config['processBy'] = $processBy;
	$config['key'] = $key;
	
	$keyword = "";
	if(isset($_POST['keyword'])){
		if($_POST['keyword'] != ""){
			$base_64 = base64_encode($_POST['keyword']);
			$keyword = rtrim($base_64, '=');
		}
	}
	if($this->uri->segment(4)){
		$keyword = $this->uri->segment(4);
	}
	
	$adjacent = '3';
	$base_url = base_url(). 'admin/search/';
	$total_pages = $this->Buyer_Model->get_searh_reg_rows($sign_up_date_start, $sign_up_date_end, $process_date_start, $process_date_end, $company, $project, $statusSearch, $processBy, $key)->num_rows();
	$page = 0;
	$limit = 10;
	if($this->uri->segment(3)){
		$page = $this->uri->segment(3);
		$offset = ($page - 1) * $limit;
	}else{
		$offset = '0';
	}
	
	$config['buyerInfo']= $this->Buyer_Model->get_searh_reg($limit,$offset,$sign_up_date_start,$sign_up_date_end, $process_date_start, $process_date_end, $company, $project, $statusSearch, $processBy, $key);
	$config['total_pages'] = $total_pages;
	
	$config['pages'] =  $this->pagination2->displaypagination($total_pages, $adjacent, $base_url, $page, $offset, $limit, $keyword);
	
	// pass the data to the view
	$config['display_offset'] = $offset + 1;
	if($page == '0'){
		$config['display_page'] = $limit;
		if($total_pages == '0'){
			$config['display_offset'] = 0;
		}
		if($limit > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}else if($total_pages == '0'){
		$config['display_offset'] = 0;
		$config['display_page'] = 0;
	}else if($limit > $total_pages){
		$config['display_page'] = $total_pages;
	}else{
		$config['display_page'] = $page * $limit;
		if($config['display_page'] > $total_pages){
			$config['display_page'] = $total_pages;
		}
	}
	
	$this->load->view('admin/support-header');
	$this->load->view('admin/sidebar',$data_session);
	$this->load->view('admin/registration_view',$config);
	$this->load->view('admin/footer');
}

function account_settings(){
	$this->check_access();
	
	$session_data = $this->session->userdata('hoa_admin_accepted__');
	
	$data_session = array('sess_id' => $session_data['sess_id'],
			'sess_uname' => $session_data['sess_uname']);
	
	$data_session['active'] = "settings";
	
	$data['title'] = "Account Settings";
	$data['notification'] = "";
	$this->form_validation->set_rules('pword', 'Old Password', 'required|trim|xss_clean|callback_check_password');
	$this->form_validation->set_rules('npword', 'New Password', 'required|trim|xss_clean|callback_check_new_password');
	$this->form_validation->set_rules('cpword', 'Confirm Password', 'required|trim|xss_clean');
	
	if($_POST){
		if($this->form_validation->run() == TRUE) {
				$this->Admin_User_Model->update_password($session_data['sess_id'], $this->input->post('npword'));
				$data['notification'] = "<div class='notification-true'> Password has been successfully changed.</div>";
				
		}
	}
	
	$this -> load -> view('admin/support-header');
	$this -> load -> view('admin/sidebar', $data_session);
	$this -> load -> view('admin/account_settings_view',$data);
	$this -> load -> view('templates/footer');
}

function check_password($old_password) {

	$session_data = $this->session->userdata('hoa_admin_accepted__');
	$session_id = $session_data['sess_id'];
	$session_uname = $session_data['sess_uname'];
	//query the database
	$result = $this -> Admin_User_Model -> get_user($session_uname, $old_password,1,1);

	if($result) {

		return TRUE;

	} else  {

		$this->form_validation->set_message('check_password', "Old Password is incorrect.");
		return false;

	}

}

function check_new_password($new_password) {

	$confirm_password = $this->input->post('cpword');
	
	if($new_password != $confirm_password){
		$this->form_validation->set_message('check_new_password', "Password does not match the confirm password.");
		return false;
	}else{
		return true;
	}
}

function test(){
			$email  = "schristianangelo@gmail.com";	
			// $email  = "christianangelo_santos@mandalay.com.ph";	
						$subject = "Vista HOMe: Verify your Email Address – Response Required";
						$message = $this -> load -> view('signup/signup_email');
								;

						$this -> send_email($email, $message, $subject);
		}


function send_email($email, $message, $subject){
	$this->load->library('email');
	
	$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => '192.168.7.83',
			'smtp_user' => '',
			'smtp_pass' => '',
			'smtp_port' => 25,
			'crlf' => "\r\n",
			'newline' => "\r\n",
			'mailtype' => 'html'
	));
	
	$this->email->from('noreply@vistahome.com.ph', "Vista HOMe Portal");
	$this->email->to($email);
	$this->email->subject($subject);
	$this->email->message($message);
	if($this->email->send()){
		return true;
	}else{
		return false;
	}
	
}

function forgot_password(){
	
	$this->form_validation->set_rules('email', 'Email', 'required');
	$email = $this->input->post('email');
	if($_POST){
		if($this->form_validation->run() == TRUE) {
			
			$data['user_info'] = $this->Admin_User_Model->get_user_by_email($email);
			$data['new_password'] = hash("adler32",date('Y-m-d H:i:s'.'uNiqU3'));
			$message = $this->load->view('admin/forgot_password_temp',$data,true);
			if($this->send_email($email, $message, 'HOA Password Reset')){
				$this->Admin_User_Model->update_password($data['user_info']->row()->a_id,$data['new_password']);
				redirect(base_url('admin/?a=1'), 'refresh');
			}else{
				redirect(base_url('admin/?a=2'), 'refresh');
			}
			
			
			
			
		}
	}
	

	
	
	
	
	
	
}



}

?>