<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Test extends CI_Controller{

		function index(){

			$this -> load -> view('test');

		}

		function upload(){
		
			$config['upload_path'] = base_url().'assets/test/';
			$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg';
			$config['max_size']	= '3000';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload()) {

				$error = array('error' => $this->upload->display_errors());
				echo $error['error'];
			} else {
				$data = $this->upload->data();
				echo "<img src=".base_url()."assets/test/".$data['file_name']." />";
				echo $data['file_name'];
				
			}
		}

		function calendar(){

			$this->load->library('calendar');

			$data['calendar'] = $this->calendar->generate();

		}

	}