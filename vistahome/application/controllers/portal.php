<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	session_start();
	
	class Portal extends CI_Controller {

		function  __construct(){

			parent::__construct();
			date_default_timezone_set("UTC"); 
			$this -> load -> library('email');
		    $this -> load -> model('Main','',TRUE);
		    $this -> load -> helper('text');
			$this -> load -> library("pagination");
			$this -> load -> model('Buyer_Model');

		}

		function index(){

			if($this->session->userdata('hoa_user_accepted__')) {

				$this -> home();
			
			} else {

			 	redirect(base_url('login/login'), 'refresh');

			}

		}

		function home(){

			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['profimg'] = $session_data['sess_img'];

			    $data['active'] = "myHome";
			    // $data['active'] = "contacts";

			    $select = "SWENR";
			    $table = "ZHOA_byrinfo as a";
			    $join1 = "ZHOA_BUYER_PROF as b";
			    $join2 = "a.bi_cust_no = b.KUNNR";
			    $where = "a.bi_owner_id = ".$data['id'];
			    $orderby = "a.bi_owner_id ASC";

			    $getclient = $this->Main->get_data_where_join($select, $table, $orderby, $join1, $join2, $where);
			    foreach ($getclient as $el) {
				    $select = "";
					$table = "ZHOA_announcements as a";
					$orderby = "a.a_dposted ASC";
					$join1 = "ZHOA_admlogin as b";
					$join2 = "a.a_postedby = b.a_id";
					$where = "a.a_is_deleted = 0";
				    $offset = 0;
				    $limit = 3;
				    $proj = $el['SWENR'];
				    $data['get_announcements'] = $this -> Buyer_Model -> get_client_announcement($proj, $offset, $limit,$data['id']);
			    }

			    $select=""; 
			    $table="ZHOA_logs"; 
			    $where="tl_username ='".$session_data['sess_uname']."'";
			    $logs = $this -> Main -> count_select_where($select, $table, $where);

			    if(!$logs){} else {

				    $limit = $logs-1;
			    	
			    }

			    $select=""; 
			    $table="ZHOA_logs"; 
			    $where="tl_username ='".$session_data['sess_uname']."'"; 
			    $orderby = "tl_id ASC"; 
			    $start = 0;
			    $data['logs'] = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start);

			    $select="bi_cust_no"; 
			    $table="ZHOA_byrinfo"; 
			    $where="bi_owner_id = ".$session_data['sess_id'];
			    $get_owner = $this -> Main -> select_data_where($select, $table, $where);

				foreach($get_owner as $row){

					$cust_no = $row['bi_cust_no'];

				}

			    $select = "KUNNR, SWENR, REFNO, ARKTX, FLR_AREA, LOT_SIZE, VBELN, ID "; 
			    $table = "ZHOA_BUYER_PROF"; 
			    $where = "KUNNR =".$cust_no." AND ACTIVE = 1 "; 
			    $orderby = "KUNNR ASC";
			    $data['get_property'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
			  
			    $this->load->library('calendar');
				$data['calendar'] = $this->calendar->generate();
			    $this -> load -> view('templates/app-header', $data);
			    $this -> load -> view('pages/home', $data);
				$this -> load -> view('templates/footer');

			    // $this -> load -> view('templates/footer');
			
			} else {

				//If no session, redirect to login page
			 	redirect(base_url('login/login'), 'refresh');
				//show_404();

			}

		}

			function properties(){

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    $data['profimg'] = $session_data['sess_img'];

				    $data['active'] = "myProperty";

				    $select="bi_cust_no"; 
				    $table="ZHOA_byrinfo"; 
				    $where="bi_owner_id = ".$session_data['sess_id'];
				    $get_owner = $this -> Main -> select_data_where($select, $table, $where);

					foreach($get_owner as $row){

						$cust_no = $row['bi_cust_no'];

					}

				    $data['active'] = "myProperty";

					$select = "ID, KUNNR, SWENR, REFNO, ARKTX, FLR_AREA, LOT_SIZE, VBELN, PROP_IMG1, PROP_IMG2, ID"; 
					$table = "ZHOA_BUYER_PROF"; 
					$where = "KUNNR =".$cust_no." AND ACTIVE = 1 "; 
					$orderby = "KUNNR ASC";
				    $data['get_property'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

	   				$this -> load -> view('templates/app-header', $data);
				    $this -> load -> view('pages/properties', $data);
				    $this -> load -> view('templates/footer');
				
				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();

				}

			}

				function single_properties($SO=NULL){

					if($this->session->userdata('hoa_user_accepted__')) {

						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
					    $data['uname'] = $session_data['sess_uname'];
					    $data['profimg'] = $session_data['sess_img'];

						$select = "KUNNR, SWENR, REFNO, ARKTX, FLR_AREA, LOT_SIZE, VBELN, PROP_IMG1, PROP_IMG2, ID"; 
						$table = "ZHOA_BUYER_PROF"; 
						$where = "VBELN ='".$SO."' AND ACTIVE = 1 "; // $orderby = "KUNNR ASC";
					    $data['get_property'] = $this -> Main -> select_data_where($select, $table, $where);
					  
					    $data['active'] = "myProperty";

					    $select = ""; 
					    $table = "ZHOA_prop_attachments";
						$where = "pa_owner_id = ".$data['id']." AND pa_is_deleted = 0"; 
					    $orderby = "pa_date_file DESC";
						$data['attachments'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

					    $this -> load -> view('templates/app-header', $data);
					    $this -> load -> view('pages/single_properties', $data);
					    $this -> load -> view('templates/footer');

					} else {

						//If no session, redirect to login page
					 	redirect(base_url('login/login'), 'refresh');
						//show_404();

					}

				}

				function request_upload(){
					$config['upload_path'] = './assets/concerns/';
					$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg|xls|xlsx';
					$config['max_size']	= '2097152'; // 2MB 

					$this->load->library('upload', $config);
					if ( $this->upload->do_upload()) {
						$upload_data = $this->upload->data();
					}
				}


				function upload_prop_r($id = NULL){

					if($id == NULL){
						show_404();
					} else {
			
						$config['upload_path'] = './assets/img/properties/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '3000';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';

						$this->load->library('upload', $config);

						$select = "PROP_IMG1"; 
						$table = "ZHOA_BUYER_PROF"; 
						$where = "ID = '".$id."' AND ACTIVE = 1 ";
						$propimg = $this -> Main -> select_data_where($select, $table, $where);

						foreach($propimg as $row){ $filename = $row['PROP_IMG1']; }
						if($filename != "") { $filename = $row['PROP_IMG1']; } else { $filename = "no-image.jpg";}

						if ( ! $this->upload->do_upload()) {
					        echo "<img src=\"".base_url()."assets/img/properties/".$filename."\" width=\"100%\" class=\"img-thumbnail\">";
					        $error = array('error' => $this->upload->display_errors());
							echo "<span style='z-index: 9999px; position: relative;'>".$error['error']."</span>";
							// echo $error['error'];
						} else {
							$upload_data = $this->upload->data();
							if($filename!="no-image.jpg"){
								if(file_exists(APPPATH."../assets/img/properties/".$filename)){
							 	// if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$filename)){
									unlink($config['upload_path'].$filename);
								}
							}
							$where = "ID"; $table = "ZHOA_BUYER_PROF";
							$data = array('PROP_IMG2' => $upload_data['file_name']);
							$this -> Main -> update_data($where, $table, $id, $data);
							echo "<img src=\"".base_url()."assets/img/properties/".$upload_data['file_name']."\" width=\"100%\" class=\"img-thumbnail\">";
							echo "<input type='hidden' id='prop_img_r' value='".$upload_data['file_name']."'>";
						}
					}
				}

				function prop_img_def_l($id = NULL){
					$img = $this->input->post('curr_img');
					$path = APPPATH."../assets/img/properties/".$img;
					// $path = $_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$img;
					if(file_exists($path)){
						unlink($path);
					}
					$where = "ID"; 
					$table = "ZHOA_BUYER_PROF";
					$data = array( 'PROP_IMG1' => "no-image.jpg" );
					$this -> Main -> update_data($where, $table, $id, $data);
					echo "<img src=\"".base_url()."assets/img/properties/no-image.jpg"."\" width=\"100%\" class=\"img-thumbnail\">";
				}

				function dP_def($id = NULL){
					$img = $this->input->post('curr_img');
					$path = '/home/devhoavistalandcomph/assets/img/users/'.$img;
					// $path = $_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/users/".$img;
						if(file_exists($path)){
							unlink($path);
						}	
					$where = "bi_id"; $table = "ZHOA_byrinfo";
					$data = array('bi_profimg' => "user.png" );
					$this -> Main -> update_data($where, $table, $id, $data);
					echo "<img id=\"dP\" src=\"".base_url()."assets/img/users/user.png\" class=\"media-object mod_user\">
				                <input type=\"hidden\" id=\"prop_img\" value='user.png'>
                  				<label for=\"userfile\">
                    				<span class=\"glyphicon glyphicon-camera profile_image\"></span>
                  				</label>
                  				<span class=\"glyphicon glyphicon-remove profile_image_rm\"></span>";
				}

				function upload_prop_l($id = NULL){

					if($id == NULL){
						show_404();
					} else {
			
						$config['upload_path'] = './assets/img/properties/';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
						$config['max_size']	= '3000';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';

						$this->load->library('upload', $config);

						$select = "PROP_IMG1"; 
						$table = "ZHOA_BUYER_PROF"; 
						$where = "ID = '".$id."' AND ACTIVE = 1 ";
						$propimg = $this -> Main -> select_data_where($select, $table, $where);
						foreach($propimg as $row){ $filename = $row['PROP_IMG1']; }
						if($filename != "") { $filename = $row['PROP_IMG1']; } else { $filename = "no-image.jpg";}

						if ( ! $this->upload->do_upload()) {
							$error = array('error' => $this->upload->display_errors());
							echo $error['error'];
					        echo "<img src=\"".base_url()."assets/img/properties/".$filename."\" width=\"100%\" class=\"img-thumbnail\">";
						} else {
							$upload_data = $this->upload->data();
							if($filename!="no-image.jpg"){
								if(file_exists(APPPATH."../assets/img/properties/".$filename)){
							 	// if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$filename)){
									unlink($config['upload_path'].$filename);
								}
							}
							$where = "ID"; $table = "ZHOA_BUYER_PROF";
							$data = array('PROP_IMG1' => $upload_data['file_name']);
							$this -> Main -> update_data($where, $table, $id, $data);
							echo "<img src=\"".base_url()."assets/img/properties/".$upload_data['file_name']."\" width=\"100%\" class=\"img-thumbnail\">";
							echo "<input type='hidden' id='prop_img' value='".$upload_data['file_name']."'>";

						}
					}
				}

				function prop_img_def_r($id = NULL){
					$img = $this->input->post('curr_img');
					$path = APPPATH."../assets/img/properties/".$img;
					// $path = $_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/properties/".$img;
					if(file_exists($path)){
						unlink($path);
					}
					$where = "ID"; 
					$table = "ZHOA_BUYER_PROF";
					$data = array('PROP_IMG2' => "no-image.jpg");
					$this -> Main -> update_data($where, $table, $id, $data);
					echo "<img src=\"".base_url()."assets/img/properties/no-image.jpg"."\" width=\"100%\" class=\"img-thumbnail\">";
				}

				function upload_attach(){

					if($this->session->userdata('hoa_user_accepted__')) {

						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
					    $data['uname'] = $session_data['sess_uname'];

						$config['upload_path'] = './assets/img/prop_attachments/';
						$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg|xls|xlsx';
						$config['max_size']	= '3000';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';

						$this->load->library('upload', $config);

						if ( ! $this->upload->do_upload()) {

							$error = array('error' => $this->upload->display_errors());
							echo $error['error'];

						} else {

							$upload_data = $this->upload->data();
							$today = date('Y-m-d H:i:s');
							$table = "ZHOA_prop_attachments";
							$data = array('pa_owner_id' => $data['id'],
										  'pa_filename' => $upload_data['file_name'],
										  'pa_date_file' => $today,
										  'pa_is_deleted' => 0);
							$this -> Main -> insert_data($table, $data);
							
							$select = "*"; 
							$table = "ZHOA_prop_attachments";
						    $where = "pa_owner_id = ".$session_data['sess_id']." AND pa_is_deleted = 0"; 
						    $orderby = "pa_date_file DESC";
						    $data['attachments'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

						    $this -> load -> view('pages/prop_attachments', $data);

						}

					} else {
			 			redirect(base_url('login/login'), 'refresh');
					}
				}

				function del_attach($file_id = NULL){

					if($file_id == NULL){ 
						show_404();
					} else { 
						if($this->session->userdata('hoa_user_accepted__')) {
							$session_data = $this -> session -> userdata('hoa_user_accepted__');
						    $data['id'] = $session_data['sess_id'];
						    $data['uname'] = $session_data['sess_uname'];

							$table = "ZHOA_prop_attachments";
							$where = "pa_id";
							$data = array('pa_is_deleted' => 1);
							$this -> Main -> update_data($where, $table, $file_id, $data);
							
							$select = "*"; 
							$table = "ZHOA_prop_attachments";
						    $where = "pa_owner_id = ".$session_data['sess_id']." AND pa_is_deleted = 0"; 
						    $orderby = "pa_date_file DESC";
						    $data['attachments'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

						    $this -> load -> view('pages/prop_attachments', $data);
						} else {
				 			redirect(base_url('login/login'), 'refresh');
						}
					}
				}

				function download_attach($file_id=NULL){

					if($file_id == NULL){
						show_404();
					}

					$select = "*"; 
					$table = "ZHOA_prop_attachments";
					$where = "pa_is_deleted = 0 AND pa_id =".$file_id; 
				    $attachments = $this -> Main -> select_data_where($select, $table, $where);

				    foreach($attachments as $row){
				    	$filename = $row['pa_filename'];
				    }

					$data = file_get_contents(base_url()."assets/img/prop_attachments/".$filename); // Read the file's contents
					$name = $filename;

					force_download($name, $data);

				}

			function upload_img_prop_l($SO=NULL){

				// if($NO_DISPLAY==NULL){

				// 	show_404();

				// } else {

					$GET_SO = $SO;

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];

					$config['upload_path'] = './assets/img/properties/';
					$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg';
					$config['max_size']	= '3000';
					$config['max_width']  = '1920';
					$config['max_height']  = '1080';

					$this->load->library('upload', $config);

					if ( ! $this->upload->do_upload()) {

						$error = array('error' => $this->upload->display_errors());
						echo $error['error'];

					} else {

						$data = $this->upload->data();
						$filename = $data['file_name'];
						$table = "ZHOA_BUYER_PROF";
						$data = array('PROP_IMG1' => $filename);
						$where = "VBELN";
						$this -> Main -> update_data($where, $table, $GET_SO, $data);
						// echo $filename;
						echo "<img src=".base_url()."assets/img/properties/".$filename."  width=\"100%\" class=\"img-thumbnail\"/>";

					}

				// }

			}

			function upload_img_prop_r($SO=NULL){

				// if($NO_DISPLAY==NULL){

				// 	show_404();

				// } else {

					$GET_SO = $SO;

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];

					$config['upload_path'] = './assets/img/properties/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
					$config['max_size']	= '3000';
					$config['max_width']  = '1920';
					$config['max_height']  = '1080';

					$this->load->library('upload', $config);

					if ( ! $this->upload->do_upload()) {
						$error = array('error' => $this->upload->display_errors());
						echo $error['error'];
					} else {
						$data = $this->upload->data();
						$filename = $data['file_name'];
						$table = "ZHOA_BUYER_PROF";
						$data = array('PROP_IMG2' => $filename);
						$where = "VBELN";
						$this -> Main -> update_data($where, $table, $GET_SO, $data);
						// echo $filename;
						echo "<img src=".base_url()."assets/img/properties/".$filename."  width=\"100%\" class=\"img-thumbnail\"/>";

					}

				// }

			}

			function get_file(){

				file_get_contents($_REQUEST['attachment']);

			}

		function my_profile($id=NULL){

			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['profimg'] = $session_data['sess_img'];
			    
			    $data['active'] = "myProfile";

				$select = "bi_owner_id, bi_cust_no, bi_company, bi_unit, bi_project, bi_blklot, bi_lname, bi_fname, bi_mname, bi_bdate, bi_add1, bi_add2, bi_province, bi_zip, bi_contact, bi_email, bi_sec1, bi_sec2, bi_ans1, bi_ans2, bi_regdate";
				$table = "ZHOA_byrinfo";
				$where = "bi_owner_id = ".$data['id'];
				$limit = 1;
				$data['get_profile'] = $this -> Main -> select_data_where_limit($select, $table, $where, $limit);

				$select="bi_cust_no"; 
				$table="ZHOA_byrinfo"; 
				$where="bi_owner_id = ".$session_data['sess_id'];
			    $get_owner = $this -> Main -> select_data_where($select, $table, $where);

				foreach($get_owner as $row){

					$cust_no = $row['bi_cust_no'];

				}

				$select = "VBELN "; 
				$table = "ZHOA_BUYER_PROF"; 
				$where = "KUNNR =".$cust_no." AND ACTIVE = 1 ";
				$data['owned_prop'] = $this -> Main -> count_select_where($select, $table, $where);

				$select="b_cpass_date"; $table="ZHOA_byrlogin"; $where="b_id =".$session_data['sess_id'];
				$data['pword_update'] = $this -> Main -> select_data_where($select, $table, $where);

				$select = "";
				$table = "ZHOA_byrinfo";
				$where = "bi_owner_id = ".$session_data['sess_id'];
				$limit = 1;
				$data['get_profile'] = $this -> Main -> select_data_where_limit($select, $table, $where, $limit);

				$select="bi_csec_date"; 
				$table="ZHOA_byrinfo"; 
				$where="bi_owner_id =".$session_data['sess_id'];
			    $data['sec_update']=$this -> Main -> select_data_where($select, $table, $where);

			    $this -> load -> view('templates/app-header', $data);
			    $this -> load -> view('pages/my_profile', $data);
			    $this -> load -> view('templates/footer');
			
			} else {

				//If no session, redirect to login page
			 	redirect(base_url('login/login'), 'refresh');
				//show_404();

			}

		}

			function edit_profile(){

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    $data['profimg'] = $session_data['sess_img'];
				    
				    $data['active'] = "myProfile";
				    $data['btn_active'] = "myProfile";

				    $select = "bi_owner_id, bi_cust_no, bi_company, bi_unit, bi_project, bi_blklot, bi_lname, bi_fname, bi_mname, bi_bdate, bi_add1, bi_add2, bi_province, bi_zip, bi_contact, bi_email, bi_sec1, bi_sec2, bi_ans1, bi_ans2, bi_regdate";
					$table = "ZHOA_byrinfo";
					$where = "bi_owner_id = ".$data['id'];
					$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);
					$data['notification'] = "";

					if(isset($_POST['update'])){

						$where = "bi_owner_id"; 
						$table="ZHOA_byrinfo"; 
						$id=$data['id'];
						$data = array('bi_add1' => $this -> input -> post('add1'),
									  'bi_add2' => $this -> input -> post('add2'),
									  'bi_province' => $this -> input -> post('province'),
									  'bi_zip' => $this -> input -> post('zip'),
									  'bi_contact' => $this -> input -> post('contact'),
									  'bi_email' => $this -> input -> post('email'));
						$this -> Main -> update_data($where, $table, $id, $data);

						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
					    $data['uname'] = $session_data['sess_uname'];
					    
					    $data['active'] = "myProfile";
					    $data['btn_active'] = "myProfile";

					    $select = "bi_owner_id, bi_cust_no, bi_company, bi_unit, bi_project, bi_blklot, bi_lname, bi_fname, bi_mname, bi_bdate, bi_add1, bi_add2, bi_province, bi_zip, bi_contact, bi_email, bi_sec1, bi_sec2, bi_ans1, bi_ans2, bi_regdate";
						$table = "ZHOA_byrinfo";
						$where = "bi_owner_id = ".$data['id'];
						$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);

						$data['notification'] = "<div class=\"container\">
													<div class=\"col-md-1\"> </div>
													<div class=\"alert alert-success alert-dismissible col-md-10\" role=\"alert\" style=\"padding-top: 5px; padding-bottom: 5px;\">
														<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
														<strong>Profile Updated!</strong>
													</div>
												</div>";

						}
											
				    $this -> load -> view('templates/app-header', $data);
				    $this -> load -> view('pages/edit_profile', $data);
				    $this -> load -> view('templates/footer');
			
				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();

				}

			}

			function change_password(){

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    $data['profimg'] = $session_data['sess_img'];
				    
				    $data['active'] = "myProfile";
				    $data['btn_active'] = "cPassword";
				    $data['notification'] = "";

				    $select="b_cpass_date"; 
				    $table="ZHOA_byrlogin"; 
				    $where="b_id =".$data['id'];
				    $data['pword_update']=$this -> Main -> select_data_where($select, $table, $where);

				    if(isset($_POST['save'])){

				    	$select="b_pass";
				    	$table="ZHOA_byrlogin";
				    	$where="b_id = ".$data['id'];
				    	$get_pword = $this -> Main -> select_data_where($select, $table, $where);

				    	foreach($get_pword as $row){

				    		$pword = $row['b_pass'];

				    	}

				    	$curpword = md5($this -> input -> post('curpword'));
				    	if($pword == $curpword){
				    		$today = date('Y-m-d h:i:s');
				    		$newpword = $this -> input -> post('newpword');
				    		$repword = $this -> input -> post('repword');

				    		if($newpword == $repword){

						    	$where="b_id"; $table="ZHOA_byrlogin"; $id=$data['id'];
						    	$data = array('b_pass' => md5($this -> input -> post('repword')),
											  'b_cpass_date' => $today);
						    	$this -> Main -> update_data($where, $table, $id, $data);

						    	$result = "Password successfully changed.";
						    	$alert = "success";

					    	} else {

					    		$result = "New Password and Retype Password didn't match. Please Try Again.";
						    	$alert = "danger";

					    	}

					    } else {

					    	$result = "The current password you entered is incorrect.";
						    $alert = "danger";

					    }

					    $data['notification'] = "<div class=\"container\">
													<div class=\"col-md-1\"> </div>
													<div class=\"alert alert-".$alert." alert-dismissible col-md-10\" role=\"alert\" style=\"padding-top: 5px; padding-bottom: 5px;\">
														<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
														<strong>".$result."</strong>
													</div>
												</div>";


					    $session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
					    $data['uname'] = $session_data['sess_uname'];
					    
					    $data['active'] = "myProfile";
					    $data['btn_active'] = "cPassword";
						$select="b_cpass_date"; 
						$table="ZHOA_byrlogin"; 
						$where="b_id =".$data['id'];
				   		$data['pword_update']=$this -> Main -> select_data_where($select, $table, $where);

				    }
				    
				    
				    $this -> load -> view('templates/app-header', $data);
				    $this -> load -> view('pages/change_password', $data);
				    $this -> load -> view('templates/footer');
			
				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();

				}

			}

				function change_pword($id=NULL){

					if($id == NULL){

						show_404();

					} else {

						$data['id'] = $id;

						$select="b_pass";
				    	$table="ZHOA_byrlogin";
				    	$where="b_id = ".$data['id'];
				    	$get_pword = $this -> Main -> select_data_where($select, $table, $where);

				    	foreach($get_pword as $row){

				    		$pword = $row['b_pass'];

				    	}

				    	if(isset($_REQUEST['curpword']) && isset($_REQUEST['newpword']) && isset($_REQUEST['repword'])){

					    	$curpword = md5($_REQUEST['curpword']);

					    	if($pword == $curpword){

					    		$today = date('Y-m-d h:i:s');

						    	$newpword = $_REQUEST['newpword'];
						    	$repword = $_REQUEST['repword'];

					    		if($newpword == $repword){

							    	$where="b_id"; $table="ZHOA_byrlogin";// $id=$data['id'];
							    	$data = array('b_pass' => md5($repword),
												  'b_cpass_date' => $today);
							    	$this -> Main -> update_data($where, $table, $id, $data);

							    	$result = "Password successfully changed.";
							    	$alert = "success";

						    	} else {

						    		$result = "New Password and Retype Password didn't match. Please Try Again.";
							    	$alert = "danger";

						    	}

						    } else {

						    	$result = "The current password you entered is incorrect.";
							    $alert = "danger";

						    }

						    echo "<script>";
						    	echo "alert('".$result."')";
						    echo "</script>";
						    $data['notification'] = "<div class=\"container\">
														<div class=\"col-md-1\"> </div>
														<div class=\"alert alert-".$alert." alert-dismissible col-md-10\" role=\"alert\" style=\"padding-top: 5px; padding-bottom: 5px;\">
															<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
															<strong>".$result."</strong>
														</div>
													</div>";
								
							}

						}

						$data['id'] = $id;

						$select="b_cpass_date"; 
						$table="ZHOA_byrlogin"; 
						$where="b_id =".$id;
				    	$data['pword_update']=$this -> Main -> select_data_where($select, $table, $where);

						// $this -> load -> view('pages/change_pword', $data);


				}

				function sec_question($id=NULL){

					if($id == NULL){

						show_404();

					} else {

						$data['id'] = $id;

						if(isset($_REQUEST['sec1']) != "" || isset($_REQUEST['ans1']) != "" || isset($_REQUEST['sec2']) != "" || isset($_REQUEST['ans2']) != ""){ 

							$sec1 = $_REQUEST['sec1'];
							$ans1 = $_REQUEST['ans1'];
							$sec2 = $_REQUEST['sec2'];
							$ans2 = $_REQUEST['ans2'];

							$today = date('Y-m-d h:i:s');
							$where="bi_owner_id"; $table="ZHOA_byrinfo"; //$id=$data['id'];
							$data = array('bi_sec1' => $sec1,
										  'bi_ans1' => $ans1,
										  'bi_sec2' => $sec2,
										  'bi_ans2' => $ans2,
										  'bi_csec_date' => $today);
							$this -> Main ->update_data($where, $table, $id, $data);

							$result = "Security Questions has been updated.";
							$alert = "success";

						    $select = "";
						    $table = "ZHOA_byrinfo";
							$where = "bi_owner_id = ".$id;
							$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);

							echo "<script>";
						    	echo "alert('".$result."')";
						    echo "</script>";

							$data['notification'] = "<div class=\"container\">
														<div class=\"col-md-1\"> </div>
														<div class=\"alert alert-".$alert." alert-dismissible col-md-10\" role=\"alert\" style=\"padding-top: 5px; padding-bottom: 5px;\">
															<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
															<strong>".$result."</strong>
														</div>
													</div>";

						}

						$data['id'] = $id;

					    $select = "";
					    $table = "ZHOA_byrinfo";
						$where = "bi_owner_id = ".$id;
						$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);

						$select="bi_csec_date"; 
						$table="ZHOA_byrinfo"; 
						$where="bi_owner_id =".$id;
					    $data['sec_update']=$this -> Main -> select_data_where($select, $table, $where);

						// $this -> load -> view('pages/sec_question', $data);

					}

				}

				function edprofile($id=NULL){

					if($id == NULL){

						show_404();

					} else {

						$data['id'] = $id;

						if($this->session->userdata('hoa_user_accepted__')) {

							$session_data = $this -> session -> userdata('hoa_user_accepted__');
						    $data['uname'] = $session_data['sess_uname'];

						    if(isset($_REQUEST['add1']) != "" || isset($_REQUEST['add2']) != "" || isset($_REQUEST['province']) != "" || isset($_REQUEST['zip']) != "" || isset($_REQUEST['contact']) != "" || isset($_REQUEST['email']) != "") {

						    	$where = "bi_owner_id"; $table="ZHOA_byrinfo"; //$id=$data['id'];
								$data = array('bi_add1' => $_REQUEST['add1'],
											  'bi_add2' => $_REQUEST['add2'],
											  'bi_province' => $_REQUEST['province'],
											  'bi_zip' => $_REQUEST['zip'],
											  'bi_contact' => $_REQUEST['contact'],
											  'bi_email' => $_REQUEST['email']);
								$this -> Main -> update_data($where, $table, $id, $data);

							    echo "<script> alert('Profile Updated!'); </script>";

								$data['notification'] = "<div class=\"container\">
															<div class=\"col-md-1\"> </div>
															<div class=\"alert alert-success alert-dismissible col-md-10\" role=\"alert\" style=\"padding-top: 5px; padding-bottom: 5px;\">
																<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
																<strong>Profile Updated!</strong>
															</div>
														</div>";

						    }

							$select = "bi_owner_id, bi_cust_no, bi_company, bi_unit, bi_project, bi_blklot, bi_lname, bi_fname, bi_mname, bi_bdate, bi_add1, bi_add2, bi_province, bi_zip, bi_contact, bi_email, bi_sec1, bi_sec2, bi_ans1, bi_ans2, bi_regdate";
							$table = "ZHOA_byrinfo";
							$where = "bi_owner_id = ".$id;
							$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);
							
							$data['uname'] = $session_data['sess_uname'];

							// $this -> load -> view('pages/edprofile', $data);

						} else {

				 			redirect(base_url('login/login'), 'refresh');

						}

					}

				}

			function update_security(){

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    $data['profimg'] = $session_data['sess_img'];
				    
				    $data['active'] = "myProfile";
				    $data['btn_active'] = "uSec";

				    $select = "";
				    $table = "ZHOA_byrinfo";
					$where = "bi_owner_id = ".$data['id'];
					$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);

					$select="bi_csec_date"; $table="ZHOA_byrinfo"; $where="bi_owner_id =".$data['id'];
				    $data['sec_update']=$this -> Main -> select_data_where($select, $table, $where);

					$data['notification'] = "";

					if(isset($_POST['save'])){
				    	$today = date('Y-m-d h:i:s');
						$where="bi_owner_id"; $table="ZHOA_byrinfo"; $id=$data['id'];
						$data = array('bi_sec1' => $this -> input -> post('sec1'),
									  'bi_ans1' => $this -> input -> post('ans1'),
									  'bi_sec2' => $this -> input -> post('sec2'),
									  'bi_ans2' => $this -> input -> post('ans2'),
									  'bi_csec_date' => $today);
						$this -> Main ->update_data($where, $table, $id, $data);

						$result = "Security Questions hass been updated.";
						$alert = "success";

						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
					    $data['uname'] = $session_data['sess_uname'];

						$data['active'] = "myProfile";
					    $data['btn_active'] = "uSec";

					    $select = "";
					    $table = "ZHOA_byrinfo";
						$where = "bi_owner_id = ".$data['id'];
						$data['get_profile'] = $this -> Main -> select_data_where($select, $table, $where);

						$data['notification'] = "<div class=\"container\">
													<div class=\"col-md-1\"> </div>
													<div class=\"alert alert-".$alert." alert-dismissible col-md-10\" role=\"alert\" style=\"padding-top: 5px; padding-bottom: 5px;\">
														<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
														<strong>".$result."</strong>
													</div>
												</div>";

						$select="bi_csec_date"; $table="ZHOA_byrinfo"; $where="bi_owner_id =".$data['id'];
				   		$data['sec_update']=$this -> Main -> select_data_where($select, $table, $where);					
					
					}

				    $this -> load -> view('templates/app-header', $data);
				    $this -> load -> view('pages/update_security', $data);
				    $this -> load -> view('templates/footer');
			
				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();

				}
				
			}

			function upload($id = NULL){

				if($id == NULL){
					show_404();
				} else {
		
					$config['upload_path'] = './assets/img/users/';
					$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg';
					$config['max_size']	= '3000';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';

					$this->load->library('upload', $config);

					$select = "bi_profimg"; 
					$table = "ZHOA_byrinfo"; 
					$where = "bi_id = ".$id;
					$profimg = $this -> Main -> select_data_where($select, $table, $where);
					foreach($profimg as $row){ $filename = $row['bi_profimg']; }
					if($filename != "") { $filename = $row['bi_profimg']; } else { $filename = "user.png";}

					if ( ! $this->upload->do_upload()) {
						$error = array('error' => $this->upload->display_errors());
						echo $error['error'];
						echo "<img id=\"dP\" src=\"".base_url()."assets/img/users/".$filename."\" class=\"media-object mod_user\"> 
				               <input type=\"hidden\" id=\"prop_img\" value='".$filename."'>
                  				<label for=\"userfile\">
                    				<span class=\"glyphicon glyphicon-camera profile_image\"></span>
                  				</label>
                  				<span class=\"glyphicon glyphicon-remove profile_image_rm\"></span>";
					} else {
						$upload_data = $this->upload->data();
						if($filename!="user.png"){
							if(file_exists(APPPATH."../assets/img/users/".$filename)){
							// if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/img/users/".$filename)){
								unlink($config['upload_path'].$filename);
							}
						}
						$where = "bi_id"; $table = "ZHOA_byrinfo";
						$data = array( 'bi_profimg' => $upload_data['file_name'] );
						$this -> Main -> update_data($where, $table, $id, $data);
						echo "<img id=\"dP\" src=\"".base_url()."assets/img/users/".$upload_data['file_name']."\" class=\"media-object mod_user\">
				                <input type=\"hidden\" id=\"prop_img\" value='".$upload_data['file_name']."'>
                  				<label for=\"userfile\">
                    				<span class=\"glyphicon glyphicon-camera profile_image\"></span>
                  				</label>
                  				<span class=\"glyphicon glyphicon-remove profile_image_rm\"></span>";
                  		$this->load->library('session');
						$session_data = $this -> session -> userdata('hoa_user_accepted__');
						$session_data['sess_img'] = $upload_data['file_name'];
					    $this->session->set_userdata('hoa_user_accepted__', $session_data);
					}
				}
			}

		function announcement_page(){
			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
				$data['id'] = $session_data['sess_id'];
				$data['uname'] = $session_data['sess_uname'];
				$data['profimg'] = $session_data['sess_img'];
				    
				$data['active'] = "myHome";

				$this -> load -> view('templates/app-header', $data);
				$this -> load -> view('pages/announcements', $data);
				$this -> load -> view('templates/footer');
			} else{
				redirect(base_url('login/login'), 'refresh');
			}
		}

			function announcements($offset=0){

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    $data['profimg'] = $session_data['sess_img'];
				    
				    $data['active'] = "myHome";

				    $where = "a.bi_owner_id = ".$data['id'];
					$limit = 5;

					$config['base_url'] = base_url('portal/announcements/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_client_announcement_row($where, $data['id']);
					$this->pagination->initialize($config);

					$data['get_announcements'] = $this -> Buyer_Model -> get_client_announcement($where, $offset, $limit, $data['id']);
				    $data["announce_links"] = $this->pagination->create_links();

				    $data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}

				    $this -> load -> view('pages/announcements_list', $data);
				
				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();
				}
			}

			function quick_view(){
				if($_POST){
					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
					$where = $this->input->post('val');
					$data['get_announcements'] = $this -> Buyer_Model -> get_client_announcement_where($where, $data['id']);
					$this->load->view('pages/announcement_view',$data);
				}
			}

		// function my_request($offset=0){

		// 	if($this->session->userdata('hoa_user_accepted__')) {

		// 		$session_data = $this -> session -> userdata('hoa_user_accepted__');
		// 	    $data['id'] = $session_data['sess_id'];
		// 	    $data['uname'] = $session_data['sess_uname'];
		// 	    $data['profimg'] = $session_data['sess_img'];
			    
		// 	    $data['active'] = "myRequest";
		        
		// 	    $limit = 10;

		// 		$config['base_url'] = base_url('portal/my_request/0');
		// 		$config['per_page'] = $limit;
		// 		$config['total_rows'] = $this -> Buyer_Model -> get_buyer_concerns_rows($session_data['sess_id']);
		// 		$this->pagination->initialize($config);

		// 		$data['tickets'] = $this -> Buyer_Model -> get_buyer_concerns($config["per_page"], $offset, $session_data['sess_id']);
		// 	    $data["ticket_links"] = $this->pagination->create_links();

		// 	    $data['total_pages'] = $this -> Buyer_Model -> get_buyer_concerns_rows($session_data['sess_id']);
		// 		$data['display_offset'] = $offset + 1;
		// 		$data['display_limit'] = $offset + $limit;
		// 		if($data['display_limit'] > $data['total_pages']){
		// 			$data['display_limit'] = $data['total_pages'];
		// 		}
		// 		if($data['total_pages'] == 0){
		// 			$data['display_offset'] = $offset;
		// 			$data['display_limit'] = 0;
		// 		}

		//      	/*$select = ""; $table = "ZHOA_request_type"; $where = "rt_is_deleted = 0";
		//         $orderby = "rt_desc";
		//         $data['request_type'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);*/

		// 	    $this -> load -> view('templates/app-header', $data);
		// 	    $this -> load -> view('pages/my_request', $data);
		// 	    $this -> load -> view('templates/footer');
			
		// 	} else {

		// 		//If no session, redirect to login page
		// 	 	redirect(base_url('login/login'), 'refresh');
		// 		//show_404();

		// 	}

		// }

			function my_request_load($offset=0){

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    
				    $data['active'] = "myRequest";
			        
				    $limit = 10;

					$config['base_url'] = base_url('portal/my_request_load/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_buyer_concerns_rows($session_data['sess_id']);
					$this->pagination->initialize($config);

					$data['tickets'] = $this -> Buyer_Model -> get_buyer_concerns($config["per_page"], $offset, $session_data['sess_id']);
				    $data["ticket_links"] = $this->pagination->create_links();

				    $data['total_pages'] = $this -> Buyer_Model -> get_buyer_concerns_rows($session_data['sess_id']);
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}

			     	/*$select = ""; $table = "ZHOA_request_type"; $where = "rt_is_deleted = 0";
			        $orderby = "rt_desc";
			        $data['request_type'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);*/
				    
				    $this -> load -> view('pages/my_request_load', $data);
				
				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();

				}

			}

			function my_search_request($offset=0){

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
				$data['id'] = $session_data['sess_id'];
				$data['uname'] = $session_data['sess_uname'];
				if($_POST){

					$date_created_start = $this -> input -> post('date_created_start');
					$date_created_end = $this -> input -> post('date_created_end');
					$status = $this -> input -> post('status');
					$keyword = $this -> input -> post('keyword');
					$userid = $session_data['sess_id'];

					$limit = 10;

					$config['base_url'] = base_url('support/my_search_request/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this->Buyer_Model->get_search_req_rows($date_created_start, $date_created_end, $status, $keyword, $userid);
					$this->pagination->initialize($config);

					$data['tickets'] = $this->Buyer_Model->get_search_req($limit, $offset, $date_created_start, $date_created_end, $status, $keyword, $userid);
				    $data["tickets_links"] = $this->pagination->create_links();
			    
					$data['total_pages'] = $this->Buyer_Model->get_search_req_rows($date_created_start, $date_created_end, $status, $keyword, $userid);
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}

			    	$this -> load -> view('pages/my_request_search', $data);

				}
			}

			function view_request($id = NULL){

				if($id == NULL){

					show_404();

				} 

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
				    $data['id'] = $session_data['sess_id'];
				    $data['uname'] = $session_data['sess_uname'];
				    $data['profimg'] = $session_data['sess_img'];
			    
				    $data['active'] = "myRequest";
				    $data['notification'] = "";

			        $select = ""; 
			        $table = "ZHOA_tickets"; 
			        $where = "t_owner_id = ".$session_data['sess_id']." AND t_is_deleted = 0 AND t_id = ".$id;
			        $orderby = "t_date_created DESC"; 
			        $start = 0; 
			        $limit = 10;
			        $data['tickets'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			        $select = ""; 
			        $table = "ZHOA_concerns"; 
			        $where = "c_ticket_id = ".$id." AND c_is_deleted = 0";
			        $orderby = "c_date_created DESC"; 
			        $start = 0; 
			        $limit = 10;
			        $data['concerns'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
					$data['concerns'] = $this -> Buyer_Model -> get_single_ticket($id,$where = "");
					
				    $this -> load -> view('templates/app-header', $data);
				    $this -> load -> view('pages/view_request', $data);
				    $this -> load -> view('templates/footer');

				} else {

					//If no session, redirect to login page
				 	redirect(base_url('login/login'), 'refresh');
					//show_404();

				}

			}


			function reply($id=NULL){
				$data['get_id'] = $id;

				if($id=NULL){
					show_404();
					return false;
				}

				if($this->session->userdata('hoa_user_accepted__')) {

					$session_data = $this -> session -> userdata('hoa_user_accepted__');
					$data['id'] = $session_data['sess_id'];
					$data['uname'] = $session_data['sess_uname'];

					$select=""; 
					$table="ZHOA_concerns"; 
					$where="c_id = ".$data['get_id'];
					$get_status = $this -> Main -> select_data_where($select, $table, $where);

					foreach($get_status as $row){
						$c_status = $row['c_is_active'];
						$reqtype = $row['c_rtype'];
					}

					$data['get_status'] = $c_status;

					if($_POST){

					   	$today = date('Y-m-d H:i:s');

				    	$ticket_details = $this -> Buyer_Model -> get_ticket_info($_POST['ticket_id']);

				    	foreach($ticket_details as $row){
				    		$ticket_number = $row['t_id'];
				    		$blklot = $row['t_blklot'];
				    		$notification = $row['t_notif'];
				    		$cust_no = $row['bi_cust_no'];
							$email = $row['bi_email'];
							$name = $row['bi_fname']. " " .$row['bi_lname'];
							$project = $row['XWETEXT'];
							$pcode = $row['bi_projcode'];
				    	}

				    	$table = "ZHOA_concern_details";
				    	$data3 = array( 'cd_desc' => $_POST['get_concern'], 
				    					'cd_concern_id' => $data['get_id'], 
				    					'cd_date_created' => $today,
				    					'cd_is_deleted' => 0, 
				    					'cd_reply_by' => $data['uname'], 
				    					'cd_persona' => 1 );
				    	$this -> Main -> insert_data($table, $data3);

					    $where = "t_id";
				    	$table = "ZHOA_tickets";
				    	$data1 = array( 't_reply_by' => $data['uname'], 
				    					't_reply_date' => $today, 
				    					't_concern' => $_POST['get_concern'] );
				    	$this -> Main -> update_data($where, $table, $_POST['ticket_id'], $data1);

					    $where = "c_id";
					    $table = "ZHOA_concerns";
					    $data2 = array( 'c_reply_by' => $data['uname'], 'c_reply_date' => $today );
					    $this -> Main -> update_data($where, $table, $data['get_id'], $data2);

				    }
						// echo $get_id;
					$select = ""; 
					$table = "ZHOA_concern_details"; 
					$where = "cd_concern_id = ".$data['get_id']. "AND cd_persona = 0 OR cd_persona = 1"; 
					$orderby = "cd_date_created DESC"; 
					$start = "0"; 
					$limit = "1000";
					$data['concern_details'] = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start);
					
					$this -> load -> view('pages/send_reply', $data);	

					if($_POST){
					    $subject = "Vista HOMe Ticket Alert: ".str_pad($ticket_number,10,'0',STR_PAD_LEFT);
					    
						$receiver = $this -> Buyer_Model -> restricted_mailnotif($pcode);
						
						$message2 = " <style> .email_format{ font-family: 'arial'; font-size: 16px;} </style>
									<div class='email_format'>
									Customer Number: ". $cust_no .  "<br><br>
									<span style='font-weight:bold'>Project Information:</span> <br>
									&nbsp;&nbsp;&nbsp;Project: ".$project. "<br>
									&nbsp;&nbsp;&nbsp;Blk Lot: ".$blklot . "<br><br>
									Hi, <br><br>
									We want to make you aware of update to this ticket.<br>
									<a href='".base_url()."support/single_ticket/".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."'>Go to ticket</a>
									<br><br><br>
									Sincerely,<br><br>
									Vista HOMe Team</div>";

					    $recipient = array();
					    foreach ($receiver as $sender) {
					    	$recipient[] =  $sender['mn_mail'];
					    }

					    $this -> send_email($recipient, $message2, $subject);

					}

				} else {
				 	redirect(base_url('login/login'), 'refresh');
				}	
			}

			function reply_upload($id=NULL){

				$get_id = $id;
				$data['get_id'] = $id;

				if($id=NULL){

					show_404();

				} else {

					/*echo $get_id;

					if($get_id == 5){

						echo "Comments Here";

					}*/

					if($this->session->userdata('hoa_user_accepted__')) {

						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
				    	$data['uname'] = $session_data['sess_uname'];

					    if($_POST){
					    	$filename = '';
					    	$config['upload_path'] = './assets/concerns';
							$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg|xls|xlsx';
							$config['max_size']	= '3000';
							$config['max_width']  = '1024';
							$config['max_height']  = '768';
							
							$this->load->library('upload', $config);
						    if ( ! $this->upload->do_upload()) {
								$error = array('error' => $this->upload->display_errors());
								echo "<script> alert('".$error['error']."');</script>";
								$filename = "Attachment is not valid";
							} else {
								$upload_data = $this->upload->data();
								$filename = $upload_data['file_name'];
							}

					    	$concern = $this -> input -> post('concern');
					    	$concern_id = $this -> input -> post('concern_id');
					    	$ticketid = $this -> input -> post('ticket_id');

					    	$today = date('Y-m-d H:i:s');

					    	$concernid = $data['id'];
					    	$replyby = $data['uname'];

					    	$select=""; 
					    	$table="ZHOA_tickets"; 
					    	$where="t_id = ".$_POST['ticket_id'];
					    	$ticket_details = $this -> Main -> select_data_where($select, $table, $where);

					    	if(!$ticket_details){
					    		$ticket_number ="";
					    		$blklot ="";
					    		$notif ="";
					    		$cust_no ="";
					    		$email ="";
					    		$name ="";
					    		$notification="";
					    	} else {
					    		foreach($ticket_details as $row){
					    			$ticket_number = $row['t_id'];
					    			$blklot = $row['t_blklot'];
					    			$notification = $row['t_notif'];
					    		}	
					    	}

					    	$select="bi_cust_no, bi_email, bi_lname, bi_fname, bi_projcode"; 
					    	$table="ZHOA_byrinfo"; 
					    	$where="bi_owner_id = ".$session_data['sess_id'];
						    $get_owner = $this -> Main -> select_data_where($select, $table, $where);

							foreach($get_owner as $row){

								$cust_no = $row['bi_cust_no'];
								$name = $row['bi_fname']. " " .$row['bi_lname'];
								$project = $row['bi_projcode'];
							}

					    	$table = "ZHOA_concern_details";
					    	$data = array('cd_desc' => $_POST['concern'],
					    				  'cd_concern_id' => $_POST['concern_id'],
					    				  'cd_date_created' => $today,
					    				  'cd_is_deleted' => 0,
					    				  'cd_reply_by' => $replyby,
					    				  'cd_filename' => $filename,
					    				  'cd_persona' => 1);
					    	$this -> Main -> insert_data($table, $data);

					    	$where = "t_id";
					    	$table = "ZHOA_tickets";
					    	$data = array('t_reply_by' => $replyby,
					    				  't_reply_date' => $today,
					    				  't_concern' => $_POST['concern']);
					    	$this -> Main -> update_data($where, $table, $_POST['ticket_id'], $data);

					    	$where = "c_id";
						    $table = "ZHOA_concerns";
						    $data = array('c_reply_by' => $replyby, 'c_reply_date' => $today);
						    $this -> Main -> update_data($where, $table, $_POST['concern_id'], $data);

						    $receiver = $this -> Buyer_Model -> restricted_mailnotif($project);

				    		$subject = "Vista HOMe Ticket Alert: ".str_pad($ticket_number,10,'0',STR_PAD_LEFT);
				    		$message = " <style> .email_format{ font-family: 'arial'; font-size: 16px;} </style>
								<div class='email_format'>
								Customer Number: ". $cust_no .  "<br><br>
								<span style='font-weight:bold'>Project Information:</span> <br>
								&nbsp;&nbsp;&nbsp;Project: ".$project. "<br>
								&nbsp;&nbsp;&nbsp;Blk Lot: ".$blklot . "<br><br>
								Hi, <br><br>
								We want to make you aware of update to this ticket.<br>
								<a href='".base_url()."support/single_ticket/".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."'>Go to ticket</a>
								<br><br><br>
								Sincerely,<br><br>
								Vista HOMe Team</div>";

							$recipient = array();
					    	foreach ($receiver as $sender) {
						    	$recipient[] =  $sender['mn_mail'];
						    }

				    		$this -> send_email($recipient, $message, $subject);					
					    }

						// echo $data['get_id'];
						$select = ""; 
						$table = "ZHOA_concern_details"; 
						$where = "cd_concern_id = ".$get_id; 
						$orderby = "cd_date_created DESC"; 
						$start = "0"; 
						$limit = "100";
						$data['concern_details'] = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start);

						$this -> load -> view('pages/send_reply', $data);		
		
					} else {

						//If no session, redirect to login page
					 	redirect(base_url('login/login'), 'refresh');
						//show_404();

					}

				}

			}

			function remove_email_notif($id=NULL){
				$get_id = $id;
				if($id=NULL){
					show_404();
				} else {
					if($this->session->userdata('hoa_user_accepted__')) {
						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
				    	$data['uname'] = $session_data['sess_uname'];

				    	$where="t_id"; $table="ZHOA_tickets"; 
				    	$data = array('t_notif' => 0);
				    	$this -> Main -> update_data($where, $table, $get_id, $data);
				    	$getnotif = 0;

				    	$check = "";
			    		if($getnotif == 1){
			    			$check = 'checked';
			    		}

						echo "<input type=\"checkbox\" id=\"email_cbox\" name=\"getinfo\" value=".$getnotif." ".$check." onclick=\"tick_cbox(".$get_id.")\"> Email me notifications for this ticket.";


					} else {
						//If no session, redirect to login page
					 	redirect(base_url('login/login'), 'refresh');
						//show_404();
					}
				} 
			}

			function set_email_notif($id=NULL){
				$get_id = $id;
				if($id=NULL){
					show_404();
				} else {
					if($this->session->userdata('hoa_user_accepted__')) {
						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
				    	$data['uname'] = $session_data['sess_uname'];

				    	$where="t_id"; $table="ZHOA_tickets"; 
				    	$data = array('t_notif' => 1);
				    	$this -> Main -> update_data($where, $table, $get_id, $data);
				    	$getnotif = 1;
				    	
				    	$check = "";
			    		if($getnotif == 1){
			    			$check = 'checked';
			    		}

						echo "<input type=\"checkbox\" id=\"email_cbox\" name=\"getinfo\" value=".$getnotif." ".$check." onclick=\"tick_cbox(".$get_id.")\"> Email me notifications for this ticket.";

					} else {
						//If no session, redirect to login page
					 	redirect(base_url('login/login'), 'refresh');
						//show_404();
					}
				} 
			}

			function close_ticket($id){
				$session_data = $this -> session -> userdata('hoa_user_accepted__');
				$uname = $session_data['sess_uname'];

				$select = "";
                $table = "ZHOA_concerns";
                $where = "c_id = ".$id;
                $get_ticket = $this -> Main -> select_data_where($select, $table, $where);
                if($get_ticket){
                    foreach($get_ticket as $row){
                        $ticket_id = $row['c_ticket_id'];
                    }
                }else{
                    show_404();
                    return false;
                }

				$today = date('Y-m-d');
				$where = "c_id"; 
				$table = "ZHOA_concerns"; 
				$thisday = strtotime($today);
				$data = array('c_is_active' => 2,
							  'c_closed_date' => $today,
							  'c_closed_by' => $uname);
				$this -> Main -> update_data($where, $table, $id, $data);

				echo "<script> alert('Ticket Closed.'); </script>";
				echo "<script> window.location.href='".base_url()."portal/view_request/".str_pad($ticket_id, 10, 0, STR_PAD_LEFT)."' </script>";

			}

		function new_request(){

			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['profimg'] = $session_data['sess_img'];

			    $data['active'] = "myRequest";

			    $select="bi_cust_no, bi_email, bi_lname, bi_fname, bi_projcode"; 
			    $table="ZHOA_byrinfo"; 
			    $where="bi_owner_id = ".$session_data['sess_id'];
			    $get_owner = $this -> Main -> select_data_where($select, $table, $where);

				foreach($get_owner as $row){
					$cust_no = $row['bi_cust_no'];
					$email = $row['bi_email'];
					$name = $row['bi_fname']. " " .$row['bi_lname'];
					$pcode = $row['bi_projcode'];
				}

			    $select = "KUNNR, XWETEXT, SWENR, REFNO, ARKTX, FLR_AREA, LOT_SIZE, VBELN "; 
			    $table = "ZHOA_BUYER_PROF"; 
			    $where = "KUNNR =".$cust_no." AND ACTIVE = 1 "; 
			    $orderby = "KUNNR ASC";
			    $data['blklot'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			    foreach ($data['blklot'] as $e) {
			    	$project = $e['XWETEXT'];
			    }

			    $select=""; 
			    $table="ZHOA_request_type"; 
			    $where="rt_is_deleted =0"; 
			    $orderby="rt_desc ASC";
			    $data['request_type'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
			    $data['notification'] = "";

			    if(isset($_POST['save'])){

			    	$notif = 0;
			    	if(isset($_POST['getnotif'])){
			    		$notif = 1;
			    	}

			    	$today = date('Y-m-d H:i:s');
			    	$blklot = $this -> input -> post('blk');
			    	$so = $this -> input -> post('so');
			    	$status = 1;
			    	$owner_id = $session_data['sess_id'];
			    	$table = "ZHOA_tickets";
			    	$data1 = array("t_blklot" => $blklot,
			    				   "t_date_created" => $today,
			    				   "t_status" => $status,
					    		   "t_owner_id" => $owner_id,
					    		   "t_is_deleted" => 0,
					    		   "t_so" => $so,
					    		   "t_reply_by" => $session_data['sess_uname'],
					    		   "t_reply_date" => $today,
					    		   "t_concern" => $_SESSION['message'.$_SESSION['counter']],
					    		   "t_notif" => $notif,
					    		   "t_source" => 8);
			    	$_SESSION['match_ticket'] = md5($today);//.$blklot.$owner_id);
			    	$this -> Main -> insert_data($table, $data1);

			    	$select = "";
			    	$table = "ZHOA_tickets";
			    	$where = "t_owner_id = ".$session_data['sess_id']. "AND t_date_created = '" .$today."'";	
			    	$tickets = $this -> Main -> select_data_where($select, $table, $where);

			    	foreach($tickets as $row){

			    		$date_created = strtotime($row['t_date_created']);

			    		// if($_SESSION['match_ticket'] == md5(date('Y-m-d H:i:s', $date_created))) {//.$row['t_blklot'].$row['t_owner_id'])){
			    		$notification = $row['t_notif'];
			    		$ticket_number = $row['t_id'];
			    		$blklot = $row['t_blklot'];

			    			for($x=1; $x<=$_SESSION['counter']; $x++){

					    		$_SESSION['request'.$x];
					    		$_SESSION['sub_request'.$x];
					    		$_SESSION['message'.$x];
					    		$_SESSION['attachment'.$x];

					    		$data2 = array("c_rtype" => $_SESSION['request'.$x],
					    					   "c_stype" => $_SESSION['sub_request'.$x],
					    					   "c_message" => $_SESSION['message'.$x],
							    			   "c_is_active" => 1,
							    			   "c_date_created" => date('Y-m-d H:i:s', $date_created),
							    			   "c_is_deleted" => 0,
							    			   "c_reply_by" => $session_data['sess_uname'],
							    			   "c_ticket_id" => $row['t_id'],
							    			   "c_reply_date" => date('Y-m-d H:i:s', $date_created),
							    			   "c_hsec"=> null, 
							    			   "c_closed_date"=>null, 
							    			   "c_closed_by"=>null, 
							    			   "c_closed"=>'00000000', 
							    			   "c_created"=>date('Ymd', $date_created));
					    		$table = "ZHOA_concerns";
					    		$this -> Main -> insert_data($table, $data2);

					    		$condate = date('Y-m-d H:i:s', $date_created);
					    		$select = "c_id";
					    		$where = "c_reply_date = '".$condate."' ";
					    		$c_id = $this->Main->select_data_where($select, $table, $where);

					    		$con_id="";
					    		foreach ($c_id as $key) {
					    			$con_id = $key['c_id'];
					    		}

					    		$data3 = array("cd_desc"=>$_SESSION['message'.$x], 
					    					   "cd_concern_id"=>$con_id, 
					    					   "cd_date_created"=>$condate, 
					    					   "cd_is_deleted"=>0,
					    					   "cd_reply_by"=>$session_data['sess_uname'], 
					    					   "cd_filename"=>str_replace(' ', '_',$_SESSION['attachment'.$x]), 
					    					   "is_viewed_2"=>0, 
					    					   "cd_persona"=>1);
					    		$table = "ZHOA_concern_details";
					    		$this -> Main -> insert_data($table, $data3);
					    	}
			    		// }
			    	}
			    	// acknowledgement email receipt
			    	$subject = "Vista HOMe Ticket Receipt";
			    	$message = "<style>.email_format{font-family: 'arial'; font-size: 16px;}</style>
								<div class='email_format'>
						    		Hi Sir/Ma'am " . $name . ",<br><br>
						    		This is to acknowledge the ticket you posted at Vista HOMe Portal.<br>
						    		Pls see ticket details below.<br><br>
						   			Ticket Number: ".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."<br>
						   			Customer Number: ". $cust_no ."<br><br>
						   			<span style='font-weight:bold'>Project Information:</span><br>
									&nbsp;&nbsp;&nbsp;Project: ".$project. "<br>
									&nbsp;&nbsp;&nbsp;Blk Lot: ".$blklot . "<br><br>
									<a href='".base_url()."portal/view_request/".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."'>Go to ticket</a>
									<br><br><br>
						   			Sincerely,<br><br>
					    			Vista HOMe Team
				    			</div>";
			    	$this -> send_email($email, $message, $subject);

			    	$receiver = $this -> Buyer_Model -> restricted_mailnotif($pcode);
			    	
		    		// notification email
		    		$subject2 = "Vista HOMe Ticket Alert: ".str_pad($ticket_number,10,'0',STR_PAD_LEFT);
		    		$message2 = "<style>.email_format{font-family: 'arial'; font-size: 16px;}</style>
								<div class='email_format'>Customer Number: ". $cust_no .  "<br><br>
					    			<span style='font-weight:bold'>Project Information:</span><br>
									&nbsp;&nbsp;&nbsp;Project: ".$project. "<br>
									&nbsp;&nbsp;&nbsp;Blk Lot: ".$blklot . "<br><br>
					    			Hi, <br><br>
					    			We want to make you aware of update to this ticket.<br>
					    			<a href='".base_url()."support/single_ticket/".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."'>Go to ticket</a>
					    			<br><br><br>
					    			Sincerely,<br><br>
					    			Vista HOMe Team 
			    				</div>";
		    		$recipient = array();
				    foreach ($receiver as $sender) {
				    	$recipient[] =  $sender['mn_mail'];
				    }

				   	$this -> send_email($recipient, $message2, $subject2);
			    	
					$data['notification'] = "Concerns has been created";
			    }

			    if(isset($_SESSION['counter'])){

			    	for($x=1; $x<=$_SESSION['counter']; $x++){

			    		unset($_SESSION['request'.$x]);
			    		unset($_SESSION['sub_request'.$x]);
			    		unset($_SESSION['message'.$x]);

			    	}
			    	unset($_SESSION['counter']);
			    }

			    $session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['active'] = "myRequest";

			    $select = ""; 
			    $table = "ZHOA_BUYER_PROF"; 
			    $where = "KUNNR =".$cust_no." AND ACTIVE = 1 AND ABGRU=''"; 
			    $orderby = "KUNNR ASC";
			    $data['blklot'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			    $select=""; 
			    $table="ZHOA_request_type"; 
			    $where="rt_is_deleted =0"; 
			    $orderby="rt_desc ASC";
			    $data['request_type'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			    $this -> load -> view('templates/app-header', $data);
			    $this -> load -> view('pages/new_request', $data);
			    $this -> load -> view('templates/footer');
			
			} else {
			 	redirect(base_url('login/login'), 'refresh');
			}
		}

			function get_sub_req($id=NULL){

				if($id == NULL){

					show_404();

				} else {

					$select=""; 
					$table="ZHOA_subrequest_type"; 
					$where="st_rtype = ".$id." AND st_is_deleted = 0"; 
					$orderby="st_desc ASC";
					$sub_req = $this -> Main -> get_data_where($select, $table, $where, $orderby);

					// echo "<select class='form-control input-sm' name='sub_req_type' id='sub_req_type'>";
					echo "<option value=''>--select sub type--</option>";
					foreach($sub_req as $row){

						echo "<option value='".$row['st_id']."'>".$row['st_desc']."</option>";

					}

					// echo "</select>";

				}

			}

			function get_blklot($id=NULL){

				if($id == NULL){

					show_404();

				} else {


					if($this->session->userdata('hoa_user_accepted__')) {

						$session_data = $this -> session -> userdata('hoa_user_accepted__');
					    $data['id'] = $session_data['sess_id'];
					    $data['uname'] = $session_data['sess_uname'];

						$select="bi_cust_no"; 
						$table="ZHOA_byrinfo"; 
						$where="bi_owner_id = ".$session_data['sess_id'];
					    $get_owner = $this -> Main -> select_data_where($select, $table, $where);

						foreach($get_owner as $row){

							$cust_no = $row['bi_cust_no'];

						}

					    $select = "KUNNR, SWENR, REFNO, ARKTX, FLR_AREA, LOT_SIZE, VBELN, XWETEXT, MOVEIN_DATE"; 
					    $table = "ZHOA_BUYER_PROF"; 
					    $where = "(KUNNR =".$cust_no ." AND REFNO = '".$id."')  AND ACTIVE = 1 "; 
					    $orderby = "KUNNR ASC";
					    $blklot = $this -> Main -> get_data_where($select, $table, $where, $orderby);

						foreach($blklot as $rows){

							$project = $rows['XWETEXT'];
							$blk_lot = $rows['REFNO'];
							$lot_area = $rows['LOT_SIZE'];
							$model = $rows['ARKTX'];
							$flr_area = $rows['FLR_AREA'];
							$movein = $rows['MOVEIN_DATE'];
							$SO = $rows['VBELN'];
						}
						$get_move_in = "--";
						if($movein != 0){
							$get_move_in = date('m/d/Y', strtotime($movein));
						}
 
						if($project == "" || $project == " ") {

							$project = "--";

						}

						if($blk_lot == "" || $blk_lot == " ") {

							$blk_lot = "--";

						}

						if($lot_area == "" || $lot_area == " ") {

							$lot_area = "--";

						}

						if($model == "" || $model == " ") {

							$model = "--";

						}

						if($flr_area == "" || $flr_area == " ") {

							$flr_area = "--";

						}

						echo "<div class=\"col-md-4\">";
						echo "	<div class=\"col-md-12\">";
						echo "		<div class=\"col-md-5\">";
						echo "			<label> Project </label>";
						echo "		</div>";
						echo "		<div class=\"col-md-7\">".$project."</div>";
						echo "	</div>";
						echo "	<div class=\"col-md-12\">";
						echo "		<div class=\"col-md-5\">";
						echo "			<label> Model </label>";
						echo "		</div>";
						echo "		<div class=\"col-md-7\">".$model."</div>";
						echo "	</div>";
						echo "</div>";

						echo "<div class=\"col-md-4\">";
						echo "	<div class=\"col-md-12\">";
						echo "		<div class=\"col-md-5\">";
						echo "			<label> Blk/Lot </label>";
						echo "		</div>";
						echo "		<div class=\"col-md-7\">".$blk_lot."</div>";
						echo "	</div>";
						echo "	<div class=\"col-md-12\">";
						echo "		<div class=\"col-md-5\">";
						echo "			<label> Flr Area </label>";
						echo "		</div>";
						echo "		<div class=\"col-md-7\">".$flr_area."</div>";
						echo "	</div>";
						echo "</div>";

						echo "<div class=\"col-md-4\">";
						echo "	<div class=\"col-md-12\">";
						echo "		<div class=\"col-md-5\">";
						echo "			<label> Lot Area </label>";
						echo "		</div>";
						echo "		<div class=\"col-md-7\">".$lot_area."</div>";
						echo "	</div>";
						echo "	<div class=\"col-md-12\">";
						echo "		<div class=\"col-md-5\">";
						echo "			<label> Move In Date </label>";
						echo "		</div>";
						echo "		<div class=\"col-md-7\">".$get_move_in."</div>";
						echo "	</div>";
						echo "</div>";
						echo "<input type='hidden' value='".$SO."' name='so'>";
						echo "<input type='hidden' value='".$blk_lot."' name='blk'>";


					} else {

			 			redirect(base_url('login/login'), 'refresh');

					}

				}

			}

			// function add_concern(){		//unused function

			// 	if(isset($_SESSION['counter'])){

			// 		$x = $_SESSION['counter']++;

			// 	} else {

			// 		$_SESSION['counter'] = 1;
			// 		//$x = 1;

			// 	}

			// 	if($_REQUEST['request'] && $_REQUEST['sub_request'] && $_REQUEST['message']){

			// 		$_SESSION['request'.$_SESSION['counter']] = $_REQUEST['request'];
			// 		$_SESSION['sub_request'.$_SESSION['counter']] = $_REQUEST['sub_request'];
			// 		$_SESSION['message'.$_SESSION['counter']] = $_REQUEST['message'];
			// 		//echo $request." ".$sub_request." ".$message;

			// 	}

			// 	echo "<div class=\"container\">";

			// 		echo "<div class='col-md-12'>";

			// 			echo "<div class='panel panel-primary'>";
			// 			echo "<div class='panel-heading'>Concerns</div>";
			// 			echo "<div class='table-responsive>";
			// 			echo "<table class='table'>";
			// 				echo "<tr>";
			// 					echo "<th>Request</th>";
			// 					echo "<th>Sub Type</th>";
			// 					echo "<th>Message</th>";
			// 				echo "</tr>";

			// 			// unset($_SESSION['counter']);


			// 			for($i=1; $i<=$_SESSION['counter']; $i++){

			// 				$select=""; 
			// 				$table="ZHOA_request_type"; 
			// 				$where="rt_id = ".$_SESSION['request'.$i];
			// 				$get_request = $this -> Main -> select_data_where($select, $table, $where);

			// 				foreach($get_request as $gt){
			// 					$request_res = $gt['rt_desc'];
			// 				}

			// 				$select=""; 
			// 				$table="ZHOA_subrequest_type"; 
			// 				$where="st_id = ".$_SESSION['sub_request'.$i];
			// 				$get_srequest = $this -> Main -> select_data_where($select, $table, $where);

			// 				foreach($get_srequest as $gt){
			// 					$srequest_res = $gt['st_desc'];
			// 				}

			// 				echo "<tr>";
			// 					echo "<td style='width: 150px;'>".$request_res."</td>";
			// 					echo "<td style='width: 300px;'>".$srequest_res."</td>";
			// 					echo "<td>".$_SESSION['message'.$i]."</td>";
			// 				echo "</tr>";

			// 			}

			// 			echo "</table>";
			// 			echo "</div>";
			// 			echo "</div>";

			// 		echo "</div>";

			// 		//echo "<div class='col-md-1'><input type='submit' class='btn btn-primary btn-sm form-control' name='add_concern' value='Save'></div>";
			// 		//echo "<div class='col-md-1'><input type='submit' class='btn btn-danger btn-sm form-control' name='cancel' value='Cancel'></div>";

			// 	echo "</div>";
			
			// }

			// function add_concern1(){		//unused function

			// 	if(isset($_SESSION['counter'])){

			// 		$x = $_SESSION['counter']++;

			// 	} else {
			// 		$_SESSION['counter'] = 1;
			// 	}

			// 	if($_REQUEST['request'] && $_REQUEST['sub_request'] && $_REQUEST['message']){
			// 		$_SESSION['request'.$_SESSION['counter']] = $_REQUEST['request'];
			// 		$_SESSION['sub_request'.$_SESSION['counter']] = $_REQUEST['sub_request'];
			// 		$_SESSION['message'.$_SESSION['counter']] = $_REQUEST['message'];
			// 	}

			// 	for($i=1; $i<=$_SESSION['counter']; $i++){

			// 		$select=""; 
			// 		$table="ZHOA_request_type"; 
			// 		$where="rt_id = ".$_SESSION['request'.$i];
			// 		$get_request = $this -> Main -> select_data_where($select, $table, $where);

			// 		foreach($get_request as $gt){
			// 			$request_res = $gt['rt_desc'];
			// 		}

			// 		$select=""; 
			// 		$table="ZHOA_subrequest_type"; 
			// 		$where="st_id = ".$_SESSION['sub_request'.$i];
			// 		$get_srequest = $this -> Main -> select_data_where($select, $table, $where);

			// 		foreach($get_srequest as $gt){
			// 			$srequest_res = $gt['st_desc'];
			// 		}

			// 		echo "<div class=\"container\">";
			// 			echo "<div class=\"col-md-12\">";
			// 				echo "<div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">";
			// 					echo "<div class=\"panel panel-default\">";
			// 						echo "<div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">";
			// 							echo "<h4 class=\"panel-title\">";
			// 								echo "<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">";
			// 									echo $request_res;
			// 								echo "</a>";
			// 							echo "</h4>";
			// 						echo "</div>";
			// 					echo "</div>";
			// 					echo "<div id=\"collapseOne\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"headingOne\">";
			// 				    	echo "<div class=\"panel-body\">";
			// 				       		echo "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.";
			// 				    	echo "</div>";
			// 					echo "</div>";
			// 				echo "</div>";
			// 			echo "</div>";
			// 		echo "</div>";
				
			// 	}

			
			// }

			function add_concern2(){

				if(isset($_SESSION['counter'])){
					$x = $_SESSION['counter']++;
				} else {
					$_SESSION['counter'] = 1;
				}
				if($_REQUEST['request'] == 1){
					if($_REQUEST['request'] && $_REQUEST['sub_request'] && $_REQUEST['message']){

						$_SESSION['request'.$_SESSION['counter']] = $_REQUEST['request'];
						$_SESSION['sub_request'.$_SESSION['counter']] = $_REQUEST['sub_request'];
						$_SESSION['message'.$_SESSION['counter']] = $_REQUEST['message'];
						$_SESSION['attachment'.$_SESSION['counter']] = $_REQUEST['attachment'];
					}
				}
				if($_REQUEST['request'] == 2){
					if($_REQUEST['request'] && $_REQUEST['message']){

						$_SESSION['request'.$_SESSION['counter']] = $_REQUEST['request'];
						$_SESSION['sub_request'.$_SESSION['counter']] = NULL;
						$_SESSION['message'.$_SESSION['counter']] = $_REQUEST['message'];
						$_SESSION['attachment'.$_SESSION['counter']] = $_REQUEST['attachment'];
					}
				}

				echo "<div class=\"container\">";

					echo "<div class='col-md-12'>";

						echo "<div class='panel panel-dark'>";
						echo "<div class='panel-heading'>Concerns</div>";

						echo "<div class='table-responsive'>";
						echo "<table class='table'>";
							echo "<tr>";
								echo "<th>Request</th>";
								echo "<th>Sub Type</th>";
								echo "<th>Message</th>";
							echo "</tr>";

						// unset($_SESSION['counter']);


						for($i=1; $i<=$_SESSION['counter']; $i++){

							$select=""; 
							$table="ZHOA_request_type"; 
							$where="rt_id = ".$_SESSION['request'.$i];
							$get_request = $this -> Main -> select_data_where($select, $table, $where);

							foreach($get_request as $gt){
								$request_res = $gt['rt_desc'];
							}
							if($_SESSION['request'.$i] == 1){
								$select=""; 
								$table="ZHOA_subrequest_type"; 
								$where="st_id = ".$_SESSION['sub_request'.$i];
								$get_srequest = $this -> Main -> select_data_where($select, $table, $where);

								foreach($get_srequest as $gt){
									$srequest_res = $gt['st_desc'];
								}
							} else {
								$srequest_res = "";
							}

							echo "<tr>";
								echo "<td style='width: 150px;'>".$request_res."</td>";
								echo "<td style='width: 300px;'>".$srequest_res."</td>";
								echo "<td>".$_SESSION['message'.$i]."</td>";
								echo "<td>".$_SESSION['attachment'.$i]."</td>";
								// echo "<td><img src=\"".$_SESSION['attachment'.$i]."\" width='200px' height='100px'></td>";
							echo "</tr>";
							
						}

						echo "</table>";

						echo "</div>";

						echo "</div>";

					echo "</div>";
					
				echo "</div>";


				echo "<div class='container'>";
					echo "<div class='col-md-1'> <input type='submit' class='btn btn-primary btn-sm form-control' name='save' value='Save'> </div>";
					echo "<div class='col-md-1'><input type='button' class='btn btn-danger btn-sm form-control' name='cancel' value='Cancel' onclick=\"window.location.href='".base_url()."portal/my_request'\"></div>";
				echo "</div>";

			}

			function reset_concern(){

				if(isset($_SESSION['counter'])){

			    	for($x=1; $x<=$_SESSION['counter']; $x++){

			    		unset($_SESSION['request'.$x]);
			    		unset($_SESSION['sub_request'.$x]);
			    		unset($_SESSION['message'.$x]);
			    		unset($_SESSION['attachment'.$x]);
			    	}

			    	unset($_SESSION['counter']);

			    }

			    echo "<div class=\"container\">";
					echo "<div class=\"col-md-12\">";
						echo "Concerns...";
					echo "</div>";
				echo "</div>";


			}

		function myDocs(){
			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['profimg'] = $session_data['sess_img'];
			    
			    $data['active'] = "docs";

			    $this -> load -> view('templates/app-header', $data);
			    $this -> load -> view('pages/my_docs', $data);
			    $this -> load -> view('templates/footer');
			
			} else {

				//If no session, redirect to login page
			 	redirect(base_url('login/login'), 'refresh');
				//show_404();

			}
		}

		function SOA(){
			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['profimg'] = $session_data['sess_img'];

			    $data['blklot'] = $this -> Buyer_Model -> get_byr_by_id($data['id']);
			    $data['active'] = "soa";

			    $this -> load -> view('templates/app-header', $data);

			    $soa = $this->Buyer_Model->get_project_soa($data['id']);
			    if($soa){
			    	$this -> load -> view('pages/sampleSOA', $data);
			    }else{
			    	$this -> load -> view('pages/soa', $data);
			    }

			    $this -> load -> view('templates/footer');
			
			} else {

				//If no session, redirect to login page
			 	redirect(base_url('login/login'), 'refresh');
				//show_404();

			}
		}

			function soa_date() {
				if ($_POST) {
					$so = $this->input->post('so');
					$soaPeriod = $this->Buyer_Model->get_soa_date_by_so($so);
					if($soaPeriod){
						foreach ($soaPeriod as $v) {
							switch ($v['SOA_MO']) {
								case 1:
									$mon = "January";
									break;
								case 2:
									$mon = "February";
									break;
								case 3:
									$mon = "March";
									break;
								case 4:
									$mon = "April";
									break;
								case 5:
									$mon = "May";
									break;
								case 6:
									$mon = "June";
									break;
								case 7:
									$mon = "July";
									break;
								case 8:
									$mon = "August";
									break;
								case 9:
									$mon = "September";
									break;
								case 10:
									$mon = "October";
									break;
								case 11:
									$mon = "November";
									break;
								case 12:
									$mon = "December";
									break;	
								default:
									$mon = "";
									break;
							}
							echo "<option value=''>Select Statement Date</option>";
							echo "<option value='".$v['SOA_NUM']."' > ".$mon." ".$v['SOA_YEAR']." </option>";
						}
					}else{
						echo "<script> alert('Sorry no SOA available on this project.') </script>";
					}
				}
			}

			function viewSOA() {

				// if(isset($_GET['submit'])) {
					$soa = $this->input->get('soa_period');
					$so = $this->input->get('prop');
					$data['SOA'] = $this -> Buyer_Model -> get_soa($soa,$so);
					foreach ($data['SOA'] as $k) {
						$fname = $k['SOA_MO']."/".$k['SOA_YEAR'].".pdf";
					}
					$raw = $this->load->view('pages/pdf', $data, true);

					include(APPPATH.'../assets/admin/mpdf/mpdf.php');
					$mpdf=new mPDF('c','A4','','' , 12 , 0 , 10 , 0 , 0 , 0); 
					$mpdf->showImageErrors = true;
					$mpdf->SetDisplayMode('fullpage');
					$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
					$doc = file_get_contents( APPPATH.'../application/views/pages/pdf.php');
					$stylesheet = file_get_contents( APPPATH.'../assets/styles/css/bootstrap.css');
					$mpdf->WriteHTML($stylesheet,1);
					$mpdf->WriteHTML($raw);
					$mpdf->debug = true;
					$mpdf->Output($fname, I);

				// }
				
			}

			function test(){
				echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
			}

		function contacts(){

			if($this->session->userdata('hoa_user_accepted__')) {

				$session_data = $this -> session -> userdata('hoa_user_accepted__');
			    $data['id'] = $session_data['sess_id'];
			    $data['uname'] = $session_data['sess_uname'];
			    $data['profimg'] = $session_data['sess_img'];
			    
			    $data['active'] = "contacts";

			    $this -> load -> view('templates/app-header', $data);
			    $this -> load -> view('pages/contacts', $data);
			    $this -> load -> view('templates/footer');
			
			} else {

				//If no session, redirect to login page
			 	redirect(base_url('login/login'), 'refresh');
				//show_404();

			}

		}

		function logout(){

			$this->session->unset_userdata('hoa_user_accepted__');
			$this->session->sess_destroy();
		    redirect(base_url(), 'refresh');

		}

		function send_email($email, $message, $subject){
			
			$this->email->initialize(array(
				'protocol' => 'smtp',
				'smtp_host' => '192.168.7.83',
				'smtp_user' => '',
				'smtp_pass' => '',
				'smtp_port' => 25,
				'crlf' => "\r\n",
				'newline' => "\r\n",
				'mailtype' => 'html'
			));
			
			$this->email->from('noreply@vistahome.com.ph', "Vista Home Portal");
			$this->email->bcc($email);
			$this->email->subject($subject);
			$this->email->message($message);
			if($this->email->send()){
				return true;
			}else{
				return false;
			}
			
		}

		// function test(){

		// 	$data['active'] = "";
		// 	$data['uname'] = "";
		// 	$this -> load -> view('templates/app-header', $data);
		// 	$this -> load -> view('pages/w_home');
		// 	$this -> load -> view('templates/footer');

		// }

	}


/* End of file portal.php */
/* Location: ./application/controllers/portal.php */