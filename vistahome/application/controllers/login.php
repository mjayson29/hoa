<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	session_start();

	class Login extends CI_Controller {

		function  __construct(){

			parent::__construct();
		    $this -> load -> model('Main','',TRUE);
		    $this -> load -> helper('text');
			date_default_timezone_set("Asia/Manila"); 

		}

		function index(){
			if($this->session->userdata('hoa_user_accepted__')) {

				redirect(base_url('portal/'), 'refresh');
			
			} else {

				$select = "c_id, c_brandname, c_filename";
				$table = "ZHOA_carousel";
				$data['carousel'] = $this -> Main -> select_data($select, $table);
				$data['count'] = $this -> Main -> count_select($select, $table);
				
				$data['active'] = "";

				$this -> load -> view('templates/header',$data);
				$this -> load -> view('login/login', $data);
				$this -> load -> view('templates/footer');
			}

		}
 
		function verify_login(){

			//This method will have the credentials validation
		    $this->load->library('form_validation');

		    $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean');
		    $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|callback_check_database');
			
		    if($this->form_validation->run() == FALSE) {

		        //Field validation failed.  User redirected to login page
		    	$select = "c_id, c_brandname, c_filename";
				$table = "ZHOA_carousel";
				$data['carousel'] = $this -> Main -> select_data($select, $table);
				$data['count'] = $this -> Main -> count_select($select, $table);
				
				$data['active'] = "";

		    	$this -> load -> view('templates/header',$data);
				$this -> load -> view('login/login', $data);
				$this -> load -> view('templates/footer');



		    } else  {

		        //Go to private area

		        redirect(base_url('portal/'), 'refresh');

		    }

		}

		function check_database($password) {
		    
		    //Field validation succeeded.  Validate against database
		    $uname = $this -> input -> post('username');
		    // $filtered = mysql_real_escape_string(stripslashes($uname));
		    
		    //query the database
		    $result = $this -> Main -> login($uname, $password,1,1);
		    
		    if($result) {

		      $sess_array = array();

		      foreach($result as $row) {

		        $sess_array = array(
		          'sess_id' => $row->b_id,
		          'sess_uname' => $row->b_uname,
		          'sess_img' => $row->bi_profimg
		        );

		        $this->session->set_userdata('hoa_user_accepted__', $sess_array);

		        $today = date('Y-m-d H:i:s');

			    $table = "ZHOA_logs";
			    $data = array(
			    	'tl_date' => $today,
			    	'tl_username' => $uname
			    );

			    $this -> Main -> insert_data($table, $data);

		      }

		     /* $table = "ZHOA_byrlogin";
		      $where = "b_id";
		      $id = $sess_array['b_id'];*/

		      /*$data = array(
		          'is_online' => 1
		        );

		      $this -> Enduser -> update_data($where, $table, $id, $data);*/

		      return TRUE;

		    } else  {

		      $today = date('Y-m-d H:i:s');

		      $table = "ZHOA_attempts";
		      $data = array(
		      		'at_date' => $today,
		      		'at_username' => $this -> input -> post('username')
		      	);

		      $this -> Main -> insert_data($table, $data);

		      $this->form_validation->set_message('check_database', "<label class='col-md-12 col-sm-12 col-lg-12 col-xs-12 label label-danger flash-notif' style='font-size: 15px;'> Invalid Username or Password. </label><br><br>");
		      return false;
				  
		    }

		}

		/*function changepass(){

			if(isset($_POST['hoa_changepass'])){

				$email = $this -> input -> post('email');
				$sec1 = $this -> input -> post('sec1');
				$sec2 = $this -> input -> post('sec2');
				$ans1 = $this -> input -> post('ans1');
				$ans2 = $this -> input -> post('ans2');

				$select = "bi_email, bi_sec1, bi_sec2, bi_ans1, bi_sec2 , bi_ans2, bi_owner_id";
				$table = "ZHOA_byrinfo";
				$validation = $this -> Main -> select_data($select, $table);

				$counter=0;
				foreach($validation as $row){

					if($email == $row['bi_email'] AND $sec1 == $row['bi_sec1'] AND $ans1 == $row['bi_ans1'] AND $sec2 == $row['bi_sec2'] AND $ans2 == $row['bi_ans2']){

						$counter++;
						$owner = $row['bi_owner_id'];

					} else {


					}

				}

				if($counter != 1){

					echo "<script> alert('Invalid Email') </script>";
					echo "<script> window.location.href='".base_url('login')."' </script>";

				} else {

					$hash = hash('adler32', date('Y-m-d H:i:s').$owner.'uNiqU3'); //Returns IST 
					
					$where = "b_id";
					$table = "ZHOA_byrlogin";
					$id = $owner;
					$data = array(
						'b_pass' => md5($hash)
						);

					$this -> Main -> update_data($where, $table, $id, $data);

					// $select = "b_id, b_uname";
					$select = "b_uname";
					$where = "b_id = ".$owner;
					$table = "ZHOA_byrlogin";
					$username = $this -> Main -> select_data_where($select, $table, $where);

					foreach($username as $row){

						$uname = $row['b_uname'];

					}

					$my_file = "Do not reply";
					$my_path = $_SERVER['DOCUMENT_ROOT']."/tempfolder/";
					$from="mailer@02camella.com.ph";
				  	$namefrom="02Camella";
				 	$to  = $_POST['email'];	
					$nameto = "Homeowner's Portal";
				 	$subject = "Homeowner's Portal Password Reset";
					$message = "User ID <span style='font-weight: bold;'>$uname</span> requested to reset password.".
								"<br/>".
								"Your new password is ".
								"<span style='font-weight: bold;'>".
								$hash.
								"</span>".
								"<br/>".
								"<br/>".
								"Please change password upon login. <a href='".base_url('login/')."'>Click here to login </a>"
								;

				    $this -> authgMail($from, $namefrom, $to, $nameto, $subject, $message);

				}

			}

		}*/

		function changepword(){

			if(isset($_REQUEST['email'])){

				$email = $_REQUEST['email'];
				$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 

				if ( preg_match( $regex, $email ) ) {
				    
					$email = $_REQUEST['email'];
					$sec1 = $_REQUEST['sec1'];
					$sec2 = $_REQUEST['sec2'];
					$ans1 = $_REQUEST['ans1'];
					$ans2 = $_REQUEST['ans2'];

					$select = "bi_email, bi_sec1, bi_sec2, bi_ans1, bi_sec2 , bi_ans2, bi_owner_id";
					$table = "ZHOA_byrinfo";
					$validation = $this -> Main -> select_data($select, $table);

					$counter=0;
					foreach($validation as $row){

						if($email == $row['bi_email']){

							$counter++;
							$owner = $row['bi_owner_id'];
							$rsec1 = $row['bi_sec1'];
							$rsec2 = $row['bi_sec2'];
							$rans1 = $row['bi_ans1'];
							$rans2 = $row['bi_ans2'];

						} else {


						}

					}

					if($counter != 1){

						echo "<script> alert('Invalid Email. Please try again.') </script>";

					} else {

						if($sec1 == $rsec1 AND $ans1 == $rans1 AND $sec2 == $rsec2 AND $ans2 == $rans2){

							$hash = hash('adler32', date('Y-m-d H:i:s').$owner.'uNiqU3'); //Returns IST 
							
							$where = "b_id";
							$table = "ZHOA_byrlogin";
							$id = $owner;
							$data = array(
								'b_pass' => md5($hash)
								);

							$this -> Main -> update_data($where, $table, $id, $data);

							// $select = "b_id, b_uname";
							$select = "b_uname";
							$where = "b_id = ".$owner;
							$table = "ZHOA_byrlogin";
							$username = $this -> Main -> select_data_where($select, $table, $where);

							foreach($username as $row){

								$uname = $row['b_uname'];

							}

							$email  = $_POST['email'];	
						 	$subject = "Vista HOMe Portal Password Reset";
							$message = "Your Password has been successfully updated.".
											"<br/>".
											"User ID <span style='font-weight: bold;'>$uname</span>".
											"<br/>".
											"Your new password is <span style='font-weight: bold;'>$hash</span>".
											"<br/>".
											"<br/>".
											"<a href='".base_url('login/')."'>Click here to login </a>"
											;



						    $this -> send_email($email, $message, $subject);

							echo "<script> alert('Password has been successfully reset. Please check your email for your new password.'); </script>";
							echo "<script> window.location.href='".base_url('login')."' </script>";

						} else {

							echo "<script> alert('Security information is not valid.'); </script>";

						}	

					}

				} else { 
				    echo "<script> alert('".$email . " is an invalid email. Please try again.'); </script>";
				} 

			}

		}

		function test(){
				$email  = "schristianangelo@gmail.com";	
						 	$subject = "Vista HOMe Portal Password Reset";
							$message = "Your Password has been successfully updated.".
											"<br/>".
											"User ID <span style='font-weight: bold;'>$uname</span>".
											"<br/>".
											"Your temporary password is <span style='font-weight: bold;'>$hash</span>".
											"<br/>".
											"Please change your password upon login".
											"<br/>".
											"<br/>".
											"<a href='".base_url('login/')."'>Click here to login </a>"
											;



						    $this -> send_email($email, $message, $subject);
		}

		function changeuname(){

			if(isset($_REQUEST['email'])){

				$email = $_REQUEST['email'];
				$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 

				if ( preg_match( $regex, $email ) ) {
				    
					$email = $_REQUEST['email'];
					$captcha = $_REQUEST['captcha'];
					$username = $_REQUEST['username'];
					$captcha_session = $_REQUEST['captcha_session'];

					$select = "bi_email, bi_owner_id";
					$table = "ZHOA_byrinfo";
					$validation = $this -> Main -> select_data($select, $table);

					$counter=0;
					foreach($validation as $row){

						if($email == $row['bi_email']){

							$counter++;
							$owner = $row['bi_owner_id'];

						} else {


						}

					}

					if($counter != 1){

						echo "<script> alert('Invalid Email. Please try again.') </script>";

					} else {
						
						if($captcha == $captcha_session){

						/*
							$select = "b_uname";
							$where = "b_id = ".$owner;
							$table = "ZHOA_byrlogin";
							$username = $this -> Main -> select_data_where($select, $table, $where);

							foreach($username as $row){

								$uname = $row['b_uname'];

							}
						*/
							$where = "b_id";
							$table = "ZHOA_byrlogin";
							$id = $owner;
							$data = array(
								'b_uname' => $username
								);

							$this -> Main -> update_data($where, $table, $id, $data);

							// $select = "b_id, b_uname";

							$select = "b_uname";
							$where = "b_id = ".$owner;
							$table = "ZHOA_byrlogin";
							$get_username = $this -> Main -> select_data_where($select, $table, $where);

							foreach($get_username as $row){

								$uname = $row['b_uname'];

							}

						 	$email  = $_POST['email'];	
						 	$subject = "Vista HOMe Portal Change of Username";
							$message = "Your User ID was successfully updated.".
										"<br/>".
										"Your new User ID is <span style='font-weight: bold;'>$uname</span>".
										"<br/>".
										"<br/>".
										"<a href='".base_url('login/')."'>Click here to login </a>"
										;

						    $this -> send_email($email, $message, $subject);

							echo "<script> alert('Username has been successfully changed. Please check your email for your username update.'); </script>";
							echo "<script> window.location.href='".base_url('login')."' </script>";

						} else {

							echo "<script> alert('Captcha didn\'t match and is not valid.'); </script>";

						}	

					}

				} else { 
				    echo "<script> alert('".$email . " is an invalid email. Please try again.'); </script>";
				} 

			}

		}

		/*function changeUsername(){

			if(isset($_POST['hoa_changeuname'])){

				$email = $this->input->post('email');
				$select = "bi_email, bi_owner_id";
				$table = "ZHOA_byrinfo";
				$validation = $this -> Main -> select_data($select, $table);

				$counter=0;
				foreach($validation as $row){

					if($email == $row['bi_email']){

						$counter++;
						$owner = $row['bi_owner_id'];

					} else {


					}

				}

				if($counter != 1){

					echo "<script> alert('Invalid Email') </script>";
					echo "<script> window.location.href='".base_url('login')."' </script>";

				} else {

					if($_SESSION['captcha'] != $this -> input -> post('captcha')){

						echo "<script> alert('Captcha not match!') </script>";
						echo "<script> window.location.href='".base_url('login')."' </script>";

					} else {

						
						$where = "b_id";
						$table = "ZHOA_byrlogin";
						$id = $owner;
						$data = array(
							'b_uname' => $this->input->post('username')
							);

						$this -> Main -> update_data($where, $table, $id, $data);
						
						$select = "b_uname";
						$where = "b_id = ".$owner;
						$table = "ZHOA_byrlogin";
						$username = $this -> Main -> select_data_where($select, $table, $where);

						foreach($username as $row){

							$uname = $row['b_uname'];

						}

						$my_file = "Do not reply";
						$my_path = $_SERVER['DOCUMENT_ROOT']."/tempfolder/";
						$from="mailer@02camella.com.ph";
					  	$namefrom="02Camella";
					 	$to  = $_POST['email'];	
						$nameto = "Homeowner's Portal";
					 	$subject = "Homeowner's Portal Password Reset";
						$message = "Your User ID was successfully updated.".
									"<br/>".
									"Your new User ID is <span style='font-weight: bold;'>$uname</span>".
									"<br/>".
									"<br/>".
									"<a href='".base_url('login/')."'>Click here to login </a>"
									;

					    $this -> authgMail($from, $namefrom, $to, $nameto, $subject, $message);

					    echo "<script> alert('Username has been updated!') </script>";
						echo "<script> window.location.href='".base_url('login')."' </script>";

					}

				}

			}

		}*/

		function send_email($email, $message, $subject){
			
			$this->load->library('email');
			
			$this->email->initialize(array(
					'protocol' => 'smtp',
					'smtp_host' => '192.168.7.83',
					'smtp_user' => '',
					'smtp_pass' => '',
					'smtp_port' => 25,
					'crlf' => "\r\n",
					'newline' => "\r\n",
					'mailtype' => 'html'
			));
			
			$this->email->from('mailer@02camella.com.ph', "Vista HOMe Portal");
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($message);
			if($this->email->send()){
				return true;
			}else{
				return false;
			}
			
		}

	}

/* End of file login.php */
/* Location: ./application/controllers/login.php */