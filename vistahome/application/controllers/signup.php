<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	session_start();

	class Signup extends CI_Controller {

		function __construct(){

			parent::__construct();
			$this -> load -> model('Main');
			date_default_timezone_set("UTC"); 

		}

		function index(){

			if(isset($_SESSION['temp_company'])){
				unset($_SESSION['temp_company']);
			} 

			if(isset($_SESSION['temp_unit'])){
				unset($_SESSION['temp_unit']);
			}

			$select = "sq_id, sq_desc";
			$table = "ZHOA_secquestion";
			$where = "sq_is_deleted = 0";
			$orderby = "sq_desc ASC";
			$data['secquestion'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			/*$select = "tc_id, tc_brand";
			$table = "ZHOA_company";
			$where = "tc_is_deleted = 0";
			$orderby = "tc_brand ASC";
			$data['company'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);*/

			$select = "PROJ_ID, PROJ_AREA";
			$table = "ZHOA_PROJ_AREA";
			$where = "MANDT = 113";
			$orderby = "PROJ_AREA ASC";
			$data['company'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			$select = "u_id, u_desc";
			$table = "ZHOA_unit";
			$where = "u_is_deleted = 0";
			$orderby = "u_desc ASC";
			$data['unit'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			/*$select = "XWETEXT";
			$table = "ZHOA_BUYER_PROF";
			$where = "u_is_deleted = 0";
			$gpby = "XWETEXT";
			$odby = "XWETEXT ASC";
			$data['proj'] = $this -> Main -> select_data_odby_gpby($select, $table, $odby, $gpby);*/

			$data['active'] = "";

			$this -> load -> view('templates/solid-header', $data);
			$this -> load -> view('signup/signup', $data);
			$this -> load -> view('templates/footer');

		}

		function confirmation1(){

			$data['email'] = $this -> input -> post('email');

			$this -> load -> view('templates/solid-header');
			$this -> load -> view('signup/confirmation', $data);
			$this -> load -> view('templates/footer');

		}

		function confirmation(){
			$matching = "";
			if(isset($_POST['submit'])){
				$today = date('Y-m-d h:i:s');
				$get_date =  date('Y-m-d H:i:s'); //Returns IST 
				$pword = md5(hash('adler32', $this->input->post('cust_no').$get_date.'#hash'));
				$uname = $this -> input -> post('email');
				$recapt = $this->input->post('g-recaptcha-response');
				$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfLyw8TAAAAAFqxZkpTKtFu9hP_Psrwvgjtt-PY&response=".$recapt."&remoteip=".$_SERVER['REMOTE_ADDR']);
				$decode = json_decode($response, true);

				if($decode["success"]==true){
					$table = "ZHOA_byrlogin";
					$data = array('b_uname' => $uname,
						'b_pass' => $pword,
						'b_is_validated' => 0,
						'b_is_active' => 0,
						'b_date_reg' => $get_date,
						'b_cpass_date' => $today);

					// $_SESSION['matching'] = md5($get_date.$pword);
					// $matching = md5($get_date.$pword.$uname);				// echo $matching. "====== ";
					$this -> Main -> insert_data($table, $data);

					$select = "";
					$table = "ZHOA_byrlogin";
					$where = "b_date_reg = '".$get_date."' and b_uname = '".$uname."' and b_pass = '".$pword."'";
					$recent = $this -> Main -> select_data_where($select, $table, $where);

					foreach($recent as $row){

						$table = "ZHOA_byrinfo";
						$hash = hash('adler32', date('Y-m-d H:i:s').'uNiqU3'); //Returns IST 
						$data = array(
									'bi_owner_id' => $row['b_id'],
									'bi_cust_no' => $this -> input -> post('cust_no'),
									'bi_company' => $this -> input -> post('company'),
									'bi_unit' => $this -> input -> post('unit'),
									'bi_project' => $this -> input -> post('projtext'),
									'bi_projcode' => $this -> input -> post('project'),
									'bi_blklot' => $this -> input -> post('blklot'),
									'bi_lname' => $this -> input -> post('lname'),
									'bi_fname' => $this -> input -> post('fname'),
									'bi_mname' => $this -> input -> post('mname'),
									'bi_bdate' => $this -> input -> post('bdate'),
									// 'bi_add1' => $this -> input -> post('add1'),
									// 'bi_add2' => $this -> input -> post('add2'),
									// 'bi_province' => $this -> input -> post('province'),
									// 'bi_zip' => $this -> input -> post('zip'),
									'bi_contact' => $this -> input -> post('contact'),
									'bi_email' => $this -> input -> post('email'),
									// 'bi_sec1' => $this -> input -> post('sec1'),
									// 'bi_ans1' => $this -> input -> post('ans1'),
									// 'bi_sec2' => $this -> input -> post('sec2'),
									// 'bi_ans2' => $this -> input -> post('ans2'),
									'bi_regdate' => $get_date,
									'bi_csec_date' => $today,
									'bi_code' => $hash,
									'bi_code_date' => $today,
									'bi_email_verif' => 0,
									'bi_temp_concern' => $this -> input -> post('concern'),
									'bi_temp_concern_date' => $get_date );

						$this -> Main -> insert_data($table, $data);

						$select = "bi_lname, bi_fname, bi_code";
						$table = "ZHOA_byrinfo";
						$where = "bi_owner_id = ".$row['b_id'];
						$get_name = $this -> Main -> select_data_where($select, $table, $where);

						foreach($get_name as $g){
							$fullname = ucwords($g['bi_fname']." ".$g['bi_lname']);
							$code = $g['bi_code'];
						}

						$email = $this -> input -> post('email');
						$data['fullname'] = $fullname;
						$data['code'] = $code;
						$data['email'] = $email;	
						$data['active'] = "";

						$this -> load -> view('templates/solid-header', $data);
						$this -> load -> view('signup/confirmation', $data);
						$this -> load -> view('templates/footer');

						$subject = "Vista HOMe: Verify your Email Address - Response Required";
						$message = "<style> .email_format{ font-family: 'arial'; font-size: 16px; } </style>
									<div class='email_format'>
										Dear Mr. / Ms. <span style=\"font-weight: bold;\">".$fullname."</span> <br/><br/>
										Thank you for registering an account into our Vista HOMe - the Vista Land Homeowner's Portal. Please validate this email address by clicking on the link below. <br/>
										Your code is <span style=\"font-weight: bold;\">".$code.".</span> <br/><br/>
										Thank you, <br/><br/>
										Vista HOMe Team <br/><br/>
										<a href='".base_url('signup/verify_email/')."'>Click here to verfiy your email. </a>
									</div>";
						$this -> send_email($email, $message, $subject);
					}	
				} else{
					redirect(base_url('signup/'), 'refresh');
				}
			} else {
				redirect(base_url('signup/'), 'refresh');
			}
		}



		function check_sec1($id=NULL){
			if($id == NULL){
				show_404();
			} else {
				$select = "sq_desc, sq_id"; $table = "ZHOA_secquestion";
				$where = "sq_id != ".$id;
            	$squestion = $this -> Main -> select_data_where($select, $table, $where);
				echo "<option value=''> -- Select Security Question 2 </option>";
				foreach($squestion as $row1){
                  	echo "<option value=".$row1['sq_id'].">".$row1['sq_desc']."</option>";
                } 
			}
		}

		function check_sec2($id=NULL){
			if($id == NULL){
				show_404();
			} else {
				$select = "sq_desc, sq_id"; $table = "ZHOA_secquestion";
				$where = "sq_id != ".$id;
           		$squestion = $this -> Main -> select_data_where($select, $table, $where);
				echo "<option value=''> -- Select Security Question 1 </option>";
				foreach($squestion as $row1){
                echo "<option value=".$row1['sq_id'].">".$row1['sq_desc']."</option>";
	            } 
			}
		}

		function test(){
			// $email  = "santos.christianangelo@hotmail.com";	
			$email  = "christianangelo_santos@mandalay.com.ph";	
			// $email  = "schristianangelo@gmail.com";	
			$subject = "Vista HOMe: Verify your Email Address - Response Required";
			$message = "Dear Mr. / Ms. <span style=\"font-weight: bold;\">asfd</span>
						<br/><br/>
							Thank you for registering an account into our Vista HOMe - the Vista Land Homeowner's Portal. Please validate this email address by clicking on the link below.
						<br/>
							Your code is <span style=\"font-weight: bold;\">asff</span>
						<br/><br/>
							Thank you,
						<br/><br/>
							Vista HOMe Team
						<br/><br/>
						<a href='".base_url('signup/verify_email/')."'>Click here to verfiy your email. </a>";
			$this -> send_email($email, $message, $subject);
		}

		function verify_email(){
			$data['notification'] = "";
			$today =  date('Y-m-d H:i:s'); //Returns IST 

			if(isset($_POST['verify'])){
				$email = $this -> input -> post('email');
				$code = $this -> input -> post('code');

				$select = "";
				$table = "ZHOA_byrinfo";
				$where = "bi_code = '". $code . "' AND bi_email = '". $email ."'";

				$verification = $this -> Main -> select_data_where($select, $table, $where);

				if(!$verification){
					echo "<script>";
					echo "alert('Invalid Email or Verification Code! Please try again.')";
					echo "</script>";
				} else {
					$counter = 0;
					foreach($verification as $row){
						$fullname = ucfirst($row['bi_fname']." ".$row['bi_lname']);
						$owner_id = $row['bi_owner_id'];
						$email_verif = $row['bi_email_verif'];
						$code_date = $row['bi_code_date'];
						$counter++;
					}
					$code_date = strtotime($code_date);
					$get_today =  date('Y-m-d'); //Returns IST 

					$start_date = new DateTime(date('Y-m-d', $code_date));
					$since_start = $start_date->diff(new DateTime($get_today));

					$code_exp = 0;
					if($since_start->d > 3){
						$code_exp = 1;
					}

					if($counter != 1){
						echo "<script> alert('Invalid Verification Code/Email! Please try again.'); </script>";
					} else {

						if($email_verif == 1){ 
							echo "<script> alert('Your email is already verified. Verification code is already deactivated.'); </script>";
						} 
						if($email_verif == 0 && $code_exp == 1){
							echo "<script> alert('Verification code is already expired. Request a new one.'); </script>";
						} 
						if($email_verif == 0 && $code_exp == 0){
							
							// echo "<script> alert('Verification code is already expired. Request a new one.'); </script>";

							$table = "ZHOA_byrinfo";
							$where = "bi_owner_id";
							$id = $owner_id;
							$data = array(
								'bi_email_verif' => 1,
								'bi_regdate' => $today
								);

							$this -> Main -> update_data($where, $table, $id, $data);

							$email  = $email;	
							$subject = "Vista HOMe: Account Registration";
							$message = 	"<style> .email_format{ font-family: 'arial'; font-size: 16px; } </style>
									<div class='email_format'>
										Dear Mr. / Ms. <span style='font-weight: bold;'>$fullname</span>, <br/> <br/>
										Thank you for validating your email address. <br/> <br/>
										Please allow us to process your registration and validate the information you provided. Expect a feedback on the status of your registration within 24 hours. <br/> <br/>
										Thank you, <br/> <br/>
										Vista HOMe Team <br/> <br/>
										<a href='".base_url()."'>".base_url()."</a>
									</div>" ;
							$this -> send_email($email, $message, $subject);

							echo "<script> alert('Email has been successfully verified.'); </script>";
							echo "<script> window.location.href='".base_url('login')."'; </script>";

						}
					}
				}
			}

			$data['active'] = "";
			$data['notification'] = "";
			$this -> load -> view('templates/solid-header', $data);
			$this -> load -> view('signup/verify_email', $data);
			$this -> load -> view('templates/footer');
		}

		function req_code(){

			$email = $this -> input -> post('email');

			$select = "";
			$table = "ZHOA_byrinfo";
			$where = "bi_email = '". $email ."'";

			$get_email = $this -> Main -> select_data_where($select, $table, $where);

			$counter = 0;
			foreach($get_email as $row){
				$counter++;
				$fname = $row['bi_fname'];
				$lname = $row['bi_lname'];
				$owner_id = $row['bi_id'];
				$code = $row['bi_code'];
			}

			$fullname = $fname . " " . $lname;

			if($counter != 0){
				$today = date('Y-m-d');
				$hash = hash('adler32', date('Y-m-d H:i:s').'uNiqU3'); //Returns IST 
				$table = "ZHOA_byrinfo";
				$where = "bi_id";
				$id = $owner_id;
				$data = array(
					'bi_code' => $hash,
					'bi_code_date' => $today
					);

				$this -> Main -> update_data($where, $table, $id, $data);

				$email  = $email;	
				$subject = "Vista HOMe: Verify your Email Address - Response Required";
				$message = "<style> .email_format{ font-family: 'arial'; font-size: 16px; } </style>
							<div class='email_format'>
								Dear Mr. / Ms. <span style=\"font-weight: bold;\">".$fullname."</span> <br/><br/>
								Thank you for registering an account into our Vista HOMe - the Vista Land Homeowner's Portal. Please validate this email address by clicking on the link below. <br/>
								Your new code is <span style=\"font-weight: bold;\">".$hash.".</span> <br/><br/>
								Thank you, <br/><br/>
								Vista HOMe Team <br/><br/>
								<a href='".base_url('signup/verify_email/')."'>Click here to verfiy your email. </a>
							</div>";
				$this -> send_email($email, $message, $subject);
			} else {
				echo "<script> alert('Invalid Email.'); </script>";
			}
		}

		function get_company($val=NULL){

			if($val == NULL){
				show_404();
			} else {

				$_SESSION['temp_company'] = $val;

				if(isset($_SESSION['temp_company'])){
					$_SESSION['temp_company'] = $val;
				}

				if(!isset($_SESSION['temp_unit'])){ 
						echo "<option value=''>-- SELECT PROJECT --</option>";
				} else {
					$select = "XWETEXT,SWENR";
					$table = "ZHOA_SU";
					$where = "SNUNR = ". $_SESSION['temp_unit'] ." AND PROJ_ID = ". $_SESSION['temp_company'];
					$odby = "XWETEXT ASC";

					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);

					if(!$project){
						echo "<option value=''>-- SELECT PROJECT --</option>";
					} else {
						echo "<option value=''>-- SELECT PROJECT --</option>";
						foreach($project as $p){
							echo "<option value='".$p['SWENR']."'>".$p['XWETEXT']."</option>";
						}
					}
				}
			}
		}

		function get_proj($val=NULL){

			if($val == NULL){
				show_404();
			} else {

				if(isset($_SESSION['temp_unit'])){
					$_SESSION['temp_unit'] = $val;
				}

				$_SESSION['temp_unit'] = $val;

				if(!isset($_SESSION['temp_company'])){ 
						echo "<option value=''>-- SELECT PROJECT --</option>";
				} else {
					$select = "XWETEXT, SWENR";
					$table = "ZHOA_SU";
					$where = "SNUNR = ". $_SESSION['temp_unit'] ." AND PROJ_ID = ". $_SESSION['temp_company'];
					$odby = "XWETEXT ASC";

					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);

					if(!$project){
						echo "<option value=''>-- SELECT PROJECT --</option>";
					} else {
						echo "<option value=''>-- SELECT PROJECT --</option>";
						foreach($project as $p){
							echo "<option value='".$p['SWENR']."'>".$p['XWETEXT']."</option>";
						}
					}
				}
			}
		}

		function check_username($val=NULL){

			if($val == NULL){

				show_404();

			} else {

				$select = "";
				$table = "ZHOA_byrlogin";
				$where = "b_uname ='".$val."'";

				$chck_uname = $this -> Main -> count_select_where($select, $table, $where);	

				$str_val = strlen($val);

				if($str_val >=8){

					if($chck_uname == 0 ){
						
						echo "<div class=\"alert alert-success btn-sm\" role=\"alert\" style='padding:5px;'>
							  	<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
							 	Username is VALID
								</div>
								<input type='hidden' id='validate_uname' value='valid'>
							 ";

					} else {

						echo "<div class=\"alert alert-danger btn-sm\" role=\"alert\" style='padding:5px;'>
							  	<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
							 	INVALID Username
								</div>
								<input type='hidden' id='validate_uname' value='invalid'>
							 ";

					}

				} else {

					echo "<div class=\"alert alert-danger btn-sm\" role=\"alert\" style='padding:5px;'>
							  	<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
							 	Username must be 8 characters and up.
								</div>
							 ";

				}

			}

		}

		function check_email(){

			if($_POST['email']){
				$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 

				if ( preg_match( $regex, $_POST['email'] ) ) { 
					$select = "bi_email";
					$table = "ZHOA_byrinfo";
					$where = "bi_email = '".$_POST['email']."'";

					$check_email = $this -> Main -> select_data_where($select, $table, $where);	
						
					if($check_email){
						echo "<div class=\"alert alert-danger btn-sm\" style=\"padding:5px;\"> 
			                    <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
			                       Email is already exists. Please try again.
		                        <input type=\"hidden\" id=\"validate_email\" value=\"invalid\">
			                  </div> ";
					}else{
						echo "<div class=\"alert alert-success btn-sm\" style=\"padding:5px;\"> 
			                    <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
			                       Email is valid.
		                        <input type=\"hidden\" id=\"validate_email\" value=\"valid\">
			                  </div> ";
						}

				} else {
					echo "<div class=\"alert alert-danger btn-sm\" style=\"padding:5px;\"> 
			                <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
			                   Email is not valid.	
		                    <input type=\"hidden\" id=\"validate_email\" value=\"invalid\">
			              </div> ";
				}
			}
		}

		function check_existing_email(){
			if($_GET['email']){
				$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 

				if ( preg_match( $regex, $_GET['email'] ) ) { 
					$select = "bi_email";
					$table = "ZHOA_byrinfo";

					$check_email = $this -> Main -> select_data($select, $table);	
					
					$counter = 0;
					foreach($check_email as $row){
						if($row['bi_email'] == $_GET['email']){
							$counter++;
						}
					}

					if($counter == 0) {
						echo "<input type=\"hidden\" id=\"validate_email\" value=\"invalid\">";
					} else {
						echo "<input type=\"hidden\" id=\"validate_email\" value=\"valid\">";
					}
				} else {
					echo "<input type=\"hidden\" id=\"validate_email\" value=\"invalid\">";
				}
			}
		}

		function send_email($email, $message, $subject){
			
			$this->load->library('email');
			$this->email->initialize(array(
					'protocol' => 'smtp',
					'smtp_host' => '192.168.7.83',
					'smtp_user' => '',
					'smtp_pass' => '',
					'smtp_port' => 25,
					'crlf' => "\r\n",
					'newline' => "\r\n",
					'mailtype' => 'html'
			));
			$this->email->from('noreply@vistahome.com.ph', "Vista HOMe Portal");
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($message);
			if($this->email->send()){
				return true;
			}else{
				return false;
			}
			
		}

	}


/* End of file signup.php */
/* Location: ./application/controllers/signup.php */