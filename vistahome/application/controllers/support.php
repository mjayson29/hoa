<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	ini_set('session.gc_maxlifetime', 86400);
	session_set_cookie_params(86400);
	session_start();

	class Support Extends CI_Controller{

		function  __construct() {
			parent::__construct();
			date_default_timezone_set("UTC");
			$this->load->library('form_validation');
			$this -> load -> model('Admin_User_Model');
			$this -> load -> model('Buyer_Model');
			$this -> load -> model('Main');
			$this->load->library('Pagination');
			$this->load->library('session');
		}
		
		function index(){
			if($this->session->userdata('hoa_admin_accepted__')){
				$this->forward();
			} else {
			$select = "c_id, c_brandname, c_filename";
			$table = "ZHOA_carousel";
			$data['carousel'] = $this -> Main -> select_data($select, $table);
			$data['count'] = $this -> Main -> count_select($select, $table);
			
			$data['active'] = "";

			$this -> load -> view('templates/header',$data);
			$this -> load -> view('login/admin_login', $data);
			$this -> load -> view('templates/footer');
			}
		}

		function forward(){
			$session_data = $this->session->userdata('hoa_admin_accepted__'); 
		    $access = $session_data['ui_access'];
		    $acc = explode("-", $access);
			$reg=$acc[0];
			$supp=$acc[2];
			$rpt=$acc[4];
			$mtc=$acc[6];
			$ho=$acc[7];
			$accnt=$acc[8];
			$usr=$acc[9];
			if($reg==1){
		        redirect(base_url('support/reg_list/'), 'refresh');
	    	} elseif($supp==1){
		        redirect(base_url('support/support_page/'), 'refresh');
	    	}elseif($mtc==1){
		        redirect(base_url('support/carousel/'), 'refresh');
	    	}elseif($ho==1){
		        redirect(base_url('support/homeownerList/'), 'refresh');
	    	}elseif($accnt==1){
		        redirect(base_url('support/account_settings/'), 'refresh');
	    	}elseif($usr==1){
		        redirect(base_url('support/users/'), 'refresh');
	    	}
		}

		function myProfile(){
			$data = $this->get_notif();

			$select = "a.ai_fname, a.ai_lname, a.ai_spark, b.a_uname, b.a_email, b.a_profimg";
			$table = "ZHOA_adminfo as a";
			$orderby = "a.ai_fname ASC";
			$join1 = "ZHOA_admlogin as b";
			$join2 = "a.ai_id = b.a_id";
			$where = "a.ai_id = ".$data['sess_id'];
			$data['get_profile'] = $this -> Main -> get_data_where_join($select, $table, $orderby, $join1, $join2, $where);
			
			$data['active'] = "myProfile";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/myProfile', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

		function upload($id=NULL){
			if($id == NULL){
				show_404();
			} else {
		
				$config['upload_path'] = './assets/img/admin/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']	= '2097152';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';

				$this->load->library('upload', $config);
				$select = "a_profimg"; $table = "ZHOA_admlogin"; $where = "a_id = ".$id;
				$profimg = $this -> Main -> select_data_where($select, $table, $where);
				foreach($profimg as $row){ 
					$filename = $row['a_profimg']; 
				}
				if ( ! $this->upload->do_upload()) {

					$error = array('error' => $this->upload->display_errors());
					echo $error['error'];
						
					if($filename != "") {
						$filename = $row['a_profimg'];
					} else {
						$filename = "official.png";
					}
					echo "<img id=\"img-circle\" src=\"".base_url()."assets/img/admin/".$filename."\" class=\"prof-img\"> 
				          <input type=\"hidden\" id=\"prop_img\" value='".$filename."'>
                  			<label for=\"userfile\">
                    			<span class=\"glyphicon glyphicon-camera profile_img\" title=\"upload\"></span>
                  			</label>
                  			<span class=\"glyphicon glyphicon-remove profile_img rm\" title=\"remove\"></span>";
				} else {
					$upload_data = $this->upload->data();
					if($filename!="official.png"){
						unlink($config['upload_path'].$filename);
					}
					$where = "a_id"; $table = "ZHOA_admlogin";
					$data = array( 'a_profimg' => $upload_data['file_name'] );
					$this -> Main -> update_data($where, $table, $id, $data);
					echo "<img id=\"img-circle\" src=\"".base_url()."assets/img/admin/".$upload_data['file_name']."\" class=\"prof-img\"> 
				          <input type=\"hidden\" id=\"prop_img\" value='".$upload_data['file_name']."'>
                  		  <label for=\"userfile\">
                   			<span class=\"glyphicon glyphicon-camera profile_img\" title=\"upload\"></span>
                  	      </label>
                  		  <span class=\"glyphicon glyphicon-remove profile_img rm\" title=\"remove\"></span>";
				}
			}
		}

		function dP_def($id=NULL){
			$img = $this->input->post('curr_img');
			$path = './assets/img/admin/'.$img;
			unlink($path);
			$where = "a_id"; $table = "ZHOA_admlogin";
			$data = array('a_profimg' => "official.png" );
			$this -> Main -> update_data($where, $table, $id, $data);
			echo "<img id=\"img-circle\" src=\"".base_url()."assets/img/admin/official.png\" class=\"prof-img\"> 
			       <input type=\"hidden\" id=\"prop_img\" value='official.png'>
            		<label for=\"userfile\">
               			<span class=\"glyphicon glyphicon-camera profile_img\" title=\"upload\"></span>
                  	</label>
                  	<span class=\"glyphicon glyphicon-remove profile_img rm\" title=\"remove\"></span>";
		}

			function changepword(){
				if(isset($_REQUEST['email'])){
					$email = $_REQUEST['email'];
					$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 
					if ( preg_match( $regex, $email ) ) {
						$email = $_REQUEST['email'];
						$select = "a_email, a_id";
						$table = "ZHOA_admlogin";
						$validation = $this -> Main -> select_data($select, $table);
						$counter=0;
						foreach($validation as $row){
							if($email == $row['a_email']){
								$counter++;
								$owner = $row['a_id'];
							}
						}
						if($counter != 1){
							echo "<script> alert('Invalid Email. Please try again.') </script>";
						} else {
							// if($sec1 == $rsec1 AND $ans1 == $rans1 AND $sec2 == $rsec2 AND $ans2 == $rans2){
							$hash = hash('adler32', date('Y-m-d H:i:s').$owner.'uNiqU3'); //Returns IST 	
							$where = "a_id";
							$table = "ZHOA_admlogin";
							$id = $owner;
							$data = array(
								'a_pass' => md5($hash));
							$this -> Main -> update_data($where, $table, $id, $data);
							// $select = "b_id, b_uname";
							$select = "a_uname";
							$where = "a_id = ".$owner;
							$table = "ZHOA_admlogin";
							$username = $this -> Main -> select_data_where($select, $table, $where);
							foreach($username as $row){
								$uname = $row['a_uname'];
							}
							$email  = $_POST['email'];	
						 	$subject = "Vista HOMe Portal Change of Password";
							$message =  "<style> .email_format{ font-family: 'arial';  font-size: 16px;} </style>
										<div class='email_format'>
											Your Password has been successfully updated. <br/>
											User ID <span style='font-weight: bold;'>$uname</span> <br/>
											Your new password is <span style='font-weight: bold;'>$hash</span> <br/> <br/>
											<a href='".base_url('support/')."'>Click here to login </a>
										</div>";
							$this -> send_email($email, $message, $subject);
							echo "<script> alert('Password has been successfully reset. Please check your email for your new password.'); </script>";
							echo "<script> window.location.href='".base_url('support')."' </script>";
							// } else {
								// echo "<script> alert('Security information is not valid.'); </script>";
							// }	
						}
					} else { 
					    echo "<script> alert('".$email . " is an invalid email. Please try again.'); </script>";
					} 
				}
			}

			function changeuname(){
				if(isset($_REQUEST['email'])){
					$email = $_REQUEST['email'];
					$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 
					if ( preg_match( $regex, $email ) ) {
						$email = $_REQUEST['email'];
						$captcha = $_REQUEST['captcha'];
						$username = $_REQUEST['username'];
						$captcha_session = $_REQUEST['captcha_session'];
						$select = "bi_email, bi_owner_id";
						$table = "ZHOA_byrinfo";
						$validation = $this -> Main -> select_data($select, $table);
						$counter=0;
						foreach($validation as $row){
							if($email != $row['bi_email']){
								$counter++;
								$owner = $row['bi_owner_id'];
							}
						}
						if($counter != 1){
							echo "<script> alert('Invalid Email. Please try again.') </script>";
						} else {
							if($captcha == $captcha_session){
							$where = "b_id";
							$table = "ZHOA_byrlogin";
							$id = $owner;
							$data = array(
								'b_uname' => $username);
							$this -> Main -> update_data($where, $table, $id, $data);
							// $select = "b_id, b_uname";
							$select = "b_uname";
							$where = "b_id = ".$owner;
							$table = "ZHOA_byrlogin";
							$get_username = $this -> Main -> select_data_where($select, $table, $where);
							foreach($get_username as $row){
								$uname = $row['b_uname'];
							}
						 	$email  = $_POST['email'];	
						 	$subject = "Vista HOMe Portal Change of Username";
							$message =  "<style>
										.email_format{
										font-family: 'arial'; 
				  						font-size: 16px;
										}
										</style>
										<div class='email_format'>".
										"Your User ID was successfully updated.".
										"<br/>".
										"Your new User ID is <span style='font-weight: bold;'>$uname</span>".
										"<br/>".
										"<br/>".
										"<a href='".base_url('login/')."'>Click here to login </a>".
										"</div>";
							$this -> send_email($email, $message, $subject);
							echo "<script> alert('Username has been successfully changed. Please check your email for your username update.'); </script>";
							echo "<script> window.location.href='".base_url('login')."' </script>";
							} else {
								echo "<script> alert('Captcha didn\'t match and is not valid.'); </script>";
							}	
						}
					} else { 
					    echo "<script> alert('".$email . " is an invalid email. Please try again.'); </script>";
					} 
				}
			}

		function verify_login(){
			if($_POST){
			    $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean');
			    $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|callback_check_admin');
				$this->form_validation->set_error_delimiters('', '');
			    if($this->form_validation->run() == FALSE) {
			        //Field validation failed.  User redirected to login page
			    	$select = "c_id, c_brandname, c_filename";
					$table = "ZHOA_carousel";
					$data['carousel'] = $this -> Main -> select_data($select, $table);
					$data['count'] = $this -> Main -> count_select($select, $table);
					$data['active'] = "";
			    	$this -> load -> view('templates/header',$data);
					$this -> load -> view('login/admin_login', $data);
					$this -> load -> view('templates/footer');
			    } else  {
			    	$this->forward();
			    }
			} else{
				show_404();
			}
		}

		function check_admin($password) {
			$uname = $this -> input -> post('username');
			$result = $this -> Admin_User_Model -> get_user($uname, $password,1,1);
			if($result) {
				$sess_array = array();
				foreach($result as $row) {
					$id = $row->a_id;
					$role = $row->ur_roleid;
					$sess_array = array(
							'sess_id' => $row->a_id,
							'sess_uname' => $row->a_uname,
							'ui_access' => $row->ur_permission,
							'ui_role' => $row->ur_roleid);
					$this->session->set_userdata('hoa_admin_accepted__', $sess_array);
				}
				$this->set_session($role, $id);
				return TRUE;
			} else  {
				$this->form_validation->set_message('check_admin', "<label class='col-md-12 col-sm-12 col-lg-12 col-xs-12 label label-danger flash-notif' style='font-size: 15px;'> Invalid Username or Password. </label><br><br>");
				return false;
			}
		}

		function set_session($role, $id){
			if($role != 1){
				if(isset($_SESSION['BE'])){
					unset($_SESSION['BE']);
				}
				$select = "ar_pcode";
				$table = "ZHOA_assign_role";
				$wher = "ar_usrid =".$id;
				$BE = $this -> Main -> select_data_where($select, $table, $wher);
				if($BE) {
					$pcode ="";
					foreach ($BE as $proj) {
						$pcode .= " {$proj['ar_pcode']}, ";
					}
					$where = substr($pcode, 0, -2);
					$_SESSION['BE'] = $where;
					$data = 1;
				}else{
					$data = 0;
				}
			} else{
				$_SESSION['BE'] = "";
				$data = 1;
			}
			return $data;
		}
		function get_notif(){
			$session_data = $this->session->userdata('hoa_admin_accepted__'); 
			$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
			if(isset($_SESSION['BE'])) {
				$data['home_new_counter'] =$this->Buyer_Model->get_new_reg_rows($_SESSION['BE']);
				$data['home_new_ticket'] =$this->Buyer_Model->get_new_concerns_rows($_SESSION['BE']);
				$data['mycompany'] = $_SESSION['BE'];
			} else {
				$dat = $this->set_session($data['role'], $data['sess_id']);
				if($dat ==0){
					$data['home_new_counter'] = 0; 
					$data['home_new_ticket'] = 0;
					$data['mycompany'] = "";
				}else{
					$data['home_new_counter'] =$this->Buyer_Model->get_new_reg_rows($_SESSION['BE']);
					$data['home_new_ticket'] =$this->Buyer_Model->get_new_concerns_rows($_SESSION['BE']);
					$data['mycompany'] = $_SESSION['BE'];
				}
			}
			return $data;
		}

		function homeownerList(){
			$this->check_access($x=7);
			$data = $this->get_notif();

			$select = "PROJ_ID, PROJ_AREA";
			$table = "ZHOA_PROJ_AREA";
			$where = "MANDT = 113";
			$orderby = "PROJ_AREA ASC";
			$data['proj_area'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);

			$data['active'] = "homeownerList";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/homeownerList_page', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}	

			function homeowner_new($offset=0){
				$this->check_access($x=7);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	   'sess_uname' => $session_data['sess_uname'],
						  	   'access' => $session_data['ui_access'],
					  	  	   'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/homeowner/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_new_ownerlist_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['new']= $this -> Buyer_Model -> get_new_ownerlist($config["per_page"],$offset,$_SESSION['BE']);
				    $data["new_links"] = $this->pagination->create_links();
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				}else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/homeowner/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["new_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['new']="";
					}else{
						$this -> homeowner_new($offset=0);
					}
				}
				$this -> load -> view('support/homeowner', $data);
			}

			function ownerinfo($id){
				$this->check_access($x=7);
				$data = $this->get_notif();

				$data['buyer']= $this->Buyer_Model->get_buyer_by_id($id);
				$data['active'] = "homeownerList";
				$this -> load -> view('templates/sup-templates/header', $data);
				$this -> load -> view('support/ownerinfo', $data);
				$this -> load -> view('templates/sup-templates/footer');
			}

		function users() {
			$this->check_access($x=9);
			$data = $this->get_notif();

			$select = "PROJ_ID, PROJ_AREA";
			$table = "ZHOA_PROJ_AREA";
			$where = "MANDT = 113";
			$orderby = "PROJ_AREA ASC";
			$data['proj_area'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
			
			$data['active'] = "users";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/users', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}	

			function users_list($offset=0) {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if($session_data['ui_role'] == 1){
					$where = 0;
				}else{
					$where = $session_data['sess_id'];
				}
				$config['base_url'] = base_url('support/users_list/0');
				$config['per_page'] = $limit;
				$config['total_rows'] = $this -> Buyer_Model -> get_userlist_rows($where);
				$this->pagination->initialize($config);
				$data['new']= $this -> Buyer_Model -> get_userlist($config["per_page"],$offset,$where);
			    $data["new_links"] = $this->pagination->create_links();
				$data['total_pages'] = $config['total_rows'];
				$data['display_offset'] = $offset + 1;
				$data['display_limit'] = $offset + $limit;
				if($data['display_limit'] > $data['total_pages']){
					$data['display_limit'] = $data['total_pages'];
				}
				if($data['total_pages'] == 0){
					$data['display_offset'] = $offset;
					$data['display_limit'] = 0;
				}
				$this -> load -> view('support/users_list', $data);
			}

			function search_users($offset=0){
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST){
					$data['keyword'] = $this -> input -> post('keyword');
					$data['project'] = $this -> input -> post('project');
					$data['company'] = $this -> input -> post('company');
					$limit = 10;
					$config['base_url'] = base_url('support/search_users/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this->Buyer_Model->get_search_users_rows($data['company'], $data['project'], $data['keyword']);
					$this->pagination->initialize($config);
					$data['search'] = $this->Buyer_Model->get_search_users($limit, $offset, $data['company'], $data['project'], $data['keyword']);
				    $data["search_links"] = $this->pagination->create_links();  
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
					$this -> load -> view('support/search_users', $data);
				}
			}

		function projects() {
			$this->check_access($x=9);
			$data = $this->get_notif();

			$select = "BUKRS, BUTXT, SWENR, XWETEXT";
			$table = "ZHOA_SU";
			$orderby = "BUTXT ASC";
			$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
			$data['active'] = "users";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/projects', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function businessGroup($offset=0) {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				 $data = array('sess_id' => $session_data['sess_id'],
						  	   'sess_uname' => $session_data['sess_uname'],
						  	   'access' => $session_data['ui_access'],
						  	   'role' => $session_data['ui_role']);
				$limit = 10;
				$config['base_url'] = base_url('support/businessGroup/0');
				$config['per_page'] = $limit;
				$config['total_rows'] = $this->Buyer_Model->get_projects_rows();
				$this->pagination->initialize($config);
				$data['new'] = $this->Buyer_Model->get_projects($config['per_page'], $offset);
			    $data["new_links"] = $this->pagination->create_links(); 
				$data['total_pages'] = $this->Buyer_Model->get_projects_rows();
				$data['display_offset'] = $offset + 1;
				$data['display_limit'] = $offset + $limit;
				if($data['display_limit'] > $data['total_pages']){
					$data['display_limit'] = $data['total_pages'];
				}
				if($data['total_pages'] == 0){
					$data['display_offset'] = $offset;
					$data['display_limit'] = 0;
				}
				$this -> load -> view('support/businessGroup', $data);
			}

		function roles() {
			$this->check_access($x=9);
			$data = $this->get_notif();

			$select = "BUKRS, BUTXT, SWENR, XWETEXT";
			$table = "ZHOA_SU";
			$orderby = "BUTXT ASC";
			$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
			$data['active'] = "users";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/roles', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function load_roles($offset=0) {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				 $data = array('sess_id' => $session_data['sess_id'],
						  	   'sess_uname' => $session_data['sess_uname'],
						  	   'access' => $session_data['ui_access'],
						  	   'role' => $session_data['ui_role']);
				$limit = 10;
				$where = 0;
				if($session_data['ui_role'] == 1){
					$where = 1;
				}
				$config['base_url'] = base_url('support/load_roles/0');
				$config['per_page'] = $limit;
				$config['total_rows'] = $this->Buyer_Model->get_roles_rows($where);
				$this->pagination->initialize($config);
				$data['new'] = $this->Buyer_Model->get_roles($config['per_page'], $offset, $where);
			    $data["new_links"] = $this->pagination->create_links();    
				$data['total_pages'] = $config['total_rows'];
				$data['display_offset'] = $offset + 1;
				$data['display_limit'] = $offset + $limit;
				if($data['display_limit'] > $data['total_pages']){
					$data['display_limit'] = $data['total_pages'];
				}
				if($data['total_pages'] == 0){
					$data['display_offset'] = $offset;
					$data['display_limit'] = 0;
				}
				$this -> load -> view('support/roles_table', $data);
			}

			function edit_roles() {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				 $data = array('sess_id' => $session_data['sess_id'],
						  	   'sess_uname' => $session_data['sess_uname'],
						  	   'access' => $session_data['ui_access'],
						  	   'role' => $session_data['ui_role']);
				if($_POST) {
					$where = $this->input->post('id'); 
					$select = "";
					$table = "ZHOA_role_business";
					$saan = "rb_grp_id = ".$where;
					$data['new'] = $this->Buyer_Model->get_roles_where($where);
					$data['bussEnt'] = $this->Main->select_data_where($select, $table, $saan);
					$select = "BUKRS, BUTXT";
					$table = "ZHOA_SU";
					$orderby = "BUTXT ASC";
					$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
					$this -> load -> view('support/view_roles', $data);
				}
			}

			function edit_users($saan) {
				$this->check_access($x=9);
				$data = $this->get_notif();

				$limit = 1000;
				$offset = 0;
				$where = $data['sess_id'];
				$data['role'] = $this -> Buyer_Model -> get_roles($limit,$offset,$where);
				$data['where'] = $saan;
				
				$data['grpComp'] = $this -> Buyer_Model -> get_distinct_grpCo_where($saan);
				$data['active'] = "users";
				$this -> load -> view('templates/sup-templates/header', $data);
				$this -> load -> view('support/edit_users', $data);
				$this -> load -> view('templates/sup-templates/footer');
			}

			function usrGrp($id) {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				$data['usrid'] = $id;
				$data['grpTable'] = $this -> Buyer_Model -> get_bsGrp_where($id);
				$this -> load -> view('support/businessGroupTable', $data);

			}

			function user_info($saan) {
				$this->check_access($x=9);
				$data = $this->get_notif();

				$data['grpComp'] = $this -> Buyer_Model -> get_distinct_grpCo_where($saan);
				$data['where'] = $saan;
				$data['active'] = "users";
				$this -> load -> view('templates/sup-templates/header', $data);
				$this -> load -> view('support/user_info', $data);
				$this -> load -> view('templates/sup-templates/footer');
			}

			function editBusinessGroup() {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST) {
					$where = $this->input->post('id'); 
					$select = "";
					$table = "ZHOA_role_business";
					$saan = "rb_grp_id = ".$where;
					$data['bussEnt'] = $this->Main->select_data_where($select, $table, $saan);
					$select = "BUKRS, BUTXT";
					$table = "ZHOA_SU";
					$orderby = "BUTXT ASC";
					$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
					$this -> load -> view('support/editBusinessGroup', $data);
				}
			}

			function deleteroles() {
				if($_POST) {
					$id = $this->input->post('id');
					$num = $this->Buyer_Model->get_used_role_rows($id);
					if($num > 0) {
						echo 0;
					} else {
						$table = "ZHOA_usraccess";
						$where = "ua_id";
						$this->Main->delete_data_where($where, $id, $table);
						echo 1;
					}
				}
			}

			function deleteBusinessGroup() {
				if($_POST) {
					$id = $this->input->post('grpid');
					$num = $this->Buyer_Model->get_used_group_rows($id);
					if($num > 0) {
						echo 0;
					} else {
						$table = "ZHOA_role_business";
						$where = "rb_grp_id";
						$this->Main->delete_data_where($where, $id, $table);
						echo 1;
					}
				}
			}

			function del_usr() {
				if($_POST) {
					$id = $this->input->post('usrId');
					$table = "ZHOA_adminfo";
					$where = "ai_id";
					$this->Main->delete_data_where($where, $id, $table);
					$table = "ZHOA_admlogin";
					$where = "a_id";
					$this->Main->delete_data_where($where, $id, $table);
					redirect(base_url('support/users_list'));
				}
			}

			function del_usrgrp() {
				if($_POST) {
					$usrid = $this->input->post('usrId');
					$grpid = $this->input->post('grpId');
					$this->Main->delete_usrBusinessGroup($usrid, $grpid);
					redirect(base_url('support/usrGrp/'.$usrid));
				}
			}

			function edit_usrgrp() {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST) {
					$data['usrid'] = $this->input->post('usrId');
					$data['grpid'] = $this->input->post('grpId');
					$data['grpBE'] = $this->Buyer_Model->get_specific_BE($data['usrid'], $data['grpid']);
					$data['bussEnt'] = $this->Buyer_Model->get_usrBE($data['usrid'], $data['grpid']);
					$select = "rb_name";
					$table = "ZHOA_role_business";
					$where = "rb_grp_id =".$data['grpid'];
					$odby = "rb_name";
					$data['grpname'] =$this->Main->get_data_distinct_where($select, $table, $where, $odby);
					$select = "BUKRS, BUTXT";
					$table = "ZHOA_SU";
					$orderby = "BUTXT ASC";
					$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
					$this -> load -> view('support/editBusinessGroup', $data);
				}
			}

			function list_roles() {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST) {
					$where = $this->input->post('id'); 
					$data['new'] = $this->Buyer_Model->get_roles_where($where);
					$select = "BUKRS, BUTXT";
					$table = "ZHOA_SU";
					$orderby = "BUTXT ASC";
					$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
					$this -> load -> view('support/list_roles', $data);
				}
			}

			function list_BG() {
				if($_POST) {
					$where = $this->input->post('id');
					$select = "rb_ccode, rb_cname, rb_name, rb_grp_id";
					$table = "ZHOA_role_business";
					$saan = "rb_grp_id = ".$where;
					$odby = "rb_cname asc";
					$cname;
					$bg = $this->Main->get_data_distinct_where($select, $table, $saan, $odby);
					echo "<script>$('#grpid').val('$where');</script>";
					foreach ($bg as $key) {
						$cname = $key['rb_name'];
						echo "<optgroup value='".$key['rb_ccode']."' label='".$key['rb_cname']."'>";
						$select = "rb_pcode, rb_pname";
						$table = "ZHOA_role_business";
						$where = "rb_ccode = '".$key['rb_ccode']."' AND rb_grp_id = '".$key['rb_grp_id']."'";
						$odby = "rb_pname ASC";
						$grouProj = $this->Main->get_data_where($select, $table, $where, $odby);
						foreach ($grouProj as $value) {
							echo "<option value='".$value['rb_pcode']."'>".$value['rb_pname']."</option>";
						}
						echo "</optgroup>";
						echo "<script>$('#bgrpname').val('$cname');</script>";
					}
				}
			}

			function updateroles() {
				if($_POST) {
				    $id=$this->input->post('id');
                    $rname=$this->input->post('rname');
                    $regview=$this->input->post('regview');
                    $regconf=$this->input->post('regconf');
                    $suppview=$this->input->post('suppview');
                    $suppreply=$this->input->post('suppreply');
                    $rptview=$this->input->post('rptview');
                    $rptexpo=$this->input->post('rptexpo');
                    $mtc=$this->input->post('mtc');
                    $carview=$this->input->post('carview');
                    $annview=$this->input->post('annview');
                    $soaview=$this->input->post('soaview');
                    $hoview=$this->input->post('hoview');
                    $accnt=$this->input->post('accnt');
                    $usrview=$this->input->post('usrview');
                    $usredit=$this->input->post('usredit');
                    $usrdel=$this->input->post('usrdel');
                    $usradd=$this->input->post('usradd');
                    $roleadd=$this->input->post('roleadd');
                    $roleedit=$this->input->post('roleedit');
                    $roledel=$this->input->post('roledel');
                    $concern=$this->input->post('concern');
                    $inq=$this->input->post('inq');
                    $req=$this->input->post('req');
                    $suppcomm=$this->input->post('suppcomm');
					$session_data = $this->session->userdata('hoa_admin_accepted__');
                    $today = date('Y-m-d');
                    $where="ua_id";
					$table="ZHOA_usraccess";
					$data=array("ua_rname"=>$rname, 
								"ua_regview"=>$regview, 
								"ua_regconfirm"=>$regconf, 
								"ua_suppview"=>$suppview, 
								"ua_suppreply"=>$suppreply, 
								"ua_rptview"=>$rptview, 
								"ua_rptexport"=>$rptexpo, 
								"ua_mtcview"=>$mtc, 
								"ua_carview"=>$carview, 
								"ua_annview"=>$annview, 
								"ua_soaview"=>$soaview, 
								"ua_hoview"=>$hoview, 
								"ua_settings"=>$accnt, 
								"ua_usrview"=>$usrview, 
								"ua_usredit"=>$usredit, 
								"ua_usrdel"=>$usrdel, 
								"ua_usradd"=>$usradd, 
								"ua_roleadd"=>$roleadd, 
								"ua_roleedit"=>$roleedit, 
								"ua_roledel"=>$roledel, 
								"ua_suppcomm"=>$suppcomm,
								"ua_updby" => $session_data['sess_id'], 
								"ua_update"=>$today,  
								"ua_concern"=>$concern, 
								"ua_inq"=>$inq, 
								"ua_req"=>$req);
					$this -> Main -> update_data($where, $table, $id, $data);
					redirect(base_url('support/load_roles'));
				}
			}

			function saverole() {
				if($_POST) {
					$projArr = $this->input->post('bussEnt');
					$rname=$this->input->post('rname');
		            $regview=$this->input->post('regview');
		            $regconf=$this->input->post('regconf');
		            $suppview=$this->input->post('suppview');
		            $suppreply=$this->input->post('suppreply');
		            $rptview=$this->input->post('rptview');
		            $rptexpo=$this->input->post('rptexpo');
		            $mtc=$this->input->post('mtc');
		            $carview=$this->input->post('carview');
                    $annview=$this->input->post('annview');
                    $soaview=$this->input->post('soaview');
		            $hoview=$this->input->post('hoview');
		            $accnt=$this->input->post('accnt');
		            $usrview=$this->input->post('usrview');
   		            $usredit=$this->input->post('usredit');
		            $usrdel=$this->input->post('usrdel');
		  	        $usradd=$this->input->post('usradd');
		            $roleadd=$this->input->post('roleadd');
		            $roleedit=$this->input->post('roleedit');
		            $roledel=$this->input->post('roledel');
		            $concern=$this->input->post('concern');
		            $inq=$this->input->post('inq');
		            $req=$this->input->post('req');
		            $suppcomm=$this->input->post('suppcomm');
					$session_data = $this->session->userdata('hoa_admin_accepted__');
		            $today = date('Y-m-d');
					$table="ZHOA_usraccess";
					$data=array('ua_rname'=>$rname, 
								'ua_regview'=>$regview, 
								'ua_regconfirm'=>$regconf, 
								'ua_suppview'=>$suppview, 
								'ua_suppreply'=>$suppreply, 
								'ua_rptview'=>$rptview, 
								'ua_rptexport'=>$rptexpo, 
								'ua_mtcview'=>$mtc, 
								"ua_carview"=>$carview, 
								"ua_annview"=>$annview, 
								"ua_soaview"=>$soaview, 
								'ua_hoview'=>$hoview, 
								'ua_settings'=>$accnt, 
								'ua_usrview'=>$usrview, 
								'ua_usredit'=>$usredit, 
								'ua_usrdel'=>$usrdel, 
								'ua_usradd'=>$usradd, 
								'ua_roleadd'=>$roleadd, 
								'ua_roleedit'=>$roleedit, 
								'ua_roledel'=>$roledel, 
								"ua_suppcomm"=>$suppcomm,									
								'ua_updby' => $session_data['sess_id'], 
								'ua_update'=>$today,  
								'ua_concern'=>$concern, 
								'ua_inq'=>$inq, 
								'ua_req'=>$req,
								'ua_createdby'=>$session_data['sess_id'], 
								'ua_createdate'=>$today);
					$this -> Main -> insert_data($table, $data);
					redirect(base_url('support/load_roles'));
				}
			}

			function savebusinessgrp() {
				if($_POST) {
					$projArr = $this->input->post('bussEnt');
         		    $grpname=$this->input->post('rname');
					$select = "";
					$table = "ZHOA_role_business";
					$start = 0;
					$limit = 1;
					$odby ="rb_grp_id desc";
					$today = date('Y-m-d');
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$result = $this->Main->select_data_odby_l($select, $table, $odby, $start, $limit);
					$id;
					if($result) {
						foreach ($result as $mj) {
							if($mj['rb_grp_id']==null) {
								$id = 1;
							} else {
								$id=$mj['rb_grp_id']+1;
							}
						}
					} else {
						$id = 1;
					}
					if($projArr){
						foreach ($projArr as $key => $value) {
							$select = "BUKRS, BUTXT";
							$table = "ZHOA_SU";
							$where = "SWENR = ".$key;
							$odby = "BUKRS ASC";
							$getComp = $this->Main->get_data_distinct_where($select, $table, $where, $odby);
							foreach ($getComp as $ke) {
								$table = "ZHOA_role_business";
								$data = array('rb_grp_id' => $id, 
											  'rb_name' => $grpname, 
											  'rb_ccode' => $ke['BUKRS'], 
											  'rb_cname' => $ke['BUTXT'] , 
											  'rb_pcode' => $key,
											  'rb_pname' => $value, 
											  'rb_createdby' => $session_data['sess_id'], 
											  'rb_createdate' => $today, 
											  'rb_updby' => $session_data['sess_id'], 
											  'rb_update' => $today);
								$this->Main->insert_data($table, $data);
							}
						}
						redirect(base_url('support/businessGroup/'));
					}
				}
			}

			function updateBusinessGroup() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$projArr = $this->input->post('bussEnt');
					$grpid = $this->input->post('grpid');
					$grpname = $this->input->post('gname');
					$select = "rb_grp_id, rb_createdby, rb_createdate";
					$where = "rb_grp_id = ".$grpid;
					$table = "ZHOA_role_business";
					$odby = "rb_grp_id ASC";
					$today = date('Y-m-d');
					$result= $this->Main->get_data_distinct_where($select, $table, $where, $odby);
					$createby;
					$createdate;
					foreach ($result as $data) {
						$createby = $data['rb_createdby'];
						$createdate = $data['rb_createdate'];
					}
					$where="rb_grp_id";
					$this->Main->delete_data_where($where, $grpid, $table);
					if($projArr){
						foreach ($projArr as $key => $value) {
							$select = "BUKRS, BUTXT";
							$table = "ZHOA_SU";
							$where = "SWENR = ".$key;
							$odby = "BUKRS ASC";
							$getComp = $this->Main->get_data_distinct_where($select, $table, $where, $odby);
							foreach ($getComp as $ke) {
								$table = "ZHOA_role_business";
								$data = array('rb_grp_id' => $grpid, 
											  'rb_name' => $grpname, 
											  'rb_ccode' => $ke['BUKRS'], 
											  'rb_cname' => $ke['BUTXT'] , 
											  'rb_pcode' => $key,
											  'rb_pname' => $value, 
											  'rb_createdby' => $createby, 
											  'rb_createdate' => $createdate, 
											  'rb_updby' => $session_data['sess_id'], 
											  'rb_update' => $today);
								$this->Main->insert_data($table, $data);
							}
						}
						redirect(base_url('support/businessGroup/'));
					}
				}
			}

			function updateUsrGrp() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$projArr = $this->input->post('bussEnt');
					$usrid = $this->input->post('usrid');
					$grpid = $this->input->post('grpid');
					$this->Main->delete_usrBusinessGroup($usrid, $grpid);
					$select = "ur_roleid, ur_permission";
					$table = "ZHOA_usrole";
					$where = "ur_usrid = ".$usrid;
					$dist = $this -> Main ->select_data_where($select, $table, $where);
					$today = date('Y-m-d');					
					foreach ($dist as $ket) {
						$roleid = $ket['ur_roleid'];
						$per = $ket['ur_permission'];
						foreach ($projArr as $ke => $value) {
							$table = "ZHOA_assign_role";
							$data = array('ar_usrid' => $usrid, 
										  'ar_roleid' => $roleid, 
										  'ar_grpid' => $grpid,
										  'ar_permission' => $per, 
										  'ar_ccode' => $value, 
										  'ar_pcode' => $ke, 
										  'ar_added_date' => $today, 
										  'ar_added_by' => $session_data['sess_id']);
							$this->Main->insert_data($table,$data);
						}
					}
				}
			}

			function checkusrname() {
				if($_POST) {
					$val = $this->input->post('usrname');
					$select = "";
					$table = "ZHOA_admlogin";
					$where = $val;
					$query = $this->Buyer_Model->get_usrname_rows($select, $table, $where);
					if($query > 0) {
						echo "<div class='alert alert-danger form-control' style='margin-bottom: inherit; padding: 5px'> 
		                        	<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
		                        	 $val is already used!  
		                        	<input type='hidden' id='chckusrname' value='invalid'>
		                       </div>";
					}else { 
						echo "<div class='alert alert-success form-control' style='margin-bottom: inherit; padding: 5px'> 
		                        	<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
		                        	 $val is valid. 
		                        	<input type='hidden' id='chckusrname' value='valid'>
		                        </div>";
					}
				}
			}

			function listproj(){
				if ($_POST) {
					$co = $this->input->post('options');
					$val = $this->input->post('co');
					$select = "XWETEXT, SWENR";
					$table = "ZHOA_SU";
					$where = "BUKRS = ". $val;
					$odby = "XWETEXT ASC";
					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);
					if($project){
						$proj_arr = array();
						foreach($project as $p){
								$proj_arr[$p['SWENR']] = $p['XWETEXT'];
							}
						if ($co != null){
							$diff = array_diff($proj_arr, $co);
							foreach ($diff as $key => $value) {
								echo "<option value='".$key."'>".$value."</option>";
							}
						} else {
							foreach($proj_arr as $key => $value){
								echo "<option value='".$key."'>".$value."</option>";
							}
						}
					}
				}
			}

			function listproj2(){
				if ($_POST) {
					$co = $this->input->post('options');
					$val = $this->input->post('co');
					$select = "XWETEXT, SWENR";
					$table = "ZHOA_SU";
					$where = "BUKRS = ". $val;
					$odby = "XWETEXT ASC";
					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);
					if($project){
						$proj_arr = array();
						foreach($project as $p){
								$proj_arr[$p['SWENR']] = $p['XWETEXT'];
							}
						echo "<option value=0>--SELECT PROJECT--</option>";
						if ($co != null){
							$diff = array_diff($proj_arr, $co);
							foreach ($diff as $key => $value) {
								echo "<option value='".$key."'>".$value."</option>";
							}
						} else {
							foreach($proj_arr as $key => $value){
								echo "<option value='".$key."'>".$value."</option>";
							}
						}
					}
				}
			}

			function bussEnt(){
				if ($_POST) {
					$roleid = $this->input->post('bgID');
					$val = $this->input->post('co');
					$select = "rb_pcode, rb_pname";
					$table = "ZHOA_role_business";
					$where = "rb_ccode = ". $val . "AND rb_grp_id = ".$roleid;
					$odby = "rb_pname ASC";
					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);
					if($project) {
						$proj_arr = array();
						foreach($project as $p){
								$proj_arr[$p['rb_pcode']] = $p['rb_pname'];
						}
						foreach($proj_arr as $key => $value){
							echo "<option value='".$key."'>".$value."</option>";
						}
					}
				}
			}

			function addGroup() {
				$this->check_access($x=9);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST) {
					$where = $this->input->post('usrid');
					$data['company'] = $this -> Buyer_Model -> get_unassignGroup($where);
					$select = "BUKRS, BUTXT";
					$table = "ZHOA_SU";
					$orderby = "BUTXT ASC";
					$data['co'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
					$this -> load -> view('support/addGroup', $data);
				}
			}

			function saveuser() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$projArr = $this->input->post('bussEnt');
                    $regview=$this->input->post('regview');
                    $regconf=$this->input->post('regconf');
                    $suppview=$this->input->post('suppview');
                    $suppreply=$this->input->post('suppreply');
                    $rptview=$this->input->post('rptview');
                    $rptexpo=$this->input->post('rptexpo');
                    $mtc=$this->input->post('mtc');
                    $carview=$this->input->post('carview');
                    $annview=$this->input->post('annview');
                    $soaview=$this->input->post('soaview');
                    $hoview=$this->input->post('hoview');
                    $accnt=$this->input->post('accnt');
                    $usrview=$this->input->post('usrview');
                    $usredit=$this->input->post('usredit');
                    $usrdel=$this->input->post('usrdel');
                    $usradd=$this->input->post('usradd');
                    $roleadd=$this->input->post('roleadd');
                    $roleedit=$this->input->post('roleedit');
                    $roledel=$this->input->post('roledel');
                    $concern=$this->input->post('concern');
                    $inq=$this->input->post('inq');
                    $req=$this->input->post('req');
					$uname = $this->input->post('uname');
					$lname = $this->input->post('lname');
					$fname = $this->input->post('fname');
					$mail = $this->input->post('mail');
					$spark = $this->input->post('spark');
					$roleid = $this->input->post('role');
					$suppcomm = $this->input->post('suppcomm');
					$bgrp = $this->input->post('bgrp');
					$role = $regview. "-" . $regconf. "-" . $suppview. "-" . $suppreply. "-" . $rptview. "-" . $rptexpo. "-" . $mtc. "-" . $hoview. "-" . $accnt. "-" . $usrview. "-" . $usredit. "-" . $usrdel. "-" . $usradd. "-" . $roleadd. "-" . $roleedit. "-" .$roledel. "-" . $concern. "-" . $inq. "-" . $req. "-" . $suppcomm. "-" . $carview. "-" . $annview. "-" . $soaview;
					$day = date('D, d M Y H:i:s');
					$hash = hash('adler32',$day.$uname);
					$pass = md5($hash);
					$table = "ZHOA_admlogin";
					$today =date('Y-m-d');
					$data = array('a_uname' => $uname, 
								  'a_pass' => $pass,
								  'a_is_validated' => 1, 
								  'a_is_active' => 1,
								  'a_date_created' => $today, 
								  'a_cpass_date' => $today,
								  'a_role' => $roleid, 'a_email' => $mail);
					$this->Main->insert_data($table, $data);	
					$subject = "Vista HOMe User Registration";
					$message =  "Hi Ms./Mr." .strtoupper($fname)." ".strtoupper($lname).", ". "<br/><br/>".
							"Below are your User ID and password for Vista HOMe.". "<br/>".
							"User ID <span style='font-weight: bold;'>$uname</span>". "<br/>".
							"Your password is <span style='font-weight: bold;'>$hash</span>". "<br/>".
							"Please change password upon login.". "<br/><br/>".
							"<a href='".base_url('support')."'>Click here to login </a>";
				    $this -> send_email($mail, $message, $subject);
					$matching = md5($uname.$pass);
					$select = "a_id, a_uname, a_pass";
					$table = "ZHOA_admlogin";
					$login = $this->Main->select_data($select,$table);
					foreach ($login as $key) {
						if($matching == md5($key['a_uname'].$key['a_pass'])) {
							$table1 = "ZHOA_mail_notif";
							// module number
							// 1 => concern
							// 2 => request
							// 3 => inquiry
								$data1 = array('mn_usrid'=>$key['a_id'], 
											   'mn_module'=>1, 
											   'mn_mail'=>$mail, 
											   'mn_active'=>$concern);
								$this->Main->insert_data($table1,$data1);

								$data1 = array('mn_usrid'=>$key['a_id'], 
											   'mn_module'=>2, 
											   'mn_mail'=>$mail, 
											   'mn_active'=>$req);
								$this->Main->insert_data($table1,$data1);

								$data1 = array('mn_usrid'=>$key['a_id'], 
											   'mn_module'=>3, 
											   'mn_mail'=>$mail, 
											   'mn_active'=>$inq);
								$this->Main->insert_data($table1,$data1);

							$table = "ZHOA_adminfo";
							$data = array('ai_id' => $key['a_id'], 
										  'ai_lname' => $lname,
										  'ai_fname' => $fname, 
										  'ai_mname' => null, 
										  'ai_update' => $today, 
										  'ai_role' => $roleid,
										  'ai_regdate' => $today, 
										  'ai_updby' => $session_data['sess_id'],
										  'ai_spark' => $spark, 
										  'ai_createby' => $session_data['sess_id']);
							$this->Main->insert_data($table,$data);

							$table = "ZHOA_usrole";
							$data = array('ur_usrid' => $key['a_id'], 
										  'ur_roleid' => $roleid, 
										  'ur_grpid' => $bgrp,
										  'ur_permission' => $role,);
							$this->Main->insert_data($table,$data);

							foreach ($projArr as $ke => $value) {
								$table = "ZHOA_assign_role";
								$data = array('ar_usrid' => $key['a_id'], 
											  'ar_roleid' => $roleid, 
											  'ar_grpid' => $bgrp,
											  'ar_permission' => $role, 
											  'ar_ccode' => $value, 
											  'ar_pcode' => $ke, 
											  'ar_added_date' => $today, 
											  'ar_added_by' => $session_data['sess_id']);
								$this->Main->insert_data($table,$data);
							}
							echo "<script>alert('User Successfully Added!');</script>";
							redirect(base_url('support/users'));
						}
					}
				}
			}

			function updateUsr() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__');
                    $regview=$this->input->post('regview');
                    $regconf=$this->input->post('regconf');
                    $suppview=$this->input->post('suppview');
                    $suppreply=$this->input->post('suppreply');
                    $rptview=$this->input->post('rptview');
                    $rptexpo=$this->input->post('rptexpo');
                    $mtc=$this->input->post('mtc');
                    $carview=$this->input->post('carview');
                    $annview=$this->input->post('annview');
                    $soaview=$this->input->post('soaview');
                    $hoview=$this->input->post('hoview');
                    $accnt=$this->input->post('accnt');
                    $usrview=$this->input->post('usrview');
                    $usredit=$this->input->post('usredit');
                    $usrdel=$this->input->post('usrdel');
                    $usradd=$this->input->post('usradd');
                    $roleadd=$this->input->post('roleadd');
                    $roleedit=$this->input->post('roleedit');
                    $roledel=$this->input->post('roledel');
                    $concern=$this->input->post('concern');
                    $inq=$this->input->post('inq');
                    $req=$this->input->post('req');
					$uname = $this->input->post('uname');
					$lname = $this->input->post('lname');
					$fname = $this->input->post('fname');
					$mail = $this->input->post('email');
					$spark = $this->input->post('spark');
					$roleid = $this->input->post('roleid');
					$suppcomm = $this->input->post('suppcomm');
					$role = $regview. "-" . $regconf. "-" . $suppview. "-" . $suppreply. "-" . $rptview. "-" . $rptexpo. "-" . $mtc. "-" . $hoview. "-" . $accnt. "-" . $usrview. "-" . $usredit. "-" . $usrdel. "-" . $usradd. "-" . $roleadd. "-" . $roleedit. "-" .$roledel. "-" . $concern. "-" . $inq. "-" . $req. "-" . $suppcomm. "-" . $carview. "-" . $annview. "-" . $soaview;
					$today = date('Y-m-d');
					$id = $this->input->post('usrid');

					$where = "ar_usrid = ";
					$table = "ZHOA_assign_role";
					$data = array('ar_roleid' => $roleid, 'ar_permission' => $role);
					$this->Main->update_data($where, $table, $id, $data);

					$table = "ZHOA_usrole";
					$where = "ur_usrid = ";
					$data = array('ur_roleid' => $roleid, 'ur_permission' => $role);
					$this->Main->update_data($where, $table, $id, $data);

					$id = $this->input->post('usrid');
					$where = "ai_id = ";
					$table = "ZHOA_adminfo";
					$data = array('ai_lname' => $lname, 
								  'ai_fname' => $fname,
								  'ai_spark' => $spark, 
								  'ai_role' => $roleid,
								  'ai_updby' => $session_data['sess_id'], 
								  'ai_update' => $today);
					$this->Main->update_data($where, $table, $id, $data);

					$id = $this->input->post('usrid');
					$where = "a_id = ";
					$table = "ZHOA_admlogin";
					$data = array('a_uname' => $uname, 
								  'a_role' => $roleid,
								  'a_email' => $mail);
					$this->Main->update_data($where, $table, $id, $data);

					$table = "ZHOA_mail_notif";
					$where = "mn_module = 1 AND mn_usrid =";
					if($concern != 0){ 
						$data1 = array('mn_active'=>1, 'mn_mail'=>$mail);
						$this->Main->update_data($where, $table, $id, $data1);
					}else {
						$data1 = array('mn_active'=>0, 'mn_mail'=>$mail);
						$this->Main->update_data($where, $table, $id, $data1);
					}


					$where = "mn_module = 2 AND mn_usrid =";
					if($req != 0){ 
						$data1 = array('mn_active'=>1, 'mn_mail'=>$mail);
						$this->Main->update_data($where, $table, $id, $data1);
					}else {
						$data1 = array('mn_active'=>0, 'mn_mail'=>$mail);
						$this->Main->update_data($where, $table, $id, $data1);
					}

					$where = "mn_module = 3 AND mn_usrid =";
					if($inq != 0){ 
						$data1 = array('mn_active'=>1, 'mn_mail'=>$mail);
						$this->Main->update_data($where, $table, $id, $data1);
					}else {
						$data1 = array('mn_active'=>0, 'mn_mail'=>$mail);
						$this->Main->update_data($where, $table, $id, $data1);
					}
				}
			}

			function saveAddGrp() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$projArr = $this->input->post('bussEnt');
					$usrid = $this->input->post('usrid');
					$bgrp = $this->input->post('bgrp');
					$today = date('Y-m-d');
					$roleid; 
					$per;
					$select = "ur_usrid, ur_roleid, ur_permission";
					$table = "ZHOA_usrole";
					$where = "ur_usrid = ".$usrid;
					$dist = $this -> Main ->select_data_where($select, $table, $where);
					foreach ($dist as $ket) {
						$roleid = $ket['ur_roleid'];
						$per = $ket['ur_permission'];
						foreach ($projArr as $ke => $value) {
							$table = "ZHOA_assign_role";
							$data = array('ar_usrid' => $usrid, 
										  'ar_roleid' => $roleid, 
										  'ar_grpid' => $bgrp,
										  'ar_permission' => $per, 
										  'ar_ccode' => $value, 
										  'ar_pcode' => $ke, 
										  'ar_added_date' => $today, 
										  'ar_added_by' => $session_data['sess_id']);
							$this->Main->insert_data($table,$data);
						}
						redirect(base_url('support/usrGrp/'.$usrid));
					}
				}
			}

			function add() {
				if($_POST) {
					$co = $this->input->post('options');
					echo "<select multiple id='resultcompany' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px; font-size:12px'>";
						foreach($co as $key=>$value){
							echo "<option value='".$key."'>".$value."</option>";
						}
					echo "</select>";
				}
			}

			function addall() {
				if($_POST) {
					$company = $this->input->post('company');
					$select = "XWETEXT, SWENR";
					$table = "ZHOA_SU";
					$where = "BUKRS = ". $company;
					$odby = "XWETEXT ASC";
					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);
					if(!$project){
					} else {
						echo "<select multiple id='selectedcompany' class='form-control' style='height:100%; border:0px; overflow-y:auto; padding:1px'; font-size:12px>";
						foreach($project as $p){
							echo "<option value='".$p['SWENR']."'>".$p['XWETEXT']."</option>";
						}
						echo "</select>";
					}
				}
			}

		function add_users() {
			$this->check_access($x=9);
			$data = $this->get_notif();

			$limit = 1000;
			$offset = 0;
			$where = $data['sess_id'];
			$data['role'] = $this -> Buyer_Model -> get_roles($limit,$offset,$where);

			$data['active'] = "users";
			$select = "rb_grp_id, rb_name";
			$table = "ZHOA_role_business";
			$orderby = "rb_name ASC";
			$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);

			$select = "BUKRS, BUTXT";
			$table = "ZHOA_SU";
			$orderby = "BUTXT ASC";
			$data['brands'] = $this -> Main -> get_data_distinct($select, $table, $orderby);

			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/add_users', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function checkEmail(){

				if($_POST['email']){
					$regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$^"; 

					if ( preg_match( $regex, $_POST['email'] ) ) { 
						$select = "a_email";
						$table = "ZHOA_admlogin";
						$where = "a_email = '".trim($_POST['email'])."'";

						$check_email = $this -> Main -> select_data_where($select, $table, $where);	
							
						if($check_email){
							echo "<div class=\"alert alert-danger form-control\" style=\"padding:5px;\"> 
				                    <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
				                       Email is already in used.
			                        <input type=\"hidden\" id=\"validate_email\" value=\"invalid\">
				                  </div> ";
						}else{
							echo "<div class=\"alert alert-success form-control\" style=\"padding:5px;\"> 
				                    <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
				                       Email is valid.
			                        <input type=\"hidden\" id=\"validate_email\" value=\"valid\">
				                  </div> ";
							}

					} else {
						echo "<div class=\"alert alert-danger form-control\" style=\"padding:5px;\"> 
				                <span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
				                   Email is not valid.	
			                    <input type=\"hidden\" id=\"validate_email\" value=\"invalid\">
				              </div> ";
					}
				}
			}

		function search_homeowner($offset=0){
			$this->check_access($x=7);
			$session_data = $this->session->userdata('hoa_admin_accepted__'); 
			$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
			if($_POST){
				$keyword = $this -> input -> post('keyword');
				$project = $this -> input -> post('project');
				$company = $this -> input -> post('company');
				$data = array('keyword' => $keyword, 'project' => $project,
							  'company' => $company);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/search_homeowner/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this->Buyer_Model->get_search_owners_rows($_SESSION['BE'], $company, $project, $keyword);
					$this->pagination->initialize($config);
					$data['search'] = $this->Buyer_Model->get_search_owners($_SESSION['BE'], $limit,$offset, $company, $project, $keyword);
				    $data["search_links"] = $this->pagination->create_links();
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				} else {
					$dat = $this->set_session($session_data['ui_role'], $session_data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/search_homeowner/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["search_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['search']="";
					}else{
						$this -> search_homeowner($offset=0);
					}
				}
				$this -> load -> view('support/search_homeowner', $data);
			}
		}

		function reg_list(){
			$this->check_access($x=0);
			$session_data = $this->session->userdata('hoa_admin_accepted__'); 
			$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
			$select = "PROJ_ID, PROJ_AREA";
			$table = "ZHOA_PROJ_AREA";
			$where = "MANDT = 113";
			$orderby = "PROJ_AREA ASC";
			$data['proj_area'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
			if(isset($_SESSION['BE'])){
				$data['home_new_ticket'] =$this->Buyer_Model->get_new_concerns_rows($_SESSION['BE']);
				$data['home_new_counter'] =$this->Buyer_Model->get_new_reg_rows($_SESSION['BE']);
				$data['home_out_counter'] =$this->Buyer_Model->get_out_reg_rows($_SESSION['BE']);
				$data['home_conf_counter'] =$this->Buyer_Model->get_con_reg_rows($_SESSION['BE']);
				$data['home_rej_counter'] =$this->Buyer_Model->get_rej_reg_rows($_SESSION['BE']);
				$data['home_wait_counter'] =$this->Buyer_Model->get_wait_reg_rows($_SESSION['BE']);
			} else {
				$data['home_new_ticket'] = 0;
				$data['home_new_counter'] = 0; 
				$data['home_out_counter'] = 0;
				$data['home_conf_counter'] = 0;
				$data['home_rej_counter'] = 0;
				$data['home_wait_counter'] = 0;
			}
			$data['active'] = "home";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/home', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function get_new($offset=0){
				$this->check_access($x=0);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
					  	  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_new/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this->Buyer_Model->get_new_reg_rows($_SESSION['BE']);

					$this->pagination->initialize($config);

					$data['new'] = $this->Buyer_Model->get_new_reg($config["per_page"],$offset,$_SESSION['BE']);
				    $data["new_links"] = $this->pagination->create_links();
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;

					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				}else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_new/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["new_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['new']="";
					}else{
						$this -> get_new($offset=0);
					}
				}
				$this -> load -> view('support/new', $data);
			}

			function get_outstanding($offset = 0){
				$this->check_access($x=0);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						 	  'sess_uname' => $session_data['sess_uname'],
						 	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_outstanding/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_out_reg_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['outstanding']= $this -> Buyer_Model -> get_out_reg($config["per_page"],$offset,$_SESSION['BE']);
				    $data["outstanding_links"] = $this->pagination->create_links();
				    $data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				}else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_outstanding/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["outstanding_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['outstanding']="";
					}else{
						$this -> get_outstanding($offset = 0);
					}
				}
				$this -> load -> view('support/outstanding', $data);
			}

			function get_confirmed($offset=0){
				$this->check_access($x=0);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
					  	  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_confirmed/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_con_reg_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['confirmed']= $this -> Buyer_Model -> get_con_reg($config["per_page"],$offset,$_SESSION['BE']);
				    $data["confirmed_links"] = $this->pagination->create_links();
				    $data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				}else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_confirmed/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["confirmed_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['confirmed']="";
					}else{
						$this -> get_confirmed($offset=0);
					}
				}
				$this -> load -> view('support/confirmed', $data);
			}

			function get_rejected($offset=0){
				$this->check_access($x=0);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
					  	  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_rejected/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_rej_reg_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['rejected']= $this -> Buyer_Model -> get_rej_reg($config["per_page"],$offset,$_SESSION['BE']);
				    $data["rejected_links"] = $this->pagination->create_links();
				    $data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				}else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_rejected/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["rejected_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['rejected']="";
					}else{
						$this -> get_rejected($offset=0);
					}
				}
				$this -> load -> view('support/rejected', $data);
			}

			function get_waiting($offset=0){
				$this->check_access($x=0);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
					  	  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_waiting/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_wait_reg_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['waiting']= $this -> Buyer_Model -> get_wait_reg($config["per_page"],$offset,$_SESSION['BE']);
				    $data["waiting_links"] = $this->pagination->create_links();
				    $data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				}else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_waiting/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["waiting_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['waiting']="";
					}else{
						$this -> get_waiting($offset=0);
					}
				}
				$this -> load -> view('support/waiting', $data);
			}
		

			// function count_new_reg(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_new_reg_rows($where);
			// }

			// function count_out_reg(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_out_reg_rows($where);
			// }

			// function count_conf_reg(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_con_reg_rows($where);
			// }

			// function count_rej_reg(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_rej_reg_rows($where);
			// }

			// function count_wait_reg(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_wait_reg_rows($where);
			// }

			// function count_new_concerns_rows(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_new_concerns_rows($where);
			// }

			// function count_open_concerns_rows(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_open_concerns_rows($where);
			// }

			// function count_closed_concerns_rows(){
			// 	$where = $this->input->post('where');
			// 	echo $this->Buyer_Model->get_closed_concerns_rows($where);
			// }

			function registration($id){
				$this->check_access($x=0);
				$data = $this->get_notif();

				$data['buyer']= $this->Buyer_Model->get_buyer_by_id2($id);
				if(!$data['buyer']){
					show_404();
					return false;
				}else{
					foreach ($data['buyer'] as $v) {
						if($v['bi_status']==1){
							show_404();
							return false;
						}
					}
				}

				$select=""; $table="ZHOA_request_type"; $where="rt_is_deleted =0"; $orderby="rt_desc ASC";
			    $data['request_type'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
				$data['active'] = "home";
				$this -> load -> view('templates/sup-templates/header', $data);
				$this -> load -> view('support/new_reg', $data);
				$this -> load -> view('templates/sup-templates/footer');
			}

			function validated_reg($id){
				$this->check_access($x=0);
				$data = $this->get_notif();

				$data['buyer']= $this->Buyer_Model->get_buyer_by_id($id);
				$data['active'] = "home";
				$this -> load -> view('templates/sup-templates/header', $data);
				$this -> load -> view('support/validated_reg', $data);
				$this -> load -> view('templates/sup-templates/footer');
			}			

			function confirmation($id){
				if($_POST){
					$this->check_access($x=0);
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$data = array('sess_id' => $session_data['sess_id'],
							  	  'sess_uname' => $session_data['sess_uname'],
							  	  'access' => $session_data['ui_access'],
							  	  'role' => $session_data['ui_role']);

					$today = date('Y-m-d H:i:s');
					$select = "";
					$where="bi_id =".$id;
					$table="ZHOA_byrinfo";

					$confirm = $this->Main->select_data_where($select, $table, $where);

					foreach ($confirm as $updated) {

						$where = "bi_cust_no = ".$updated['bi_cust_no'];
						$validity = $this->Main->select_data_where($select, $table, $where);
						foreach ($validity as $val) {
							if($val['bi_status'] == 1){
								return 0;
							}
						}
						
						if($updated['bi_status'] == 0){

							$where="bi_id";
							$data=array( "bi_status" => 1,  
										"bi_process_date" => $today,
										"bi_process_by" => $session_data['sess_id'] );
							$this -> Main -> update_data($where, $table, $id, $data);

							$b_id = $_POST['b_id'];
							$where="b_id";
							$table="ZHOA_byrlogin";
							$data=array( "b_is_validated" => 1, "b_is_active" => 1 );
							$this -> Main -> update_data($where, $table, $b_id, $data);

							$select = "";
							$where="bi_id = ".$id;
							$table="ZHOA_byrinfo as a";
							$join1="ZHOA_byrlogin as b";
							$join2="b.b_id = a.bi_owner_id";
							$orderby = "b.b_id ASC";
							$temp_concern = $this -> Main -> get_data_where_join($select, $table, $orderby, $join1, $join2, $where);
							
							$concern = "";
							$counter = 0;
								
							foreach($temp_concern as $row){
								$cust_no = $row['bi_cust_no'];
								$blklot = $row['bi_blklot'];
								$concern = $row['bi_temp_concern'];
								$owner_id = $row['bi_owner_id'];
								$uname = $row['b_uname'];
								$counter++;
							}

							if($concern != "" && $counter >= 1){
								$select = "VBELN"; 
								$table = 'ZHOA_BUYER_PROF'; 
								$where = "KUNNR = ".$cust_no." AND REFNO = '".$blklot."' AND ACTIVE = 1 ";
								$get_so = $this -> Main -> select_data_where($select, $table, $where);
								foreach($get_so as $gs){
									$so = $gs['VBELN'];
								}

								$table = 'ZHOA_tickets';
								$data = array( 't_blklot' => $blklot, 
											   't_date_created' => $today, 
											   't_status' => 1, 
											   't_owner_id' => $owner_id, 
											   't_is_deleted' => 0,
											   't_so' => $so, 
											   't_reply_by' => $uname, 
											   't_reply_date' => $today, 
											   't_concern' => $concern,
											   't_source' => 08 );
								$this -> Main -> insert_data($table, $data);

								$validation = $uname.$blklot;

								$select = '';
								$where = "t_owner_id = '".$owner_id."' AND t_so = '".$so."'";
								$get_ticket_id = $this -> Main -> select_data_where($select, $table, $where);	

								if(!$get_ticket_id){
									$ticket_id = 0000;
								} else {
									foreach($get_ticket_id as $gt){
										$ticket_id = $gt['t_id'];
									}
									$con_sub2 = $this->input->post('sub_req2');
									if($_POST['sub_request'] != 3){
										$con_sub2 = null;
									}
									$thisday = strtotime($today);
									$table = "ZHOA_concerns";
									$data = array( 'c_rtype' => $_POST['request'], 
												   'c_stype' => $_POST['sub_request'], 
												   'c_message' => $concern, 'c_is_active' => 1,
												   'c_date_created' => $today, 
												   'c_is_deleted' => 0, 
												   'c_reply_by' => $uname, 
												   'c_ticket_id' => $ticket_id, 
												   'c_reply_date' => $today,
												   'c_hsec' => $con_sub2, 
												   'c_closed_date'=>null, 
												   'c_closed_by'=>null, 
												   'c_closed'=>'00000000', 
												   'c_created' => date('Ymd', $thisday));
									$this -> Main -> insert_data($table, $data);
								}

								$table = 'ZHOA_concerns';
								$where = "c_message = '".$concern."' AND c_reply_by = '".$uname."'";
								$get_concern_id = $this -> Main -> select_data_where($select, $table, $where);	

								if($get_concern_id){
									foreach ($get_concern_id as $gci) {
										$c_id = $gci['c_id'];
									}
								}

								$table = "ZHOA_concern_details";
						    	$data = array( 'cd_desc' => $concern,
								    		   'cd_concern_id' => $c_id,
								    		   'cd_date_created' => $today,
								    		   'cd_is_deleted' => 0,
								    		   'cd_reply_by' => $uname,
								    		   'cd_persona' => 1 );
						    	$this -> Main -> insert_data($table, $data);
							}

							$email  = $_POST['email'];	
							$subject = "Vista HOMe: Registration Successful";
							$message =  "<style>.email_format{ font-family: 'arial'; font-size: 16px; } </style>
										<div class='email_format'>".
											"Dear Mr. / Ms. <span style=\"font-weight: bold;\">".$_POST['fullname']."</span>,". "<br/>". "<br/>".
											"Welcome to Vista HOMe - Vista Land's Homeowner's Portal! Your account has been successfully created.". "<br/>". "<br/>".
											"You can start using your Vista HOMe account by clicking the link below.". "<br/>".
											"<a href='".base_url('portal/login/')."'>http://dev.homeowners.vistaland.com.ph/portal/login</a>". "<br/>". "<br/>".
											"Your account details are:". "<br/>".
											"Username: ".$_POST['uname']. "<br/>".
											"Email: ".$_POST['email']. "<br/>".
											"Property: ".$_POST['property']. "<br/>". "<br/>".
											"Thank You,". "<br/>".
											"Vista HOMe Team". "<br/>".
										"</div>"
									;
						    $this -> send_email($email, $message, $subject);
						}
					}
				} else {
					show_404();
				}
			}

			function rejected($id){

				$this->check_access($x=0);
			
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				
				$data = array('sess_id' => $session_data['sess_id'],
							  'sess_uname' => $session_data['sess_uname']);
				$today = date('Y-m-d');
				$where="bi_id";
				$table="ZHOA_byrinfo";
				$data=array( "bi_reason" => $_POST['get_reason'], 
							 "bi_status" => 2, 
							 "bi_process_date" => $today, 
							 "bi_process_by" => $session_data['sess_id'] );
				$this -> Main -> update_data($where, $table, $id, $data);
				$email  = $_POST['email'];	
				$subject = "Vista HOMe: Registration Rejected";
				$message = "<style> .email_format{ font-family: 'arial';  font-size: 16px; } </style>
							<div class='email_format'>".
							"Dear Mr. / Ms. ".$_POST['fullname'].",". "<br/>". "<br/>".
							"We regret to notify you that your sign up for Vista HOME was rejected due to:". "<br/>".
							$_POST['get_reason']. "<br/>". "<br/>".
							"Thank You,". "<br/>".
							"Vista HOMe Team". "<br/>".
							"</div>"
						;
			    $this -> send_email($email, $message, $subject);
			}

			function search($offset=0){
				if($_POST){
					$this->check_access($x=0);
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$data = array('sess_id' => $session_data['sess_id'],
							  	  'sess_uname' => $session_data['sess_uname'],
							  	  'access' => $session_data['ui_access'],
						  	  	  'role' => $session_data['ui_role']);
					$signup_from = $this -> input -> post('signup_from');
					$signup_to = $this -> input -> post('signup_to');
					$status = $this -> input -> post('status');
					$process_from = $this -> input -> post('process_from');
					$process_to = $this -> input -> post('process_to');
					$process_by = $this -> input -> post('process_by');
					$company = $this -> input -> post('company');
					$project = $this -> input -> post('project');
					$keyword = $this -> input -> post('keyword');
					$limit = 10; 

					if(isset($_SESSION['BE'])) {
						$config['base_url'] = base_url('support/search/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = $this->Buyer_Model->get_search_reg_rows($_SESSION['BE'],$signup_from, $signup_to, $process_from, $process_to, $company, $project, $status, $process_by, $keyword);
						$this->pagination->initialize($config);
						$data['search'] = $this->Buyer_Model->get_search_reg($_SESSION['BE'],$limit,$offset,$signup_from,$signup_to, $process_from, $process_to, $company, $project, $status, $process_by, $keyword);
					    $data["search_links"] = $this->pagination->create_links();
						$data['total_pages'] = $config['total_rows'];
						$data['display_offset'] = $offset + 1;
						$data['display_limit'] = $offset + $limit;
						if($data['display_limit'] > $data['total_pages']){
							$data['display_limit'] = $data['total_pages'];
						}
						if($data['total_pages'] == 0){
							$data['display_offset'] = $offset;
							$data['display_limit'] = 0;
						}
					} else {
						$dat = $this->set_session($data['role'], $data['sess_id']);
						if($dat ==0){
							$config['base_url'] = base_url('support/search/0');
							$config['per_page'] = $limit;
							$config['total_rows'] = 0;
							$this->pagination->initialize($config);
							$data["search_links"] = $this->pagination->create_links();
							$data['display_limit'] = 0;
							$data['display_offset'] = 0;
							$data['total_pages'] = 0;
							$data['search']="";
						}else{
							$this -> get_waiting($offset=0);
						}
					}
					$this -> load -> view('support/search', $data);
				}
			}

		function support_page(){
			$this->check_access($x=2);
			$session_data = $this->session->userdata('hoa_admin_accepted__');
			$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
			$select = "PROJ_ID, PROJ_AREA";
			$table = "ZHOA_PROJ_AREA";
			$where = "MANDT = 113";
			$orderby = "PROJ_AREA ASC";
			$data['proj_area'] = $this -> Main -> get_data_where($select, $table, $where, $orderby);
			if(isset($_SESSION['BE'])) {
				$data['home_new_counter'] =$this->Buyer_Model->get_new_reg_rows($_SESSION['BE']);
				$data['home_new_ticket'] =$this->Buyer_Model->get_new_concerns_rows($_SESSION['BE']);
				$data['home_open_ticket'] =$this->Buyer_Model->get_open_concerns_rows($_SESSION['BE']);
				$data['home_closed_ticket'] =$this->Buyer_Model->get_closed_concerns_rows($_SESSION['BE']);
			} else {
				$dat = $this->set_session($data['role'], $data['sess_id']);
				if($dat ==0){
					$data['home_new_counter'] = 0; 
					$data['home_new_ticket'] = 0;
					$data['home_open_ticket'] = 0;
					$data['home_closed_ticket'] = 0;
				}else{
					$this -> support_page();
				}
			}
			$data['active'] = "support";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/support_page', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function get_new_tickets($offset=0){
				$this->check_access($x=2);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_new_tickets/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_new_concerns_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['new_tickets']= $this -> Buyer_Model -> get_new_concerns($config["per_page"],$offset,$_SESSION['BE']);
					$data["new_links"] = $this->pagination->create_links();
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				} else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_new_tickets/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["new_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['new_tickets']="";
					}else{
						$this -> get_new_tickets($offset=0);
					}
				}
				$this -> load -> view('support/tickets', $data);
			}

			function get_open_tickets($offset=0){
				$this->check_access($x=2);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
				
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_open_tickets/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_open_concerns_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['open_tickets']= $this -> Buyer_Model -> get_open_concerns($config["per_page"],$offset,$_SESSION['BE']);
				    $data["open_links"] = $this->pagination->create_links();
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				} else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_open_tickets/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["open_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['open_tickets']="";
					}else{
						$this -> get_open_tickets($offset=0);
					}
				}
				$this -> load -> view('support/open_tickets', $data);
			}

			function get_closed_tickets($offset=0){
				$this->check_access($x=2);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  'sess_uname' => $session_data['sess_uname'],
						  'access' => $session_data['ui_access'],
						  'role' => $session_data['ui_role']);
				
				$limit = 10;
				if(isset($_SESSION['BE'])) {
					$config['base_url'] = base_url('support/get_closed_tickets/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this -> Buyer_Model -> get_closed_concerns_rows($_SESSION['BE']);
					$this->pagination->initialize($config);
					$data['closed_tickets']= $this -> Buyer_Model -> get_closed_concerns($config["per_page"],$offset,$_SESSION['BE']);
				    $data["closed_links"] = $this->pagination->create_links();
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
				} else {
					$dat = $this->set_session($data['role'], $data['sess_id']);
					if($dat ==0){
						$config['base_url'] = base_url('support/get_closed_tickets/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = 0;
						$this->pagination->initialize($config);
						$data["closed_links"] = $this->pagination->create_links();
						$data['display_limit'] = 0;
						$data['display_offset'] = 0;
						$data['total_pages'] = 0;
						$data['closed_tickets']="";
					}else{
						$this -> get_closed_tickets($offset=0);
					}
				}
				$this -> load -> view('support/closed_tickets', $data);
			}

			function single_ticket($ticketid=NULL){
				if($ticketid==NULL){
					show_404();
				} else {
					$this->check_access($x=2);
					$data = $this->get_notif();

					$data['concerns'] = $this -> Buyer_Model -> get_single_ticket($ticketid,$_SESSION['BE']);
					if(!$data['concerns']){
						show_404();
						return false;
					}

					$data['roles'] = $this->Main->select_data($s="",$t="ZHOA_usraccess");
					$data['active'] = "support";
					$data['ticketid'] = $ticketid;
					$get_tickets = $this -> Buyer_Model -> get_ticket_owner($ticketid); 
					foreach($get_tickets as $row){
						$owner_id = $row['t_owner_id'];
					}
					$data['buyer']= $this->Buyer_Model->get_ticket_info($data['ticketid']);

					$select ="";
					$table = "ZHOA_request_type";
					$orderby = "rt_desc ASC";
					$data['reqtyp'] = $this->Main->select_data_odby($select, $table, $orderby);

					$table = "ZHOA_EMPLOYEE";
					$orderby = "NAME1 ASC";
					$data['emp'] = $this->Main->select_data_odby($select, $table, $orderby);

					$table = "ZHOA_departments";
					$data['dept'] = $this->Main->select_data($select, $table);

					// $data['ticketinfo'] = $this -> Buyer_Model -> get_ticket_info($ticketid);
					// $data['ticket'] = $this -> Main -> ;
					$this -> load -> view('templates/sup-templates/header', $data);
					$this -> load -> view('support/single_ticket', $data);
					$this -> load -> view('templates/sup-templates/footer');
				}
			}

			function email_forward() {
				if($_POST){
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$data = array('sess_id' => $session_data['sess_id'],
						  	  'uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
					$users = $this->input->post('usrs');
					$con_id = $this->input->post('con_id');
					
					$con_det = $this->Buyer_Model->get_concern_details($con_id);

					foreach ($con_det as $concerns) {

						$select="a.a_email, b.ai_lname, b.ai_fname"; 
						$table="ZHOA_admlogin AS a"; 
						$join = "ZHOA_adminfo AS b";
						$join1 = "a.a_id = b.ai_id";
						$where="a_id";

						$receiver = $this -> Main -> get_data_join_wherein($select, $table, $join, $join1, $where, $users);

						$concern = "This concern has been forwarded to: <br/>";
						$recipient = array();
						foreach ($receiver as $sender) {
						   	$recipient[] =  $sender['a_email'];
						   	$concern .="&nbsp;".$sender['ai_fname']." ".$sender['ai_lname']."  &lt;".$sender['a_email']."&gt;<br />";
						}

						$today = date('Y-m-d H:i:s');
				    	$table = "ZHOA_concern_details";
				    	$data1 = array( 'cd_desc' => $concern, 'cd_concern_id' => $con_id, 'cd_date_created' => $today,
				    				   'cd_is_deleted' => 0, 'cd_reply_by' => $data['uname'], 'cd_persona' => 9 );
				    	$this -> Main -> insert_data($table, $data1);
				    	
				    	$where = "t_id";
				    	$table = "ZHOA_tickets";
				    	$data1 = array( 't_reply_by' => $data['uname'], 't_reply_date' => $today);
				    	$this -> Main -> update_data($where, $table, $concerns['t_id'], $data1);
				    	
				    	$where = "c_id";
				    	$table = "ZHOA_concerns";
				    	$data1 = array( 'c_reply_by' => $data['uname'], 'c_reply_date' => $today );
				    	$this -> Main -> update_data($where, $table, $con_id, $data1);

						$subject = "FW: Vista HOMe Ticket #".str_pad($concerns['t_id'],10,'0',STR_PAD_LEFT);
						$message = " <style> .email_format{ font-family: 'arial'; font-size: 16px;} </style>
								<div class='email_format'> 
									Customer Number: ". $concerns['KUNNR'] .  "<br><br>
								    <span style='font-weight:bold'>Project Information:</span> <br>
									&nbsp;&nbsp;&nbsp;Project: ".$concerns['XWETEXT']. "<br>
									&nbsp;&nbsp;&nbsp;Blk Lot: ".$concerns['REFNO']. "<br><br>
								    \"" . $concerns['c_message'] . "\"<br>
								    <a href='".base_url()."support/single_ticket/".str_pad($concerns['t_id'],10,'0',STR_PAD_LEFT)."'>Go to ticket</a>	
								    <br><br><br>
								    Sincerely,<br><br>
								    Vista HOMe Team 
						    	</div>";

						$this -> send_email($recipient, $message, $subject);
					}
				}
			}

			function comment($a = NULL){
				$this->check_access($x=2);
				$session_data = $this -> session -> userdata('hoa_admin_accepted__');
				$data['id'] = $session_data['sess_id'];
				$data['uname'] = $session_data['sess_uname'];
				$data['access'] = $session_data['ui_access'];

				$data['get_id'] = $a;

				if($a=NULL){
					show_404();
				} else {
					
				    if($_POST){
				    	$concern = $this -> input -> post('concern');
				    	$concern_id = $this -> input -> post('concern_id');
				    	$ticketid = $this -> input -> post('ticket_id');

				    	$today = date('Y-m-d H:i:s');
				    	$table = "ZHOA_concern_details";
				    	$data1 = array( 'cd_desc' => $concern, 'cd_concern_id' => $concern_id, 'cd_date_created' => $today,
				    				   'cd_is_deleted' => 0, 'cd_reply_by' => $data['uname'], 'cd_persona' => 9 );
				    	$this -> Main -> insert_data($table, $data1);
				    	
				    	$where = "t_id";
				    	$table = "ZHOA_tickets";
				    	$data1 = array( 't_reply_by' => $data['uname'], 't_reply_date' => $today);
				    	$this -> Main -> update_data($where, $table, $ticketid, $data1);
				    	
				    	$where = "c_id";
				    	$table = "ZHOA_concerns";
				    	$data1 = array( 'c_reply_by' => $data['uname'], 'c_reply_date' => $today );
				    	$this -> Main -> update_data($where, $table, $concern_id, $data1);
				    }

					$select = "";
					$table = "ZHOA_concern_details"; 
					$where = "cd_concern_id = ".$data['get_id']; 
					$orderby = "cd_date_created DESC"; 
					$start = "0"; $limit = "100";
					$data['concern_details'] = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start);

					$table="ZHOA_concerns"; 
					$where="c_id = ".$data['get_id']; 
					$data['get_status'] = $this -> Main -> select_data_where($select, $table, $where);
					foreach ($data['get_status'] as $con) {
						$conid = $con['c_ticket_id'];
					}

					$table = "ZHOA_tickets";
					$where = "t_id = ".$conid;
					$f = $this->Main->select_data_where($select, $table, $where);
					if($f){
						foreach ($f as $key) {
							$d = $key['t_source'];
						}
					}
					$data['source'] = $d;
					$this -> load -> view('support/get_single_ticket', $data);
				}
			}

			function reply($id=NULL){
				$this->check_access($x=2);
				$session_data = $this -> session -> userdata('hoa_admin_accepted__');
				$data['id'] = $session_data['sess_id'];
				$data['uname'] = $session_data['sess_uname'];
				$data['access'] = $session_data['ui_access'];

				$data['get_id'] = $id;

				if($id==NULL){
					show_404();
				} else {
					
				    if($_POST){
				    	$concern = $this -> input -> post('concern');
				    	$concern_id = $this -> input -> post('concern_id');
				    	$ticketid = $this -> input -> post('ticket_id');

				    	$select="";
				    	$table="ZHOA_tickets as a";
				    	$orderby = "a.t_id ASC";
				    	$join1 = "ZHOA_byrinfo as b";
				    	$join2 = "a.t_owner_id = b.bi_owner_id"; 
				    	$where="a.t_id = ".$ticketid;
				    	$ticket_details = $this -> Main -> get_data_where_join($select, $table, $orderby, $join1, $join2, $where);

				    	if(!$ticket_details){
				    		return false;
				    	} else {
				    		foreach($ticket_details as $row){
								$cust_no = $row['bi_cust_no'];
								$email = $row['bi_email'];
								$name = $row['bi_fname']. " " .$row['bi_lname'];
				    			$notification = $row['t_notif'];
				    			$project = $row['bi_project'];
				    			$ticket_number = $row['t_id'];
				    			$blklot = $row['t_blklot'];
							}
				    	}
				    	
				    	$today = date('Y-m-d H:i:s');
				    	$table = "ZHOA_concern_details";
				    	$data1 = array( 'cd_desc' => $concern, 'cd_concern_id' => $concern_id, 'cd_date_created' => $today,
				    				   'cd_is_deleted' => 0, 'cd_reply_by' => $data['uname'], 'cd_persona' => 0 );
				    	$this -> Main -> insert_data($table, $data1);
				    	
				    	$where = "t_id";
				    	$table = "ZHOA_tickets";
				    	$data1 = array( 't_reply_by' => $data['uname'], 't_reply_date' => $today, 't_concern' => $concern );
				    	$this -> Main -> update_data($where, $table, $ticketid, $data1);

				    	$firstrep = $this->Buyer_Model->get_support_1st_reply($concern_id);
				    	if($firstrep){
					    	foreach ($firstrep as $q) {
					    		$rep = $q['cd_date_created'];
					    	}
					    }else{
					    	$rep = $today;
					    }

				    	$where = "c_id";
				    	$table = "ZHOA_concerns";
				    	$data1 = array( 'c_reply_by' => $data['uname'], 'c_reply_date' => $today, 'c_first_reply_date' => $rep );
				    	$this -> Main -> update_data($where, $table, $concern_id, $data1);

				    	if($notification == 1){
				    		$subject = "Vista HOMe Ticket Alert: ".str_pad($ticket_number,10,'0',STR_PAD_LEFT);
				    		$message = " <style> .email_format{ font-family: 'arial'; font-size: 16px;} </style>
									<div class='email_format'>". "Customer Number: ". $cust_no .  "<br><br>
						    			<span style='font-weight:bold'>Project Information:</span> <br>
										&nbsp;&nbsp;&nbsp;Project: ".$project. "<br>
										&nbsp;&nbsp;&nbsp;Blk Lot: ".$blklot . "<br><br>
						    			Hi " . $name . ", <br><br>
						    			We want to make you aware of update to your ticket.<br>
						    			<a href='".base_url()."portal/view_request/".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."'>Go to ticket</a>
						    			<br><br><br>
						    			Sincerely,<br><br>
						    			Vista HOMe Team
				    				</div>";
				    		$this -> send_email($email, $message, $subject);
				    	}
				    }

					$select = "";
					$table = "ZHOA_concern_details"; 
					$where = "cd_concern_id = ".$data['get_id']; 
					$orderby = "cd_date_created DESC"; 
					$start = "0"; $limit = "100";
					$data['concern_details'] = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start);

					$table="ZHOA_concerns"; 
					$where="c_id = ".$data['get_id']; 
					$data['get_status'] = $this -> Main -> select_data_where($select, $table, $where);
					foreach ($data['get_status'] as $con) {
						$conid = $con['c_ticket_id'];
					}

					$table = "ZHOA_tickets";
					$where = "t_id = ".$conid;
					$f = $this->Main-> select_data_where($select, $table, $where);
					if($f){
						foreach ($f as $key) {
							$d = $key['t_guest_id'];
						}
					}
					$data['source'] = $d;
					$this -> load -> view('support/get_single_ticket', $data);
				}
			}

			function reply_upload($id=NULL){

				$this->check_access($x=2);
				$session_data = $this -> session -> userdata('hoa_admin_accepted__');
				$data['id'] = $session_data['sess_id'];
				$data['uname'] = $session_data['sess_uname'];
				$data['access'] = $session_data['ui_access'];

				$get_id = $id;
				$data['get_id'] = $id;

				if($id==NULL){
					show_404();
				} else {
					    
				    if($_POST){
				    	$filename = '';
				    	$config['upload_path'] = './assets/concerns';
						$config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|jpeg|xls|xlsx';
						$config['max_size']	= '2097152';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						
						$this->load->library('upload', $config);
					    if ( ! $this->upload->do_upload()) {
							$error = array('error' => $this->upload->display_errors());
							echo "<script> alert('".$error['error']."');</script>";
							$filename = "Attachment is not valid";
						} else {
							$upload_data = $this->upload->data();
							$filename = $upload_data['file_name'];
						}

				    	$concern = $this -> input -> post('concern');
				    	$concern_id = $this -> input -> post('concern_id');
				    	$ticketid = $this -> input -> post('ticket_id');
				    	$today = date('Y-m-d H:i:s');
				    	$concernid = $data['id'];
				    	$replyby = $data['uname'];

				    	$select="a.t_id, a.t_blklot, a.t_notif, b.bi_cust_no, b.bi_email, b.bi_lname, b.bi_fname, bi_project";
				    	$table="ZHOA_tickets as a";
				    	$orderby = "a.t_id ASC";
				    	$join1 = "ZHOA_byrinfo as b";
				    	$join2 = "a.t_blklot = b.bi_blklot"; 
				    	$where="a.t_id = ".$ticketid;
				    	$ticket_details = $this -> Main -> get_data_where_join($select, $table, $orderby, $join1, $join2, $where);

				    	if(!$ticket_details){
				    		return false;
				    	} else {
				    		foreach($ticket_details as $row){
								$cust_no = $row['bi_cust_no'];
								$email = $row['bi_email'];
								$name = $row['bi_fname']. " " .$row['bi_lname'];
				    			$notification = $row['t_notif'];
				    			$project = $row['bi_project'];
				    			$ticket_number = $row['t_id'];
				    			$blklot = $row['t_blklot'];
							}
				    	}

				    	$table = "ZHOA_concern_details";
				    	$data1 = array( 'cd_desc' => $_POST['concern'], 
				    					'cd_concern_id' => $_POST['concern_id'], 
				    					'cd_date_created' => $today,
					    				'cd_is_deleted' => 0, 
					    				'cd_reply_by' => $replyby, 
					    				'cd_filename' => $filename, 
					    				'cd_persona' => 0 );
				    	$this -> Main -> insert_data($table, $data1);

				    	$where = "t_id";
				    	$table = "ZHOA_tickets";
				    	$data1 = array('t_reply_by' => $replyby, 
				    				   't_reply_date' => $today, 
				    				   't_concern' => $_POST['concern'] );
				    	$this -> Main -> update_data($where, $table, $_POST['ticket_id'], $data1);

				    	$where = "c_id";
					    $table = "ZHOA_concerns";
					    $data1 = array('c_reply_by' => $replyby, 'c_reply_date' => $today);
					    $this -> Main -> update_data($where, $table, $_POST['concern_id'], $data1);

					    if($notification == 1){
				    		$subject = "Vista HOMe Ticket Alert: ".str_pad($ticket_number,10,'0',STR_PAD_LEFT);
				    		$message = " <style> .email_format{ font-family: 'arial'; font-size: 16px;} </style>
									<div class='email_format'>". "Customer Number: ". $cust_no .  "<br><br>
						    			<span style='font-weight:bold'>Project Information:</span> <br>
										&nbsp;&nbsp;&nbsp;Project: ".$project. "<br>
										&nbsp;&nbsp;&nbsp;Blk Lot: ".$blklot . "<br><br>
						    			Hi " . $name . ", <br><br>
						    			We want to make you aware of update to your ticket.<br>
						    			<a href='".base_url()."portal/view_request/".str_pad($ticket_number,10,'0',STR_PAD_LEFT)."'>Go to ticket</a>
						    			<br><br><br>
						    			Sincerely,<br><br>
						    			Vista HOMe Team
				    				</div>";
				    		$this -> send_email($email, $message, $subject);
				    	}
			    	}
				    $data['get_id'] = $id;
					$select = ""; 
					$table = "ZHOA_concern_details"; 
					$where = "cd_concern_id = ".$get_id; 
					$orderby = "cd_date_created DESC"; 
					$start = "0"; $limit = "100";
					$data['concern_details'] = $this -> Main -> get_data_where_limit($select, $table, $where, $orderby, $limit, $start);
					$select=""; $table="ZHOA_concerns"; $where="c_id = ".$get_id;
					$get_status = $this -> Main -> select_data_where($select, $table, $where);
					if($get_status){
						foreach($get_status as $row){
							$c_status = $row['c_is_active'];
						}
					}
					$data['get_status'] = $c_status;
					$this -> load -> view('support/get_single_ticket', $data);
				}
			}

			function update_reqtype(){
				if($_POST){
					$session_data = $this -> session -> userdata('hoa_admin_accepted__');
					$uname = $session_data['sess_uname'];

					$today = date('Y-m-d H:i:s');
					$thisday = strtotime($today);
					$cdate = date('Ymd', $thisday);

					$id = $this->input->post('con_id');
					$con = $this->input->post('con_type');
					$con_sub = $this->input->post('con_sub');
					$con_sub2 = $this->input->post('con_sub2');
					$stat = $this->input->post('validity');
					$pstart = $this->input->post('pstart');
					$pfinish = $this->input->post('pfinish');
					$employee = $this->input->post('employee');
					$department = $this->input->post('department');
					$tasktext = $this->input->post('text');
					$compby = $this->input->post('compby');
					$compdate = $this->input->post('compdate');
					$compday = date('Ymd', strtotime($compdate));

					$select = "";
					$table = "ZHOA_concerns";
					$where = "c_id = ".$id;
					$get_ticket = $this -> Main -> select_data_where($select, $table, $where);

					if($get_ticket){
						foreach($get_ticket as $row){
							$ticket_id = $row['c_ticket_id'];

							if($stat == 1){
								if($row['c_is_active'] == 2){
									$stat = $row['c_is_active'];
								}
								$compby = NULL;
								$compday = '00000000';
							}
						}
					}else{
						show_404();
						return false;
					}
					
					$table = "ZHOA_concerns";
					$where = "c_id";
					if($stat != 3){
						$uname = null;
						$today = null;
					} 
					
					$data = array('c_hsec'=>$con_sub2, 
								'c_rtype'=>$con,
								'c_stype'=>$con_sub, 
								'c_is_active'=>$stat,
								'c_plan_start' =>$pstart,
								'c_plan_finish' =>$pfinish,
								'c_closed_date' => $today,
								'c_closed_by' => $uname,
								'c_closed' => $compday,
								'c_employee' => $employee,
								'c_department' => $department,
								'c_task_text' => $tasktext,
								'c_completedby' => $compby);
					$this->Main->update_data($where, $table, $id, $data);

					$select = "";
					$table = "ZHOA_concerns";
					$where = "c_ticket_id = ".$ticket_id;
					$get_concern = $this -> Main -> select_data_where($select, $table, $where);

					$open = 0;	$posted = 0;	$counter = 0;
					foreach($get_concern as $row){
						if($row['c_is_active'] == 1){
							$open++;
						}
						if($row['qmnum'] != ""){
							$posted++;
						}
						$counter++;
					}

					if ($posted == 0) {		//UNPOSTED
						$pstat = 1;
					}
					if($counter > $posted && $posted > 0){		//PARTIAL
						$pstat = 2;
					}
					if($posted == $counter){		//POSTED
						$pstat = 3;	
					}

					$open == 0? $stat = 2: $stat = 1;

					$where = "t_id";
					$table = "ZHOA_tickets"; 
					$data = array( 't_status' => $stat,
								   't_post_status' => $pstat );
					$this -> Main -> update_data($where, $table, $ticket_id, $data);

					echo "<div class=\"panel panel-primary\" style=\"border-color:#4cae4c\">";
					echo "<div class=\"panel-heading text-center\" style=\"background-color:#5cb85c;border-color:transparent\">";
					echo "<span class=\"panel-title\">Concern Successfully Validated</span>"; 
				}
			}

			function close_ticket($id){
				$session_data = $this -> session -> userdata('hoa_admin_accepted__');
				$uname = $session_data['sess_uname'];

				$select = "";
				$table = "ZHOA_concerns";
				$where = "c_id = ".$id;
				$get_ticket = $this -> Main -> select_data_where($select, $table, $where);

				if($get_ticket){
					foreach($get_ticket as $row){
						$ticket_id = $row['c_ticket_id'];
					}
				}else{
					show_404();
					return false;
				}

				$cby = $this->input->post('cby');
				$cdate = $this->input->post('cdate');

				$compdate = strtotime($cdate);

				$today = date('Y-m-d H:i:s');
				$where = "c_id"; 
				$table = "ZHOA_concerns"; 
				$thisday = strtotime($today);
				$data = array( 'c_is_active' => 2,
							   'c_closed_date' => $today,
							   'c_closed_by' => $uname,
							   'c_closed' => date('Ymd', $compdate),
							   'c_completedby' => $cby);
				$this -> Main -> update_data($where, $table, $id, $data);

				$select = "";
				$table = "ZHOA_concerns";
				$where = "c_ticket_id = ".$ticket_id;
				$get_concern = $this -> Main -> select_data_where($select, $table, $where);

				$counter = 0;
				foreach($get_concern as $row){
					$concern_status = $row['c_is_active'];
					$cdate = $row['c_closed'];
					$cby = $row['c_completedby'];
					if($concern_status == 1 || (!isset($cby) && ($cdate == '00000000')) ){
						$counter++;
					}
				}

				if($counter == 0){
					$where = "t_id"; $table = "ZHOA_tickets"; 
					$data = array( 't_status' => 2 );
					$this -> Main -> update_data($where, $table, $ticket_id, $data);
				}

				echo "<script> alert('Ticket Closed.'); </script>";
				echo "<script> window.location.href='".base_url()."support/single_ticket/".str_pad($ticket_id, 10, 0, STR_PAD_LEFT)."' </script>";

			}

			function download_file($fileID){
				$select = "cd_filename"; $table = "ZHOA_concern_details"; $where = "cd_id = ".$fileID;
				$get_file = $this -> Main -> select_data_where($select, $table, $where);
				$baseUrl = base_url()."assets/concerns/";
				if($get_file){
					foreach($get_file as $row){
						$filename = $row['cd_filename'];
					}
					if(file_exists(APPPATH."../assets/concerns/".$filename)){				//for devserver 
						$filepath = file_get_contents(APPPATH."../assets/concerns/".$filename); //for devserver
					// if(file_exists($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/concerns/".$filename)){
						// $filepath = file_get_contents($_SERVER['DOCUMENT_ROOT']."/vistahome/assets/concerns/".$filename); // Read the file's contents
						force_download($filename, $filepath);
					} else {
						echo "<script>alert('File corrupted!');</script>";
					}
				}
			}

			function search_concern($offset=0){
				if($_POST){
					$this->check_access($x=2);
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$data = array('sess_id' => $session_data['sess_id'],
							  	  'sess_uname' => $session_data['sess_uname'],
							  	  'access' => $session_data['ui_access'],
						  	  	  'role' => $session_data['ui_role']);
					$posted_from = $this -> input -> post('posted_from');
					$posted_to = $this -> input -> post('posted_to');
					$status = $this -> input -> post('status');
					$project_area = $this -> input -> post('company');
					$project = $this -> input -> post('project');
					$keyword = $this -> input -> post('keyword');
					$limit = 10;

					if(isset($_SESSION['BE'])) {
						$config['base_url'] = base_url('support/search_concern/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = $this->Buyer_Model->get_search_concern_rows($_SESSION['BE'], $posted_from, $posted_to, $status, $project_area, $project, $keyword);
						$this->pagination->initialize($config);
						$data['search'] = $this->Buyer_Model->get_search_concern($_SESSION['BE'], $limit, $offset, $posted_from, $posted_to, $status, $project_area, $project, $keyword);
					    $data["search_links"] = $this->pagination->create_links();
						$data['total_pages'] = $config['total_rows'];
						$data['display_offset'] = $offset + 1;
						$data['display_limit'] = $offset + $limit;
						if($data['display_limit'] > $data['total_pages']){
							$data['display_limit'] = $data['total_pages'];
						}
						if($data['total_pages'] == 0){
							$data['display_offset'] = $offset;
							$data['display_limit'] = 0;
						}
					} else {
						$dat = $this->set_session($data['role'], $data['sess_id']);
						if($dat ==0){
							$config['base_url'] = base_url('support/search_concern/0');
							$config['per_page'] = $limit;
							$config['total_rows'] = 0;
							$this->pagination->initialize($config);
							$data["search_links"] = $this->pagination->create_links();
							$data['display_limit'] = 0;
							$data['display_offset'] = 0;
							$data['total_pages'] = 0;
							$data['search']="";
						}else{
							$this -> get_closed_tickets($offset=0);
						}
					}
					$this -> load -> view('support/search_concern', $data);
				}
			}

			function newTickets(){
				$this->check_access($x=2);
				$data =  $this->get_notif();

				$select = "";
				$table = "ZHOA_TASKS";
				$where = "CODEGRUPPE = 'MEDIA'";
				$data['source'] = $this->Main->select_data_where($select,$table,$where);

				$select = "";
				$table = "ZHOA_request_type";
				$data['rtype'] = $this->Main->select_data($select,$table);

				$notification = "";
				$data['notification'] = $notification;
				$data['active'] = "support";
				$this -> load -> view('templates/sup-templates/header', $data);
				$this -> load -> view('support/add_ticket', $data);
				$this -> load -> view('templates/sup-templates/footer');
			}

				function addTickets(){

					if(isset($_SESSION['counter'])){
				    	for($x=1; $x<=$_SESSION['counter']; $x++){
				    		unset($_SESSION['req'.$x]);
				    		unset($_SESSION['sub_type'.$x]);
				    		unset($_SESSION['sub_opt'.$x]);
				    		unset($_SESSION['message'.$x]);
				    		unset($_SESSION['attachment'.$x]);
				    	}
				    	unset($_SESSION['counter']);
				    }

					if($_POST) {
						$tkt = $this->input->post('tkt');
						if(isset($_SESSION['BE'])){
							if(!empty($_SESSION['BE'])){
								$where = "SWENR IN (".$_SESSION['BE'].") AND ";
							}else{
								$where = "";
							}
							switch ($tkt) {
								case "accnt":{
									$accnt = $this->input->post('num');
									$where .= "KUNNR = '".str_pad(preg_replace('/\s+/', '', $accnt), 10, '0', STR_PAD_LEFT). "' AND ABGRU = '' AND ACTIVE = 1 ";
									break;
								}
								case "blk":{
									$proj = $this->input->post('pro');
									$blk = $this->input->post('blk');
									$where .= "SWENR = '".$proj. "' AND REFNO = '".$blk."' AND ABGRU = '' AND ACTIVE = 1";
									break;
								}
							}
							$select = "";
							$table = "ZHOA_BUYER_PROF";
							$data['res'] = $this->Main->select_data_where($select, $table, $where);
						}

						$table = "ZHOA_source_request";
						$where = "CODEGRUPPE = 'MEDIA'";
						$data['source'] = $this->Main->select_data($select,$table);

						$table = "ZHOA_request_type";
						$data['rtype'] = $this->Main->select_data($select,$table);

						$this -> load -> view('support/new_ticket', $data);
					}
				}

				function concern_enlist(){

					if(isset($_SESSION['counter'])){
						$x = $_SESSION['counter']++;
					} else {
						$_SESSION['counter'] = 1;
					}

					if($_REQUEST['req'] && $_REQUEST['sub_type']){

						if($_REQUEST['sub_type'] == 2 || $_REQUEST['sub_type'] == 3 || $_REQUEST['sub_type'] == 4 || $_REQUEST['sub_type'] == 5){
							$_SESSION['req'.$_SESSION['counter']] = $_REQUEST['req'];
							$_SESSION['sub_type'.$_SESSION['counter']] = $_REQUEST['sub_type'];
							$_SESSION['sub_opt'.$_SESSION['counter']] = $_REQUEST['sub_opt'];
							$_SESSION['message'.$_SESSION['counter']] = $_REQUEST['message'];
							$_SESSION['attachment'.$_SESSION['counter']] = $_REQUEST['attachment'];
							//echo $request." ".$sub_request." ".$message;
						} else {
							$_SESSION['req'.$_SESSION['counter']] = $_REQUEST['req'];
							$_SESSION['sub_type'.$_SESSION['counter']] = $_REQUEST['sub_type'];
							$_SESSION['message'.$_SESSION['counter']] = $_REQUEST['message'];
							$_SESSION['attachment'.$_SESSION['counter']] = $_REQUEST['attachment'];
						}
					}


					echo "<div class='panel panel-primary'>";
					echo 	"<div class='panel-heading'>Concerns</div>";
					echo 		"<div class='table-responsive'>";
					echo 			"<table class='table'>";
					echo 				"<tr>";
					echo 					"<th>Request</th>";
					echo 					"<th>Sub Type</th>";
					echo 					"<th>Message</th>";
					echo 				"</tr>";

					for($i=1; $i<=$_SESSION['counter']; $i++){

						$select=""; $table="ZHOA_request_type"; $where="rt_id = ".$_SESSION['req'.$i];
						$get_request = $this -> Main -> select_data_where($select, $table, $where);

						foreach($get_request as $gt){
							$request_res = $gt['rt_desc'];
						}

						$select=""; $table="ZHOA_subrequest_type"; $where="st_id = ".$_SESSION['sub_type'.$i];
						$get_srequest = $this -> Main -> select_data_where($select, $table, $where);

						foreach($get_srequest as $gt){
							$srequest_res = $gt['st_desc'];
						}

					echo 				"<tr>";
					echo 					"<td style='width: 150px;'>".$request_res."</td>";
					echo 					"<td style='width: 300px;'>";
					echo 					"<span>".$srequest_res."</span>";

					if(isset($_SESSION['sub_opt'.$i])){
						switch ($_SESSION['sub_type'.$i]){
							case 2: $code = 'HSEC'; break;
							case 3: $code = 'DEVT'; break;
							case 4: $code = 'LOTC'; break;
							case 5: $code = 'SERV'; break;
						}
						$select = "KURZTEXT";
						$table = "ZHOA_TASKS";
						$where = "CODE = '".$_SESSION['sub_opt'.$i]."' AND CODEGRUPPE = '".$code. "'";
						$qwe= $this->Main->select_data_where($select, $table, $where);
						
						foreach ($qwe as $k) {
											
					echo 					"<br /><span> [".$k['KURZTEXT']."] </span>";
						}	
					}
					echo 					"</td>";
					echo 					"<td>".$_SESSION['message'.$i]."</td>";
					echo 					"<td>".$_SESSION['attachment'.$i]."</td>";
					echo 				"</tr>";	
					}
					echo 			"</table>";
					echo 		"</div>";
					echo 	"</div>";
					echo "</div>";
				}

				function save_ticket(){
					$this->check_access($x=2);
					$session_data = $this->session->userdata('hoa_admin_accepted__'); 

					$today = date('Y-m-d H:i:s');
				    $blklot = $this -> input -> post('blk');
				   	$so = $this -> input -> post('so');
				   	$source = $this -> input -> post('source');
				   	$name = $this -> input -> post('name');
				   	$n = $this -> input -> post('id');
				   	$accnt = $this -> input -> post('kunnr');
				   	$owner = 0;
					$buyer_id = 0;


				   	if($n == 1) {
				   		$cont = $this -> input -> post('cont');
				   		$email = $this -> input -> post('email');
				   		$str = explode(" ", $name);
				   		$name1 = $str[0];

				   		$table = "ZHOA_guest_info";
				   		$data0 = array("gi_name" => $name, "gi_contact" => $cont, "gi_email" => $email, "gi_dcreated" => $today);
				   		$this -> Main -> insert_data($table, $data0);

				   		$select = "";
				   		$where = "gi_name = '".$name."' and gi_dcreated = '".$today."'";
				   		$q = $this->Main->select_data_where($select,$table,$where);
				   		foreach ($q as $key) {
				   			$owner = $key['gi_id'];
				   		}				   	
				   	}else{
					   	$select = "";
					   	$table = "ZHOA_byrinfo as a";
					   	$join = "ZHOA_byrlogin as b";
					   	$join1 = "a.bi_owner_id = b.b_id";
					   	$where = "bi_cust_no =".$accnt;
					   	$orderby = "a.bi_owner_id asc";
					   	$byr = $this->Main->get_data_where_join($select, $table, $orderby, $join, $join1, $where);
					   	if($byr){
					   		foreach ($byr as $ho) {
					   			$buyer_id = $ho['bi_owner_id'];
					   			$name = $ho['b_uname'];
					   			$owner = NULL;
					   		} 
					   	}
				   	}

				   	$table = "ZHOA_tickets";
			    	$data1 = array("t_blklot" => $blklot,
					    		  "t_date_created" => $today,
					    		  "t_status" => 1,
					    		  "t_owner_id" => $buyer_id,
					    		  "t_is_deleted" => 0,
					    		  "t_so" => $so,
					    		  "t_reply_by" => $name, 
					    		  "t_guest_id" => $owner,
					    		  "t_reply_date" => $today,
					    		  "t_concern" => $_SESSION['message'.$_SESSION['counter']],
					    		  "t_notif" => "", 
					    		  "t_source" => $source);
				    $_SESSION['match_ticket'] = md5($today);
				   	$this -> Main -> insert_data($table, $data1);

				   	$select = "";
				   	$table = "ZHOA_tickets";
				   	$where = "t_owner_id = ".$buyer_id." AND t_so = '".$so."' AND t_date_created = '" .$today."'";	
				   	$tickets = $this -> Main -> select_data_where($select, $table, $where);

				   	foreach($tickets as $row){

				   		$date_created = strtotime($row['t_date_created']);

				    	for($x=1; $x<=$_SESSION['counter']; $x++){
				    		if(isset($_SESSION['sub_opt'.$x])){
				    			$_SESSION['sub_opt'.$x];
				    		}else {
				    			$_SESSION['sub_opt'.$x] = null;
				    		}
				    		$_SESSION['req'.$x];
				    		$_SESSION['sub_type'.$x];
							$_SESSION['message'.$x];
				    		$_SESSION['attachment'.$x];

						    $data2 = array("c_rtype" => $_SESSION['req'.$x],
								    	  "c_stype" => $_SESSION['sub_type'.$x],
								    	  "c_message" => $_SESSION['message'.$x],
						    			  "c_is_active" => 1,
						    			  "c_date_created" => date('Y-m-d H:i:s', $date_created),
						    			  "c_is_deleted" => 0,
						    			  "c_reply_by" => $name,
						    			  "c_ticket_id" => $row['t_id'],
						    			  "c_reply_date" => date('Y-m-d H:i:s', $date_created),
						    			  "c_hsec"=> $_SESSION['sub_opt'.$x], 
						    			  "c_closed_date"=>null, 
						    			  "c_closed_by"=>null, 
						    			  "c_closed"=>'00000000', 
						    			  "c_created"=>date('Ymd', $date_created));
						    $table = "ZHOA_concerns";
							$this -> Main -> insert_data($table, $data2);

							$condate = date('Y-m-d H:i:s', $date_created);
				    		$select = "c_id";
						    $where = "c_reply_date = '".$condate."' ";
							$c_id = $this->Main->select_data_where($select, $table, $where);

					   		$con_id="";
				    		foreach ($c_id as $key) {
								$con_id = $key['c_id'];
				    		}

						    $data3 = array("cd_desc"=>$_SESSION['message'.$x], 
							    	"cd_concern_id"=>$con_id, 
							    	"cd_date_created"=>$condate, 
							    	"cd_is_deleted"=>0,
						   			"cd_reply_by"=>$name, 
						   			"cd_filename"=>str_replace(' ', '_',$_SESSION['attachment'.$x]), 
						   			"is_viewed_2"=>0, 
						   			'cd_persona'=>1);
					 		$table = "ZHOA_concern_details";
				    		$this -> Main -> insert_data($table, $data3);
						}
				    }

				    if(isset($_SESSION['counter'])){

				    	for($x=1; $x<=$_SESSION['counter']; $x++){

				    		unset($_SESSION['req'.$x]);
				    		unset($_SESSION['sub_type'.$x]);
				    		unset($_SESSION['sub_opt'.$x]);
				    		unset($_SESSION['message'.$x]);
				    		unset($_SESSION['attachment'.$x]);
				    	}
				    	unset($_SESSION['counter']);
				    }
				}

				function unset_sess_tkt(){
					if(isset($_SESSION['counter'])){

				    	for($x=1; $x<=$_SESSION['counter']; $x++){
				    		unset($_SESSION['req'.$x]);
				    		unset($_SESSION['sub_type'.$x]);
				    		unset($_SESSION['sub_opt'.$x]);
				    		unset($_SESSION['message'.$x]);
				    		unset($_SESSION['attachment'.$x]);
				    	}
				    	unset($_SESSION['counter']);
				    }
				}

			function get_proj($val=NULL){
				if($val == NULL){
					show_404();
				} else { 
					$select = "XWETEXT";
					$table = "ZHOA_SU";
					$where = "PROJ_ID = ". $val;
					$odby = "XWETEXT ASC";
					$project = $this -> Main -> get_data_distinct_where($select, $table, $where, $odby);
					echo "<select id='project' class='form-control input-sm' onchange='search_homeowner()'>";
						if(!$project){
							echo "<option value=''>-- Select Project --</option>";
						} else {
							echo "<option value=''>-- Select Project --</option>";
							foreach($project as $p){
								echo "<option value='".$p['XWETEXT']."'>".$p['XWETEXT']."</option>";
							}
						}
					echo "</select>";
				}
			}

		function reports(){
			$data = $this->get_notif();

			$notification = "";
			$data['notification'] = $notification;
			$data['active'] = "reports";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/reports', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

		function mtc(){
			$this->check_access($x=6);
			$data = $this->get_notif();
			$data['active'] = "mtc";

			$data['active'] = "mtc";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/mtc', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

		function carousel(){
			$this->check_access($x=20);
			$data = $this->get_notif();
			
			$notification = "";
			$select = "";
			$table = "ZHOA_carousel";
			$data['carousel'] = $this -> Main -> select_data($select, $table);
			$data['notification'] = $notification;
			$data['active'] = "mtc";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/carousel', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

		function announcements(){
			$this->check_access($x=21);
			$data =  $this->get_notif();
			$data['active'] = "mtc";

			$select = "BUKRS, BUTXT";
			$table = "ZHOA_SU";
			$orderby = "BUTXT ASC";
			$data['company'] = $this -> Main -> get_data_distinct($select, $table, $orderby);
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/announcements', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function announce_load($offset=0){
				
					$this->check_access($x=21);
					$session_data = $this->session->userdata('hoa_admin_accepted__');
					$data = array('sess_id' => $session_data['sess_id'],
							  	  'sess_uname' => $session_data['sess_uname'],
							  	  'access' => $session_data['ui_access'],
						  	  	  'role' => $session_data['ui_role']);
					$limit = 6;
					$select ="";
					$table = "ZHOA_announcements as a";
					$where = "a.a_is_deleted = 0";
					// if($where != "") {
						$config['base_url'] = base_url('support/announce_load/0');
						$config['per_page'] = $limit;
						$config['total_rows'] = $this ->Main->count_select_where($select, $table, $where);
						$this->pagination->initialize($config);
						$data['announcements'] = $this->Buyer_Model->get_announcements($offset, $limit);
					    $data["new_links"] = $this->pagination->create_links();
						$data['total_pages'] = $config['total_rows'];
						$data['display_offset'] = $offset + 1;
						$data['display_limit'] = $offset + $limit;
						if($data['display_limit'] > $data['total_pages']){
							$data['display_limit'] = $data['total_pages'];
						}
						if($data['total_pages'] == 0){
							$data['display_offset'] = $offset;
							$data['display_limit'] = 0;
						}
					// }else {
					// 	$config['base_url'] = base_url('support/announce_load/0');
					// 	$config['per_page'] = $limit;
					// 	$config['total_rows'] = 0;
					// 	$this->pagination->initialize($config);
					// 	$data["new_links"] = $this->pagination->create_links();
					// 	$data['display_limit'] = 0;
					// 	$data['display_offset'] = 0;
					// 	$data['total_pages'] = 0;
					// 	$data['new']="";
					// }
					$this -> load -> view('support/announce_load', $data);
				
			}

			function post_announcement() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__'); 
		            $subject=$this->input->post('subject');
		            $message=$this->input->post('message');
		            $recipient = $this->input->post('recipient');
		            $today = date('Y-m-d H:i:s');
					$matching = hash('adler32', $today.$message.'#hash');
					$arrdata = array('a_subject'=>$subject, 
									 'a_msg'=>$message, 
									 'a_dposted'=>$today, 
									 'a_is_deleted'=>0, 
									 'a_is_updated'=>0, 
									 'a_postedby'=>$session_data['sess_id'], 
									 'a_dupd'=>null, 
									 'a_byupd'=>null, 
									 'a_attachment'=>null, 
									 'a_unique'=> $matching, 
									 'a_dcan'=>null, 
									 'a_bycan'=>null);
					$table = "ZHOA_announcements";
					$this-> Main-> insert_data($table, $arrdata);

					$select="a_id,a_dposted,a_msg,a_unique";
					$where = "a_unique = '".$matching."'";
					$getid = $this->Main->select_data_where($select, $table, $where);
					foreach ($getid as $val) {
						$a_id = $val['a_id'];
						foreach ($recipient as $key	 => $value) {
							$table = "ZHOA_announcement_recipient";
							$data = array('ar_announcement'=>$a_id,'ar_proj'=>$key);
							$this-> Main-> insert_data($table, $data);
						}
					}
					redirect(base_url('support/announce_load'));
				}
			}

			function post_announcement2() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__'); 

					$filename = '';
					$config['upload_path'] = './assets/img/announcements';
					$config['allowed_types'] = 'jpg|png|jpeg';
					$config['max_size']	= '2097152';
					$config['encrypt_name'] = TRUE;
							
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload()) {
						$error = array('error' => $this->upload->display_errors());
						echo "<script> alert('".$error['error']."');</script>";
						$filename = "Attachment is not valid";
					} else {
						$upload_data = $this->upload->data();
						$filename = $upload_data['file_name'];
					}

		            $subject=$this->input->post('subject');
		            $message=$this->input->post('message');
		            $recipient = $this->input->post('recipient');
		            $arr = json_decode($recipient);
		            $today = date('Y-m-d H:i:s');
					$matching = hash('adler32', $today.$message.'#hash');
					$arrdata = array('a_subject'=>$subject, 
									 'a_msg'=>$message, 
									 'a_dposted'=>$today, 
									 'a_is_deleted'=>0, 
									 'a_is_updated'=>0, 
									 'a_postedby'=>$session_data['sess_id'], 
									 'a_dupd'=>null, 
									 'a_byupd'=>null, 
									 'a_attachment'=>$filename, 
									 'a_unique'=> $matching, 
									 'a_dcan'=>null, 
									 'a_bycan'=>null);
					$table = "ZHOA_announcements";
					$this-> Main-> insert_data($table, $arrdata);

					$select="a_id,a_dposted,a_msg,a_unique";
					$where = "a_unique = '".$matching."'";
					$getid = $this->Main->select_data_where($select, $table, $where);
					foreach ($getid as $val) {
						$a_id = $val['a_id'];
						foreach ($arr as $key => $value) {
							$table = "ZHOA_announcement_recipient";
							$data = array('ar_announcement'=>$a_id,'ar_proj'=>$key);
							$this-> Main-> insert_data($table, $data);
						}
					}
					redirect(base_url('support/announce_load'));
				}
			}

			function update_announcement() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__'); 
					$id=$this->input->post('id');
		            $subject=$this->input->post('subject');
		            $message=$this->input->post('message');
		            $fimg = $this->input->post('flabel');
					$data;
		            $today = date('Y-m-d');
		            if($fimg==""){
						$data = array('a_subject'=>$subject, 
									  'a_msg'=>$message, 
									  'a_is_updated'=>1, 
									  'a_dupd'=>$today, 
									  'a_byupd'=>$session_data['sess_id'], 
									  'a_attachment'=>null);
		            } else{
						$data = array('a_subject'=>$subject, 
									  'a_msg'=>$message, 
									  'a_is_updated'=>1, 
									  'a_dupd'=>$today, 
									  'a_byupd'=>$session_data['sess_id']);
		            }
		            $where = "a_id";
					$table = "ZHOA_announcements";
					$this-> Main-> update_data($where, $table, $id, $data);
					redirect(base_url('support/announce_load'));
				}
			}

			function update_announcement2() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__'); 

					$filename = '';
					$config['upload_path'] = './assets/img/announcements';
					$config['allowed_types'] = 'jpg|png|jpeg';
					$config['max_size']	= '2097152';
					$config['encrypt_name'] = TRUE;
							
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload()) {
						$error = array('error' => $this->upload->display_errors());
						echo "<script> alert('".$error['error']."');</script>";
						$filename = "Attachment is not valid";
					} else {
						$upload_data = $this->upload->data();
						$filename = $upload_data['file_name'];
					}

					$id=$this->input->post('id');
		            $subject=$this->input->post('subject');
		            $message=$this->input->post('message');
		            $where = "a_id";
		            $today = date('Y-m-d');
					$data = array('a_subject'=>$subject, 
								  'a_msg'=>$message, 
								  'a_is_updated'=>1, 
								  'a_dupd'=>$today, 
								  'a_byupd'=>$session_data['sess_id'], 
								  'a_attachment'=>$filename);
					$table = "ZHOA_announcements";
					$this-> Main-> update_data($where, $table, $id, $data);
					redirect(base_url('support/announce_load'));
				}
			}

			function cancel_announcement() {
				if($_POST) {
					$session_data = $this->session->userdata('hoa_admin_accepted__'); 
					$id=$this->input->post('id');
		            $where = "a_id";
		            $today = date('Y-m-d');
					$data = array('a_is_deleted'=>1, 'a_dcan'=>$today, 'a_bycan'=>$session_data['sess_id']);
					$table = "ZHOA_announcements";
					$this-> Main-> update_data($where, $table, $id, $data);
					redirect(base_url('support/announce_load'));
				}
			}

			function search_announcements($offset=0) {
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST){
					$keyword = $this -> input -> post('keyword');
					$limit = 10;
					$data['keyword'] = $keyword;
					$config['base_url'] = base_url('support/search_announcement/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this->Buyer_Model->search_announcement_rows($keyword);
					$this->pagination->initialize($config);
					$data['announcements'] = $this->Buyer_Model->search_announcement($limit, $offset, $keyword);
				    $data["new_links"] = $this->pagination->create_links();  
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
					$this -> load -> view('support/announce_load', $data);
				}
			}

		function soa(){
			$this->check_access($x=22);
			$data = $this->get_notif();
			$data['active'] = "mtc";
			
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/soa', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

			function project_list($offset=0) {
				$this->check_access($x=22);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				$limit = 10;
				if($session_data['ui_role'] == 1){
					$where = 0;
				}else{
					$where = $session_data['sess_id'];
				}
				$config['base_url'] = base_url('support/project_list/0');
				$config['per_page'] = $limit;
				$config['total_rows'] = $this -> Buyer_Model -> get_projectlist_rows($where);
				$this->pagination->initialize($config);
				$data['new']= $this -> Buyer_Model -> get_projectlist($config["per_page"],$offset,$where);
			    $data["new_links"] = $this->pagination->create_links();
				$data['total_pages'] = $config['total_rows'];
				$data['display_offset'] = $offset + 1;
				$data['display_limit'] = $offset + $limit;
				if($data['display_limit'] > $data['total_pages']){
					$data['display_limit'] = $data['total_pages'];
				}
				if($data['total_pages'] == 0){
					$data['display_offset'] = $offset;
					$data['display_limit'] = 0;
				}
				$this -> load -> view('support/project_list', $data);
			}	

			function search_proj($offset=0){
				$this->check_access($x=22);
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$data = array('sess_id' => $session_data['sess_id'],
						  	  'sess_uname' => $session_data['sess_uname'],
						  	  'access' => $session_data['ui_access'],
						  	  'role' => $session_data['ui_role']);
				if($_POST){
					$keyword = $this -> input -> post('key');
					$limit = 10;
					if($session_data['ui_role'] == 1){
						$where = 0;
					}else{
						$where = $session_data['sess_id'];
					}
					$data['key'] = $keyword;
					$config['func'] = 'paginate2';
					$config['base_url'] = base_url('support/search_proj/0');
					$config['per_page'] = $limit;
					$config['total_rows'] = $this->Buyer_Model->get_search_soaProj_rows($keyword, $where);
					$this->pagination->initialize($config);
					$data['new'] = $this->Buyer_Model->get_search_soaProj($limit, $offset, $keyword, $where);
				    $data["new_links"] = $this->pagination->create_links();  
					$data['total_pages'] = $config['total_rows'];
					$data['display_offset'] = $offset + 1;
					$data['display_limit'] = $offset + $limit;
					if($data['display_limit'] > $data['total_pages']){
						$data['display_limit'] = $data['total_pages'];
					}
					if($data['total_pages'] == 0){
						$data['display_offset'] = $offset;
						$data['display_limit'] = 0;
					}
					$this -> load -> view('support/search_proj', $data);
				}
			}

			function soa_act(){
				if($_POST){
					$swenr = $this->input->post('swenr');
					$status = $this->input->post('act');
					$where = "SWENR =";

					$table = "ZHOA_PROJECTS";
					$data = array('SOA_ACTIVE'=>$status);
					$this->Main->update_data($where, $table, $swenr, $data);
				}
			}

		function housecon_type(){
			if($_POST){
				$where = $this->input->post('where');
				$select=""; $table="ZHOA_TASKS"; $where="CODEGRUPPE = '" .$where. "'"; $orderby="KURZTEXT ASC";
				$sub_req = $this -> Main -> get_data_where($select, $table, $where, $orderby);

				echo "<option value=''>--select house concern type--</option>";
				foreach($sub_req as $row){
					echo "<option value='".$row['CODE']."'>".$row['KURZTEXT']."</option>";
				}
			}
		}

		function account_settings(){
			$this->check_access($x=8);
			$session_data = $this->session->userdata('hoa_admin_accepted__'); 
			
			$notification = "";
			if(isset($_POST['changepword'])){
				$cpword = md5($_POST['cpword']);
				$npword = md5($_POST['npword']);
				$rpword = md5($_POST['rpword']);
				$select = "";
				$table = "ZHOA_admlogin"; 
				$where = "a_id = ".$session_data['sess_id'];
				$acc = $this -> Main -> select_data_where($select, $table, $where);
				foreach($acc as $row){
					$password = $row['a_pass'];
				}
				if($cpword == $password){
					if($npword == $rpword){
						$where1 = "a_id"; 
						$table = "ZHOA_admlogin"; 
						$id = $session_data['sess_id'];
						$data = array(
							'a_pass' => $rpword
							);
						$this -> Main -> update_data($where1, $table, $id, $data);
						$notification = "<div class=\"alert alert-success\" role=\"alert\">Your password is successfully updated.</div>";
					} else {
						$notification = "<div class=\"alert alert-danger1\" role=\"alert\">New Password and Retype Password didn't match!.</div>";
					}	
				} else {
					$notification = "<div class=\"alert alert-danger1\" role=\"alert\">Current Password didn't match!. Please try again.</div>";
				}
			}
			$data = $this->get_notif();
			$data['notification'] = $notification;
			$data['active'] = "accountSettings";
			$this -> load -> view('templates/sup-templates/header', $data);
			$this -> load -> view('support/account_settings', $data);
			$this -> load -> view('templates/sup-templates/footer');
		}

		function get_sub_req($id=NULL){
			if($id == NULL){
				show_404();
			} else {
				$select=""; $table="ZHOA_subrequest_type"; $where="st_rtype = ".$id; $orderby="st_desc ASC";
				$sub_req = $this -> Main -> get_data_where($select, $table, $where, $orderby);
				echo "<option value=''>--Select--</option>";
				foreach($sub_req as $row){
					echo "<option value='".$row['st_id']."'>".$row['st_desc']."</option>";
				}
				echo "</select>";
			}
		}

		function check_access($x){
			if(!$this->session->userdata('hoa_admin_accepted__')){
				redirect(base_url('support/'));
				exit;
			} else {
				$session_data = $this->session->userdata('hoa_admin_accepted__');
				$access = $session_data['ui_access'];
		        $acc = explode("-", $access);
				$reg=$acc[$x];
				if($reg!=1){
		        	show_404();
		        	exit;
		    	}
			}
		}

		function send_email($email, $message, $subject){
			$this->load->library('email');
			$this->email->initialize(array(
					'protocol' => 'smtp',
					'smtp_host' => '192.168.7.83',
					'smtp_user' => '',
					'smtp_pass' => '',
					'smtp_port' => 25,
					'crlf' => "\r\n",
					'newline' => "\r\n",
					'mailtype' => 'html'
			));
			$this->email->from('noreply@vistahome.com.ph', "Vista HOMe Portal");
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($message);
			if($this->email->send()){
				return true;
			}else{
				return false;
			}
		}

		function logout(){
			$this->session->unset_userdata('hoa_admin_accepted__');
			$this->session->sess_destroy();
		    redirect(base_url('support'), 'refresh');
		}

		function get_unit($swenr, $refno){
			$select = "";
			$table = "ZHOA_SU";
			$where ="SWENR = ".$swenr." and  REFNO ='".$refno."'";
			$raw = $this->Main->select_data_where($select,$table,$where);
			foreach ($raw as $key) {
				$table = "ZHOA_unit";
				$where ="u_desc = '".$key['XMBEZ']."'";
				$u = $this->Main->select_data_where($select,$table,$where);
				foreach ($u as $value) {
					return $value['u_id'];
				}
			}
		}

		function get_company($swenr){
			$select = "";
			$table = "ZHOA_PROJECTS";
			$where ="SWENR = ".$swenr;
			$raw = $this->Main->select_data_where($select,$table,$where);
			foreach ($raw as $key) {
				$table = "ZHOA_PROJ_AREA";
				$where ="PROJ_AREA = '".$key['ADRZUS']."'";
				$u = $this->Main->select_data_where($select,$table,$where);
				foreach ($u as $value) {
					return $value['PROJ_ID'];
				}
			}
		}

		function name(){
			$this -> load -> view('support/underconstruction');
			$select = "";
			$table = "name as a";
			$join1 = "ZHOA_BUYER_PROF as b";
			$join2 = "a.buyerid = b.KUNNR and a.so=b.VBELN";
			$where = "b.ACTIVE = 1";
			$orderby = "a.id asc";
			$names = $this->Main->get_data_where_join($select, $table, $orderby, $join1, $join2, $where);
			$today = date('Y-m-d h:i:s');

			if($names){
				foreach ($names as $k) {
					// $table = "name";
					// $data = array('so' => $k['VBELN']);
					// $where = "id";
					// $id = $k['id'];
					// $this -> Main -> update_data($where, $table, $id, $data);

					$raw = $k['name'];
			    	$p = explode(" ", $raw);
			    	if(isset($p[0]) && isset($p[1]) && !isset($p[2]) && !isset($p[3]) && !isset($p[4])){
			    		$fname = $p[0];
			    		$lname = $p[1];
			    	}
			    	if(isset($p[0]) && isset($p[1]) && isset($p[2]) && isset($p[3]) && isset($p[4])){
			    		$fname = $p[0]." ".$p[1];
			    		$lname = $p[3]." ".$p[4];
			    	}
			    	if(isset($p[0]) && isset($p[1]) && isset($p[2]) && !isset($p[3]) && !isset($p[4])){
			    		$fname = $p[0];
			    		$lname = $p[2];
			    	}

			    	$unit = $this->get_unit($k['SWENR'], $k['REFNO']);
			    	$company = $this->get_company($k['SWENR']);
						
					$table = "ZHOA_byrlogin";
					$data = array('b_uname' => $k['uname'],'b_pass' => $k['pass'],'b_is_validated' => 1,
									'b_is_active' => 1,'b_date_reg' => $k['cdate'],'b_cpass_date' => $today);

					$matching = md5($k['cdate'].$k['pass'].$k['uname']);

					$this -> Main -> insert_data($table, $data);

					$select = "b_id, b_uname, b_pass, b_date_reg";
					$table = "ZHOA_byrlogin";
					$recent = $this -> Main -> select_data($select, $table);

					foreach($recent as $row){
						$regdate = strtotime($row['b_date_reg']);

						if($matching == md5($k['cdate'].$row['b_pass'].$row['b_uname'])){
							$table = "ZHOA_byrinfo";
							$data = array(
												'bi_owner_id' => $row['b_id'],
												'bi_cust_no' => $k['KUNNR'],
												'bi_company' => $company,
												'bi_unit' => $unit,
												'bi_project' => $k['XWETEXT'],
												'bi_projcode' => $k['SWENR'],
												'bi_blklot' => $k['REFNO'],
												'bi_lname' => $lname,
												'bi_fname' => $fname,
												'bi_mname' => NULL,
												'bi_bdate' => $k['bday'],
												'bi_add1' => $k['street'].", ".$k['city'],
												'bi_add2' => $k['street'].", ".$k['city'],
												'bi_province' => $k['city'],
												'bi_zip' => $k['zip'],
												'bi_contact' => $k['MOBILE'],
												'bi_email' => $k['email'],
												'bi_sec1' => NULL,
												'bi_ans1' => NULL,
												'bi_sec2' => NULL,
												'bi_ans2' => NULL,
												'bi_regdate' => $k['cdate'],
												'bi_csec_date' => $today,
												'bi_code' => NULL,
												'bi_code_date' => $today,
												'bi_email_verif' => 1,
												'bi_temp_concern' => NULL,
												'bi_temp_concern_date' => NULL, 
												'bi_status' => 1,  
												'bi_process_date' => $today,  
												'bi_regdate' => $today,  
												'bi_process_by' => 1
											);

							$this -> Main -> insert_data($table, $data);
						}		
					}
				}
				show_404();
			}else{
				$this -> load -> view('support/underconstruction');
			}
		}

		function normalize(){
			$select = "";
			$table = "name";
			$raw = $this->Main->select_data($select, $table);
			foreach ($raw as $ho) {
				$id = $ho['id'];
				$table = "ZHOA_BUYER_PROF";
				$where = "SWENR ='".$ho['projid']."' and REFNO ='".$ho['refno']."'";
				$valid = $this->Main->select_data_where($select, $table, $where);
				if(!$valid){
					$where = "id";
					$table = "name";
					$this->Main->delete_data_where($where, $id, $table);
				} else {
					foreach ($valid as $datum) {
						$where = "id";
						$table = "name";
						$data = array('so'=>$datum['VBELN'], 'num'=>$datum['MOBILE'], 'email'=>$datum['EMAIL']);
						$this->Main->update_data($where, $table, $id, $data);
					}
				}
			}
			echo "<script> alert('Finish Normalizing raw data');</script>";
		}

		function normalize_ticket_status(){
					$select = "";
					$table = "ZHOA_tickets";
					$get_tickets = $this -> Main -> select_data($select, $table);

					foreach ($get_tickets as $k) {
						$select = "";
						$table = "ZHOA_concerns";
						$where = "c_ticket_id =".$k['t_id'];
						$get_concern = $this -> Main -> select_data_where($select, $table, $where);

						$open = 0;	$posted = 0;	$counter = 0;
						foreach($get_concern as $row){
							$cdate = $row['c_closed'];
                    		$cby = $row['c_completedby'];

							if($row['c_is_active'] == 1 || (!isset($cby) && ($cdate == '00000000')) ){        // if open or unposted
								$open++;
							}
							if($row['qmnum'] != ""){
								$posted++;
							}
							$counter++;
						}

						if ($posted == 0) {
							$pstat = 1;
						}
						if($counter > $posted && $posted > 0){
							$pstat = 2;
						}
						if($posted == $counter){
							$pstat = 3;
						}
						if($open == 0){
							$stat = 2;
						} 
						if($open > 0){
							$stat = 1;
						} 

						$where = "t_id";
						$table = "ZHOA_tickets"; 
						$data = array( 't_status' => $stat,
									   't_post_status' => $pstat );
						$this -> Main -> update_data($where, $table, $k['t_id'], $data);
					}
		}

		function normalize_BE(){
			$select = "";
			$table = "ZHOA_role_business";
			$list = $this->Main->select_data($select, $table);

			foreach ($list as $be) {
				$id = $be['rb_pcode'];
				$proj = $this->Main->custom($id);

				foreach ($proj as $k) {
					$data = array('rb_ccode' => $k['BUKRS'], 
								'rb_cname' => $k['BUTXT'],
								'rb_pname' => $k['XWETEXT'],
								'rb_createdby' => 1);
				$where = "rb_id =";
				$id = $be['rb_id'];
				$this->Main->update_data($where, $table, $id, $data);
				}
			}
		}
				
	}

/* End of file support.php */
/* Location: ./application/controllers/support.php */