<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pagination2 {
	
	/** Generate pagination **/
	
	public function displayPagination($total_pages, $adjacents, $targetpage, $page, $start, $limit, $keyword) {
		$keyword = urlencode($keyword);
	
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;
		
		$pagination = "";
			if($lastpage > 1)
			{	
				$pagination .= "<div class=\"reg_user_pagination\">";
				//previous button
				if ($page > 1) 
					$pagination.= "<a href='".$targetpage.$prev."/".$keyword."' data='".$targetpage."/".$prev."/".$keyword."' class=\"navs\">previous</a>";
				else
					$pagination.= "<span class=\"disabled navs\" >previous</span>";	
				
				//pages	
				if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href='".$targetpage.$counter."/".$keyword."' data='page=".$counter."'>$counter</a>";					
					}
				}
				elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href='".$targetpage.$counter."/".$keyword."' data='page=".$counter."'>$counter</a>";					
						}
						$pagination.= "...";
						$pagination.= "<a href='".$targetpage.$lpm1."/".$keyword."' data='page=".$lpm1.">$lpm1</a>";
						$pagination.= "<a href='".$targetpage.$lastpage."/".$keyword."' data='page=".$lastpage."'>$lastpage</a>";		
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						$pagination.= "<a href='".$targetpage."1/".$keyword."' data='page=1'>1</a>";
						$pagination.= "<a href='".$targetpage."2/".$keyword."' data='page=2'>2</a>";
						$pagination.= "...";
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href='".$targetpage.$counter."/".$keyword."' data='page=".$counter."'>$counter</a>";					
						}
						$pagination.= "...";
						$pagination.= "<a href='".$targetpage.$lpm1."/".$keyword."' data='page=".$lpm1."'>$lpm1</a>";
						$pagination.= "<a href='".$targetpage.$lastpage."/".$keyword."' data='page=".$lastpage."'>$lastpage</a>";		
					}
					//close to end; only hide early pages
					else
					{
						$pagination.= "<a href='".$targetpage."1/".$keyword."' data='page=1'>1</a>";
						$pagination.= "<a href='".$targetpage."2/".$keyword."' data='page=2'>2</a>";
						$pagination.= "...";
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href='".$targetpage.$counter."/".$keyword."' data='page=".$counter."'>$counter</a>";					
						}
					}
				}
				
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<a href='".$targetpage.$next."/".$keyword."' data='page=".$next."/".$keyword."' class=\"navs\">next</a>";
				else
					$pagination.= "<span class=\"disabled navs hide\">next</span>";
				$pagination.= "</div>\n";		
			}
		
		return $pagination;
	}



}