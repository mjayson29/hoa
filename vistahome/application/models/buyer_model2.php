<?php
class Buyer_Model extends CI_Model {
	
// function get_new_reg($limit,$offset,$keyword) {
function get_new_ownerlist($limit,$offset) {

	$query = "SELECT * FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id 
		      LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id 
			  WHERE a.bi_status = 1
			  ORDER BY b.b_uname desc
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_new_ownerlist_rows() {
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_byrlogin as b','a.bi_owner_id = b.b_id','left');
	$this->db->join('ZHOA_unit as c','a.bi_unit = c.u_id','left');
	$this->db->where('a.bi_status = 1');
	$this->db->order_by('b.b_uname','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

function get_search_owners($limit, $offset, $key){
	$queryJoin = "SELECT * FROM ZHOA_byrinfo as a
				  LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id
				  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id ";
	$counter = 0;
		
		if($key != ""){
			$queryJoin .= " WHERE  a.bi_status = 1 AND";
		}

		if($key != ""){
			$counter++;
		}

		if($key != ""){
			$queryJoin .= " ( b.b_uname LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_lname LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_fname LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_project LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_blklot LIKE '%".$key."%'";
	 		$queryJoin .= "  OR c.u_desc LIKE '%".$key."%')";

			if($counter > 1){
				$counter--;
				$queryJoin .= " AND";
			}else{
				$queryJoin .= " ";
			}
		}
		
		$queryJoin .="WHERE a.bi_status = 1
					  ORDER BY b.b_uname desc
				      OFFSET ".$offset." ROWS
	 			  	  FETCH NEXT ".$limit." ROWS ONLY";
		$query = $this->db->query($queryJoin);
		return $query->result_array();
}

function get_search_owners_rows($key){
	$queryJoin = "SELECT * FROM ZHOA_byrinfo as a
				  LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id
				  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id ";
	$counter = 0;
		
		if($key != ""){
			$queryJoin .= " WHERE a.bi_status = 1 AND";
		}

		if($key != ""){
			$counter++;
		}

		if($key != ""){
			$queryJoin .= " ( b.b_uname LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_lname LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_fname LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_project LIKE '%".$key."%'";
			$queryJoin .= "  OR a.bi_blklot LIKE '%".$key."%'";
	 		$queryJoin .= "  OR c.u_desc LIKE '%".$key."%')";

			if($counter > 1){
				$counter--;
				$queryJoin .= " AND";
			}else{
				$queryJoin .= " ";
			}
		}
		$queryJoin .="WHERE a.bi_status = 1 
				      ORDER BY b.b_uname desc";
		$query = $this->db->query($queryJoin);
		return $query->num_rows();
}

function get_new_reg($limit,$offset) {

	$query = "SELECT * FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID
			  WHERE a.bi_status = '0' 
		      ORDER BY a.bi_regdate ASC
		      OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($query);
	$get_query = $query->result_array();
	
	date_default_timezone_set("Asia/Manila");
	$today = date("Y-m-d");
	foreach($get_query as $row){
		// if(substr($row['bi_regdate'],0,10) < $today && $row['bi_email_verif'] == 0){
		if($row['bi_email_verif'] == 0){
			return false;
		} else {
			return $query->result_array();
		}
	}
	
}
	// AND a.bi_regdate = '".date("Y-m-d")."'

function get_new_reg_rows() {
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_admlogin as b','a.bi_process_by = b.a_id','left');
	$this->db->join('ZHOA_PROJ_AREA as c','a.bi_company = c.PROJ_ID','left');
	$this->db->where('a.bi_status','0');
	// $this->db->where('a.bi_email_verif','0');
	// $this->db->where('a.bi_regdate', date("Y-m-d"));
	$this->db->order_by('a.bi_regdate','ASC');
	$query = $this->db->get();
	$get_query = $query->result_array();

	$waiting = 0;
	date_default_timezone_set("Asia/Manila");
	$today = date("Y-m-d");
	foreach($get_query as $row){
		// if(substr($row['bi_regdate'],0,10) < $today && $row['bi_email_verif'] == 0){
		if($row['bi_email_verif'] == 0){
			$waiting++;
		}
	}
	return ($query->num_rows) - $waiting;
}

function get_out_reg_rows(){
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_admlogin as b','a.bi_process_by = b.a_id','left');
	$this->db->join('ZHOA_PROJ_AREA as c','a.bi_company = c.PROJ_ID','left');
	$this->db->where('a.bi_status','0');
	$this->db->where('a.bi_email_verif','1');
	$this->db->where('a.bi_regdate <', date("Y-m-d")); 
	$this->db->order_by('a.bi_regdate','desc');
	$query = $this->db->get();

	return $query->num_rows;
}

// function get_out_reg($limit,$offset,$keyword){
function get_out_reg($limit,$offset){
	$query = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID
			  WHERE a.bi_status = '0'
			  AND a.bi_email_verif = 1
			  AND a.bi_regdate < '".date("Y-m-d")."' ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);	
	return $query->result_array();
}

function get_con_reg_rows(){
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_admlogin as b','a.bi_process_by = b.a_id','left');
	$this->db->join('ZHOA_PROJ_AREA as c','a.bi_company = c.PROJ_ID','left');
	$this->db->where('a.bi_status','1');
	$this->db->order_by('a.bi_regdate','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

// function get_con_reg($limit,$offset,$keyword){
function get_con_reg($limit,$offset){
	$query = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID
			  WHERE a.bi_status = 1
		      ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";	
	
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_rej_reg_rows(){
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_admlogin as b','a.bi_process_by = b.a_id','left');
	$this->db->join('ZHOA_PROJ_AREA as c','a.bi_company = c.PROJ_ID','left');
	$this->db->where('a.bi_status','2');
	$this->db->order_by('a.bi_regdate','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

// function get_rej_reg($limit,$offset,$keyword){
function get_rej_reg($limit,$offset){
	$query = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID
			  WHERE a.bi_status = 2
		      ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_wait_reg($limit,$offset) {

	$query = "SELECT * FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID
			  WHERE a.bi_status = '0' AND a.bi_email_verif = 0
		      ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($query);
	return $query->result_array();
	}
	// AND a.bi_regdate = '".date("Y-m-d")."'

function get_wait_reg_rows() {
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_admlogin as b','a.bi_process_by = b.a_id','left');
	$this->db->join('ZHOA_PROJ_AREA as c','a.bi_company = c.PROJ_ID','left');
	$this->db->where('a.bi_status','0');
	$this->db->where('a.bi_email_verif','0');
	// $this->db->where('a.bi_regdate', date("Y-m-d"));
	$this->db->order_by('a.bi_regdate','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

function get_buyer_by_id($id){
	$this->db->select('*');
	$this->db->from('ZHOA_byrinfo as a');
	$this->db->join('ZHOA_PROJ_AREA as b','a.bi_company = b.PROJ_ID','left');
	$this->db->join('ZHOA_unit as c','a.bi_unit = c.u_id','left');
	// $this->db->join('ZHOA_concern_temp as d','a.bi_owner_id = d.ct_owner_id','left');
	$this->db->join('ZHOA_BUYER_PROF as e','a.bi_cust_no = e.KUNNR','left');
	$this->db->join('ZHOA_byrlogin as f','a.bi_owner_id = f.b_id','left');
	$this->db->where('a.bi_id',$id);
	$this->db->limit(1);
	$query = $this->db->get();
	return $query->result_array();
}

function get_sap_info($id, $blklot){
	$this->db->select('*');
	$this->db->from('ZHOA_BUYER_PROF as a');
	$this->db->join('ZHOA_PROJ_AREA as b','a.PROJ_ID = b.PROJ_ID','left');
	$this->db->where('KUNNR',$id);
	$this->db->where('REFNO',$blklot);
	$query = $this->db->get();
	return $query->result_array();
}

function updateStatus($id,$status,$reason,$process_by,$process_date){
		$data = array('bi_status' => $status,
					  'bi_remarks' => $reason,
					  'bi_process_by' => $process_by,
					  'bi_process_date' => $process_date);
		
		$this->db->where('bi_id',$id);
		$query = $this->db->update('ZHOA_byrinfo',$data);
		return $query;
}

function get_search_reg($limit, $offset, $sign_up_date_start, $sign_up_date_end, $process_date_start, $process_date_end, $company, $project, $statusSearch, $processBy, $key){
	
$queryJoin = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_PROJ_AREA as b ON a.bi_company = b.PROJ_ID
  			  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id
			  LEFT JOIN ZHOA_concern_temp as d ON a.bi_owner_id = d.ct_owner_id
			  LEFT JOIN ZHOA_admlogin as f ON a.bi_process_by = f.a_id";
//			  LEFT JOIN ZHOA_BUYER_PROF as g ON a.bi_cust_no = g.KUNNR";
	
   $counter = 0;
	
	if($sign_up_date_start != "" || $sign_up_date_end != "" || $process_date_start != "" || $process_date_end != "" || $company != "" || $project != "" || $statusSearch != "" || $processBy != "" || $key != ""){
		$queryJoin .= " WHERE";
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$counter++;
	}
	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$counter++;
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
		$counter++;
	}
	if($process_date_start != "" && $process_date_end != ""){
		$counter++; //1
	}
	if($company != ""){
		$counter++;
	}
	if($project != ""){
		$counter++;
	}
	
	if($statusSearch != ""){
		$counter++;
	}
	
	if($processBy != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$queryJoin .= " a.bi_regdate = '".$sign_up_date_start."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$queryJoin .= " a.bi_regdate <= '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
  		$queryJoin .= " a.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($process_date_start != "" && $process_date_end != ""){
		$queryJoin .= "  a.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
		
 		if($counter > 1){ 
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";  
 		}
	}
	
	if($company != ""){
		$queryJoin .= "  b.PROJ_ID LIKE '%".$company."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  a.bi_project LIKE '%".$project."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($statusSearch != ""){
		
				switch ($statusSearch){
				    	    case 0:
				    	    	//NEW
				    	    	$queryJoin .= "  a.bi_status = '0'";
				    	    	$queryJoin .= "  AND a.bi_regdate = '".date("Y-m-d")."' AND a.bi_email_verif = 1";
				    			break;
				    		case 1:
				    			//OUTSTANDING
				    			$queryJoin .= "  a.bi_status = '0'";
				    			$queryJoin .= "  AND a.bi_regdate < '".date("Y-m-d")."' AND a.bi_email_verif = 1";
				    			break;
				    		case 2:
				    			//CONFIRMED
				    			$queryJoin .= "  a.bi_status = '1'";
				    			break;
				    		case 3:
				    			//REJECTED
				    			$queryJoin .= "  a.bi_status = '2'";
				    			break;
				    		case 4: 
				    			//WAITING
				    			$queryJoin .= "  a.bi_status = '0'";
				    			$queryJoin .= "  AND a.bi_email_verif = 0";

				    	}
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($processBy != ""){
		$queryJoin .= "  f.a_uname LIKE '%".$processBy."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.bi_regdate LIKE '%".$key."%'";
		$queryJoin .= "  OR b.PROJ_AREA LIKE '%".$key."%'";
		// $queryJoin .= "  OR g.XWETEXT  LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_blklot LIKE '%".$key."%'";
		$queryJoin .= "  OR c.u_desc LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_concern_temp LIKE '%".$key."%'";
//$queryJoin .= "  OR a.concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_status LIKE '%".$key."%'";
 		$queryJoin .= "  OR f.a_uname LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_process_date LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_project LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != ""){
// 		$queryJoin .= " a.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
// 	}
	
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != "" && $process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= " AND";
// 	}
	
// 	if($process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= "  a.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
// 	}
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != "" || $process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= " AND";
// 	}
	
// 	if($company != ""){
// 		$queryJoin .= "  b.tc_brand LIKE '%".$company."%'";
// 	}
	
// // 	if($sign_up_date_start != "" && $sign_up_date_end != "" || $process_date_start != "" && $process_date_end != "" || $company != "" || $project != ""){
// // 		$queryJoin .= " AND";
// // 	}
	
// // 	if($project != ""){
// // 		$queryJoin .= "  g.XWETEXT LIKE '%".$project."%'";
// // 	}
	
	
	$queryJoin .=" ORDER BY a.bi_regdate desc
			      OFFSET ".$offset." ROWS
 			  	  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->result_array();
}

function get_search_reg_rows($sign_up_date_start, $sign_up_date_end, $process_date_start, $process_date_end, $company, $project, $statusSearch, $processBy, $key){
	
	$queryJoin = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_PROJ_AREA as b ON a.bi_company = b.PROJ_ID
  			  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id
			  -- LEFT JOIN ZHOA_concern_temp as d ON a.bi_owner_id = d.ct_owner_id
			  LEFT JOIN ZHOA_admlogin as f ON a.bi_process_by = f.a_id";
			  // LEFT JOIN ZHOA_BUYER_PROF as g ON a.bi_cust_no = g.KUNNR";
	
   $counter = 0;
	
	if($sign_up_date_start != "" || $sign_up_date_end != "" || $process_date_start != "" || $process_date_end != "" || $company != "" || $project != "" || $statusSearch != "" || $processBy != "" || $key != ""){
		$queryJoin .= " WHERE";
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$counter++;
	}
	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$counter++;
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
		$counter++;
	}
	if($process_date_start != "" && $process_date_end != ""){
		$counter++; //1
	}
	if($company != ""){
		$counter++;
	}
	if($project != ""){
		$counter++;
	}
	
	if($statusSearch != ""){
		$counter++;
	}
	
	if($processBy != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$queryJoin .= " a.bi_regdate = '".$sign_up_date_start."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$queryJoin .= " a.bi_regdate <= '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
  		$queryJoin .= " a.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($process_date_start != "" && $process_date_end != ""){
		$queryJoin .= "  a.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
		
 		if($counter > 1){
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";
 		}
	}
	
	if($company != ""){
		$queryJoin .= "  b.PROJ_ID LIKE '%".$company."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  a.bi_project LIKE '%".$project."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($statusSearch != ""){
		
				switch ($statusSearch){
				    	    case 0:
				    	    	//NEW
				    	    	$queryJoin .= "  a.bi_status = '0'";
				    	    	$queryJoin .= "  AND a.bi_regdate = '".date("Y-m-d")."' AND a.bi_email_verif = 1";
				    			break;
				    		case 1:
				    			//OUTSTANDING
				    			$queryJoin .= "  a.bi_status = '0'";
				    			$queryJoin .= "  AND a.bi_regdate < '".date("Y-m-d")."' AND a.bi_email_verif = 1";
				    			break;
				    		case 2:
				    			//CONFIRMED
				    			$queryJoin .= "  a.bi_status = '1'";
				    			break;
				    		case 3:
				    			//REJECTED
				    			$queryJoin .= "  a.bi_status = '2'";
				    			break;
				    		case 4: 
				    			//WAITING
				    			$queryJoin .= "  a.bi_status = '0'";
				    			$queryJoin .= "  AND a.bi_email_verif = 0";

				    	}
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($processBy != ""){
		$queryJoin .= "  f.a_uname LIKE '%".$processBy."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.bi_regdate LIKE '%".$key."%'";
		$queryJoin .= "  OR b.PROJ_AREA LIKE '%".$key."%'";
		// $queryJoin .= "  OR g.XWETEXT LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_blklot LIKE '%".$key."%'";
		$queryJoin .= "  OR c.u_desc LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_concern_temp LIKE '%".$key."%'";
		// $queryJoin .= "  OR d.ct_desc LIKE '%".$key."%'";
//$queryJoin .= "  OR a.concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_status LIKE '%".$key."%'";
 		$queryJoin .= "  OR f.a_uname LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_process_date LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_project LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	$queryJoin .=" ORDER BY a.bi_regdate desc";

	$query = $this->db->query($queryJoin);
	return $query->num_rows();
}

function get_projects_by_cust_no($cust_no){
	$this->db->where('KUNNR',$cust_no);
	$query = $this->db->get('ZHOA_BUYER_PROF');
	return $query;
}

function get_new_concerns($limit,$offset){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE a.t_status = '1'
			  AND a.t_date_created >= '".date('Y-m-d').' 00:00:00.000'."'
			  AND a.t_date_created <= '".date('Y-m-d').' 23:59:59.997'."'
			  ORDER BY a.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_open_concerns($limit,$offset){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE a.t_status = '1'
			  AND a.t_date_created < '".date('Y-m-d').' 00:00:00.000'."'
			  ORDER BY a.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_closed_concerns($limit,$offset){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE a.t_status = '0'
			  ORDER BY a.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

/*function get_new_concerns() {
	$this->db->select('*');
	$this->db->from('ZHOA_tickets');
	$this->db->where('t_status','1');
	$this->db->where('t_date_created >=', date("Y-m-d").' 00:00:00.000');
	$this->db->where('t_date_created <=', date("Y-m-d").' 23:59:59.997');
	$this->db->order_by('t_date_created','desc');
	$query = $this->db->get();
	return $query->result_array();
}*/

function get_new_concerns_rows() {
	$this->db->select('*');
	$this->db->from('ZHOA_tickets');
	$this->db->where('t_status','1');
	$this->db->where('t_date_created >=', date("Y-m-d").' 00:00:00.000');
	$this->db->where('t_date_created <=', date("Y-m-d").' 23:59:59.997');
	$this->db->order_by('t_date_created','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

function get_open_concerns_rows() {
	$this->db->select('*');
	$this->db->from('ZHOA_tickets');
	$this->db->where('t_status','1');
	$this->db->where('t_date_created <', date("Y-m-d").' 00:00:00.000');
	$this->db->order_by('t_date_created','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

function get_closed_concerns_rows() {
	$this->db->select('*');
	$this->db->from('ZHOA_tickets');
	$this->db->where('t_status','0');
	$this->db->order_by('t_date_created','desc');
	$query = $this->db->get();
	return $query->num_rows;
}

function get_search_concern($limit, $offset, $date_posted_from, $date_posted_to, $status, $project_area, $project, $key){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
	";
//			  LEFT JOIN ZHOA_BUYER_PROF as g ON a.bi_cust_no = g.KUNNR";
	
   $counter = 0;
	
	if($date_posted_from != "" || $date_posted_to != "" || $status != "" || $project_area != "" || $project != "" || $key != ""){
		$queryJoin .= " WHERE";
	}

	if($date_posted_from != "" && $date_posted_to == ""){
		$counter++;
	}
	if($date_posted_from == "" && $date_posted_to != ""){
		$counter++;
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$counter++;
	}
	if($status != ""){
		$counter++;
	}
	if($project_area != ""){
		$counter++;
	}
	
	if($project != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_posted_from != "" && $date_posted_to == ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_from." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_posted_from == "" && $date_posted_to != ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_posted_to." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
 		if($counter > 1){
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";
 		}
	}

	if($status != ""){
		
				switch ($status){
				    	    case 0:
				    	    	//New
				    	    	$queryJoin .= "  a.t_status = '1'";
				    	    	$queryJoin .= "  AND a.t_date_created between '".date("Y-m-d")." 00:00:00.000' AND '".date("Y-m-d")." 23:59:59.997'";
				    			break;
				    		case 1:
				    			//Open
				    			$queryJoin .= "  a.t_status = '1'";
				    			$queryJoin .= "  AND a.t_date_created < '".date("Y-m-d")."'";
				    			break;
				    		case 2:
				    			//Closed
				    			$queryJoin .= "  a.t_status = '0'";
				    			break;
				    		
				    	}
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project_area != ""){
		$queryJoin .= "  d.PROJ_ID = '".$project_area."'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  c.XWETEXT like '%".$project."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.t_id LIKE '%".$key."%'";
		$queryJoin .= "  OR a.t_date_created LIKE '%".$key."%'";
		$queryJoin .= "  OR d.PROJ_AREA LIKE '%".$key."%'";
		// $queryJoin .= "  OR g.XWETEXT  LIKE '%".$key."%'";
		$queryJoin .= "  OR a.t_blklot LIKE '%".$key."%'";
		$queryJoin .= "  OR e.u_desc LIKE '%".$key."%'";
		//$queryJoin .= "  OR a.concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_status LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_reply_by LIKE '%".$key."%'";
 		$queryJoin .= "  OR c.XWETEXT LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	
	$queryJoin .=" ORDER BY a.t_date_created desc
			      OFFSET ".$offset." ROWS
 			  	  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->result_array();
}

function get_search_concern_rows($date_posted_from, $date_posted_to, $status, $project_area, $project, $key){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
	";
//			  LEFT JOIN ZHOA_BUYER_PROF as g ON a.bi_cust_no = g.KUNNR";
	
   $counter = 0;
	
	if($date_posted_from != "" || $date_posted_to != "" || $status != "" || $project_area != "" || $project != "" || $key != ""){
		$queryJoin .= " WHERE";
	}

	if($date_posted_from != "" && $date_posted_to == ""){
		$counter++;
	}
	if($date_posted_from == "" && $date_posted_to != ""){
		$counter++;
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$counter++;
	}
	if($status != ""){
		$counter++;
	}
	if($project_area != ""){
		$counter++;
	}
	
	if($project != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_posted_from != "" && $date_posted_to == ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_from." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_posted_from == "" && $date_posted_to != ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_posted_to." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
 		if($counter > 1){
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";
 		}
	}

	if($status != ""){
		
				switch ($status){
				    	    case 0:
				    	    	//New
				    	    	$queryJoin .= "  a.t_status = '1'";
				    	    	$queryJoin .= "  AND a.t_date_created between '".date("Y-m-d")." 00:00:00.000' AND '".date("Y-m-d")." 23:59:59.997'";
				    			break;
				    		case 1:
				    			//Open
				    			$queryJoin .= "  a.t_status = '1'";
				    			$queryJoin .= "  AND a.t_date_created < '".date("Y-m-d")."'";
				    			break;
				    		case 2:
				    			//Closed
				    			$queryJoin .= "  a.t_status = '0'";
				    			break;
				    		
				    	}
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project_area != ""){
		$queryJoin .= "  d.PROJ_ID = '".$project_area."'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  c.XWETEXT like '%".$project."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.t_id LIKE '%".$key."%'";
		$queryJoin .= "  OR a.t_date_created LIKE '%".$key."%'";
		$queryJoin .= "  OR d.PROJ_AREA LIKE '%".$key."%'";
		// $queryJoin .= "  OR g.XWETEXT  LIKE '%".$key."%'";
		$queryJoin .= "  OR a.t_blklot LIKE '%".$key."%'";
		$queryJoin .= "  OR e.u_desc LIKE '%".$key."%'";
		//$queryJoin .= "  OR a.concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_status LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_reply_by LIKE '%".$key."%'";
 		$queryJoin .= "  OR c.XWETEXT LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != ""){
// 		$queryJoin .= " a.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
// 	}
	
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != "" && $process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= " AND";
// 	}
	
// 	if($process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= "  a.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
// 	}
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != "" || $process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= " AND";
// 	}
	
// 	if($company != ""){
// 		$queryJoin .= "  b.tc_brand LIKE '%".$company."%'";
// 	}
	
// // 	if($sign_up_date_start != "" && $sign_up_date_end != "" || $process_date_start != "" && $process_date_end != "" || $company != "" || $project != ""){
// // 		$queryJoin .= " AND";
// // 	}
	
// // 	if($project != ""){
// // 		$queryJoin .= "  g.XWETEXT LIKE '%".$project."%'";
// // 	}
	
	
	$queryJoin .=" ORDER BY a.t_date_created desc";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->num_rows();
}

function get_concerns($ticketID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_concerns')
				-> WHERE('c_ticket_id', $ticketID)
				-> ORDER_BY('c_date_created', 'DESC')
				;
	$query = $this -> db -> get();
	return $query -> result_array();

}

function get_single_ticket($ticketID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_concerns as a')
				-> JOIN('ZHOA_request_type as b','a.c_rtype = b.rt_id','left')
				-> JOIN('ZHOA_subrequest_type as c','a.c_stype = c.st_id','left')
				// -> JOIN('ZHOA_tickets as d','a.c_ticket_id = d.t_id','left')
				-> WHERE('a.c_ticket_id',$ticketID)
				-> ORDER_BY('a.c_date_created', 'DESC')
				;
	$query = $this -> db -> get();
	return $query -> result_array();

}

function get_ticket_info($ticketID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_tickets as a')
				-> JOIN('ZHOA_BUYER_PROF as b','','left')
				;

}

function get_single_concern($concernID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_concern_details as a')
				// -> JOIN('ZHOA_concern_attachments as b','a.cd_id = b.ca_cd_id','left')
				-> WHERE('a.cd_concern_id',$concernID)
				-> WHERE('a.cd_is_deleted', 0)
				-> ORDER_BY('a.cd_date_created', 'DESC')
				;
	$query = $this -> db -> get();
	return $query -> result_array();

}
                        
function get_buyer_concerns($limit,$offset,$id){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE a.t_status = '1'
			  AND a.t_owner_id = '".$id."'
			  ORDER BY a.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_buyer_concerns_rows($id){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE a.t_status = '1'
			  AND a.t_owner_id = '".$id."'
			  ORDER BY a.t_date_created desc";
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->num_rows();	
}

// portal

function get_search_req($limit, $offset, $date_post_start, $date_post_end, $status, $key, $userid){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as a
		      LEFT JOIN ZHOA_byrlogin as b ON a.t_owner_id = b.b_id";

   $counter = 0;
	
	if($date_post_start != "" || $date_post_end != "" || $status != "" || $key != ""){
		$queryJoin .= " WHERE a.t_owner_id = ".$userid." AND";
	}

	if($date_post_start != "" && $date_post_end == ""){
		$counter++;
	}
	if($date_post_start == "" && $date_post_end != ""){
		$counter++;
	}
	
	if($date_post_start != "" && $date_post_end != ""){
		$counter++;
	}
	
	if($status != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_post_start != "" && $date_post_end == ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_post_start." 00:00:00.000' AND '".$date_post_start." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_post_start == "" && $date_post_end != ""){
		$queryJoin .= " a.t_date_created <= '".$date_post_end." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_post_start != "" && $date_post_end != ""){
  		$queryJoin .= " a.t_date_created BETWEEN '".$date_post_start."' AND '".$date_post_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($status != ""){
		
				switch ($status){
				    	    case 1:
				    	    	//NEW
				    	    	$queryJoin .= "  a.t_status = '1'";
				    			break;
				    		case 2:
				    			//OUTSTANDING
				    			$queryJoin .= "  a.t_status = '2'";
				    			break;
				    	}

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.t_date_created LIKE '%".$key."%'";
		$queryJoin .= "  OR a.t_blklot LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_status LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	$queryJoin .=" ORDER BY a.t_date_created desc
			      OFFSET ".$offset." ROWS
 			  	  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->result_array();
}

function get_search_req_rows($date_post_start, $date_post_end, $status, $key, $userid){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as a
		      LEFT JOIN ZHOA_byrlogin as b ON a.t_owner_id = b.b_id";

   $counter = 0;
	
	if($date_post_start != "" || $date_post_end != "" || $status != "" || $key != ""){
		$queryJoin .= " WHERE a.t_owner_id = ".$userid." AND";
	}

	if($date_post_start != "" && $date_post_end == ""){
		$counter++;
	}
	if($date_post_start == "" && $date_post_end != ""){
		$counter++;
	}
	
	if($date_post_start != "" && $date_post_end != ""){
		$counter++;
	}
	
	if($status != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_post_start != "" && $date_post_end == ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_post_start." 00:00:00.000' AND '".$date_post_start." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_post_start == "" && $date_post_end != ""){
		$queryJoin .= " a.t_date_created <= '".$date_post_end." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_post_start != "" && $date_post_end != ""){
  		$queryJoin .= " a.t_date_created BETWEEN '".$date_post_start."' AND '".$date_post_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($status != ""){
		
				switch ($status){
				    	    case 1:
				    	    	//NEW
				    	    	$queryJoin .= "  a.t_status = '1'";
				    			break;
				    		case 2:
				    			//OUTSTANDING
				    			$queryJoin .= "  a.t_status = '2'";
				    			break;
				    	}

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.t_date_created LIKE  '%".$key."%'";
		$queryJoin .= "  OR a.t_blklot LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_status LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	$queryJoin .=" ORDER BY a.t_date_created desc";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->num_rows();
}

function get_buyer_reply($id){
	$query = "SELECT * ZHOA_concern_details
			  WHERE cd_concern_id = $id
			  ORDER BY cd_date_created ASC";
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->result_array();	
}


}

?>