<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Main extends CI_Model {

		function select_data($select, $table){

			$this -> db -> SELECT($select)
						-> FROM($table);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_odby($select, $table, $order){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($order);
			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

			function select_data_l($select, $table, $start, $limit){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> LIMIT($start, $limit);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

			function select_data_odby_l($select, $table, $odby, $start, $limit){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($odby)
						-> LIMIT($start, $limit);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_where($select, $table, $where){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_where_in($select, $table, $where, $array){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE_IN($where, $array);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_where_limit($select, $table, $where, $limit){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> LIMIT($limit);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function select_data_odby_gpby($select, $table, $odby, $gpby){

			$this -> db -> SELECT($select)
						-> FROM($table)
						// -> WHERE($where)
						-> GROUP_BY($gpby)
						-> ORDER_BY($odby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_distinct_where($select, $table, $where, $odby){

			$this -> db -> DISTINCT()
						-> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> ORDER_BY($odby);

			$query = $this -> db -> get();

			return $query -> result_array();

		}

		function get_data_distinct($select, $table, $odby){

			$this -> db -> DISTINCT()
						-> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($odby);

			$query = $this -> db -> get();

			return $query -> result_array();

		}

		function get_data_where($select, $table, $where, $orderby){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> ORDER_BY($orderby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_where_limit($select, $table, $where, $orderby, $limit, $start){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where)
						-> ORDER_BY($orderby)
						-> LIMIT($limit, $start);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_join($select, $table, $orderby, $join1, $join2){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> ORDER_BY($orderby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_join_wherein($select, $table, $join1, $join2, $where, $array){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> WHERE_IN($where, $array);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_where_join($select, $table, $orderby, $join1, $join2, $where){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> WHERE($where)
						-> ORDER_BY($orderby);

			$query = $this -> db -> get();

			return $query -> result_array();
						
		}

		function get_data_where_join_limit($select, $table, $orderby, $join1, $join2, $where, $limit, $offset){ 

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> JOIN($join1, $join2)
						-> WHERE($where)
						-> ORDER_BY($orderby)
						-> LIMIT($limit, $offset);
 			  
			$query = $this -> db -> get();

			return $query -> result_array();

		}

		function count_select($select, $table){

			$this -> db -> SELECT($select)
						-> FROM($table);

			$query = $this -> db -> get();

			return $query -> num_rows();

		}	

		function count_select_where($select, $table, $where){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> WHERE($where);

			$query = $this -> db -> get();

			return $query -> num_rows();

		}	

		function count_select_where_orderby($select, $table, $where, $orderby){

			$this -> db -> SELECT($select)
						-> FROM($table)
						-> ORDER_BY($orderby)
						-> WHERE($where);

			$query = $this -> db -> get();

			return $query -> num_rows();

		}

		function delete_usrBusinessGroup($usrid, $grpid) {
			$this -> db -> where('ar_usrid = ' . "'" . $usrid . "'");
			$this -> db -> where('ar_grpid = ' . "'" . $grpid . "'");
			$this -> db -> delete("ZHOA_assign_role");
		}

		function login($uname, $pword, $is_valid, $is_active) {

			$this -> db -> select('a.*, b.bi_profimg');
			$this -> db -> from('ZHOA_byrlogin as a');
			$this -> db -> join('ZHOA_byrinfo as b','a.b_id = b.bi_owner_id');
			$this -> db -> where('a.b_uname = ' . "'" . htmlspecialchars($uname) . "'"); 
			$this -> db -> where('a.b_pass = ' . "'" . md5($pword) . "'"); 
			$this -> db -> where('a.b_is_validated = ' . "'" . $is_valid . "'"); 
			$this -> db -> where('a.b_is_active = ' . "'" . $is_active . "'"); 
			$this -> db -> limit(1);

			$query = $this -> db -> get();

			if($query -> num_rows() == 1)
			{
				return $query->result();
			}
			else
			{
				return false;
			}

		}

		function insert_data($table, $data){

	  		$this -> db -> insert($table, $data);
	  		return $this -> db -> insert_id();

	  	}	

	  	function update_data($where, $table, $id, $data) {

	  		$this -> db -> WHERE($where, $id);
	        $this -> db -> UPDATE($table, $data);

	  	}

	  	function delete_data_where($where, $id, $table) {
	  		$this->db->where($where, $id);
	  		$this->db->delete($table);
	  	}

	  	function custom($where){

			$query = "SELECT distinct a.BUKRS, a.BUTXT, a.XWETEXT from ZHOA_PROJECTS as a 
						where a.SWENR =".$where."";
			
			$query = $this->db->query($query);
			return $query->result_array();	
	  	}

	}

/* End of file main.php */
/* Location: ./application/models/main.php */