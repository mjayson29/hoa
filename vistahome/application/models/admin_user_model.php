<?php
class Admin_User_Model extends CI_Model {
	
	function get_user($uname, $pword, $is_valid, $is_active) {
		
		$this -> db -> select();
		$this -> db -> from('ZHOA_admlogin');
		$this -> db -> JOIN('ZHOA_usrole', 'ZHOA_admlogin.a_id = ZHOA_usrole.ur_usrid');	
		$this -> db -> where('a_uname',htmlspecialchars($uname));
		$this -> db -> where('a_pass',md5($pword));
		$this -> db -> where('a_is_validated',$is_valid);
		$this -> db -> where('a_is_active',$is_active);
		$this -> db -> limit(1);
	
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	
	}

	
	function get_access($uname, $pword, $is_valid, $is_active) {
		$this -> db -> select();
		$this -> db -> from('ZHOA_admlogin');
		$this -> db -> JOIN('ZHOA_assign_role', 'ZHOA_admlogin.a_id = ZHOA_assign_role.ar_usrid');
		$this -> db -> where('a_uname',htmlspecialchars($uname));
		$this -> db -> where('a_pass',md5($pword));
		$this -> db -> where('a_is_validated',$is_valid);
		$this -> db -> where('a_is_active',$is_active);
	
		$query = $this -> db -> get();
		return $query -> result_array();
	}
	
	function update_password($id,$npword){
			$this->db->where('a_id',$id);
			$query = $this->db->update('ZHOA_admlogin',array('a_pass' => md5($npword)));
			return $query;
	}
	
	function get_user_by_email($email){
		$this->db->where('a_email',$email);
		$query = $this->db->get('ZHOA_admlogin');
		return $query;
	}
	
}