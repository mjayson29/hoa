<?php
class Buyer_Model extends CI_Model {

function get_support_1st_reply($concern_id){
	$query = "SELECT TOP 1 cd_date_created 
			FROM ZHOA_concern_details
			WHERE cd_concern_id = ".$concern_id." AND cd_persona = 0
			ORDER BY cd_date_created ASC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_concern_details($where){
	$query = "SELECT * 
			FROM ZHOA_concerns AS a
			LEFT JOIN ZHOA_tickets AS b ON a.c_ticket_id = b.t_id
			LEFT JOIN ZHOA_BUYER_PROF AS c ON b.t_so = c.VBELN
			WHERE a.c_id = ".$where;
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_announcements($offset, $limit) {
	$query = "SELECT a.a_id, a.a_subject, a.a_msg, a.a_dposted, a.a_is_updated, a.a_attachment, a.a_postedby, a.a_dupd, a.a_byupd, b.a_uname as 'postedby', c.a_uname as 'byupd' 
				FROM ZHOA_announcements as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.a_postedby = b.a_id
		      LEFT JOIN ZHOA_admlogin as c ON a.a_byupd = c.a_id
			  WHERE a.a_is_deleted = 0
			  ORDER BY a.a_dposted DESC
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_client_announcement($proj, $offset, $limit, $byrid){
	$query = "SELECT a.a_id, a.a_subject, a.a_msg, a.a_dposted, a.a_is_updated, a.a_postedby, a.a_dupd, a.a_byupd, b.a_uname as 'postedby', c.a_uname as 'byupd', c.a_profimg, a.a_attachment
				from ZHOA_announcements as a
				left join ZHOA_admlogin as b on a.a_postedby = b.a_id
				LEFT JOIN ZHOA_admlogin as c ON a.a_byupd = c.a_id
				left join ZHOA_announcement_recipient as d on a.a_id = d.ar_announcement
				left join ZHOA_byrinfo as e on d.ar_proj = e.bi_company
				where e.bi_id = 1 and a.a_is_deleted = 0 or d.ar_proj = 1 and e.bi_owner_id = ".$byrid."
			  	ORDER BY a.a_dposted DESC
			 	OFFSET ".$offset." ROWS 
 			  	FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_client_announcement_row($where, $byrid){
	$query = "SELECT a.a_id, a.a_subject, a.a_msg, a.a_dposted, a.a_is_updated, a.a_postedby, a.a_dupd, a.a_byupd, b.a_uname as 'postedby', c.a_uname as 'byupd', c.a_profimg, a.a_attachment
				from ZHOA_announcements as a
				left join ZHOA_admlogin as b on a.a_postedby = b.a_id
				LEFT JOIN ZHOA_admlogin as c ON a.a_byupd = c.a_id
				left join ZHOA_announcement_recipient as d on a.a_id = d.ar_announcement
				left join ZHOA_byrinfo as e on d.ar_proj = e.bi_company
				where e.bi_id = 1 and a.a_is_deleted = 0 or d.ar_proj = 1  and e.bi_owner_id = ".$byrid."";
	$query = $this->db->query($query);
	return $query->num_rows();
}

function get_client_announcement_where($where){
	$query = "SELECT a.a_id, a.a_subject, a.a_msg, a.a_dposted, a.a_is_updated, a.a_postedby, a.a_dupd, a.a_byupd, b.a_uname as 'postedby', c.a_uname as 'byupd', c.a_profimg, a.a_attachment
				from ZHOA_announcements as a
				left join ZHOA_admlogin as b on a.a_postedby = b.a_id
				LEFT JOIN ZHOA_admlogin as c ON a.a_byupd = c.a_id
				left join ZHOA_announcement_recipient as d on a.a_id = d.ar_announcement
				where a.a_id = ".$where." ORDER BY a.a_dposted DESC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function search_announcement($limit, $offset, $keyword) {
	$queryJoin = "SELECT a.a_id, a.a_subject, a.a_msg, a.a_dposted, a.a_is_updated, a.a_attachment, a.a_postedby, a.a_dupd, a.a_byupd, b.a_uname as 'postedby', c.a_uname as 'byupd' 
			  FROM ZHOA_announcements as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.a_postedby = b.a_id
		      LEFT JOIN ZHOA_admlogin as c ON a.a_byupd = c.a_id";
			if ($keyword != "") {
				$queryJoin .= " WHERE";
				$queryJoin .= " ";
					$queryJoin .= " (a.a_subject LIKE '%".$keyword."%'";
					$queryJoin .= " OR b.a_uname LIKE '%".$keyword."%'";
					$queryJoin .= " OR c.a_uname LIKE '%".$keyword."%')";
			} else { $queryJoin .= " "; }
			$queryJoin .="ORDER BY b.a_uname desc
						  OFFSET ".$offset." ROWS
				 		  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($queryJoin);
	return $query->result_array();
}

function search_announcement_rows($keyword) {
	$queryJoin = "SELECT a.a_id, a.a_subject, a.a_msg, a.a_dposted, a.a_is_updated, a.a_postedby, a.a_dupd, a.a_byupd, b.a_uname as 'postedby', c.a_uname as 'byupd' 
			  FROM ZHOA_announcements as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.a_postedby = b.a_id
		      LEFT JOIN ZHOA_admlogin as c ON a.a_byupd = c.a_id";
			if ($keyword != "") {
				$queryJoin .= " WHERE";
				$queryJoin .= " ";
					$queryJoin .= " (a.a_subject LIKE '%".$keyword."%'";
					$queryJoin .= " OR b.a_uname LIKE '%".$keyword."%'";
					$queryJoin .= " OR c.a_uname LIKE '%".$keyword."%')";
			} else { $queryJoin .= " "; }
			$queryJoin .="ORDER BY b.a_uname desc";
	$query = $this->db->query($queryJoin);
	return $query->num_rows();
}

function get_specific_BE($usrid, $grpid) {
	$query = "SELECT  a.ar_ccode, b.BUTXT
			  FROM ZHOA_assign_role AS a
			  LEFT JOIN ZHOA_su AS b ON  a.ar_ccode = b.BUKRS
			  WHERE a.ar_usrid = ".$usrid." AND a.ar_grpid = ".$grpid."
			  GROUP BY a.ar_ccode, b.BUTXT
			  ORDER BY b.BUTXT ASC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_usrBE($usrid, $grpid) {
	$query = "SELECT a.ar_ccode, c.SWENR, c.XWETEXT
			from ZHOA_assign_role as a
			left join ZHOA_SU as c on a.ar_pcode=c.SWENR 
			where ar_usrid = ".$usrid." and a.ar_grpid = ".$grpid."
			GROUP BY a.ar_ccode, c.SWENR, c.XWETEXT
			ORDER BY c.XWETEXT ASC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_used_role_rows($where) {
	$this->db->select('*');
	$this->db->from('ZHOA_admlogin as a');
	$this->db->where('a.a_role', $where);
	$query = $this->db->get();
	return $query->num_rows;
}

function get_used_group_rows($where) {
	$this->db->select('*');
	$this->db->from('ZHOA_assign_role as a');
	$this->db->where('a.ar_grpid', $where);
	$query = $this->db->get();
	return $query->num_rows;
}

function get_usrname_rows($select, $table, $where){
	$this -> db -> SELECT($select)
   		  -> FROM($table)
		  -> WHERE('a_uname',$where);
	$query = $this -> db -> get();
	return $query -> num_rows();
}	
	
function get_new_ownerlist($limit,$offset,$where) {
	$query = "SELECT bi_id, b_uname, bi_lname, bi_fname, bi_project,bi_blklot,u_desc 
			  FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id 
		      LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id";
	if(!empty($where)){
		$query .=" WHERE a.bi_projcode IN (".$where.") AND a.bi_status = 1";
	}else{
		$query .=" WHERE a.bi_status = 1";
	}
	$query .= "ORDER BY b.b_uname desc
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_new_ownerlist_rows($where) {
	$query = "SELECT bi_id, b_uname, bi_lname, bi_fname, bi_project,bi_blklot,u_desc 
			  FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id 
		      LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id";
	if(!empty($where)){
		$query .=" WHERE a.bi_projcode IN (".$where.") AND a.bi_status = 1";
	}else{
		$query .=" WHERE a.bi_status = 1";
	}
	$query .= "ORDER BY b.b_uname desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_search_soaProj($limit, $offset, $keyword, $where){
	$queryJoin = "SELECT * FROM ZHOA_PROJECTS";
	if ($keyword != "") {
		if(!empty($where)){
			$queryJoin .=" WHERE SWENR IN (".$where.") AND";
		}else{
			$queryJoin .=" WHERE ";
		}
		if($keyword != ""){
			$queryJoin .= " LOWER(XWETEXT) LIKE '%".strtolower($keyword)."%' ";
			$queryJoin .= " OR LOWER(BUTXT) LIKE '%".strtolower($keyword)."%' ";
		}	
	}
	$queryJoin .=" ORDER BY BUTXT asc, XWETEXT asc
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($queryJoin);
	return $query->result_array();
}

function get_search_soaProj_rows($keyword, $where){
	$queryJoin = "SELECT * FROM ZHOA_PROJECTS";
	if ($keyword != "") {
		if(!empty($where)){
			$queryJoin .=" WHERE SWENR IN (".$where.") AND";
		}else{
			$queryJoin .=" WHERE ";
		}
		if($keyword != ""){
			$queryJoin .= " LOWER(XWETEXT) LIKE '%".strtolower($keyword)."%' ";
			$queryJoin .= " OR LOWER(BUTXT) LIKE '%".strtolower($keyword)."%' ";
		}	
	}
	$queryJoin .=" ORDER BY BUTXT asc, XWETEXT asc";
	$query = $this->db->query($queryJoin);
	return $query->num_rows;
}

function get_projectlist($limit,$offset,$where) {
	$query = "SELECT * FROM ZHOA_PROJECTS";
	if(!empty($where)){
		$query .=" WHERE SWENR IN (".$where.")";
	}
	$query .= " ORDER BY BUTXT asc, XWETEXT asc
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_projectlist_rows($where) {
	$query = "SELECT * FROM ZHOA_PROJECTS";
	if(!empty($where)){
		$query .=" WHERE SWENR IN (".$where.")";
	}

	$query .= " ORDER BY BUTXT desc, XWETEXT desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_projects_rows() {
	$this->db->distinct();
	$this->db->select('a.rb_grp_id, a.rb_name, b.a_uname, a.rb_update');
	$this->db->from('ZHOA_role_business as a');
	$this->db->join('ZHOA_admlogin as b','a.rb_updby = b.a_id','left');
	$this->db->order_by('a.rb_name','ASC');
	$query = $this->db->get();
	return $query->num_rows;
}

function get_projects($limit,$offset) {
	$query = "SELECT DISTINCT a.rb_grp_id, a.rb_name, b.a_uname, a.rb_update
			  FROM ZHOA_role_business as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.rb_updby = b.a_id
		      ORDER BY a.rb_name ASC
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_usr_by_roles($id) {
	$query = "SELECT b.a_uname, a.ai_lname, a.ai_fname, a.ai_proj, a.ai_updby, a.ai_update, a.ai_id, c.a_uname as uname2, b.a_id 
			  FROM ZHOA_adminfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.ai_id = b.a_id 
		      LEFT JOIN ZHOA_admlogin as c ON a.ai_updby = c.a_id 
		      WHERE b.a_role = ".$id."
		      ORDER BY b.a_uname ASC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_userlist($limit,$offset,$where) {
	$query = "SELECT b.a_uname, a.ai_lname, a.ai_fname, a.ai_proj, a.ai_updby, a.ai_update, a.ai_id, c.a_uname as uname2, b.a_id 
			  FROM ZHOA_adminfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.ai_id = b.a_id 
		      LEFT JOIN ZHOA_admlogin as c ON a.ai_updby = c.a_id 
		      WHERE a.ai_id <> ".$where."
		      ORDER BY b.a_uname ASC
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_userlist_rows($where) {
	$this->db->select('b.a_uname, a.ai_lname, a.ai_fname, c.PROJ_AREA, a.ai_proj, a.ai_updby, a.ai_update, a.ai_id, b.a_id');
	$this->db->from('ZHOA_adminfo as a');
	$this->db->join('ZHOA_admlogin as b','a.ai_id = b.a_id','left');
	$this->db->join('ZHOA_PROJ_AREA as c','a.ai_projarea = c.PROJ_ID','left');
	$this->db->where('a.ai_id <>',$where);
	$this->db->order_by('b.a_uname','desc');
	$query = $this->db->get();
	return $query->num_rows;
}


function get_roles($limit,$offset,$where) {
	$query = "SELECT *
			  FROM ZHOA_usraccess as a 
		      LEFT JOIN ZHOA_adminfo as b ON a.ua_updby = b.ai_id";
	if($where != 1){
		$query .= " WHERE a.ua_id <> 1";
	}
	$query .= " ORDER BY a.ua_rname asc
			  OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_roles_rows($where) {
	$query = "SELECT *
			  FROM ZHOA_usraccess as a 
		      LEFT JOIN ZHOA_adminfo as b ON a.ua_updby = b.ai_id";
	if($where != 1){
		$query .= " WHERE a.ua_id <> 1";
	}
	$query .= " ORDER BY a.ua_rname asc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_roles_where($where) {
	$query = "SELECT *
			  FROM ZHOA_usraccess as a 
			  LEFT JOIN ZHOA_adminfo as b ON a.ua_updby = b.ai_id
			  WHERE a.ua_id = ".$where."
			  ORDER BY a.ua_rname ASC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_usraccess_where($where) {
	$query = "SELECT distinct A.ar_pcode, D.BUKRS, D.BUTXT, D.SWENR, D.XWETEXT 
			  FROM ZHOA_assign_role as A
			  LEFT JOIN ZHOA_admlogin as B ON A.ar_usrid = B.a_id
              LEFT JOIN ZHOA_adminfo as C ON A.ar_usrid = C.ai_id
              LEFT JOIN ZHOA_SU as D ON A.ar_pcode = D.SWENR
              where A.ar_usrid = ".$where."
              ORDER BY D.XWETEXT";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_unassignGroup($where) {
	$query = "SELECT DISTINCT x.rb_grp_id, x.rb_name 
			  From ZHOA_role_business as x
			 EXCEPT
			 SELECT DISTINCT  a.ar_grpid, b.rb_name 
			 FROM ZHOA_assign_role as a
			 left join ZHOA_role_business as b on a.ar_grpid = b.rb_grp_id
			 WHERE a.ar_usrid = ".$where."";
	$query = $this->db->query($query);
	return $query->result_array(); 
}

function get_distinct_grpCo_where($where) {
	$query = "SELECT distinct C.a_uname, C.a_email, D.ai_lname, D.ai_fname,
			  D.ai_role, D.ai_spark, A.ur_permission, B.ar_ccode
			  FROM ZHOA_usrole as A
			  LEFT JOIN ZHOA_assign_role as B on B.ar_usrid = A.ur_usrid
			  LEFT JOIN ZHOA_admlogin as C ON A.ur_usrid = C.a_id
              LEFT JOIN ZHOA_adminfo as D ON A.ur_usrid = D.ai_id
			  WHERE A.ur_usrid = ".$where."";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_bsGrp_where($where) {
	$query = "SELECT DISTINCT A.ar_usrid, A.ar_added_by, A.ar_grpid, A.ar_added_date, B.rb_name, C.a_uname
			  FROM ZHOA_assign_role AS A
			  LEFT JOIN ZHOA_role_business AS B ON A.ar_grpid= B.rb_grp_id
			  LEFT JOIN ZHOA_admlogin AS C ON A.ar_added_by=C.a_id
		   	  WHERE A.ar_usrid = ".$where."";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_search_users($limit, $offset, $company, $project, $keyword){
	$queryJoin = "SELECT b.a_uname, a.ai_lname, a.ai_fname, a.ai_proj, a.ai_updby, a.ai_update, a.ai_id, c.a_uname as uname2 
			  FROM ZHOA_adminfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.ai_id = b.a_id 
		      LEFT JOIN ZHOA_admlogin as c ON a.ai_updby = c.a_id";
	if ($keyword != "") {
		$queryJoin .= " WHERE";
		$queryJoin .= " ";
			$queryJoin .= " (b.a_uname LIKE '%".$keyword."%'";
			$queryJoin .= " OR a.ai_lname LIKE '%".$keyword."%'";
			$queryJoin .= " OR a.ai_fname LIKE '%".$keyword."%'";
			$queryJoin .= " OR a.ai_proj LIKE '%".$keyword."%')";
	} else { $queryJoin .= " "; }
	$queryJoin .="ORDER BY b.a_uname desc
				  OFFSET ".$offset." ROWS
		 		  FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($queryJoin);
	return $query->result_array();
}

function get_search_users_rows($company, $project, $keyword){
	$queryJoin = "SELECT b.a_uname, a.ai_lname, a.ai_fname, c.PROJ_AREA, a.ai_proj, a.ai_updby, a.ai_update, a.ai_id 
				  FROM ZHOA_adminfo as a 
			      LEFT JOIN ZHOA_admlogin as b ON a.ai_id = b.a_id 
			      LEFT JOIN ZHOA_PROJ_AREA as c ON a.ai_projarea = c.PROJ_ID";
	if ($company != "" || $project != "" || $keyword != "") {
		$queryJoin .= " WHERE";
		if($company != "") {
			$queryJoin .= "  LOWER(c.PROJ_ID) LIKE '%".strtolower($company)."%' ";
		}
		if ($project != "") {
			if ($company != "") {
				$queryJoin .= " AND ";
			} else { $queryJoin .= " "; }
			$queryJoin .= " a.ai_proj LIKE '%".$project."%' ";
		}
		if ($keyword != "") {
			if ($company != "" || $project != "") {
				$queryJoin .= " AND ";
			} else { $queryJoin .= " "; } 
			$queryJoin .= " (b.a_uname LIKE '%".$keyword."%'";
			$queryJoin .= " OR a.ai_lname LIKE '%".$keyword."%'";
			$queryJoin .= " OR a.ai_fname LIKE '%".$keyword."%'";
			$queryJoin .= " OR LOWER(c.PROJ_AREA) LIKE '%".strtolower($keyword)."%'";
			$queryJoin .= " OR a.ai_proj LIKE '%".$keyword."%')";
		}
	} else { $queryJoin .= " "; }
		$queryJoin .="ORDER BY b.a_uname desc";
		$query = $this->db->query($queryJoin);
		return $query->num_rows();
}

function get_search_owners($where, $limit, $offset, $company, $project, $keyword){
	$queryJoin = "SELECT b.b_uname, a.bi_lname, a.bi_fname, d.PROJ_AREA, a.bi_project, a.bi_blklot, c.u_desc, a.bi_id
				  FROM ZHOA_byrinfo as a
				  LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id
				  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id 
				  LEFT JOIN ZHOA_PROJ_AREA as d ON a.bi_company = d.PROJ_ID
				  WHERE ";
	if ($company != "" || $project != "" || $keyword != "") {
		if(!empty($where)){
			$queryJoin .= "a.bi_projcode IN (".$where.") AND ";
		}
		if($company != "") {
			$queryJoin .= "  a.bi_company = '".$company."' ";
		}
		if ($project != "") {
			if ($company != "" ) {
				$queryJoin .= " AND ";
			} else { $queryJoin .= " "; }
			$queryJoin .= " a.bi_project LIKE '%".$project."%' ";
		}
		if($keyword != ""){
			if ($company != "" || $project != "") {
				$queryJoin .= " AND ";
			}else { $queryJoin .= " "; }
			$queryJoin .= " (b.b_uname LIKE '%".$keyword."%' ";
			$queryJoin .= " OR a.bi_lname LIKE '%".$keyword."%' ";
			$queryJoin .= " OR a.bi_fname LIKE '%".$keyword."%' ";
			$queryJoin .= " OR LOWER(d.PROJ_AREA) LIKE '%".strtolower($keyword)."%' ";
			$queryJoin .= " OR a.bi_project LIKE '%".$keyword."%' ";
			$queryJoin .= " OR a.bi_blklot LIKE '%".$keyword."%' ";
	 		$queryJoin .= " OR c.u_desc LIKE '%".$keyword."%') ";
		}
		$queryJoin .= " AND a.bi_status = '1' ";	
	} else { $queryJoin .= " WHERE ".$where." a.bi_status = '1' "; }
	$queryJoin .=" ORDER BY b.b_uname desc
				   OFFSET ".$offset." ROWS
	 	  		   FETCH NEXT ".$limit." ROWS ONLY";
	$query = $this->db->query($queryJoin);
	return $query->result_array();
}

function get_search_owners_rows($where, $company, $project, $keyword){
	$queryJoin = "SELECT b.b_uname, a.bi_lname, a.bi_fname, d.PROJ_AREA, a.bi_project, a.bi_blklot, c.u_desc, a.bi_id
				  FROM ZHOA_byrinfo as a
				  LEFT JOIN ZHOA_byrlogin as b ON a.bi_owner_id = b.b_id
				  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id 
				  LEFT JOIN ZHOA_PROJ_AREA as d ON a.bi_company = d.PROJ_ID
				  WHERE ";
	if ($company != "" || $project != "" || $keyword != "") {
		if(!empty($where)){
			$queryJoin .= "a.bi_projcode IN (".$where.") AND ";
		}
		if($company != "") {
			$queryJoin .= "  a.bi_company LIKE '%".$company."%' ";
		}
		if ($project != "") {
			if ($company != "" ) {
				$queryJoin .= " AND ";
			} else { $queryJoin .= " "; }
			$queryJoin .= " a.bi_project LIKE '%".$project."%' ";
		}
		if($keyword != ""){
			if ($company != "" || $project != "") {
				$queryJoin .= " AND";
			}else { $queryJoin .= " "; }
			$queryJoin .= " (b.b_uname LIKE '%".$keyword."%' ";
			$queryJoin .= " OR a.bi_lname LIKE '%".$keyword."%' ";
			$queryJoin .= " OR a.bi_fname LIKE '%".$keyword."%' ";
			$queryJoin .= " OR LOWER(d.PROJ_AREA) LIKE '%".strtolower($keyword)."%' ";
			$queryJoin .= " OR a.bi_project LIKE '%".$keyword."%' ";
			$queryJoin .= " OR a.bi_blklot LIKE '%".$keyword."%' ";
	 		$queryJoin .= " OR c.u_desc LIKE '%".$keyword."%')";
		}
		$queryJoin .= " AND a.bi_status = '1' ";	
	} else { $queryJoin .= " WHERE ".$where." a.bi_status = '1' "; }
	$queryJoin .=" ORDER BY b.b_uname desc";
	$query = $this->db->query($queryJoin);
	return $query->num_rows();
}

function get_new_reg($limit,$offset,$where) {

	$query = "SELECT a.bi_id, a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot
			  FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '0'";		
	} else {
		$query .= " WHERE a.bi_status = '0'";
	}
	$query .= " AND a.bi_email_verif = '1'
			  AND a.bi_regdate > '".date("Y-m-d")."'
		      ORDER BY a.bi_regdate ASC
		      OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($query);
	return $query->result_array();
	
}

function get_new_reg_rows($where) {
	$query = "SELECT a.bi_lname, a.bi_fname, a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot
			  FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '0'";		
	} else {
		$query .= " WHERE a.bi_status = '0'";
	}
	$query .= " AND a.bi_email_verif = '1'
			  AND a.bi_regdate > '".date("Y-m-d")."'
		      ORDER BY a.bi_regdate ASC";

	$query = $this->db->query($query);
	$get_query = $query->result_array();
	
	return ($query->num_rows);
}

function get_out_reg_rows($where){

	$query = "SELECT a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot 
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '0'";		
	} else {
		$query .= " WHERE a.bi_status = '0'";
	}
	$query .= "AND a.bi_email_verif = 1
			  AND a.bi_regdate < '".date("Y-m-d")."' 
			  ORDER BY a.bi_regdate desc";
	$query = $this->db->query($query);	
	return $query->num_rows;
}

function get_out_reg($limit,$offset,$where){
	$query = "SELECT a.bi_id, a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot 
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '0'";		
	} else {
		$query .= " WHERE a.bi_status = '0'";
	}
	$query .= "AND a.bi_email_verif = 1
			  AND a.bi_regdate < '".date("Y-m-d")."' 
			  ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);	
	return $query->result_array();
}

function get_con_reg_rows($where){
	$query = "SELECT a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot 
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '1'";		
	} else {
		$query .= " WHERE a.bi_status = '1'";
	}
	$query .= " AND a.bi_email_verif = 1
				ORDER BY a.bi_regdate desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_con_reg($limit,$offset,$where){
	$query = "SELECT b.a_uname, a.bi_id, a.bi_regdate, a.bi_process_by, a.bi_process_date, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot  
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '1'";		
	} else {
		$query .= " WHERE a.bi_status = '1'";
	}
	$query .= "AND a.bi_email_verif = 1
		      ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";	
	
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_rej_reg_rows($where){
	$query = "SELECT a.bi_process_by, a.bi_process_date, a.bi_id, a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot, b.a_uname
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '2'";		
	} else {
		$query .= " WHERE a.bi_status = '2'";
	}
	$query .= " ORDER BY a.bi_regdate desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_rej_reg($limit,$offset,$where){
	$query = "SELECT a.bi_process_by, a.bi_process_date, a.bi_id, a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot, b.a_uname
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '2'";		
	} else {
		$query .= " WHERE a.bi_status = '2'";
	}
	$query .= " ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_wait_reg($limit,$offset,$where) {

	$query = "SELECT a.bi_id, a.bi_process_by, a.bi_process_date, a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot  
			  FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '0'";		
	} else {
		$query .= " WHERE a.bi_status = '0'";
	}
	$query .= "AND a.bi_email_verif = '0'
			  ORDER BY a.bi_regdate desc
		      OFFSET ".$offset." ROWS 
 			  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($query);
	return $query->result_array();
	}
	// AND a.bi_regdate = '".date("Y-m-d")."'

function get_wait_reg_rows($where) {
	$query = "SELECT a.bi_regdate, a.bi_unit, a.bi_temp_concern, a.bi_status, a.bi_email_verif, c.PROJ_AREA, a.bi_project, a.bi_blklot  
			  FROM ZHOA_byrinfo as a 
		      LEFT JOIN ZHOA_admlogin as b ON a.bi_process_by = b.a_id 
			  LEFT JOIN ZHOA_PROJ_AREA as c ON a.bi_company = c.PROJ_ID";
	if(!empty($where)){	
		$query .= " WHERE a.bi_projcode IN (".$where.") AND a.bi_status = '0'";		
	} else {
		$query .= " WHERE a.bi_status = '0'";
	}
	$query .= "AND a.bi_email_verif = '0'
			   ORDER BY a.bi_regdate desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_buyer_by_id($id){
	$query = "SELECT * 
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_PROJ_AREA as b ON a.bi_company = b.PROJ_ID 
		      LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id 
		      LEFT JOIN ZHOA_BUYER_PROF as e ON a.bi_cust_no = e.KUNNR 
			  LEFT JOIN ZHOA_byrlogin as f ON a.bi_owner_id = f.b_id
			  WHERE a.bi_id = ".$id." AND e.ACTIVE = 1";

	$query = $this->db->query($query);
	return $query->result_array();
}

function get_buyer_by_id2($id){
	$query = "SELECT * 
			  FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_PROJ_AREA as b ON a.bi_company = b.PROJ_ID 
		      LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id 
			  LEFT JOIN ZHOA_byrlogin as f ON a.bi_owner_id = f.b_id
			  WHERE a.bi_id = ".$id;

	$query = $this->db->query($query);
	return $query->result_array();
}

function get_byr_by_id($id){
	$query = "SELECT * 
			  FROM ZHOA_byrlogin as f 
			  LEFT JOIN  ZHOA_byrinfo as a ON a.bi_owner_id = f.b_id
		      LEFT JOIN ZHOA_PROJ_AREA as b ON a.bi_company = b.PROJ_ID 
		      LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id 
		      LEFT JOIN ZHOA_BUYER_PROF as e ON a.bi_cust_no = e.KUNNR 
			  WHERE f.b_id = ".$id." AND e.ACTIVE = 1";

	$query = $this->db->query($query);
	return $query->result_array();
}

function get_ticket_info($id) {
	$query = "SELECT * FROM ZHOA_tickets as a
			left join ZHOA_BUYER_PROF as b on a.t_so = b.VBELN
			left join Zhoa_guest_info as c on a.t_guest_id = c.gi_id 
			left join Zhoa_su as d on b.SWENR = d.SWENR and b.SMENR = d.SMENR
			LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id
			LEFT JOIN ZHOA_byrlogin as g ON f.bi_owner_id = g.b_id
			where a.t_id = ".$id;
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_sap_info($id, $blklot){
	$this->db->select('*');
	$this->db->from('ZHOA_BUYER_PROF as a');
	$this->db->join('ZHOA_PROJ_AREA as b','a.PROJ_ID = b.PROJ_ID','left');
	$this->db->where('KUNNR',$id);
	$this->db->where('REFNO',$blklot);
	$this->db->where('a.ACTIVE', 1);
	$query = $this->db->get();
	return $query->result_array();
}

function updateStatus($id,$status,$reason,$process_by,$process_date){
		$data = array('bi_status' => $status,
					  'bi_remarks' => $reason,
					  'bi_process_by' => $process_by,
					  'bi_process_date' => $process_date);
		
		$this->db->where('bi_id',$id);
		$query = $this->db->update('ZHOA_byrinfo',$data);
		return $query;
}

function get_search_reg($where, $limit, $offset, $sign_up_date_start, $sign_up_date_end, $process_date_start, $process_date_end, $company, $project, $statusSearch, $processBy, $key){
	
$queryJoin = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_PROJECTS as b ON a.bi_company = b.PROJ_ID AND a.bi_projcode = b.SWENR
  			  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id
			  LEFT JOIN ZHOA_byrlogin as d ON a.bi_owner_id = d.b_id
			  LEFT JOIN ZHOA_admlogin as f ON a.bi_process_by = f.a_id
			  WHERE ";
	
   $counter = 0;
	
	if($sign_up_date_start != "" || $sign_up_date_end != "" || $process_date_start != "" || $process_date_end != "" || $company != "" || $project != "" || $statusSearch != "" || $processBy != "" || $key != ""){
		if(!empty($where)){
			$queryJoin .= " a.bi_projcode IN (".$where.") AND ";
		} else {
			$queryJoin .= "";
		}
	}

	if($sign_up_date_start == "" && $sign_up_date_end == "" && $process_date_start == "" && $process_date_end == "" && $company == "" && $project == "" && $statusSearch == "" && $processBy == "" && $key == ""){
		if($where != "/* */"){
			$queryJoin .= " WHERE ".$where." ";
		}
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$counter++;
	}
	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$counter++;
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
		$counter++;
	}
	if($process_date_start != "" && $process_date_end != ""){
		$counter++; //1
	}
	if($company != ""){
		$counter++;
	}
	if($project != ""){
		$counter++;
	}
	
	if($statusSearch != ""){
		$counter++;
	}
	
	if($processBy != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$queryJoin .= " a.bi_regdate = '".$sign_up_date_start."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$queryJoin .= " a.bi_regdate <= '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
  		$queryJoin .= " a.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($process_date_start != "" && $process_date_end != ""){
		$queryJoin .= "  a.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
		
 		if($counter > 1){ 
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";  
 		}
	}
	
	if($company != ""){
		$queryJoin .= "  LOWER(b.PROJ_ID) LIKE '%".strtolower($company)."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  a.bi_project LIKE '%".$project."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($statusSearch != ""){
		
		switch ($statusSearch){
			case 0:
				//NEW
				$queryJoin .= "  a.bi_status = '0'";
				$queryJoin .= "  AND a.bi_regdate BETWEEN '".date("Y-m-d")." 00:00:00.000' AND '".date("Y-m-d")." 23:59:59.997'  AND a.bi_email_verif = 1";
				break;
			case 1:
				//OUTSTANDING
				$queryJoin .= "  a.bi_status = '0'";
				$queryJoin .= "  AND a.bi_regdate < '".date("Y-m-d")."' AND a.bi_email_verif = 1";
				break;
			case 2:
				//CONFIRMED
				$queryJoin .= "  a.bi_status = '1'";
				break;
			case 3:
				//REJECTED
				$queryJoin .= "  a.bi_status = '2'";
				break;
			case 4: 
				//WAITING
				$queryJoin .= "  a.bi_status = '0'";
				$queryJoin .= "  AND a.bi_email_verif = 0";
		}

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($processBy != ""){
		$queryJoin .= "  f.a_uname LIKE '%".$processBy."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.bi_regdate LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_blklot LIKE '%".$key."%'";
		if(is_numeric($key)){
			$queryJoin .= "  OR a.bi_cust_no = ".$key;
		}
 		$queryJoin .= "  OR a.bi_status LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_temp_concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_process_date LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_project LIKE '%".$key."%'";
		$queryJoin .= "  OR LOWER(b.ADRZUS) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR a.bi_fname LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_lname LIKE '%".$key."%'";
		$queryJoin .= "  OR LOWER(b.XWETEXT) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR LOWER(b.BUTXT) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR c.u_desc LIKE '%".$key."%'";
		$queryJoin .= "  OR d.b_uname LIKE '%".$key."%'";
 		$queryJoin .= "  OR f.a_uname LIKE '%".$key."%')";

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	$queryJoin .=" ORDER BY a.bi_regdate desc
			      OFFSET ".$offset." ROWS
 			  	  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->result_array();
}

function get_search_reg_rows($where,$sign_up_date_start, $sign_up_date_end, $process_date_start, $process_date_end, $company, $project, $statusSearch, $processBy, $key){
	
	$queryJoin = "SELECT * FROM ZHOA_byrinfo as a
		      LEFT JOIN ZHOA_PROJECTS as b ON a.bi_company = b.PROJ_ID AND a.bi_projcode = b.SWENR
  			  LEFT JOIN ZHOA_unit as c ON a.bi_unit = c.u_id
			  LEFT JOIN ZHOA_byrlogin as d ON a.bi_owner_id = d.b_id
			  LEFT JOIN ZHOA_admlogin as f ON a.bi_process_by = f.a_id
			  WHERE ";
	
   $counter = 0;
	
	if($sign_up_date_start != "" || $sign_up_date_end != "" || $process_date_start != "" || $process_date_end != "" || $company != "" || $project != "" || $statusSearch != "" || $processBy != "" || $key != ""){
		if(!empty($where)){
			$queryJoin .= " a.bi_projcode IN (".$where.") AND ";
		} else {
			$queryJoin .= "";
		}
	}

	if($sign_up_date_start == "" && $sign_up_date_end == "" && $process_date_start == "" && $process_date_end == "" && $company == "" && $project == "" && $statusSearch == "" && $processBy == "" && $key == ""){
		if($where != "/* */"){
			$queryJoin .= " WHERE ".$where." ";
		}
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$counter++;
	}
	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$counter++;
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
		$counter++;
	}
	if($process_date_start != "" && $process_date_end != ""){
		$counter++; //1
	}
	if($company != ""){
		$counter++;
	}
	if($project != ""){
		$counter++;
	}
	
	if($statusSearch != ""){
		$counter++;
	}
	
	if($processBy != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($sign_up_date_start != "" && $sign_up_date_end == ""){
		$queryJoin .= " a.bi_regdate = '".$sign_up_date_start."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($sign_up_date_start == "" && $sign_up_date_end != ""){
		$queryJoin .= " a.bi_regdate <= '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($sign_up_date_start != "" && $sign_up_date_end != ""){
  		$queryJoin .= " a.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($process_date_start != "" && $process_date_end != ""){
		$queryJoin .= "  a.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
		
 		if($counter > 1){
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";
 		}
	}
	
	if($company != ""){
		$queryJoin .= "  LOWER(b.PROJ_ID) LIKE '%".strtolower($company)."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  a.bi_project LIKE '%".$project."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($statusSearch != ""){
		
		switch ($statusSearch){
			case 0:
				//NEW
				$queryJoin .= "  a.bi_status = '0'";
				$queryJoin .= "  AND a.bi_regdate BETWEEN '".date("Y-m-d")." 00:00:00.000' AND '".date("Y-m-d")." 23:59:59.997'  AND a.bi_email_verif = 1";
				break;
			case 1:
				//OUTSTANDING
				$queryJoin .= "  a.bi_status = '0'";
				$queryJoin .= "  AND a.bi_regdate < '".date("Y-m-d")."' AND a.bi_email_verif = 1";
				break;
			case 2:
				//CONFIRMED
				$queryJoin .= "  a.bi_status = '1'";
				break;
			case 3:
				//REJECTED
				$queryJoin .= "  a.bi_status = '2'";
				break;
			case 4: 
				//WAITING
				$queryJoin .= "  a.bi_status = '0'";
				$queryJoin .= "  AND a.bi_email_verif = 0";
		}

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($processBy != ""){
		$queryJoin .= "  f.a_uname LIKE '%".$processBy."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.bi_regdate LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_blklot LIKE '%".$key."%'";
		if(is_numeric($key)){
			$queryJoin .= "  OR a.bi_cust_no = ".$key;
		}
 		$queryJoin .= "  OR a.bi_status LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_temp_concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_process_date LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_project LIKE '%".$key."%'";
		$queryJoin .= "  OR LOWER(b.ADRZUS) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR a.bi_fname LIKE '%".$key."%'";
		$queryJoin .= "  OR a.bi_lname LIKE '%".$key."%'";
		$queryJoin .= "  OR LOWER(b.XWETEXT) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR LOWER(b.BUTXT) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR c.u_desc LIKE '%".$key."%'";
		$queryJoin .= "  OR d.b_uname LIKE '%".$key."%'";
 		$queryJoin .= "  OR f.a_uname LIKE '%".$key."%')";

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}

	
	$queryJoin .=" ORDER BY a.bi_regdate desc";

	$query = $this->db->query($queryJoin);
	return $query->num_rows();
}

function get_projects_by_cust_no($cust_no){
	$this->db->where('KUNNR',$cust_no);
	$query = $this->db->get('ZHOA_BUYER_PROF');
	return $query;
}

function get_new_concerns($limit,$offset,$where){
	$query = "SELECT * FROM ZHOA_tickets as b
			  LEFT JOIN Zhoa_guest_info as f on b.t_guest_id = f.gi_id 
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR";
  	if(!empty($where)){	
		$query .= " WHERE c.SWENR IN (".$where.") AND b.t_status = '1'";		
	} else {
		$query .= " WHERE b.t_status = '1'";
	}
	$query .= " AND b.t_date_created >= '".date('Y-m-d').' 00:00:00.000'."'
			  AND b.t_date_created <= '".date('Y-m-d').' 23:59:59.997'."'
			  ORDER BY b.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_open_concerns($limit,$offset,$where){
	$query = "SELECT * FROM ZHOA_tickets as b
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR";
  	if(!empty($where)){	
		$query .= " WHERE c.SWENR IN (".$where.") AND b.t_status = '1'";		
	} else {
		$query .= " WHERE b.t_status = '1'";
	}
	$query .= " AND b.t_date_created < '".date('Y-m-d').' 00:00:00.000'."'
			  ORDER BY b.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_closed_concerns($limit,$offset,$where){
	$query = "SELECT * FROM ZHOA_tickets as b
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR";
  	if(!empty($where)){	
		$query .= " WHERE c.SWENR IN (".$where.") AND b.t_status = '2' OR b.t_status = 3";		
	} else {
		$query .= " WHERE b.t_status = '2' OR b.t_status = 3";
	}
	$query .= " ORDER BY b.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_new_concerns_rows($where) {
	$query = "SELECT * FROM ZHOA_tickets as b
			  LEFT JOIN Zhoa_guest_info as f on b.t_guest_id = f.gi_id 
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR";
  	if(!empty($where)){	
		$query .= " WHERE c.SWENR IN (".$where.") AND b.t_status = '1'";		
	} else {
		$query .= " WHERE b.t_status = '1'";
	}
	$query .= "AND b.t_date_created >= '".date('Y-m-d').' 00:00:00.000'."'
			  AND b.t_date_created <= '".date('Y-m-d').' 23:59:59.997'."'
			  ORDER BY b.t_date_created desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_open_concerns_rows($where) {
	$query = "SELECT * FROM ZHOA_tickets as b
			  LEFT JOIN Zhoa_guest_info as f on b.t_guest_id = f.gi_id 
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR";
  	if(!empty($where)){	
		$query .= " WHERE c.SWENR IN (".$where.") AND b.t_status = '1'";		
	} else {
		$query .= " WHERE b.t_status = '1'";
	}
	$query .= " AND b.t_date_created < '".date('Y-m-d').' 00:00:00.000'."'
			  ORDER BY b.t_date_created desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_closed_concerns_rows($where) {
	$query = "SELECT * FROM ZHOA_tickets as b
			  LEFT JOIN Zhoa_guest_info as f on b.t_guest_id = f.gi_id 
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR";
  	if(!empty($where)){	
		$query .= " WHERE c.SWENR IN (".$where.") AND b.t_status = '2' OR b.t_status = 3";		
	} else {
		$query .= " WHERE b.t_status = '2' OR b.t_status = 3";
	}
	$query .= " ORDER BY b.t_date_created desc";
	$query = $this->db->query($query);
	return $query->num_rows;
}

function get_search_concern($where, $limit, $offset, $date_posted_from, $date_posted_to, $status, $project_area, $project, $key){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as b
			  LEFT JOIN Zhoa_guest_info as f on b.t_guest_id = f.gi_id 
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
			  LEFT JOIN ZHOA_byrlogin as g ON a.bi_owner_id = g.b_id
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR
  			  WHERE ";
if(!empty($where)){	
		$queryJoin .= " c.SWENR IN (".$where.") AND ";		
	}  	
//			  LEFT JOIN ZHOA_BUYER_PROF as g ON b.bi_cust_no = g.KUNNR";
	
   $counter = 0;

	if($date_posted_from != "" && $date_posted_to == ""){
		$counter++;
	}
	if($date_posted_from == "" && $date_posted_to != ""){
		$counter++;
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$counter++;
	}
	if($status != ""){
		$counter++;
	}
	if($project_area != ""){
		$counter++;
	}
	
	if($project != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_posted_from != "" && $date_posted_to == ""){
		$queryJoin .= "  b.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_from." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_posted_from == "" && $date_posted_to != ""){
		$queryJoin .= "  b.t_date_created BETWEEN '".$date_posted_to." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$queryJoin .= "  b.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
 		if($counter > 1){
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";
 		}
	}

	if($status != ""){
		
		switch ($status){
			 case 0:
				//New
				$queryJoin .= "  b.t_status = '1'";
				$queryJoin .= "  AND b.t_date_created between '".date("Y-m-d")." 00:00:00.000' AND '".date("Y-m-d")." 23:59:59.997'";
				break;
			case 1:
				//Open
				$queryJoin .= "  b.t_status = '1'";
				$queryJoin .= "  AND b.t_date_created < '".date("Y-m-d")."'";
				break;
			case 2:
				//Closed
				$queryJoin .= "  b.t_status = '0'";
				break;
			case 3:
				//Unposted
				$queryJoin .= "  b.t_post_status = '1'";
				break;
			case 4:
				//Posted
				$queryJoin .= "  b.t_post_status = '3'";
				break;
			case 5:
				//Partial
				$queryJoin .= "  b.t_post_status = '2'";
				break;
				    		
		}
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project_area != ""){
		$queryJoin .= "  d.PROJ_ID = '".$project_area."'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  LOWER(c.XWETEXT) like '%".strtolower($project)."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
 		$queryJoin .= "  ( a.bi_lname LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_fname LIKE '%".$key."%'";
		$queryJoin .= "  OR b.t_id LIKE '%".$key."%'";
		$queryJoin .= "  OR b.t_date_created LIKE '%".$key."%'";
		$queryJoin .= "  OR LOWER(d.PROJ_AREA) LIKE '%".strtolower($key)."%'";
 		$queryJoin .= "  OR b.t_concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR b.t_status LIKE '%".$key."%'";
 		$queryJoin .= "  OR b.t_reply_by LIKE '%".$key."%'";
		$queryJoin .= "  OR b.t_blklot LIKE '%".$key."%'";
 		$queryJoin .= "  OR LOWER(c.NAME1) LIKE '%".strtolower($key)."%'";
 		$queryJoin .= "  OR LOWER(c.XWETEXT) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR LOWER(e.XMBEZ) LIKE '%".strtolower($key)."%'";
 		$queryJoin .= "  OR g.b_uname LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	
	$queryJoin .=" ORDER BY b.t_date_created desc
			      OFFSET ".$offset." ROWS
 			  	  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->result_array();
}

function get_search_concern_rows($where, $date_posted_from, $date_posted_to, $status, $project_area, $project, $key){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as b
			  LEFT JOIN Zhoa_guest_info as f on b.t_guest_id = f.gi_id 
		      LEFT JOIN ZHOA_BUYER_PROF as c ON b.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as a ON b.t_owner_id = a.bi_owner_id 
			  LEFT JOIN ZHOA_byrlogin as g ON a.bi_owner_id = g.b_id
  			  LEFT JOIN ZHOA_su as e ON c.SWENR = e.SWENR and c.SMENR = e.SMENR
  			  WHERE ";
if(!empty($where)){	
		$queryJoin .= " c.SWENR IN (".$where.") AND ";		
	} 
//			  LEFT JOIN ZHOA_BUYER_PROF as g ON b.bi_cust_no = g.KUNNR";
	
   $counter = 0;

	if($date_posted_from != "" && $date_posted_to == ""){
		$counter++;
	}
	if($date_posted_from == "" && $date_posted_to != ""){
		$counter++;
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$counter++;
	}
	if($status != ""){
		$counter++;
	}
	if($project_area != ""){
		$counter++;
	}
	
	if($project != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_posted_from != "" && $date_posted_to == ""){
		$queryJoin .= "  b.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_from." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_posted_from == "" && $date_posted_to != ""){
		$queryJoin .= "  b.t_date_created BETWEEN '".$date_posted_to." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_posted_from != "" && $date_posted_to != ""){
		$queryJoin .= "  b.t_date_created BETWEEN '".$date_posted_from." 00:00:00.000' AND '".$date_posted_to." 23:59:59.997'";
		
 		if($counter > 1){
 			$counter--;
 			$queryJoin .= " AND";
 		}else{
 			$queryJoin .= " ";
 		}
	}

	if($status != ""){
		
		switch ($status){
			 case 0:
				//New
				$queryJoin .= "  b.t_status = '1'";
				$queryJoin .= "  AND b.t_date_created between '".date("Y-m-d")." 00:00:00.000' AND '".date("Y-m-d")." 23:59:59.997'";
				break;
			case 1:
				//Open
				$queryJoin .= "  b.t_status = '1'";
				$queryJoin .= "  AND b.t_date_created < '".date("Y-m-d")."'";
				break;
			case 2:
				//Closed
				$queryJoin .= "  b.t_status = '0'";
				break;
			case 3:
				//Unposted
				$queryJoin .= "  b.t_post_status = '1'";
				break;
			case 4:
				//Posted
				$queryJoin .= "  b.t_post_status = '3'";
				break;
			case 5:
				//Partial
				$queryJoin .= "  b.t_post_status = '2'";
				break;
				    		
		}
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project_area != ""){
		$queryJoin .= "  d.PROJ_ID = '".$project_area."'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($project != ""){
		$queryJoin .= "  LOWER(c.XWETEXT) like '%".strtolower($project)."%'";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= "  ( a.bi_lname LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.bi_fname LIKE '%".$key."%'";
		$queryJoin .= "  OR b.t_id LIKE '%".$key."%'";
		$queryJoin .= "  OR b.t_date_created LIKE '%".$key."%'";
		$queryJoin .= "  OR LOWER(d.PROJ_AREA) LIKE '%".strtolower($key)."%'";
 		$queryJoin .= "  OR b.t_concern LIKE '%".$key."%'";
 		$queryJoin .= "  OR b.t_status LIKE '%".$key."%'";
 		$queryJoin .= "  OR b.t_reply_by LIKE '%".$key."%'";
		$queryJoin .= "  OR b.t_blklot LIKE '%".$key."%'";
 		$queryJoin .= "  OR LOWER(c.NAME1) LIKE '%".strtolower($key)."%'";
 		$queryJoin .= "  OR LOWER(c.XWETEXT) LIKE '%".strtolower($key)."%'";
		$queryJoin .= "  OR LOWER(e.XMBEZ) LIKE '%".strtolower($key)."%'";
 		$queryJoin .= "  OR g.b_uname LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != ""){
// 		$queryJoin .= " b.bi_regdate BETWEEN '".$sign_up_date_start."' AND '".$sign_up_date_end."'";
// 	}
	
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != "" && $process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= " AND";
// 	}
	
// 	if($process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= "  b.bi_process_date BETWEEN '".$process_date_start." 00:00:00.000' AND '".$process_date_end." 23:59:59.997'";
// 	}
	
// 	if($sign_up_date_start != "" && $sign_up_date_end != "" || $process_date_start != "" && $process_date_end != ""){
// 		$queryJoin .= " AND";
// 	}
	
// 	if($company != ""){
// 		$queryJoin .= "  a.tc_brand LIKE '%".$company."%'";
// 	}
	
// // 	if($sign_up_date_start != "" && $sign_up_date_end != "" || $process_date_start != "" && $process_date_end != "" || $company != "" || $project != ""){
// // 		$queryJoin .= " AND";
// // 	}
	
// // 	if($project != ""){
// // 		$queryJoin .= "  g.XWETEXT LIKE '%".$project."%'";
// // 	}
	
	
	$queryJoin .=" ORDER BY b.t_date_created desc";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->num_rows();
}

function get_concerns($ticketID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_concerns')
				-> WHERE('c_ticket_id', $ticketID)
				-> ORDER_BY('c_date_created', 'DESC')
				;
	$query = $this -> db -> get();
	return $query -> result_array();

}

function get_single_ticket($ticketID,$where){
	$query = "SELECT * FROM ZHOA_tickets as a
			LEFT JOIN ZHOA_concerns as b ON a.t_id = b.c_ticket_id
			LEFT JOIN ZHOA_byrinfo as c ON a.t_owner_id = c.bi_owner_id
			LEFT JOIN ZHOA_request_type as d ON b.c_rtype = d.rt_id
			LEFT JOIN ZHOA_subrequest_type as e ON b.c_stype = e.st_id
			LEFT JOIN ZHOA_BUYER_PROF as f on a.t_so = f.VBELN ";
	if(!empty($where)){	
		$query .= "WHERE b.c_ticket_id = '".$ticketID."' AND f.SWENR IN (".$where.")";		
	} else {
		$query .= "WHERE b.c_ticket_id = '".$ticketID."'";
	}
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_ticket_owner($ticketID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_tickets as a')
				-> WHERE('t_id', $ticketID)
				;
	$query = $this -> db -> get();
	return $query -> result_array();

}

function get_single_concern($concernID){

	$this -> db -> SELECT()
				-> FROM('ZHOA_concern_details as a')
				// -> JOIN('ZHOA_concern_attachments as b','a.cd_id = b.ca_cd_id','left')
				-> WHERE('a.cd_concern_id',$concernID)
				-> WHERE('a.cd_is_deleted', 0)
				-> ORDER_BY('a.cd_date_created', 'DESC')
				;
	$query = $this -> db -> get();
	return $query -> result_array();

}
                        
function get_buyer_concerns($limit,$offset,$id){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE c.ACTIVE = '1'
			  AND a.t_owner_id = '".$id."'
			  ORDER BY a.t_date_created desc
		      OFFSET ".$offset." ROWS
 			  FETCH NEXT ".$limit." ROWS ONLY";
			  // WHERE a.t_status = '1'
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_buyer_concerns_rows($id){
	$query = "SELECT * FROM ZHOA_tickets as a
			  LEFT JOIN ZHOA_byrinfo as b ON a.t_owner_id = b.bi_owner_id
		      LEFT JOIN ZHOA_BUYER_PROF as c ON a.t_so = c.VBELN
			  LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
			  LEFT JOIN ZHOA_byrinfo as f ON a.t_owner_id = f.bi_owner_id 
  			  LEFT JOIN ZHOA_unit as e ON f.bi_unit = e.u_id
			  WHERE c.ACTIVE = '1'
			  AND a.t_owner_id = '".$id."'
			  ORDER BY a.t_date_created desc";
			  // WHERE a.t_status = '1'
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->num_rows();	
}

// portal

function get_search_req($limit, $offset, $date_post_start, $date_post_end, $status, $key, $userid){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as a
		      LEFT JOIN ZHOA_byrlogin as b ON a.t_owner_id = b.b_id";

   $counter = 0;
	
	if($date_post_start != "" || $date_post_end != "" || $status != "" || $key != ""){
		$queryJoin .= " WHERE a.t_owner_id = ".$userid." AND";
	}

	if($date_post_start != "" && $date_post_end == ""){
		$counter++;
	}
	if($date_post_start == "" && $date_post_end != ""){
		$counter++;
	}
	
	if($date_post_start != "" && $date_post_end != ""){
		$counter++;
	}
	
	if($status != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_post_start != "" && $date_post_end == ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_post_start." 00:00:00.000' AND '".$date_post_start." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_post_start == "" && $date_post_end != ""){
		$queryJoin .= " a.t_date_created <= '".$date_post_end." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_post_start != "" && $date_post_end != ""){
  		$queryJoin .= " a.t_date_created BETWEEN '".$date_post_start."' AND '".$date_post_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($status != ""){
		
				switch ($status){
				    	    case 1:
				    	    	//NEW
				    	    	$queryJoin .= "  a.t_status = '1'";
				    			break;
				    		case 2:
				    			//OUTSTANDING
				    			$queryJoin .= "  a.t_status = '2'";
				    			break;
				    	}

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.t_date_created LIKE '%".$key."%'";
		$queryJoin .= "  OR a.t_blklot LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_status LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	$queryJoin .=" ORDER BY a.t_date_created desc
			      OFFSET ".$offset." ROWS
 			  	  FETCH NEXT ".$limit." ROWS ONLY";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->result_array();
}

function get_search_req_rows($date_post_start, $date_post_end, $status, $key, $userid){
	
$queryJoin = "SELECT * FROM ZHOA_tickets as a
		      LEFT JOIN ZHOA_byrlogin as b ON a.t_owner_id = b.b_id";

   $counter = 0;
	
	if($date_post_start != "" || $date_post_end != "" || $status != "" || $key != ""){
		$queryJoin .= " WHERE a.t_owner_id = ".$userid." AND";
	}

	if($date_post_start != "" && $date_post_end == ""){
		$counter++;
	}
	if($date_post_start == "" && $date_post_end != ""){
		$counter++;
	}
	
	if($date_post_start != "" && $date_post_end != ""){
		$counter++;
	}
	
	if($status != ""){
		$counter++;
	}
	
	if($key != ""){
		$counter++;
	}

	if($date_post_start != "" && $date_post_end == ""){
		$queryJoin .= "  a.t_date_created BETWEEN '".$date_post_start." 00:00:00.000' AND '".$date_post_start." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}

	if($date_post_start == "" && $date_post_end != ""){
		$queryJoin .= " a.t_date_created <= '".$date_post_end." 23:59:59.997'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($date_post_start != "" && $date_post_end != ""){
  		$queryJoin .= " a.t_date_created BETWEEN '".$date_post_start."' AND '".$date_post_end."'";
		
  		if($counter > 1){
  			$counter--;
  			$queryJoin .= " AND";
  		}else{
  			$queryJoin .= " ";
  		}
	}
	
	if($status != ""){
		
				switch ($status){
				    	    case 1:
				    	    	//NEW
				    	    	$queryJoin .= "  a.t_status = '1'";
				    			break;
				    		case 2:
				    			//OUTSTANDING
				    			$queryJoin .= "  a.t_status = '2'";
				    			break;
				    	}

		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	if($key != ""){
		$queryJoin .= " ( a.t_date_created LIKE  '%".$key."%'";
		$queryJoin .= "  OR a.t_blklot LIKE '%".$key."%'";
 		$queryJoin .= "  OR a.t_status LIKE '%".$key."%')";
		if($counter > 1){
			$counter--;
			$queryJoin .= " AND";
		}else{
			$queryJoin .= " ";
		}
	}
	
	$queryJoin .=" ORDER BY a.t_date_created desc";

	$query = $this->db->query($queryJoin);
	//echo $this->db->last_query();
	// $this->output->enable_profiler(TRUE);	
	return $query->num_rows();
}

function get_buyer_reply($id){
	$query = "SELECT * ZHOA_concern_details
			  WHERE cd_concern_id = $id
			  ORDER BY cd_date_created ASC";
			  // LEFT JOIN ZHOA_PROJ_AREA as d ON c.PROJ_ID = d.PROJ_ID
	
	$query = $this->db->query($query);
	return $query->result_array();	
}

function get_admin_by_pcode($proj) {
	$query = "SELECT distinct  a.ar_usrid, b.*, c.*
			  FROM ZHOA_assign_role as a
			  left join ZHOA_admlogin as b on a.ar_usrid = b.a_id
			  left join ZHOA_adminfo as c on b.a_id = c.ai_id
			  where a.ar_pcode = ".$proj."
			  ORDER BY c.ai_fname ASC";
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_soa_date_by_so($so){
	$query = "SELECT * FROM ZSOA_ITEM as a
			LEFT JOIN ZHOA_BUYER_PROF as b on a.VBELN = b.VBELN
			WHERE a.VBELN = '".$so."' AND b.ACTIVE = 1
			ORDER BY a.SOA_YEAR ASC, a.SOA_MO ASC";
			
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_soa($soa,$so) {
		$query = "SELECT * FROM ZSOA_ITEM as a
			LEFT JOIN ZHOA_BUYER_PROF as b on a.VBELN = b.VBELN
			WHERE a.SOA_NUM = '".$soa."' AND a.VBELN = '".$so."' AND b.ACTIVE = 1
			ORDER BY a.SOA_YEAR ASC, a.SOA_MO ASC";
			
	$query = $this->db->query($query);
	return $query->result_array();
}

function get_project_soa($id){
	$query = "SELECT * FROM ZHOA_BUYER_PROF as a
			LEFT JOIN ZHOA_byrinfo as b on b.bi_cust_no = a.KUNNR
			LEFT JOIN ZHOA_PROJECTS as c on c.SWENR = a.SWENR
			WHERE a.ACTIVE = 1 AND c.SOA_ACTIVE = 1 AND b.bi_owner_id = ".$id."";
	$query = $this->db->query($query);
	return $query->result_array();
}

function restricted_mailnotif($pcode){
	$query = "SELECT * from ZHOA_assign_role as a
  		left join ZHOA_mail_notif as b on a.ar_usrid = b.mn_usrid
  		where a.ar_pcode = ".$pcode." AND b.mn_active = 1 AND b.mn_module = 1";
  	$query = $this->db->query($query);
	return $query->result_array();
}


}

?>